# ABOUT #
Personal Food Database is a GPL V3 licensed application that is designed to help with the creation of meal plans while also facilitating quick and easy access to the nutritional information of all the foods and recipes that you consume. 

There is also support for generating the grocery list for a meal planned calendar week based upon what the planned meals require.

The application uses an embedded H2 database that is stored in the application folder for easy portability. This means if you need to move to another machine or want to share your database with another person you can simply copy the database file and drop it in the application folder on the new machine. 

Application source code may be found on [Bitbucket](https://bitbucket.org/iceflyer3/personal-food-database-fx). Further information as well as the download for the application itself may be found on the [application website](https://pfd.iceflyer3.com).

# REQUIREMENTS #

### Drive Space ###
* Windows: ~88 MB of hard drive space
* Linux (x64): ~96 MB of hard drive space

# INSTALLATION #

Extract the contents of the downloaded application zip file. A copy of the required JRE is provided alongside the application so no further setup is required.

For Windows a batch file is provided and for Linux a shell script is provided to allow you to launch the application. 
 
### Running the application on Windows ##
Run the "launch-windows.bat" batch file by double-clicking it or invoking it from the command line to start the application.

### Running the application on Linux ##
If you are using Linux you have two options for starting the application (both from the terminal). First, open the terminal and navigate to the appropriate directory via the command ```cd [path-to-downloaded-directory]/pfd-[application-version]```

The first option is to execute the shell script with ```sh``` via ```sh launch-linux.sh```

The second option is to make the shell script executable and execute it directly.

Instructions for making the shell script executable and executing it may be found below in the event that you are unaware how to do so
1. Grant execute permissions with the command ```chmod +x launch-linux.sh```
3. Run the command ```./launch-linux.sh``` to launch the application

# DATABASE VERSION #
The version of the schema used by a given database file is indicated in the filename. For example a file with the name "pfd-db-1-0-0.mv.db" would indicate that the database file uses version 1.0.0 of the database schema.

# TECH STACK #
The Personal Food Database application is built using several other open source libraries. Below you may find the full tech stack used in the project. 

Build Tool
* Maven

Libraries
* JavaFX
* Spring Framework
* Spring Boot
* H2 Database
* Hibernate ORM
* Google Guava
* Logback
* Jackson
* ControlsFX
* CalendarFX

Unit Testing Libraries
* TestNG
* AssertJ

Iconography for the application is provided by [Font Awesome](https://fontawesome.com/)

# VERSIONING #

[SemVer](http://semver.org/) is used for versioning of the application and (insofar as it is applicable) the database schema.

# LICENSE #
The Personal Food Database application is released under the [GNU General Public License Version 3](https://www.gnu.org/licenses/gpl-3.0.en.html)