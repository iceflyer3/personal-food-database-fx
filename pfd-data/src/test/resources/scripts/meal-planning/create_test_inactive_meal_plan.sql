/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


-- Test user: 0b185cf5-47ab-486b-914b-2851c16a7bac

-- Meal plan data based on average American female.
-- See comment near bottom of MealPlanningCalculatorTest for further details

INSERT INTO
    PFD_V1.MEAL_PLAN
VALUES
    (
        '24f4e5a4-b7ac-4bee-b110-77800382cd0a',
        '0b185cf5-47ab-486b-914b-2851c16a7bac',
        162.56,
        77.55,
        20,
        'FEMALE',
        'MODERATE',
        'LOSE',
        'MIFFLIN_ST_JEOR',
        40,
        1897.83,
        2372.29,
        false
    );

INSERT INTO
    PFD_V1.MEAL_PLAN_NUTRIENT_SUGGESTION
VALUES
    (
        'da9ba3e0-21d0-46a5-8902-2fc8f01612b8',
        '24f4e5a4-b7ac-4bee-b110-77800382cd0a',
        'PROTEIN',
        '10',
        'GRAM'
    ),
    (
        '70a06b90-2fee-4466-94a4-317e90027a48',
        '24f4e5a4-b7ac-4bee-b110-77800382cd0a',
        'CARBOHYDRATES',
        '10',
        'GRAM'
    ),
    (
        'e5f31a7d-1d66-49ca-b057-9028745cd885',
        '24f4e5a4-b7ac-4bee-b110-77800382cd0a',
        'TOTAL_FATS',
        '10',
        'GRAM'
    );
