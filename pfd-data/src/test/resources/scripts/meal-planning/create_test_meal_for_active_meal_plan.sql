/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


-- For day id: aa67f942-412c-40bb-a925-8abca281ee32

INSERT INTO
    PFD_V1.MEAL_PLANNING_MEAL
VALUES
       (
        '8b170828-2b1b-4bb8-bbf5-ba9cb109d763',
        'aa67f942-412c-40bb-a925-8abca281ee32',
        'Test Meal One',
        CURRENT_DATE
       );