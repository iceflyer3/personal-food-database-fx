/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

-- Test carb cycling plan id: 78a3e26a-75b4-49eb-9031-e3589331c91d

INSERT INTO
    PFD_V1.CARB_CYCLING_MEAL_CONFIGURATION
VALUES
    ('884ea197-b596-4ad2-8288-dd0b2f15bee9','78a3e26a-75b4-49eb-9031-e3589331c91d',10.00,15.00,2,'AFTER',true),
    ('98ba9187-526e-483c-bb40-6753f4121ff5','78a3e26a-75b4-49eb-9031-e3589331c91d',0.00,0.00,3,'BEFORE',true),
    ('da8b3f1a-f5c1-44b2-99d9-6c05d290e6cc','78a3e26a-75b4-49eb-9031-e3589331c91d',20.00,25.00,1,'AFTER',true),
    ('7a4d523b-eed5-461d-bd24-11104eb40b71','78a3e26a-75b4-49eb-9031-e3589331c91d',15.00,17.00,3,'NONE',false),
    ('6877e984-c193-4be1-835a-df63e1151ef9','78a3e26a-75b4-49eb-9031-e3589331c91d',0.00,0.00,2,'BEFORE',true),
    ('836fc5ab-2e84-42ae-91e5-4e6dd5ed35b1','78a3e26a-75b4-49eb-9031-e3589331c91d',15.00,17.00,4,'NONE',false),
    ('5f27f6b2-bd14-4e6c-9d5c-ac2de1ea9c4e','78a3e26a-75b4-49eb-9031-e3589331c91d',10.00,15.00,3,'AFTER',true),
    ('292eee98-9d76-4505-97b3-d18399799646','78a3e26a-75b4-49eb-9031-e3589331c91d',30.00,35.00,1,'NONE',false),
    ('ee4ad62a-f49d-4618-a455-da2fb67d46e3','78a3e26a-75b4-49eb-9031-e3589331c91d',0.00,0.00,4,'BEFORE',true),
    ('03188361-2827-4958-85a0-aa19accf94ee','78a3e26a-75b4-49eb-9031-e3589331c91d',0.00,0.00,1,'BEFORE',true),
    ('662be539-0603-439d-9e4c-5e6b376e0d7a','78a3e26a-75b4-49eb-9031-e3589331c91d',10.00,15.00,4,'AFTER',true),
    ('c79839e2-7055-4d7b-a41b-45811bdf86d6','78a3e26a-75b4-49eb-9031-e3589331c91d',15.00,17.00,2,'NONE',false),
    ('86bde7c8-7af5-4a67-961e-4422481a9f70','78a3e26a-75b4-49eb-9031-e3589331c91d',15.00,17.00,5,'NONE',false),
    ('41f356cd-f3fb-4cae-a835-ccc225fa1f69','78a3e26a-75b4-49eb-9031-e3589331c91d',30.00,35.00,0,'NONE',true);

