/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

-- Test carb cycling plan id: 78a3e26a-75b4-49eb-9031-e3589331c91d
-- Test active meal plan id: 16171b52-94f7-4ab1-85c4-917c0f95121c

INSERT INTO
    PFD_V1.MEAL_PLANNING_DAY
VALUES
    (
        '56d6d3f3-e41b-4088-831b-f07c5aa4e577',
        '16171b52-94f7-4ab1-85c4-917c0f95121c',
        'LOSE',
        'Unit Test Carb Cycling Day 1',
        CURRENT_DATE
    );

INSERT INTO
    PFD_V1.CARB_CYCLING_DAY
VALUES
       (
        '56d6d3f3-e41b-4088-831b-f07c5aa4e577',
        '78a3e26a-75b4-49eb-9031-e3589331c91d',
        'HIGH',
        true,
        100,
        10,
        10,
        10
       );