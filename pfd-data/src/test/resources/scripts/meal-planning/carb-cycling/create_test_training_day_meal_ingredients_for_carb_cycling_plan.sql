/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

-- Ingredients comprised of the two complex food test foods for the meal of the day that belongs
-- to the test carb cycling plan
-- Meal Id:  d344812e-2fc9-4218-b497-de6bbe82e09b
-- Food 1 Id: d78b2a7d-f62f-4ec7-aecc-12bb76c047ee
-- Food 2 Id: bc0cea4d-977d-4b15-9e38-e311dbb531f5

INSERT INTO
    PFD_V1.MEAL_PLANNING_MEAL_INGREDIENT
VALUES
    (
        '59e88e76-01f6-41b8-804b-69f08a7c64c3', -- Ingredient Id
        'd78b2a7d-f62f-4ec7-aecc-12bb76c047ee', -- Ingredient Food Id
        'd344812e-2fc9-4218-b497-de6bbe82e09b', -- Meal Id
        1,
        false,
        false
    );

INSERT INTO
    PFD_V1.MEAL_PLANNING_MEAL_INGREDIENT
VALUES
    (
        'fe43fd79-5fe9-4fc2-b4e2-4cb6762a481e', -- Ingredient Id
        'bc0cea4d-977d-4b15-9e38-e311dbb531f5', -- Ingredient Food Id
        'd344812e-2fc9-4218-b497-de6bbe82e09b', -- Meal Id
        2,
        false,
        false
    );