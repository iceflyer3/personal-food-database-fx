/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

-- Test carb cycling day id: 56d6d3f3-e41b-4088-831b-f07c5aa4e577

INSERT INTO
    PFD_V1.MEAL_PLANNING_MEAL
VALUES
    (
        'd344812e-2fc9-4218-b497-de6bbe82e09b',
        '56d6d3f3-e41b-4088-831b-f07c5aa4e577',
        'Test Carb Cycling Meal One',
        CURRENT_DATE
    );


INSERT INTO
    PFD_V1.CARB_CYCLING_MEAL
VALUES
       (
        'd344812e-2fc9-4218-b497-de6bbe82e09b',
        '56d6d3f3-e41b-4088-831b-f07c5aa4e577',
        'POST_WORKOUT',
        1,
        30,
        100,
        10,
        10,
        10
       );