/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


-- Ingredients comprised of the two complex food test foods for the meal of the day that belongs
-- to the test active meal plan.
-- Meal Id:  8b170828-2b1b-4bb8-bbf5-ba9cb109d763
-- Food 1 Id: d78b2a7d-f62f-4ec7-aecc-12bb76c047ee
-- Food 2 Id: bc0cea4d-977d-4b15-9e38-e311dbb531f5

INSERT INTO
    PFD_V1.MEAL_PLANNING_MEAL_INGREDIENT
VALUES
       (
        '4df80479-dc03-4dee-9f23-751da82d4a8c', -- Ingredient Id
        'd78b2a7d-f62f-4ec7-aecc-12bb76c047ee', -- Ingredient Food Id
        '8b170828-2b1b-4bb8-bbf5-ba9cb109d763', -- Meal Id
        1,
        false,
        false
       );

INSERT INTO
    PFD_V1.MEAL_PLANNING_MEAL_INGREDIENT
VALUES
    (
        '5895c755-3bec-40ce-b2f6-d1f4c2f43254', -- Ingredient Id
        'bc0cea4d-977d-4b15-9e38-e311dbb531f5', -- Ingredient Food Id
        '8b170828-2b1b-4bb8-bbf5-ba9cb109d763', -- Meal Id
        2,
        false,
        false
    );