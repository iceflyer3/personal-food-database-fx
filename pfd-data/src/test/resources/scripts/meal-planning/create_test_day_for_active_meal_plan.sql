/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


-- Day for the active test meal plan: 16171b52-94f7-4ab1-85c4-917c0f95121c

INSERT INTO
    PFD_V1.MEAL_PLANNING_DAY
VALUES
    (
     'aa67f942-412c-40bb-a925-8abca281ee32',
     '16171b52-94f7-4ab1-85c4-917c0f95121c',
     'LOSE',
     'Unit Test Day 1',
     CURRENT_DATE
    );