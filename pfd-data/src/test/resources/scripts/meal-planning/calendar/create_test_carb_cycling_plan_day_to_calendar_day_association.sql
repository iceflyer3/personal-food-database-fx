/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

-- Test carb cycling day id: 56d6d3f3-e41b-4088-831b-f07c5aa4e577

INSERT INTO
    PFD_V1.MEAL_PLAN_CALENDAR_DAY
VALUES (
           '1981a6fc-8dfe-4507-b380-bba5407fbc7c', -- Calendar day association Id
           '56d6d3f3-e41b-4088-831b-f07c5aa4e577', -- Carb Cycling Plan Day Id
           '0b185cf5-47ab-486b-914b-2851c16a7bac', -- User Id
           '2023-01-01',
           'NONE',
           0,
           false
       );