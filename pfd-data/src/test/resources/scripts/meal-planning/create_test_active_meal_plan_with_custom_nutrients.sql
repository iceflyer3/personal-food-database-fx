/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023, 2025  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

-- Test user: 0b185cf5-47ab-486b-914b-2851c16a7bac

-- Meal plan data based on average American male.
-- See comment near bottom of MealPlanningCalculatorTest for further details

-- Active male meal plan
INSERT INTO
    PFD_V1.MEAL_PLAN
VALUES
    (
        '16171b52-94f7-4ab1-85c4-917c0f95121c',
        '0b185cf5-47ab-486b-914b-2851c16a7bac',
        175.26,
        90.70,
        20,
        'MALE',
        'MODERATE',
        'LOSE',
        'MIFFLIN_ST_JEOR',
        28.0,
        2365.18,
        2956.48,
        true
    );

INSERT INTO
    PFD_V1.MEAL_PLAN_NUTRIENT_SUGGESTION
VALUES
    (
        '077e6e45-1d11-42f5-bffb-ba3da19a45a4',
        '16171b52-94f7-4ab1-85c4-917c0f95121c',
        'PROTEIN',
        '10',
        'GRAM'
    ),
    (
        '6cdbd104-00e2-4f68-9ad7-a3f039402c32',
        '16171b52-94f7-4ab1-85c4-917c0f95121c',
        'CARBOHYDRATES',
        '10',
        'GRAM'
    ),
    (
        '09e7f994-2c95-4ecb-a384-308b041db160',
        '16171b52-94f7-4ab1-85c4-917c0f95121c',
        'TOTAL_FATS',
        '10',
        'GRAM'
    ),
    (
        '75fa7d37-9b7c-4fee-b43a-3c6a87164e55',
        '16171b52-94f7-4ab1-85c4-917c0f95121c',
        'FIBER',
        '10',
        'GRAM'
    ),
    (
        'a4376a38-4a52-4701-9c95-4799bcc4ab5d',
        '16171b52-94f7-4ab1-85c4-917c0f95121c',
        'VITAMIN_A',
        '10',
        'MICROGRAM'
    );
