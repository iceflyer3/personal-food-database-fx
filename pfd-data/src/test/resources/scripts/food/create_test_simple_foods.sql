/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

-- Test User is 0b181cf5-47ab-486b-914b-2851c16a7bac
INSERT INTO PFD_V1.FOOD_META_DATA VALUES ('f0c183ed-79cf-43ce-a86d-f6fe1efa3a1f', 'SIMPLE');
INSERT INTO PFD_V1.FOOD_SERVING_SIZE VALUES ('fac8fc8f-5df1-43e5-af14-35b3369009d6', 'GRAM', 1, 'f0c183ed-79cf-43ce-a86d-f6fe1efa3a1f');
INSERT INTO PFD_V1.FOOD_SERVING_SIZE VALUES ('b68b5baa-dd81-4376-bd5d-1c511e93226a', 'POUND', 1, 'f0c183ed-79cf-43ce-a86d-f6fe1efa3a1f');
INSERT INTO
    PFD_V1.SIMPLE_FOOD
VALUES
    (
     'f0c183ed-79cf-43ce-a86d-f6fe1efa3a1f',
     'TestFood1',
     'TestBrand',
     'TestSource',
     '0b185cf5-47ab-486b-914b-2851c16a7bac',
     CURRENT_DATE,
     '0b185cf5-47ab-486b-914b-2851c16a7bac',
     CURRENT_DATE,
     0,
     100,
     10,
     10,
     10,
     10,
     10,
     10,
     10,
     10,
     10,
     10,
     10,
     10,
     10,
     10,
     10,
     10,
     10,
     10,
     10,
     10,
     10,
     10,
     10,
     10,
     10,
     10,
     10,
     10,
     10,
     10,
     10,
     10,
     10,
     10
    );

INSERT INTO PFD_V1.FOOD_META_DATA VALUES ('d9aa90da-7e27-4ce9-a869-b49a4c402876', 'SIMPLE');
INSERT INTO PFD_V1.FOOD_SERVING_SIZE VALUES ('19c631f2-ac8f-469a-bcf8-5a98a8f838cf', 'GRAM', 1, 'd9aa90da-7e27-4ce9-a869-b49a4c402876');
INSERT INTO PFD_V1.FOOD_SERVING_SIZE VALUES ('0aabef9e-a04d-4161-8a0a-331a13d3371e', 'POUND', 1, 'd9aa90da-7e27-4ce9-a869-b49a4c402876');
INSERT INTO
    PFD_V1.SIMPLE_FOOD
VALUES
    (
        'd9aa90da-7e27-4ce9-a869-b49a4c402876',
        'TestFood2',
        'TestBrand',
        'TestSource',
        '0b185cf5-47ab-486b-914b-2851c16a7bac',
        CURRENT_DATE,
        '0b185cf5-47ab-486b-914b-2851c16a7bac',
        CURRENT_DATE,
        0,
        100,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
        5
    );

INSERT INTO PFD_V1.FOOD_META_DATA VALUES ('57dfe6e3-d6c0-46f9-a3e5-ba08d9ea03d3', 'SIMPLE');
INSERT INTO PFD_V1.FOOD_SERVING_SIZE VALUES ('33e960e1-9898-4f84-a249-1674ce4b62c4', 'GRAM', 1, '57dfe6e3-d6c0-46f9-a3e5-ba08d9ea03d3');
INSERT INTO PFD_V1.FOOD_SERVING_SIZE VALUES ('aeff9a89-6766-42ce-844d-5d445ae22efb', 'POUND', 1, '57dfe6e3-d6c0-46f9-a3e5-ba08d9ea03d3');
INSERT INTO
    PFD_V1.SIMPLE_FOOD
VALUES
    (
        '57dfe6e3-d6c0-46f9-a3e5-ba08d9ea03d3',
        'TestFood3',
        'TestBrand',
        'TestSource',
        '0b185cf5-47ab-486b-914b-2851c16a7bac',
        CURRENT_DATE,
        '0b185cf5-47ab-486b-914b-2851c16a7bac',
        CURRENT_DATE,
        0,
        100,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1
    );