/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

-- Simple food 1: f0c183ed-79cf-43ce-a86d-f6fe1efa3a1f
-- Simple food 2: d9aa90da-7e27-4ce9-a869-b49a4c402876
-- Simple food 3: 57dfe6e3-d6c0-46f9-a3e5-ba08d9ea03d3

-- Complex Food 1
INSERT INTO PFD_V1.FOOD_META_DATA VALUES ('d78b2a7d-f62f-4ec7-aecc-12bb76c047ee', 'COMPLEX');
INSERT INTO PFD_V1.FOOD_SERVING_SIZE VALUES ('05b9ab64-2235-40ce-806f-ed303a8fda4a', 'GRAM', 1, 'd78b2a7d-f62f-4ec7-aecc-12bb76c047ee');
INSERT INTO PFD_V1.FOOD_SERVING_SIZE VALUES ('5a2bd2ec-6f4c-4e25-8973-c1d63ca07146', 'POUND', 1, 'd78b2a7d-f62f-4ec7-aecc-12bb76c047ee');
INSERT INTO
    PFD_V1.COMPLEX_FOOD
VALUES
       ('d78b2a7d-f62f-4ec7-aecc-12bb76c047ee', 'UnitTestFood', '0b185cf5-47ab-486b-914b-2851c16a7bac', CURRENT_DATE, '0b185cf5-47ab-486b-914b-2851c16a7bac', CURRENT_DATE);

INSERT INTO
    PFD_V1.COMPLEX_FOOD_INGREDIENT
VALUES
       (
        'b6c0ea79-351f-4056-b32a-59a2a94089c4', -- Ingredient ID
        'f0c183ed-79cf-43ce-a86d-f6fe1efa3a1f', -- Simple Food ID
        'd78b2a7d-f62f-4ec7-aecc-12bb76c047ee', -- Complex Food ID
        1,
        false,
        false
       );

INSERT INTO
    PFD_V1.COMPLEX_FOOD_INGREDIENT
VALUES
    (
        '222c251d-8259-44b0-a47f-76d6db97c583',
        'd9aa90da-7e27-4ce9-a869-b49a4c402876',
        'd78b2a7d-f62f-4ec7-aecc-12bb76c047ee',
        1,
        false,
        false
    );

-- Complex Food 2
INSERT INTO PFD_V1.FOOD_META_DATA VALUES ('bc0cea4d-977d-4b15-9e38-e311dbb531f5', 'COMPLEX');
INSERT INTO PFD_V1.FOOD_SERVING_SIZE VALUES ('8a593f88-3523-438a-abf5-b80ff79b4b9c', 'GRAM', 1, 'bc0cea4d-977d-4b15-9e38-e311dbb531f5');
INSERT INTO PFD_V1.FOOD_SERVING_SIZE VALUES ('f8b93aa0-7ebe-447a-9213-c2b24cb90d52', 'POUND', 1, 'bc0cea4d-977d-4b15-9e38-e311dbb531f5');
INSERT INTO
    PFD_V1.COMPLEX_FOOD
VALUES
    ('bc0cea4d-977d-4b15-9e38-e311dbb531f5', 'UnitTestFood', '0b185cf5-47ab-486b-914b-2851c16a7bac', CURRENT_DATE, '0b185cf5-47ab-486b-914b-2851c16a7bac', CURRENT_DATE);

INSERT INTO
    PFD_V1.COMPLEX_FOOD_INGREDIENT
VALUES
    (
        '9ba519f8-a495-4b68-9384-26cdc19d550c',
        'd9aa90da-7e27-4ce9-a869-b49a4c402876',
        'bc0cea4d-977d-4b15-9e38-e311dbb531f5',
        1,
        false,
        false
    );

INSERT INTO
    PFD_V1.COMPLEX_FOOD_INGREDIENT
VALUES
    (
        '7c5f77c3-ac78-4c4f-95f2-92afad22f3a5',
        '57dfe6e3-d6c0-46f9-a3e5-ba08d9ea03d3',
        'bc0cea4d-977d-4b15-9e38-e311dbb531f5',
        1,
        false,
        false
    );