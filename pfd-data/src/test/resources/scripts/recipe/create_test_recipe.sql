/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

-- Test user id: 0b185cf5-47ab-486b-914b-2851c16a7bac

INSERT INTO
    PFD_V1.RECIPE
VALUES
       (
        'c2e39b67-6ac9-4d15-8c63-daac5d197794',
        'Unit Test Recipe',
        'Unit Test Source',
        '1',
        '0b185cf5-47ab-486b-914b-2851c16a7bac',
        CURRENT_DATE,
        '0b185cf5-47ab-486b-914b-2851c16a7bac',
        CURRENT_DATE
       );

INSERT INTO
    PFD_V1.RECIPE_SERVING_SIZE
VALUES
       (
        '70ad465e-67a5-4c20-96dd-6a5fde97a32c',
        'c2e39b67-6ac9-4d15-8c63-daac5d197794',
        'GRAM',
        '1'
       );