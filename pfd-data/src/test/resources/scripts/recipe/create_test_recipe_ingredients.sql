/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

-- Recipe id: c2e39b67-6ac9-4d15-8c63-daac5d197794
-- Food 1 Id: f0c183ed-79cf-43ce-a86d-f6fe1efa3a1f
-- Food 2 Id: d9aa90da-7e27-4ce9-a869-b49a4c402876

INSERT INTO
    PFD_V1.RECIPE_INGREDIENT
VALUES
    (
     'af897eb8-189b-4660-b457-1b5404e6d0fb',
     'c2e39b67-6ac9-4d15-8c63-daac5d197794',
     'f0c183ed-79cf-43ce-a86d-f6fe1efa3a1f',
     1,
     false,
     false
    );

INSERT INTO
    PFD_V1.RECIPE_INGREDIENT
VALUES
       (
        '3f91217a-bd53-4b85-84f5-686e0241899d',
        'c2e39b67-6ac9-4d15-8c63-daac5d197794',
        'd9aa90da-7e27-4ce9-a869-b49a4c402876',
        1,
        false,
        false
       );