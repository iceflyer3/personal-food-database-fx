/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

-- Recipe id: c2e39b67-6ac9-4d15-8c63-daac5d197794
-- Ingredient 1 Id: af897eb8-189b-4660-b457-1b5404e6d0fb
-- Ingredient 2 Id: 3f91217a-bd53-4b85-84f5-686e0241899d

-- Ingredient 1
INSERT INTO
    PFD_V1.RECIPE_STEP
VALUES
       (
        '7baf6b0f-7514-4e7a-aa0d-0b190ad29d6a',
        'c2e39b67-6ac9-4d15-8c63-daac5d197794',
        'Unit Test Step One',
        1,
        'This is the first step'
       );

INSERT INTO
    PFD_V1.RECIPE_STEP_INGREDIENT
VALUES
       (
        'c2e39b67-6ac9-4d15-8c63-daac5d197794',
        '7baf6b0f-7514-4e7a-aa0d-0b190ad29d6a',
        'af897eb8-189b-4660-b457-1b5404e6d0fb'
       );


-- Ingredient 2
INSERT INTO
    PFD_V1.RECIPE_STEP
VALUES
       (
        'b28699d4-a60b-4bc0-8353-c2f588e03640',
        'c2e39b67-6ac9-4d15-8c63-daac5d197794',
        'Unit Test Step Two',
        2,
        'This is the second step'
       );

INSERT INTO
    PFD_V1.RECIPE_STEP_INGREDIENT
VALUES
    (
        'c2e39b67-6ac9-4d15-8c63-daac5d197794',
        'b28699d4-a60b-4bc0-8353-c2f588e03640',
        '3f91217a-bd53-4b85-84f5-686e0241899d'
    );