/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.test.data.task.mealplanning;

import com.iceflyer3.pfd.test.data.config.FxTaskTest;
import com.iceflyer3.pfd.test.data.config.TestDbConstants;
import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanMeal;
import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanMealIngredient;
import com.iceflyer3.pfd.data.mapping.IngredientMapper;
import com.iceflyer3.pfd.data.mapping.mealplanning.MealPlanDayMapper;
import com.iceflyer3.pfd.data.mapping.mealplanning.MealPlanMealMapper;
import com.iceflyer3.pfd.data.repository.FoodRepository;
import com.iceflyer3.pfd.data.repository.MealPlanningRepository;
import com.iceflyer3.pfd.test.data.config.FxRuntimeBootstrappedBase;
import com.iceflyer3.pfd.data.task.mealplanning.meal.*;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.ReadOnlyFoodViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanDayViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanMealViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static org.assertj.core.api.Assertions.assertThat;

@FxTaskTest
public class MealPlanMealTaskTests extends FxRuntimeBootstrappedBase
{
    @Autowired
    private FoodRepository foodRepository;

    @Autowired
    private MealPlanningRepository mealPlanningRepository;

    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_MEAL_FOR_DAY_FOR_ACTIVE_MEAL_PLAN
    })
    public void Find_All_Meals_For_Day()
    {
        // Arrange
        UUID dayId = UUID.fromString(TestDbConstants.ACTIVE_MEAL_PLAN_DAY_ONE_ID);
        GetMealsForDayTask sut = new GetMealsForDayTask(mealPlanningRepository, dayId);
        List<MealPlanMealViewModel> retrievedMeals = new ArrayList<>();

        // Act
        try
        {
            sut.run();
            retrievedMeals = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(retrievedMeals.size()).isEqualTo(1);
    }

    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_MEAL_FOR_DAY_FOR_ACTIVE_MEAL_PLAN
    })
    public void Find_Meal_By_Id()
    {
        // Arrange
        UUID mealId = UUID.fromString(TestDbConstants.ACTIVE_MEAL_PLAN_DAY_ONE_MEAL_ONE_ID);
        GetMealByIdTask sut = new GetMealByIdTask(mealPlanningRepository, mealId);
        MealPlanMealViewModel retrievedMeal = null;

        // Act
        try
        {
            sut.run();
            retrievedMeal = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(retrievedMeal).isNotNull();
        assertThat(retrievedMeal.getId()).isEqualByComparingTo(mealId);
    }

    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_DAY_FOR_ACTIVE_MEAL_PLAN
    })
    public void Create_New_Meal()
    {
        // Arrange
        UUID dayId = UUID.fromString(TestDbConstants.ACTIVE_MEAL_PLAN_DAY_ONE_ID);
        MealPlanDayViewModel dayViewModel = MealPlanDayMapper.toModel(mealPlanningRepository.getDayForId(dayId), NutritionalSummary.empty());
        MealPlanMealViewModel newMealViewModel = new MealPlanMealViewModel(null, NutritionalSummary.empty());
        newMealViewModel.setName("Test Meal One");

        SaveMealForMealPlanTask sut = new SaveMealForMealPlanTask(mealPlanningRepository, dayViewModel, newMealViewModel);
        MealPlanMealViewModel savedMeal = null;

        // Act
        try
        {
            sut.run();
            savedMeal = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(savedMeal).isNotNull();
        assertThat(savedMeal.getId()).isNotNull();
    }

    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_MEAL_FOR_DAY_FOR_ACTIVE_MEAL_PLAN
    })
    public void Update_Existing_Meal()
    {
        // Arrange
        UUID mealId = UUID.fromString(TestDbConstants.ACTIVE_MEAL_PLAN_DAY_ONE_MEAL_ONE_ID);
        MealPlanMealViewModel exisitingMeal = MealPlanMealMapper.toModel(mealPlanningRepository.getMealForId(mealId), NutritionalSummary.empty());

        UUID dayId = UUID.fromString(TestDbConstants.ACTIVE_MEAL_PLAN_DAY_ONE_ID);
        MealPlanDayViewModel dayViewModel = MealPlanDayMapper.toModel(mealPlanningRepository.getDayForId(dayId), NutritionalSummary.empty());

        String newMealName = "Updated Meal Planning Meal";
        exisitingMeal.setName(newMealName);

        SaveMealForMealPlanTask sut = new SaveMealForMealPlanTask(mealPlanningRepository, dayViewModel, exisitingMeal);
        MealPlanMealViewModel updatedMeal = null;

        // Act
        try
        {
            sut.run();
            updatedMeal = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(updatedMeal).isNotNull();
        assertThat(updatedMeal.getId()).isEqualByComparingTo(mealId);
        assertThat(updatedMeal.getName()).isEqualTo(newMealName);
    }

    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS,
            TestDbConstants.SCRIPT_TEST_COMPLEX_FOODS,
            TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_MEAL_FOR_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_MEAL_INGREDIENTS_FOR_DAY_FOR_ACTIVE_MEAL_PLAN
    })
    public void Delete_Meal()
    {
        // Arrange
        UUID dayId = UUID.fromString(TestDbConstants.ACTIVE_MEAL_PLAN_DAY_ONE_ID);
        UUID mealId = UUID.fromString(TestDbConstants.ACTIVE_MEAL_PLAN_DAY_ONE_MEAL_ONE_ID);
        DeleteMealPlanMealTask sut = new DeleteMealPlanMealTask(mealPlanningRepository, mealId);

        // Act
        try
        {
            sut.run();
            sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        List<MealPlanMeal> allMealsForDay = mealPlanningRepository.getMealsForDay(dayId);

        // Assert
        assertThat(allMealsForDay.size()).isEqualTo(0);
    }

    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS,
            TestDbConstants.SCRIPT_TEST_COMPLEX_FOODS,
            TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_MEAL_FOR_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_MEAL_INGREDIENTS_FOR_DAY_FOR_ACTIVE_MEAL_PLAN
    })
    public void Find_Ingredients_For_Meal()
    {
        // Arrange
        UUID mealId = UUID.fromString(TestDbConstants.ACTIVE_MEAL_PLAN_DAY_ONE_MEAL_ONE_ID);
        GetMealIngredientsForMealTask sut = new GetMealIngredientsForMealTask(mealPlanningRepository, foodRepository, mealId);
        List<IngredientModel> retrievedIngredients = new ArrayList<>();

        // Act
        try
        {
            sut.run();
            retrievedIngredients = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(retrievedIngredients.size()).isGreaterThan(0);
    }

    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS,
            TestDbConstants.SCRIPT_TEST_COMPLEX_FOODS,
            TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_MEAL_FOR_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_MEAL_INGREDIENTS_FOR_DAY_FOR_ACTIVE_MEAL_PLAN
    })
    public void Add_Ingredients_To_Meal()
    {
        // Arrange
        UUID mealId = UUID.fromString(TestDbConstants.ACTIVE_MEAL_PLAN_DAY_ONE_MEAL_ONE_ID);
        MealPlanMealViewModel existingMeal = MealPlanMealMapper.toModel(mealPlanningRepository.getMealForId(mealId), NutritionalSummary.empty());

        List<IngredientModel> ingredients = loadIngredientsForMeal(mealId);
        existingMeal.setIngredients(ingredients);

        UUID ingredientFoodId = UUID.fromString(TestDbConstants.SIMPLE_FOOD_ONE_ID);
        ReadOnlyFoodViewModel newIngredientFood = foodRepository.getFoodForId(ingredientFoodId);
        existingMeal.addIngredient(newIngredientFood);

        SaveMealIngredientsForMealPlanTask sut = new SaveMealIngredientsForMealPlanTask(mealPlanningRepository, foodRepository, existingMeal);
        List<IngredientModel> updatedIngredientList = new ArrayList<>();

        // Act
        try
        {
            sut.run();
            updatedIngredientList = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(updatedIngredientList.size()).isEqualTo(3);
    }

    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS,
            TestDbConstants.SCRIPT_TEST_COMPLEX_FOODS,
            TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_MEAL_FOR_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_MEAL_INGREDIENTS_FOR_DAY_FOR_ACTIVE_MEAL_PLAN
    })
    public void Remove_Ingredients_From_Meal()
    {
        // Arrange
        UUID mealId = UUID.fromString(TestDbConstants.ACTIVE_MEAL_PLAN_DAY_ONE_MEAL_ONE_ID);
        MealPlanMealViewModel existingMeal = MealPlanMealMapper.toModel(mealPlanningRepository.getMealForId(mealId), NutritionalSummary.empty());

        List<IngredientModel> ingredients = loadIngredientsForMeal(mealId);
        existingMeal.setIngredients(ingredients);
        existingMeal.removeIngredient(ingredients.get(1));

        SaveMealIngredientsForMealPlanTask sut = new SaveMealIngredientsForMealPlanTask(mealPlanningRepository, foodRepository, existingMeal);
        List<IngredientModel> updatedIngredientList = new ArrayList<>();

        // Act
        try
        {
            sut.run();
            updatedIngredientList = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(updatedIngredientList.size()).isEqualTo(1);
    }

    // Load the ingredients. Usually this is handled by the proxy but since we aren't using
    // the proxy here we've gotta do it manually.
    private List<IngredientModel> loadIngredientsForMeal(UUID mealId)
    {
        List<IngredientModel> ingredientViewModels = new ArrayList<>();
        List<MealPlanMealIngredient> ingredientEntities = mealPlanningRepository.getIngredientsForMeal(mealId  );
        for(MealPlanMealIngredient ingredientEntity : ingredientEntities)
        {
            ReadOnlyFoodViewModel ingredientFood = foodRepository.getFoodForId(ingredientEntity.getFoodMeta().getFoodId());
            ingredientViewModels.add(IngredientMapper.toModel(ingredientEntity, ingredientFood));
        }
        return ingredientViewModels;
    }
}
