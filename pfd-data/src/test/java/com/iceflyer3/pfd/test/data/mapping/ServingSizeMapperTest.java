package com.iceflyer3.pfd.test.data.mapping;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.test.data.TestDataProvider;
import com.iceflyer3.pfd.data.entities.food.FoodServingSize;
import com.iceflyer3.pfd.data.entities.food.SimpleFood;
import com.iceflyer3.pfd.data.entities.recipe.Recipe;
import com.iceflyer3.pfd.data.entities.recipe.RecipeServingSize;
import com.iceflyer3.pfd.data.mapping.ServingSizeMapper;
import com.iceflyer3.pfd.enums.UnitOfMeasure;
import com.iceflyer3.pfd.ui.model.viewmodel.ServingSizeViewModel;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class ServingSizeMapperTest
{
    TestDataProvider testDataProvider = new TestDataProvider();

    @Test
    public void Map_Food_Serving_Size_Entity()
    {
        // Arrange
        SimpleFood simpleFood = testDataProvider.getTestSimpleFoodEntity(1);
        FoodServingSize servingSizeEntity = simpleFood
                .getFoodMetaData()
                .getServingSizes()
                .stream()
                .filter(servingSize -> servingSize.getUnitOfMeasure() == UnitOfMeasure.GRAM)
                .findFirst()
                .get();

        // Act
        ServingSizeViewModel servingSizeViewModel = ServingSizeMapper.toModel(servingSizeEntity);

        // Assert
        assertThat(servingSizeEntity)
                .usingRecursiveComparison()
                .ignoringFields("foodMeta")
                .isEqualTo(servingSizeViewModel);
    }

    @Test
    public void Map_Recipe_Serving_Size_Entity()
    {
        // Arrange
        Recipe testRecipe = testDataProvider.getEmptyTestRecipeEntity();
        RecipeServingSize servingSizeEntity = new RecipeServingSize(testRecipe, UnitOfMeasure.GRAM, BigDecimal.ONE);

        // Act
        ServingSizeViewModel mappedViewModel = ServingSizeMapper.toModel(servingSizeEntity);

        // Assert
        assertThat(servingSizeEntity)
                .usingRecursiveComparison()
                .ignoringFields("recipe")
                .isEqualTo(mappedViewModel);
    }
}
