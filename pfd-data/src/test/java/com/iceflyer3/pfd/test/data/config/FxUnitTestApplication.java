/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


package com.iceflyer3.pfd.test.data.config;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FxUnitTestApplication extends Application
{
    private final static ExecutorService executorService = Executors.newSingleThreadExecutor();

    public static void launchApplication()
    {
        /*
         * This must be started on another thread because, as stated by the Javadoc for the launch
         * function, "The launch method does not return until the application has exited, either via
         * a call to Platform.exit() or all of the application windows have been closed."
         */
        executorService.submit(() -> Application.launch());
    }

    public static void stopApplication()
    {
        Platform.exit();
    }

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        // No work to do here.
        // Just a dummy application so JavaFx can bootstrap itself.
    }
}