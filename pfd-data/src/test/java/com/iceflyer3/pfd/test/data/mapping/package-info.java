/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Package that contains the unit tests for the mappers that map data from the database to
 * view models.
 *
 * Each class in this package is designed to only test the mapper it is named after. This means that if
 * a view model requires additional information (such as ingredients or a nutritional summary for example)
 * that are not part of the Hibernate entity object then the test will use dummy information.
 *
 * These tests only test the mapping from the Hibernate entity class to the view model class.
 */

package com.iceflyer3.pfd.test.data.mapping;