package com.iceflyer3.pfd.test.data.calculator.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.calculator.mealplanning.CalculationResult;
import com.iceflyer3.pfd.data.calculator.mealplanning.MealPlanningCalculator;
import com.iceflyer3.pfd.enums.*;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.registration.MealPlanningCalculatedRegistration;
import com.iceflyer3.pfd.util.NumberUtils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class MealPlanningCalculatorTest
{

    @Test(dataProvider = "tdeeCaloricRequirementsForMaleProvider")
    public void TDEE_Caloric_Requirements_For_Male(BasalMetabolicRateFormula bmrFormula, ActivityLevel activityLevel, BigDecimal expectedTdeeCalories)
    {
        // Arrange
        MealPlanningCalculatedRegistration maleCalculationRequest = getInitializedMaleCalcRequest();
        maleCalculationRequest.setActivityLevel(activityLevel);
        maleCalculationRequest.setBmrFormula(bmrFormula);

        // One calorie above or below expected allowable error
        BigDecimal expectedMin = NumberUtils.withStandardPrecision(expectedTdeeCalories.subtract(BigDecimal.ONE));
        BigDecimal expectedMax = NumberUtils.withStandardPrecision(expectedTdeeCalories.add(BigDecimal.ONE));

        // Act
        MealPlanningCalculator systemUnderTest = MealPlanningCalculator.instance();
        CalculationResult calculationResult = systemUnderTest.calculatePlanRequirements(maleCalculationRequest);

        // Assert
        assertThat(calculationResult.getTdeeCalories()).isBetween(expectedMin, expectedMax);
    }

    @Test(dataProvider = "tdeeCaloricRequirementsForFemaleProvider")
    public void TDEE_Caloric_Requirements_For_Female(BasalMetabolicRateFormula bmrFormula, ActivityLevel activityLevel, BigDecimal expectedTdeeCalories)
    {
        // Arrange
        MealPlanningCalculatedRegistration femaleCalculationRequest = getInitializedFemaleCalcRequest();
        femaleCalculationRequest.setActivityLevel(activityLevel);
        femaleCalculationRequest.setBmrFormula(bmrFormula);

        // One calorie above or below expected allowable error
        BigDecimal expectedMin = NumberUtils.withStandardPrecision(expectedTdeeCalories.subtract(BigDecimal.ONE));
        BigDecimal expectedMax = NumberUtils.withStandardPrecision(expectedTdeeCalories.add(BigDecimal.ONE));

        // Act
        MealPlanningCalculator systemUnderTest = MealPlanningCalculator.instance();
        CalculationResult calculationResult = systemUnderTest.calculatePlanRequirements(femaleCalculationRequest);

        // Assert
        assertThat(calculationResult.getTdeeCalories()).isBetween(expectedMin, expectedMax);
    }

    @Test(dataProvider = "dailyCaloricRequirementsForMaleProvider")
    public void Daily_Caloric_Requirements_For_Male(BasalMetabolicRateFormula bmrFormula, ActivityLevel activityLevel, FitnessGoal fitnessGoal, BigDecimal expectedDailyCalories)
    {
        // Arrange
        MealPlanningCalculatedRegistration maleCalculationRequest = getInitializedMaleCalcRequest();
        maleCalculationRequest.setActivityLevel(activityLevel);
        maleCalculationRequest.setBmrFormula(bmrFormula);
        maleCalculationRequest.setFitnessGoal(fitnessGoal);

        // One calorie above or below expected allowable error
        BigDecimal expectedMin = NumberUtils.withStandardPrecision(expectedDailyCalories.subtract(BigDecimal.ONE));
        BigDecimal expectedMax = NumberUtils.withStandardPrecision(expectedDailyCalories.add(BigDecimal.ONE));

        // Act
        MealPlanningCalculator systemUnderTest = MealPlanningCalculator.instance();
        CalculationResult calculationResult = systemUnderTest.calculatePlanRequirements(maleCalculationRequest);

        // Assert
        assertThat(calculationResult.getNeededCalories()).isBetween(expectedMin, expectedMax);
    }

    @Test(dataProvider = "dailyCaloricRequirementsForFemaleProvider")
    public void Daily_Caloric_Requirements_For_Female(BasalMetabolicRateFormula bmrFormula, ActivityLevel activityLevel, FitnessGoal fitnessGoal, BigDecimal expectedDailyCalories)
    {
        // Arrange
        MealPlanningCalculatedRegistration femaleCalculationRequest = getInitializedFemaleCalcRequest();
        femaleCalculationRequest.setActivityLevel(activityLevel);
        femaleCalculationRequest.setBmrFormula(bmrFormula);
        femaleCalculationRequest.setFitnessGoal(fitnessGoal);

        // One calorie above or below expected allowable error
        BigDecimal expectedMin = NumberUtils.withStandardPrecision(expectedDailyCalories.subtract(BigDecimal.ONE));
        BigDecimal expectedMax = NumberUtils.withStandardPrecision(expectedDailyCalories.add(BigDecimal.ONE));

        // Act
        MealPlanningCalculator systemUnderTest = MealPlanningCalculator.instance();
        CalculationResult calculationResult = systemUnderTest.calculatePlanRequirements(femaleCalculationRequest);

        // Assert
        assertThat(calculationResult.getNeededCalories()).isBetween(expectedMin, expectedMax);
    }


    @Test
    public void Macronutrient_Requirements_For_Plan()
    {
        /* This test doesn't need to be paramterized as we're just doing math on previously
         * validated (the above tests) daily calories.
         *
         * See the last paragraph of the comment above the calc request init functions for
         * additional elaboration on this.
         */

        // Arrange
        MealPlanningCalculatedRegistration calculationRequest = getInitializedMaleCalcRequest();
        calculationRequest.setActivityLevel(ActivityLevel.SEDENTARY);
        calculationRequest.setBmrFormula(BasalMetabolicRateFormula.MIFFLIN_ST_JEOR);
        calculationRequest.setFitnessGoal(FitnessGoal.MAINTAIN);

        // Base values
        BigDecimal gramsOfProtein = new BigDecimal("171.67");
        BigDecimal gramsOfCarbs = new BigDecimal("200.28");
        BigDecimal gramsOfFats = new BigDecimal("89.01");

        // One gram above or below expected allowable error
        BigDecimal minExpectedProtein = gramsOfProtein.subtract(BigDecimal.ONE);
        BigDecimal maxExpectedProtein = gramsOfProtein.add(BigDecimal.ONE);
        BigDecimal minExpectedCarbs = gramsOfCarbs.subtract(BigDecimal.ONE);
        BigDecimal maxExpectedCarbs = gramsOfCarbs.add(BigDecimal.ONE);
        BigDecimal minExpectedFats = gramsOfFats.subtract(BigDecimal.ONE);
        BigDecimal maxExpectedFats = gramsOfFats.add(BigDecimal.ONE);

        // Act
        MealPlanningCalculator systemUnderTest = MealPlanningCalculator.instance();
        CalculationResult calculationResult = systemUnderTest.calculatePlanRequirements(calculationRequest);

        // Assert

        assertThat(calculationResult.getMacronutrientSuggestion(Nutrient.PROTEIN)).isBetween(minExpectedProtein, maxExpectedProtein);
        assertThat(calculationResult.getMacronutrientSuggestion(Nutrient.CARBOHYDRATES)).isBetween(minExpectedCarbs, maxExpectedCarbs);
        assertThat(calculationResult.getMacronutrientSuggestion(Nutrient.TOTAL_FATS)).isBetween(minExpectedFats, maxExpectedFats);
    }

    /*----------------------------------------------------------------------------------------------------------------------------------
     *--
     *--                                            DATA PROVIDERS FOR PARAMETERIZED TESTS
     *--                                                            See
     *--                                https://testng.org/doc/documentation-main.html#parameters-dataproviders
     *--
     *----------------------------------------------------------------------------------------------------------------------------------
     */

    // TDEE calorie tests test all combinations of activity levels and BMR formulas
    // for correct calculation of TDEE (total daily energy expenditure) calories
    @DataProvider(name = "tdeeCaloricRequirementsForMaleProvider")
    public Object[][] tdeeCaloricRequirementsForMaleProvider()
    {
        return new Object[][] {
                {BasalMetabolicRateFormula.KATCH_MCARDLE, ActivityLevel.SEDENTARY, new BigDecimal("2136.73")},
                {BasalMetabolicRateFormula.KATCH_MCARDLE, ActivityLevel.LIGHT, new BigDecimal("2448.34")},
                {BasalMetabolicRateFormula.KATCH_MCARDLE, ActivityLevel.MODERATE, new BigDecimal("2759.95")},
                {BasalMetabolicRateFormula.KATCH_MCARDLE, ActivityLevel.HIGH, new BigDecimal("3071.56")},
                {BasalMetabolicRateFormula.KATCH_MCARDLE, ActivityLevel.EXTREME, new BigDecimal("3383.56")},
                {BasalMetabolicRateFormula.CUNNINGHAM, ActivityLevel.SEDENTARY, new BigDecimal("2324.08")},
                {BasalMetabolicRateFormula.CUNNINGHAM, ActivityLevel.LIGHT, new BigDecimal("2663.01")},
                {BasalMetabolicRateFormula.CUNNINGHAM, ActivityLevel.MODERATE, new BigDecimal("3001.94")},
                {BasalMetabolicRateFormula.CUNNINGHAM, ActivityLevel.HIGH, new BigDecimal("3341.01")},
                {BasalMetabolicRateFormula.CUNNINGHAM, ActivityLevel.EXTREME, new BigDecimal("3679.80")},
                {BasalMetabolicRateFormula.MIFFLIN_ST_JEOR, ActivityLevel.SEDENTARY, new BigDecimal("2288.89")},
                {BasalMetabolicRateFormula.MIFFLIN_ST_JEOR, ActivityLevel.LIGHT, new BigDecimal("2622.68")},
                {BasalMetabolicRateFormula.MIFFLIN_ST_JEOR, ActivityLevel.MODERATE, new BigDecimal("2956.48")},
                {BasalMetabolicRateFormula.MIFFLIN_ST_JEOR, ActivityLevel.HIGH, new BigDecimal("3290.27")},
                {BasalMetabolicRateFormula.MIFFLIN_ST_JEOR, ActivityLevel.EXTREME, new BigDecimal("3624.07")},
                {BasalMetabolicRateFormula.ORIGINAL_HARRIS_BENEDICT, ActivityLevel.SEDENTARY, new BigDecimal("2466.67")},
                {BasalMetabolicRateFormula.ORIGINAL_HARRIS_BENEDICT, ActivityLevel.LIGHT, new BigDecimal("2826.40")},
                {BasalMetabolicRateFormula.ORIGINAL_HARRIS_BENEDICT, ActivityLevel.MODERATE, new BigDecimal("3186.12")},
                {BasalMetabolicRateFormula.ORIGINAL_HARRIS_BENEDICT, ActivityLevel.HIGH, new BigDecimal("3545.84")},
                {BasalMetabolicRateFormula.ORIGINAL_HARRIS_BENEDICT, ActivityLevel.EXTREME, new BigDecimal("3905.57")},
                {BasalMetabolicRateFormula.REVISED_HARRIS_BENEDICT, ActivityLevel.SEDENTARY, new BigDecimal("2437.57")},
                {BasalMetabolicRateFormula.REVISED_HARRIS_BENEDICT, ActivityLevel.LIGHT, new BigDecimal("2793.05")},
                {BasalMetabolicRateFormula.REVISED_HARRIS_BENEDICT, ActivityLevel.MODERATE, new BigDecimal("3148.53")},
                {BasalMetabolicRateFormula.REVISED_HARRIS_BENEDICT, ActivityLevel.HIGH, new BigDecimal("3504.01")},
                {BasalMetabolicRateFormula.REVISED_HARRIS_BENEDICT, ActivityLevel.EXTREME, new BigDecimal("3859.49")}
        };
    }

    @DataProvider(name = "tdeeCaloricRequirementsForFemaleProvider")
    public Object[][] tdeeCaloricRequirementsForFemaleProvider()
    {
        return new Object[][] {
                {BasalMetabolicRateFormula.KATCH_MCARDLE, ActivityLevel.SEDENTARY, new BigDecimal("1650.07")},
                {BasalMetabolicRateFormula.KATCH_MCARDLE, ActivityLevel.LIGHT, new BigDecimal("1890.71")},
                {BasalMetabolicRateFormula.KATCH_MCARDLE, ActivityLevel.MODERATE, new BigDecimal("2131.34")},
                {BasalMetabolicRateFormula.KATCH_MCARDLE, ActivityLevel.HIGH, new BigDecimal("2371.98")},
                {BasalMetabolicRateFormula.KATCH_MCARDLE, ActivityLevel.EXTREME, new BigDecimal("2612.62")},
                {BasalMetabolicRateFormula.CUNNINGHAM, ActivityLevel.SEDENTARY, new BigDecimal("1828.41")},
                {BasalMetabolicRateFormula.CUNNINGHAM, ActivityLevel.LIGHT, new BigDecimal("2095.05")},
                {BasalMetabolicRateFormula.CUNNINGHAM, ActivityLevel.MODERATE, new BigDecimal("2361.69")},
                {BasalMetabolicRateFormula.CUNNINGHAM, ActivityLevel.HIGH, new BigDecimal("2628.34")},
                {BasalMetabolicRateFormula.CUNNINGHAM, ActivityLevel.EXTREME, new BigDecimal("2894.98")},
                {BasalMetabolicRateFormula.MIFFLIN_ST_JEOR, ActivityLevel.SEDENTARY, new BigDecimal("1836.61")},
                {BasalMetabolicRateFormula.MIFFLIN_ST_JEOR, ActivityLevel.LIGHT, new BigDecimal("2104.45")},
                {BasalMetabolicRateFormula.MIFFLIN_ST_JEOR, ActivityLevel.MODERATE, new BigDecimal("2372.29")},
                {BasalMetabolicRateFormula.MIFFLIN_ST_JEOR, ActivityLevel.HIGH, new BigDecimal("2640.13")},
                {BasalMetabolicRateFormula.MIFFLIN_ST_JEOR, ActivityLevel.EXTREME, new BigDecimal("2907.97")},
                {BasalMetabolicRateFormula.ORIGINAL_HARRIS_BENEDICT, ActivityLevel.SEDENTARY, new BigDecimal("1924.69")},
                {BasalMetabolicRateFormula.ORIGINAL_HARRIS_BENEDICT, ActivityLevel.LIGHT, new BigDecimal("2205.37")},
                {BasalMetabolicRateFormula.ORIGINAL_HARRIS_BENEDICT, ActivityLevel.MODERATE, new BigDecimal("2486.05")},
                {BasalMetabolicRateFormula.ORIGINAL_HARRIS_BENEDICT, ActivityLevel.HIGH, new BigDecimal("2766.74")},
                {BasalMetabolicRateFormula.ORIGINAL_HARRIS_BENEDICT, ActivityLevel.EXTREME, new BigDecimal("3047.42")},
                {BasalMetabolicRateFormula.REVISED_HARRIS_BENEDICT, ActivityLevel.SEDENTARY, new BigDecimal("1898.06")},
                {BasalMetabolicRateFormula.REVISED_HARRIS_BENEDICT, ActivityLevel.LIGHT, new BigDecimal("2174.86")},
                {BasalMetabolicRateFormula.REVISED_HARRIS_BENEDICT, ActivityLevel.MODERATE, new BigDecimal("2451.66")},
                {BasalMetabolicRateFormula.REVISED_HARRIS_BENEDICT, ActivityLevel.HIGH, new BigDecimal("2728.46")},
                {BasalMetabolicRateFormula.REVISED_HARRIS_BENEDICT, ActivityLevel.EXTREME, new BigDecimal("3005.26")}
        };
    }

    // Daily calorie tests test all combinations of TDEE calories plus any surplus or deficit due to fitness goal
    @DataProvider(name = "dailyCaloricRequirementsForMaleProvider")
    public Object[][] dailyCaloricRequirementsForMaleProvider()
    {
        return new Object[][] {
                {BasalMetabolicRateFormula.KATCH_MCARDLE, ActivityLevel.SEDENTARY, FitnessGoal.GAIN, new BigDecimal("2636.63")},
                {BasalMetabolicRateFormula.KATCH_MCARDLE, ActivityLevel.SEDENTARY, FitnessGoal.LOSE, new BigDecimal("1709.39")},
                {BasalMetabolicRateFormula.KATCH_MCARDLE, ActivityLevel.LIGHT, FitnessGoal.GAIN, new BigDecimal("2948.34")},
                {BasalMetabolicRateFormula.KATCH_MCARDLE, ActivityLevel.LIGHT, FitnessGoal.LOSE, new BigDecimal("1958.67")},
                {BasalMetabolicRateFormula.KATCH_MCARDLE, ActivityLevel.MODERATE, FitnessGoal.GAIN, new BigDecimal("3260.08")},
                {BasalMetabolicRateFormula.KATCH_MCARDLE, ActivityLevel.MODERATE, FitnessGoal.LOSE, new BigDecimal("2207.96")},
                {BasalMetabolicRateFormula.KATCH_MCARDLE, ActivityLevel.HIGH, FitnessGoal.GAIN, new BigDecimal("3571.56")},
                {BasalMetabolicRateFormula.KATCH_MCARDLE, ActivityLevel.HIGH, FitnessGoal.LOSE, new BigDecimal("2457.24")},
                {BasalMetabolicRateFormula.KATCH_MCARDLE, ActivityLevel.EXTREME, FitnessGoal.GAIN, new BigDecimal("3883.16")},
                {BasalMetabolicRateFormula.KATCH_MCARDLE, ActivityLevel.EXTREME, FitnessGoal.LOSE, new BigDecimal("2706.53")},
                {BasalMetabolicRateFormula.CUNNINGHAM, ActivityLevel.SEDENTARY, FitnessGoal.GAIN, new BigDecimal("2824.08")},
                {BasalMetabolicRateFormula.CUNNINGHAM, ActivityLevel.SEDENTARY, FitnessGoal.LOSE, new BigDecimal("1859.27")},
                {BasalMetabolicRateFormula.CUNNINGHAM, ActivityLevel.LIGHT, FitnessGoal.GAIN, new BigDecimal("3163.01")},
                {BasalMetabolicRateFormula.CUNNINGHAM, ActivityLevel.LIGHT, FitnessGoal.LOSE, new BigDecimal("2130.41")},
                {BasalMetabolicRateFormula.CUNNINGHAM, ActivityLevel.MODERATE, FitnessGoal.GAIN, new BigDecimal("3501.94")},
                {BasalMetabolicRateFormula.CUNNINGHAM, ActivityLevel.MODERATE, FitnessGoal.LOSE, new BigDecimal("2401.55")},
                {BasalMetabolicRateFormula.CUNNINGHAM, ActivityLevel.HIGH, FitnessGoal.GAIN, new BigDecimal("3840.87")},
                {BasalMetabolicRateFormula.CUNNINGHAM, ActivityLevel.HIGH, FitnessGoal.LOSE, new BigDecimal("2672.69")},
                {BasalMetabolicRateFormula.CUNNINGHAM, ActivityLevel.EXTREME, FitnessGoal.GAIN, new BigDecimal("4179.80")},
                {BasalMetabolicRateFormula.CUNNINGHAM, ActivityLevel.EXTREME, FitnessGoal.LOSE, new BigDecimal("2943.84")},
                {BasalMetabolicRateFormula.MIFFLIN_ST_JEOR, ActivityLevel.SEDENTARY, FitnessGoal.GAIN, new BigDecimal("2788.89")},
                {BasalMetabolicRateFormula.MIFFLIN_ST_JEOR, ActivityLevel.SEDENTARY, FitnessGoal.LOSE, new BigDecimal("1831.11")},
                {BasalMetabolicRateFormula.MIFFLIN_ST_JEOR, ActivityLevel.LIGHT, FitnessGoal.GAIN, new BigDecimal("3122.68")},
                {BasalMetabolicRateFormula.MIFFLIN_ST_JEOR, ActivityLevel.LIGHT, FitnessGoal.LOSE, new BigDecimal("2098.14")},
                {BasalMetabolicRateFormula.MIFFLIN_ST_JEOR, ActivityLevel.MODERATE, FitnessGoal.GAIN, new BigDecimal("3456.48")},
                {BasalMetabolicRateFormula.MIFFLIN_ST_JEOR, ActivityLevel.MODERATE, FitnessGoal.LOSE, new BigDecimal("2365.18")},
                {BasalMetabolicRateFormula.MIFFLIN_ST_JEOR, ActivityLevel.HIGH, FitnessGoal.GAIN, new BigDecimal("3790.27")},
                {BasalMetabolicRateFormula.MIFFLIN_ST_JEOR, ActivityLevel.HIGH, FitnessGoal.LOSE, new BigDecimal("2632.32")},
                {BasalMetabolicRateFormula.MIFFLIN_ST_JEOR, ActivityLevel.EXTREME, FitnessGoal.GAIN, new BigDecimal("4124.07")},
                {BasalMetabolicRateFormula.MIFFLIN_ST_JEOR, ActivityLevel.EXTREME, FitnessGoal.LOSE, new BigDecimal("2899.25")},
                {BasalMetabolicRateFormula.ORIGINAL_HARRIS_BENEDICT, ActivityLevel.SEDENTARY, FitnessGoal.GAIN, new BigDecimal("2966.67")},
                {BasalMetabolicRateFormula.ORIGINAL_HARRIS_BENEDICT, ActivityLevel.SEDENTARY, FitnessGoal.LOSE, new BigDecimal("1973.34")},
                {BasalMetabolicRateFormula.ORIGINAL_HARRIS_BENEDICT, ActivityLevel.LIGHT, FitnessGoal.GAIN, new BigDecimal("3326.40")},
                {BasalMetabolicRateFormula.ORIGINAL_HARRIS_BENEDICT, ActivityLevel.LIGHT, FitnessGoal.LOSE, new BigDecimal("2261.12")},
                {BasalMetabolicRateFormula.ORIGINAL_HARRIS_BENEDICT, ActivityLevel.MODERATE, FitnessGoal.GAIN, new BigDecimal("3686.12")},
                {BasalMetabolicRateFormula.ORIGINAL_HARRIS_BENEDICT, ActivityLevel.MODERATE, FitnessGoal.LOSE, new BigDecimal("2548.90")},
                {BasalMetabolicRateFormula.ORIGINAL_HARRIS_BENEDICT, ActivityLevel.HIGH, FitnessGoal.GAIN, new BigDecimal("4045.84")},
                {BasalMetabolicRateFormula.ORIGINAL_HARRIS_BENEDICT, ActivityLevel.HIGH, FitnessGoal.LOSE, new BigDecimal("2836.68")},
                {BasalMetabolicRateFormula.ORIGINAL_HARRIS_BENEDICT, ActivityLevel.EXTREME, FitnessGoal.GAIN, new BigDecimal("4405.57")},
                {BasalMetabolicRateFormula.ORIGINAL_HARRIS_BENEDICT, ActivityLevel.EXTREME, FitnessGoal.LOSE, new BigDecimal("3124.45")},
                {BasalMetabolicRateFormula.REVISED_HARRIS_BENEDICT, ActivityLevel.SEDENTARY, FitnessGoal.GAIN, new BigDecimal("2937.57")},
                {BasalMetabolicRateFormula.REVISED_HARRIS_BENEDICT, ActivityLevel.SEDENTARY, FitnessGoal.LOSE, new BigDecimal("1950.06")},
                {BasalMetabolicRateFormula.REVISED_HARRIS_BENEDICT, ActivityLevel.LIGHT, FitnessGoal.GAIN, new BigDecimal("3293.05")},
                {BasalMetabolicRateFormula.REVISED_HARRIS_BENEDICT, ActivityLevel.LIGHT, FitnessGoal.LOSE, new BigDecimal("2234.44")},
                {BasalMetabolicRateFormula.REVISED_HARRIS_BENEDICT, ActivityLevel.MODERATE, FitnessGoal.GAIN, new BigDecimal("3648.53")},
                {BasalMetabolicRateFormula.REVISED_HARRIS_BENEDICT, ActivityLevel.MODERATE, FitnessGoal.LOSE, new BigDecimal("2518.83")},
                {BasalMetabolicRateFormula.REVISED_HARRIS_BENEDICT, ActivityLevel.HIGH, FitnessGoal.GAIN, new BigDecimal("4004.01")},
                {BasalMetabolicRateFormula.REVISED_HARRIS_BENEDICT, ActivityLevel.HIGH, FitnessGoal.LOSE, new BigDecimal("2803.21")},
                {BasalMetabolicRateFormula.REVISED_HARRIS_BENEDICT, ActivityLevel.EXTREME, FitnessGoal.GAIN, new BigDecimal("4359.49")},
                {BasalMetabolicRateFormula.REVISED_HARRIS_BENEDICT, ActivityLevel.EXTREME, FitnessGoal.LOSE, new BigDecimal("3087.59")}
        };
    }

    @DataProvider(name = "dailyCaloricRequirementsForFemaleProvider")
    public Object[][] dailyCaloricRequirementsForFemaleProvider()
    {
        return new Object[][] {
                {BasalMetabolicRateFormula.KATCH_MCARDLE, ActivityLevel.SEDENTARY, FitnessGoal.GAIN, new BigDecimal("2150.07")},
                {BasalMetabolicRateFormula.KATCH_MCARDLE, ActivityLevel.SEDENTARY, FitnessGoal.LOSE, new BigDecimal("1320.06")},
                {BasalMetabolicRateFormula.KATCH_MCARDLE, ActivityLevel.LIGHT, FitnessGoal.GAIN, new BigDecimal("2390.71")},
                {BasalMetabolicRateFormula.KATCH_MCARDLE, ActivityLevel.LIGHT, FitnessGoal.LOSE, new BigDecimal("1512.57")},
                {BasalMetabolicRateFormula.KATCH_MCARDLE, ActivityLevel.MODERATE, FitnessGoal.GAIN, new BigDecimal("2631.34")},
                {BasalMetabolicRateFormula.KATCH_MCARDLE, ActivityLevel.MODERATE, FitnessGoal.LOSE, new BigDecimal("1705.08")},
                {BasalMetabolicRateFormula.KATCH_MCARDLE, ActivityLevel.HIGH, FitnessGoal.GAIN, new BigDecimal("2871.98")},
                {BasalMetabolicRateFormula.KATCH_MCARDLE, ActivityLevel.HIGH, FitnessGoal.LOSE, new BigDecimal("1897.58")},
                {BasalMetabolicRateFormula.KATCH_MCARDLE, ActivityLevel.EXTREME, FitnessGoal.GAIN, new BigDecimal("3112.62")},
                {BasalMetabolicRateFormula.KATCH_MCARDLE, ActivityLevel.EXTREME, FitnessGoal.LOSE, new BigDecimal("2090.09")},
                {BasalMetabolicRateFormula.CUNNINGHAM, ActivityLevel.SEDENTARY, FitnessGoal.GAIN, new BigDecimal("2328.41")},
                {BasalMetabolicRateFormula.CUNNINGHAM, ActivityLevel.SEDENTARY, FitnessGoal.LOSE, new BigDecimal("1462.73")},
                {BasalMetabolicRateFormula.CUNNINGHAM, ActivityLevel.LIGHT, FitnessGoal.GAIN, new BigDecimal("2595.05")},
                {BasalMetabolicRateFormula.CUNNINGHAM, ActivityLevel.LIGHT, FitnessGoal.LOSE, new BigDecimal("1676.04")},
                {BasalMetabolicRateFormula.CUNNINGHAM, ActivityLevel.MODERATE, FitnessGoal.GAIN, new BigDecimal("2861.69")},
                {BasalMetabolicRateFormula.CUNNINGHAM, ActivityLevel.MODERATE, FitnessGoal.LOSE, new BigDecimal("1889.36")},
                {BasalMetabolicRateFormula.CUNNINGHAM, ActivityLevel.HIGH, FitnessGoal.GAIN, new BigDecimal("3128.34")},
                {BasalMetabolicRateFormula.CUNNINGHAM, ActivityLevel.HIGH, FitnessGoal.LOSE, new BigDecimal("2102.67")},
                {BasalMetabolicRateFormula.CUNNINGHAM, ActivityLevel.EXTREME, FitnessGoal.GAIN, new BigDecimal("3394.98")},
                {BasalMetabolicRateFormula.CUNNINGHAM, ActivityLevel.EXTREME, FitnessGoal.LOSE, new BigDecimal("2315.98")},
                {BasalMetabolicRateFormula.MIFFLIN_ST_JEOR, ActivityLevel.SEDENTARY, FitnessGoal.GAIN, new BigDecimal("2336.61")},
                {BasalMetabolicRateFormula.MIFFLIN_ST_JEOR, ActivityLevel.SEDENTARY, FitnessGoal.LOSE, new BigDecimal("1469.29")},
                {BasalMetabolicRateFormula.MIFFLIN_ST_JEOR, ActivityLevel.LIGHT, FitnessGoal.GAIN, new BigDecimal("2604.45")},
                {BasalMetabolicRateFormula.MIFFLIN_ST_JEOR, ActivityLevel.LIGHT, FitnessGoal.LOSE, new BigDecimal("1683.56")},
                {BasalMetabolicRateFormula.MIFFLIN_ST_JEOR, ActivityLevel.MODERATE, FitnessGoal.GAIN, new BigDecimal("2872.29")},
                {BasalMetabolicRateFormula.MIFFLIN_ST_JEOR, ActivityLevel.MODERATE, FitnessGoal.LOSE, new BigDecimal("1897.83")},
                {BasalMetabolicRateFormula.MIFFLIN_ST_JEOR, ActivityLevel.HIGH, FitnessGoal.GAIN, new BigDecimal("3140.13")},
                {BasalMetabolicRateFormula.MIFFLIN_ST_JEOR, ActivityLevel.HIGH, FitnessGoal.LOSE, new BigDecimal("2112.10")},
                {BasalMetabolicRateFormula.MIFFLIN_ST_JEOR, ActivityLevel.EXTREME, FitnessGoal.GAIN, new BigDecimal("3407.97")},
                {BasalMetabolicRateFormula.MIFFLIN_ST_JEOR, ActivityLevel.EXTREME, FitnessGoal.LOSE, new BigDecimal("2326.38")},
                {BasalMetabolicRateFormula.ORIGINAL_HARRIS_BENEDICT, ActivityLevel.SEDENTARY, FitnessGoal.GAIN, new BigDecimal("2424.69")},
                {BasalMetabolicRateFormula.ORIGINAL_HARRIS_BENEDICT, ActivityLevel.SEDENTARY, FitnessGoal.LOSE, new BigDecimal("1539.75")},
                {BasalMetabolicRateFormula.ORIGINAL_HARRIS_BENEDICT, ActivityLevel.LIGHT, FitnessGoal.GAIN, new BigDecimal("2705.37")},
                {BasalMetabolicRateFormula.ORIGINAL_HARRIS_BENEDICT, ActivityLevel.LIGHT, FitnessGoal.LOSE, new BigDecimal("1764.30")},
                {BasalMetabolicRateFormula.ORIGINAL_HARRIS_BENEDICT, ActivityLevel.MODERATE, FitnessGoal.GAIN, new BigDecimal("2986.05")},
                {BasalMetabolicRateFormula.ORIGINAL_HARRIS_BENEDICT, ActivityLevel.MODERATE, FitnessGoal.LOSE, new BigDecimal("1988.84")},
                {BasalMetabolicRateFormula.ORIGINAL_HARRIS_BENEDICT, ActivityLevel.HIGH, FitnessGoal.GAIN, new BigDecimal("3266.74")},
                {BasalMetabolicRateFormula.ORIGINAL_HARRIS_BENEDICT, ActivityLevel.HIGH, FitnessGoal.LOSE, new BigDecimal("2213.39")},
                {BasalMetabolicRateFormula.ORIGINAL_HARRIS_BENEDICT, ActivityLevel.EXTREME, FitnessGoal.GAIN, new BigDecimal("3547.42")},
                {BasalMetabolicRateFormula.ORIGINAL_HARRIS_BENEDICT, ActivityLevel.EXTREME, FitnessGoal.LOSE, new BigDecimal("2437.94")},
                {BasalMetabolicRateFormula.REVISED_HARRIS_BENEDICT, ActivityLevel.SEDENTARY, FitnessGoal.GAIN, new BigDecimal("2398.06")},
                {BasalMetabolicRateFormula.REVISED_HARRIS_BENEDICT, ActivityLevel.SEDENTARY, FitnessGoal.LOSE, new BigDecimal("1518.45")},
                {BasalMetabolicRateFormula.REVISED_HARRIS_BENEDICT, ActivityLevel.LIGHT, FitnessGoal.GAIN, new BigDecimal("2674.86")},
                {BasalMetabolicRateFormula.REVISED_HARRIS_BENEDICT, ActivityLevel.LIGHT, FitnessGoal.LOSE, new BigDecimal("1739.89")},
                {BasalMetabolicRateFormula.REVISED_HARRIS_BENEDICT, ActivityLevel.MODERATE, FitnessGoal.GAIN, new BigDecimal("2951.66")},
                {BasalMetabolicRateFormula.REVISED_HARRIS_BENEDICT, ActivityLevel.MODERATE, FitnessGoal.LOSE, new BigDecimal("1961.33")},
                {BasalMetabolicRateFormula.REVISED_HARRIS_BENEDICT, ActivityLevel.HIGH, FitnessGoal.GAIN, new BigDecimal("3228.46")},
                {BasalMetabolicRateFormula.REVISED_HARRIS_BENEDICT, ActivityLevel.HIGH, FitnessGoal.LOSE, new BigDecimal("2182.77")},
                {BasalMetabolicRateFormula.REVISED_HARRIS_BENEDICT, ActivityLevel.EXTREME, FitnessGoal.GAIN, new BigDecimal("3505.26")},
                {BasalMetabolicRateFormula.REVISED_HARRIS_BENEDICT, ActivityLevel.EXTREME, FitnessGoal.LOSE, new BigDecimal("2404.21")}
        };
    }

   /*
    * The below age, height, and weight stats are based on the average American male
    * and female according to the CDC. The body fat percentages are taken from the
    * National Center for Biotechnology Information.
    *
    * The particular figures here aren't actually very important. What is actually
    * important for these tests is the results of the calculations they perform. Trying
    * to use average American data seemed as good as any for the purposes of the test
    * and provided some simple defaults for the inputs to the calculations that don't
    * change from test to test.
    *
    * For more information on the chosen constant data see the below links:
    * https://www.cdc.gov/nchs/fastats/body-measurements.htm
    * https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3837418/
    *
    * It is also worth mentioning that the macronutrient percentages here don't matter
    * and can be the same regardless of goal or sex. In real use specific macro ranges
    * are enforced based upon fitness goal. But since we're just verifying that we calculate
    * the correct amount of each macro based on the desired percentage of total daily
    * calories for that macro the actual desired percentage values specified for each
    * macro don't matter.
    */
    private MealPlanningCalculatedRegistration getInitializedMaleCalcRequest()
    {
        MealPlanningCalculatedRegistration calculationRequest = new MealPlanningCalculatedRegistration();
        calculationRequest.setAge(20);
        calculationRequest.setHeight(BigDecimal.valueOf(175.26));
        calculationRequest.setWeight(BigDecimal.valueOf(90.70));
        calculationRequest.setBodyFactPercentage(BigDecimal.valueOf(28.0));
        calculationRequest.setSex(Sex.MALE);
        calculationRequest.setProteinPercent(BigDecimal.valueOf(30));
        calculationRequest.setFatsPercent(BigDecimal.valueOf(35));
        calculationRequest.setCarbsPercent(BigDecimal.valueOf(35));
        return calculationRequest;
    }

    private MealPlanningCalculatedRegistration getInitializedFemaleCalcRequest()
    {
        MealPlanningCalculatedRegistration calculationRequest = new MealPlanningCalculatedRegistration();
        calculationRequest.setAge(20);
        calculationRequest.setHeight(BigDecimal.valueOf(162.56));
        calculationRequest.setWeight(BigDecimal.valueOf(77.55));
        calculationRequest.setBodyFactPercentage(BigDecimal.valueOf(40.0));
        calculationRequest.setSex(Sex.FEMALE);
        calculationRequest.setProteinPercent(BigDecimal.valueOf(30));
        calculationRequest.setFatsPercent(BigDecimal.valueOf(35));
        calculationRequest.setCarbsPercent(BigDecimal.valueOf(35));
        return calculationRequest;
    }
}
