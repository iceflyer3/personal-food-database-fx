/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


package com.iceflyer3.pfd.test.data.task.mealplanning.carbcycling;

import com.iceflyer3.pfd.test.data.config.FxTaskTest;
import com.iceflyer3.pfd.test.data.config.TestDbConstants;
import com.iceflyer3.pfd.enums.MealType;
import com.iceflyer3.pfd.data.mapping.mealplanning.carbcycling.CarbCyclingDayMapper;
import com.iceflyer3.pfd.data.mapping.mealplanning.carbcycling.CarbCyclingMealMapper;
import com.iceflyer3.pfd.data.repository.MealPlanningRepository;
import com.iceflyer3.pfd.test.data.config.FxRuntimeBootstrappedBase;
import com.iceflyer3.pfd.data.task.mealplanning.carbcycling.meal.GetCarbCyclingMealByIdTask;
import com.iceflyer3.pfd.data.task.mealplanning.carbcycling.meal.GetCarbCyclingMealsForDayTask;
import com.iceflyer3.pfd.data.task.mealplanning.carbcycling.meal.SaveCarbCyclingMealTask;
import com.iceflyer3.pfd.exception.InvalidApplicationStateException;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingDayViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingMealViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static org.assertj.core.api.Assertions.assertThat;

@FxTaskTest
public class CarbCyclingMealTasksTests extends FxRuntimeBootstrappedBase
{
    @Autowired
    private MealPlanningRepository mealPlanningRepository;

    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_PLAN,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_TRAINING_DAY,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_MEAL_FOR_TRAINING_DAY
    })
    public void Find_All_Meals_For_Day()
    {
        // Arrange
        UUID dayId = UUID.fromString(TestDbConstants.CARB_CYCLING_PLAN_TRAINING_DAY_ID);
        GetCarbCyclingMealsForDayTask sut = new GetCarbCyclingMealsForDayTask(mealPlanningRepository, dayId);
        List<CarbCyclingMealViewModel> retrievedDays = new ArrayList<>();

        // Act
        try
        {
            sut.run();
            retrievedDays = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(retrievedDays.size()).isEqualTo(1);
    }

    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_PLAN,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_TRAINING_DAY,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_MEAL_FOR_TRAINING_DAY
    })
    public void Find_Meal_By_Id()
    {
        // Arrange
        UUID mealId = UUID.fromString(TestDbConstants.CARB_CYCLING_PLAN_TRAINING_DAY_MEAL_ID);
        GetCarbCyclingMealByIdTask sut = new GetCarbCyclingMealByIdTask(mealPlanningRepository, mealId);
        CarbCyclingMealViewModel retrievedMeal = null;

        // Act
        try
        {
            sut.run();
            retrievedMeal = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(retrievedMeal).isNotNull();
        assertThat(retrievedMeal.getId()).isEqualByComparingTo(mealId);
    }

    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_PLAN,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_NON_TRAINING_DAY
    })
    public void Create_New_Meal_For_Non_Training_Day()
    {
        // Arrange
        UUID dayId = UUID.fromString(TestDbConstants.CARB_CYCLING_PLAN_NON_TRAINING_DAY_ID);
        CarbCyclingDayViewModel dayViewModel = CarbCyclingDayMapper.toModel(mealPlanningRepository.getCarbCyclingDayForId(dayId), NutritionalSummary.empty());

        CarbCyclingMealViewModel newMealViewModel = new CarbCyclingMealViewModel(null, NutritionalSummary.empty());
        newMealViewModel.setName("Test New Carb Cycling Non-Training Day Meal");
        newMealViewModel.setMealType(MealType.REGULAR);
        newMealViewModel.setMealNumber(1);
        newMealViewModel.setCarbConsumptionPercentage(BigDecimal.TEN);

        SaveCarbCyclingMealTask sut = new SaveCarbCyclingMealTask(mealPlanningRepository, dayViewModel, newMealViewModel);
        CarbCyclingMealViewModel savedMeal = null;

        // Act
        try
        {
            sut.run();
            savedMeal = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(savedMeal).isNotNull();
        assertThat(savedMeal.getId()).isNotNull();
    }

    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_PLAN,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_TRAINING_DAY
    })
    public void Create_New_Post_Workout_Meal_For_Training_Day()
    {
        // Arrange
        UUID dayId = UUID.fromString(TestDbConstants.CARB_CYCLING_PLAN_TRAINING_DAY_ID);
        CarbCyclingDayViewModel dayViewModel = CarbCyclingDayMapper.toModel(mealPlanningRepository.getCarbCyclingDayForId(dayId), NutritionalSummary.empty());

        CarbCyclingMealViewModel newMealViewModel = new CarbCyclingMealViewModel(null, NutritionalSummary.empty());
        newMealViewModel.setName("Test New Carb Cycling Post-Workout Meal");
        newMealViewModel.setMealType(MealType.POST_WORKOUT);
        newMealViewModel.setMealNumber(1);
        newMealViewModel.setCarbConsumptionPercentage(BigDecimal.TEN);

        SaveCarbCyclingMealTask sut = new SaveCarbCyclingMealTask(mealPlanningRepository, dayViewModel, newMealViewModel);
        CarbCyclingMealViewModel savedMeal = null;

        // Act
        try
        {
            sut.run();
            savedMeal = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(savedMeal).isNotNull();
        assertThat(savedMeal.getId()).isNotNull();
    }

    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_PLAN,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_TRAINING_DAY
    })
    public void Post_Workout_Meal_For_Training_Day_Must_Be_Created_Before_Other_Meals()
    {
        // Arrange
        UUID dayId = UUID.fromString(TestDbConstants.CARB_CYCLING_PLAN_TRAINING_DAY_ID);
        CarbCyclingDayViewModel dayViewModel = CarbCyclingDayMapper.toModel(mealPlanningRepository.getCarbCyclingDayForId(dayId), NutritionalSummary.empty());

        CarbCyclingMealViewModel newMealViewModel = new CarbCyclingMealViewModel(null, NutritionalSummary.empty());
        newMealViewModel.setName("Test New Carb Cycling Post-Workout Meal");
        newMealViewModel.setMealType(MealType.REGULAR);
        newMealViewModel.setMealNumber(1);
        newMealViewModel.setCarbConsumptionPercentage(BigDecimal.TEN);

        SaveCarbCyclingMealTask sut = new SaveCarbCyclingMealTask(mealPlanningRepository, dayViewModel, newMealViewModel);

        // Act
        try
        {
            sut.run();
            sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            // Assert
            assertThat(e.getCause()).isInstanceOf(InvalidApplicationStateException.class);
        }
    }

    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_PLAN,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_TRAINING_DAY,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_MEAL_FOR_TRAINING_DAY
    })
    public void Update_Existing_Meal()
    {
        // Arrange
        UUID mealId = UUID.fromString(TestDbConstants.CARB_CYCLING_PLAN_TRAINING_DAY_MEAL_ID);
        CarbCyclingMealViewModel existingMeal = CarbCyclingMealMapper.toModel(mealPlanningRepository.getCarbCyclingMealById(mealId), NutritionalSummary.empty());

        UUID dayId = UUID.fromString(TestDbConstants.CARB_CYCLING_PLAN_TRAINING_DAY_ID);
        CarbCyclingDayViewModel dayViewModel = CarbCyclingDayMapper.toModel(mealPlanningRepository.getCarbCyclingDayForId(dayId), NutritionalSummary.empty());

        String newMealName = "Updated Carb Cycling Plan Meal";
        existingMeal.setName(newMealName);

        SaveCarbCyclingMealTask sut = new SaveCarbCyclingMealTask(mealPlanningRepository, dayViewModel, existingMeal);
        CarbCyclingMealViewModel updatedMeal = null;

        // Act
        try
        {
            sut.run();
            updatedMeal = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(updatedMeal).isNotNull();
        assertThat(updatedMeal.getId()).isEqualByComparingTo(mealId);
        assertThat(updatedMeal.getName()).isEqualTo(newMealName);
    }

    // Ingredient and meal delete tests are covered by the same tasks as
    // those tested in MealPlanMealTasksTests
}
