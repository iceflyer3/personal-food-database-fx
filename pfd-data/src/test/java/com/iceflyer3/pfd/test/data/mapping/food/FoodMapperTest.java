package com.iceflyer3.pfd.test.data.mapping.food;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.test.data.TestDataProvider;
import com.iceflyer3.pfd.data.entities.food.ComplexFood;
import com.iceflyer3.pfd.data.entities.food.SimpleFood;
import com.iceflyer3.pfd.data.mapping.IngredientMapper;
import com.iceflyer3.pfd.data.mapping.food.FoodMapper;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.ComplexFoodViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.ReadOnlyFoodViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.SimpleFoodViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class FoodMapperTest
{
    TestDataProvider testDataProvider = new TestDataProvider();

    /*
     * A general note on these tests:
     *
     * The created and last modified dates are chained down to LocalDateTime.now()
     * which makes them hard to test.
     *
     * This might be a point of refactoring in the future. But they aren't really
     * actually important at all. What is infinitely more important is the rest of
     * the actual real food data (like calories and such). So for now we just skip
     * those date fields when making the comparison.
     *
     * Serving sizes for the food are tested independently of the mapping of the
     * foods in their own unit tests as they have their own mapper.
     */

    @Test
    public void Map_Simple_Food_To_Immutable_Model()
    {
        // Arrange
        SimpleFood simpleFood = testDataProvider.getTestSimpleFoodEntity(1);

        // Act
        ReadOnlyFoodViewModel mappedModel = FoodMapper.toModel(simpleFood);

        // Assert
        assertThat(simpleFood)
                .usingRecursiveComparison()
                .ignoringFields("createdDate", "lastModifiedDate", "foodMetaData")
                .withEqualsForFields(testDataProvider.getEntityToViewModelUserComparisonPredicate(), "createdUser")
                .withEqualsForFields(testDataProvider.getEntityToViewModelUserComparisonPredicate(), "lastModifiedUser")
                .isEqualTo(mappedModel);

        assertThat(simpleFood.getFoodMetaData().getServingSizes().size())
                .isEqualTo(mappedModel.getServingSizes().size());
    }

    @Test
    public void Map_Simple_Food_To_Mutable_Model()
    {
        // Arrange
        SimpleFood simpleFood = testDataProvider.getTestSimpleFoodEntity(1);

        // Act
        SimpleFoodViewModel mappedModel = FoodMapper.toMutableModel(simpleFood);

        // Assert
        assertThat(simpleFood)
                .usingRecursiveComparison()
                .ignoringFields("createdDate", "lastModifiedDate", "foodMetaData")
                .withEqualsForFields(testDataProvider.getEntityToViewModelUserComparisonPredicate(), "createdUser")
                .withEqualsForFields(testDataProvider.getEntityToViewModelUserComparisonPredicate(), "lastModifiedUser")
                .isEqualTo(mappedModel);

        assertThat(simpleFood.getFoodMetaData().getServingSizes().size())
                .isEqualTo(mappedModel.getServingSizes().size());
    }

    @Test
    public void Map_Complex_Food_To_Immutable_Model()
    {
        // Arrange
        ComplexFood complexFood = testDataProvider.getTestComplexFoodEntity();
        List<IngredientModel> ingredientViewModels = complexFood.getIngredients().stream().map(IngredientMapper::toModel).toList();
        NutritionalSummary nutritionalSummary = NutritionalSummary.deriveUpdateFrom(ingredientViewModels);

        // Act
        ReadOnlyFoodViewModel mappedModel = FoodMapper.toModel(complexFood, nutritionalSummary);

        // Assert
        // When mapping a complex food to an immutable model the ingredients are flattened into
        // the macros and micros for the food. So we ignore them here too.
        assertThat(complexFood)
                .usingRecursiveComparison()
                .ignoringFields("createdDate", "lastModifiedDate", "foodMetaData", "ingredients")
                .withEqualsForFields(testDataProvider.getEntityToViewModelUserComparisonPredicate(), "createdUser")
                .withEqualsForFields(testDataProvider.getEntityToViewModelUserComparisonPredicate(), "lastModifiedUser")
                .isEqualTo(mappedModel);

        assertThat(complexFood.getFoodMetaData().getServingSizes().size())
                .isEqualTo(mappedModel.getServingSizes().size());
    }

    @Test
    public void Map_Complex_Food_To_Mutable_Model()
    {
        // Arrange
        ComplexFood complexFood = testDataProvider.getTestComplexFoodEntity();

        // Act
        ComplexFoodViewModel mappedModel = FoodMapper.toMutableModel(complexFood, NutritionalSummary.empty());

        // Assert
        // Ingredients aren't necessarily loaded when the complex food is so they aren't mapped by the FoodMapper. Hence we don't need to check them.
        assertThat(complexFood)
                .usingRecursiveComparison()
                .ignoringFields("createdDate", "lastModifiedDate", "foodMetaData", "ingredients")
                .withEqualsForFields(testDataProvider.getEntityToViewModelUserComparisonPredicate(), "createdUser")
                .withEqualsForFields(testDataProvider.getEntityToViewModelUserComparisonPredicate(), "lastModifiedUser")
                .isEqualTo(mappedModel);
    }
}
