/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.test.data.task;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.iceflyer3.pfd.data.entities.food.SimpleFood;
import com.iceflyer3.pfd.data.entities.mealplanning.MealPlan;
import com.iceflyer3.pfd.data.entities.recipe.Recipe;
import com.iceflyer3.pfd.data.json.JsonFoodData;
import com.iceflyer3.pfd.data.json.JsonMealPlanningData;
import com.iceflyer3.pfd.data.json.JsonRecipeData;
import com.iceflyer3.pfd.data.json.food.JsonSimpleFood;
import com.iceflyer3.pfd.data.json.recipe.JsonRecipe;
import com.iceflyer3.pfd.data.repository.FoodRepository;
import com.iceflyer3.pfd.data.repository.MealPlanningRepository;
import com.iceflyer3.pfd.data.repository.RecipeRepository;
import com.iceflyer3.pfd.data.request.backup.ExportDataRequest;
import com.iceflyer3.pfd.data.task.data.ExportDataTask;
import com.iceflyer3.pfd.data.task.data.ImportDataTask;
import com.iceflyer3.pfd.test.data.TestDataProvider;
import com.iceflyer3.pfd.test.data.config.FxRuntimeBootstrappedBase;
import com.iceflyer3.pfd.test.data.config.FxTaskTest;
import com.iceflyer3.pfd.test.data.config.TestDbConstants;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static org.assertj.core.api.Assertions.assertThat;

@FxTaskTest
public class ImportExportDataTasksTests extends FxRuntimeBootstrappedBase
{
    /*
     * Not all queries here are executed by Hibernate as we leverage some built-in Spring functionality.
     *
     * As such we have to manually flush the changes when Hibernate isn't being used. The FlushMode
     * for Hibernate doesn't matter if you aren't actually using Hibernate to execute the queries.
     */
    @PersistenceContext
    private EntityManager entityManager;

    private final TestDataProvider testDataProvider;

    @Autowired
    private FoodRepository foodRepository;

    @Autowired
    private MealPlanningRepository mealPlanningRepository;

    @Autowired
    private RecipeRepository recipeRepository;

    private String dateString;

    private ObjectMapper jacksonMapper = new ObjectMapper();

    public ImportExportDataTasksTests()
    {
        this.testDataProvider = new TestDataProvider();
    }

    @BeforeClass
    private void init()
    {
        LocalDateTime today = LocalDateTime.now();
        dateString = String.format("%s-%s-%s", today.getYear(), today.getMonthValue(), today.getDayOfMonth());
        jacksonMapper = new ObjectMapper().registerModule(new JavaTimeModule());
    }

    private String getExportFilePath(String fileName)
    {
        return String.format("../data-export/%s", fileName);
    }

    /**
     * Tests that data is extracted from the database and arranged appropriately in the data export object
     * when simple food but no complex food data exists in the database.
     */
    @Test
    @Sql(TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS)
    public void Export_Simple_Food_Data_To_Json()
    {
        JsonFoodData sut = foodRepository.exportData();

        assertThat(sut.getFoodMetaData().size()).isEqualTo(3);
        assertThat(sut.getFoodServingSizes().size()).isEqualTo(6);
        assertThat(sut.getSimpleFoods().size()).isEqualTo(3);

        assertThat(sut.getComplexFoods().size()).isEqualTo(0);
        assertThat(sut.getComplexFoodIngredients().size()).isEqualTo(0);
    }

    /**
     * Tests that the ExportDataTask can export simple food data from the database to json
     * and then write that data to a file
     */
    @Test
    @Sql(TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS)
    public void Export_Simple_Food_Json_To_File()
    {
        ExportDataRequest exportRequest = new ExportDataRequest();
        exportRequest.setIncludeFoods(true);
        ExportDataTask sut = new ExportDataTask(foodRepository, mealPlanningRepository, recipeRepository, exportRequest);

        try
        {
            sut.run();
            sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            Assert.fail("The task has failed during execution.", e);
        }

        String filename = String.format("foods_%s.json", dateString);
        File exportedDataFile = new File(getExportFilePath(filename));
        assertThat(exportedDataFile.exists()).isTrue();
        assertThat(exportedDataFile.length()).isGreaterThan(0);
    }

    /**
     * Tests that a simple food that does not already exist in the database, upon import, is created.
     */
    @Test
    public void Import_Creates_New_Simple_Food() throws IOException
    {
        // Arrange
        JsonFoodData jsonFoodData = new JsonFoodData(1);
        jsonFoodData.setFoodMetaData(List.of(testDataProvider.getTestFoodMetaDataJson(1)));
        jsonFoodData.setFoodServingSizes(List.of(testDataProvider.getTestFoodServingSizeJson(1)));
        jsonFoodData.setSimpleFoods(List.of(testDataProvider.getTestSimpleFoodJson(1)));

        String filename = String.format("foods_%s.json", dateString);
        String filePath = getExportFilePath(filename);
        jacksonMapper.writeValue(new File(filePath), jsonFoodData);

        // Act
        ArrayList<String> filenamesToImport = new ArrayList<>();
        filenamesToImport.add(filename);
        ImportDataTask sut = new ImportDataTask(foodRepository, mealPlanningRepository, recipeRepository, filenamesToImport);
        try
        {
            sut.run();
            sut.get();
            entityManager.flush();
        }
        catch(InterruptedException | ExecutionException e)
        {
            Assert.fail("The task has failed during execution.", e);
        }

        // Assert
        assertThat(countRowsInTable(TestDbConstants.TABLE_SIMPLE_FOOD)).isEqualTo(1);
        assertThat(countRowsInTable(TestDbConstants.TABLE_FOOD_META_DATA)).isEqualTo(1);
        assertThat(countRowsInTable(TestDbConstants.TABLE_FOOD_SERVING_SIZE)).isEqualTo(1);
    }

    /**
     * Tests that a simple food that has the same name and brand as another food in the database,
     * upon import, updates the existing food instead of creating a new simple food.
     */
    @Test
    @Sql(TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS)
    public void Import_Updates_Existing_Simple_Food() throws IOException
    {
        // Arrange
        JsonFoodData jsonFoodData = new JsonFoodData(1);
        jsonFoodData.setFoodMetaData(List.of(testDataProvider.getTestFoodMetaDataJson(1)));
        jsonFoodData.setFoodServingSizes(List.of(testDataProvider.getTestFoodServingSizeJson(1)));
        jsonFoodData.setSimpleFoods(List.of(testDataProvider.getTestSimpleFoodJson(1)));

        String filename = String.format("foods_%s.json", dateString);
        String filePath = getExportFilePath(filename);
        jacksonMapper.writeValue(new File(filePath), jsonFoodData);

        // Act
        ArrayList<String> filenamesToImport = new ArrayList<>();
        filenamesToImport.add(filename);
        ImportDataTask sut = new ImportDataTask(foodRepository, mealPlanningRepository, recipeRepository, filenamesToImport);
        try
        {
            sut.run();
            sut.get();
            entityManager.flush();
        }
        catch(InterruptedException | ExecutionException e)
        {
            Assert.fail("The task has failed during execution.", e);
        }

        // Assert

        // Make sure the appropriate database records exist
        assertThat(countRowsInTable(TestDbConstants.TABLE_SIMPLE_FOOD)).isEqualTo(3);
        assertThat(countRowsInTable(TestDbConstants.TABLE_FOOD_META_DATA)).isEqualTo(3);
        assertThat(countRowsInTable(TestDbConstants.TABLE_FOOD_SERVING_SIZE)).isEqualTo(5);

        // Make sure the food's id hasn't been changed and data was updated
        JsonSimpleFood importedJsonSimpleFood = jsonFoodData.getSimpleFoods().stream().findFirst().orElseThrow();
        SimpleFood updatedSimpleFood = foodRepository.getSimpleFoodById(importedJsonSimpleFood.getFoodId());

        assertThat(updatedSimpleFood.getFoodId()).isEqualByComparingTo(importedJsonSimpleFood.getFoodId());
        assertThat(updatedSimpleFood.getCalories()).isEqualByComparingTo(importedJsonSimpleFood.getCalories());
    }

    /**
     * Tests that data is extracted from the database and arranged appropriately in the data export object
     * when both simple food and complex food data exists in the database.
     */
    @Test
    @Sql({TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS, TestDbConstants.SCRIPT_TEST_COMPLEX_FOODS})
    public void Export_Complex_Food_Data_To_Json()
    {
        JsonFoodData sut = foodRepository.exportData();

        assertThat(sut.getFoodMetaData().size()).isEqualTo(5);
        assertThat(sut.getFoodServingSizes().size()).isEqualTo(10);
        assertThat(sut.getComplexFoods().size()).isEqualTo(2);
        assertThat(sut.getComplexFoodIngredients().size()).isEqualTo(4);

        // Simple foods that are required for complex foods should be exported
        assertThat(sut.getSimpleFoods().size()).isEqualTo(3);
    }

    /**
     * Tests that the ExportDataTask can export complex food data from the database to json
     * and then write that data to a file
     */
    @Test
    @Sql({TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS, TestDbConstants.SCRIPT_TEST_COMPLEX_FOODS})
    public void Export_Complex_Food_Json_To_File()
    {
        ExportDataRequest exportRequest = new ExportDataRequest();
        exportRequest.setIncludeFoods(true);
        ExportDataTask sut = new ExportDataTask(foodRepository, mealPlanningRepository, recipeRepository, exportRequest);

        try
        {
            sut.run();
            sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            Assert.fail("The task has failed during execution.", e);
        }

        String filename = String.format("foods_%s.json", dateString);
        File exportedDataFile = new File(getExportFilePath(filename));
        assertThat(exportedDataFile.exists()).isTrue();
        assertThat(exportedDataFile.length()).isGreaterThan(0);
    }

    /**
     * Tests that a complex food that does not already exist in the database, upon import, is created.
     */
    @Test
    public void Import_Creates_New_Complex_Food() throws IOException
    {
        // Arrange
        String filename = createComplexFoodsTestFile();

        // Act
        ArrayList<String> filenamesToImport = new ArrayList<>();
        filenamesToImport.add(filename);
        ImportDataTask sut = new ImportDataTask(foodRepository, mealPlanningRepository, recipeRepository, filenamesToImport);
        try
        {
            sut.run();
            sut.get();
            entityManager.flush();
        }
        catch(InterruptedException | ExecutionException e)
        {
            Assert.fail("The task has failed during execution.", e);
        }

        // Assert
        assertThat(countRowsInTable(TestDbConstants.TABLE_COMPLEX_FOOD)).isEqualTo(1);
        assertThat(countRowsInTable(TestDbConstants.TABLE_FOOD_META_DATA)).isEqualTo(3);
        assertThat(countRowsInTable(TestDbConstants.TABLE_COMPLEX_FOOD_INGREDIENT)).isEqualTo(2);
        assertThat(countRowsInTable(TestDbConstants.TABLE_FOOD_SERVING_SIZE)).isEqualTo(3);
    }

    /**
     * Tests that a complex food that has the same name and nutritional profile as another complex
     * food in the database, upon import, updates the existing food instead of creating a new
     * complex food.
     */
    @Test
    @Sql({TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS, TestDbConstants.SCRIPT_TEST_COMPLEX_FOODS})
    public void Import_Updates_Existing_Complex_Food() throws IOException
    {
        // Arrange
        String filename = createComplexFoodsTestFile();

        // Act
        ArrayList<String> filenamesToImport = new ArrayList<>();
        filenamesToImport.add(filename);
        ImportDataTask sut = new ImportDataTask(foodRepository, mealPlanningRepository, recipeRepository, filenamesToImport);
        try
        {
            sut.run();
            sut.get();
            entityManager.flush();
        }
        catch(InterruptedException | ExecutionException e)
        {
            Assert.fail("The task has failed during execution.", e);
        }

        // Assert
        assertThat(countRowsInTable(TestDbConstants.TABLE_COMPLEX_FOOD)).isEqualTo(2);
        assertThat(countRowsInTable(TestDbConstants.TABLE_FOOD_META_DATA)).isEqualTo(5);
        assertThat(countRowsInTable(TestDbConstants.TABLE_COMPLEX_FOOD_INGREDIENT)).isEqualTo(4);
        assertThat(countRowsInTable(TestDbConstants.TABLE_FOOD_SERVING_SIZE)).isEqualTo(7);
    }

    /**
     * Tests that data is extracted from the database and arranged appropriately in the data export object
     * for recipe data found in the database
     */
    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS,
            TestDbConstants.SCRIPT_TEST_RECIPE,
            TestDbConstants.SCRIPT_TEST_RECIPE_INGREDIENTS,
            TestDbConstants.SCRIPT_TEST_RECIPE_STEPS
    })
    public void Export_Recipe_Data_To_Json()
    {
        JsonRecipeData sut = recipeRepository.exportData();

        assertThat(sut.getRecipes().size()).isEqualTo(1);
        assertThat(sut.getRecipeServingSizes().size()).isEqualTo(1);
        assertThat(sut.getRecipeIngredients().size()).isEqualTo(2);
        assertThat(sut.getRecipeSteps().size()).isEqualTo(2);
        assertThat(sut.getRecipeStepIngredients().size()).isEqualTo(2);
    }

    /**
     * Tests that the ExportDataTask can export recipe data from the database to json
     * and then write that data to a file
     */
    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS,
            TestDbConstants.SCRIPT_TEST_RECIPE,
            TestDbConstants.SCRIPT_TEST_RECIPE_INGREDIENTS,
            TestDbConstants.SCRIPT_TEST_RECIPE_STEPS
    })
    public void Export_Recipe_Json_To_File()
    {
        ExportDataRequest exportRequest = new ExportDataRequest();
        exportRequest.setIncludeRecipes(true);
        ExportDataTask sut = new ExportDataTask(foodRepository, mealPlanningRepository, recipeRepository, exportRequest);

        try
        {
            sut.run();
            sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            Assert.fail("The task has failed during execution.", e);
        }

        String filename = String.format("recipes_%s.json", dateString);
        File exportedDataFile = new File(getExportFilePath(filename));
        assertThat(exportedDataFile.exists()).isTrue();
        assertThat(exportedDataFile.length()).isGreaterThan(0);
    }

    /**
     * Tests that a complex food that does not already exist in the database, upon import, is created.
     */
    @Test
    public void Import_Creates_New_Recipe() throws IOException
    {
        // Arrange
        String foodsDataFileName = createComplexFoodsTestFile();
        JsonRecipeData jsonRecipeData = getTestRecipeData();

        String filename = String.format("recipes_%s.json", dateString);
        String filePath = getExportFilePath(filename);
        jacksonMapper.writeValue(new File(filePath), jsonRecipeData);

        ArrayList<String> filenamesToImport = new ArrayList<>();
        filenamesToImport.add(foodsDataFileName);
        filenamesToImport.add(filename);

        // Act
        ImportDataTask sut = new ImportDataTask(foodRepository, mealPlanningRepository, recipeRepository, filenamesToImport);
        try
        {
            sut.run();
            sut.get();
            entityManager.flush();
        }
        catch(InterruptedException | ExecutionException e)
        {
            Assert.fail("The task has failed during execution.", e);
        }

        // Assert
        assertThat(countRowsInTable(TestDbConstants.TABLE_RECIPE)).isEqualTo(1);
        assertThat(countRowsInTable(TestDbConstants.TABLE_RECIPE_SERVING_SIZE)).isEqualTo(2);
        assertThat(countRowsInTable(TestDbConstants.TABLE_RECIPE_INGREDIENT)).isEqualTo(2);
        assertThat(countRowsInTable(TestDbConstants.TABLE_RECIPE_STEP)).isEqualTo(3);
        assertThat(countRowsInTable(TestDbConstants.TABLE_RECIPE_STEP_INGREDIENT)).isEqualTo(2);
    }

    /**
     * Tests that a recipe that has the same name and nutritional profile as another recipe in the
     * database, upon import, updates the existing food instead of creating a new recipe.
     */
    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS,
            TestDbConstants.SCRIPT_TEST_RECIPE,
            TestDbConstants.SCRIPT_TEST_RECIPE_INGREDIENTS,
            TestDbConstants.SCRIPT_TEST_RECIPE_STEPS
    })
    public void Import_Updates_Existing_Recipe() throws IOException
    {
        // Arrange
        String foodsDataFileName = createComplexFoodsTestFile();
        JsonRecipeData jsonRecipeData = getTestRecipeData();

        String filename = String.format("recipes_%s.json", dateString);
        String filePath = getExportFilePath(filename);
        jacksonMapper.writeValue(new File(filePath), jsonRecipeData);

        ArrayList<String> filenamesToImport = new ArrayList<>();
        filenamesToImport.add(foodsDataFileName);
        filenamesToImport.add(filename);

        // Act
        ImportDataTask sut = new ImportDataTask(foodRepository, mealPlanningRepository, recipeRepository, filenamesToImport);
        try
        {
            sut.run();
            sut.get();
            entityManager.flush();
        }
        catch(InterruptedException | ExecutionException e)
        {
            Assert.fail("The task has failed during execution.", e);
        }

        // Assert
        assertThat(countRowsInTable(TestDbConstants.TABLE_RECIPE)).isEqualTo(1);
        assertThat(countRowsInTable(TestDbConstants.TABLE_RECIPE_SERVING_SIZE)).isEqualTo(2);
        assertThat(countRowsInTable(TestDbConstants.TABLE_RECIPE_INGREDIENT)).isEqualTo(2);
        assertThat(countRowsInTable(TestDbConstants.TABLE_RECIPE_STEP)).isEqualTo(3);
        assertThat(countRowsInTable(TestDbConstants.TABLE_RECIPE_STEP_INGREDIENT)).isEqualTo(2);

        Recipe updatedRecipe = recipeRepository.getAllRecipesForUser(UUID.fromString(TestDbConstants.USER_ID)).get(0);
        JsonRecipe importedRecipe = jsonRecipeData.getRecipes().stream().findFirst().orElseThrow();
        assertThat(updatedRecipe.getRecipeId()).isEqualByComparingTo(importedRecipe.getRecipeId());
    }

    /**
     * Tests that data is extracted from the database and arranged appropriately in the data export object
     * when meal plan but no carb cycling plan data exists in the database.
     */
    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS,
            TestDbConstants.SCRIPT_TEST_COMPLEX_FOODS,
            TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_MEAL_FOR_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_MEAL_INGREDIENTS_FOR_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_MEAL_PLAN_DAY_TO_CALENDAR_DAY_ASSOCIATION,
    })
    public void Export_Meal_Plan_Data_To_Json()
    {
        JsonMealPlanningData sut = mealPlanningRepository.exportData();

        assertThat(sut.getMealPlans().size()).isEqualTo(1);
        assertThat(sut.getNutrientSuggestions().size()).isEqualTo(3);
        assertThat(sut.getMealPlanDays().size()).isEqualTo(1);
        assertThat(sut.getMealPlanMeals().size()).isEqualTo(1);
        assertThat(sut.getMealPlanMealIngredients().size()).isEqualTo(2);
        assertThat(sut.getMealPlanCalendarDays().size()).isEqualTo(1);

        // None of this test data is present in the db. So the collections should be empty
        assertThat(sut.getCarbCyclingPlans().size()).isEqualTo(0);
        assertThat(sut.getCarbCyclingDays().size()).isEqualTo(0);
        assertThat(sut.getCarbCyclingMeals().size()).isEqualTo(0);
        assertThat(sut.getCarbCyclingMealConfigurations().size()).isEqualTo(0);
        assertThat(sut.getCarbCyclingPlanMealIngredients().size()).isEqualTo(0);
        assertThat(sut.getCarbCyclingPlanCalendarDays().size()).isEqualTo(0);
    }

    /**
     * Tests that the ExportDataTask can export meal plan data from the database to json
     * and then write that data to a file
     */
    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS,
            TestDbConstants.SCRIPT_TEST_COMPLEX_FOODS,
            TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_MEAL_FOR_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_MEAL_INGREDIENTS_FOR_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_MEAL_PLAN_DAY_TO_CALENDAR_DAY_ASSOCIATION,
    })
    public void Export_Meal_Plan_Json_To_File()
    {
        ExportDataRequest exportRequest = new ExportDataRequest();
        exportRequest.setIncludeMealPlans(true);
        ExportDataTask sut = new ExportDataTask(foodRepository, mealPlanningRepository, recipeRepository, exportRequest);

        try
        {
            sut.run();
            sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            Assert.fail("The task has failed during execution.", e);
        }

        String filename = String.format("meal-planning_%s.json", dateString);
        File exportedDataFile = new File(getExportFilePath(filename));
        assertThat(exportedDataFile.exists()).isTrue();
        assertThat(exportedDataFile.length()).isGreaterThan(0);
    }

    /**
     * Tests that a new meal plan and its associated hierarchy (days, meals, etc...) are created upon import
     * of meal planning data.
     *
     * This data should be created regardless of if other meal planning data already exists and the newly created
     * meal plan(s) should be deactivated by default (even if they are active according to the imported data)
     */
    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS,
            TestDbConstants.SCRIPT_TEST_COMPLEX_FOODS,
            TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_MEAL_FOR_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_MEAL_INGREDIENTS_FOR_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_MEAL_PLAN_DAY_TO_CALENDAR_DAY_ASSOCIATION,
    })
    public void Import_Meal_Plan_Data() throws IOException
    {
        // Arrange
        String foodsDataFileName = createComplexFoodsTestFile();

        JsonMealPlanningData jsonMealPlanningData = new JsonMealPlanningData(1);
        jsonMealPlanningData.setMealPlans(List.of(testDataProvider.getTestMealPlanJson()));
        jsonMealPlanningData.setNutrientSuggestions(testDataProvider.getTestMealPlanMacroSuggestionsJson());
        jsonMealPlanningData.setMealPlanCalendarDays(List.of(testDataProvider.getTestMealPlanCalendarDayJson()));
        jsonMealPlanningData.setMealPlanDays(List.of(testDataProvider.getTestMealPlanDayJson()));
        jsonMealPlanningData.setMealPlanMeals(List.of(testDataProvider.getTestMealPlanMealJson()));
        jsonMealPlanningData.setMealPlanMealIngredients(testDataProvider.getTestMealPlanMealIngredientsJson());

        // Act
        String filename = String.format("meal-planning_%s.json", dateString);
        String filePath = getExportFilePath(filename);
        jacksonMapper.writeValue(new File(filePath), jsonMealPlanningData);

        ArrayList<String> filenamesToImport = new ArrayList<>();
        filenamesToImport.add(foodsDataFileName);
        filenamesToImport.add(filename);
        ImportDataTask sut = new ImportDataTask(foodRepository, mealPlanningRepository, recipeRepository, filenamesToImport);
        try
        {
            sut.run();
            sut.get();
            entityManager.flush();
        }
        catch(InterruptedException | ExecutionException e)
        {
            Assert.fail("The task has failed during execution.", e);
        }

        // Assert
        assertThat(countRowsInTable(TestDbConstants.TABLE_MEAL_PLAN)).isEqualTo(2);
        assertThat(countRowsInTable(TestDbConstants.TABLE_MEAL_PLAN_CALENDAR_DAY)).isEqualTo(2);
        assertThat(countRowsInTable(TestDbConstants.TABLE_MEAL_PLAN_NUTRIENT_SUGGESTION)).isEqualTo(6);
        assertThat(countRowsInTable(TestDbConstants.TABLE_MEAL_PLANNING_DAY)).isEqualTo(2);
        assertThat(countRowsInTable(TestDbConstants.TABLE_MEAL_PLANNING_MEAL)).isEqualTo(2);
        assertThat(countRowsInTable(TestDbConstants.TABLE_MEAL_PLANNING_MEAL_INGREDIENT)).isEqualTo(4);

        Optional<MealPlan> importedMealPlan = mealPlanningRepository
                .getMealPlansForUser(UUID.fromString(TestDbConstants.USER_ID))
                .stream()
                .filter(mealPlanEntity -> !mealPlanEntity.getIsActive())
                .findFirst();
        assertThat(importedMealPlan.isPresent()).isTrue();
    }

    /**
     * Tests that data is extracted from the database and arranged appropriately in the data export object
     * when both meal plan and carb cycling plan data exists in the database.
     */
    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS,
            TestDbConstants.SCRIPT_TEST_COMPLEX_FOODS,
            TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_PLAN,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_PLAN_MEAL_CONFIGURATIONS,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_TRAINING_DAY,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_MEAL_FOR_TRAINING_DAY,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_MEAL_INGREDIENTS_FOR_TRAINING_DAY,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_PLAN_DAY_TO_CALENDAR_DAY_ASSOCIATION
    })
    public void Export_Carb_Cycling_Plan_Data_To_Json()
    {
        JsonMealPlanningData sut = mealPlanningRepository.exportData();

        /*
         * From the db's point of view, for the most part, carb cycling data is simply an extension
         * of meal planning data so the meal planning data must also exist.
         *
         * There are a couple exceptions to this with those being ingredients for carb cycling meals
         * and planned calendar days for carb cycling plan days. These two pieces of a meal plan don't
         * need tested here as they have their own test.
         */
        assertThat(sut.getMealPlans().size()).isEqualTo(1);
        assertThat(sut.getNutrientSuggestions().size()).isEqualTo(3);
        assertThat(sut.getMealPlanDays().size()).isEqualTo(1);
        assertThat(sut.getMealPlanMeals().size()).isEqualTo(1);

        assertThat(sut.getCarbCyclingPlans().size()).isEqualTo(1);
        assertThat(sut.getCarbCyclingDays().size()).isEqualTo(1);
        assertThat(sut.getCarbCyclingMeals().size()).isEqualTo(1);
        assertThat(sut.getCarbCyclingMealConfigurations().size()).isEqualTo(14);
        assertThat(sut.getCarbCyclingPlanMealIngredients().size()).isEqualTo(2);
        assertThat(sut.getCarbCyclingPlanCalendarDays().size()).isEqualTo(1);
    }

    /**
     * Tests that the ExportDataTask can export carb cycling data from the database to json
     * and then write that data to a file
     */
    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS,
            TestDbConstants.SCRIPT_TEST_COMPLEX_FOODS,
            TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_MEAL_FOR_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_MEAL_INGREDIENTS_FOR_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_PLAN,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_PLAN_MEAL_CONFIGURATIONS,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_TRAINING_DAY,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_MEAL_FOR_TRAINING_DAY,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_MEAL_INGREDIENTS_FOR_TRAINING_DAY,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_PLAN_DAY_TO_CALENDAR_DAY_ASSOCIATION
    })
    public void Export_Carb_Cycling_Plan_Json_To_File()
    {
        ExportDataRequest exportRequest = new ExportDataRequest();
        exportRequest.setIncludeMealPlans(true);
        ExportDataTask sut = new ExportDataTask(foodRepository, mealPlanningRepository, recipeRepository, exportRequest);

        try
        {
            sut.run();
            sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            Assert.fail("The task has failed during execution.", e);
        }

        String filename = String.format("meal-planning_%s.json", dateString);
        File exportedDataFile = new File(getExportFilePath(filename));
        assertThat(exportedDataFile.exists()).isTrue();
        assertThat(exportedDataFile.length()).isGreaterThan(0);
    }

    /**
     * Tests that a new carb cycling plan and its associated hierarchy (days, meals, etc...) are created upon import
     * of meal planning data.
     *
     * This data should be created regardless of if other carb cycling data already exists and the newly created
     * carb cycling plan(s) should be deactivated by default (even if they are active according to the imported data)
     */
    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS,
            TestDbConstants.SCRIPT_TEST_COMPLEX_FOODS,
            TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_PLAN,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_PLAN_MEAL_CONFIGURATIONS,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_TRAINING_DAY,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_MEAL_FOR_TRAINING_DAY,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_MEAL_INGREDIENTS_FOR_TRAINING_DAY,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_PLAN_DAY_TO_CALENDAR_DAY_ASSOCIATION
    })
    public void Import_Carb_Cycling_Plan_Data() throws IOException
    {
        // Arrange
        String foodsDataFileName = createComplexFoodsTestFile();
        /*
         * It is important to remember that, from the view of the database, carb cycling information
         * is simply an extension of the associated meal planning information. As such, carb cycling
         * records also require their associated records in the meal planning tables.
         */
        JsonMealPlanningData jsonMealPlanningData = new JsonMealPlanningData(1);

        jsonMealPlanningData.setMealPlans(List.of(testDataProvider.getTestMealPlanJson()));
        jsonMealPlanningData.setCarbCyclingPlans(List.of(testDataProvider.getTestCarbCyclingPlanJson()));
        jsonMealPlanningData.setCarbCyclingMealConfigurations(List.of(testDataProvider.getTestCarbCyclingPlanMealConfigJson()));

        jsonMealPlanningData.setMealPlanDays(List.of(testDataProvider.getTestCarbCyclingMealPlanDayJson()));
        jsonMealPlanningData.setCarbCyclingDays(List.of(testDataProvider.getTestCarbCyclingDayJson()));

        jsonMealPlanningData.setMealPlanMeals(List.of(testDataProvider.getTestCarbCyclingMealPlanMealJson()));
        jsonMealPlanningData.setCarbCyclingMeals(List.of(testDataProvider.getTestCarbCyclingMealJson()));
        jsonMealPlanningData.setCarbCyclingPlanMealIngredients(testDataProvider.getTestCarbCyclingMealIngredientsJson());

        // Act
        String filename = String.format("meal-planning_%s.json", dateString);
        String filePath = getExportFilePath(filename);
        jacksonMapper.writeValue(new File(filePath), jsonMealPlanningData);

        ArrayList<String> filenamesToImport = new ArrayList<>();
        filenamesToImport.add(foodsDataFileName);
        filenamesToImport.add(filename);
        ImportDataTask sut = new ImportDataTask(foodRepository, mealPlanningRepository, recipeRepository, filenamesToImport);
        try
        {
            sut.run();
            sut.get();
            entityManager.flush();
        }
        catch(InterruptedException | ExecutionException e)
        {
            Assert.fail("The task has failed during execution.", e);
        }

        // Assert
        assertThat(countRowsInTable(TestDbConstants.TABLE_CARB_CYCLING_PLAN)).isEqualTo(2);
        assertThat(countRowsInTable(TestDbConstants.TABLE_CARB_CYCLING_MEAL_CONFIGURATION)).isEqualTo(15);
        assertThat(countRowsInTable(TestDbConstants.TABLE_CARB_CYCLING_DAY)).isEqualTo(2);
        assertThat(countRowsInTable(TestDbConstants.TABLE_CARB_CYCLING_MEAL)).isEqualTo(2);
        assertThat(countRowsInTable(TestDbConstants.TABLE_MEAL_PLANNING_MEAL_INGREDIENT)).isEqualTo(4);
    }

    /**
     * Sets up test data such that both simple and complex foods are imported (because simple foods
     * are also always exported with complex foods due to complex foods being composed of and
     * depending on them) and then writes the test foods out to a file on disk.
     *
     * Due to using the minimum amount of data required this same file is also used for testing
     * meal plan and recipe data imports.
     *
     * @return The filename of the test file that is written to disk
     * @throws IOException Thrown if Jackson fails to write the file
     */
    private String createComplexFoodsTestFile() throws IOException
    {
        JsonFoodData jsonFoodData = new JsonFoodData(1);
        jsonFoodData.setFoodMetaData(List.of(testDataProvider.getTestFoodMetaDataJson(1), testDataProvider.getTestFoodMetaDataJson(2), testDataProvider.getTestFoodMetaDataJson(3)));
        jsonFoodData.setFoodServingSizes(List.of(testDataProvider.getTestFoodServingSizeJson(1), testDataProvider.getTestFoodServingSizeJson(2), testDataProvider.getTestFoodServingSizeJson(3)));
        jsonFoodData.setSimpleFoods(List.of(testDataProvider.getTestSimpleFoodJson(1), testDataProvider.getTestSimpleFoodJson(2)));
        jsonFoodData.setComplexFoods(List.of(testDataProvider.getTestComplexFoodJson()));
        jsonFoodData.setComplexFoodIngredients(testDataProvider.getTestComplexFoodIngredientsJson());

        String filename = String.format("foods_%s.json", dateString);
        String filePath = getExportFilePath(filename);
        jacksonMapper.writeValue(new File(filePath), jsonFoodData);

        return filename;
    }

    private JsonRecipeData getTestRecipeData()
    {
        JsonRecipeData jsonRecipeData = new JsonRecipeData(1);
        jsonRecipeData.setRecipes(List.of(testDataProvider.getTestRecipeJson()));
        jsonRecipeData.setRecipeIngredients(testDataProvider.getTestRecipeIngredientsJson());
        jsonRecipeData.setRecipeServingSizes(testDataProvider.getTestRecipeServingSizeJson());
        jsonRecipeData.setRecipeSteps(testDataProvider.getTestRecipeStepsJson());
        jsonRecipeData.setRecipeStepIngredients(testDataProvider.getTestRecipeStepIngredients());
        return jsonRecipeData;
    }
}
