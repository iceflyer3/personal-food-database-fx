/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.test.data.task.food;

import com.iceflyer3.pfd.data.task.food.IsUniqueSimpleFoodTask;
import com.iceflyer3.pfd.test.data.TestDataProvider;
import com.iceflyer3.pfd.test.data.config.FxRuntimeBootstrappedBase;
import com.iceflyer3.pfd.test.data.config.FxTaskTest;
import com.iceflyer3.pfd.test.data.config.TestDbConstants;
import com.iceflyer3.pfd.data.entities.User;
import com.iceflyer3.pfd.data.entities.food.SimpleFood;
import com.iceflyer3.pfd.data.mapping.UserMapper;
import com.iceflyer3.pfd.data.mapping.food.FoodMapper;
import com.iceflyer3.pfd.data.repository.FoodRepository;
import com.iceflyer3.pfd.data.repository.UserRepository;
import com.iceflyer3.pfd.data.task.food.GetSimpleFoodByIdTask;
import com.iceflyer3.pfd.data.task.food.SaveSimpleFoodTask;
import com.iceflyer3.pfd.exception.InvalidApplicationStateException;
import com.iceflyer3.pfd.exception.NonUniqueFoodException;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.SimpleFoodViewModel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static org.assertj.core.api.Assertions.assertThat;

@FxTaskTest
public class SimpleFoodTasksTests extends FxRuntimeBootstrappedBase
{
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FoodRepository foodRepository;

    private final TestDataProvider testDataProvider = new TestDataProvider();

    /**
     * Tests that a new unique simple food may be created. A new simple food is unique if it does
     * not share both a name _and_ a brand with any food that already exists.
     */
    @Test
    public void Can_Create_New_Unique_Simple_Food()
    {
        // Arrange
        User userEntity = userRepository.getByUsername(TestDbConstants.USER_USERNAME);
        SimpleFoodViewModel newFoodViewModel = testDataProvider.getTestSimpleFoodViewModel(1);
        SaveSimpleFoodTask sut = new SaveSimpleFoodTask(foodRepository, UserMapper.toModel(userEntity), newFoodViewModel);

        // Act
        SimpleFoodViewModel savedFood = null;
        try
        {
            sut.run();
            savedFood = sut.get();
        }
        catch (ExecutionException | InterruptedException e)
        {
            Assert.fail("The task has failed during execution", e);
        }

        // Assert
        assertThat(savedFood).isNotNull();
        assertThat(savedFood.getId()).isNotNull();
        assertThat(savedFood.getCreatedUser()).isEqualTo(userEntity.getUsername());
    }

    /**
     * Tests that the name/brand of a previously saved simple food may be updated as long as its name / brand
     * combination remain unique.
     */
    @Test
    @Sql(TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS)
    public void Can_Update_Unique_Simple_Food_Name()
    {
        // Arrange
        String updatedFoodName = "UpdatedFoodName";
        String updatedFoodBrand = "UpdatedFoodBrand";

        User userEntity = userRepository.getByUsername(TestDbConstants.USER_USERNAME);
        UserViewModel userViewModel = UserMapper.toModel(userEntity);

        UUID foodId = UUID.fromString(TestDbConstants.SIMPLE_FOOD_ONE_ID);
        SimpleFood foodEntity = foodRepository.getSimpleFoodById(foodId);
        SimpleFoodViewModel updatedTestViewModel = FoodMapper.toMutableModel(foodEntity);
        updatedTestViewModel.setName(updatedFoodName);
        updatedTestViewModel.setBrand(updatedFoodBrand);

        SaveSimpleFoodTask sut = new SaveSimpleFoodTask(foodRepository, userViewModel, updatedTestViewModel);

        // Act
        SimpleFoodViewModel updatedFood = null;
        try
        {
            sut.run();
            updatedFood = sut.get();
        }
        catch (ExecutionException | InterruptedException e)
        {
            Assert.fail("The task has failed during execution", e);
        }

        // Assert
        assertThat(updatedFood).isNotNull();
        assertThat(updatedFood.getName()).isEqualTo(updatedFoodName);
        assertThat(updatedFood.getBrand()).isEqualTo(updatedFoodBrand);
        assertThat(updatedFood.getLastModifiedUser()).isEqualTo(userViewModel.getUsername());
        assertThat(foodEntity) // No other fields changed
                .usingRecursiveComparison()
                .ignoringFields("name", "lastModifiedUser", "createdDate", "lastModifiedDate", "foodMetaData")
                .withEqualsForFields(testDataProvider.getEntityToViewModelUserComparisonPredicate(), "createdUser")
                .isEqualTo(updatedFood);
    }

    /**
     * Tests that non-name/brand details of a previously saved simple food may be updated as long as
     * its name / brand combination remain unique.
     */
    @Test
    @Sql(TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS)
    public void Can_Update_Unique_Simple_Food_Details()
    {
        // Arrange
        BigDecimal updatedCalories = BigDecimal.valueOf(9999);

        User userEntity = userRepository.getByUsername(TestDbConstants.USER_USERNAME);
        UserViewModel userViewModel = UserMapper.toModel(userEntity);

        UUID foodId = UUID.fromString(TestDbConstants.SIMPLE_FOOD_ONE_ID);
        SimpleFood foodEntity = foodRepository.getSimpleFoodById(foodId);
        SimpleFoodViewModel updatedTestViewModel = FoodMapper.toMutableModel(foodEntity);
        updatedTestViewModel.setCalories(updatedCalories);

        SaveSimpleFoodTask sut = new SaveSimpleFoodTask(foodRepository, userViewModel, updatedTestViewModel);

        // Act
        SimpleFoodViewModel updatedFood = null;
        try
        {
            sut.run();
            updatedFood = sut.get();
        }
        catch (ExecutionException | InterruptedException e)
        {
            Assert.fail("The task has failed during execution", e);
        }

        // Assert
        assertThat(updatedFood).isNotNull();
        assertThat(updatedFood.getCalories()).isEqualByComparingTo(updatedCalories);
        assertThat(updatedFood.getLastModifiedUser()).isEqualTo(userViewModel.getUsername());
        assertThat(foodEntity) // No other fields changed
                .usingRecursiveComparison()
                .ignoringFields("name", "lastModifiedUser", "createdDate", "lastModifiedDate", "foodMetaData")
                .withEqualsForFields(testDataProvider.getEntityToViewModelUserComparisonPredicate(), "createdUser")
                .isEqualTo(updatedFood);
    }

    /**
     * Tests that creation of a new simple food with a name / brand combination that already
     * exists fails.
     */
    @Test
    @Sql(TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS)
    public void Cannot_Create_New_Duplicate_Simple_Food()
    {
        // Arrange
        User userEntity = userRepository.getByUsername(TestDbConstants.USER_USERNAME);
        UserViewModel userViewModel = UserMapper.toModel(userEntity);

        UUID foodId = UUID.fromString(TestDbConstants.SIMPLE_FOOD_ONE_ID);
        SimpleFood existingFood = foodRepository.getSimpleFoodById(foodId);

        SimpleFoodViewModel newFoodViewModel = testDataProvider.getTestSimpleFoodViewModel(1);
        SaveSimpleFoodTask sut = new SaveSimpleFoodTask(foodRepository, userViewModel, newFoodViewModel);

        // Act
        try
        {
            sut.run();
            sut.get();
        }
        catch (ExecutionException | InterruptedException e)
        {
            String exceptionMessage = e.getCause().getMessage();

            // Assert
            assertThat(e.getCause()).isInstanceOf(NonUniqueFoodException.class);
            assertThat(exceptionMessage).contains(existingFood.getName());
            assertThat(exceptionMessage).contains(existingFood.getBrand());
        }
    }

    /**
     * Tests that an update to a previously saved simple food that makes the name / brand
     * combination of the food the same as another previously saved food fails.
     */
    @Test
    @Sql(TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS)
    public void Cannot_Update_Simple_Food_To_Duplicate()
    {
        // Arrange
        User userEntity = userRepository.getByUsername(TestDbConstants.USER_USERNAME);
        UserViewModel userViewModel = UserMapper.toModel(userEntity);

        UUID foodOneId = UUID.fromString(TestDbConstants.SIMPLE_FOOD_ONE_ID);
        SimpleFood firstFoodEntity = foodRepository.getSimpleFoodById(foodOneId);

        UUID foodTwoId = UUID.fromString(TestDbConstants.SIMPLE_FOOD_TWO_ID);
        SimpleFood secondFoodEntity = foodRepository.getSimpleFoodById(foodTwoId);
        SimpleFoodViewModel simpleFoodViewModel = FoodMapper.toMutableModel(secondFoodEntity);
        simpleFoodViewModel.setName(firstFoodEntity.getName());
        simpleFoodViewModel.setBrand(firstFoodEntity.getBrand());

        SaveSimpleFoodTask sut = new SaveSimpleFoodTask(foodRepository, userViewModel, simpleFoodViewModel);

        // Act
        try
        {
            sut.run();
            sut.get();
        }
        catch (ExecutionException | InterruptedException e)
        {
            String exceptionMessage = e.getCause().getMessage();

            // Assert
            assertThat(e.getCause()).isInstanceOf(NonUniqueFoodException.class);
            assertThat(exceptionMessage).contains(simpleFoodViewModel.getName());
            assertThat(exceptionMessage).contains(simpleFoodViewModel.getBrand());
        }
    }

    /**
     * Tests that a previously saved simple food may be retrieved by its unique id.
     */
    @Test
    @Sql(TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS)
    public void Can_Retrieve_Extant_Simple_Food_By_Id()
    {
        // Arrange
        User userEntity = userRepository.getByUsername(TestDbConstants.USER_USERNAME);

        UUID foodId = UUID.fromString(TestDbConstants.SIMPLE_FOOD_ONE_ID);
        GetSimpleFoodByIdTask sut = new GetSimpleFoodByIdTask(foodRepository, foodId);

        // Act
        SimpleFoodViewModel retrievedSimpleFood = null;
        try
        {
            sut.run();
            retrievedSimpleFood = sut.get();
        }
        catch (ExecutionException | InterruptedException e)
        {
            Assert.fail("The task has failed during execution", e);
        }

        // Assert
        assertThat(retrievedSimpleFood).isNotNull();
        assertThat(retrievedSimpleFood.getId()).isEqualByComparingTo(foodId);
    }

    /**
     * Tests that an attempt to retrieve a simple food by an id that does not already exist
     * fails.
     */
    @Test
    public void Cannot_Retrieve_NonExistent_Simple_Food_By_Id()
    {
        // Arrange
        UUID nonExistentId = UUID.fromString("df451622-8ad9-460a-88df-ac38ca334a46");
        GetSimpleFoodByIdTask sut = new GetSimpleFoodByIdTask(foodRepository, nonExistentId);

        // Act
        try
        {
            sut.run();
            sut.get();
        }
        catch (ExecutionException | InterruptedException e)
        {
            // Assert
            assertThat(e.getCause()).isInstanceOf(InvalidApplicationStateException.class);
            assertThat(e.getCause().getMessage()).contains(nonExistentId.toString());
        }
    }

    @Test
    @Sql(TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS)
    public void Unique_Simple_Food_Is_Detected_As_Unique()
    {
        // Arrange
        SimpleFoodViewModel newFoodViewModel = testDataProvider.getTestSimpleFoodViewModel(1);

        boolean isUnique = false;
        IsUniqueSimpleFoodTask sut = new IsUniqueSimpleFoodTask(foodRepository, newFoodViewModel);

        // Act
        try
        {
            sut.run();
            isUnique = sut.get();
        }
        catch (ExecutionException | InterruptedException e)
        {
            Assert.fail("The task has failed during execution", e);
        }

        // Assert
        assertThat(isUnique).isTrue();
    }

    @Test
    @Sql(TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS)
    public void Non_Unique_Simple_Food_Is_Not_Detected_As_Unique()
    {
        // Arrange
        SimpleFoodViewModel newFoodViewModel = testDataProvider.getTestSimpleFoodViewModel(1);
        newFoodViewModel.setName("TestFood1");
        newFoodViewModel.setBrand("TestBrand");

        boolean isUnique = true;
        IsUniqueSimpleFoodTask sut = new IsUniqueSimpleFoodTask(foodRepository, newFoodViewModel);

        // Act
        try
        {
            sut.run();
            isUnique = sut.get();
        }
        catch (ExecutionException | InterruptedException e)
        {
            Assert.fail("The task has failed during execution", e);
        }

        // Assert
        assertThat(isUnique).isFalse();
    }
}
