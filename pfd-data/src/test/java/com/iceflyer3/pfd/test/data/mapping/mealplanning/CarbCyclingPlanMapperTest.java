package com.iceflyer3.pfd.test.data.mapping.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.test.data.TestDataProvider;
import com.iceflyer3.pfd.data.entities.mealplanning.carbcycling.CarbCyclingPlan;
import com.iceflyer3.pfd.data.mapping.mealplanning.carbcycling.CarbCyclingPlanMapper;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingPlanViewModel;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class CarbCyclingPlanMapperTest
{
    private final TestDataProvider testDataProvider = new TestDataProvider();

    @Test
    public void Map_Active_Carb_Cycling_Plan_When_Timing_Fats()
    {
        // Arrange
        String highToLowCarbDayRatio = "4:3";
        CarbCyclingPlan planEntity = new CarbCyclingPlan(
                testDataProvider.getTestMealPlanEntity(),
                testDataProvider.getTestUserEntity(),
                highToLowCarbDayRatio,
                BigDecimal.TEN,
                BigDecimal.TEN,
                true,
                6);

        planEntity.setActive(true);

        // Act
        CarbCyclingPlanViewModel mappedViewModel = CarbCyclingPlanMapper.toModel(planEntity);

        // Assert
        String[] highLowCarbParts = highToLowCarbDayRatio.split(":");
        assertThat(mappedViewModel.getHighCarbDays()).isEqualTo(Integer.parseInt(highLowCarbParts[0]));
        assertThat(mappedViewModel.getLowCarbDays()).isEqualTo(Integer.parseInt(highLowCarbParts[1]));
        assertThat(mappedViewModel.getLowCarbDayDeficitPercentage().compareTo(BigDecimal.TEN)).isEqualTo(0);
        assertThat(mappedViewModel.getHighCarbDaySurplusPercentage().compareTo(BigDecimal.TEN)).isEqualTo(0);
        assertThat(mappedViewModel.getMealsPerDay()).isEqualTo(planEntity.getMealsPerDay());
        assertThat(mappedViewModel.isShouldTimeFats()).isEqualTo(planEntity.getShouldTimeFats());
        assertThat(mappedViewModel.isActive()).isEqualTo(planEntity.getActive());
    }

    @Test
    public void Map_Active_Carb_Cycling_Plan_When_Not_Timing_Fats()
    {
        // Arrange
        String highToLowCarbDayRatio = "4:3";
        CarbCyclingPlan planEntity = new CarbCyclingPlan(
                testDataProvider.getTestMealPlanEntity(),
                testDataProvider.getTestUserEntity(),
                highToLowCarbDayRatio,
                BigDecimal.TEN,
                BigDecimal.TEN,
                false,
                6);

        planEntity.setActive(true);

        // Act
        CarbCyclingPlanViewModel mappedViewModel = CarbCyclingPlanMapper.toModel(planEntity);

        // Assert
        String[] highLowCarbParts = highToLowCarbDayRatio.split(":");
        assertThat(mappedViewModel.getHighCarbDays()).isEqualTo(Integer.parseInt(highLowCarbParts[0]));
        assertThat(mappedViewModel.getLowCarbDays()).isEqualTo(Integer.parseInt(highLowCarbParts[1]));
        assertThat(mappedViewModel.getLowCarbDayDeficitPercentage().compareTo(BigDecimal.TEN)).isEqualTo(0);
        assertThat(mappedViewModel.getHighCarbDaySurplusPercentage().compareTo(BigDecimal.TEN)).isEqualTo(0);
        assertThat(mappedViewModel.getMealsPerDay()).isEqualTo(planEntity.getMealsPerDay());
        assertThat(mappedViewModel.isShouldTimeFats()).isEqualTo(planEntity.getShouldTimeFats());
        assertThat(mappedViewModel.isActive()).isEqualTo(planEntity.getActive());
    }

    @Test
    public void Map_Inactive_Carb_Cycling_Plan()
    {
        // Arrange
        String highToLowCarbDayRatio = "4:3";
        CarbCyclingPlan planEntity = new CarbCyclingPlan(
                testDataProvider.getTestMealPlanEntity(),
                testDataProvider.getTestUserEntity(),
                highToLowCarbDayRatio,
                BigDecimal.TEN,
                BigDecimal.TEN,
                false,
                6);

        // Act
        CarbCyclingPlanViewModel mappedViewModel = CarbCyclingPlanMapper.toModel(planEntity);

        // Assert
        String[] highLowCarbParts = highToLowCarbDayRatio.split(":");
        assertThat(mappedViewModel.getHighCarbDays()).isEqualTo(Integer.parseInt(highLowCarbParts[0]));
        assertThat(mappedViewModel.getLowCarbDays()).isEqualTo(Integer.parseInt(highLowCarbParts[1]));
        assertThat(mappedViewModel.getLowCarbDayDeficitPercentage().compareTo(BigDecimal.TEN)).isEqualTo(0);
        assertThat(mappedViewModel.getHighCarbDaySurplusPercentage().compareTo(BigDecimal.TEN)).isEqualTo(0);
        assertThat(mappedViewModel.getMealsPerDay()).isEqualTo(planEntity.getMealsPerDay());
        assertThat(mappedViewModel.isShouldTimeFats()).isEqualTo(planEntity.getShouldTimeFats());
        assertThat(mappedViewModel.isActive()).isEqualTo(planEntity.getActive());
    }
}
