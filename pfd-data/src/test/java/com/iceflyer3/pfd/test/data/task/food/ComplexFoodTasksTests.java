/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.test.data.task.food;

import com.iceflyer3.pfd.data.task.food.IsUniqueComplexFoodTask;
import com.iceflyer3.pfd.test.data.config.FxTaskTest;
import com.iceflyer3.pfd.test.data.TestDataProvider;
import com.iceflyer3.pfd.test.data.config.TestDbConstants;
import com.iceflyer3.pfd.data.entities.food.ComplexFood;
import com.iceflyer3.pfd.data.entities.food.ComplexFoodIngredient;
import com.iceflyer3.pfd.data.entities.food.SimpleFood;
import com.iceflyer3.pfd.data.mapping.IngredientMapper;
import com.iceflyer3.pfd.data.mapping.UserMapper;
import com.iceflyer3.pfd.data.mapping.food.FoodMapper;
import com.iceflyer3.pfd.data.repository.FoodRepository;
import com.iceflyer3.pfd.data.repository.UserRepository;
import com.iceflyer3.pfd.test.data.config.FxRuntimeBootstrappedBase;
import com.iceflyer3.pfd.data.task.food.GetComplexFoodByIdTask;
import com.iceflyer3.pfd.data.task.food.GetComplexFoodIngredientsTask;
import com.iceflyer3.pfd.data.task.food.SaveComplexFoodTask;
import com.iceflyer3.pfd.exception.InvalidApplicationStateException;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.viewmodel.IngredientViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.ComplexFoodViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.ReadOnlyFoodViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static org.assertj.core.api.Assertions.assertThat;

@FxTaskTest
public class ComplexFoodTasksTests extends FxRuntimeBootstrappedBase
{
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FoodRepository foodRepository;

    private final TestDataProvider testDataProvider = new TestDataProvider();

    /**
     * Tests that a previously saved complex food may be retrieved by its unique id.
     */
    @Test
    @Sql({TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS, TestDbConstants.SCRIPT_TEST_COMPLEX_FOODS})
    public void Can_Retrieve_Extant_Complex_Food_By_Id()
    {
        // Arrange
        UUID foodId = UUID.fromString(TestDbConstants.COMPLEX_FOOD_ONE_ID);
        GetComplexFoodByIdTask sut = new GetComplexFoodByIdTask(foodRepository, foodId);

        // Act
        ComplexFoodViewModel retrievedFood = null;
        try
        {
            sut.run();
            retrievedFood = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(retrievedFood).isNotNull();
        assertThat(retrievedFood.getId()).isEqualByComparingTo(foodId);
    }

    /**
     * Tests that an attempt to retrieve a complex food by an id that does not already exist
     * fails.
     */
    @Test
    public void Cannot_Retrieve_NonExistent_Complex_Food_By_Id()
    {
        // Arrange
        UUID nonExistentId = UUID.fromString("df451622-8ad9-460a-88df-ac38ca334a46");
        GetComplexFoodByIdTask sut = new GetComplexFoodByIdTask(foodRepository, nonExistentId);

        // Act
        try
        {
            sut.run();
            sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            // Assert
            assertThat(e.getCause()).isInstanceOf(InvalidApplicationStateException.class);
            assertThat(e.getCause().getMessage()).contains(nonExistentId.toString());
        }
    }

    /**
     * Tests that the ingredients for a previously saved complex food may be retrieved.
     */
    @Test
    @Sql({TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS, TestDbConstants.SCRIPT_TEST_COMPLEX_FOODS})
    public void Retrieve_Ingredients_For_Complex_Food()
    {
        // Arrange
        UUID foodId = UUID.fromString(TestDbConstants.COMPLEX_FOOD_ONE_ID);
        GetComplexFoodIngredientsTask sut = new GetComplexFoodIngredientsTask(foodRepository, foodId);

        // Act
        List<IngredientModel> retrievedIngredients = new ArrayList<>();
        try
        {
            sut.run();
            retrievedIngredients = sut.get();
        }
        catch (InterruptedException | ExecutionException e)
        {
            Assert.fail("The task has failed during execution", e);
        }

        // Assert
        assertThat(retrievedIngredients.size()).isEqualTo(2);
    }

    /**
     * Tests that a new complex food may be saved
     */
    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS
    })
    public void Create_New_Complex_Food()
    {
        // Arrange
        UserViewModel user = UserMapper.toModel(userRepository.getByUsername(TestDbConstants.USER_USERNAME));
        SimpleFood foodEntity1 = foodRepository.getSimpleFoodById(UUID.fromString(TestDbConstants.SIMPLE_FOOD_ONE_ID));
        SimpleFood foodEntity2 = foodRepository.getSimpleFoodById(UUID.fromString(TestDbConstants.SIMPLE_FOOD_TWO_ID));

        IngredientModel ingredient1 = new IngredientViewModel(FoodMapper.toModel(foodEntity1));
        IngredientModel ingredient2 = new IngredientViewModel(FoodMapper.toModel(foodEntity2));
        List<IngredientModel> ingredients = List.of(ingredient1, ingredient2);

        ComplexFoodViewModel complexFoodViewModel = testDataProvider.getTestComplexFoodViewModel(1);
        complexFoodViewModel.setIngredients(ingredients);

        SaveComplexFoodTask sut = new SaveComplexFoodTask(foodRepository, user, complexFoodViewModel);

        // Act
        ComplexFoodViewModel savedFood = null;
        try
        {
            sut.run();
            savedFood = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            Assert.fail("The task has failed during execution", e);
        }

        // Assert
        assertThat(savedFood).isNotNull();
        assertThat(savedFood.getId()).isNotNull();
    }

    /**
     * Tests that a complex food can be updated by adding an ingredient from it.
     */
    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS,
            TestDbConstants.SCRIPT_TEST_COMPLEX_FOODS
    })
    public void Add_Ingredient_To_Complex_Food()
    {
        // Arrange
        UserViewModel user = UserMapper.toModel(userRepository.getByUsername(TestDbConstants.USER_USERNAME));

        UUID complexFoodId = UUID.fromString(TestDbConstants.COMPLEX_FOOD_ONE_ID);
        ComplexFood complexFoodEntity = foodRepository.getComplexFoodById(complexFoodId);
        ComplexFoodViewModel complexFoodViewModel = FoodMapper.toMutableModel(complexFoodEntity, NutritionalSummary.empty());

        List<ComplexFoodIngredient> complexFoodIngredientEntities = foodRepository.getIngredientsForComplexFood(complexFoodId);
        List<IngredientModel> complexFoodIngredientModels = complexFoodIngredientEntities.stream().map(IngredientMapper::toModel).toList();
        complexFoodViewModel.setIngredients(complexFoodIngredientModels);

        UUID newIngredientFoodId = UUID.fromString(TestDbConstants.SIMPLE_FOOD_THREE_ID);
        SimpleFood newIngredientFoodEntity = foodRepository.getSimpleFoodById(newIngredientFoodId);
        ReadOnlyFoodViewModel newIngredientFood = FoodMapper.toModel(newIngredientFoodEntity);
        complexFoodViewModel.addIngredient(newIngredientFood);

        SaveComplexFoodTask sut = new SaveComplexFoodTask(foodRepository, user, complexFoodViewModel);

        // Act
        ComplexFoodViewModel updatedComplexFood = null;
        try
        {
            sut.run();
            updatedComplexFood = sut.get();

            // Load the ingredients. Usually this is handled by the proxy but since we aren't using
            // the proxy here we've gotta do it manually.
            List<ComplexFoodIngredient> ingredientEntities = foodRepository.getIngredientsForComplexFood(complexFoodViewModel.getId());
            List<IngredientModel> updatedIngredientsList = ingredientEntities.stream().map(IngredientMapper::toModel).toList();
            updatedComplexFood.setIngredients(updatedIngredientsList);
        }
        catch (InterruptedException | ExecutionException e)
        {
            Assert.fail("The task has failed during execution", e);
        }

        // Assert
        assertThat(updatedComplexFood).isNotNull();
        assertThat(updatedComplexFood.getIngredients().size()).isEqualTo(complexFoodIngredientModels.size() + 1);
        assertThat(updatedComplexFood.getIngredients().stream().anyMatch(ingredient -> ingredient.getIngredientFood().getId().equals(newIngredientFood.getId()))).isTrue();
    }

    /**
     * Tests that a complex food can be updated by removing an ingredient from it.
     */
    @Test
    @Sql({TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS, TestDbConstants.SCRIPT_TEST_COMPLEX_FOODS})
    public void Remove_Ingredient_From_Complex_Food()
    {
        // Arrange
        UserViewModel user = UserMapper.toModel(userRepository.getByUsername(TestDbConstants.USER_USERNAME));
        ComplexFood complexFoodEntity = foodRepository.getComplexFoodById(UUID.fromString(TestDbConstants.COMPLEX_FOOD_ONE_ID));

        List<IngredientModel> ingredientViewModels = complexFoodEntity.getIngredients().stream().map(IngredientMapper::toModel).toList();
        ComplexFoodViewModel complexFoodViewModel = FoodMapper.toMutableModel(complexFoodEntity, NutritionalSummary.empty());
        complexFoodViewModel.setIngredients(ingredientViewModels);

        List<IngredientModel> originalIngredientList = List.copyOf(complexFoodViewModel.getIngredients());

        IngredientModel removedIngredient = originalIngredientList.get(0);
        complexFoodViewModel.removeIngredient(removedIngredient);

        SaveComplexFoodTask sut = new SaveComplexFoodTask(foodRepository, user, complexFoodViewModel);

        // Act
        ComplexFoodViewModel updatedComplexFood = null;
        try
        {
            sut.run();
            updatedComplexFood = sut.get();

            // Load the ingredients. Usually this is handled by the proxy but since we aren't using
            // the proxy here we've gotta do it manually.
            List<ComplexFoodIngredient> ingredientEntities = foodRepository.getIngredientsForComplexFood(complexFoodEntity.getFoodId());
            List<IngredientModel> updatedIngredientsList = ingredientEntities.stream().map(IngredientMapper::toModel).toList();
            updatedComplexFood.setIngredients(updatedIngredientsList);
        }
        catch (InterruptedException | ExecutionException e)
        {
            Assert.fail("The task has failed during execution", e);
        }

        // Assert
        assertThat(updatedComplexFood).isNotNull();
        assertThat(updatedComplexFood.getIngredients().size()).isEqualTo(originalIngredientList.size() - 1);
        assertThat(
                    updatedComplexFood
                        .getIngredients()
                        .stream()
                        .noneMatch(ingredient -> ingredient.getIngredientFood().getId().equals(removedIngredient.getIngredientFood().getId()))
                  )
                  .isTrue();
    }


    /**
     * Tests that a complex food that has the same nutritional profile as another complex food but
     * a different name is correctly detected as being unique
     */
    @Test
    @Sql({TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS, TestDbConstants.SCRIPT_TEST_COMPLEX_FOODS})
    public void Complex_Food_With_Existing_Nutritional_Profile_But_Different_Name_Is_Unique()
    {
        // Arrange
        SimpleFood foodEntity1 = foodRepository.getSimpleFoodById(UUID.fromString(TestDbConstants.SIMPLE_FOOD_ONE_ID));
        SimpleFood foodEntity2 = foodRepository.getSimpleFoodById(UUID.fromString(TestDbConstants.SIMPLE_FOOD_TWO_ID));

        IngredientModel ingredient1 = new IngredientViewModel(FoodMapper.toModel(foodEntity1));
        IngredientModel ingredient2 = new IngredientViewModel(FoodMapper.toModel(foodEntity2));
        List<IngredientModel> ingredients = List.of(ingredient1, ingredient2);

        ComplexFoodViewModel complexFoodViewModel = testDataProvider.getTestComplexFoodViewModel(1);
        complexFoodViewModel.setIngredients(ingredients);

        boolean isUnique = false;

        // Act
        IsUniqueComplexFoodTask sut = new IsUniqueComplexFoodTask(foodRepository, complexFoodViewModel);

        try
        {
            sut.run();
            isUnique = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            Assert.fail("The task has failed during execution", e);
        }

        // Assert
        assertThat(isUnique).isTrue();
    }

    /**
     * Tests that a complex food that has the same name as another complex food but a different
     * nutritional profile is correctly detected as being unique
     */
    @Test
    @Sql({TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS, TestDbConstants.SCRIPT_TEST_COMPLEX_FOODS})
    public void Complex_Food_With_Existing_Name_But_Different_Ingredients_Is_Unique()
    {
        // Arrange
        SimpleFood foodEntity1 = foodRepository.getSimpleFoodById(UUID.fromString(TestDbConstants.SIMPLE_FOOD_ONE_ID));
        SimpleFood foodEntity2 = foodRepository.getSimpleFoodById(UUID.fromString(TestDbConstants.SIMPLE_FOOD_TWO_ID));
        SimpleFood foodEntity3 = foodRepository.getSimpleFoodById(UUID.fromString(TestDbConstants.SIMPLE_FOOD_THREE_ID));

        IngredientModel ingredient1 = new IngredientViewModel(FoodMapper.toModel(foodEntity1));
        IngredientModel ingredient2 = new IngredientViewModel(FoodMapper.toModel(foodEntity2));
        IngredientModel ingredient3 = new IngredientViewModel(FoodMapper.toModel(foodEntity3));
        List<IngredientModel> ingredients = List.of(ingredient1, ingredient2, ingredient3);

        ComplexFoodViewModel complexFoodViewModel = testDataProvider.getTestComplexFoodViewModel(1);
        complexFoodViewModel.setName("UnitTestFood");
        complexFoodViewModel.setIngredients(ingredients);

        boolean isUnique = false;

        // Act
        IsUniqueComplexFoodTask sut = new IsUniqueComplexFoodTask(foodRepository, complexFoodViewModel);

        try
        {
            sut.run();
            isUnique = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            Assert.fail("The task has failed during execution", e);
        }

        // Assert
        assertThat(isUnique).isTrue();
    }

    /**
     * Tests that a complex food that has both the same name and the same nutritional profile
     * as another complex food is correctly detected as non-unique.
     */
    @Test
    @Sql({TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS, TestDbConstants.SCRIPT_TEST_COMPLEX_FOODS})
    public void Duplicate_Complex_Food_Is_Not_Unique()
    {
        // Arrange
        SimpleFood foodEntity1 = foodRepository.getSimpleFoodById(UUID.fromString(TestDbConstants.SIMPLE_FOOD_ONE_ID));
        SimpleFood foodEntity2 = foodRepository.getSimpleFoodById(UUID.fromString(TestDbConstants.SIMPLE_FOOD_TWO_ID));

        IngredientModel ingredient1 = new IngredientViewModel(FoodMapper.toModel(foodEntity1));
        IngredientModel ingredient2 = new IngredientViewModel(FoodMapper.toModel(foodEntity2));
        List<IngredientModel> ingredients = List.of(ingredient1, ingredient2);

        ComplexFoodViewModel complexFoodViewModel = testDataProvider.getTestComplexFoodViewModel(1);
        complexFoodViewModel.setName("UnitTestFood");
        complexFoodViewModel.setIngredients(ingredients);

        boolean isUnique = true;

        // Act
        IsUniqueComplexFoodTask sut = new IsUniqueComplexFoodTask(foodRepository, complexFoodViewModel);

        try
        {
            sut.run();
            isUnique = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            Assert.fail("The task has failed during execution", e);
        }

        // Assert
        assertThat(isUnique).isFalse();
    }
}
