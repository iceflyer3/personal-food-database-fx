/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


package com.iceflyer3.pfd.test.data.task.mealplanning;

import com.iceflyer3.pfd.data.entities.mealplanning.MealPlan;
import com.iceflyer3.pfd.data.mapping.mealplanning.MealPlanMapper;
import com.iceflyer3.pfd.data.repository.MealPlanningRepository;
import com.iceflyer3.pfd.data.task.mealplanning.*;
import com.iceflyer3.pfd.enums.Nutrient;
import com.iceflyer3.pfd.enums.Sex;
import com.iceflyer3.pfd.enums.UnitOfMeasure;
import com.iceflyer3.pfd.test.data.config.FxRuntimeBootstrappedBase;
import com.iceflyer3.pfd.test.data.config.FxTaskTest;
import com.iceflyer3.pfd.test.data.config.TestDbConstants;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanNutrientSuggestionViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.registration.MealPlanningCalculatedRegistration;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.registration.MealPlanningManualRegistration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static org.assertj.core.api.Assertions.assertThat;

@FxTaskTest
public class MealPlanTasksTests extends FxRuntimeBootstrappedBase
{
    @Autowired
    private MealPlanningRepository mealPlanningRepository;

    /**
     * Tests that an inactive meal plan may be activated
     */
    @Test
    @Sql(TestDbConstants.SCRIPT_TEST_INACTIVE_MEAL_PLAN)
    public void Activate_Meal_Plan()
    {
        // Arrange
        UUID planId = UUID.fromString(TestDbConstants.MEAL_PLAN_INACTIVE_ID);
        ChangeMealPlanActivationStatusTask sut = new ChangeMealPlanActivationStatusTask(mealPlanningRepository, planId,true);

        // Act
        MealPlanViewModel updatedMealPlan = null;
        try
        {
            sut.run();
            updatedMealPlan = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(updatedMealPlan).isNotNull();
        assertThat(updatedMealPlan.isActive()).isTrue();
    }

    /**
     * Tests that an active meal plan may be deactivated
     */
    @Test
    @Sql(TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN)
    public void Deactivate_Meal_Plan()
    {
        // Arrange
        UUID planId = UUID.fromString(TestDbConstants.MEAL_PLAN_ACTIVE_ID);
        ChangeMealPlanActivationStatusTask sut = new ChangeMealPlanActivationStatusTask(mealPlanningRepository, planId,false);
        MealPlanViewModel updatedMealPlan = null;

        // Act
        try
        {
            sut.run();
            updatedMealPlan = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(updatedMealPlan).isNotNull();
        assertThat(updatedMealPlan.isActive()).isFalse();
    }

    /**
     * Tests that the active meal plan for a user may be retrieved
     */
    @Test
    @Sql(TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN)
    public void Find_Active_Meal_Plan_For_User()
    {
        // Arrange
        UUID userId = UUID.fromString(TestDbConstants.USER_ID);
        GetActiveMealPlanForUserTask sut = new GetActiveMealPlanForUserTask(mealPlanningRepository, userId);
        Optional<MealPlanViewModel> retrievedPlan = null;

        // Act
        try
        {
            sut.run();
            retrievedPlan = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(retrievedPlan).isNotNull();
        assertThat(retrievedPlan.isPresent()).isTrue();
    }

    /**
     * Tests that searching for an active meal plan for a user who does not have an active meal plan
     * returns a value indicating there was no result found and does not result in an error.
     */
    @Test
    @Sql(TestDbConstants.SCRIPT_TEST_INACTIVE_MEAL_PLAN)
    public void Search_For_Active_Meal_Plan_For_User_With_No_Result_Is_Valid()
    {
        // Arrange
        UUID planId = UUID.fromString(TestDbConstants.MEAL_PLAN_ACTIVE_ID);
        GetActiveMealPlanForUserTask sut = new GetActiveMealPlanForUserTask(mealPlanningRepository, planId);
        Optional<MealPlanViewModel> retrievedPlan = null;

        // Act
        try
        {
            sut.run();
            retrievedPlan = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(retrievedPlan).isNotNull();
        assertThat(retrievedPlan.isPresent()).isFalse();
    }

    /**
     * Tests that all meal plans for a user may be retrieved regardless of active status
     */
    @Test
    @Sql({TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN, TestDbConstants.SCRIPT_TEST_INACTIVE_MEAL_PLAN})
    public void Find_All_Meal_Plans_For_User()
    {
        // Arrange
        UUID userId = UUID.fromString(TestDbConstants.USER_ID);
        GetMealPlansForUserTask sut = new GetMealPlansForUserTask(mealPlanningRepository, userId);
        List<MealPlanViewModel> retrievedMealPlans = new ArrayList<>();

        // Act
        try
        {
            sut.run();
            retrievedMealPlans = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(retrievedMealPlans.size()).isEqualTo(2);
    }

    /**
     * Tests that a meal plan may be retrieved by its unique id
     */
    @Test
    @Sql(TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN)
    public void Find_Meal_Plan_By_Id()
    {
        // Arrange
        UUID planId = UUID.fromString(TestDbConstants.MEAL_PLAN_ACTIVE_ID);
        GetMealPlanByIdTask sut = new GetMealPlanByIdTask(mealPlanningRepository, planId);
        MealPlanViewModel retrievedMealPlan = null;

        // Act
        try
        {
            sut.run();
            retrievedMealPlan = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(retrievedMealPlan).isNotNull();
        assertThat(retrievedMealPlan.getId()).isEqualByComparingTo(planId);
    }

    /**
     * Tests that a meal plan retrieved with its unique id also loads data that is optional
     * during the creation of the plan (when that data was specified during creation)
     */
    @Test
    @Sql(TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN_WITH_OPTIONAL_DATA)
    public void Find_Meal_Plan_With_Optional_Data_By_Id()
    {
        // Arrange
        UUID planId = UUID.fromString(TestDbConstants.MEAL_PLAN_ACTIVE_ID);
        GetMealPlanByIdTask sut = new GetMealPlanByIdTask(mealPlanningRepository, planId);
        MealPlanViewModel retrievedMealPlan = null;

        // Act
        try
        {
            sut.run();
            retrievedMealPlan = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(retrievedMealPlan).isNotNull();
        assertThat(retrievedMealPlan.getId()).isEqualByComparingTo(planId);
        assertThat(retrievedMealPlan.getNutrientSuggestions().size()).isEqualTo(5);
    }

    /**
     * Tests that new meal plans may be created for a user where the details of the meal plan are calculated by the
     * application.
     *
     * Also tests that the plan is automatically activated after its creation and any previously active meal plan is
     * deactivated.
     */
    @Test
    @Sql(TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN)
    public void Create_New_Calculated_Meal_Plan()
    {
        // Arrange
        UUID userId = UUID.fromString(TestDbConstants.USER_ID);
        UUID previouslyActivePlanId = UUID.fromString(TestDbConstants.MEAL_PLAN_ACTIVE_ID);

        MealPlanningCalculatedRegistration calculatedRegistration = new MealPlanningCalculatedRegistration();
        calculatedRegistration.setAge(20);
        calculatedRegistration.setHeight(BigDecimal.valueOf(162.56));
        calculatedRegistration.setWeight(BigDecimal.valueOf(77.55));
        calculatedRegistration.setBodyFactPercentage(BigDecimal.valueOf(40.0));
        calculatedRegistration.setSex(Sex.FEMALE);
        calculatedRegistration.setProteinPercent(BigDecimal.valueOf(30));
        calculatedRegistration.setFatsPercent(BigDecimal.valueOf(35));
        calculatedRegistration.setCarbsPercent(BigDecimal.valueOf(35));

        SaveMealPlanTask sut = new SaveMealPlanTask(mealPlanningRepository, userId, calculatedRegistration);
        MealPlanViewModel savedMealPlan = null;
        MealPlanViewModel previouslyActivePlan = null;

        // Act
        try
        {
            sut.run();
            savedMealPlan = sut.get();
            previouslyActivePlan = MealPlanMapper.toModel(mealPlanningRepository.getMealPlanForId(previouslyActivePlanId));
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(savedMealPlan).isNotNull();
        assertThat(savedMealPlan.getId()).isNotNull();
        assertThat(savedMealPlan.isActive()).isTrue();
        assertThat(previouslyActivePlan.isActive()).isFalse();
    }

    @Test
    @Sql(TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN)
    public void Create_New_Manual_Meal_Plan()
    {
        // Arrange
        UUID userId = UUID.fromString(TestDbConstants.USER_ID);
        UUID previouslyActivePlanId = UUID.fromString(TestDbConstants.MEAL_PLAN_ACTIVE_ID);

        BigDecimal tdeeCaloriesAmount = BigDecimal.valueOf(100);
        BigDecimal dailyCaloriesAmount = BigDecimal.valueOf(200);
        BigDecimal proteinAmount = BigDecimal.TEN;
        BigDecimal carbsAmount = BigDecimal.TEN;
        BigDecimal fatsAmount = BigDecimal.TEN;

        MealPlanningManualRegistration manualRegistration = new MealPlanningManualRegistration();
        manualRegistration.setDailyCalories(dailyCaloriesAmount);
        manualRegistration.setTdeeCalories(tdeeCaloriesAmount);
        manualRegistration.setProteinAmount(proteinAmount);
        manualRegistration.setCarbsAmount(carbsAmount);
        manualRegistration.setTotalFatsAmount(fatsAmount);

        SaveMealPlanTask sut = new SaveMealPlanTask(mealPlanningRepository, userId, manualRegistration);
        MealPlanViewModel savedMealPlan = null;
        MealPlanViewModel previouslyActivePlan = null;

        // Act
        try
        {
            sut.run();
            savedMealPlan = sut.get();
            previouslyActivePlan = MealPlanMapper.toModel(mealPlanningRepository.getMealPlanForId(previouslyActivePlanId));
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(savedMealPlan).isNotNull();
        assertThat(savedMealPlan.getId()).isNotNull();
        assertThat(savedMealPlan.isActive()).isTrue();
        assertThat(previouslyActivePlan.isActive()).isFalse();

        // Verify that macronutrients match the manually specified amounts
        assertThat(savedMealPlan.getDailyCalories()).isEqualByComparingTo(dailyCaloriesAmount);
        assertThat(savedMealPlan.getTdeeCalories()).isEqualByComparingTo(tdeeCaloriesAmount);
        assertThat(savedMealPlan.getNutrientSuggestedAmount(Nutrient.PROTEIN)).isEqualByComparingTo(proteinAmount);
        assertThat(savedMealPlan.getNutrientSuggestedAmount(Nutrient.CARBOHYDRATES)).isEqualByComparingTo(carbsAmount);
        assertThat(savedMealPlan.getNutrientSuggestedAmount(Nutrient.TOTAL_FATS)).isEqualByComparingTo(fatsAmount);
    }

    /**
     * Tests that new meal plans may be created for a user where the details of the meal plan are calculated by the
     * application.
     *
     * Additionally, tests that any optional data (e.g. custom nutrient requirements) are saved alongside the required
     * data of the meal plan.
     */
    @Test
    public void Create_New_Calculated_Meal_Plan_With_Optional_Data()
    {
        // Arrange
        UUID userId = UUID.fromString(TestDbConstants.USER_ID);

        MealPlanningCalculatedRegistration planCalcRequest = new MealPlanningCalculatedRegistration();
        planCalcRequest.setAge(20);
        planCalcRequest.setHeight(BigDecimal.valueOf(162.56));
        planCalcRequest.setWeight(BigDecimal.valueOf(77.55));
        planCalcRequest.setBodyFactPercentage(BigDecimal.valueOf(40.0));
        planCalcRequest.setSex(Sex.FEMALE);
        planCalcRequest.setProteinPercent(BigDecimal.valueOf(30));
        planCalcRequest.setFatsPercent(BigDecimal.valueOf(35));
        planCalcRequest.setCarbsPercent(BigDecimal.valueOf(35));

        planCalcRequest.getCustomNutrients().addAll(
                new MealPlanNutrientSuggestionViewModel(Nutrient.FIBER, BigDecimal.ONE, UnitOfMeasure.GRAM),
                new MealPlanNutrientSuggestionViewModel(Nutrient.VITAMIN_A, BigDecimal.ONE, UnitOfMeasure.MICROGRAM)
        );

        SaveMealPlanTask sut = new SaveMealPlanTask(mealPlanningRepository, userId, planCalcRequest);
        MealPlanViewModel savedMealPlan = null;

        // Act
        try
        {
            sut.run();
            savedMealPlan = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(savedMealPlan).isNotNull();
        assertThat(savedMealPlan.getId()).isNotNull();
        assertThat(savedMealPlan.isActive()).isTrue();
        assertThat(savedMealPlan.getNutrientSuggestions().size()).isEqualTo(5);

    }

    /**
     * Tests that existing meal plans that have not yet had any days
     * or carb cycling plans created may be deleted for a user.
     *
     * The test meal plan is also inactive for this test case.
     */
    @Test
    @Sql(TestDbConstants.SCRIPT_TEST_INACTIVE_MEAL_PLAN)
    public void Delete_Empty_Meal_Plan()
    {
        // Arrange
        UUID userId = UUID.fromString(TestDbConstants.USER_ID);
        UUID planId = UUID.fromString(TestDbConstants.MEAL_PLAN_INACTIVE_ID);
        DeleteMealPlanTask sut = new DeleteMealPlanTask(mealPlanningRepository, planId);

        // Act
        try
        {
            sut.run();
            sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        List<MealPlan> mealPlanEntities = mealPlanningRepository.getMealPlansForUser(userId);

        // Assert
        assertThat(mealPlanEntities.isEmpty()).isTrue();
    }

    /**
     * Tests that existing meal plans that have not yet had any days
     * or carb cycling plans created may be deleted for a user.
     *
     * The test meal plan is active for this test case.
     */
    @Test
    @Sql(TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN)
    @Sql(TestDbConstants.SCRIPT_TEST_DAY_FOR_ACTIVE_MEAL_PLAN)
    @Sql(TestDbConstants.SCRIPT_TEST_MEAL_FOR_DAY_FOR_ACTIVE_MEAL_PLAN)
    public void Delete_Populated_Meal_Plan()
    {
        // Arrange
        UUID userId = UUID.fromString(TestDbConstants.USER_ID);
        UUID planId = UUID.fromString(TestDbConstants.MEAL_PLAN_ACTIVE_ID);
        DeleteMealPlanTask sut = new DeleteMealPlanTask(mealPlanningRepository, planId);

        // Act
        try
        {
            sut.run();
            sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        List<MealPlan> mealPlanEntities = mealPlanningRepository.getMealPlansForUser(userId);

        // Assert
        assertThat(mealPlanEntities.isEmpty()).isTrue();
    }
}
