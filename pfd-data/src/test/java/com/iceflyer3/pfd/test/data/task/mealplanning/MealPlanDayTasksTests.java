/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


package com.iceflyer3.pfd.test.data.task.mealplanning;

import com.iceflyer3.pfd.test.data.config.FxTaskTest;
import com.iceflyer3.pfd.test.data.config.TestDbConstants;
import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanDay;
import com.iceflyer3.pfd.enums.FitnessGoal;
import com.iceflyer3.pfd.data.mapping.mealplanning.MealPlanDayMapper;
import com.iceflyer3.pfd.data.repository.MealPlanningRepository;
import com.iceflyer3.pfd.test.data.config.FxRuntimeBootstrappedBase;
import com.iceflyer3.pfd.data.task.mealplanning.day.DeleteMealPlanDayTask;
import com.iceflyer3.pfd.data.task.mealplanning.day.GetDayForMealPlanByIdTask;
import com.iceflyer3.pfd.data.task.mealplanning.day.GetDaysForMealPlanTask;
import com.iceflyer3.pfd.data.task.mealplanning.day.SaveDayForMealPlanTask;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanDayViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static org.assertj.core.api.Assertions.assertThat;

@FxTaskTest
public class MealPlanDayTasksTests extends FxRuntimeBootstrappedBase
{
    @Autowired
    private MealPlanningRepository mealPlanningRepository;

    /**
     * Tests that all days for a meal plan may be retrieved
     */
    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_DAY_FOR_ACTIVE_MEAL_PLAN
    })
    public void Find_All_Days_For_Meal_Plan()
    {
        // Arrange
        UUID planId = UUID.fromString(TestDbConstants.MEAL_PLAN_ACTIVE_ID);
        GetDaysForMealPlanTask sut = new GetDaysForMealPlanTask(mealPlanningRepository, planId);
        List<MealPlanDayViewModel> allDays = new ArrayList<>();

        // Act
        try
        {
            sut.run();
            allDays = sut.get();
        }
        catch (InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(allDays.size()).isEqualTo(1);
    }

    /**
     * Tests that a day may be retrieved by its unique id
     */
    @Test
    @Sql({TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN, TestDbConstants.SCRIPT_TEST_DAY_FOR_ACTIVE_MEAL_PLAN})
    public void Find_Day_By_Id()
    {
        // Arrange
        UUID dayId = UUID.fromString(TestDbConstants.ACTIVE_MEAL_PLAN_DAY_ONE_ID);
        GetDayForMealPlanByIdTask sut = new GetDayForMealPlanByIdTask(mealPlanningRepository, dayId);
        MealPlanDayViewModel retrievedDay = null;

        // Act
        try
        {
            sut.run();
            retrievedDay = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(retrievedDay).isNotNull();
        assertThat(retrievedDay.getId()).isEqualByComparingTo(dayId);
    }

    /**
     * Tests that a new day may be created for a meal plan
     */
    @Test
    @Sql(TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN)
    public void Create_New_Day()
    {
        // Arrange
        UUID mealPlanId = UUID.fromString(TestDbConstants.MEAL_PLAN_ACTIVE_ID);

        MealPlanDayViewModel newDay = new MealPlanDayViewModel(null, mealPlanId, NutritionalSummary.empty());
        newDay.setDayLabel("Unit Test Day Creation");
        newDay.setFitnessGoal(FitnessGoal.LOSE);
        newDay.setLastModifiedDate(LocalDateTime.now());

        SaveDayForMealPlanTask sut = new SaveDayForMealPlanTask(mealPlanningRepository, newDay);
        MealPlanDayViewModel savedDay = null;

        // Act
        try
        {
            sut.run();
            savedDay = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        List<MealPlanDay> allDays = mealPlanningRepository.getDaysForMealPlan(mealPlanId);

        // Assert
        assertThat(savedDay).isNotNull();
        assertThat(savedDay.getId()).isNotNull();
        assertThat(allDays.size()).isEqualTo(1);
    }

    /**
     * Tests that a previously created day may be updated
     */
    @Test
    @Sql({TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN, TestDbConstants.SCRIPT_TEST_DAY_FOR_ACTIVE_MEAL_PLAN})
    public void Update_Existing_Day()
    {
        // Arrange
        String newDayLabel = "Updated Unit Test Day";
        UUID dayId = UUID.fromString(TestDbConstants.ACTIVE_MEAL_PLAN_DAY_ONE_ID);
        MealPlanDayViewModel day = MealPlanDayMapper.toModel(mealPlanningRepository.getDayForId(dayId), NutritionalSummary.empty());
        day.setDayLabel(newDayLabel);

        SaveDayForMealPlanTask sut = new SaveDayForMealPlanTask(mealPlanningRepository, day);
        MealPlanDayViewModel updatedDay = null;

        // Act
        try
        {
            sut.run();
            updatedDay = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(updatedDay).isNotNull();
        assertThat(updatedDay.getId()).isEqualByComparingTo(dayId);
        assertThat(updatedDay.getDayLabel()).isEqualTo(newDayLabel);
    }

    /**
     * Tests that an existing day may be deleted. When a day is deleted the entire hierarchy of
     * meal ingredient --> meal --> day should be deleted.
     */
    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS,
            TestDbConstants.SCRIPT_TEST_COMPLEX_FOODS,
            TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_MEAL_FOR_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_MEAL_INGREDIENTS_FOR_DAY_FOR_ACTIVE_MEAL_PLAN
    })
    public void Delete_Day()
    {
        // Arrange
        UUID mealPlanId = UUID.fromString(TestDbConstants.MEAL_PLAN_ACTIVE_ID);
        UUID dayId = UUID.fromString(TestDbConstants.ACTIVE_MEAL_PLAN_DAY_ONE_ID);
        DeleteMealPlanDayTask sut = new DeleteMealPlanDayTask(mealPlanningRepository, dayId);

        // Act
        try
        {
            sut.run();
            sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        List<MealPlanDay> allDays = mealPlanningRepository.getDaysForMealPlan(mealPlanId);

        // Assert
        assertThat(allDays.size()).isEqualTo(0);
    }
}
