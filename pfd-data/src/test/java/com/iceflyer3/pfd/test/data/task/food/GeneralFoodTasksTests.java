/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.test.data.task.food;

import com.iceflyer3.pfd.test.data.config.FxTaskTest;
import com.iceflyer3.pfd.test.data.config.TestDbConstants;
import com.iceflyer3.pfd.data.repository.FoodRepository;
import com.iceflyer3.pfd.test.data.config.FxRuntimeBootstrappedBase;
import com.iceflyer3.pfd.data.task.food.CanDeleteFoodTask;
import com.iceflyer3.pfd.data.task.food.DeleteFoodTask;
import com.iceflyer3.pfd.data.task.food.GetAllFoodsTask;
import com.iceflyer3.pfd.data.task.food.GetReadOnlyFoodByIdTask;
import com.iceflyer3.pfd.exception.InvalidApplicationStateException;
import com.iceflyer3.pfd.ui.model.viewmodel.FoodAsIngredientViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.ReadOnlyFoodViewModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Contains unit tests for the various food related tasks that are neither simple nor complex food
 * specific.
 */
@FxTaskTest
public class GeneralFoodTasksTests extends FxRuntimeBootstrappedBase
{
    @Autowired
    private FoodRepository foodRepository;

    @Test
    @Sql({TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS, TestDbConstants.SCRIPT_TEST_COMPLEX_FOODS})
    public void Deletion_Of_Food_Used_As_Ingredient_Is_Invalid()
    {
        // Arrange
        CanDeleteFoodTask sut = new CanDeleteFoodTask(foodRepository, UUID.fromString(TestDbConstants.SIMPLE_FOOD_ONE_ID));

        // Act
        List<FoodAsIngredientViewModel> ingredientOwners = new ArrayList<>();
        try
        {
            sut.run();
            ingredientOwners = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(ingredientOwners.size()).isGreaterThan(0);
        assertThat(ingredientOwners.stream().anyMatch(ingredientOwner -> ingredientOwner.getOwnerType().equals("Complex Food"))).isTrue();
    }

    @Test
    @Sql({TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS})
    public void Deletion_Of_Food_Not_Used_As_Ingredient_Is_Valid()
    {
        // Arrange
        CanDeleteFoodTask sut = new CanDeleteFoodTask(foodRepository, UUID.fromString(TestDbConstants.SIMPLE_FOOD_ONE_ID));

        // Act
        List<FoodAsIngredientViewModel> ingredientOwners = new ArrayList<>();
        try
        {
            sut.run();
            ingredientOwners = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(ingredientOwners.size()).isEqualTo(0);
    }

    @Test
    @Sql({TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS, TestDbConstants.SCRIPT_TEST_COMPLEX_FOODS})
    public void Valid_Deletion_Of_Food_Is_Successful()
    {
        // Arrange
        UUID foodId = UUID.fromString(TestDbConstants.COMPLEX_FOOD_ONE_ID);
        DeleteFoodTask sut = new DeleteFoodTask(foodRepository, foodId);

        // Act
        try
        {
            sut.run();
            sut.get();
        }
        catch (InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        try
        {
            foodRepository.getFoodForId(foodId);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            assertThat(e).isInstanceOf(InvalidApplicationStateException.class);
        }
    }

    @Test
    @Sql({TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS, TestDbConstants.SCRIPT_TEST_COMPLEX_FOODS})
    public void Retrieve_All_Foods()
    {
        // Arrange
        GetAllFoodsTask sut = new GetAllFoodsTask(foodRepository);

        // Act
        List<ReadOnlyFoodViewModel> allFoodsList = new ArrayList<>();
        try
        {
            sut.run();
            allFoodsList = sut.get();
        }
        catch (InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(allFoodsList.size()).isEqualTo(5);
    }

    @Test
    @Sql({TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS, TestDbConstants.SCRIPT_TEST_COMPLEX_FOODS})
    public void Can_Retrieve_Extant_Food_By_Id()
    {
        // Arrange
        UUID foodId = UUID.fromString(TestDbConstants.COMPLEX_FOOD_ONE_ID);
        GetReadOnlyFoodByIdTask sut = new GetReadOnlyFoodByIdTask(foodRepository, foodId);

        // Act
        ReadOnlyFoodViewModel retrievedFood = null;
        try
        {
            sut.run();
            retrievedFood = sut.get();
        }
        catch (InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(retrievedFood).isNotNull();
    }

    @Test
    @Sql({TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS, TestDbConstants.SCRIPT_TEST_COMPLEX_FOODS})
    public void Cannot_Retrieve_NonExistent_Food_By_Id()
    {
        // Arrange
        UUID nonExistentId = UUID.fromString("df451622-8ad9-460a-88df-ac38ca334a46");
        GetReadOnlyFoodByIdTask sut = new GetReadOnlyFoodByIdTask(foodRepository, nonExistentId);

        // Act
        try
        {
            sut.run();
            sut.get();
        }
        catch (InterruptedException | ExecutionException e)
        {
            // Assert
            assertThat(e.getCause()).isInstanceOf(InvalidApplicationStateException.class);
            assertThat(e.getCause().getMessage()).contains(nonExistentId.toString());
        }
    }
}
