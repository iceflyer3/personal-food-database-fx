package com.iceflyer3.pfd.test.data.calculator.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.calculator.mealplanning.CalculationResult;
import com.iceflyer3.pfd.data.calculator.mealplanning.CarbCyclingCalculator;
import com.iceflyer3.pfd.data.calculator.mealplanning.CarbCyclingDayRequirementsCalcuationRequest;
import com.iceflyer3.pfd.data.calculator.mealplanning.CarbCyclingMealCalculationRequest;
import com.iceflyer3.pfd.enums.CarbCyclingDayType;
import com.iceflyer3.pfd.enums.FitnessGoal;
import com.iceflyer3.pfd.enums.Nutrient;
import com.iceflyer3.pfd.util.NumberUtils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class CarbCyclingCalculatorTest {

    /*
     * Carb Cycling Day requests use the weight for the average American male based on data from the CDC.
     * See: https://www.cdc.gov/nchs/fastats/body-measurements.htm
     *
     * The requests use the TDEE calories for an average American male who is Sedentary and using the
     * Mifflin St Jeor BMR formula.
     *
     * Carb cycling performs all of its calculations based on the TDEE calories and disregards any surplus
     * or deficit applied by the meal plan (aka daily calories for the meal plan).
     *
     * The meal plan is one strategy for calculating the amount of calories to consume on a per day basis
     * and splitting them out into macronutrient amounts. The carb cycling plan is a different strategy
     * for accomplishing this same task.
     */
    @Test(dataProvider = "caloricRequirementsForCarbCyclingDayProvider")
    public void Caloric_Requirements_For_Carb_Cycling_Day(FitnessGoal fitnessGoal, CarbCyclingDayType dayType, BigDecimal expectedCalories)
    {
        // Arrange
        CarbCyclingDayRequirementsCalcuationRequest dayRequirementsCalculationRequest = new CarbCyclingDayRequirementsCalcuationRequest(fitnessGoal, dayType, new BigDecimal("2288.97"), new BigDecimal("200.00"));

        // Act
        CarbCyclingCalculator systemUnderTest = CarbCyclingCalculator.instance();
        CalculationResult calculationResult = systemUnderTest.calculateDayRequirements(dayRequirementsCalculationRequest);

        // Assert
        assertThat(calculationResult.getNeededCalories()).isEqualByComparingTo(NumberUtils.withStandardPrecision(expectedCalories));
    }

    @Test(dataProvider = "macronutrientRequirementsForCarbCyclingDayProvider")
    public void Macronutrient_Requirements_For_Carb_Cycling_Day(FitnessGoal fitnessGoal, CarbCyclingDayType dayType, BigDecimal expectedProtein, BigDecimal expectedCarbs, BigDecimal expectedFats)
    {
        // Arrange
        // Body weight is stored in the DB as kg. So we must be sure to also use kgs here as the input
        CarbCyclingDayRequirementsCalcuationRequest dayRequirementsCalculationRequest = new CarbCyclingDayRequirementsCalcuationRequest(fitnessGoal, dayType, new BigDecimal("2288.97"), new BigDecimal("90.71"));

        // Act
        CarbCyclingCalculator systemUnderTest = CarbCyclingCalculator.instance();
        CalculationResult calculationResult = systemUnderTest.calculateDayRequirements(dayRequirementsCalculationRequest);

        // Assert
        assertThat(calculationResult.getMacronutrientSuggestion(Nutrient.PROTEIN)).isEqualByComparingTo(NumberUtils.withStandardPrecision(expectedProtein));
        assertThat(calculationResult.getMacronutrientSuggestion(Nutrient.CARBOHYDRATES)).isEqualByComparingTo(NumberUtils.withStandardPrecision(expectedCarbs));
        assertThat(calculationResult.getMacronutrientSuggestion(Nutrient.TOTAL_FATS)).isEqualByComparingTo(NumberUtils.withStandardPrecision(expectedFats));
    }

    @Test(dataProvider = "macronutrientRequirementsForCarbCyclingMealOnTrainingDayProvider")
    public void Macronutrient_Requirements_For_Carb_Cycling_Meal_On_Training_Day(int mealNumber,
                                                                                 int postWorkoutMealNumber,
                                                                                 BigDecimal carbConsumptionPercentage,
                                                                                 boolean isTimingFats,
                                                                                 BigDecimal expectedProtein,
                                                                                 BigDecimal expectedCarbs,
                                                                                 BigDecimal expectedFats)
    {
        /*
         * Number of meals is hardcoded to four because this allows us to test each of the four
         * meal scenarios that need to be tested for training days.
         *
         * These four scenarios are:
         *      1.) A meal is before the post-workout meal
         *      2.) A meal is immediately after the post-workout meal
         *      3.) A meal is two or more meals after the post-workout meal
         *      4.) A meal IS the post-workout meal.
         */

        // Arrange
        CarbCyclingMealCalculationRequest mealCalculationRequest = new CarbCyclingMealCalculationRequest(
                6,
                mealNumber,
                postWorkoutMealNumber,
                carbConsumptionPercentage,
                BigDecimal.valueOf(200),
                BigDecimal.valueOf(286),
                BigDecimal.valueOf(38),
                isTimingFats,
                true);

        // Act
        CarbCyclingCalculator systemUnderTest = CarbCyclingCalculator.instance();
        CalculationResult mealCalcResult = systemUnderTest.calculateMealRequirements(mealCalculationRequest);

        // Assert
        assertThat(mealCalcResult.getMacronutrientSuggestion(Nutrient.PROTEIN)).isEqualByComparingTo(NumberUtils.withStandardPrecision(expectedProtein));
        assertThat(mealCalcResult.getMacronutrientSuggestion(Nutrient.CARBOHYDRATES)).isEqualByComparingTo(NumberUtils.withStandardPrecision(expectedCarbs));
        assertThat(mealCalcResult.getMacronutrientSuggestion(Nutrient.TOTAL_FATS)).isEqualByComparingTo(NumberUtils.withStandardPrecision(expectedFats));
    }

    @Test(dataProvider = "macronutrientRequirementsForCarbCyclingMealOnNonTrainingDay")
    public void Macronutrient_Requirements_For_Carb_Cycling_Meal_On_Non_Training_Day(int mealNumber,
                                                                                 BigDecimal carbConsumptionPercentage,
                                                                                 boolean isTimingFats,
                                                                                 BigDecimal expectedProtein,
                                                                                 BigDecimal expectedCarbs,
                                                                                 BigDecimal expectedFats)
    {
        /*
         * Number of meals is hardcoded to two because this allows us to test each of the two
         * scenarios that need tested for non-training days.
         *
         * By definition non-training days have no post-workout meal because there is no workout.
         *
         * These two scenarios are:
         *      1.) The meal is the first meal of the day.
         *      2.) The meal is not the first meal of the day.
         */
        // Arrange
        CarbCyclingMealCalculationRequest mealCalculationRequest = new CarbCyclingMealCalculationRequest(
                6,
                mealNumber,
                -1,
                carbConsumptionPercentage,
                BigDecimal.valueOf(200),
                BigDecimal.valueOf(286),
                BigDecimal.valueOf(38),
                isTimingFats,
                false);

        // Act
        CarbCyclingCalculator systemUnderTest = CarbCyclingCalculator.instance();
        CalculationResult mealCalcResult = systemUnderTest.calculateMealRequirements(mealCalculationRequest);

        // Assert
        assertThat(mealCalcResult.getMacronutrientSuggestion(Nutrient.PROTEIN)).isEqualByComparingTo(NumberUtils.withStandardPrecision(expectedProtein));
        assertThat(mealCalcResult.getMacronutrientSuggestion(Nutrient.CARBOHYDRATES)).isEqualByComparingTo(NumberUtils.withStandardPrecision(expectedCarbs));
        assertThat(mealCalcResult.getMacronutrientSuggestion(Nutrient.TOTAL_FATS)).isEqualByComparingTo(NumberUtils.withStandardPrecision(expectedFats));
    }

    /*----------------------------------------------------------------------------------------------------------------------------------
     *--
     *--                                            DATA PROVIDERS FOR PARAMETERIZED TESTS
     *--                                                            See
     *--                                https://testng.org/doc/documentation-main.html#parameters-dataproviders
     *--
     *----------------------------------------------------------------------------------------------------------------------------------
     */

    @DataProvider(name = "caloricRequirementsForCarbCyclingDayProvider")
    public Object[][] caloricRequirementsForCarbCyclingDayProvider()
    {
        return new Object[][] {
                {FitnessGoal.LOSE, CarbCyclingDayType.HIGH, new BigDecimal("2060.07")},
                {FitnessGoal.LOSE, CarbCyclingDayType.LOW, new BigDecimal("1716.73")},
                {FitnessGoal.GAIN, CarbCyclingDayType.HIGH, new BigDecimal("2517.87")},
                {FitnessGoal.GAIN, CarbCyclingDayType.LOW, new BigDecimal("2517.87")},
                {FitnessGoal.MAINTAIN, CarbCyclingDayType.HIGH, new BigDecimal("2288.97")},
                {FitnessGoal.MAINTAIN, CarbCyclingDayType.LOW, new BigDecimal("2288.97")}
        };
    }

    @DataProvider(name = "macronutrientRequirementsForCarbCyclingDayProvider")
    public Object[][] macronutrientRequirementsForCarbCyclingDayProvider()
    {
        return new Object[][] {
                {FitnessGoal.LOSE, CarbCyclingDayType.HIGH, new BigDecimal("200"), new BigDecimal("257.51"), new BigDecimal("25.56")},
                {FitnessGoal.LOSE, CarbCyclingDayType.LOW, new BigDecimal("200"), new BigDecimal("85.84"), new BigDecimal("63.71")},
                {FitnessGoal.GAIN, CarbCyclingDayType.HIGH, new BigDecimal("200"), new BigDecimal("314.73"), new BigDecimal("50.99")},
                {FitnessGoal.GAIN, CarbCyclingDayType.LOW, new BigDecimal("200"), new BigDecimal("157.37"), new BigDecimal("120.93")},
                {FitnessGoal.MAINTAIN, CarbCyclingDayType.HIGH, new BigDecimal("200"), new BigDecimal("286.12"), new BigDecimal("38.28")},
                {FitnessGoal.MAINTAIN, CarbCyclingDayType.LOW, new BigDecimal("200"), new BigDecimal("143.06"), new BigDecimal("101.86")}
        };
    }

    @DataProvider(name = "macronutrientRequirementsForCarbCyclingMealOnTrainingDayProvider")
    public Object[][] macronutrientRequirementsForCarbCyclingMealOnTrainingDayProvider()
    {
        /*
         * Macro values are rounded to the nearest whole number.
         *
         * Argument order is:
         *  - Meal Number
         *  - Post-workout meal number
         *  - Carb consumption percentage
         *  - is timing fats?
         *  - Expected protein
         *  - Expected carbs
         *  - Expected fats
         */
        BigDecimal tenPercentOfCarbs = BigDecimal.valueOf(10.0);
        BigDecimal twentyPercentOfCarbs = BigDecimal.valueOf(20.0);
        BigDecimal thirtyPercentOfCarbs = BigDecimal.valueOf(30.0);

        return new Object[][] {
                {1, 2, tenPercentOfCarbs, true, BigDecimal.valueOf(33.0), BigDecimal.valueOf(29.0), BigDecimal.ZERO},
                {1, 2, tenPercentOfCarbs, false, BigDecimal.valueOf(33.0), BigDecimal.valueOf(29.0), BigDecimal.valueOf(6.0)},
                {2, 2, thirtyPercentOfCarbs, true, BigDecimal.valueOf(33.0), BigDecimal.valueOf(86.0), BigDecimal.ZERO},
                {2, 2, thirtyPercentOfCarbs, false, BigDecimal.valueOf(33.0), BigDecimal.valueOf(86.0), BigDecimal.valueOf(6.0)},
                {3, 2, twentyPercentOfCarbs, true, BigDecimal.valueOf(33.0), BigDecimal.valueOf(57.0), BigDecimal.valueOf(10.0)},
                {3, 2, twentyPercentOfCarbs, false, BigDecimal.valueOf(33.0), BigDecimal.valueOf(57.0), BigDecimal.valueOf(6.0)},
                {4, 2, tenPercentOfCarbs, true, BigDecimal.valueOf(33.0), BigDecimal.valueOf(29.0), BigDecimal.valueOf(10.0)},
                {4, 2, tenPercentOfCarbs, false, BigDecimal.valueOf(33.0), BigDecimal.valueOf(29.0), BigDecimal.valueOf(6.0)}
        };
    }

    @DataProvider(name = "macronutrientRequirementsForCarbCyclingMealOnNonTrainingDay")
    public Object[][] macronutrientRequirementsForCarbCyclingMealOnNonTrainingDay()
    {
        /*
         * Macro values are rounded to the nearest whole number
         *
         * Argument order is:
         *  - Meal number
         *  - Carb consumption percentage
         *  - Is timing fats?
         *  - Expected protein
         *  - Expected carbs
         *  - Expected fats
         */

        /*
         * This value comes from taking the remaining percentage of carbs after the first meal and
         * dividing it by the remaining number of meals.
         *
         * So, as a formula:
         *  (100 - [meal one carb consumption percentage]) / ([number of meals for day] - 1)
         *
         * For our specific test case here that is:
         *  (100 - 30) / (6 - 1) = 70 / 5 = 14
         *
         * So each meal after first will consume 14% of the carbs for the day.
         */
        BigDecimal remainingMealsCarbPercentage = BigDecimal.valueOf(14.0);
        BigDecimal thirtyPercentOfCarbs = BigDecimal.valueOf(30.0);

        return new Object[][] {
                {1, thirtyPercentOfCarbs, true, BigDecimal.valueOf(33.0), BigDecimal.valueOf(86), BigDecimal.valueOf(6.0)},
                {1, thirtyPercentOfCarbs, false, BigDecimal.valueOf(33.0), BigDecimal.valueOf(86), BigDecimal.valueOf(6.0)},
                {2, remainingMealsCarbPercentage, true, BigDecimal.valueOf(33.0), BigDecimal.valueOf(40), BigDecimal.valueOf(6.0)},
                {2, remainingMealsCarbPercentage, false, BigDecimal.valueOf(33.0), BigDecimal.valueOf(40), BigDecimal.valueOf(6.0)}
        };
    }
}
