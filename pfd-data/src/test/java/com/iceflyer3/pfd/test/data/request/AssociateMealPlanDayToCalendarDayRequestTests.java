/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.test.data.request;

import com.iceflyer3.pfd.test.data.TestDataProvider;
import com.iceflyer3.pfd.enums.WeeklyInterval;
import com.iceflyer3.pfd.data.request.mealplanning.AssociateMealPlanDayToCalendarDayRequest;
import com.iceflyer3.pfd.ui.model.api.mealplanning.MealPlanDayModel;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanDayViewModel;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class AssociateMealPlanDayToCalendarDayRequestTests
{
    private final TestDataProvider testDataProvider;
    private final MealPlanDayViewModel testDay;
    private final Set<DayOfWeek> daysOfWeek;

    /*
     * Invariants:
     *  - A meal plan day must be selected
     *  - The date of the week must be selected
     *  - At least one calendar day must be selected
     *  - If the association is to be used on an interval then the interval must be provided
     */

    public AssociateMealPlanDayToCalendarDayRequestTests()
    {
        testDataProvider = new TestDataProvider();
        testDay = testDataProvider.getTestMealPlanDayViewModel();
        daysOfWeek = new HashSet<>();
        daysOfWeek.add(DayOfWeek.SUNDAY);
    }

    @Test(dataProvider = "unmetInvariantsDataProvider")
    public void Association_Request_With_Unmet_Invariants_Is_Invalid(MealPlanDayModel planDayModel, LocalDate weekStartDate, Set<DayOfWeek> daysOfWeek, WeeklyInterval weeklyInterval, int weeklyIntervalMonthsDuration)
    {
        AssociateMealPlanDayToCalendarDayRequest sut = new AssociateMealPlanDayToCalendarDayRequest(testDataProvider.getTestUserViewModel(1));
        sut.setWeekStartDate(weekStartDate);
        sut.setWeeklyInterval(weeklyInterval);
        sut.setWeeklyIntervalMonths(weeklyIntervalMonthsDuration);
        sut.setForDaysOfWeek(daysOfWeek);

        /*
         * This is a bit of an abuse of the polymorphism here. In practice the value used will be
         * either a meal planning day or carb cycling day proxy.
         *
         * But since we're just testing that the request object correctly checks for any value for
         * the selected day or not the exact value used for that day doesn't actually matter here.
         *
         * By extension the same is true of the test day view model. Which is why no effort is
         * made to set it up properly in the data provider function.
         */
        sut.setSelectedDay(planDayModel);

        assertThat(sut.validate().wasSuccessful()).isFalse();
    }

    @Test(dataProvider = "metInvariantsProvider")
    public void Association_Request_With_Met_Variants_Is_Valid(MealPlanDayModel planDayModel, LocalDate weekStartDate, Set<DayOfWeek> daysOfWeek, WeeklyInterval weeklyInterval, int weeklyIntervalMonthsDuration)
    {
        AssociateMealPlanDayToCalendarDayRequest sut = new AssociateMealPlanDayToCalendarDayRequest(testDataProvider.getTestUserViewModel(1));
        sut.setWeekStartDate(weekStartDate);
        sut.setWeeklyInterval(weeklyInterval);
        sut.setWeeklyIntervalMonths(weeklyIntervalMonthsDuration);
        sut.setSelectedDay(planDayModel);
        sut.setForDaysOfWeek(daysOfWeek);

        assertThat(sut.validate().wasSuccessful()).isTrue();
    }

    @DataProvider(name = "unmetInvariantsDataProvider")
    public Object[][] unmetInvariantsDataProvider()
    {
        return new Object[][]{
                {null, LocalDate.now(), daysOfWeek, WeeklyInterval.NONE, 0},
                {testDay, null, daysOfWeek, WeeklyInterval.NONE, 0},
                {testDay, LocalDate.now(), null, WeeklyInterval.NONE, 0},
                {testDay, LocalDate.now(), daysOfWeek, WeeklyInterval.BI_WEEKLY, 0}
        };
    }

    @DataProvider(name = "metInvariantsProvider")
    public Object[][] metInvariantsProvider()
    {
        return new Object[][]{
                {testDay, LocalDate.now(), daysOfWeek, WeeklyInterval.NONE, 0},
                {testDay, LocalDate.now(), daysOfWeek, WeeklyInterval.BI_WEEKLY, 1}
        };
    }
}
