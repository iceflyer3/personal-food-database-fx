package com.iceflyer3.pfd.test.data.mapping.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.test.data.TestDataProvider;
import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanDay;
import com.iceflyer3.pfd.data.mapping.mealplanning.carbcycling.CarbCyclingDayMapper;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingDayViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CarbCyclingPlanDayMapperTest
{
    private final TestDataProvider testDataProvider = new TestDataProvider();

    @Test
    public void Map_Carb_Cycling_Training_Day()
    {
        // Arrange
        MealPlanDay testDayEntity = testDataProvider.getTestCarbCyclingDay();
        testDayEntity.getCarbCyclingDay().setIsTrainingDay(true);

        // Act
        CarbCyclingDayViewModel mappedViewModel = CarbCyclingDayMapper.toModel(testDayEntity, NutritionalSummary.empty());

        // Assert
        assertThat(testDayEntity.getCarbCyclingDay().getDayType()).isEqualTo(mappedViewModel.getDayType());
        assertThat(testDayEntity.getCarbCyclingDay().getRequiredCalories()).isEqualByComparingTo(mappedViewModel.getRequiredCalories());
        assertThat(testDayEntity.getCarbCyclingDay().getRequiredProtein()).isEqualByComparingTo(mappedViewModel.getRequiredProtein());
        assertThat(testDayEntity.getCarbCyclingDay().getRequiredCarbs()).isEqualByComparingTo(mappedViewModel.getRequiredCarbs());
        assertThat(testDayEntity.getCarbCyclingDay().getRequiredFats()).isEqualByComparingTo(mappedViewModel.getRequiredFats());
        assertThat(testDayEntity.getCarbCyclingDay().isTrainingDay()).isEqualTo(mappedViewModel.isTrainingDay());
    }

    @Test
    public void Map_Carb_Cycling_Non_Training_Day()
    {
        // Arrange
        MealPlanDay testDayEntity = testDataProvider.getTestCarbCyclingDay();
        testDayEntity.getCarbCyclingDay().setIsTrainingDay(false);

        // Act
        CarbCyclingDayViewModel mappedViewModel = CarbCyclingDayMapper.toModel(testDayEntity, NutritionalSummary.empty());

        // Assert
        assertThat(testDayEntity.getCarbCyclingDay().getDayType()).isEqualTo(mappedViewModel.getDayType());
        assertThat(testDayEntity.getCarbCyclingDay().getRequiredCalories()).isEqualByComparingTo(mappedViewModel.getRequiredCalories());
        assertThat(testDayEntity.getCarbCyclingDay().getRequiredProtein()).isEqualByComparingTo(mappedViewModel.getRequiredProtein());
        assertThat(testDayEntity.getCarbCyclingDay().getRequiredCarbs()).isEqualByComparingTo(mappedViewModel.getRequiredCarbs());
        assertThat(testDayEntity.getCarbCyclingDay().getRequiredFats()).isEqualByComparingTo(mappedViewModel.getRequiredFats());
        assertThat(testDayEntity.getCarbCyclingDay().isTrainingDay()).isEqualTo(mappedViewModel.isTrainingDay());
    }
}
