/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.test.data.mapping.recipe;

import com.iceflyer3.pfd.test.data.TestDataProvider;
import com.iceflyer3.pfd.data.entities.recipe.Recipe;
import com.iceflyer3.pfd.data.mapping.recipe.RecipeMapper;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.api.recipe.RecipeStepModel;
import com.iceflyer3.pfd.ui.model.viewmodel.recipe.RecipeViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;
import org.testng.annotations.Test;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class RecipeMapperTest
{
    private final TestDataProvider testDataProvider = new TestDataProvider();

    /*
     * A general note on these tests:
     *
     * The created and last modified dates are chained down to LocalDateTime.now()
     * which makes them hard to test.
     *
     * This might be a point of refactoring in the future. But they aren't really
     * actually important at all. So for now these are just skipped outright.
     *
     * Serving sizes, ingredients, and recipe steps are skipped for all tests as
     * they have their own unit tests since they have their own mappers.
     */

    @Test
    public void Map_Recipe_To_Mutable_Model_Without_Ingredients_Or_Steps()
    {
        // Arrange
        Recipe emptyRecipeEntity = testDataProvider.getEmptyTestRecipeEntity();
        NutritionalSummary nutritionalSummary = testDataProvider.getTestNutritionalSummary();

        // Act
        RecipeViewModel mappedViewModel = RecipeMapper.toModel(emptyRecipeEntity, nutritionalSummary);

        // Assert
        assertThat(emptyRecipeEntity.getRecipeId()).isEqualTo(mappedViewModel.getId());
        assertThat(emptyRecipeEntity.getRecipeName()).isEqualTo(mappedViewModel.getName());
        assertThat(emptyRecipeEntity.getSource()).isEqualTo(mappedViewModel.getSource());
        assertThat(emptyRecipeEntity.getNumberOfServingsMade()).isEqualByComparingTo(mappedViewModel.getNumberOfServingsMade());
        assertThat(emptyRecipeEntity.getCreatedUser().getUsername()).isEqualTo(mappedViewModel.getCreatedUser());
        assertThat(emptyRecipeEntity.getLastModifiedUser().getUsername()).isEqualTo(mappedViewModel.getLastModifiedUser());
        assertThat(emptyRecipeEntity.getServingSizes().size()).isEqualTo(mappedViewModel.getServingSizes().size());
        assertThat(emptyRecipeEntity.getIngredients().size()).isEqualTo(0);
        assertThat(emptyRecipeEntity.getSteps().size()).isEqualTo(0);
    }

    @Test
    public void Map_Recipe_To_Mutable_Model_With_Ingredients_And_Steps()
    {
        // Arrange
        Recipe emptyRecipeEntity = testDataProvider.getEmptyTestRecipeEntity();
        Set<IngredientModel> testIngredients = testDataProvider.getTestIngredientViewModelSet(1);
        Set<RecipeStepModel> testSteps = testDataProvider.getTestRecipeSteps();
        NutritionalSummary nutritionalSummary = testDataProvider.getTestNutritionalSummary();

        // Act
        RecipeViewModel mappedViewModel = RecipeMapper.toModel(emptyRecipeEntity, nutritionalSummary, testIngredients, testSteps);

        // Assert
        assertThat(emptyRecipeEntity.getRecipeId()).isEqualTo(mappedViewModel.getId());
        assertThat(emptyRecipeEntity.getRecipeName()).isEqualTo(mappedViewModel.getName());
        assertThat(emptyRecipeEntity.getSource()).isEqualTo(mappedViewModel.getSource());
        assertThat(emptyRecipeEntity.getNumberOfServingsMade()).isEqualByComparingTo(mappedViewModel.getNumberOfServingsMade());
        assertThat(emptyRecipeEntity.getCreatedUser().getUsername()).isEqualTo(mappedViewModel.getCreatedUser());
        assertThat(emptyRecipeEntity.getLastModifiedUser().getUsername()).isEqualTo(mappedViewModel.getLastModifiedUser());
        assertThat(emptyRecipeEntity.getServingSizes().size()).isEqualTo(mappedViewModel.getServingSizes().size());

        assertThat(testIngredients.size()).isEqualTo(mappedViewModel.getIngredients().size());
        assertThat(testSteps.size()).isEqualTo(mappedViewModel.getSteps().size());
    }
}
