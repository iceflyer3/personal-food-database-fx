/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


package com.iceflyer3.pfd.test.data.task.mealplanning.carbcycling;

import com.iceflyer3.pfd.test.data.config.FxTaskTest;
import com.iceflyer3.pfd.test.data.config.TestDbConstants;
import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanDay;
import com.iceflyer3.pfd.enums.CarbCyclingDayType;
import com.iceflyer3.pfd.enums.FitnessGoal;
import com.iceflyer3.pfd.data.mapping.mealplanning.carbcycling.CarbCyclingDayMapper;
import com.iceflyer3.pfd.data.repository.MealPlanningRepository;
import com.iceflyer3.pfd.test.data.config.FxRuntimeBootstrappedBase;
import com.iceflyer3.pfd.data.task.mealplanning.carbcycling.day.GetCarbCyclingDayByIdTask;
import com.iceflyer3.pfd.data.task.mealplanning.carbcycling.day.GetCarbCyclingDaysForCarbCyclingPlanTask;
import com.iceflyer3.pfd.data.task.mealplanning.carbcycling.day.SaveCarbCyclingDayTask;
import com.iceflyer3.pfd.data.task.mealplanning.day.DeleteMealPlanDayTask;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingDayViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static org.assertj.core.api.Assertions.assertThat;

@FxTaskTest
public class CarbCyclingDayTasksTests extends FxRuntimeBootstrappedBase
{
    @Autowired
    private MealPlanningRepository mealPlanningRepository;

    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_PLAN,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_TRAINING_DAY
    })
    public void Find_All_Days_For_Carb_Cycling_Plan()
    {
        // Arrange
        UUID carbCyclingPlanId = UUID.fromString(TestDbConstants.CARB_CYCLING_PLAN_ID);
        GetCarbCyclingDaysForCarbCyclingPlanTask sut = new GetCarbCyclingDaysForCarbCyclingPlanTask(mealPlanningRepository, carbCyclingPlanId);
        List<CarbCyclingDayViewModel> retrievedDays = new ArrayList<>();

        // Act
        try
        {
            sut.run();
            retrievedDays = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(retrievedDays.size()).isEqualTo(1);
    }

    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_PLAN,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_TRAINING_DAY
    })
    public void Find_Day_By_Id()
    {
        // Arrange
        UUID dayId = UUID.fromString(TestDbConstants.CARB_CYCLING_PLAN_TRAINING_DAY_ID);
        GetCarbCyclingDayByIdTask sut = new GetCarbCyclingDayByIdTask(mealPlanningRepository, dayId);
        CarbCyclingDayViewModel retrievedDay = null;

        // Act
        try
        {
            sut.run();
            retrievedDay = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(retrievedDay).isNotNull();
        assertThat(retrievedDay.getId()).isEqualByComparingTo(dayId);
    }

    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_PLAN
    })
    public void Create_New_Day()
    {
        // Arrange
        UUID mealPlanId = UUID.fromString(TestDbConstants.MEAL_PLAN_ACTIVE_ID);
        UUID carbCyclingPlanId = UUID.fromString(TestDbConstants.CARB_CYCLING_PLAN_ID);

        CarbCyclingDayViewModel newDay = new CarbCyclingDayViewModel(null, mealPlanId, carbCyclingPlanId, NutritionalSummary.empty());
        newDay.setDayLabel("Unit Test Day Creation");
        newDay.setFitnessGoal(FitnessGoal.LOSE);
        newDay.setLastModifiedDate(LocalDateTime.now());
        newDay.setDayType(CarbCyclingDayType.HIGH);
        newDay.setRequiredCalories(BigDecimal.valueOf(100));
        newDay.setRequiredCarbs(BigDecimal.TEN);
        newDay.setRequiredFats(BigDecimal.TEN);
        newDay.setRequiredProtein(BigDecimal.TEN);

        SaveCarbCyclingDayTask sut = new SaveCarbCyclingDayTask(mealPlanningRepository, newDay);
        CarbCyclingDayViewModel savedDay = null;

        // Act
        try
        {
            sut.run();
            savedDay = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        List<MealPlanDay> allDaysForPlan = mealPlanningRepository.getCarbCyclingDaysForPlan(carbCyclingPlanId);

        // Assert
        assertThat(savedDay).isNotNull();
        assertThat(savedDay.getId()).isNotNull();
        assertThat(allDaysForPlan.size()).isEqualTo(1);
    }

    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_PLAN,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_TRAINING_DAY
    })
    public void Update_Existing_Day()
    {
        // Arrange
        String newDayLabel = "Updated Unit Test Carb Cycling Day";
        UUID dayId = UUID.fromString(TestDbConstants.CARB_CYCLING_PLAN_TRAINING_DAY_ID);
        CarbCyclingDayViewModel day = CarbCyclingDayMapper.toModel(mealPlanningRepository.getCarbCyclingDayForId(dayId), NutritionalSummary.empty());
        day.setDayLabel(newDayLabel);

        SaveCarbCyclingDayTask sut = new SaveCarbCyclingDayTask(mealPlanningRepository, day);
        CarbCyclingDayViewModel updatedDay = null;

        // Act
        try
        {
            sut.run();
            updatedDay = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(updatedDay).isNotNull();
        assertThat(updatedDay.getId()).isEqualByComparingTo(dayId);
        assertThat(updatedDay.getDayLabel()).isEqualTo(newDayLabel);
    }

    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS,
            TestDbConstants.SCRIPT_TEST_COMPLEX_FOODS,
            TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_PLAN,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_TRAINING_DAY,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_MEAL_FOR_TRAINING_DAY,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_MEAL_INGREDIENTS_FOR_TRAINING_DAY
    })
    public void Delete_Day()
    {
        // Arrange
        UUID carbCyclingPlanId = UUID.fromString(TestDbConstants.CARB_CYCLING_PLAN_ID);
        UUID dayId = UUID.fromString(TestDbConstants.CARB_CYCLING_PLAN_TRAINING_DAY_ID);
        DeleteMealPlanDayTask sut = new DeleteMealPlanDayTask(mealPlanningRepository, dayId);

        // Act
        try
        {
            sut.run();
            sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        List<MealPlanDay> daysForPlan = mealPlanningRepository.getCarbCyclingDaysForPlan(carbCyclingPlanId);

        // Assert
        assertThat(daysForPlan.isEmpty()).isTrue();
    }
}
