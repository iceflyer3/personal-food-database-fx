/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.test.data.config;

import com.iceflyer3.pfd.data.AppConfigUtils;
import com.iceflyer3.pfd.data.repository.UserRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.support.PersistenceAnnotationBeanPostProcessor;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * Spring Configuration class that has mostly the same beans as the main application's configuration
 * but uses an in-memory H2 database for the purposes of unit testing the data access layer instead
 * of an embedded H2 database.
 */
@Configuration
@EnableTransactionManagement
@PropertySource("classpath:configuration/appConfig.properties")
@ComponentScan(
        basePackageClasses = {
                AppConfigUtils.class,
                UserRepository.class
        })
public class SpringTestingConfig
{
        @Bean
        public DataSource dataSource()
        {
                return new EmbeddedDatabaseBuilder()
                        .setType(EmbeddedDatabaseType.H2)
                        .addScript("classpath:configuration/pfdDbInit.sql")
                        .build();
        }

        @Bean
        public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource, JpaVendorAdapter jpaVendorAdapter)
        {
                Properties hibernateProperties = new Properties();
                hibernateProperties.setProperty("hibernate.default_schema", "PFD_V1");

                hibernateProperties.setProperty("org.hibernate.flushMode", "ALWAYS");

                LocalContainerEntityManagerFactoryBean entityManager = new LocalContainerEntityManagerFactoryBean();
                entityManager.setDataSource(dataSource);
                entityManager.setJpaVendorAdapter(jpaVendorAdapter);
                entityManager.setPackagesToScan("com.iceflyer3.pfd.data.entities");
                entityManager.setJpaProperties(hibernateProperties);

                return entityManager;
        }

        @Bean
        public JpaVendorAdapter jpaVendorAdapter()
        {
                HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
                adapter.setDatabase(Database.H2);

                adapter.setShowSql(true);
                adapter.setGenerateDdl(false);

                adapter.setDatabasePlatform("org.hibernate.dialect.H2Dialect");
                return adapter;
        }

        @Bean
        public PlatformTransactionManager transactionManager(LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean)
        {
                JpaTransactionManager transactionManager = new JpaTransactionManager();
                transactionManager.setEntityManagerFactory(localContainerEntityManagerFactoryBean.getObject());
                return transactionManager;
        }

        @Bean
        public PersistenceAnnotationBeanPostProcessor paPostProcessor()
        {
                return new PersistenceAnnotationBeanPostProcessor();
        }
}
