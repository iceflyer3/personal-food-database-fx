/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


package com.iceflyer3.pfd.test.data.task.mealplanning.carbcycling;

import com.iceflyer3.pfd.test.data.config.FxTaskTest;
import com.iceflyer3.pfd.test.data.config.TestDbConstants;
import com.iceflyer3.pfd.data.entities.mealplanning.carbcycling.CarbCyclingMealConfiguration;
import com.iceflyer3.pfd.data.entities.mealplanning.carbcycling.CarbCyclingPlan;
import com.iceflyer3.pfd.data.repository.MealPlanningRepository;
import com.iceflyer3.pfd.test.data.config.FxRuntimeBootstrappedBase;
import com.iceflyer3.pfd.data.task.mealplanning.carbcycling.*;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingMealConfigurationViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingPlanViewModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ExecutionException;

import static org.assertj.core.api.Assertions.assertThat;

@FxTaskTest
public class CarbCyclingPlanTasksTests extends FxRuntimeBootstrappedBase
{
    @Autowired
    private MealPlanningRepository mealPlanningRepository;
    
    /**
     * Tests that all carb cycling plans for a meal plan may be retrieved
     */
    @Test
    @Sql({TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN, TestDbConstants.SCRIPT_TEST_CARB_CYCLING_PLAN})
    public void Find_All_Carb_Cycling_Plans_For_Meal_Plan()
    {
        // Arrange
        UUID mealPlanId = UUID.fromString(TestDbConstants.MEAL_PLAN_ACTIVE_ID);
        GetCarbCyclingPlansForMealPlanTask sut = new GetCarbCyclingPlansForMealPlanTask(mealPlanningRepository, mealPlanId);
        List<CarbCyclingPlanViewModel> retrievedPlans = new ArrayList<>();

        // Act
        try
        {
            sut.run();
            retrievedPlans = sut.get();
        }
        catch (InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(retrievedPlans.size()).isEqualTo(1);
    }

    /**
     * Tests that a carb cycling plan may be retrieved by its unique id.
     */
    @Test
    @Sql({TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN, TestDbConstants.SCRIPT_TEST_CARB_CYCLING_PLAN})
    public void Find_Carb_Cycling_Plan_By_Id()
    {
        // Arrange
        UUID carbCyclingPlanId = UUID.fromString(TestDbConstants.CARB_CYCLING_PLAN_ID);
        GetCarbCyclingPlanByIdTask sut = new GetCarbCyclingPlanByIdTask(mealPlanningRepository, carbCyclingPlanId);
        CarbCyclingPlanViewModel retrievedPlan = null;

        // Act
        try
        {
            sut.run();
            retrievedPlan = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(retrievedPlan).isNotNull();
        assertThat(retrievedPlan.getId()).isEqualByComparingTo(carbCyclingPlanId);
        // Configurations should not be loaded until explicitly requested.
        assertThat(retrievedPlan.getMealConfigurations().size()).isEqualTo(0);
    }

    /**
     * Tests that new carb cycling plans may be created for a meal plan
     */
    @Test
    @Sql(TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN)
    public void Create_New_Carb_Cycling_Plan()
    {
        // Arrange
        UUID userId = UUID.fromString(TestDbConstants.USER_ID);
        UUID mealPlanId = UUID.fromString(TestDbConstants.MEAL_PLAN_ACTIVE_ID);

        CarbCyclingPlanViewModel newCarbCyclingPlan = new CarbCyclingPlanViewModel();
        newCarbCyclingPlan.setMealsPerDay(5);
        newCarbCyclingPlan.setHighCarbDays(3);
        newCarbCyclingPlan.setLowCarbDays(1);
        newCarbCyclingPlan.setLowCarbDayDeficitPercentage(BigDecimal.valueOf(10));
        newCarbCyclingPlan.setHighCarbDaySurplusPercentage(BigDecimal.valueOf(20));
        newCarbCyclingPlan.toggleShouldTimeFats();
        newCarbCyclingPlan.initMealConfigurations();

        SaveCarbCyclingPlanTask sut = new SaveCarbCyclingPlanTask(mealPlanningRepository, userId, mealPlanId, newCarbCyclingPlan);
        CarbCyclingPlanViewModel savedPlan = null;

        // Act
        try
        {
            sut.run();
            savedPlan = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(savedPlan).isNotNull();
        assertThat(savedPlan.getId()).isNotNull();
        // Configurations should not be loaded until explicitly requested.
        assertThat(savedPlan.getMealConfigurations().isEmpty()).isTrue();
    }

    /**
     * Tests that existing carb cycling plans may be deleted for a meal plan.
     *
     * The carb cycling plan for this test also has meal configurations declared which should
     * also be deleted.
     */
    @Test
    @Sql({TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN, TestDbConstants.SCRIPT_TEST_CARB_CYCLING_PLAN, TestDbConstants.SCRIPT_TEST_CARB_CYCLING_PLAN_MEAL_CONFIGURATIONS})
    public void Delete_Carb_Cycling_Plan()
    {
        // Arrange
        UUID mealPlanId = UUID.fromString(TestDbConstants.MEAL_PLAN_ACTIVE_ID);
        UUID carbCyclingPlanId = UUID.fromString(TestDbConstants.CARB_CYCLING_PLAN_ID);
        DeleteCarbCyclingPlanTask sut = new DeleteCarbCyclingPlanTask(mealPlanningRepository, carbCyclingPlanId);

        // Act
        try
        {
            sut.run();
            sut.get();
        }
        catch (InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        List<CarbCyclingPlan> plansForMealPlan = mealPlanningRepository.getCarbCyclingPlansForMealPlan(mealPlanId);
        List<CarbCyclingMealConfiguration> mealConfigsForPlan = mealPlanningRepository.getMealConfigurationsForCarbCyclingPlan(carbCyclingPlanId);

        // Assert
        assertThat(plansForMealPlan.isEmpty()).isTrue();
        assertThat(mealConfigsForPlan.isEmpty()).isTrue();
    }

    /**
     * Tests the meal configurations defined at plan creation time may be loaded for a
     * carb cycling plan.
     */
    @Test
    @Sql({TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN, TestDbConstants.SCRIPT_TEST_CARB_CYCLING_PLAN, TestDbConstants.SCRIPT_TEST_CARB_CYCLING_PLAN_MEAL_CONFIGURATIONS})
    public void Load_Meal_Configurations_For_Carb_Cycling_Plan()
    {
        // Arrange
        UUID carbCyclingPlanId = UUID.fromString(TestDbConstants.CARB_CYCLING_PLAN_ID);
        Set<CarbCyclingMealConfigurationViewModel> retrievedMealConfigs = new HashSet<>();
        GetCarbCyclingMealConfigurationsByPlanIdTask sut = new GetCarbCyclingMealConfigurationsByPlanIdTask(mealPlanningRepository, carbCyclingPlanId);

        // Act
        try
        {
            sut.run();
            retrievedMealConfigs = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        /*
         * 5 meals per day on the test plan. When specified, configurations are required for both
         * training and non-training days. On training days configurations are required for meals
         * both before and after the post-workout meal as well as the post-workout meal itself.
         * On non-training days one configuration per meal is required.
         *
         * Generally speaking there should be a configuration that covers the place of any meal on any
         * day for the plan.
         *
         * 5 meals per day means:
         * On training days: 1 post-workout - 4 before workout - 4 after workout = 9 meal configs total
         * On non-training days: 5 meals = 5 meal configs total
         * 9 + 5 = 14
         */
        assertThat(retrievedMealConfigs.size()).isEqualTo(14);
    }
}
