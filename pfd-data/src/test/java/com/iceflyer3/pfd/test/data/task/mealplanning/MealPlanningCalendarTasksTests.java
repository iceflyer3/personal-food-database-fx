/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.test.data.task.mealplanning;

import com.iceflyer3.pfd.test.data.config.FxTaskTest;
import com.iceflyer3.pfd.test.data.config.TestDbConstants;
import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanCalendarDay;
import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanDay;
import com.iceflyer3.pfd.data.entities.views.CarbCyclingDayNutritionalSummary;
import com.iceflyer3.pfd.data.entities.views.MealPlanDayNutritionalSummary;
import com.iceflyer3.pfd.data.mapping.mealplanning.MealPlanDayMapper;
import com.iceflyer3.pfd.data.mapping.summary.NutritionalSummaryMapper;
import com.iceflyer3.pfd.data.repository.FoodRepository;
import com.iceflyer3.pfd.data.repository.MealPlanningRepository;
import com.iceflyer3.pfd.data.request.mealplanning.AssociateMealPlanDayToCalendarDayRequest;
import com.iceflyer3.pfd.data.request.mealplanning.GroceryListForMealPlanCalendarDateRangeSearchRequest;
import com.iceflyer3.pfd.test.data.config.FxRuntimeBootstrappedBase;
import com.iceflyer3.pfd.data.task.mealplanning.calendar.DeleteMealPlanCalendarDayTask;
import com.iceflyer3.pfd.data.task.mealplanning.calendar.FindGroceryListForCalendarDateRangeTask;
import com.iceflyer3.pfd.data.task.mealplanning.calendar.GetMealPlanCalendarDaysForUserTask;
import com.iceflyer3.pfd.data.task.mealplanning.calendar.SaveMealPlanCalendarDayTask;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanCalendarDayViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanDayViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.testng.annotations.Test;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.ExecutionException;

import static org.assertj.core.api.Assertions.assertThat;

@FxTaskTest
public class MealPlanningCalendarTasksTests extends FxRuntimeBootstrappedBase
{
    @Autowired
    private FoodRepository foodRepository;

    @Autowired
    private MealPlanningRepository mealPlanningRepository;

    /*
     * Careful readers will note that we've included the scripts for test simple foods
     * and test complex foods individually with each method below instead of using
     * a single class level annotation.
     *
     * This is because when that was attempted it appeared as if our user creation
     * script wasn't running. Since a majority of the task tests in the entire suite
     * depend on that script we're just going to work around that problem by adding
     * the food creation scripts to each individual method.
     *
     * This, for whatever reason, appears to resolve the error.
     */

    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS,
            TestDbConstants.SCRIPT_TEST_COMPLEX_FOODS,
            TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_MEAL_FOR_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_MEAL_INGREDIENTS_FOR_DAY_FOR_ACTIVE_MEAL_PLAN
    })
    public void Create_New_MealPlanDay_CalendarDay_Association()
    {
        // Arrange
        UUID mealPlanDayId = UUID.fromString(TestDbConstants.ACTIVE_MEAL_PLAN_DAY_ONE_ID);
        Optional<MealPlanDayNutritionalSummary> nutritionalSummaryEntity = mealPlanningRepository.getDayNutritionalSummary(mealPlanDayId);
        NutritionalSummary nutritionalSummary = NutritionalSummaryMapper.toModel(nutritionalSummaryEntity.get()); // Guaranteed to be present

        MealPlanDay dayEntity = mealPlanningRepository.getDayForId(mealPlanDayId);
        MealPlanDayViewModel dayViewModel = MealPlanDayMapper.toModel(dayEntity, nutritionalSummary);

        UserViewModel userModel = new UserViewModel(UUID.fromString(TestDbConstants.USER_ID), "UnitTestUser");
        AssociateMealPlanDayToCalendarDayRequest request = new AssociateMealPlanDayToCalendarDayRequest(userModel);
        request.setSelectedDay(dayViewModel);
        request.setForDaysOfWeek(Set.of(DayOfWeek.MONDAY, DayOfWeek.WEDNESDAY, DayOfWeek.FRIDAY));
        request.setWeekStartDate(LocalDate.parse("2023-08-06"));


        // Act
        List<MealPlanCalendarDayViewModel> createdDays = new ArrayList<>();
        SaveMealPlanCalendarDayTask sut = new SaveMealPlanCalendarDayTask(mealPlanningRepository, request);
        try
        {
            sut.run();
            createdDays = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(createdDays.size()).isEqualTo(3);
    }

    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS,
            TestDbConstants.SCRIPT_TEST_COMPLEX_FOODS,
            TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_PLAN,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_TRAINING_DAY,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_MEAL_FOR_TRAINING_DAY,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_MEAL_INGREDIENTS_FOR_TRAINING_DAY
    })
    public void Create_New_CarbCyclingDay_CalendarDay_Association()
    {
        // Arrange
        UUID dayId = UUID.fromString(TestDbConstants.CARB_CYCLING_PLAN_TRAINING_DAY_ID);
        Optional<CarbCyclingDayNutritionalSummary> nutritionalSummaryEntity = mealPlanningRepository.getCarbCyclingDayNutritionalSummary(dayId);
        NutritionalSummary nutritionalSummary = NutritionalSummaryMapper.toModel(nutritionalSummaryEntity.get()); // Guaranteed to be present for the test

        MealPlanDay dayEntity = mealPlanningRepository.getCarbCyclingDayForId(dayId);
        MealPlanDayViewModel dayViewModel = MealPlanDayMapper.toModel(dayEntity, nutritionalSummary);

        UserViewModel userModel = new UserViewModel(UUID.fromString(TestDbConstants.USER_ID), "UnitTestUser");
        AssociateMealPlanDayToCalendarDayRequest request = new AssociateMealPlanDayToCalendarDayRequest(userModel);
        request.setSelectedDay(dayViewModel);
        request.setForDaysOfWeek(Set.of(DayOfWeek.MONDAY, DayOfWeek.WEDNESDAY, DayOfWeek.FRIDAY));
        request.setWeekStartDate(LocalDate.parse("2023-08-06"));

        // Act
        List<MealPlanCalendarDayViewModel> createdDays = new ArrayList<>();
        SaveMealPlanCalendarDayTask sut = new SaveMealPlanCalendarDayTask(mealPlanningRepository, request);
        try
        {
            sut.run();
            createdDays = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(createdDays.size()).isEqualTo(3);
    }

    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS,
            TestDbConstants.SCRIPT_TEST_COMPLEX_FOODS,
            TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_MEAL_FOR_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_MEAL_INGREDIENTS_FOR_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_MEAL_PLAN_DAY_TO_CALENDAR_DAY_ASSOCIATION
    })
    public void Delete_MealPlanDay_CalendarDay_Association()
    {
        DeleteMealPlanCalendarDayTask sut = new DeleteMealPlanCalendarDayTask(mealPlanningRepository, UUID.fromString(TestDbConstants.MEAL_PLAN_CALENDAR_MEAL_PLAN_DAY_ASSOCIATION_ID));

        try
        {
            sut.run();
            sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        List<MealPlanCalendarDay> calendarDays = mealPlanningRepository.getMealPlanCalendarDaysForUser(UUID.fromString(TestDbConstants.USER_ID));
        assertThat(calendarDays.size()).isEqualByComparingTo(0);
    }

    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS,
            TestDbConstants.SCRIPT_TEST_COMPLEX_FOODS,
            TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_PLAN,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_TRAINING_DAY,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_MEAL_FOR_TRAINING_DAY,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_MEAL_INGREDIENTS_FOR_TRAINING_DAY,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_PLAN_DAY_TO_CALENDAR_DAY_ASSOCIATION
    })
    public void Delete_CarbCyclingDay_CalendarDay_Association()
    {
        DeleteMealPlanCalendarDayTask sut = new DeleteMealPlanCalendarDayTask(mealPlanningRepository, UUID.fromString(TestDbConstants.MEAL_PLAN_CALENDAR_CARB_CYCLING_PLAN_DAY_ASSOCIATION_ID));

        try
        {
            sut.run();
            sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        List<MealPlanCalendarDay> calendarDays = mealPlanningRepository.getMealPlanCalendarDaysForUser(UUID.fromString(TestDbConstants.USER_ID));
        assertThat(calendarDays.size()).isEqualByComparingTo(0);
    }

    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS,
            TestDbConstants.SCRIPT_TEST_COMPLEX_FOODS,
            TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_MEAL_FOR_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_MEAL_INGREDIENTS_FOR_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_MEAL_PLAN_DAY_TO_CALENDAR_DAY_ASSOCIATION,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_PLAN,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_TRAINING_DAY,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_MEAL_FOR_TRAINING_DAY,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_MEAL_INGREDIENTS_FOR_TRAINING_DAY,
            TestDbConstants.SCRIPT_TEST_CARB_CYCLING_PLAN_DAY_TO_CALENDAR_DAY_ASSOCIATION
    })
    public void Find_All_CalendarDay_Associations()
    {
        GetMealPlanCalendarDaysForUserTask sut = new GetMealPlanCalendarDaysForUserTask(mealPlanningRepository, UUID.fromString(TestDbConstants.USER_ID));

        Set<MealPlanCalendarDayViewModel> calendarDays = new HashSet<>();
        try
        {
            sut.run();
            calendarDays = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        assertThat(calendarDays.size()).isEqualByComparingTo(2);
    }

    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS,
            TestDbConstants.SCRIPT_TEST_COMPLEX_FOODS,
            TestDbConstants.SCRIPT_TEST_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_MEAL_FOR_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_MEAL_INGREDIENTS_FOR_DAY_FOR_ACTIVE_MEAL_PLAN,
            TestDbConstants.SCRIPT_TEST_MEAL_PLAN_DAY_TO_CALENDAR_DAY_ASSOCIATION
    })
    public void Find_Grocery_List_For_Calendar_Date_Range()
    {
        GroceryListForMealPlanCalendarDateRangeSearchRequest request = new GroceryListForMealPlanCalendarDateRangeSearchRequest();
        // Test meal falls on 2023-01-01
        request.setStartDate(LocalDate.parse("2022-12-20"));
        request.setEndDate(LocalDate.parse("2023-01-01"));

        FindGroceryListForCalendarDateRangeTask sut = new FindGroceryListForCalendarDateRangeTask(mealPlanningRepository, foodRepository, request);

        List<IngredientModel> ingredients = new ArrayList<>();
        try
        {
            sut.run();
            ingredients = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        /*
         * The day contains a meal that contains two complex foods. Each complex food shares one
         * simple food ingredient. This brings us to a total of three distinct ingredients.
         */
        assertThat(ingredients.size()).isEqualByComparingTo(3);
    }
}
