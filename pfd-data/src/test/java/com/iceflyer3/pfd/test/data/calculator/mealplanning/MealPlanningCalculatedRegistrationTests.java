/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


package com.iceflyer3.pfd.test.data.calculator.mealplanning;

import com.iceflyer3.pfd.enums.FitnessGoal;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.registration.MealPlanningCalculatedRegistration;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class MealPlanningCalculatedRegistrationTests
{
    /*
     * Invariants
     *  - Age is required and must be non-zero
     *  - Height is required and must be non-zero
     *  - Weight is required and must be non-zero
     *  - Individual macronutrient percentages must fall in an allowable range
     *    that is determined by the user selected goal
     *  - Macronutrient percentages must collectively add up to 100%
     */

    @Test(dataProvider = "unmetInvariantsDataProvider")
    public void Calculation_Request_With_Unmet_Invariants_Is_Invalid(int age, BigDecimal height, BigDecimal weight, FitnessGoal goal, BigDecimal proteinPercentage, BigDecimal carbsPercentage, BigDecimal fatsPercentage)
    {
        MealPlanningCalculatedRegistration sut = new MealPlanningCalculatedRegistration();

        /*
         * Sex, Activity Level, and BMR Formula don't actually have any impact on the
         * correctness of the request object itself. These values use sensible defaults
         * and are always guaranteed to be set.
         *
         * So we don't need to explicitly set them here.
         */
        sut.setAge(age);
        sut.setHeight(height);
        sut.setWeight(weight);
        sut.setFitnessGoal(goal);
        sut.setProteinPercent(proteinPercentage);
        sut.setCarbsPercent(carbsPercentage);
        sut.setFatsPercent(fatsPercentage);

        assertThat(sut.validate().wasSuccessful()).isFalse();
    }

    @Test(dataProvider = "metInvariantsProvider")
    public void Calculation_Request_With_Met_Invariants_Is_Valid(int age, BigDecimal height, BigDecimal weight, FitnessGoal goal, BigDecimal proteinPercentage, BigDecimal carbsPercentage, BigDecimal fatsPercentage)
    {
        MealPlanningCalculatedRegistration sut = new MealPlanningCalculatedRegistration();

        // These fields do have impact on the validity of the request itself
        sut.setAge(age);
        sut.setHeight(height);
        sut.setWeight(weight);
        sut.setFitnessGoal(goal);
        sut.setProteinPercent(proteinPercentage);
        sut.setCarbsPercent(carbsPercentage);
        sut.setFatsPercent(fatsPercentage);

        assertThat(sut.validate().wasSuccessful()).isTrue();
    }

    @DataProvider(name="unmetInvariantsDataProvider")
    public Object[][] unmetInvariantsDataProvider()
    {
        return new Object[][]{
                // Age tests
                {0, BigDecimal.ONE, BigDecimal.ONE, FitnessGoal.MAINTAIN, BigDecimal.valueOf(25), BigDecimal.valueOf(50), BigDecimal.valueOf(25)},
                // Height tests
                {1, null, BigDecimal.ONE, FitnessGoal.MAINTAIN, BigDecimal.valueOf(25), BigDecimal.valueOf(50), BigDecimal.valueOf(25)},
                {1, BigDecimal.ZERO, BigDecimal.ONE, FitnessGoal.MAINTAIN, BigDecimal.valueOf(25), BigDecimal.valueOf(50), BigDecimal.valueOf(25)},
                // Weight tests
                {1, BigDecimal.ONE, null, FitnessGoal.MAINTAIN, BigDecimal.valueOf(25), BigDecimal.valueOf(50), BigDecimal.valueOf(25)},
                {1, BigDecimal.ONE, BigDecimal.ZERO, FitnessGoal.MAINTAIN, BigDecimal.valueOf(25), BigDecimal.valueOf(50), BigDecimal.valueOf(25)},
                // Maintain goal tests
                {1, BigDecimal.ONE, BigDecimal.ONE, FitnessGoal.MAINTAIN, BigDecimal.valueOf(0), BigDecimal.valueOf(50), BigDecimal.valueOf(25)}, // Invalid range
                {1, BigDecimal.ONE, BigDecimal.ONE, FitnessGoal.MAINTAIN, BigDecimal.valueOf(35), BigDecimal.valueOf(50), BigDecimal.valueOf(25)}, // Valid range - over 100%
                {1, BigDecimal.ONE, BigDecimal.ONE, FitnessGoal.MAINTAIN, BigDecimal.valueOf(25), BigDecimal.valueOf(30), BigDecimal.valueOf(25)}, // Valid range - under 100%
                // Gain goal tests
                {1, BigDecimal.ONE, BigDecimal.ONE, FitnessGoal.GAIN, BigDecimal.valueOf(0), BigDecimal.valueOf(40), BigDecimal.valueOf(15)}, // Invalid range
                {1, BigDecimal.ONE, BigDecimal.ONE, FitnessGoal.GAIN, BigDecimal.valueOf(35), BigDecimal.valueOf(60), BigDecimal.valueOf(25)}, // Valid range - over 100%
                {1, BigDecimal.ONE, BigDecimal.ONE, FitnessGoal.GAIN, BigDecimal.valueOf(25), BigDecimal.valueOf(40), BigDecimal.valueOf(15)}, // Valid range - under 100%
                // Lose goal tests
                {1, BigDecimal.ONE, BigDecimal.ONE, FitnessGoal.GAIN, BigDecimal.valueOf(0), BigDecimal.valueOf(10), BigDecimal.valueOf(30)}, // Invalid range
                {1, BigDecimal.ONE, BigDecimal.ONE, FitnessGoal.GAIN, BigDecimal.valueOf(50), BigDecimal.valueOf(30), BigDecimal.valueOf(40)}, // Valid range - over 100%
                {1, BigDecimal.ONE, BigDecimal.ONE, FitnessGoal.GAIN, BigDecimal.valueOf(40), BigDecimal.valueOf(10), BigDecimal.valueOf(30)} // Valid range - under 100%
        };
    }

    @DataProvider(name="metInvariantsProvider")
    public Object[][] metInvariantsProvider()
    {
        return new Object[][]{
                {1, BigDecimal.ONE, BigDecimal.ONE, FitnessGoal.MAINTAIN, BigDecimal.valueOf(25), BigDecimal.valueOf(50), BigDecimal.valueOf(25)},
                {1, BigDecimal.ONE, BigDecimal.ONE, FitnessGoal.GAIN, BigDecimal.valueOf(25), BigDecimal.valueOf(60), BigDecimal.valueOf(15)},
                {1, BigDecimal.ONE, BigDecimal.ONE, FitnessGoal.LOSE, BigDecimal.valueOf(50), BigDecimal.valueOf(20), BigDecimal.valueOf(30)}
        };
    }
}
