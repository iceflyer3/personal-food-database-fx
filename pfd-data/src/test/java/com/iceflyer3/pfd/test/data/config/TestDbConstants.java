/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.test.data.config;

public class TestDbConstants
{
    private TestDbConstants() {}

    ///////////////////////////////////////////////////////////////////
    //
    //                CREATION SCRIPTS LOCATIONS / NAMES
    //
    ///////////////////////////////////////////////////////////////////

    // Foods
    public static final String SCRIPT_TEST_SIMPLE_FOODS = "classpath:scripts/food/create_test_simple_foods.sql";
    public static final String SCRIPT_TEST_COMPLEX_FOODS = "classpath:scripts/food/create_test_complex_foods.sql";

    // Meal Plans
    public static final String SCRIPT_TEST_ACTIVE_MEAL_PLAN = "classpath:scripts/meal-planning/create_test_active_meal_plan.sql";
    public static final String SCRIPT_TEST_ACTIVE_MEAL_PLAN_WITH_OPTIONAL_DATA = "classpath:scripts/meal-planning/create_test_active_meal_plan_with_custom_nutrients.sql";
    public static final String SCRIPT_TEST_INACTIVE_MEAL_PLAN = "classpath:scripts/meal-planning/create_test_inactive_meal_plan.sql";

    public static final String SCRIPT_TEST_DAY_FOR_ACTIVE_MEAL_PLAN = "classpath:scripts/meal-planning/create_test_day_for_active_meal_plan.sql";

    public static final String SCRIPT_TEST_MEAL_FOR_DAY_FOR_ACTIVE_MEAL_PLAN = "classpath:scripts/meal-planning/create_test_meal_for_active_meal_plan.sql";
    public static final String SCRIPT_TEST_MEAL_INGREDIENTS_FOR_DAY_FOR_ACTIVE_MEAL_PLAN = "classpath:scripts/meal-planning/create_test_meal_ingredients_for_active_meal_plan.sql";

    // Carb Cycling Plans
    public static final String SCRIPT_TEST_CARB_CYCLING_PLAN = "classpath:scripts/meal-planning/carb-cycling/create_test_carb_cycling_plan.sql";
    public static final String SCRIPT_TEST_CARB_CYCLING_PLAN_MEAL_CONFIGURATIONS = "classpath:scripts/meal-planning/carb-cycling/create_test_carb_cycling_plan_meal_configurations.sql";

    public static final String SCRIPT_TEST_CARB_CYCLING_NON_TRAINING_DAY = "classpath:scripts/meal-planning/carb-cycling/create_test_non_training_day_for_carb_cycling_plan.sql";

    public static final String SCRIPT_TEST_CARB_CYCLING_TRAINING_DAY = "classpath:scripts/meal-planning/carb-cycling/create_test_training_day_for_carb_cycling_plan.sql";
    public static final String SCRIPT_TEST_CARB_CYCLING_MEAL_FOR_TRAINING_DAY = "classpath:scripts/meal-planning/carb-cycling/create_test_training_day_meal_for_carb_cycling_plan.sql";
    public static final String SCRIPT_TEST_CARB_CYCLING_MEAL_INGREDIENTS_FOR_TRAINING_DAY = "classpath:scripts/meal-planning/carb-cycling/create_test_training_day_meal_ingredients_for_carb_cycling_plan.sql";

    // Meal Planning Calendar
    public static final String SCRIPT_TEST_MEAL_PLAN_DAY_TO_CALENDAR_DAY_ASSOCIATION = "classpath:scripts/meal-planning/calendar/create_test_meal_plan_day_to_calendar_day_association.sql";
    public static final String SCRIPT_TEST_CARB_CYCLING_PLAN_DAY_TO_CALENDAR_DAY_ASSOCIATION = "classpath:scripts/meal-planning/calendar/create_test_carb_cycling_plan_day_to_calendar_day_association.sql";

    // Recipes
    public static final String SCRIPT_TEST_RECIPE = "classpath:scripts/recipe/create_test_recipe.sql";
    public static final String SCRIPT_TEST_RECIPE_INGREDIENTS = "classpath:scripts/recipe/create_test_recipe_ingredients.sql";
    public static final String SCRIPT_TEST_RECIPE_STEPS = "classpath:scripts/recipe/create_test_recipe_steps.sql";

    ///////////////////////////////////////////////////////////////////
    //
    //                          TABLE NAMES
    //
    ///////////////////////////////////////////////////////////////////
    public static final String TABLE_FOOD_META_DATA = "PFD_V1.FOOD_META_DATA";
    public static final String TABLE_FOOD_SERVING_SIZE = "PFD_V1.FOOD_SERVING_SIZE";
    public static final String TABLE_SIMPLE_FOOD = "PFD_V1.SIMPLE_FOOD";
    public static final String TABLE_COMPLEX_FOOD = "PFD_V1.COMPLEX_FOOD";
    public static final String TABLE_COMPLEX_FOOD_INGREDIENT = "PFD_V1.COMPLEX_FOOD_INGREDIENT";

    public static final String TABLE_MEAL_PLAN = "PFD_V1.MEAL_PLAN";
    public static final String TABLE_MEAL_PLAN_CALENDAR_DAY = "PFD_V1.MEAL_PLAN_CALENDAR_DAY";
    public static final String TABLE_MEAL_PLAN_NUTRIENT_SUGGESTION = "PFD_V1.MEAL_PLAN_NUTRIENT_SUGGESTION";
    public static final String TABLE_MEAL_PLANNING_DAY = "PFD_V1.MEAL_PLANNING_DAY";
    public static final String TABLE_MEAL_PLANNING_MEAL = "PFD_V1.MEAL_PLANNING_MEAL";
    public static final String TABLE_MEAL_PLANNING_MEAL_INGREDIENT = "PFD_V1.MEAL_PLANNING_MEAL_INGREDIENT";


    public static final String TABLE_CARB_CYCLING_PLAN = "PFD_V1.CARB_CYCLING_PLAN";
    public static final String TABLE_CARB_CYCLING_DAY = "PFD_V1.CARB_CYCLING_DAY";
    public static final String TABLE_CARB_CYCLING_MEAL = "PFD_V1.CARB_CYCLING_MEAL";
    public static final String TABLE_CARB_CYCLING_MEAL_CONFIGURATION = "PFD_V1.CARB_CYCLING_MEAL_CONFIGURATION";

    public static final String TABLE_RECIPE = "PFD_V1.RECIPE";
    public static final String TABLE_RECIPE_INGREDIENT = "PFD_V1.RECIPE_INGREDIENT";
    public static final String TABLE_RECIPE_SERVING_SIZE = "PFD_V1.RECIPE_SERVING_SIZE";
    public static final String TABLE_RECIPE_STEP = "PFD_V1.RECIPE_STEP";
    public static final String TABLE_RECIPE_STEP_INGREDIENT = "PFD_V1.RECIPE_STEP_INGREDIENT";

    ///////////////////////////////////////////////////////////////////
    //
    //                          TEST ENTITY IDS
    //
    ///////////////////////////////////////////////////////////////////

    // User
    public static final String USER_ID = "0b185cf5-47ab-486b-914b-2851c16a7bac";
    public static final String USER_USERNAME = "UnitTestUser";

    // Foods
    public static final String SIMPLE_FOOD_ONE_ID = "f0c183ed-79cf-43ce-a86d-f6fe1efa3a1f";
    public static final String SIMPLE_FOOD_TWO_ID = "d9aa90da-7e27-4ce9-a869-b49a4c402876";
    public static final String SIMPLE_FOOD_THREE_ID = "57dfe6e3-d6c0-46f9-a3e5-ba08d9ea03d3";

    public static final String COMPLEX_FOOD_ONE_ID = "d78b2a7d-f62f-4ec7-aecc-12bb76c047ee";
    public static final String COMPLEX_FOOD_TWO_ID = "bc0cea4d-977d-4b15-9e38-e311dbb531f5";

    // Meal Plans
    public static final String MEAL_PLAN_ACTIVE_ID = "16171b52-94f7-4ab1-85c4-917c0f95121c";
    public static final String MEAL_PLAN_INACTIVE_ID = "24f4e5a4-b7ac-4bee-b110-77800382cd0a";

    public static final String ACTIVE_MEAL_PLAN_DAY_ONE_ID = "aa67f942-412c-40bb-a925-8abca281ee32";

    public static final String ACTIVE_MEAL_PLAN_DAY_ONE_MEAL_ONE_ID = "8b170828-2b1b-4bb8-bbf5-ba9cb109d763";

    // Carb Cycling Plans
    public static final String CARB_CYCLING_PLAN_ID = "78a3e26a-75b4-49eb-9031-e3589331c91d";

    public static final String CARB_CYCLING_PLAN_NON_TRAINING_DAY_ID = "aebd2773-a1c4-48e8-8e6d-c7397618c4c8";

    public static final String CARB_CYCLING_PLAN_TRAINING_DAY_ID = "56d6d3f3-e41b-4088-831b-f07c5aa4e577";
    public static final String CARB_CYCLING_PLAN_TRAINING_DAY_MEAL_ID = "d344812e-2fc9-4218-b497-de6bbe82e09b";

    // Meal Planning Calendar
    public static final String MEAL_PLAN_CALENDAR_MEAL_PLAN_DAY_ASSOCIATION_ID = "b72706df-1a57-4076-8fea-25547f0af70c";
    public static final String MEAL_PLAN_CALENDAR_CARB_CYCLING_PLAN_DAY_ASSOCIATION_ID = "1981a6fc-8dfe-4507-b380-bba5407fbc7c";

    // Recipes
    public static final String RECIPE_ID = "c2e39b67-6ac9-4d15-8c63-daac5d197794";
}
