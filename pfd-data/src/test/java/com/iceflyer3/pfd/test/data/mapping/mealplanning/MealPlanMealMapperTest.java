package com.iceflyer3.pfd.test.data.mapping.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.test.data.TestDataProvider;
import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanMeal;
import com.iceflyer3.pfd.data.mapping.mealplanning.MealPlanMealMapper;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanMealViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class MealPlanMealMapperTest
{
    private final TestDataProvider testDataProvider = new TestDataProvider();

    @Test
    public void Map_Meal_Plan_Meal()
    {
        // Arrange
        // We could put this in the TestDataProvider. But it is a one liner and only needed
        // here and in the Meal Plan Day Mapper test. So, really, why bother.
        MealPlanMeal mealEntity = testDataProvider.getTestMealPlanningMealEntity();

        // Act
        MealPlanMealViewModel mappedViewModel = MealPlanMealMapper.toModel(mealEntity, NutritionalSummary.empty());

        // Assert
        assertThat(mealEntity.getMealName()).isEqualTo(mappedViewModel.getName());
    }
}
