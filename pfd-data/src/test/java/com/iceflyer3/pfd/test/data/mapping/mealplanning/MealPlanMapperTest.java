package com.iceflyer3.pfd.test.data.mapping.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023, 2025 Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.entities.mealplanning.MealPlan;
import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanNutrientSuggestion;
import com.iceflyer3.pfd.data.mapping.mealplanning.MealPlanMapper;
import com.iceflyer3.pfd.enums.Nutrient;
import com.iceflyer3.pfd.enums.UnitOfMeasure;
import com.iceflyer3.pfd.test.data.TestDataProvider;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanViewModel;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class MealPlanMapperTest
{
    private final TestDataProvider testDataProvider = new TestDataProvider();

    @Test
    public void Map_Inactive_Meal_Plan()
    {
        // Arrange
        MealPlan mealPlanEntity = testDataProvider.getTestMealPlanEntity();

        // Act
        MealPlanViewModel mappedViewModel = MealPlanMapper.toModel(mealPlanEntity);

        // Assert
        assertThat(mealPlanEntity)
                .usingRecursiveComparison()
                .ignoringFields("mealPlanId", "nutrientSuggestions", "user", "isActive")
                .withEqualsForFields(testDataProvider.getEntityToViewModelUserComparisonPredicate(), "createdUser")
                .withEqualsForFields(testDataProvider.getEntityToViewModelUserComparisonPredicate(), "lastModifiedUser")
                .isEqualTo(mappedViewModel);

        assertThat(mealPlanEntity.getIsActive()).isEqualTo(mappedViewModel.isActive());

        /*
         * Nutrient suggestions have their own mapper with their own tests. The content of the nutrient suggestions
         * collection will be verified by these tests.
         *
         * As such, the content of the collection does not need to be checked here. Simply checking the size of the
         * collections match is sufficient as a sanity check here.
         */
        assertThat(mealPlanEntity.getNutrientSuggestions().size())
                .isEqualTo(mappedViewModel.getNutrientSuggestions().size());
    }

    @Test
    public void Map_Active_Meal_Plan()
    {
        // Arrange
        MealPlan mealPlanEntity = testDataProvider.getTestMealPlanEntity();
        mealPlanEntity.setIsActive(true);

        // Act
        MealPlanViewModel mappedViewModel = MealPlanMapper.toModel(mealPlanEntity);

        // Assert
        assertThat(mealPlanEntity)
                .usingRecursiveComparison()
                .ignoringFields("mealPlanId", "nutrientSuggestions", "user", "isActive")
                .withEqualsForFields(testDataProvider.getEntityToViewModelUserComparisonPredicate(), "createdUser")
                .withEqualsForFields(testDataProvider.getEntityToViewModelUserComparisonPredicate(), "lastModifiedUser")
                .isEqualTo(mappedViewModel);

        assertThat(mealPlanEntity.getIsActive()).isEqualTo(mappedViewModel.isActive());

        assertThat(mealPlanEntity.getNutrientSuggestions().size())
                .isEqualTo(mappedViewModel.getNutrientSuggestions().size());
    }

    /**
     * Tests that any optional data (e.g. custom nutrient requirements) are mapped (when present) alongside the required
     * data of the meal plan.
     */
    @Test
    public void Map_Meal_Plan_With_Optional_Data()
    {
        // Arrange
        MealPlan mealPlanEntity = testDataProvider.getTestMealPlanEntity();
        mealPlanEntity.setIsActive(true);

        Collection<MealPlanNutrientSuggestion> customNutrients = Set.of(
                new MealPlanNutrientSuggestion(mealPlanEntity, Nutrient.CALORIES, BigDecimal.ONE, UnitOfMeasure.CALORIE),
                new MealPlanNutrientSuggestion(mealPlanEntity, Nutrient.PROTEIN, BigDecimal.ONE, UnitOfMeasure.GRAM),
                new MealPlanNutrientSuggestion(mealPlanEntity, Nutrient.CARBOHYDRATES, BigDecimal.ONE, UnitOfMeasure.GRAM),
                new MealPlanNutrientSuggestion(mealPlanEntity, Nutrient.TOTAL_FATS, BigDecimal.ONE, UnitOfMeasure.GRAM),
                new MealPlanNutrientSuggestion(mealPlanEntity, Nutrient.FIBER, BigDecimal.ONE, UnitOfMeasure.GRAM),
                new MealPlanNutrientSuggestion(mealPlanEntity, Nutrient.VITAMIN_A, BigDecimal.ONE, UnitOfMeasure.MICROGRAM)
        );
        mealPlanEntity.getNutrientSuggestions().addAll(customNutrients);

        // Act
        MealPlanViewModel mappedViewModel = MealPlanMapper.toModel(mealPlanEntity);

        // Assert
        assertThat(mealPlanEntity)
                .usingRecursiveComparison()
                .ignoringFields("mealPlanId", "nutrientSuggestions", "user", "isActive")
                .withEqualsForFields(testDataProvider.getEntityToViewModelUserComparisonPredicate(), "createdUser", "lastModifiedUser")
                .isEqualTo(mappedViewModel);

        assertThat(mealPlanEntity.getIsActive()).isEqualTo(mappedViewModel.isActive());

        assertThat(mappedViewModel.getNutrientSuggestions().size())
                .isEqualTo(customNutrients.size());
    }
}
