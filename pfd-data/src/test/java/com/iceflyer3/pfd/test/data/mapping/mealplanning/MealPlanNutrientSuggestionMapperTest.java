/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.test.data.mapping.mealplanning;

import com.iceflyer3.pfd.data.entities.mealplanning.MealPlan;
import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanNutrientSuggestion;
import com.iceflyer3.pfd.data.mapping.mealplanning.MealPlanNutrientSuggestionMapper;
import com.iceflyer3.pfd.enums.Nutrient;
import com.iceflyer3.pfd.enums.UnitOfMeasure;
import com.iceflyer3.pfd.test.data.TestDataProvider;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanNutrientSuggestionViewModel;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class MealPlanNutrientSuggestionMapperTest
{
    private final TestDataProvider testDataProvider = new TestDataProvider();

    @Test
    public void Map_Meal_Plan_Macro_Suggestion()
    {
        // Arrange
        MealPlan mealPlanEntity = testDataProvider.getTestMealPlanEntity();
        MealPlanNutrientSuggestion macroSuggestionEntity = new MealPlanNutrientSuggestion(
                mealPlanEntity,
                Nutrient.CALORIES,
                BigDecimal.TEN,
                UnitOfMeasure.CALORIE);

        // Act
        MealPlanNutrientSuggestionViewModel mappedViewModel = MealPlanNutrientSuggestionMapper.toModel(macroSuggestionEntity);

        // Assert
        assertThat(macroSuggestionEntity.getNutrient()).isEqualByComparingTo(mappedViewModel.getNutrient());
        assertThat(macroSuggestionEntity.getSuggestedAmount()).isEqualByComparingTo(mappedViewModel.getSuggestedAmount());
        assertThat(macroSuggestionEntity.getUnitOfMeasure()).isEqualByComparingTo(mappedViewModel.getUnitOfMeasure());
    }
}
