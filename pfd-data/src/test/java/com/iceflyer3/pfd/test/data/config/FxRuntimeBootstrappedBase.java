/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.test.data.config;

import com.iceflyer3.pfd.user.AuthenticatedUser;
import com.iceflyer3.pfd.user.UserAuthentication;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import java.util.UUID;

/**
 * Ensures that JavaFx gets started and stopped for testing of objects which require JavaFx to
 * have been initialized.
 *
 * This is accomplished by creating a dummy JavaFx "Application" subclass and calling the static
 * launch method (in the same manner as you would to launch a normal JavaFx application via the
 * static main method).
 *
 * The dummy application will be launched before tests run and will be stopped after all tests
 * in the suite have completed.
 */
public class FxRuntimeBootstrappedBase extends AbstractTransactionalTestNGSpringContextTests
{
    @BeforeSuite
    public void beforeTestSuite()
    {
        UserAuthentication.instance().setAuthenticatedUser(new AuthenticatedUser(UUID.fromString("0b185cf5-47ab-486b-914b-2851c16a7bac"), "UnitTestUser"));
        FxUnitTestApplication.launchApplication();
    }

    @AfterSuite
    public void afterTestSuite()
    {
        UserAuthentication.instance().clearAuthenticatedUser();
        FxUnitTestApplication.stopApplication();
    }
}
