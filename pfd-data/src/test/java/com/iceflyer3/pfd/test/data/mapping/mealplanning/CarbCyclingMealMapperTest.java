package com.iceflyer3.pfd.test.data.mapping.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.test.data.TestDataProvider;
import com.iceflyer3.pfd.data.entities.mealplanning.carbcycling.CarbCyclingMeal;
import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanDay;
import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanMeal;
import com.iceflyer3.pfd.enums.MealType;
import com.iceflyer3.pfd.data.mapping.mealplanning.carbcycling.CarbCyclingMealMapper;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingMealViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class CarbCyclingMealMapperTest
{
    private final TestDataProvider testDataProvider = new TestDataProvider();

    @Test
    public void Map_Carb_Cycling_Meal()
    {
        // Arrange
        MealPlanMeal mealEntity = testDataProvider.getTestMealPlanningMealEntity();
        MealPlanDay carbCyclingDay = testDataProvider.getTestMealPlanningDayEntity();
        CarbCyclingMeal carbCyclingMealEntity = new CarbCyclingMeal(mealEntity, carbCyclingDay.getCarbCyclingDay());
        carbCyclingMealEntity.setMealType(MealType.REGULAR);
        carbCyclingMealEntity.setMealNumber(1);
        carbCyclingMealEntity.setCarbConsumptionPercentage(BigDecimal.TEN);
        carbCyclingMealEntity.setRequiredCalories(BigDecimal.TEN);
        carbCyclingMealEntity.setRequiredProtein(BigDecimal.TEN);
        carbCyclingMealEntity.setRequiredCarbs(BigDecimal.TEN);
        carbCyclingMealEntity.setRequiredFats(BigDecimal.TEN);

        mealEntity.setCarbCyclingMeal(carbCyclingMealEntity);

        // Act
        CarbCyclingMealViewModel mappedViewModel = CarbCyclingMealMapper.toModel(mealEntity, NutritionalSummary.empty());

        // Assert
        assertThat(carbCyclingMealEntity.getMealType()).isEqualByComparingTo(mappedViewModel.getMealType());
        assertThat(carbCyclingMealEntity.getMealNumber()).isEqualTo(mappedViewModel.getMealNumber());
        assertThat(carbCyclingMealEntity.getCarbConsumptionPercentage()).isEqualByComparingTo(mappedViewModel.getCarbConsumptionPercentage());
        assertThat(carbCyclingMealEntity.getRequiredCalories()).isEqualByComparingTo(mappedViewModel.getRequiredCalories());
        assertThat(carbCyclingMealEntity.getRequiredProtein()).isEqualByComparingTo(mappedViewModel.getRequiredProtein());
        assertThat(carbCyclingMealEntity.getRequiredCarbs()).isEqualByComparingTo(mappedViewModel.getRequiredCarbs());
        assertThat(carbCyclingMealEntity.getRequiredFats()).isEqualByComparingTo(mappedViewModel.getRequiredFats());
    }
}
