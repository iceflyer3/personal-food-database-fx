/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.test.data.task;

import com.iceflyer3.pfd.test.data.config.FxTaskTest;
import com.iceflyer3.pfd.test.data.config.TestDbConstants;
import com.iceflyer3.pfd.test.data.config.FxRuntimeBootstrappedBase;
import com.iceflyer3.pfd.data.entities.User;
import com.iceflyer3.pfd.data.repository.UserRepository;
import com.iceflyer3.pfd.data.task.user.AddUserTask;
import com.iceflyer3.pfd.data.task.user.GetAllUsersTask;
import com.iceflyer3.pfd.data.task.user.GetUserByUsernameTask;
import com.iceflyer3.pfd.exception.InvalidApplicationStateException;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import javafx.collections.FXCollections;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static org.assertj.core.api.Assertions.assertThat;

@FxTaskTest
public class UserTasksTests extends FxRuntimeBootstrappedBase
{
    @Autowired
    private UserRepository userRepository;

    /**
     * Tests that a new user may be created.
     */
    @Test
    public void Create_New_User()
    {
        // Arrange
        String userName = "UnitTestUser1";
        UserViewModel newUser = new UserViewModel(null, userName);
        AddUserTask task = new AddUserTask(userRepository, newUser);

        // Act
        UserViewModel newUserViewModel = null;
        try
        {
            task.run();
            newUserViewModel = task.get();
        }
        catch (ExecutionException | InterruptedException e)
        {
            e.printStackTrace();
        }

        List<User> allUsers = userRepository.getAll();

        // Assert
        assertThat(allUsers.size()).isEqualTo(2);
        assertThat(newUserViewModel).isNotNull();
        assertThat(newUserViewModel.getUserId()).isNotNull();
        assertThat(newUserViewModel.getUsername()).isEqualTo(userName);
    }

    /**
     * Tests that the list of all created users may be retrieved
     */
    @Test
    public void Can_Retrieve_All_Users()
    {
        // Arrange
        // Insert test users
        UserViewModel user1 = new UserViewModel(null, "UnitTest1");
        UserViewModel user2 = new UserViewModel(null, "UnitTest2");
        UserViewModel user3 = new UserViewModel(null, "UnitTest3");
        List.of(user1, user2, user3).forEach(user -> userRepository.add(user));

        GetAllUsersTask getAllUsersTask = new GetAllUsersTask(userRepository);

        // Act
        List<UserViewModel> allUsers = FXCollections.observableArrayList();
        try
        {
            getAllUsersTask.run();
            allUsers = getAllUsersTask.get();
        }
        catch (ExecutionException | InterruptedException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(allUsers.size()).isEqualTo(4);
    }

    /**
     * Tests that a previously created user may be retrieved by using only their username
     */
    @Test
    public void Can_Retrieve_Extant_User_By_Username()
    {
        // Arrange
        GetUserByUsernameTask getUserByUsernameTask = new GetUserByUsernameTask(userRepository, TestDbConstants.USER_USERNAME);

        // Act
        UserViewModel retrievedUser = null;
        try
        {
            getUserByUsernameTask.run();
            retrievedUser = getUserByUsernameTask.get();
        }
        catch (ExecutionException | InterruptedException e)
        {
            e.printStackTrace();
        }

        // Assert
        UUID userId = UUID.fromString(TestDbConstants.USER_ID);
        assertThat(retrievedUser).isNotNull();
        assertThat(retrievedUser.getUserId()).isEqualByComparingTo(userId);
        assertThat(retrievedUser.getUsername()).isEqualTo(TestDbConstants.USER_USERNAME);
    }

    /**
     * Tests that an InvalidApplicationStateException is thrown when attempting to retrieve a user
     * for a username which does not yet exist.
     */
    @Test
    public void Cannot_Retrieve_NonExistent_User_By_Username()
    {
        // Arrange
        String userName = "UnitTestUser";
        GetUserByUsernameTask getUserByUsernameTask = new GetUserByUsernameTask( userRepository, userName);

        // Act
        try
        {
            getUserByUsernameTask.run();
            getUserByUsernameTask.get();
        }
        catch (ExecutionException | InterruptedException e)
        {
            // Assert
            assertThat(e.getCause()).isInstanceOf(InvalidApplicationStateException.class);
            assertThat(e.getCause().getMessage()).contains(userName);
        }
    }
}
