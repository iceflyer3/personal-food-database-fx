/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.test.data.task;

import com.iceflyer3.pfd.test.data.config.FxTaskTest;
import com.iceflyer3.pfd.test.data.config.FxRuntimeBootstrappedBase;
import com.iceflyer3.pfd.test.data.config.TestDbConstants;
import com.iceflyer3.pfd.data.entities.food.ComplexFood;
import com.iceflyer3.pfd.data.entities.recipe.Recipe;
import com.iceflyer3.pfd.data.entities.recipe.RecipeIngredient;
import com.iceflyer3.pfd.data.entities.recipe.RecipeStep;
import com.iceflyer3.pfd.data.entities.views.RecipeNutritionalSummary;
import com.iceflyer3.pfd.data.mapping.helper.RecipeMapperHelper;
import com.iceflyer3.pfd.data.mapping.UserMapper;
import com.iceflyer3.pfd.data.mapping.food.FoodMapper;
import com.iceflyer3.pfd.data.mapping.recipe.RecipeMapper;
import com.iceflyer3.pfd.data.mapping.summary.NutritionalSummaryMapper;
import com.iceflyer3.pfd.data.repository.FoodRepository;
import com.iceflyer3.pfd.data.repository.RecipeRepository;
import com.iceflyer3.pfd.data.repository.UserRepository;
import com.iceflyer3.pfd.data.task.recipe.*;
import com.iceflyer3.pfd.exception.InvalidApplicationStateException;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.api.recipe.RecipeStepModel;
import com.iceflyer3.pfd.ui.model.viewmodel.IngredientViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.ReadOnlyFoodViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.recipe.RecipeStepViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.recipe.RecipeViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static org.assertj.core.api.Assertions.assertThat;

@FxTaskTest
public class RecipeTasksTests extends FxRuntimeBootstrappedBase
{
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FoodRepository foodRepository;

    @Autowired
    private RecipeRepository recipeRepository;

    private RecipeMapperHelper recipeMapperHelper;

    @BeforeClass
    private void init()
    {
        /*
         * Unfortunately Spring doesn't support constructor injection unless we're using JUnit 5.
         * Due to this we can't create the RecipeMapperHelper instance in the constructor because
         * we won't yet have its dependency of the FoodRepository.
         *
         * So this is a simple workaround
         */
        recipeMapperHelper = new RecipeMapperHelper(foodRepository);
    }

    /**
     * Tests that all recipes for a user may be retrieved. Recipe ingredients and steps should
     * not be loaded as part of this operation.
     */
    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS,
            TestDbConstants.SCRIPT_TEST_RECIPE,
            TestDbConstants.SCRIPT_TEST_RECIPE_INGREDIENTS,
            TestDbConstants.SCRIPT_TEST_RECIPE_STEPS
    })
    public void Find_Recipes_For_User()
    {
        // Arrange
        UUID userId = UUID.fromString(TestDbConstants.USER_ID);
        GetAllRecipesForUserTask sut = new GetAllRecipesForUserTask(recipeRepository, foodRepository, userId);
        List<RecipeViewModel> retrievedRecipes = new ArrayList<>();

        // Act
        try
        {
            sut.run();
            retrievedRecipes = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(retrievedRecipes.size()).isEqualTo(1);

        RecipeViewModel firstRecipe = retrievedRecipes.get(0);
        assertThat(firstRecipe.getIngredients().size()).isEqualTo(0);
        assertThat(firstRecipe.getSteps().size()).isEqualTo(0);
        assertThat(firstRecipe.getServingSizes().size()).isEqualTo(1);
    }

    /**
     * Tests that we may retrieve a mutable view model for a recipe. Recipe ingredients and steps
     * should be loaded as part of this operation.
     */
    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS,
            TestDbConstants.SCRIPT_TEST_RECIPE,
            TestDbConstants.SCRIPT_TEST_RECIPE_INGREDIENTS,
            TestDbConstants.SCRIPT_TEST_RECIPE_STEPS
    })
    public void Find_Extant_Mutable_Recipe_By_Id()
    {
        // Arrange
        UUID recipeId = UUID.fromString(TestDbConstants.RECIPE_ID);
        GetMutableRecipeByIdTask sut = new GetMutableRecipeByIdTask(recipeRepository, foodRepository, recipeId);
        RecipeViewModel retrievedRecipe = null;

        // Act
        try
        {
            sut.run();
            retrievedRecipe = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(retrievedRecipe).isNotNull();
        assertThat(retrievedRecipe.getServingSizes().size()).isEqualTo(1);
        assertThat(retrievedRecipe.getIngredients().size()).isEqualTo(0);
        assertThat(retrievedRecipe.getSteps().size()).isEqualTo(0);
    }

    /**
     * Tests that a search for an immutable recipe with an id that does not yet exist will fail.
     */
    @Test
    public void Find_NonExistent_Mutable_Recipe_By_Id()
    {
        // Arrange
        UUID recipeId = UUID.fromString("df451622-8ad9-460a-88df-ac38ca334a46");
        GetMutableRecipeByIdTask sut = new GetMutableRecipeByIdTask(recipeRepository, foodRepository, recipeId);

        // Act
        try
        {
            sut.run();
            sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            // Assert
            e.printStackTrace();
            assertThat(e.getCause()).isInstanceOf(InvalidApplicationStateException.class);
            assertThat(e.getCause().getMessage()).contains(recipeId.toString());
        }
    }

    /**
     * Tests that we may retrieve an immutable view model for a recipe. Recipe ingredients and steps
     * should be loaded as part of this operation.
     */
    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS,
            TestDbConstants.SCRIPT_TEST_RECIPE,
            TestDbConstants.SCRIPT_TEST_RECIPE_INGREDIENTS,
            TestDbConstants.SCRIPT_TEST_RECIPE_STEPS
    })
    public void Find_Extant_Immutable_Recipe_By_Id()
    {
        // Arrange
        UUID recipeId = UUID.fromString(TestDbConstants.RECIPE_ID);
        GetReadOnlyRecipeByIdTask sut = new GetReadOnlyRecipeByIdTask(recipeRepository, foodRepository, recipeId);
        RecipeViewModel retrievedRecipe = null;

        // Act
        try
        {
            sut.run();
            retrievedRecipe = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(retrievedRecipe).isNotNull();
        assertThat(retrievedRecipe.getServingSizes().size()).isEqualTo(1);
        assertThat(retrievedRecipe.getIngredients().size()).isEqualTo(0);
        assertThat(retrievedRecipe.getSteps().size()).isEqualTo(0);
    }

    /**
     * Tests that a search for an immutable recipe with an id that does not yet exist will fail.
     */
    @Test
    public void Find_NonExistent_Immutable_Recipe_By_Id()
    {
        // Arrange
        UUID recipeId = UUID.fromString("df451622-8ad9-460a-88df-ac38ca334a46");
        GetReadOnlyRecipeByIdTask sut = new GetReadOnlyRecipeByIdTask(recipeRepository, foodRepository, recipeId);

        // Act
        try
        {
            sut.run();
            sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            // Assert
            e.printStackTrace();
            assertThat(e.getCause()).isInstanceOf(InvalidApplicationStateException.class);
            assertThat(e.getCause().getMessage()).contains(recipeId.toString());
        }
    }

    /**
     * Tests that a new recipe may be saved
     */
    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS
    })
    public void Create_New_Recipe()
    {
        // Arrange
        UserViewModel userViewModel = UserMapper.toModel(userRepository.getByUsername(TestDbConstants.USER_USERNAME));

        // Create new recipe view model
        RecipeViewModel newRecipe = new RecipeViewModel(userViewModel.getUsername());
        newRecipe.setName("Unit Test Recipe");
        newRecipe.setSource("Unit Test Source");
        newRecipe.setNumberOfServingsMade(BigDecimal.ONE);
        newRecipe.addServingSize();

        // Create a step
        RecipeStepViewModel recipeStep = new RecipeStepViewModel(1);
        recipeStep.setStepName("Step One");
        recipeStep.setDirections("Step one test directions");

        // Add an ingredient to the recipe
        UUID ingredientFoodId = UUID.fromString(TestDbConstants.SIMPLE_FOOD_ONE_ID);
        ReadOnlyFoodViewModel newIngredientFood = foodRepository.getFoodForId(ingredientFoodId);
        IngredientViewModel newIngredient = new IngredientViewModel(newIngredientFood);
        newRecipe.addIngredient(newIngredientFood);

        // Add an ingredient to the step and then add the step to the recipe
        recipeStep.getIngredients().add(newIngredient);
        newRecipe.setSteps(Set.of(recipeStep));

        SaveRecipeTask sut = new SaveRecipeTask(recipeRepository, foodRepository, userViewModel, newRecipe);
        RecipeViewModel savedRecipe = null;

        // Act
        try
        {
            sut.run();
            savedRecipe = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(savedRecipe).isNotNull();
        assertThat(savedRecipe.getId()).isNotNull();
        assertThat(savedRecipe.getServingSizes().size()).isEqualTo(1);
        assertThat(savedRecipe.getIngredients().size()).isEqualTo(1);
        assertThat(savedRecipe.getSteps().size()).isEqualTo(1);

        RecipeStepModel firstStep = savedRecipe.getSteps().get(0);
        assertThat(firstStep.getIngredients().size()).isEqualTo(1);
    }

    /**
     * Tests that part of an existing recipe may be updated
     */
    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS,
            TestDbConstants.SCRIPT_TEST_RECIPE,
            TestDbConstants.SCRIPT_TEST_RECIPE_INGREDIENTS,
            TestDbConstants.SCRIPT_TEST_RECIPE_STEPS
    })
    public void Update_Existing_Recipe()
    {
        // Arrange
        UserViewModel userViewModel = UserMapper.toModel(userRepository.getByUsername(TestDbConstants.USER_USERNAME));

        // Fully hydrate our test existing recipe
        UUID recipeId = UUID.fromString(TestDbConstants.RECIPE_ID);

        Recipe recipeEntity = recipeRepository.getRecipeById(recipeId);
        RecipeNutritionalSummary recipeNutritionalSummaryEntity = recipeRepository.getNutritionalSummaryById(recipeId);
        NutritionalSummary nutritionalSummaryViewModel = NutritionalSummaryMapper.toModel(recipeNutritionalSummaryEntity);
        RecipeViewModel recipeViewModel = RecipeMapper.toModel(recipeEntity, nutritionalSummaryViewModel);
        populateIngredientsAndStepsForRecipe(recipeViewModel);

        String newRecipeName = "Updated Recipe Test Name";
        recipeViewModel.setName(newRecipeName);

        SaveRecipeTask sut = new SaveRecipeTask(recipeRepository, foodRepository, userViewModel, recipeViewModel);
        RecipeViewModel savedRecipe = null;

        // Act
        try
        {
            sut.run();
            savedRecipe = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(savedRecipe).isNotNull();
        assertThat(savedRecipe.getName()).isEqualTo(newRecipeName);
    }

    /**
     * Tests that an existing recipe may be deleted
     */
    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS,
            TestDbConstants.SCRIPT_TEST_RECIPE,
            TestDbConstants.SCRIPT_TEST_RECIPE_INGREDIENTS,
            TestDbConstants.SCRIPT_TEST_RECIPE_STEPS
    })
    public void Delete_Recipe()
    {
        // Arrange
        UUID recipeId = UUID.fromString(TestDbConstants.RECIPE_ID);
        DeleteRecipeTask sut = new DeleteRecipeTask(recipeRepository, foodRepository, recipeId);

        // Act
        try
        {
            sut.run();
            sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        List<Recipe> allRecipes = recipeRepository.getAllRecipes();

        // Assert
        assertThat(allRecipes.size()).isEqualTo(0);
    }

    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS,
            TestDbConstants.SCRIPT_TEST_COMPLEX_FOODS,
            TestDbConstants.SCRIPT_TEST_RECIPE,
            TestDbConstants.SCRIPT_TEST_RECIPE_INGREDIENTS,
            TestDbConstants.SCRIPT_TEST_RECIPE_STEPS
    })
    public void Add_Ingredient_To_Recipe()
    {
        // Arrange
        UserViewModel userViewModel = UserMapper.toModel(userRepository.getByUsername(TestDbConstants.USER_USERNAME));

        UUID recipeId = UUID.fromString(TestDbConstants.RECIPE_ID);
        RecipeViewModel recipe = RecipeMapper.toModel(recipeRepository.getRecipeById(recipeId), NutritionalSummary.empty());
        populateIngredientsAndStepsForRecipe(recipe);

        UUID foodId = UUID.fromString(TestDbConstants.COMPLEX_FOOD_ONE_ID);
        ComplexFood foodEntity = foodRepository.getComplexFoodById(foodId);
        ReadOnlyFoodViewModel newIngredientFood = FoodMapper.toModel(foodEntity, NutritionalSummary.empty());
        recipe.addIngredient(newIngredientFood);

        SaveRecipeTask sut = new SaveRecipeTask(recipeRepository, foodRepository, userViewModel, recipe);
        RecipeViewModel savedRecipe = null;

        // Act
        try
        {
            sut.run();
            savedRecipe = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(savedRecipe).isNotNull();
        assertThat(savedRecipe.getIngredients().size()).isEqualTo(3);
    }

    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS,
            TestDbConstants.SCRIPT_TEST_RECIPE,
            TestDbConstants.SCRIPT_TEST_RECIPE_INGREDIENTS,
            TestDbConstants.SCRIPT_TEST_RECIPE_STEPS
    })
    public void Remove_Ingredient_From_Recipe()
    {
        // Arrange
        UserViewModel userViewModel = UserMapper.toModel(userRepository.getByUsername(TestDbConstants.USER_USERNAME));

        UUID recipeId = UUID.fromString(TestDbConstants.RECIPE_ID);
        RecipeViewModel recipe = RecipeMapper.toModel(recipeRepository.getRecipeById(recipeId), NutritionalSummary.empty());
        populateIngredientsAndStepsForRecipe(recipe);

        IngredientModel firstIngredient = recipe.getIngredients().get(0);
        recipe.removeIngredient(firstIngredient);

        SaveRecipeTask sut = new SaveRecipeTask(recipeRepository, foodRepository, userViewModel, recipe);
        RecipeViewModel savedRecipe = null;

        // Act
        try
        {
            sut.run();
            savedRecipe = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(savedRecipe).isNotNull();
        assertThat(savedRecipe.getIngredients().size()).isEqualTo(1);
    }


    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS,
            TestDbConstants.SCRIPT_TEST_RECIPE,
            TestDbConstants.SCRIPT_TEST_RECIPE_INGREDIENTS,
            TestDbConstants.SCRIPT_TEST_RECIPE_STEPS
    })
    public void Add_Step_To_Recipe()
    {
        // Arrange
        UserViewModel userViewModel = UserMapper.toModel(userRepository.getByUsername(TestDbConstants.USER_USERNAME));

        UUID recipeId = UUID.fromString(TestDbConstants.RECIPE_ID);
        RecipeViewModel recipe = RecipeMapper.toModel(recipeRepository.getRecipeById(recipeId), NutritionalSummary.empty());
        populateIngredientsAndStepsForRecipe(recipe);

        RecipeStepViewModel recipeStep = new RecipeStepViewModel(3);
        recipeStep.setStepName("Step Three");
        recipeStep.setDirections("Step three test directions");
        recipe.getSteps().add(recipeStep);

        SaveRecipeTask sut = new SaveRecipeTask(recipeRepository, foodRepository, userViewModel, recipe);
        RecipeViewModel savedRecipe = null;

        // Act
        try
        {
            sut.run();
            savedRecipe = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(savedRecipe).isNotNull();
        assertThat(savedRecipe.getSteps().size()).isEqualTo(3);
    }

    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS,
            TestDbConstants.SCRIPT_TEST_RECIPE,
            TestDbConstants.SCRIPT_TEST_RECIPE_INGREDIENTS,
            TestDbConstants.SCRIPT_TEST_RECIPE_STEPS
    })
    public void Remove_Step_From_Recipe()
    {
        // Arrange
        UserViewModel userViewModel = UserMapper.toModel(userRepository.getByUsername(TestDbConstants.USER_USERNAME));

        UUID recipeId = UUID.fromString(TestDbConstants.RECIPE_ID);
        RecipeViewModel recipe = RecipeMapper.toModel(recipeRepository.getRecipeById(recipeId), NutritionalSummary.empty());
        populateIngredientsAndStepsForRecipe(recipe);

        recipe.getSteps().remove(0);

        SaveRecipeTask sut = new SaveRecipeTask(recipeRepository, foodRepository, userViewModel, recipe);
        RecipeViewModel savedRecipe = null;

        // Act
        try
        {
            sut.run();
            savedRecipe = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        // Assert
        assertThat(savedRecipe).isNotNull();
        assertThat(savedRecipe.getSteps().size()).isEqualTo(1);
    }

    /**
     * Tests that a recipe that has the same nutritional profile as another recipe but
     * a different name is correctly detected as being unique
     */
    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS,
            TestDbConstants.SCRIPT_TEST_RECIPE,
            TestDbConstants.SCRIPT_TEST_RECIPE_INGREDIENTS,
            TestDbConstants.SCRIPT_TEST_RECIPE_STEPS
    })
    public void Recipe_With_Duplicate_Nutritional_Profile_But_Different_Name_Is_Unique()
    {
        // Arrange
        UUID recipeId = UUID.fromString(TestDbConstants.RECIPE_ID);
        RecipeViewModel recipe = RecipeMapper.toModel(recipeRepository.getRecipeById(recipeId), NutritionalSummary.empty());
        recipe.setName("Test Name");
        populateIngredientsAndStepsForRecipe(recipe);

        boolean isUnique = false;

        // Act
        IsUniqueRecipeTask sut = new IsUniqueRecipeTask(recipeRepository, foodRepository, recipe);

        try
        {
            sut.run();
            isUnique = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            Assert.fail("The task has failed during execution", e);
        }

        // Assert
        assertThat(isUnique).isTrue();
    }

    /**
     * Tests that a recipe that has the same name as another recipe but a different
     * nutritional profile is correctly detected as being unique
     */
    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS,
            TestDbConstants.SCRIPT_TEST_RECIPE,
            TestDbConstants.SCRIPT_TEST_RECIPE_INGREDIENTS,
            TestDbConstants.SCRIPT_TEST_RECIPE_STEPS
    })
    public void Recipe_With_Duplicate_Name_But_Different_Nutritional_Profile_Is_Unique()
    {
        // Arrange
        UUID recipeId = UUID.fromString(TestDbConstants.RECIPE_ID);
        RecipeViewModel recipe = RecipeMapper.toModel(recipeRepository.getRecipeById(recipeId), NutritionalSummary.empty());
        populateIngredientsAndStepsForRecipe(recipe);
        recipe.removeIngredient(recipe.getIngredients().get(0));

        boolean isUnique = false;

        // Act
        IsUniqueRecipeTask sut = new IsUniqueRecipeTask(recipeRepository, foodRepository, recipe);

        try
        {
            sut.run();
            isUnique = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            Assert.fail("The task has failed during execution", e);
        }

        // Assert
        assertThat(isUnique).isTrue();
    }

    /**
     * Tests that a recipe that has both the same name and the same nutritional profile
     * as another recipe is correctly detected as non-unique.
     */
    @Test
    @Sql({
            TestDbConstants.SCRIPT_TEST_SIMPLE_FOODS,
            TestDbConstants.SCRIPT_TEST_RECIPE,
            TestDbConstants.SCRIPT_TEST_RECIPE_INGREDIENTS,
            TestDbConstants.SCRIPT_TEST_RECIPE_STEPS
    })
    public void Duplicate_Recipe_Is_Not_Unique()
    {
        // Arrange
        UUID recipeId = UUID.fromString(TestDbConstants.RECIPE_ID);
        RecipeViewModel recipe = RecipeMapper.toModel(recipeRepository.getRecipeById(recipeId), NutritionalSummary.empty());
        populateIngredientsAndStepsForRecipe(recipe);

        boolean isUnique = true;

        // Act
        IsUniqueRecipeTask sut = new IsUniqueRecipeTask(recipeRepository, foodRepository, recipe);

        try
        {
            sut.run();
            isUnique = sut.get();
        }
        catch(InterruptedException | ExecutionException e)
        {
            Assert.fail("The task has failed during execution", e);
        }

        // Assert
        assertThat(isUnique).isFalse();
    }

    private void populateIngredientsAndStepsForRecipe(RecipeViewModel recipe)
    {
        List<RecipeIngredient> recipeIngredientEntities = recipeRepository.getIngredientsForRecipe(recipe.getId());
        List<IngredientModel> recipeIngredientViewModels = recipeMapperHelper.getRecipeIngredientModels(recipeIngredientEntities);
        recipe.setIngredients(recipeIngredientViewModels);

        List<RecipeStep> recipeStepEntities = recipeRepository.getStepsForRecipe(recipe.getId());
        List<RecipeStepModel> recipeStepViewModels = recipeMapperHelper.getStepModels(recipeStepEntities);
        recipe.setSteps(recipeStepViewModels);
    }
}
