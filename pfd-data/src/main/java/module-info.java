/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


module pfd.data {
    requires org.slf4j;

    requires javafx.base;
    requires javafx.graphics;

    requires com.h2database;
    requires jakarta.persistence;
    requires org.hibernate.orm.core;
    requires com.fasterxml.jackson.databind;
    requires com.fasterxml.jackson.datatype.jsr310;

    requires spring.core;
    requires spring.jdbc;
    requires spring.tx;
    requires spring.context;

    requires pfd.base;
    requires pfd.ui.model.api;
    requires pfd.ui.model.viewmodel;

    opens com.iceflyer3.pfd.data.factory;
    opens com.iceflyer3.pfd.data.repository;

    opens com.iceflyer3.pfd.data.entities;
    opens com.iceflyer3.pfd.data.entities.food;
    opens com.iceflyer3.pfd.data.entities.recipe;
    opens com.iceflyer3.pfd.data.entities.views;
    opens com.iceflyer3.pfd.data.entities.mealplanning;
    opens com.iceflyer3.pfd.data.entities.mealplanning.carbcycling;

    // TODO: Will be required for flyway migrations when there are any
    //opens db.migration;

    // Needed for access for registration with the Spring context
    exports com.iceflyer3.pfd.data to pfd.ui, spring.beans;
    exports com.iceflyer3.pfd.data.repository to pfd.ui;
    exports com.iceflyer3.pfd.data.configurer to pfd.ui;
    exports com.iceflyer3.pfd.data.factory to pfd.ui, pfd.ui.model.proxy;

    // Needed to be able to use with Jackson
    exports com.iceflyer3.pfd.data.json to com.fasterxml.jackson.databind;
    exports com.iceflyer3.pfd.data.json.food to com.fasterxml.jackson.databind;
    exports com.iceflyer3.pfd.data.json.recipe to com.fasterxml.jackson.databind;
    exports com.iceflyer3.pfd.data.json.mealplanning to com.fasterxml.jackson.databind;
    exports com.iceflyer3.pfd.data.json.mealplanning.carbcycling to com.fasterxml.jackson.databind;

    exports com.iceflyer3.pfd.data.calculator.mealplanning;

    exports com.iceflyer3.pfd.data.request.backup;
    exports com.iceflyer3.pfd.data.request.food;
    exports com.iceflyer3.pfd.data.request.mealplanning;
    exports com.iceflyer3.pfd.data.request.recipe;

    exports com.iceflyer3.pfd.data.task.data;
    exports com.iceflyer3.pfd.data.task.user;
    exports com.iceflyer3.pfd.data.task.food;
    exports com.iceflyer3.pfd.data.task.recipe;

    exports com.iceflyer3.pfd.data.task.mealplanning;
    exports com.iceflyer3.pfd.data.task.mealplanning.day;
    exports com.iceflyer3.pfd.data.task.mealplanning.meal;
    exports com.iceflyer3.pfd.data.task.mealplanning.carbcycling;
    exports com.iceflyer3.pfd.data.task.mealplanning.carbcycling.day;
    exports com.iceflyer3.pfd.data.task.mealplanning.carbcycling.meal;
    exports com.iceflyer3.pfd.data.task.mealplanning.calendar;
}