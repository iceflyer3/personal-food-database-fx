/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.entities.food;

import com.iceflyer3.pfd.enums.UnitOfMeasure;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.util.UUID;

@Entity
@Table(name = "food_serving_size")
public class FoodServingSize {

    public FoodServingSize() { }

    public FoodServingSize(UnitOfMeasure uom, BigDecimal servingSize, FoodMetaData forFood)
    {
        this.unitOfMeasure = uom;
        this.servingSize = servingSize;
        this.foodMeta = forFood;
    }

    public FoodServingSize(UUID servingSizeId, UnitOfMeasure uom, BigDecimal servingSize, FoodMetaData forFood)
    {
        this.servingSizeId = servingSizeId;
        this.unitOfMeasure = uom;
        this.servingSize = servingSize;
        this.foodMeta = forFood;
    }

    @Id
    @GeneratedValue
    @Column(name = "serving_size_id")
    protected UUID servingSizeId;

    @Enumerated(EnumType.STRING)
    @Column(name = "unit_of_measure", nullable = false)
    protected UnitOfMeasure unitOfMeasure;

    @Column(name = "serving_size", nullable = false, precision = 2)
    protected BigDecimal servingSize;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "food_id")
    protected FoodMetaData foodMeta;

    public UUID getServingSizeId() {
        return servingSizeId;
    }

    public UnitOfMeasure getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(UnitOfMeasure unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    public BigDecimal getServingSize() {
        return servingSize;
    }

    public void setServingSize(BigDecimal servingSize) {
        this.servingSize = servingSize;
    }

    public FoodMetaData getFoodMeta() {
        return foodMeta;
    }
}
