/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.entities.mealplanning;

import com.iceflyer3.pfd.constants.QueryConstants;
import com.iceflyer3.pfd.data.entities.mealplanning.carbcycling.CarbCyclingMeal;

import jakarta.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "meal_planning_meal")
@NamedQuery(
        name = QueryConstants.QUERY_MEAL_PLANNING_FIND_ALL_MEALS_FOR_USER,
        query = """
                SELECT mealPlanMeal
                FROM MealPlanMeal mealPlanMeal
                LEFT JOIN mealPlanMeal.carbCyclingMeal carbCyclingMeal
                WHERE mealPlanMeal.day.mealPlan.user.userId = :userId
                """)
@NamedQuery(name = QueryConstants.QUERY_MEAL_PLANNING_FIND_MEALS_FOR_DAY, query = "SELECT mealPlanMeal FROM MealPlanMeal mealPlanMeal WHERE mealPlanMeal.day.dayId = :dayId")
@NamedQuery(name = QueryConstants.QUERY_MEAL_PLANNING_REMOVE_MEALS_FOR_DAY, query = "DELETE FROM MealPlanMeal mealPlanMeal WHERE mealPlanMeal.day.dayId = :dayId")
public class MealPlanMeal
{
    public MealPlanMeal() { }

    public MealPlanMeal(MealPlanDay forDay, String mealName) {
        this.mealId = null;
        this.day = forDay;
        this.mealName = mealName;
        this.ingredients = new HashSet<>();
        this.lastModifiedDate = LocalDateTime.now();
    }

    public MealPlanMeal(
            UUID mealId,
            MealPlanDay forMealPlanDay,
            String mealName,
            LocalDateTime lastModifiedDate)
    {
        this(
                mealId,
                forMealPlanDay,
                null,
                mealName,
                lastModifiedDate
        );
    }

    public MealPlanMeal(
            UUID mealId,
            MealPlanDay forMealPlanDay,
            CarbCyclingMeal associatedCarbCyclingMeal,
            String mealName,
            LocalDateTime lastModifiedDate)
    {
        this.mealId = mealId;
        this.day = forMealPlanDay;
        this.carbCyclingMeal = associatedCarbCyclingMeal;
        this.mealName = mealName;
        this.lastModifiedDate = lastModifiedDate;

        this.ingredients = new HashSet<>();
    }

    @Id
    @GeneratedValue
    @Column(name = "meal_id")
    protected UUID mealId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "day_id")
    protected MealPlanDay day;

    @OneToOne(mappedBy = "mealPlanMeal", fetch = FetchType.EAGER)
    protected CarbCyclingMeal carbCyclingMeal;

    @Column(name = "meal_name", nullable = false)
    protected String mealName;

    @OneToMany(mappedBy = "meal")
    protected Set<MealPlanMealIngredient> ingredients;

    //TODO: Perhaps we should see about adding a created date column here too for consistency sake
    @Column(name = "last_modified_date", nullable = false, columnDefinition = "timestamp")
    protected LocalDateTime lastModifiedDate;

    public UUID getMealId() {
        return mealId;
    }

    public MealPlanDay getDay() {
        return day;
    }

    public void setDay(MealPlanDay day) {
        this.day = day;
    }

    public CarbCyclingMeal getCarbCyclingMeal() {
        return carbCyclingMeal;
    }

    public void setCarbCyclingMeal(CarbCyclingMeal carbCyclingMeal) {
        this.carbCyclingMeal = carbCyclingMeal;
    }

    public String getMealName() {
        return mealName;
    }

    public void setMealName(String mealName) {
        this.mealName = mealName;
    }

    public void setIngredients(Set<MealPlanMealIngredient> ingredients) {
        this.ingredients = ingredients;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }
}
