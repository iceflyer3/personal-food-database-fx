/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.entities.mealplanning;

import com.iceflyer3.pfd.constants.QueryConstants;
import com.iceflyer3.pfd.data.entities.User;
import com.iceflyer3.pfd.enums.ActivityLevel;
import com.iceflyer3.pfd.enums.BasalMetabolicRateFormula;
import com.iceflyer3.pfd.enums.FitnessGoal;
import com.iceflyer3.pfd.enums.Sex;
import jakarta.persistence.*;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "meal_plan")
@NamedQuery(name = QueryConstants.QUERY_MEAL_PLANNING_FIND_ALL_PLANS, query = "SELECT mealPlan FROM MealPlan mealPlan")
@NamedQuery(name = QueryConstants.QUERY_MEAL_PLANNING_FIND_PLANS_FOR_USER, query = "SELECT mealPlan FROM MealPlan mealPlan WHERE mealPlan.user.userId = :userId")
@NamedQuery(name = QueryConstants.QUERY_MEAL_PLANNING_FIND_ACTIVE_MEAL_PLAN, query = "SELECT mealPlan FROM MealPlan mealPlan WHERE mealPlan.user.userId = :userId AND mealPlan.isActive = true")
public class MealPlan {

    /**
     * TODO: These default constructors exist to satisfy Hibernate / JPA which require them. Specifically, they exist
     *       to satisfy the JPA spec which Hibernate implements. However, because they exist expressly for this purpose
     *       they almost certainly should not be public as they are not intended for actual use by anything other than
     *       Hibernate.
     *
     *       They must have at least package level visibility though. I'm not entirely sure what exactly my original thought
     *       process was here in making them public. But at this point I'd say it is safe to say that they no longer need to
     *       nor should be accessible to clients who may be tempted to use them.
     */
    public MealPlan() { }

    public MealPlan(
                    User user,
                    BigDecimal height,
                    BigDecimal weight,
                    Integer age,
                    Sex sex,
                    ActivityLevel activityLevel,
                    FitnessGoal fitnessGoal,
                    BasalMetabolicRateFormula bmrFormula,
                    BigDecimal dailyCalories,
                    BigDecimal tdeeCalories)
    {
        this.mealPlanId = null;
        this.user = user;
        this.height = height;
        this.weight = weight;
        this.age = age;
        this.sex = sex;
        this.activityLevel = activityLevel;
        this.fitnessGoal = fitnessGoal;
        this.bmrFormula = bmrFormula;
        this.dailyCalories = dailyCalories;
        this.tdeeCalories = tdeeCalories;
        this.nutrientSuggestions = new HashSet<>();
        this.isActive = false;
    }

    public MealPlan(
            UUID mealPlanId,
            User user,
            BigDecimal height,
            BigDecimal weight,
            Integer age,
            Sex sex,
            ActivityLevel activityLevel,
            FitnessGoal fitnessGoal,
            BasalMetabolicRateFormula bmrFormula,
            BigDecimal bodyFatPercentage,
            BigDecimal dailyCalories,
            BigDecimal tdeeCalories,
            boolean isActive)
    {
        this.mealPlanId = mealPlanId;
        this.user = user;
        this.height = height;
        this.weight = weight;
        this.age = age;
        this.sex = sex;
        this.activityLevel = activityLevel;
        this.fitnessGoal = fitnessGoal;
        this.bmrFormula = bmrFormula;
        this.bodyFatPercentage = bodyFatPercentage;
        this.dailyCalories = dailyCalories;
        this.tdeeCalories = tdeeCalories;
        this.nutrientSuggestions = new HashSet<>();
        this.isActive = isActive;
    }

    @Id
    @GeneratedValue
    @Column(name = "meal_plan_id")
    protected UUID mealPlanId;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    protected User user;

    @Column(name = "height", nullable = false, precision = 2)
    protected BigDecimal height;

    @Column(name = "weight", nullable = false, precision = 2)
    protected BigDecimal weight;

    @Column(name = "age", nullable = false)
    protected Integer age;

    @Enumerated(EnumType.STRING)
    @Column(name = "sex", nullable = false)
    protected Sex sex;

    @Enumerated(EnumType.STRING)
    @Column(name = "activity_level", nullable = false)
    protected ActivityLevel activityLevel;

    @Enumerated(EnumType.STRING)
    @Column(name = "fitness_goal", nullable = false)
    protected FitnessGoal fitnessGoal;

    @Enumerated(EnumType.STRING)
    @Column(name = "bmr_formula", nullable = false)
    protected BasalMetabolicRateFormula bmrFormula;

    @Column(name = "body_fat_percentage", precision = 2)
    protected BigDecimal bodyFatPercentage;

    // Daily calories are TDEE calories + any surplus or deficit dependent upon goal
    @Column(name = "daily_calories", nullable = false, precision = 2)
    protected BigDecimal dailyCalories;

    // TDEE calories are the base calories needed per day based upon energy expenditure.
    // This number does not contain any surpluses or deficits like daily calories does.
    @Column(name = "tdee_calories", nullable = false, precision = 2)
    protected BigDecimal tdeeCalories;

    @Column(name = "is_active", nullable = false)
    protected Boolean isActive;

    @OneToMany(mappedBy = "mealPlan", fetch = FetchType.EAGER)
    protected Set<MealPlanNutrientSuggestion> nutrientSuggestions;

    public UUID getMealPlanId() {
        return mealPlanId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) { this.user = user; }

    public BigDecimal getHeight() {
        return height;
    }

    public void setHeight(BigDecimal height) {
        this.height = height;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public ActivityLevel getActivityLevel() {
        return activityLevel;
    }

    public void setActivityLevel(ActivityLevel activityLevel) {
        this.activityLevel = activityLevel;
    }

    public FitnessGoal getFitnessGoal() {
        return fitnessGoal;
    }

    public void setFitnessGoal(FitnessGoal fitnessGoal) {
        this.fitnessGoal = fitnessGoal;
    }

    public BasalMetabolicRateFormula getBmrFormula() {
        return bmrFormula;
    }

    public void setBmrFormula(BasalMetabolicRateFormula bmrFormula) {
        this.bmrFormula = bmrFormula;
    }

    public BigDecimal getBodyFatPercentage() {
        return bodyFatPercentage;
    }

    public void setBodyFatPercentage(BigDecimal bodyFatPercentage) {
        this.bodyFatPercentage = bodyFatPercentage;
    }

    public BigDecimal getDailyCalories() {
        return dailyCalories;
    }

    public void setDailyCalories(BigDecimal dailyCalories) {
        this.dailyCalories = dailyCalories;
    }

    public BigDecimal getTdeeCalories()
    {
        return tdeeCalories;
    }

    public void setTdeeCalories(BigDecimal tdeeCalories)
    {
        this.tdeeCalories = tdeeCalories;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean active) {
        isActive = active;
    }

    public Set<MealPlanNutrientSuggestion> getNutrientSuggestions() {
        return nutrientSuggestions;
    }

    @Override
    public String toString()
    {
        return "MealPlan{" + "mealPlanId=" + mealPlanId + ", user=" + user + ", height=" + height + ", weight=" + weight + ", age=" + age + ", sex=" + sex + ", activityLevel=" + activityLevel + ", fitnessGoal=" + fitnessGoal + ", bmrFormula=" + bmrFormula + ", bodyFatPercentage=" + bodyFatPercentage + ", dailyCalories=" + dailyCalories + ", tdeeCalories=" + tdeeCalories + ", isActive=" + isActive + ", macroSuggestions=" + nutrientSuggestions + '}';
    }
}
