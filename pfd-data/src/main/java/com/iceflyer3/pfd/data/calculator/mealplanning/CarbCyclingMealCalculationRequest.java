package com.iceflyer3.pfd.data.calculator.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import java.math.BigDecimal;

/**
 * Encapsulates all of the details needed to perform the calculations needed
 * for determining caloric and macronutrient needs for a meal that is using
 * a carb cycling plan
 */
public class CarbCyclingMealCalculationRequest
{
    private final int numMealsForDay;
    private final int mealNumber;
    private final int postWorkoutMealNumber;
    private final BigDecimal carbConsumptionPercentage;
    private final BigDecimal dailyRequiredProtein;
    private final BigDecimal dailyRequiredCarbs;
    private final BigDecimal dailyRequiredFats;
    private final boolean isTimingFats;
    private final boolean isForTrainingDay;

    /**
     * Create a new request that will be used to calculate caloric and macronutrient requirements
     * for a carb cycling meal
     * @param numMealsForDay The total number of meals to be eaten in the day
     * @param mealNumber The number of the meal that this is request is for
     * @param postWorkoutMealNumber The number of the pre-workout meal in the same day.
     *                              For pre-workout meals this will be the same as mealNumber.
     * @param carbConsumptionPercentage The percentage of daily carbohydrates to consume with this meal
     * @param dailyRequiredProtein Amount of required protein for the day (in grams)
     * @param dailyRequiredCarbs Amount of required carbohydrates for the day (in grams)
     * @param dailyRequiredFats Amount of required fats for the day (in grams)
     * @param isTimingFats If this carb cycling plan should take timing of fats into consideration
     */
    public CarbCyclingMealCalculationRequest(
            int numMealsForDay,
            int mealNumber,
            int postWorkoutMealNumber,
            BigDecimal carbConsumptionPercentage,
            BigDecimal dailyRequiredProtein,
            BigDecimal dailyRequiredCarbs,
            BigDecimal dailyRequiredFats,
            boolean isTimingFats,
            boolean isForTrainingDay)
    {
        this.numMealsForDay = numMealsForDay;
        this.mealNumber = mealNumber;
        this.postWorkoutMealNumber = postWorkoutMealNumber;
        this.carbConsumptionPercentage = carbConsumptionPercentage;
        this.dailyRequiredProtein = dailyRequiredProtein;
        this.dailyRequiredCarbs = dailyRequiredCarbs;
        this.dailyRequiredFats = dailyRequiredFats;
        this.isTimingFats = isTimingFats;
        this.isForTrainingDay = isForTrainingDay;
    }

    public int getNumMealsForDay() {
        return numMealsForDay;
    }

    public int getMealNumber() {
        return mealNumber;
    }

    public int getPostWorkoutMealNumber() {
        return postWorkoutMealNumber;
    }

    public BigDecimal getCarbConsumptionPercentage() {
        return carbConsumptionPercentage;
    }

    public BigDecimal getDailyRequiredProtein() {
        return dailyRequiredProtein;
    }

    public BigDecimal getDailyRequiredCarbs() {
        return dailyRequiredCarbs;
    }

    public BigDecimal getDailyRequiredFats() {
        return dailyRequiredFats;
    }

    public boolean isTimingFats() {
        return isTimingFats;
    }

    public boolean isForTrainingDay()
    {
        return isForTrainingDay;
    }
}
