package com.iceflyer3.pfd.data.mapping;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.entities.food.ComplexFood;
import com.iceflyer3.pfd.data.entities.food.ComplexFoodIngredient;
import com.iceflyer3.pfd.data.entities.food.FoodMetaData;
import com.iceflyer3.pfd.data.entities.food.SimpleFood;
import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanMeal;
import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanMealIngredient;
import com.iceflyer3.pfd.data.entities.recipe.Recipe;
import com.iceflyer3.pfd.data.entities.recipe.RecipeIngredient;
import com.iceflyer3.pfd.data.entities.recipe.RecipeStep;
import com.iceflyer3.pfd.data.entities.recipe.RecipeStepIngredient;
import com.iceflyer3.pfd.data.json.food.JsonComplexFoodIngredient;
import com.iceflyer3.pfd.data.json.mealplanning.JsonMealPlanMealIngredient;
import com.iceflyer3.pfd.data.json.recipe.JsonRecipeIngredient;
import com.iceflyer3.pfd.data.json.recipe.JsonRecipeStepIngredient;
import com.iceflyer3.pfd.data.mapping.food.FoodMapper;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.ReadOnlyFoodViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.IngredientViewModel;

public class IngredientMapper {

    private IngredientMapper() { }

    // We'll just offer overrides here for each kind of ingredient we have.
    public static IngredientModel toModel(MealPlanMealIngredient entity, ReadOnlyFoodViewModel readOnlyFoodViewModel)
    {
        IngredientViewModel viewModel = new IngredientViewModel(
                entity.getIngredientId(),
                readOnlyFoodViewModel,
                entity.getServingsIncluded()
        );
        viewModel.setIsAlternate(entity.isAlternate());
        viewModel.setIsOptional(entity.isOptional());
        return viewModel;
    }

    public static IngredientModel toModel(ComplexFoodIngredient entity)
    {
        IngredientViewModel viewModel = new IngredientViewModel(
                entity.getIngredientId(),
                FoodMapper.toModel(entity.getSimpleFood()),
                entity.getSimpleFoodServingsIncluded());
        viewModel.setIsAlternate(entity.getIsAlternate());
        viewModel.setIsOptional(entity.getIsOptional());
        return viewModel;
    }

    public static IngredientModel toModel(RecipeIngredient ingredientEntity, ReadOnlyFoodViewModel ingredientFood)
    {
        IngredientViewModel viewModel = new IngredientViewModel(
                ingredientEntity.getRecipeIngredientId(),
                ingredientFood,
                ingredientEntity.getNumberOfServings()
        );
        viewModel.setIsAlternate(ingredientEntity.getAlternate());
        viewModel.setIsOptional(ingredientEntity.getOptional());
        return viewModel;
    }

    public static IngredientModel toModel(RecipeStepIngredient stepIngredientEntity, ReadOnlyFoodViewModel ingredientFood)
    {
        IngredientViewModel viewModel = new IngredientViewModel(
                stepIngredientEntity.getRecipeIngredient().getRecipeIngredientId(),
                ingredientFood,
                stepIngredientEntity.getRecipeIngredient().getNumberOfServings()
        );
        viewModel.setIsAlternate(stepIngredientEntity.getRecipeIngredient().getAlternate());
        viewModel.setIsOptional(stepIngredientEntity.getRecipeIngredient().getOptional());
        return viewModel;
    }

    public static JsonMealPlanMealIngredient toJson(MealPlanMealIngredient ingredient)
    {
        return new JsonMealPlanMealIngredient(
                ingredient.getIngredientId(),
                ingredient.getMeal().getMealId(),
                ingredient.getFoodMeta().getFoodId(),
                ingredient.getServingsIncluded(),
                ingredient.isAlternate(),
                ingredient.isOptional()
        );
    }

    public static JsonComplexFoodIngredient toJson(ComplexFoodIngredient complexFoodIngredient)
    {
        return new JsonComplexFoodIngredient(
                complexFoodIngredient.getIngredientId(),
                complexFoodIngredient.getSimpleFood().getFoodId(),
                complexFoodIngredient.getSimpleFood().getName(),
                complexFoodIngredient.getSimpleFood().getBrand(),
                complexFoodIngredient.getComplexFood().getFoodId(),
                complexFoodIngredient.getSimpleFoodServingsIncluded(),
                complexFoodIngredient.getIsAlternate(),
                complexFoodIngredient.getIsOptional()
        );
    }

    public static JsonRecipeIngredient toJson(RecipeIngredient recipeIngredient)
    {
        return new JsonRecipeIngredient(
                recipeIngredient.getRecipeIngredientId(),
                recipeIngredient.getRecipe().getRecipeId(),
                recipeIngredient.getFoodMeta().getFoodId(),
                recipeIngredient.getNumberOfServings(),
                recipeIngredient.getAlternate(),
                recipeIngredient.getOptional()
        );
    }

    public static JsonRecipeStepIngredient toJson(RecipeStepIngredient recipeStepIngredient)
    {
        return new JsonRecipeStepIngredient(
                recipeStepIngredient.getRecipe().getRecipeId(),
                recipeStepIngredient.getRecipeStep().getStepId(),
                recipeStepIngredient.getRecipeIngredient().getRecipeIngredientId()
        );
    }

    // There isn't any real "mapping" here. But we'll do it here for consistency's sake anyway.
    public static RecipeStepIngredient toEntity(Recipe forRecipe, RecipeStep forStep, RecipeIngredient ingredient)
    {
        return new RecipeStepIngredient(forRecipe, forStep, ingredient);
    }

    /**
     * Returns a new (and detached) ComplexFoodIngredient instance hydrated from json without a set id.
     * @param jsonComplexFoodIngredient json from which to hydrate the new instance
     * @param ingredientFood Associated ingredient simple food
     * @param forFood The complex food to which the ingredient belongs
     * @return ComplexFoodIngredient instance hydrated from json without a set id
     */
    public static ComplexFoodIngredient toFreshEntity(JsonComplexFoodIngredient jsonComplexFoodIngredient, SimpleFood ingredientFood, ComplexFood forFood)
    {
        return new ComplexFoodIngredient(
                null, // Ensure a new record is always created upon persist / merge
                ingredientFood,
                forFood,
                jsonComplexFoodIngredient.getSimpleFoodServingsIncluded(),
                jsonComplexFoodIngredient.isAlternate(),
                jsonComplexFoodIngredient.isOptional()
        );
    }

    /**
     * Returns a new (and detached) MealPlanMealIngredient instance hydrated from json without a set id.
     * @param jsonMealPlanMealIngredient json from which to hydrate the new instance
     * @param forMeal Meal to which the ingredient belongs
     * @param ingredientFoodMetaData Metadata for the ingredient food
     * @return MealPlanMealIngredient instance hydrated from json without a set id
     */
    public static MealPlanMealIngredient toFreshEntity(JsonMealPlanMealIngredient jsonMealPlanMealIngredient, MealPlanMeal forMeal, FoodMetaData ingredientFoodMetaData)
    {
        return new MealPlanMealIngredient(
                null,
                forMeal,
                ingredientFoodMetaData,
                jsonMealPlanMealIngredient.servingsIncluded(),
                jsonMealPlanMealIngredient.isAlternate(),
                jsonMealPlanMealIngredient.isOptional()
        );
    }

    /**
     * Returns a new (and detached) MealPlanMealIngredient instance hydrated from json without a set id.
     * @param jsonRecipeIngredient json from which to hydrate the new instance
     * @param forRecipe Meal to which the ingredient belongs
     * @param ingredientFoodMetaData Metadata for the ingredient food
     * @return MealPlanMealIngredient instance hydrated from json without a set id
     */
    public static RecipeIngredient toFreshEntity(JsonRecipeIngredient jsonRecipeIngredient, Recipe forRecipe, FoodMetaData ingredientFoodMetaData)
    {
        return new RecipeIngredient(
                null,
                forRecipe,
                ingredientFoodMetaData,
                jsonRecipeIngredient.getNumberOfServings(),
                jsonRecipeIngredient.isAlternate(),
                jsonRecipeIngredient.isOptional()
        );
    }
}
