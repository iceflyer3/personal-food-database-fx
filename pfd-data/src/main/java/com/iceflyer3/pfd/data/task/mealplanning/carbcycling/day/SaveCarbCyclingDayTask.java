package com.iceflyer3.pfd.data.task.mealplanning.carbcycling.day;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.calculator.mealplanning.CalculationResult;
import com.iceflyer3.pfd.data.calculator.mealplanning.CarbCyclingCalculator;
import com.iceflyer3.pfd.data.calculator.mealplanning.CarbCyclingDayRequirementsCalcuationRequest;
import com.iceflyer3.pfd.data.entities.mealplanning.MealPlan;
import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanDay;
import com.iceflyer3.pfd.data.entities.views.CarbCyclingDayNutritionalSummary;
import com.iceflyer3.pfd.data.mapping.mealplanning.carbcycling.CarbCyclingDayMapper;
import com.iceflyer3.pfd.data.mapping.summary.NutritionalSummaryMapper;
import com.iceflyer3.pfd.data.repository.MealPlanningRepository;
import com.iceflyer3.pfd.data.request.mealplanning.day.CreateCarbCyclingDayRequest;
import com.iceflyer3.pfd.data.request.mealplanning.day.ModifyDayRequest;
import com.iceflyer3.pfd.data.task.mealplanning.AbstractMealPlanningTask;
import com.iceflyer3.pfd.enums.Nutrient;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingDayViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;

import java.util.Optional;

public class SaveCarbCyclingDayTask extends AbstractMealPlanningTask<CarbCyclingDayViewModel>
{
    private final CarbCyclingDayViewModel dayViewModel;

    public SaveCarbCyclingDayTask(MealPlanningRepository mealPlanningRepository, CarbCyclingDayViewModel dayViewModel)
    {
        super(mealPlanningRepository);
        this.dayViewModel = dayViewModel;
    }

    @Override
    protected CarbCyclingDayViewModel call() throws Exception
    {
        MealPlanDay dayEntity;

        // Days will not have any meals upon initial creation so no summary will be found for new days
        // when searched for
        NutritionalSummary nutritionalSummary = NutritionalSummary.empty();

        if (dayViewModel.getId() == null)
        {
            // Calculate requirements for the new day
            MealPlan forMealPlan = mealPlanningRepository.getMealPlanForId(dayViewModel.getOwningMealPlanId());
            CarbCyclingDayRequirementsCalcuationRequest dayRequirementsCalculationRequest = new CarbCyclingDayRequirementsCalcuationRequest(forMealPlan.getFitnessGoal(), dayViewModel.getDayType(), forMealPlan.getTdeeCalories(), forMealPlan.getWeight());
            CalculationResult carbCyclingDayCalculationResult = CarbCyclingCalculator.instance().calculateDayRequirements(dayRequirementsCalculationRequest);

            dayViewModel.setRequiredCalories(carbCyclingDayCalculationResult.getNeededCalories());
            dayViewModel.setRequiredProtein(carbCyclingDayCalculationResult.getMacronutrientSuggestion(Nutrient.PROTEIN));
            dayViewModel.setRequiredCarbs(carbCyclingDayCalculationResult.getMacronutrientSuggestion(Nutrient.CARBOHYDRATES));
            dayViewModel.setRequiredFats(carbCyclingDayCalculationResult.getMacronutrientSuggestion(Nutrient.TOTAL_FATS));

            // Create the new day and persist it to the db
            CreateCarbCyclingDayRequest createCarbCyclingDayRequest = new CreateCarbCyclingDayRequest(forMealPlan.getMealPlanId(), dayViewModel, dayViewModel.getOwningCarbCyclingPlanId());
            dayEntity = mealPlanningRepository.createDayForMealPlan(createCarbCyclingDayRequest);
        }
        else
        {
            ModifyDayRequest modifyDayRequest = new ModifyDayRequest(dayViewModel.getId(), dayViewModel.getDayLabel());
            dayEntity = mealPlanningRepository.updateDayForMealPlan(modifyDayRequest);

            Optional<CarbCyclingDayNutritionalSummary> nutritionalSummaryEntity = mealPlanningRepository.getCarbCyclingDayNutritionalSummary(dayEntity.getDayId());

            if (nutritionalSummaryEntity.isPresent())
            {
                nutritionalSummary = NutritionalSummaryMapper.toModel(nutritionalSummaryEntity.get());
            }
        }

        return CarbCyclingDayMapper.toModel(dayEntity, nutritionalSummary);
    }
}
