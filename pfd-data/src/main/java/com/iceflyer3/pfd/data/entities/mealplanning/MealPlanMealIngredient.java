/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.entities.mealplanning;

import com.iceflyer3.pfd.constants.QueryConstants;
import com.iceflyer3.pfd.data.entities.food.FoodMetaData;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.util.UUID;

@Entity
@Table(name = "meal_planning_meal_ingredient")
@NamedQuery(
        name = QueryConstants.QUERY_MEAL_PLANNING_FIND_MEAL_INGREDIENTS_FOR_USER,
        query = """
                SELECT mealIngredient
                FROM MealPlanMealIngredient mealIngredient
                LEFT JOIN mealIngredient.meal.carbCyclingMeal carbCyclingMeal
                WHERE carbCyclingMeal IS NULL AND mealIngredient.meal.day.mealPlan.user.userId = :userId
                """
                )
@NamedQuery(
        name = QueryConstants.QUERY_MEAL_PLANNING_FIND_CARB_CYCLING_MEAL_INGREDIENTS_FOR_USER,
        query = """
                SELECT mealIngredient
                FROM MealPlanMealIngredient mealIngredient
                LEFT JOIN mealIngredient.meal.carbCyclingMeal carbCyclingMeal
                WHERE carbCyclingMeal IS NOT NULL AND carbCyclingMeal.carbCyclingDay.carbCyclingPlan.user.userId = :userId
                """
                )
@NamedQuery(
        name = QueryConstants.QUERY_MEAL_PLANNING_FIND_INGREDIENTS_FOR_MEAL,
        query = "SELECT mealIngredient FROM MealPlanMealIngredient mealIngredient WHERE mealIngredient.meal.mealId = :mealId"
)
@NamedQuery(
        name = QueryConstants.QUERY_MEAL_PLANNING_REMOVE_INGREDIENTS_FOR_MEAL,
        query = "DELETE FROM MealPlanMealIngredient mealIngredient WHERE mealIngredient.meal.mealId = :mealId"
)
public class MealPlanMealIngredient
{

    public MealPlanMealIngredient() { }

    public MealPlanMealIngredient(FoodMetaData food, MealPlanMeal meal)
    {
        this.foodMeta = food;
        this.meal = meal;
    }

    public MealPlanMealIngredient(
            UUID ingredientId,
            MealPlanMeal forMeal,
            FoodMetaData ingredientFoodMetaData,
            BigDecimal servingsIncluded,
            boolean isAlternate,
            boolean isOptional)
    {
        this.ingredientId = ingredientId;
        this.foodMeta = ingredientFoodMetaData;
        this.meal = forMeal;
        this.servingsIncluded = servingsIncluded;
        this.isAlternate = isAlternate;
        this.isOptional = isOptional;
    }

    @Id
    @GeneratedValue
    @Column(name = "ingredient_id")
    protected UUID ingredientId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "food_id")
    protected FoodMetaData foodMeta;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="meal_id")
    protected MealPlanMeal meal;

    @Column(name = "food_servings_included", nullable = false, precision = 2)
    protected BigDecimal servingsIncluded;

    @Column(name = "is_alternate", nullable = false)
    protected Boolean isAlternate;

    @Column(name = "is_optional", nullable = false)
    protected Boolean isOptional;

    public UUID getIngredientId() {
        return ingredientId;
    }

    public FoodMetaData getFoodMeta() {
        return foodMeta;
    }

    public MealPlanMeal getMeal() {
        return meal;
    }

    public void setMeal(MealPlanMeal meal) {
        this.meal = meal;
    }

    public BigDecimal getServingsIncluded() {
        return servingsIncluded;
    }

    public void setServingsIncluded(BigDecimal servingsIncluded) {
        this.servingsIncluded = servingsIncluded;
    }

    public Boolean isAlternate() {
        return isAlternate;
    }

    public void setIsAlternate(Boolean alternate) {
        isAlternate = alternate;
    }

    public Boolean isOptional() {
        return isOptional;
    }

    public void setIsOptional(Boolean optional) {
        isOptional = optional;
    }

    @Override
    public String toString() {
        return "MealPlanningMealIngredient{" +
                "ingredientId=" + ingredientId +
                ", foodMeta=" + foodMeta +
                ", meal=" + meal +
                ", servingsIncluded=" + servingsIncluded +
                ", isAlternate=" + isAlternate +
                ", isOptional=" + isOptional +
                '}';
    }
}
