/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.task.user;

import com.iceflyer3.pfd.data.entities.User;
import com.iceflyer3.pfd.data.mapping.UserMapper;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.data.repository.UserRepository;
import javafx.collections.ObservableList;

import java.util.List;

public class GetAllUsersTask extends AbstractUserTask<ObservableList<UserViewModel>> {

    public GetAllUsersTask(UserRepository userRepository)
    {
        super(userRepository);
    }

    @Override
    protected ObservableList<UserViewModel> call() throws Exception {
        List<User> users = userRepository.getAll();
        return UserMapper.toModelList(users);
    }
}
