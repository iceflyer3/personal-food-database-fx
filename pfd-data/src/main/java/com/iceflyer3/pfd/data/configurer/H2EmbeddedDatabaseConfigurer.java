/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.configurer;

import com.iceflyer3.pfd.data.AppConfigUtils;
import org.springframework.jdbc.datasource.embedded.ConnectionProperties;

/**
 * Implementation of EmbeddedDatabaseConfigurer that supports an H2 database with specifics defined by the user via the appConfig.properties file.
 *
 * We can't use Spring's built-in H2EmbeddedDatabaseConfigurer because, rather unfortunately, they hard-coded the URL to
 * be an in-memory database and because it is hard-coded we can't get around it so we must implement our own.
 *
 * The hard-coded source for the built-in Spring H2EmbeddedDatabaseConfigurer can be found at
 * https://github.com/spring-projects/spring-framework/blob/ba94a1216cc16a53c71ed0efb734fa7b84d34d5a/spring-jdbc/src/main/java/org/springframework/jdbc/datasource/embedded/H2EmbeddedDatabaseConfigurer.java#L34
 */
public class H2EmbeddedDatabaseConfigurer extends AbstractEmbeddedDatabaseConfigurer {

    public H2EmbeddedDatabaseConfigurer(AppConfigUtils appConfigUtils) {
        super(appConfigUtils);
    }

    @Override
    public void configureConnectionProperties(ConnectionProperties properties, String databaseName) {

        LOG.debug("Configuring connection properties for embedded H2 database...");

        properties.setDriverClass(org.h2.Driver.class);

        String dbUrl = appConfigUtils.getProperty("database.url", String.class);
        String username = appConfigUtils.getProperty("database.username", String.class);
        String password = appConfigUtils.getProperty("database.password", String.class);

        properties.setUsername(username);
        properties.setPassword(password);

        LOG.debug("Using H2 database url: {}", dbUrl);

        properties.setUrl(dbUrl);
    }
}
