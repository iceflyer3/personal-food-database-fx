package com.iceflyer3.pfd.data.factory;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.repository.FoodRepository;
import com.iceflyer3.pfd.data.task.food.*;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.ComplexFoodViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.SimpleFoodViewModel;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class FoodTaskFactory
{
    private final FoodRepository foodRepository;

    public FoodTaskFactory(FoodRepository foodRepository) {
        this.foodRepository = foodRepository;
    }

    public GetAllFoodsTask getAllFoods() {
        return new GetAllFoodsTask(foodRepository);
    }

    public GetReadOnlyFoodByIdTask getFoodByIdAsync(UUID foodID)
    {
        return new GetReadOnlyFoodByIdTask(foodRepository, foodID);
    }

    public GetSimpleFoodByIdTask getSimpleFoodByIdAsync(UUID foodId)
    {
        return new GetSimpleFoodByIdTask(foodRepository, foodId);
    }

    public SaveSimpleFoodTask saveSimpleFoodAsync(UserViewModel createdUser, SimpleFoodViewModel foodViewModel) {
        return new SaveSimpleFoodTask(foodRepository, createdUser, foodViewModel);
    }

    public GetComplexFoodByIdTask getComplexFoodByIdAsync(UUID foodId)
    {
        return new GetComplexFoodByIdTask(foodRepository, foodId);
    }

    public FindComplexFoodsWithNameTask findComplexFoodsWithNameAsync(String name)
    {
        return new FindComplexFoodsWithNameTask(foodRepository, name);
    }

    public GetComplexFoodIngredientsTask getComplexFoodIngredientsAsync(UUID foodId)
    {
        return new GetComplexFoodIngredientsTask(foodRepository, foodId);
    }

    public SaveComplexFoodTask addComplexFoodAsync(UserViewModel createdUser, ComplexFoodViewModel complexFoodViewModel) {
        return new SaveComplexFoodTask(foodRepository, createdUser, complexFoodViewModel);
    }

    public DeleteFoodTask deleteFoodAsync(UUID foodId)
    {
        return new DeleteFoodTask(foodRepository, foodId);
    }

    public CanDeleteFoodTask checkCanDeleteFood(UUID foodId)
    {
        return new CanDeleteFoodTask(foodRepository, foodId);
    }

    public IsUniqueSimpleFoodTask isSimpleFoodUnique(SimpleFoodViewModel simpleFoodViewModel)
    {
        return new IsUniqueSimpleFoodTask(foodRepository, simpleFoodViewModel);
    }

    public IsUniqueComplexFoodTask isComplexFoodUnique(ComplexFoodViewModel complexFoodViewModel)
    {
        return new IsUniqueComplexFoodTask(foodRepository, complexFoodViewModel);
    }
}
