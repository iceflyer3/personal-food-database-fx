/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.json;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.iceflyer3.pfd.data.json.mealplanning.*;
import com.iceflyer3.pfd.data.json.mealplanning.carbcycling.JsonCarbCyclingDay;
import com.iceflyer3.pfd.data.json.mealplanning.carbcycling.JsonCarbCyclingMeal;
import com.iceflyer3.pfd.data.json.mealplanning.carbcycling.JsonCarbCyclingMealConfiguration;
import com.iceflyer3.pfd.data.json.mealplanning.carbcycling.JsonCarbCyclingPlan;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Root / top level element for json serialization of meal planning data
 */
public class JsonMealPlanningData
{
    private int schemaVersion;

    private Collection<JsonMealPlan> mealPlans;
    private Collection<JsonMealPlanNutrientSuggestion> nutrientSuggestions;
    private Collection<JsonMealPlanDay> mealPlanDays;
    private Collection<JsonMealPlanMeal> mealPlanMeals;
    private Collection<JsonMealPlanMealIngredient> mealPlanMealIngredients;
    private Collection<JsonMealPlanCalendarDay> mealPlanCalendarDays;
    private Collection<JsonMealPlanCalendarDay> carbCyclingPlanCalendarDays;
    private Collection<JsonCarbCyclingPlan> carbCyclingPlans;
    private Collection<JsonCarbCyclingDay> carbCyclingDays;
    private Collection<JsonCarbCyclingMeal> carbCyclingMeals;
    private Collection<JsonMealPlanMealIngredient> carbCyclingPlanMealIngredients;
    private Collection<JsonCarbCyclingMealConfiguration> carbCyclingMealConfigurations;

    @JsonCreator
    public JsonMealPlanningData(@JsonProperty("schemaVersion") int schemaVersion)
    {
        this.schemaVersion = schemaVersion;

        this.mealPlans = new ArrayList<>();
        this.nutrientSuggestions = new ArrayList<>();
        this.mealPlanDays = new ArrayList<>();
        this.mealPlanMealIngredients = new ArrayList<>();
        this.mealPlanCalendarDays = new ArrayList<>();
        this.carbCyclingPlans = new ArrayList<>();
        this.carbCyclingDays = new ArrayList<>();
        this.carbCyclingMeals = new ArrayList<>();
        this.carbCyclingMealConfigurations = new ArrayList<>();
        this.carbCyclingPlanMealIngredients = new ArrayList<>();
        this.carbCyclingPlanCalendarDays = new ArrayList<>();
    }

    public int getSchemaVersion()
    {
        return schemaVersion;
    }

    public void setSchemaVersion(int schemaVersion)
    {
        this.schemaVersion = schemaVersion;
    }

    public Collection<JsonMealPlan> getMealPlans()
    {
        return mealPlans;
    }

    public void setMealPlans(Collection<JsonMealPlan> mealPlans)
    {
        this.mealPlans = mealPlans;
    }

    public Collection<JsonMealPlanNutrientSuggestion> getNutrientSuggestions()
    {
        return nutrientSuggestions;
    }

    public void setNutrientSuggestions(Collection<JsonMealPlanNutrientSuggestion> nutrientSuggestions)
    {
        this.nutrientSuggestions = nutrientSuggestions;
    }

    public Collection<JsonMealPlanDay> getMealPlanDays()
    {
        return mealPlanDays;
    }

    public void setMealPlanDays(Collection<JsonMealPlanDay> mealPlanDays)
    {
        this.mealPlanDays = mealPlanDays;
    }

    public Collection<JsonMealPlanMeal> getMealPlanMeals()
    {
        return mealPlanMeals;
    }

    public void setMealPlanMeals(Collection<JsonMealPlanMeal> mealPlanMeals)
    {
        this.mealPlanMeals = mealPlanMeals;
    }

    public Collection<JsonMealPlanMealIngredient> getMealPlanMealIngredients()
    {
        return mealPlanMealIngredients;
    }

    public void setMealPlanMealIngredients(Collection<JsonMealPlanMealIngredient> mealPlanMealIngredients)
    {
        this.mealPlanMealIngredients = mealPlanMealIngredients;
    }

    public Collection<JsonMealPlanCalendarDay> getMealPlanCalendarDays()
    {
        return mealPlanCalendarDays;
    }

    public void setMealPlanCalendarDays(Collection<JsonMealPlanCalendarDay> mealPlanCalendarDays)
    {
        this.mealPlanCalendarDays = mealPlanCalendarDays;
    }

    public Collection<JsonCarbCyclingPlan> getCarbCyclingPlans()
    {
        return carbCyclingPlans;
    }

    public void setCarbCyclingPlans(Collection<JsonCarbCyclingPlan> carbCyclingPlans)
    {
        this.carbCyclingPlans = carbCyclingPlans;
    }

    public Collection<JsonCarbCyclingDay> getCarbCyclingDays()
    {
        return carbCyclingDays;
    }

    public void setCarbCyclingDays(Collection<JsonCarbCyclingDay> carbCyclingDays)
    {
        this.carbCyclingDays = carbCyclingDays;
    }

    public Collection<JsonCarbCyclingMeal> getCarbCyclingMeals()
    {
        return carbCyclingMeals;
    }

    public void setCarbCyclingMeals(Collection<JsonCarbCyclingMeal> carbCyclingMeals)
    {
        this.carbCyclingMeals = carbCyclingMeals;
    }

    public Collection<JsonMealPlanMealIngredient> getCarbCyclingPlanMealIngredients()
    {
        return carbCyclingPlanMealIngredients;
    }

    public void setCarbCyclingPlanMealIngredients(Collection<JsonMealPlanMealIngredient> carbCyclingPlanMealIngredients)
    {
        this.carbCyclingPlanMealIngredients = carbCyclingPlanMealIngredients;
    }

    public Collection<JsonCarbCyclingMealConfiguration> getCarbCyclingMealConfigurations()
    {
        return carbCyclingMealConfigurations;
    }

    public void setCarbCyclingMealConfigurations(Collection<JsonCarbCyclingMealConfiguration> carbCyclingMealConfigurations)
    {
        this.carbCyclingMealConfigurations = carbCyclingMealConfigurations;
    }

    public Collection<JsonMealPlanCalendarDay> getCarbCyclingPlanCalendarDays()
    {
        return carbCyclingPlanCalendarDays;
    }

    public void setCarbCyclingPlanCalendarDays(Collection<JsonMealPlanCalendarDay> carbCyclingPlanCalendarDays)
    {
        this.carbCyclingPlanCalendarDays = carbCyclingPlanCalendarDays;
    }
}
