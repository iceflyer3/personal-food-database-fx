package com.iceflyer3.pfd.data.factory;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.repository.FoodRepository;
import com.iceflyer3.pfd.data.repository.RecipeRepository;
import com.iceflyer3.pfd.data.task.recipe.*;
import com.iceflyer3.pfd.ui.model.api.recipe.RecipeModel;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.recipe.RecipeViewModel;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class RecipeTaskFactory
{

    private final RecipeRepository recipeRepository;
    private final FoodRepository foodRepository;

    public RecipeTaskFactory(RecipeRepository recipeRepository, FoodRepository foodRepository) {
        this.foodRepository = foodRepository;
        this.recipeRepository = recipeRepository;
    }

    public GetAllRecipesForUserTask getAllRecipesForUserAsync(UUID forUserId)
    {
        return new GetAllRecipesForUserTask(recipeRepository, foodRepository, forUserId);
    }

    public GetMutableRecipeByIdTask getMutableRecipeByIdAsync(UUID recipeId)
    {
        return new GetMutableRecipeByIdTask(recipeRepository, foodRepository, recipeId);
    }

    public GetReadOnlyRecipeByIdTask getReadOnlyRecipeByIdAsync(UUID recipeId)
    {
        return new GetReadOnlyRecipeByIdTask(recipeRepository, foodRepository, recipeId);
    }

    public GetIngredientsForRecipeTask getIngredientsForRecipeAsync(UUID recipeId)
    {
        return new GetIngredientsForRecipeTask(recipeRepository, foodRepository, recipeId);
    }

    public GetStepsForRecipeTask getStepsForRecipeAsync(UUID recipeId)
    {
        return new GetStepsForRecipeTask(recipeRepository, foodRepository, recipeId);
    }

    public SaveRecipeTask saveRecipeAsync(UserViewModel user, RecipeViewModel recipeViewModel)
    {
        return new SaveRecipeTask(recipeRepository, foodRepository, user, recipeViewModel);
    }

    public DeleteRecipeTask deleteRecipeAsync(UUID recipeId)
    {
        return new DeleteRecipeTask(recipeRepository, foodRepository, recipeId);
    }

    public IsUniqueRecipeTask isUniqueAsync(RecipeModel recipe)
    {
        return new IsUniqueRecipeTask(recipeRepository, foodRepository, recipe);
    }
}
