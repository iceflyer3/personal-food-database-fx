package com.iceflyer3.pfd.data.task.recipe;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.entities.recipe.Recipe;
import com.iceflyer3.pfd.data.entities.views.RecipeNutritionalSummary;
import com.iceflyer3.pfd.data.mapping.recipe.RecipeMapper;
import com.iceflyer3.pfd.data.mapping.summary.NutritionalSummaryMapper;
import com.iceflyer3.pfd.data.repository.FoodRepository;
import com.iceflyer3.pfd.data.repository.RecipeRepository;
import com.iceflyer3.pfd.ui.model.viewmodel.recipe.RecipeViewModel;

import java.util.UUID;

/**
 * Retrieve a mutable instance of a recipe as specified by its id.
 */
public class GetMutableRecipeByIdTask extends AbstractRecipeTask<RecipeViewModel>{

    private final UUID recipeId;

    public GetMutableRecipeByIdTask(RecipeRepository recipeRepository, FoodRepository foodRepository, UUID recipeId) {
        super(recipeRepository, foodRepository);
        this.recipeId = recipeId;
    }

    @Override
    protected RecipeViewModel call() throws Exception {
        Recipe entity = recipeRepository.getRecipeById(recipeId);
        RecipeNutritionalSummary entityNutritionalSummary = recipeRepository.getNutritionalSummaryById(recipeId);
        return RecipeMapper.toModel(entity, NutritionalSummaryMapper.toModel(entityNutritionalSummary));
    }
}
