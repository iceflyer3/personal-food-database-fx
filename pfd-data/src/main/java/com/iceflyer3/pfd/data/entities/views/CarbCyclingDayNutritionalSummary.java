package com.iceflyer3.pfd.data.entities.views;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.constants.QueryConstants;
import org.hibernate.annotations.Immutable;

import jakarta.persistence.*;
import java.util.UUID;

@Entity
@Immutable
@Table(name = "carb_cycling_day_nutritional_summary")
@NamedQuery(
        name = QueryConstants.QUERY_MEAL_PLANNING_NUTRITIONAL_SUMMARY_FOR_CARB_CYCLING_DAY,
        query = "SELECT nutritionSummary FROM CarbCyclingDayNutritionalSummary nutritionSummary WHERE nutritionSummary.dayId = :dayId"
)
@NamedQuery(
        name = QueryConstants.QUERY_MEAL_PLANNING_NUTRITIONAL_SUMMARIES_FOR_CARB_CYCLING_DAYS_FOR_PLAN,
        query = "SELECT nutritionSummary FROM CarbCyclingDayNutritionalSummary nutritionSummary WHERE nutritionSummary.carbCyclingPlanId = :carbCyclingPlanId"
)
public class CarbCyclingDayNutritionalSummary extends AbstractCarbCyclingNutritionalSummary
{
    @Id
    @Column(name = "day_id")
    protected UUID dayId;

    @Column(name = "carb_cycling_plan_id")
    protected UUID carbCyclingPlanId;

    public UUID getDayId()
    {
        return dayId;
    }

    public UUID getCarbCyclingPlanId()
    {
        return carbCyclingPlanId;
    }
}
