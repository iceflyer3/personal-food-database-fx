/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.entities.food;

import com.iceflyer3.pfd.enums.FoodType;

import jakarta.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "food_meta_data")
public class FoodMetaData {

    public FoodMetaData() { }

    public FoodMetaData(UUID foodId, FoodType foodType)
    {
        this.foodId = foodId;
        this.foodType = foodType;
    }

    // Both the Simple and Complex food entities use @MapsId for a one-to-one relationship with this id.
    @Id
    @GeneratedValue
    @Column(name = "food_id")
    protected UUID foodId;

    @Enumerated(EnumType.STRING)
    @Column(name = "food_type", nullable = false)
    protected FoodType foodType;

    @OneToMany(mappedBy = "foodMeta", fetch = FetchType.EAGER)
    protected Set<FoodServingSize> servingSizes = new HashSet<>();

    public UUID getFoodId() {
        return foodId;
    }

    public FoodType getFoodType() {
        return foodType;
    }

    public void setFoodType(FoodType foodType) {
        this.foodType = foodType;
    }

    public Set<FoodServingSize> getServingSizes() {
        return servingSizes;
    }
}
