/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.json.food;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.UUID;

public class JsonFoodServingSize
{
    private UUID servingSizeId;
    private UUID foodMetaDataId;
    private String unitOfMeasure;
    private BigDecimal servingSize;

    public JsonFoodServingSize(
            @JsonProperty("servingSizeId") UUID servingSizeId,
            @JsonProperty("foodMetaDataId") UUID foodMetaDataId,
            @JsonProperty("unitOfMeasure") String unitOfMeasure,
            @JsonProperty("servingSize") BigDecimal servingSize)
    {
        this.servingSizeId = servingSizeId;
        this.foodMetaDataId = foodMetaDataId;
        this.unitOfMeasure = unitOfMeasure;
        this.servingSize = servingSize;
    }

    public UUID getServingSizeId()
    {
        return servingSizeId;
    }

    public void setServingSizeId(UUID servingSizeId)
    {
        this.servingSizeId = servingSizeId;
    }

    public UUID getFoodMetaDataId()
    {
        return foodMetaDataId;
    }

    public void setFoodMetaDataId(UUID foodMetaDataId)
    {
        this.foodMetaDataId = foodMetaDataId;
    }

    public String getUnitOfMeasure()
    {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(String unitOfMeasure)
    {
        this.unitOfMeasure = unitOfMeasure;
    }

    public BigDecimal getServingSize()
    {
        return servingSize;
    }

    public void setServingSize(BigDecimal servingSize)
    {
        this.servingSize = servingSize;
    }
}
