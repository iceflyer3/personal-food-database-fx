package com.iceflyer3.pfd.data.task.mealplanning.carbcycling.day;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanDay;
import com.iceflyer3.pfd.data.entities.views.CarbCyclingDayNutritionalSummary;
import com.iceflyer3.pfd.data.mapping.mealplanning.carbcycling.CarbCyclingDayMapper;
import com.iceflyer3.pfd.data.mapping.summary.NutritionalSummaryMapper;
import com.iceflyer3.pfd.data.repository.MealPlanningRepository;
import com.iceflyer3.pfd.data.task.mealplanning.AbstractMealPlanningTask;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingDayViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class GetCarbCyclingDaysForCarbCyclingPlanTask extends AbstractMealPlanningTask<List<CarbCyclingDayViewModel>>
{
    private final static Logger LOG = LoggerFactory.getLogger(GetCarbCyclingDaysForCarbCyclingPlanTask.class);

    private final UUID carbCyclingPlanId;

    public GetCarbCyclingDaysForCarbCyclingPlanTask(MealPlanningRepository mealPlanningRepository, UUID carbCyclingPlanId)
    {
        super(mealPlanningRepository);
        this.carbCyclingPlanId = carbCyclingPlanId;
    }

    @Override
    protected List<CarbCyclingDayViewModel> call() throws Exception
    {
        LOG.debug("Searching for carb cycling days for carb cycling plan {}", carbCyclingPlanId);

        // First, look up all the days.
        List<MealPlanDay> carbCyclingDayEntities = mealPlanningRepository.getCarbCyclingDaysForPlan(carbCyclingPlanId);
        List<CarbCyclingDayNutritionalSummary> nutritionalSummaries = mealPlanningRepository.getCarbCyclingDayNutritionalSummariesForPlan(carbCyclingPlanId);

        List<CarbCyclingDayViewModel> carbCyclingDayViewModels = new ArrayList<>();
        for(MealPlanDay dayEntity : carbCyclingDayEntities)
        {
            Optional<CarbCyclingDayNutritionalSummary> nutritionalSummaryEntity = nutritionalSummaries.stream().filter(summary -> summary.getDayId().equals(dayEntity.getDayId())).findFirst();
            if (nutritionalSummaryEntity.isPresent())
            {
                carbCyclingDayViewModels.add(CarbCyclingDayMapper.toModel(dayEntity, NutritionalSummaryMapper.toModel(nutritionalSummaryEntity.get())));
            }
            else
            {
                carbCyclingDayViewModels.add(CarbCyclingDayMapper.toModel(dayEntity, NutritionalSummary.empty()));
            }
        }

        LOG.debug("Found {} carb cycling days", carbCyclingDayViewModels.size());
        return carbCyclingDayViewModels;
    }
}
