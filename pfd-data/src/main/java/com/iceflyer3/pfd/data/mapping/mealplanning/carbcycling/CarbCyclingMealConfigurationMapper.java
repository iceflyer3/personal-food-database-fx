/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.mapping.mealplanning.carbcycling;

import com.iceflyer3.pfd.data.entities.mealplanning.carbcycling.CarbCyclingMealConfiguration;
import com.iceflyer3.pfd.data.entities.mealplanning.carbcycling.CarbCyclingPlan;
import com.iceflyer3.pfd.data.json.mealplanning.carbcycling.JsonCarbCyclingMealConfiguration;
import com.iceflyer3.pfd.enums.CarbCyclingMealRelation;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingMealConfigurationViewModel;

public class CarbCyclingMealConfigurationMapper
{
    private CarbCyclingMealConfigurationMapper() { }

    public static CarbCyclingMealConfigurationViewModel toModel(CarbCyclingMealConfiguration entity)
    {
        return new CarbCyclingMealConfigurationViewModel(
                entity.getMealConfigurationId(),
                entity.getPostWorkoutMealOffset(),
                entity.getCarbConsumptionMaximumPercent(),
                entity.getCarbConsumptionMinimumPercent(),
                entity.getPostWorkoutMealRelation(),
                entity.isForTrainingDay()
        );
    }

    public static JsonCarbCyclingMealConfiguration toJson(CarbCyclingMealConfiguration mealConfig)
    {
        return  new JsonCarbCyclingMealConfiguration(
                mealConfig.getMealConfigurationId(),
                mealConfig.getCarbCyclingPlan().getPlanId(),
                mealConfig.getCarbConsumptionMinimumPercent(),
                mealConfig.getCarbConsumptionMaximumPercent(),
                mealConfig.getPostWorkoutMealOffset(),
                mealConfig.getPostWorkoutMealRelation().toString(),
                mealConfig.isForTrainingDay()
        );
    }

    public static CarbCyclingMealConfiguration toFreshEntity(JsonCarbCyclingMealConfiguration jsonMealConfiguration, CarbCyclingPlan forPlan)
    {
        return new CarbCyclingMealConfiguration(
                null,
                forPlan,
                jsonMealConfiguration.carbConsumptionMaximumPercent(),
                jsonMealConfiguration.carbConsumptionMinimumPercent(),
                jsonMealConfiguration.postWorkoutMealOffset(),
                CarbCyclingMealRelation.valueOf(jsonMealConfiguration.postWorkoutMealRelation()),
                jsonMealConfiguration.isForTrainingDay()
        );
    }
}
