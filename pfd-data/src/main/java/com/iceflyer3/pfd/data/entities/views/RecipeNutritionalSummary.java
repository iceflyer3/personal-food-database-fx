package com.iceflyer3.pfd.data.entities.views;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.constants.QueryConstants;
import com.iceflyer3.pfd.data.entities.AbstractNutritionalDetails;

import jakarta.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "recipe_nutritional_summary")
@NamedQuery(name = QueryConstants.QUERY_RECIPE_CALCULATE_RECIPE_NUTRITIONAL_SUMMARY, query = "SELECT summary FROM RecipeNutritionalSummary summary WHERE summary.recipeId = :recipeId")
@NamedQuery(name = QueryConstants.QUERY_RECIPE_CALCULATE_ALL_NUTRITIONAL_SUMMARIES, query = "SELECT summary FROM RecipeNutritionalSummary summary")
public class RecipeNutritionalSummary extends AbstractNutritionalDetails
{
    @Id
    @Column(name = "recipe_id")
    private UUID recipeId;

    public UUID getRecipeId()
    {
        return recipeId;
    }
}
