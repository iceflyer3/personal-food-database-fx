package com.iceflyer3.pfd.data.request.mealplanning.day;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanDayViewModel;

import java.util.UUID;

public class CreateDayRequest {

    private final UUID forMealId;
    private final MealPlanDayViewModel day;

    public CreateDayRequest(UUID forMealId, MealPlanDayViewModel newDay)
    {
        this.forMealId = forMealId;
        this.day = newDay;
    }

    public UUID getForMealId()
    {
        return forMealId;
    }

    public MealPlanDayViewModel getDay() {
        return day;
    }
}
