/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


package com.iceflyer3.pfd.data.entities.mealplanning.carbcycling;

import com.iceflyer3.pfd.constants.QueryConstants;
import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanMeal;
import com.iceflyer3.pfd.enums.MealType;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.util.UUID;

@Entity
@Table(name = "carb_cycling_meal")
@NamedQuery(
        name = QueryConstants.QUERY_MEAL_PLANNING_CARB_CYCLING_FIND_MEALS_FOR_USER,
        query = "SELECT carbCyclingMeal FROM CarbCyclingMeal carbCyclingMeal WHERE carbCyclingMeal.carbCyclingDay.carbCyclingPlan.user.userId = :userId")
@NamedQuery(
        name = QueryConstants.QUERY_MEAL_PLANNING_CARB_CYCLING_FIND_MEAL_FOR_ID,
        query = "SELECT carbCyclingMeal.mealPlanMeal FROM CarbCyclingMeal carbCyclingMeal WHERE carbCyclingMeal.mealId = :mealId")
@NamedQuery(
        name = QueryConstants.QUERY_MEAL_PLANNING_CARB_CYCLING_FIND_MEALS_FOR_DAY,
        query = "SELECT carbCyclingMeal.mealPlanMeal FROM CarbCyclingMeal carbCyclingMeal WHERE carbCyclingMeal.carbCyclingDay.dayId = :dayId")
@NamedQuery(
        name = QueryConstants.QUERY_MEAL_PLANNING_FIND_CARB_CYCLING_MEAL_FOR_DAY_BY_MEAL_NUMBER,
        query = "SELECT mealPlanMeal FROM MealPlanMeal mealPlanMeal WHERE mealPlanMeal.day.dayId = :dayId AND mealPlanMeal.carbCyclingMeal.mealNumber = :mealNumber")
@NamedQuery(
        name = QueryConstants.QUERY_MEAL_PLANNING_REMOVE_CARB_CYCLING_MEALS_FOR_DAY,
        query = "DELETE FROM CarbCyclingMeal carbCyclingMeal WHERE carbCyclingMeal.mealPlanMeal.mealId IN (:mealPlanningMeals)")
public class CarbCyclingMeal {

    @Id
    protected UUID mealId;

    // See the comment in the SimpleFood entity about the use of a CascadeType here
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @MapsId
    @JoinColumn(name = "meal_id")
    protected MealPlanMeal mealPlanMeal;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "carb_cycling_day_id", nullable = false)
    protected CarbCyclingDay carbCyclingDay;

    @Enumerated(EnumType.STRING)
    @Column(name = "meal_type", nullable = false)
    protected MealType mealType;

    @Column(name = "meal_number", nullable = false)
    protected Integer mealNumber;

    @Column(name = "carb_consumption_percentage", nullable = false)
    protected BigDecimal carbConsumptionPercentage;

    @Column(name = "required_calories", precision = 2, nullable = false)
    protected BigDecimal requiredCalories;

    @Column(name = "required_protein", precision = 2, nullable = false)
    protected BigDecimal requiredProtein;

    @Column(name = "required_carbs", precision = 2, nullable = false)
    protected BigDecimal requiredCarbs;

    @Column(name = "required_fats", precision = 2, nullable = false)
    protected BigDecimal requiredFats;

    public CarbCyclingMeal() { }

    public CarbCyclingMeal(MealPlanMeal forMeal, CarbCyclingDay forCarbCyclingDay)
    {
        this.mealPlanMeal = forMeal;
        this.carbCyclingDay = forCarbCyclingDay;
    }

    public CarbCyclingMeal(
            UUID mealId,
            CarbCyclingDay forCarbCyclingDay,
            MealPlanMeal forMealPlanMeal,
            MealType mealType,
            int mealNumber,
            BigDecimal carbConsumptionPercentage,
            BigDecimal requiredCalories,
            BigDecimal requiredProtein,
            BigDecimal requiredCarbs,
            BigDecimal requiredFats
    )
    {
        this.mealId = mealId;
        this.carbCyclingDay = forCarbCyclingDay;
        this.mealPlanMeal = forMealPlanMeal;
        this.mealType = mealType;
        this.mealNumber = mealNumber;
        this.carbConsumptionPercentage = carbConsumptionPercentage;
        this.requiredCalories = requiredCalories;
        this.requiredProtein = requiredProtein;
        this.requiredCarbs = requiredCarbs;
        this.requiredFats = requiredFats;
    }

    public UUID getMealId() {
        return mealId;
    }

    public MealPlanMeal getMealPlanningMeal() {
        return mealPlanMeal;
    }

    public void setMealPlanningMeal(MealPlanMeal mealPlanMeal) {
        this.mealPlanMeal = mealPlanMeal;
    }

    public CarbCyclingDay getCarbCyclingDay()
    {
        return carbCyclingDay;
    }

    public void setCarbCyclingDay(CarbCyclingDay carbCyclingDay)
    {
        this.carbCyclingDay = carbCyclingDay;
    }

    public Integer getMealNumber() {
        return mealNumber;
    }

    public void setMealNumber(Integer mealNumber) {
        this.mealNumber = mealNumber;
    }

    public MealType getMealType() {
        return mealType;
    }

    public void setMealType(MealType mealType) {
        this.mealType = mealType;
    }

    public BigDecimal getCarbConsumptionPercentage()
    {
        return carbConsumptionPercentage;
    }

    public void setCarbConsumptionPercentage(BigDecimal carbConsumptionPercentage) { this.carbConsumptionPercentage = carbConsumptionPercentage; }

    public BigDecimal getRequiredCalories() {
        return requiredCalories;
    }

    public void setRequiredCalories(BigDecimal requiredCalories) {
        this.requiredCalories = requiredCalories;
    }

    public BigDecimal getRequiredProtein() {
        return requiredProtein;
    }

    public void setRequiredProtein(BigDecimal requiredProtein) {
        this.requiredProtein = requiredProtein;
    }

    public BigDecimal getRequiredCarbs() {
        return requiredCarbs;
    }

    public void setRequiredCarbs(BigDecimal requiredCarbs) {
        this.requiredCarbs = requiredCarbs;
    }

    public BigDecimal getRequiredFats() {
        return requiredFats;
    }

    public void setRequiredFats(BigDecimal requiredFats) {
        this.requiredFats = requiredFats;
    }
}
