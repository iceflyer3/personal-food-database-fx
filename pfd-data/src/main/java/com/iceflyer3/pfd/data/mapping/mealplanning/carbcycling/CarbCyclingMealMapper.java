package com.iceflyer3.pfd.data.mapping.mealplanning.carbcycling;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanMeal;
import com.iceflyer3.pfd.data.entities.mealplanning.carbcycling.CarbCyclingDay;
import com.iceflyer3.pfd.data.entities.mealplanning.carbcycling.CarbCyclingMeal;
import com.iceflyer3.pfd.data.json.mealplanning.carbcycling.JsonCarbCyclingMeal;
import com.iceflyer3.pfd.enums.MealType;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingMealViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;

public class CarbCyclingMealMapper {

    private CarbCyclingMealMapper() { }

    public static CarbCyclingMealViewModel toModel(MealPlanMeal entity, NutritionalSummary nutritionalSummary)
    {
        CarbCyclingMealViewModel viewModel = new CarbCyclingMealViewModel(entity.getMealId(), nutritionalSummary);
        viewModel.setName(entity.getMealName());
        viewModel.setLastModifiedDate(entity.getLastModifiedDate());
        viewModel.setMealType(entity.getCarbCyclingMeal().getMealType());
        viewModel.setMealNumber(entity.getCarbCyclingMeal().getMealNumber());
        viewModel.setCarbConsumptionPercentage(entity.getCarbCyclingMeal().getCarbConsumptionPercentage());
        viewModel.setRequiredCalories(entity.getCarbCyclingMeal().getRequiredCalories());
        viewModel.setRequiredProtein(entity.getCarbCyclingMeal().getRequiredProtein());
        viewModel.setRequiredCarbs(entity.getCarbCyclingMeal().getRequiredCarbs());
        viewModel.setRequiredFats(entity.getCarbCyclingMeal().getRequiredFats());
        return viewModel;
    }

    public static JsonCarbCyclingMeal toJson(CarbCyclingMeal meal)
    {
        return new JsonCarbCyclingMeal(
                meal.getMealId(),
                meal.getMealPlanningMeal().getMealId(),
                meal.getCarbCyclingDay().getDayId(),
                meal.getMealType().toString(),
                meal.getMealNumber(),
                meal.getCarbConsumptionPercentage(),
                meal.getRequiredCalories(),
                meal.getRequiredProtein(),
                meal.getRequiredCarbs(),
                meal.getRequiredFats()
        );
    }

    public static CarbCyclingMeal toFreshEntity(JsonCarbCyclingMeal jsonCarbCyclingMeal, CarbCyclingDay forCarbCyclingDay, MealPlanMeal forMealPlanMeal)
    {
        return new CarbCyclingMeal(
                null,
                forCarbCyclingDay,
                forMealPlanMeal,
                MealType.valueOf(jsonCarbCyclingMeal.mealType()),
                jsonCarbCyclingMeal.mealNumber(),
                jsonCarbCyclingMeal.carbConsumptionPercentage(),
                jsonCarbCyclingMeal.requiredCalories(),
                jsonCarbCyclingMeal.requiredProtein(),
                jsonCarbCyclingMeal.requiredCarbs(),
                jsonCarbCyclingMeal.requiredFats()
        );
    }
}
