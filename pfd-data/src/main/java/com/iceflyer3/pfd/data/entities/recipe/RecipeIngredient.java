/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.entities.recipe;

import com.iceflyer3.pfd.constants.QueryConstants;
import com.iceflyer3.pfd.data.entities.food.FoodMetaData;

import jakarta.persistence.*;

import java.math.BigDecimal;
import java.util.UUID;

@Entity
@Table(name = "recipe_ingredient", uniqueConstraints = @UniqueConstraint(columnNames = {"recipe_id", "food_id"}))
@NamedQuery(
        name = QueryConstants.QUERY_RECIPE_FIND_INGREDIENTS_FOR_RECIPE,
        query = "SELECT recipeIngredient FROM RecipeIngredient recipeIngredient WHERE recipeIngredient.recipe.recipeId = :recipeId")
@NamedQuery(
        name = QueryConstants.QUERY_RECIPE_FIND_INGREDIENTS_FOR_USER,
        query = "SELECT recipeIngredient FROM RecipeIngredient recipeIngredient WHERE recipeIngredient.recipe.createdUser.userId = :userId")
public class  RecipeIngredient {

    public RecipeIngredient() { }

    public RecipeIngredient(Recipe forRecipe, FoodMetaData food, BigDecimal numberOfServings)
    {
        this(null, forRecipe, food, numberOfServings, false, false);
    }

    public RecipeIngredient(
            UUID recipeIngredientId,
            Recipe forRecipe,
            FoodMetaData ingredientFood,
            BigDecimal numberOfServings,
            boolean isAlternate,
            boolean isOptional
    )
    {
        this.recipeIngredientId = recipeIngredientId;
        this.recipe = forRecipe;
        this.foodMeta = ingredientFood;
        this.numberOfServings = numberOfServings;

        this.isAlternate = isAlternate;
        this.isOptional = isOptional;
    }

    @Id
    @GeneratedValue
    @Column(name = "recipe_ingredient_id")
    protected UUID recipeIngredientId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "recipe_id", nullable = false)
    protected Recipe recipe;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "food_id", nullable = false)
    protected FoodMetaData foodMeta;

    @Column(name = "number_of_servings", nullable = false, precision = 2)
    protected BigDecimal numberOfServings;

    @Column(name = "is_alternate", nullable = false)
    protected Boolean isAlternate;

    @Column(name = "is_optional", nullable = false)
    protected Boolean isOptional;

    public UUID getRecipeIngredientId() {
        return recipeIngredientId;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public FoodMetaData getFoodMeta() {
        return foodMeta;
    }

    public BigDecimal getNumberOfServings() {
        return numberOfServings;
    }

    public void setNumberOfServings(BigDecimal numberOfServings) {
        this.numberOfServings = numberOfServings;
    }

    public Boolean getAlternate() {
        return isAlternate;
    }

    public void setAlternate(Boolean alternate) {
        isAlternate = alternate;
    }

    public Boolean getOptional() {
        return isOptional;
    }

    public void setOptional(Boolean optional) {
        isOptional = optional;
    }

    @Override
    public String toString() {
        return "RecipeIngredient{" +
                "recipeIngredientId=" + recipeIngredientId +
                ", recipe=" + recipe +
                ", foodMeta=" + foodMeta +
                ", numberOfServings=" + numberOfServings +
                ", isAlternate=" + isAlternate +
                ", isOptional=" + isOptional +
                '}';
    }
}
