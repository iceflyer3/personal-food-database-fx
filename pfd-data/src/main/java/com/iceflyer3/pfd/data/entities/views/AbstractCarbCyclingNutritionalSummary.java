package com.iceflyer3.pfd.data.entities.views;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.entities.AbstractNutritionalDetails;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import java.math.BigDecimal;

@MappedSuperclass
public abstract class AbstractCarbCyclingNutritionalSummary extends AbstractNutritionalDetails
{
    @Column(name = "required_calories", precision = 2)
    protected BigDecimal requiredCalories;

    @Column(name = "required_protein", precision = 2)
    protected BigDecimal requiredProtein;

    @Column(name = "required_carbs", precision = 2)
    protected BigDecimal requiredCarbohydrates;

    @Column(name = "required_fats", precision = 2)
    protected BigDecimal requiredFats;

    public BigDecimal getRequiredCalories()
    {
        return requiredCalories;
    }

    public BigDecimal getRequiredProtein()
    {
        return requiredProtein;
    }

    public BigDecimal getRequiredCarbohydrates()
    {
        return requiredCarbohydrates;
    }

    public BigDecimal getRequiredFats()
    {
        return requiredFats;
    }
}
