/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.task.food;

import com.iceflyer3.pfd.data.entities.food.ComplexFood;
import com.iceflyer3.pfd.data.entities.views.ComplexFoodNutritionalSummary;
import com.iceflyer3.pfd.data.mapping.food.FoodMapper;
import com.iceflyer3.pfd.data.mapping.summary.NutritionalSummaryMapper;
import com.iceflyer3.pfd.data.repository.FoodRepository;
import com.iceflyer3.pfd.ui.model.viewmodel.food.ComplexFoodViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;

import java.util.ArrayList;
import java.util.List;

public class FindComplexFoodsWithNameTask extends AbstractFoodTask<List<ComplexFoodViewModel>>
{
    private final String name;

    public FindComplexFoodsWithNameTask(FoodRepository foodRepository, String name)
    {
        super(foodRepository);
        this.name = name;
    }

    @Override
    protected List<ComplexFoodViewModel> call() throws Exception
    {
        List<ComplexFood> foodEntities = foodRepository.findComplexFoodsWithNameForUser(name);
        List<ComplexFoodViewModel> foodViewModels = new ArrayList<>();

        for(ComplexFood foodEntity: foodEntities)
        {
            ComplexFoodNutritionalSummary nutritionalSummaryEntity = foodRepository.getComplexFoodNutritionSummary(foodEntity.getFoodId());
            NutritionalSummary summaryViewModel = NutritionalSummaryMapper.toModel(nutritionalSummaryEntity);
            ComplexFoodViewModel viewModel = FoodMapper.toMutableModel(foodEntity, summaryViewModel);

            foodViewModels.add(viewModel);
        }

        return foodViewModels;
    }
}
