/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.json.food;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

public class JsonSimpleFood
{
    private UUID foodId;
    private String foodName;
    private String brandName;
    private String source;
    private UUID createdUserId;
    private UUID lastModifiedUserId;
    private LocalDateTime createdDate;
    private LocalDateTime lastModifiedDate;
    private int glycemicIndex;
    private BigDecimal calories;
    private BigDecimal protein;
    private BigDecimal carbohydrates;
    private BigDecimal totalFats;
    private BigDecimal transFat;
    private BigDecimal monounsaturatedFat;
    private BigDecimal polyunsaturatedFat;
    private BigDecimal saturatedFat;
    private BigDecimal sugars;
    private BigDecimal fiber;
    private BigDecimal cholesterol;
    private BigDecimal iron;
    private BigDecimal manganese;
    private BigDecimal copper;
    private BigDecimal zinc;
    private BigDecimal calcium;
    private BigDecimal magnesium;
    private BigDecimal phosphorus;
    private BigDecimal potassium;
    private BigDecimal sodium;
    private BigDecimal sulfur;
    private BigDecimal vitaminA;
    private BigDecimal vitaminC;
    private BigDecimal vitaminK;
    private BigDecimal vitaminD;
    private BigDecimal vitaminE;
    private BigDecimal vitaminB1;
    private BigDecimal vitaminB2;
    private BigDecimal vitaminB3;
    private BigDecimal vitaminB5;
    private BigDecimal vitaminB6;
    private BigDecimal vitaminB7;
    private BigDecimal vitaminB8;
    private BigDecimal vitaminB9;
    private BigDecimal vitaminB12;

    @JsonCreator
    public JsonSimpleFood(
            @JsonProperty("foodId") UUID foodId,
            @JsonProperty("foodName") String foodName,
            @JsonProperty("brandName") String brandName,
            @JsonProperty("source") String source,
            @JsonProperty("createdUserId") UUID createdUserId,
            @JsonProperty("lastModifiedUserId") UUID lastModifiedUserId,
            @JsonProperty("createdDate") LocalDateTime createdDate,
            @JsonProperty("lastModifiedDate") LocalDateTime lastModifiedDate,
            @JsonProperty("glycemicIndex") int glycemicIndex,
            @JsonProperty("calories") BigDecimal calories,
            @JsonProperty("protein") BigDecimal protein,
            @JsonProperty("carbohydrates") BigDecimal carbohydrates,
            @JsonProperty("totalFats") BigDecimal totalFats,
            @JsonProperty("transFat") BigDecimal transFat,
            @JsonProperty("monounsaturatedFat") BigDecimal monounsaturatedFat,
            @JsonProperty("polyunsaturatedFat") BigDecimal polyunsaturatedFat,
            @JsonProperty("saturatedFat") BigDecimal saturatedFat,
            @JsonProperty("sugars") BigDecimal sugars,
            @JsonProperty("fiber") BigDecimal fiber,
            @JsonProperty("cholesterol") BigDecimal cholesterol,
            @JsonProperty("iron") BigDecimal iron,
            @JsonProperty("manganese") BigDecimal manganese,
            @JsonProperty("copper") BigDecimal copper,
            @JsonProperty("zinc") BigDecimal zinc,
            @JsonProperty("calcium") BigDecimal calcium,
            @JsonProperty("magnesium") BigDecimal magnesium,
            @JsonProperty("phosphorus") BigDecimal phosphorus,
            @JsonProperty("potassium") BigDecimal potassium,
            @JsonProperty("sodium") BigDecimal sodium,
            @JsonProperty("sulfur") BigDecimal sulfur,
            @JsonProperty("vitaminA") BigDecimal vitaminA,
            @JsonProperty("vitaminC") BigDecimal vitaminC,
            @JsonProperty("vitaminK") BigDecimal vitaminK,
            @JsonProperty("vitaminD") BigDecimal vitaminD,
            @JsonProperty("vitaminE") BigDecimal vitaminE,
            @JsonProperty("vitaminB1") BigDecimal vitaminB1,
            @JsonProperty("vitaminB2") BigDecimal vitaminB2,
            @JsonProperty("vitaminB3") BigDecimal vitaminB3,
            @JsonProperty("vitaminB5") BigDecimal vitaminB5,
            @JsonProperty("vitaminB6") BigDecimal vitaminB6,
            @JsonProperty("vitaminB7") BigDecimal vitaminB7,
            @JsonProperty("vitaminB8") BigDecimal vitaminB8,
            @JsonProperty("vitaminB9") BigDecimal vitaminB9,
            @JsonProperty("vitaminB12") BigDecimal vitaminB12)
    {
        this.foodId = foodId;
        this.foodName = foodName;
        this.brandName = brandName;
        this.source = source;
        this.createdUserId = createdUserId;
        this.lastModifiedUserId = lastModifiedUserId;
        this.createdDate = createdDate;
        this.lastModifiedDate = lastModifiedDate;
        this.glycemicIndex = glycemicIndex;
        this.calories = calories;
        this.protein = protein;
        this.carbohydrates = carbohydrates;
        this.totalFats = totalFats;
        this.transFat = transFat;
        this.monounsaturatedFat = monounsaturatedFat;
        this.polyunsaturatedFat = polyunsaturatedFat;
        this.saturatedFat = saturatedFat;
        this.sugars = sugars;
        this.fiber = fiber;
        this.cholesterol = cholesterol;
        this.iron = iron;
        this.manganese = manganese;
        this.copper = copper;
        this.zinc = zinc;
        this.calcium = calcium;
        this.magnesium = magnesium;
        this.phosphorus = phosphorus;
        this.potassium = potassium;
        this.sodium = sodium;
        this.sulfur = sulfur;
        this.vitaminA = vitaminA;
        this.vitaminC = vitaminC;
        this.vitaminK = vitaminK;
        this.vitaminD = vitaminD;
        this.vitaminE = vitaminE;
        this.vitaminB1 = vitaminB1;
        this.vitaminB2 = vitaminB2;
        this.vitaminB3 = vitaminB3;
        this.vitaminB5 = vitaminB5;
        this.vitaminB6 = vitaminB6;
        this.vitaminB7 = vitaminB7;
        this.vitaminB8 = vitaminB8;
        this.vitaminB9 = vitaminB9;
        this.vitaminB12 = vitaminB12;
    }

    public UUID getFoodId()
    {
        return foodId;
    }

    public void setFoodId(UUID foodId)
    {
        this.foodId = foodId;
    }

    public String getFoodName()
    {
        return foodName;
    }

    public void setFoodName(String foodName)
    {
        this.foodName = foodName;
    }

    public String getBrandName()
    {
        return brandName;
    }

    public void setBrandName(String brandName)
    {
        this.brandName = brandName;
    }

    public String getSource()
    {
        return source;
    }

    public void setSource(String source)
    {
        this.source = source;
    }

    public UUID getCreatedUserId()
    {
        return createdUserId;
    }

    public void setCreatedUserId(UUID createdUserId)
    {
        this.createdUserId = createdUserId;
    }

    public UUID getLastModifiedUserId()
    {
        return lastModifiedUserId;
    }

    public void setLastModifiedUserId(UUID lastModifiedUserId)
    {
        this.lastModifiedUserId = lastModifiedUserId;
    }

    public LocalDateTime getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate)
    {
        this.createdDate = createdDate;
    }

    public LocalDateTime getLastModifiedDate()
    {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate)
    {
        this.lastModifiedDate = lastModifiedDate;
    }

    public int getGlycemicIndex()
    {
        return glycemicIndex;
    }

    public void setGlycemicIndex(int glycemicIndex)
    {
        this.glycemicIndex = glycemicIndex;
    }

    public BigDecimal getCalories()
    {
        return calories;
    }

    public void setCalories(BigDecimal calories)
    {
        this.calories = calories;
    }

    public BigDecimal getProtein()
    {
        return protein;
    }

    public void setProtein(BigDecimal protein)
    {
        this.protein = protein;
    }

    public BigDecimal getCarbohydrates()
    {
        return carbohydrates;
    }

    public void setCarbohydrates(BigDecimal carbohydrates)
    {
        this.carbohydrates = carbohydrates;
    }

    public BigDecimal getTotalFats()
    {
        return totalFats;
    }

    public void setTotalFats(BigDecimal totalFats)
    {
        this.totalFats = totalFats;
    }

    public BigDecimal getTransFat()
    {
        return transFat;
    }

    public void setTransFat(BigDecimal transFat)
    {
        this.transFat = transFat;
    }

    public BigDecimal getMonounsaturatedFat()
    {
        return monounsaturatedFat;
    }

    public void setMonounsaturatedFat(BigDecimal monounsaturatedFat)
    {
        this.monounsaturatedFat = monounsaturatedFat;
    }

    public BigDecimal getPolyunsaturatedFat()
    {
        return polyunsaturatedFat;
    }

    public void setPolyunsaturatedFat(BigDecimal polyunsaturatedFat)
    {
        this.polyunsaturatedFat = polyunsaturatedFat;
    }

    public BigDecimal getSaturatedFat()
    {
        return saturatedFat;
    }

    public void setSaturatedFat(BigDecimal saturatedFat)
    {
        this.saturatedFat = saturatedFat;
    }

    public BigDecimal getSugars()
    {
        return sugars;
    }

    public void setSugars(BigDecimal sugars)
    {
        this.sugars = sugars;
    }

    public BigDecimal getFiber()
    {
        return fiber;
    }

    public void setFiber(BigDecimal fiber)
    {
        this.fiber = fiber;
    }

    public BigDecimal getCholesterol()
    {
        return cholesterol;
    }

    public void setCholesterol(BigDecimal cholesterol)
    {
        this.cholesterol = cholesterol;
    }

    public BigDecimal getIron()
    {
        return iron;
    }

    public void setIron(BigDecimal iron)
    {
        this.iron = iron;
    }

    public BigDecimal getManganese()
    {
        return manganese;
    }

    public void setManganese(BigDecimal manganese)
    {
        this.manganese = manganese;
    }

    public BigDecimal getCopper()
    {
        return copper;
    }

    public void setCopper(BigDecimal copper)
    {
        this.copper = copper;
    }

    public BigDecimal getZinc()
    {
        return zinc;
    }

    public void setZinc(BigDecimal zinc)
    {
        this.zinc = zinc;
    }

    public BigDecimal getCalcium()
    {
        return calcium;
    }

    public void setCalcium(BigDecimal calcium)
    {
        this.calcium = calcium;
    }

    public BigDecimal getMagnesium()
    {
        return magnesium;
    }

    public void setMagnesium(BigDecimal magnesium)
    {
        this.magnesium = magnesium;
    }

    public BigDecimal getPhosphorus()
    {
        return phosphorus;
    }

    public void setPhosphorus(BigDecimal phosphorus)
    {
        this.phosphorus = phosphorus;
    }

    public BigDecimal getPotassium()
    {
        return potassium;
    }

    public void setPotassium(BigDecimal potassium)
    {
        this.potassium = potassium;
    }

    public BigDecimal getSodium()
    {
        return sodium;
    }

    public void setSodium(BigDecimal sodium)
    {
        this.sodium = sodium;
    }

    public BigDecimal getSulfur()
    {
        return sulfur;
    }

    public void setSulfur(BigDecimal sulfur)
    {
        this.sulfur = sulfur;
    }

    public BigDecimal getVitaminA()
    {
        return vitaminA;
    }

    public void setVitaminA(BigDecimal vitaminA)
    {
        this.vitaminA = vitaminA;
    }

    public BigDecimal getVitaminC()
    {
        return vitaminC;
    }

    public void setVitaminC(BigDecimal vitaminC)
    {
        this.vitaminC = vitaminC;
    }

    public BigDecimal getVitaminK()
    {
        return vitaminK;
    }

    public void setVitaminK(BigDecimal vitaminK)
    {
        this.vitaminK = vitaminK;
    }

    public BigDecimal getVitaminD()
    {
        return vitaminD;
    }

    public void setVitaminD(BigDecimal vitaminD)
    {
        this.vitaminD = vitaminD;
    }

    public BigDecimal getVitaminE()
    {
        return vitaminE;
    }

    public void setVitaminE(BigDecimal vitaminE)
    {
        this.vitaminE = vitaminE;
    }

    public BigDecimal getVitaminB1()
    {
        return vitaminB1;
    }

    public void setVitaminB1(BigDecimal vitaminB1)
    {
        this.vitaminB1 = vitaminB1;
    }

    public BigDecimal getVitaminB2()
    {
        return vitaminB2;
    }

    public void setVitaminB2(BigDecimal vitaminB2)
    {
        this.vitaminB2 = vitaminB2;
    }

    public BigDecimal getVitaminB3()
    {
        return vitaminB3;
    }

    public void setVitaminB3(BigDecimal vitaminB3)
    {
        this.vitaminB3 = vitaminB3;
    }

    public BigDecimal getVitaminB5()
    {
        return vitaminB5;
    }

    public void setVitaminB5(BigDecimal vitaminB5)
    {
        this.vitaminB5 = vitaminB5;
    }

    public BigDecimal getVitaminB6()
    {
        return vitaminB6;
    }

    public void setVitaminB6(BigDecimal vitaminB6)
    {
        this.vitaminB6 = vitaminB6;
    }

    public BigDecimal getVitaminB7()
    {
        return vitaminB7;
    }

    public void setVitaminB7(BigDecimal vitaminB7)
    {
        this.vitaminB7 = vitaminB7;
    }

    public BigDecimal getVitaminB8()
    {
        return vitaminB8;
    }

    public void setVitaminB8(BigDecimal vitaminB8)
    {
        this.vitaminB8 = vitaminB8;
    }

    public BigDecimal getVitaminB9()
    {
        return vitaminB9;
    }

    public void setVitaminB9(BigDecimal vitaminB9)
    {
        this.vitaminB9 = vitaminB9;
    }

    public BigDecimal getVitaminB12()
    {
        return vitaminB12;
    }

    public void setVitaminB12(BigDecimal vitaminB12)
    {
        this.vitaminB12 = vitaminB12;
    }
}