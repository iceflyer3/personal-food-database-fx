package com.iceflyer3.pfd.data.repository;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.FoodImportResult;
import com.iceflyer3.pfd.data.entities.recipe.Recipe;
import com.iceflyer3.pfd.data.entities.recipe.RecipeIngredient;
import com.iceflyer3.pfd.data.entities.recipe.RecipeStep;
import com.iceflyer3.pfd.data.entities.views.RecipeNutritionalSummary;
import com.iceflyer3.pfd.data.json.JsonRecipeData;
import com.iceflyer3.pfd.data.request.recipe.CreateRecipeRequest;
import com.iceflyer3.pfd.ui.model.api.recipe.RecipeModel;
import com.iceflyer3.pfd.ui.model.viewmodel.recipe.RecipeViewModel;

import java.util.List;
import java.util.UUID;

public interface RecipeRepository {
    Recipe getRecipeById(UUID recipeId);
    List<Recipe> getAllRecipes();
    List<Recipe> getAllRecipesForUser(UUID userId);
    List<RecipeNutritionalSummary> getAllNutritionalSummaries();
    RecipeNutritionalSummary getNutritionalSummaryById(UUID recipeId);

    List<RecipeIngredient> getIngredientsForRecipe(UUID recipeId);
    List<RecipeStep> getStepsForRecipe(UUID recipeId);

    Recipe createRecipe(CreateRecipeRequest creationRequest);
    Recipe updateRecipe(RecipeViewModel recipeViewModel);
    void deleteRecipe(UUID recipeId);
    boolean isRecipeUnique(RecipeModel recipeModel);

    JsonRecipeData exportData();
    void importData(JsonRecipeData jsonRecipeData, FoodImportResult importedFoods);
}
