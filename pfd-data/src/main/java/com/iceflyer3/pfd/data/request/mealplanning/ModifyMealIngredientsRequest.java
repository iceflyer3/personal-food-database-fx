package com.iceflyer3.pfd.data.request.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.ui.model.api.IngredientModel;

import java.util.*;

/**
 * Request class that allows creating or updating a group of ingredients for a meal.
 */
public class ModifyMealIngredientsRequest {
    private final UUID mealId;
    private final List<IngredientModel> ingredients;

    public ModifyMealIngredientsRequest(UUID mealId) {
        this.mealId = mealId;
        this.ingredients = new ArrayList<>();
    }

    public ModifyMealIngredientsRequest(UUID mealId, List<IngredientModel> ingredients) {
        this.mealId = mealId;
        this.ingredients = ingredients;
    }

    public UUID getMealId() {
        return mealId;
    }

    public void addIngredient(IngredientModel ingredientRequest)
    {
        this.ingredients.add(ingredientRequest);
    }

    public void removeIngredient(IngredientModel ingredientRequest)
    {
        this.ingredients.remove(ingredientRequest);
    }

    /**
     * Returns an immutable set of the ingredients. To add / remove ingredients
     * use the appropriate add / remove function.
     * @return An immutable set containing the ingredientss
     */
    public List<IngredientModel> getIngredients() {
        return Collections.unmodifiableList(ingredients);
    }
}
