/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.entities.mealplanning.carbcycling;

import com.iceflyer3.pfd.constants.QueryConstants;
import com.iceflyer3.pfd.data.entities.User;
import com.iceflyer3.pfd.data.entities.mealplanning.MealPlan;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.util.UUID;

/**
 * Represents a carb cycling plan for a user and contains the needed information for
 * other parts of the application to enforce the plan.
 *
 * The plan ties back to a single meal plan which contains the information like
 * TDEE calories that are needed for carb cycling.
 */
@Entity
@Table(name = "carb_cycling_plan")
@NamedQuery(
        name = QueryConstants.QUERY_MEAL_PLANNING_FIND_CARB_CYCLING_PLANS_FOR_USER,
        query = "SELECT carbCyclingPlan FROM CarbCyclingPlan carbCyclingPlan WHERE carbCyclingPlan.user.userId = :userId")
@NamedQuery(
        name = QueryConstants.QUERY_MEAL_PLANNING_FIND_CARB_CYCLING_PLANS_FOR_MEAL_PLAN,
        query = "SELECT carbCyclingPlan FROM CarbCyclingPlan carbCyclingPlan WHERE carbCyclingPlan.mealPlan.mealPlanId = :mealPlanId")
@NamedQuery(
        name = QueryConstants.QUERY_MEAL_PLANNING_FIND_ACTIVE_CARB_CYCLING_PLAN,
        query = "SELECT carbCyclingPlan FROM CarbCyclingPlan carbCyclingPlan WHERE carbCyclingPlan.user.userId = :userId AND carbCyclingPlan.isActive = true")
public class CarbCyclingPlan {

    public CarbCyclingPlan() { }

    public CarbCyclingPlan(
                            MealPlan forMealPlan,
                            User user,
                            String highLowDayRatio,
                            BigDecimal highCarbDaySurplusPercentage,
                            BigDecimal lowCarbDayDeficitPercentage,
                            Boolean shouldTimeFats,
                            Integer mealsPerDay)
    {
        this(
                null,
                forMealPlan,
                user,
                highLowDayRatio,
                highCarbDaySurplusPercentage,
                lowCarbDayDeficitPercentage,
                shouldTimeFats,
                mealsPerDay,
                false
        );
    }

    public CarbCyclingPlan(
            UUID planId,
            MealPlan forMealPlan,
            User user,
            String highLowDayRatio,
            BigDecimal highCarbDaySurplusPercentage,
            BigDecimal lowCarbDayDeficitPercentage,
            Boolean shouldTimeFats,
            Integer mealsPerDay,
            boolean isActive)
    {
        this.planId = planId;
        this.user = user;
        this.mealPlan = forMealPlan;
        this.highLowDayRatio = highLowDayRatio;
        this.highCarbDaySurplusPercentage = highCarbDaySurplusPercentage;
        this.lowCarbDayDeficitPercentage = lowCarbDayDeficitPercentage;
        this.shouldTimeFats = shouldTimeFats;
        this.mealsPerDay = mealsPerDay;
        this.isActive = isActive;
    }

    @Id
    @GeneratedValue
    @Column(name = "carb_cycling_plan_id")
    protected UUID planId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    protected User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "meal_plan_id", nullable = false)
    protected MealPlan mealPlan;

    // Uses the format highDayCount:lowDayCount. Ex 1:3 for one high carb day followed by three low carb days.
    @Column(name = "high_low_day_ratio", nullable = false)
    protected String highLowDayRatio;

    @Column(name = "meals_per_day", nullable = false)
    protected Integer mealsPerDay;

    @Column(name = "high_day_surplus_percentage", nullable = false)
    protected BigDecimal highCarbDaySurplusPercentage;

    @Column(name = "low_day_deficit_percentage", nullable = false)
    protected BigDecimal lowCarbDayDeficitPercentage;

    @Column(name = "should_time_fats", nullable = false)
    protected Boolean shouldTimeFats;

    @Column(name = "is_active", nullable = false)
    protected Boolean isActive;

    public UUID getPlanId() {
        return planId;
    }

    public User getUser() {
        return user;
    }

    public MealPlan getMealPlan()
    {
        return mealPlan;
    }

    public String getHighLowDayRatio() {
        return highLowDayRatio;
    }

    public BigDecimal getHighCarbDaySurplusPercentage()
    {
        return highCarbDaySurplusPercentage;
    }

    public BigDecimal getLowCarbDayDeficitPercentage()
    {
        return lowCarbDayDeficitPercentage;
    }

    public Integer getMealsPerDay() {
        return mealsPerDay;
    }

    public Boolean getShouldTimeFats() {
        return shouldTimeFats;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    @Override
    public String toString()
    {
        return "CarbCyclingPlan{" + "planId=" + planId + ", user=" + user + ", highLowDayRatio='" + highLowDayRatio + '\'' + ", mealsPerDay=" + mealsPerDay + ", shouldTimeFats=" + shouldTimeFats + ", isActive=" + isActive + '}';
    }
}
