package com.iceflyer3.pfd.data.calculator.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.enums.Nutrient;
import com.iceflyer3.pfd.enums.Sex;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.registration.MealPlanningCalculatedRegistration;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;

public class MealPlanningCalculator extends AbstractMealPlanCalculator {

    private static MealPlanningCalculator calculator;

    private MealPlanningCalculator() { }

    public static MealPlanningCalculator instance()
    {
        if (calculator == null)
        {
            calculator = new MealPlanningCalculator();
        }

        return calculator;
    }

    public CalculationResult calculatePlanRequirements(MealPlanningCalculatedRegistration calculationDetails)
    {
        BigDecimal tdeeCalories = calculateCalories(calculationDetails);
        BigDecimal dailyCalories = tdeeCalories;

        // If needed based upon goal, apply a surplus or deficit to the calorie count
        // Surplus / deficit amounts come as a recommendation from
        // https://www.bodybuilding.com/fun/macronutcal.htm
        switch(calculationDetails.getFitnessGoal())
        {
            case GAIN:
                dailyCalories = dailyCalories.add(BigDecimal.valueOf(500.0));
                break;
            case LOSE:
                // 20% by default. Maybe make this customizable in the future.
                BigDecimal deficit = dailyCalories.multiply(BigDecimal.valueOf(0.2));
                dailyCalories = dailyCalories.subtract(deficit);
                break;
            default:
                break;
        }

        Map<Nutrient, BigDecimal> macroSuggestions = calculateMacros(dailyCalories, calculationDetails);

        // We could potentially research recommended amounts for micronutrients and add that calculation in here at some point

        return new CalculationResult(tdeeCalories, dailyCalories, macroSuggestions);
    }

    /**
     * Calculates TDEE calories per day based upon the details from the calculation request
     * @param calcDetails The details of the calculation request
     * @return The amount TDEE calories needed per day
     */
    private BigDecimal calculateCalories(MealPlanningCalculatedRegistration calcDetails)
    {
        BigDecimal totalCaloriesNeeded = BigDecimal.ZERO;
        BigDecimal bmr = BigDecimal.ZERO;
        BigDecimal agePart;
        BigDecimal weightPart;
        BigDecimal heightPart;


        // First, calculate BMR
        // Based upon the bmr formulas described at https://www.muscleandstrength.com/articles/determine-daily-calorie-macronutrient-intake
        switch(calcDetails.getBmrFormula())
        {
            case KATCH_MCARDLE:
                BigDecimal leanBodyMass = calculateLeanBodyMass(calcDetails);
                BigDecimal stepOne = leanBodyMass.multiply(BigDecimal.valueOf(21.6));
                BigDecimal stepTwo = stepOne.add(BigDecimal.valueOf(370.0));
                bmr = stepTwo;
                break;
            case CUNNINGHAM:
                bmr = calculateLeanBodyMass(calcDetails).multiply(BigDecimal.valueOf(22.0)).add(BigDecimal.valueOf(500));
                break;
            case MIFFLIN_ST_JEOR:
                weightPart = calcDetails.getWeight().multiply(BigDecimal.valueOf(10.0));
                heightPart = calcDetails.getHeight().multiply(BigDecimal.valueOf(6.25));
                agePart = BigDecimal.valueOf(5.0).multiply(BigDecimal.valueOf((double)calcDetails.getAge()));

                if (calcDetails.getSex() == Sex.MALE)
                {
                    bmr = weightPart.add(heightPart).add(BigDecimal.valueOf(5.0)).subtract(agePart);
                }
                else
                {
                    bmr = weightPart.add(heightPart).subtract(agePart).subtract(BigDecimal.valueOf(161.0));
                }
                break;
            case ORIGINAL_HARRIS_BENEDICT:
                if (calcDetails.getSex() == Sex.MALE)
                {
                    weightPart = calcDetails.getWeight().multiply(BigDecimal.valueOf(13.7516));
                    heightPart = calcDetails.getHeight().multiply(BigDecimal.valueOf(5.0033));
                    agePart = BigDecimal.valueOf(6.7550).multiply(BigDecimal.valueOf((double)calcDetails.getAge()));

                    bmr = weightPart.add(heightPart).add(BigDecimal.valueOf(66.4730)).subtract(agePart);
                }
                else
                {
                    weightPart = calcDetails.getWeight().multiply(BigDecimal.valueOf(9.5634));
                    heightPart = calcDetails.getHeight().multiply(BigDecimal.valueOf(1.8496));
                    agePart = BigDecimal.valueOf(4.6756).multiply(BigDecimal.valueOf((double)calcDetails.getAge()));

                    bmr = weightPart.add(heightPart).add(BigDecimal.valueOf(655.0955)).subtract(agePart);
                }
                break;
            case REVISED_HARRIS_BENEDICT:
                if (calcDetails.getSex() == Sex.MALE)
                {
                    weightPart = calcDetails.getWeight().multiply(BigDecimal.valueOf(13.397));
                    heightPart = calcDetails.getHeight().multiply(BigDecimal.valueOf(4.799));
                    agePart = BigDecimal.valueOf(5.677).multiply(BigDecimal.valueOf((double)calcDetails.getAge()));

                    bmr = weightPart.add(heightPart).add(BigDecimal.valueOf(88.362)).subtract(agePart);
                }
                else
                {
                    weightPart = calcDetails.getWeight().multiply(BigDecimal.valueOf(9.247));
                    heightPart = calcDetails.getHeight().multiply(BigDecimal.valueOf(3.098));
                    agePart = BigDecimal.valueOf(4.330).multiply(BigDecimal.valueOf((double)calcDetails.getAge()));

                    bmr = weightPart.add(heightPart).add(BigDecimal.valueOf(447.593)).subtract(agePart);
                }
                break;
        }

        // Second, multiply by activity level to get maintenance calories
        // These adjustments also come from the same source as BMR formulas.
        // https://www.muscleandstrength.com/articles/determine-daily-calorie-macronutrient-intake
        switch(calcDetails.getActivityLevel())
        {
            case SEDENTARY:
                totalCaloriesNeeded = bmr.multiply(BigDecimal.valueOf(1.2));
                break;
            case LIGHT:
                totalCaloriesNeeded = bmr.multiply(BigDecimal.valueOf(1.375));
                break;
            case MODERATE:
                totalCaloriesNeeded = bmr.multiply(BigDecimal.valueOf(1.55));
                break;
            case HIGH:
                totalCaloriesNeeded = bmr.multiply(BigDecimal.valueOf(1.725));
                break;
            case EXTREME:
                totalCaloriesNeeded = bmr.multiply(BigDecimal.valueOf(1.9));
                break;
        }

        return totalCaloriesNeeded;
    }

    /**
     * Calculates how many grams of each macronutrient is needed per day. This calculation is based on the
     * total number of daily calories needed (which factor in any surplus / deficit) instead of tdee calories.
     *
     * These values will be ignored if there is an active carb cycling plan as carb cycling has its own strategy
     * for how to calculate macronutrient requirements.
     *
     * @param dailyNeededCalories Total needed calories per day (including any surplus or deficit)
     * @param calcDetails The details of the calculation request
     * @return The amount of each macronutrient, in grams, that is needed per day
     */
    private Map<Nutrient, BigDecimal> calculateMacros(BigDecimal dailyNeededCalories, MealPlanningCalculatedRegistration calcDetails)
    {
        BigDecimal oneHundred = BigDecimal.valueOf(100.0);
        BigDecimal proteinPercent = calcDetails.getProteinPercent().setScale(2, RoundingMode.HALF_UP).divide(oneHundred, RoundingMode.HALF_UP);
        BigDecimal carbsPercent = calcDetails.getCarbsPercent().setScale(2, RoundingMode.HALF_UP).divide(oneHundred, RoundingMode.HALF_UP);
        BigDecimal fatsPercent = calcDetails.getFatsPercent().setScale(2, RoundingMode.HALF_UP).divide(oneHundred, RoundingMode.HALF_UP);

        /*
         * The caloric computations here are based on __total__ daily calories instead of
         * __remaining__ daily calories after calculation of each macronutrient as is the case with
         *  carb cycling. Meaning while carb cycling macronutrients are calculated in relation to each
         * other, regular meal plan macronutrients are only calculated as X percent of total daily calories.
         */
        BigDecimal caloriesFromProtein = dailyNeededCalories.multiply(proteinPercent);
        BigDecimal caloriesFromCarbs = dailyNeededCalories.multiply(carbsPercent);
        BigDecimal caloriesFromFats = dailyNeededCalories.multiply(fatsPercent);

        return calculateMacronutrientGramsFromCalories(caloriesFromProtein, caloriesFromCarbs, caloriesFromFats);
    }

    private BigDecimal calculateLeanBodyMass(MealPlanningCalculatedRegistration calcDetails)
    {
        BigDecimal bodyFatPercentage = calcDetails.getBodyFactPercentage().setScale(2, RoundingMode.HALF_UP).divide(BigDecimal.valueOf(100.0), RoundingMode.HALF_UP);
        BigDecimal bodyFatWeight = calcDetails.getWeight().multiply(bodyFatPercentage);
        return calcDetails.getWeight().subtract(bodyFatWeight);
    }
}
