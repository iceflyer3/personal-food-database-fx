/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.entities.food;

import com.iceflyer3.pfd.constants.QueryConstants;
import com.iceflyer3.pfd.data.entities.AbstractNutritionalDetails;
import com.iceflyer3.pfd.data.entities.User;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name="simple_food")
@NamedQuery(name = QueryConstants.QUERY_FOOD_FIND_ALL_SIMPLE, query = "SELECT food FROM SimpleFood food")
@NamedQuery(name = QueryConstants.QUERY_FOOD_FIND_SIMPLE_FOR_USER, query = "SELECT food FROM SimpleFood food WHERE food.createdUser.userId = :userId")
@NamedQuery(name = QueryConstants.QUERY_FOOD_FIND_ALL_SIMPLE_WITH_IDS, query = "SELECT food FROM SimpleFood food WHERE food.foodId IN (:foodIds)")
@NamedQuery(
        name = QueryConstants.QUERY_FOOD_FIND_SIMPLE_BY_NAME_AND_BRAND,
        query = "SELECT food FROM SimpleFood food WHERE food.name = :foodName AND food.brand = :foodBrand")
public class SimpleFood extends AbstractNutritionalDetails
{
    public SimpleFood() { }

    public SimpleFood(String foodName, String brandName, String source, BigDecimal calories, User createdUser)
    {
        this.foodId = null;
        this.name = foodName;
        this.brand = brandName;
        this.source = source;
        this.calories = calories;

        this.createdUser = createdUser;
        this.createdDate = LocalDateTime.now();

        this.lastModifiedUser = createdUser;
        this.lastModifiedDate = LocalDateTime.now();
    }

    public SimpleFood(
            UUID foodId,
            FoodMetaData foodMetaData,
            String foodName,
            String brandName,
            String source,
            BigDecimal calories,
            User createdUser,
            User lastModifiedUser,
            LocalDateTime createdDate,
            LocalDateTime lastModifiedDate,
            Integer glycemicIndex,
            BigDecimal protein,
            BigDecimal carbohydrates,
            BigDecimal totalFats,
            BigDecimal transFat,
            BigDecimal monounsaturatedFat,
            BigDecimal polyunsaturatedFat,
            BigDecimal saturatedFat,
            BigDecimal sugars,
            BigDecimal fiber,
            BigDecimal cholesterol,
            BigDecimal iron,
            BigDecimal manganese,
            BigDecimal copper,
            BigDecimal zinc,
            BigDecimal calcium,
            BigDecimal magnesium,
            BigDecimal phosphorus,
            BigDecimal potassium,
            BigDecimal sodium,
            BigDecimal sulfur,
            BigDecimal vitaminA,
            BigDecimal vitaminC,
            BigDecimal vitaminK,
            BigDecimal vitaminD,
            BigDecimal vitaminE,
            BigDecimal vitaminB1,
            BigDecimal vitaminB2,
            BigDecimal vitaminB3,
            BigDecimal vitaminB5,
            BigDecimal vitaminB6,
            BigDecimal vitaminB7,
            BigDecimal vitaminB8,
            BigDecimal vitaminB9,
            BigDecimal vitaminB12)
    {
        this.foodId = foodId;
        this.foodMetaData = foodMetaData;

        this.name = foodName;
        this.brand = brandName;
        this.source = source;
        this.calories = calories;
        this.glycemicIndex = glycemicIndex;

        this.createdUser = createdUser;
        this.createdDate = createdDate;

        this.lastModifiedUser = lastModifiedUser;
        this.lastModifiedDate = lastModifiedDate;

        this.protein = protein;
        this.carbohydrates = carbohydrates;
        this.totalFats = totalFats;
        this.transFat = transFat;
        this.monounsaturatedFat = monounsaturatedFat;
        this.polyunsaturatedFat = polyunsaturatedFat;
        this.saturatedFat = saturatedFat;
        this.sugars = sugars;
        this.fiber = fiber;
        this.cholesterol = cholesterol;
        this.iron = iron;
        this.manganese = manganese;
        this.copper = copper;
        this.zinc = zinc;
        this.calcium = calcium;
        this.magnesium = magnesium;
        this.phosphorus = phosphorus;
        this.potassium = potassium;
        this.sodium = sodium;
        this.sulfur = sulfur;
        this.vitaminA = vitaminA;
        this.vitaminC = vitaminC;
        this.vitaminK = vitaminK;
        this.vitaminD = vitaminD;
        this.vitaminE = vitaminE;
        this.vitaminB1 = vitaminB1;
        this.vitaminB2 = vitaminB2;
        this.vitaminB3 = vitaminB3;
        this.vitaminB5 = vitaminB5;
        this.vitaminB6 = vitaminB6;
        this.vitaminB7 = vitaminB7;
        this.vitaminB8 = vitaminB8;
        this.vitaminB9 = vitaminB9;
        this.vitaminB12 = vitaminB12;
    }

    @Id
    protected UUID foodId;

    /*
     * Frankly, I'm not sure why the cascade type has to be set here, but during data import
     * Hibernate kept throwing an AssertionFailure with a "null identifier" message for this
     * entity.
     *
     * All the relevant ids were set, both for this entity and for parent and child entities,
     * and entities were merged in the correct order with the relationships set but the error
     * persisted. For magic reasons setting the CascadeType to MERGE fixes that and prevents
     * the failure from being thrown.
     *
     * Ideally, I'd like to actually understand what was going wrong without that and why it is
     * required here. But at least setting it stops the AssertionFailure from being thrown, so I
     * guess that will have to suffice.
     */
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @MapsId
    @JoinColumn(name = "food_id")
    protected FoodMetaData foodMetaData;

    @Column(name = "food_name", nullable = false)
    protected String name;

    @Column(name = "brand", nullable = false)
    protected String brand;

    @Column(name = "info_src", nullable = false, columnDefinition = "varchar(max)")
    protected String source;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "created_user_id", nullable = false)
    protected User createdUser;

    @Column(name="created_date", nullable = false, columnDefinition = "timestamp")
    protected LocalDateTime createdDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "last_modified_user_id", nullable = false)
    protected User lastModifiedUser;

    @Column(name="last_modified_date", nullable = false, columnDefinition = "timestamp")
    protected LocalDateTime lastModifiedDate;

    @Column(name = "glycemic_index")
    protected Integer glycemicIndex;

    public UUID getFoodId() {
        return foodId;
    }

    public User getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(User createdUser) {
        this.createdUser = createdUser;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public FoodMetaData getFoodMetaData() {
        return foodMetaData;
    }

    public void setFoodMetaData(FoodMetaData foodMetaData) {
        this.foodMetaData = foodMetaData;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public User getLastModifiedUser() {
        return lastModifiedUser;
    }

    public void setLastModifiedUser(User lastModifiedUser) {
        this.lastModifiedUser = lastModifiedUser;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Integer getGlycemicIndex() {
        return glycemicIndex;
    }

    public void setGlycemicIndex(Integer glycemicIndex) {
        this.glycemicIndex = glycemicIndex;
    }

    // Nutritional data getters are declared in the abstract base class
    public void setCalories(BigDecimal calories) {
        this.calories = calories;
    }

    public void setProtein(BigDecimal protein) {
        this.protein = protein;
    }

    public void setCarbohydrates(BigDecimal carbohydrates) {
        this.carbohydrates = carbohydrates;
    }

    public void setTotalFats(BigDecimal totalFats) {
        this.totalFats = totalFats;
    }

    public void setTransFat(BigDecimal transFat) {
        this.transFat = transFat;
    }

    public void setMonounsaturatedFat(BigDecimal monounsaturatedFat) {
        this.monounsaturatedFat = monounsaturatedFat;
    }

    public void setPolyunsaturatedFat(BigDecimal polyunsaturatedFat) {
        this.polyunsaturatedFat = polyunsaturatedFat;
    }

    public void setSaturatedFat(BigDecimal saturatedFat) {
        this.saturatedFat = saturatedFat;
    }

    public void setSugars(BigDecimal sugars) {
        this.sugars = sugars;
    }

    public void setFiber(BigDecimal fiber) {
        this.fiber = fiber;
    }

    public void setCholesterol(BigDecimal cholesterol) {
        this.cholesterol = cholesterol;
    }

    public void setIron(BigDecimal iron) {
        this.iron = iron;
    }

    public void setManganese(BigDecimal manganese) {
        this.manganese = manganese;
    }

    public void setCopper(BigDecimal copper) {
        this.copper = copper;
    }

    public void setZinc(BigDecimal zinc) {
        this.zinc = zinc;
    }

    public void setCalcium(BigDecimal calcium) {
        this.calcium = calcium;
    }

    public void setMagnesium(BigDecimal magnesium) {
        this.magnesium = magnesium;
    }

    public void setPhosphorus(BigDecimal phosphorus) {
        this.phosphorus = phosphorus;
    }

    public void setPotassium(BigDecimal potassium) {
        this.potassium = potassium;
    }

    public void setSodium(BigDecimal sodium) {
        this.sodium = sodium;
    }

    public void setSulfur(BigDecimal sulfur) {
        this.sulfur = sulfur;
    }

    public void setVitaminA(BigDecimal vitaminA) {
        this.vitaminA = vitaminA;
    }

    public void setVitaminC(BigDecimal vitaminC) {
        this.vitaminC = vitaminC;
    }

    public void setVitaminK(BigDecimal vitaminK) {
        this.vitaminK = vitaminK;
    }

    public void setVitaminD(BigDecimal vitaminD) {
        this.vitaminD = vitaminD;
    }

    public void setVitaminE(BigDecimal vitaminE) {
        this.vitaminE = vitaminE;
    }

    public void setVitaminB1(BigDecimal vitaminB1) {
        this.vitaminB1 = vitaminB1;
    }

    public void setVitaminB2(BigDecimal vitaminB2) {
        this.vitaminB2 = vitaminB2;
    }

    public void setVitaminB3(BigDecimal vitaminB3) {
        this.vitaminB3 = vitaminB3;
    }

    public void setVitaminB5(BigDecimal vitaminB5) {
        this.vitaminB5 = vitaminB5;
    }

    public void setVitaminB6(BigDecimal vitaminB6) {
        this.vitaminB6 = vitaminB6;
    }

    public void setVitaminB7(BigDecimal vitaminB7) {
        this.vitaminB7 = vitaminB7;
    }

    public void setVitaminB8(BigDecimal vitaminB8) {
        this.vitaminB8 = vitaminB8;
    }

    public void setVitaminB9(BigDecimal vitaminB9) {
        this.vitaminB9 = vitaminB9;
    }

    public void setVitaminB12(BigDecimal vitaminB12) {
        this.vitaminB12 = vitaminB12;
    }
}
