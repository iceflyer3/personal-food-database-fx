package com.iceflyer3.pfd.data.task.mealplanning.carbcycling.meal;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanMeal;
import com.iceflyer3.pfd.data.entities.views.CarbCyclingMealNutritionalSummary;
import com.iceflyer3.pfd.data.mapping.mealplanning.carbcycling.CarbCyclingMealMapper;
import com.iceflyer3.pfd.data.mapping.summary.NutritionalSummaryMapper;
import com.iceflyer3.pfd.data.repository.MealPlanningRepository;
import com.iceflyer3.pfd.data.task.mealplanning.AbstractMealPlanningTask;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingMealViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;

import java.util.Optional;
import java.util.UUID;

public class GetCarbCyclingMealByIdTask extends AbstractMealPlanningTask<CarbCyclingMealViewModel>
{
    private final UUID mealId;

    public GetCarbCyclingMealByIdTask(MealPlanningRepository mealPlanningRepository, UUID mealId)
    {
        super(mealPlanningRepository);
        this.mealId = mealId;
    }

    @Override
    protected CarbCyclingMealViewModel call() throws Exception
    {
        MealPlanMeal mealEntity = mealPlanningRepository.getCarbCyclingMealById(mealId);

        Optional<CarbCyclingMealNutritionalSummary> nutritionalSummaryEntity = mealPlanningRepository.getCarbCyclingMealNutritionalSummary(mealId);

        if (nutritionalSummaryEntity.isPresent())
        {
            return CarbCyclingMealMapper.toModel(mealEntity, NutritionalSummaryMapper.toModel(nutritionalSummaryEntity.get()));
        }
        else
        {
            return CarbCyclingMealMapper.toModel(mealEntity, NutritionalSummary.empty());
        }
    }
}
