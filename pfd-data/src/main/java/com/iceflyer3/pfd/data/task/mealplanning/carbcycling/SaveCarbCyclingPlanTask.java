package com.iceflyer3.pfd.data.task.mealplanning.carbcycling;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.data.entities.mealplanning.carbcycling.CarbCyclingPlan;
import com.iceflyer3.pfd.data.mapping.mealplanning.carbcycling.CarbCyclingPlanMapper;
import com.iceflyer3.pfd.data.repository.MealPlanningRepository;
import com.iceflyer3.pfd.data.request.mealplanning.CreateCarbCyclingPlanRequest;
import com.iceflyer3.pfd.data.task.mealplanning.AbstractMealPlanningTask;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingPlanViewModel;

import java.util.UUID;

public class SaveCarbCyclingPlanTask extends AbstractMealPlanningTask<CarbCyclingPlanViewModel>
{

    private final UUID forUserId;
    private final UUID forMealPlanId;
    private final CarbCyclingPlanViewModel planDetails;

    public SaveCarbCyclingPlanTask(MealPlanningRepository mealPlanningRepository, UUID forUserId, UUID forMealPlanId, CarbCyclingPlanViewModel planDetails) {
        super(mealPlanningRepository);
        this.forUserId = forUserId;
        this.planDetails = planDetails;
        this.forMealPlanId = forMealPlanId;
    }

    @Override
    protected CarbCyclingPlanViewModel call() throws Exception {
        CarbCyclingPlan entityPlan;
        if (planDetails.getId() == null)
        {
            CreateCarbCyclingPlanRequest createCarbCyclingPlanRequest = new CreateCarbCyclingPlanRequest(forUserId, forMealPlanId, planDetails);
            entityPlan = mealPlanningRepository.createCarbCyclingPlan(createCarbCyclingPlanRequest);
        }
        else
        {
            // In theory this should never actually happen. But we'll cover it just in case.
            throw new UnsupportedOperationException(String.format("Cannot create a new carb cycling plan because this plan has already been previously created. Plan Id: %s", planDetails.getId()));
        }

        return CarbCyclingPlanMapper.toModel(entityPlan);
    }
}
