package com.iceflyer3.pfd.data.task.mealplanning.carbcycling.meal;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanMeal;
import com.iceflyer3.pfd.data.entities.views.CarbCyclingMealNutritionalSummary;
import com.iceflyer3.pfd.data.mapping.mealplanning.carbcycling.CarbCyclingMealMapper;
import com.iceflyer3.pfd.data.mapping.summary.NutritionalSummaryMapper;
import com.iceflyer3.pfd.data.repository.MealPlanningRepository;
import com.iceflyer3.pfd.data.task.mealplanning.AbstractMealPlanningTask;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingMealViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class GetCarbCyclingMealsForDayTask extends AbstractMealPlanningTask<List<CarbCyclingMealViewModel>>
{
    private static final Logger LOG = LoggerFactory.getLogger(GetCarbCyclingMealsForDayTask.class);
    private final UUID forDayId;

    public GetCarbCyclingMealsForDayTask(MealPlanningRepository mealPlanningRepository, UUID forDayId)
    {
        super(mealPlanningRepository);
        this.forDayId = forDayId;
    }

    @Override
    protected List<CarbCyclingMealViewModel> call() throws Exception
    {
        List<MealPlanMeal> carbCyclingDayEntities = mealPlanningRepository.getCarbCyclingMealsForDay(forDayId);
        List<CarbCyclingMealNutritionalSummary> nutritionalSummaries = mealPlanningRepository.getCarbCyclingMealNutritionalSummariesForDay(forDayId);

        List<CarbCyclingMealViewModel> mealViewModels = new ArrayList<>();
        for(MealPlanMeal mealEntity : carbCyclingDayEntities)
        {
            Optional<CarbCyclingMealNutritionalSummary> nutritionalSummaryEntity = nutritionalSummaries.stream().filter(summary -> summary.getMealId().equals(mealEntity.getMealId())).findFirst();
            if (nutritionalSummaryEntity.isPresent())
            {
                mealViewModels.add(CarbCyclingMealMapper.toModel(mealEntity, NutritionalSummaryMapper.toModel(nutritionalSummaryEntity.get())));
            }
            else
            {
                mealViewModels.add(CarbCyclingMealMapper.toModel(mealEntity, NutritionalSummary.empty()));
            }
        }

        LOG.debug("Found {} carb cycling meals for carb cycling day {}", carbCyclingDayEntities.size(), forDayId);
        return mealViewModels;
    }
}
