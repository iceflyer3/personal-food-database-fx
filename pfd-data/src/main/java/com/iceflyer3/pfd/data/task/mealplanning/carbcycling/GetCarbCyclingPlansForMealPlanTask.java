package com.iceflyer3.pfd.data.task.mealplanning.carbcycling;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.data.entities.mealplanning.carbcycling.CarbCyclingPlan;
import com.iceflyer3.pfd.data.mapping.mealplanning.carbcycling.CarbCyclingPlanMapper;
import com.iceflyer3.pfd.data.repository.MealPlanningRepository;
import com.iceflyer3.pfd.data.task.mealplanning.AbstractMealPlanningTask;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingPlanViewModel;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class GetCarbCyclingPlansForMealPlanTask extends AbstractMealPlanningTask<List<CarbCyclingPlanViewModel>>
{
    private final UUID mealPlanId;

    public GetCarbCyclingPlansForMealPlanTask(MealPlanningRepository mealPlanningRepository, UUID mealPlanId)
    {
        super(mealPlanningRepository);
        this.mealPlanId = mealPlanId;
    }

    @Override
    protected List<CarbCyclingPlanViewModel> call() throws Exception
    {
        List<CarbCyclingPlan> planEntities = mealPlanningRepository.getCarbCyclingPlansForMealPlan(mealPlanId);
        return planEntities.stream().map(CarbCyclingPlanMapper::toModel).collect(Collectors.toList());
    }
}
