/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.json;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.iceflyer3.pfd.data.json.recipe.*;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Root / top level element for json serialization of recipe data
 */
public class JsonRecipeData
{
    private int schemaVersion;
    private Collection<JsonRecipe> recipes;
    private Collection<JsonRecipeIngredient> recipeIngredients;
    private Collection<JsonRecipeServingSize> recipeServingSizes;
    private Collection<JsonRecipeStep> recipeSteps;
    private Collection<JsonRecipeStepIngredient> recipeStepIngredients;

    @JsonCreator
    public JsonRecipeData(@JsonProperty("schemaVersion") int schemaVersion)
    {
        this.schemaVersion = schemaVersion;
        this.recipes = new ArrayList<>();
        this.recipeIngredients = new ArrayList<>();
        this.recipeServingSizes = new ArrayList<>();
        this.recipeSteps = new ArrayList<>();
        this.recipeStepIngredients = new ArrayList<>();
    }

    public int getSchemaVersion()
    {
        return schemaVersion;
    }

    public void setSchemaVersion(int schemaVersion)
    {
        this.schemaVersion = schemaVersion;
    }

    public Collection<JsonRecipe> getRecipes()
    {
        return recipes;
    }

    public void setRecipes(Collection<JsonRecipe> recipes)
    {
        this.recipes = recipes;
    }

    public Collection<JsonRecipeIngredient> getRecipeIngredients()
    {
        return recipeIngredients;
    }

    public void setRecipeIngredients(Collection<JsonRecipeIngredient> recipeIngredients)
    {
        this.recipeIngredients = recipeIngredients;
    }

    public Collection<JsonRecipeServingSize> getRecipeServingSizes()
    {
        return recipeServingSizes;
    }

    public void setRecipeServingSizes(Collection<JsonRecipeServingSize> recipeServingSizes)
    {
        this.recipeServingSizes = recipeServingSizes;
    }

    public Collection<JsonRecipeStep> getRecipeSteps()
    {
        return recipeSteps;
    }

    public void setRecipeSteps(Collection<JsonRecipeStep> recipeSteps)
    {
        this.recipeSteps = recipeSteps;
    }

    public Collection<JsonRecipeStepIngredient> getRecipeStepIngredients()
    {
        return recipeStepIngredients;
    }

    public void setRecipeStepIngredients(Collection<JsonRecipeStepIngredient> recipeStepIngredients)
    {
        this.recipeStepIngredients = recipeStepIngredients;
    }
}
