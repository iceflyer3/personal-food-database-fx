package com.iceflyer3.pfd.data.entities.views;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.constants.QueryConstants;
import org.hibernate.annotations.Immutable;

import jakarta.persistence.*;
import java.util.UUID;

/**
 * Represents an ingredient owner for a simple food.
 *
 * Mapped to the simple_foods_as_ingredients view that searches for uses of the simple food
 * as an ingredient in either a complex food or a recipe and returns the name of the food
 * or recipe if found
 */
@Entity
@Immutable
@Table(name = "simple_foods_as_ingredients")
@NamedQuery(name = QueryConstants.QUERY_FOOD_FIND_FOOD_AS_INGREDIENT, query = "SELECT ingredientOwner FROM FoodAsIngredient ingredientOwner WHERE ingredientOwner.foodId = :foodId")
public class FoodAsIngredient {
    @Id
    @Column(name="name")
    protected String ingredientOwnerName;

    // The Id of the simple food that is used as the ingredient
    @Column(name="food_id")
    protected UUID foodId;

    // The type of the entity that owns the ingredient
    @Column(name="owner_type")
    protected String ownerType;

    public FoodAsIngredient() { }

    public String getIngredientOwnerName() {
        return ingredientOwnerName;
    }

    public UUID getFoodId() {
        return foodId;
    }

    public String getOwnerType() {
        return ownerType;
    }
}
