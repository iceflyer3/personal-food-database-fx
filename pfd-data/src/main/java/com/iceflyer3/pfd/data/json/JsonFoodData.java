/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.json;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.iceflyer3.pfd.data.json.food.*;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Root / top level element for json serialization of food data
 */
public class JsonFoodData
{
    private int schemaVersion;

    private Collection<JsonFoodMetaData> foodMetaData;
    private Collection<JsonFoodServingSize> foodServingSizes;
    private Collection<JsonSimpleFood> simpleFoods;
    private Collection<JsonComplexFood> complexFoods;
    private Collection<JsonComplexFoodIngredient> complexFoodIngredients;

    @JsonCreator
    public JsonFoodData(@JsonProperty("schemaVersion") int schemaVersion)
    {
        this.schemaVersion = schemaVersion;
        this.foodMetaData = new ArrayList<>();
        this.foodServingSizes = new ArrayList<>();
        this.simpleFoods = new ArrayList<>();
        this.complexFoods = new ArrayList<>();
        this.complexFoodIngredients = new ArrayList<>();
    }

    public int getSchemaVersion()
    {
        return schemaVersion;
    }

    public void setSchemaVersion(int schemaVersion)
    {
        this.schemaVersion = schemaVersion;
    }

    public Collection<JsonFoodMetaData> getFoodMetaData()
    {
        return foodMetaData;
    }

    public void setFoodMetaData(Collection<JsonFoodMetaData> foodMetaData)
    {
        this.foodMetaData = foodMetaData;
    }

    public Collection<JsonFoodServingSize> getFoodServingSizes()
    {
        return foodServingSizes;
    }

    public void setFoodServingSizes(Collection<JsonFoodServingSize> foodServingSizes)
    {
        this.foodServingSizes = foodServingSizes;
    }

    public Collection<JsonSimpleFood> getSimpleFoods()
    {
        return simpleFoods;
    }

    public void setSimpleFoods(Collection<JsonSimpleFood> simpleFoods)
    {
        this.simpleFoods = simpleFoods;
    }

    public Collection<JsonComplexFood> getComplexFoods()
    {
        return complexFoods;
    }

    public void setComplexFoods(Collection<JsonComplexFood> complexFoods)
    {
        this.complexFoods = complexFoods;
    }

    public Collection<JsonComplexFoodIngredient> getComplexFoodIngredients()
    {
        return complexFoodIngredients;
    }

    public void setComplexFoodIngredients(Collection<JsonComplexFoodIngredient> complexFoodIngredients)
    {
        this.complexFoodIngredients = complexFoodIngredients;
    }
}
