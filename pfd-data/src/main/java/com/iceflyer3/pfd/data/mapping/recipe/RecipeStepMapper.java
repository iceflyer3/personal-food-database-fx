package com.iceflyer3.pfd.data.mapping.recipe;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.entities.recipe.Recipe;
import com.iceflyer3.pfd.data.entities.recipe.RecipeStep;
import com.iceflyer3.pfd.data.json.recipe.JsonRecipeStep;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.viewmodel.recipe.RecipeStepViewModel;

import java.util.List;

public class RecipeStepMapper {

    private RecipeStepMapper() { }

    public static RecipeStepViewModel toModel(RecipeStep entity, List<IngredientModel> stepIngredientViewModels)
    {
        return new RecipeStepViewModel(
                entity.getStepId(),
                entity.getName(),
                entity.getDirections(),
                entity.getStepNumber(),
                stepIngredientViewModels);
    }

    public static JsonRecipeStep toJson(RecipeStep recipeStep)
    {
        return new JsonRecipeStep(
                recipeStep.getStepId(),
                recipeStep.getRecipe().getRecipeId(),
                recipeStep.getName(),
                recipeStep.getDirections(),
                recipeStep.getStepNumber()
        );
    }

    /**
     * Returns a new (and detached) RecipeStep instance hydrated from json without a set id.
     * @param jsonRecipeStep json from which to hydrate the new instance
     * @param forRecipe Associated Recipe entity
     * @return RecipeStep instance hydrated from json without a set id
     */
    public static RecipeStep toFreshEntity(JsonRecipeStep jsonRecipeStep, Recipe forRecipe)
    {
        return new RecipeStep(
                null,
                forRecipe,
                jsonRecipeStep.getStepName(),
                jsonRecipeStep.getDirections(),
                jsonRecipeStep.getStepNumber()
        );
    }
}
