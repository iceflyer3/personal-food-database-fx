package com.iceflyer3.pfd.data.task.mealplanning.day;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanDay;
import com.iceflyer3.pfd.data.entities.views.MealPlanDayNutritionalSummary;
import com.iceflyer3.pfd.data.mapping.mealplanning.MealPlanDayMapper;
import com.iceflyer3.pfd.data.mapping.summary.NutritionalSummaryMapper;
import com.iceflyer3.pfd.data.repository.MealPlanningRepository;
import com.iceflyer3.pfd.data.task.mealplanning.AbstractMealPlanningTask;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanDayViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class GetDaysForMealPlanTask extends AbstractMealPlanningTask<List<MealPlanDayViewModel>>
{

    private final static Logger LOG = LoggerFactory.getLogger(GetDaysForMealPlanTask.class);
    private final UUID forMealPlanId;

    public GetDaysForMealPlanTask(MealPlanningRepository mealPlanningRepository, UUID forMealPlanId)
    {
        super(mealPlanningRepository);
        this.forMealPlanId = forMealPlanId;
    }

    @Override
    protected List<MealPlanDayViewModel> call() throws Exception
    {
        LOG.debug("Searching for days for meal plan {}", forMealPlanId);
        List<MealPlanDay> dayEntities = mealPlanningRepository.getDaysForMealPlan(forMealPlanId);
        List<MealPlanDayNutritionalSummary> dayNutritionalConsumptionSummaries = mealPlanningRepository.getDayNutritionalSummariesForMealPlan(forMealPlanId);

        // Associate day nutritional summaries to the days to which they summarize
        List<MealPlanDayViewModel> dayViewModels = new ArrayList<>();
        for(MealPlanDay dayEntity : dayEntities)
        {
            Optional<MealPlanDayNutritionalSummary> summaryEntity = dayNutritionalConsumptionSummaries.stream().filter(summary -> summary.getDayId().equals(dayEntity.getDayId())).findFirst();
            if (summaryEntity.isPresent())
            {
                dayViewModels.add(MealPlanDayMapper.toModel(dayEntity, NutritionalSummaryMapper.toModel(summaryEntity.get())));
            }
            else
            {
                dayViewModels.add(MealPlanDayMapper.toModel(dayEntity, NutritionalSummary.empty()));
            }
        }

        LOG.debug("Found {} days for meal plan {}", dayEntities.size(), forMealPlanId);
        return dayViewModels;
    }
}
