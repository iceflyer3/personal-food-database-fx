package com.iceflyer3.pfd.data.request.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import java.util.UUID;

public class ChangeMealPlanActivationStatusRequest
{
    private final UUID mealPlanId;
    private final boolean isActive;

    public ChangeMealPlanActivationStatusRequest(UUID mealPlanId, boolean isActive)
    {
        this.mealPlanId = mealPlanId;
        this.isActive = isActive;
    }

    public UUID getMealPlanId()
    {
        return mealPlanId;
    }

    public boolean isActive()
    {
        return isActive;
    }
}
