/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data;

import com.iceflyer3.pfd.exception.ApplicationConfigurationException;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * Utility class that makes retrieving information about the application configuration from
 * the environment convenient while also handling missing configuration information
 */
@Component
public class AppConfigUtils
{
    private final static Logger LOG = LoggerFactory.getLogger(AppConfigUtils.class);
    private final Environment environment;

    public AppConfigUtils(Environment environment)
    {
        this.environment = environment;
    }

    /**
     * Attempt to get an application configuration property from the Spring Environment.
     *
     * Throws an ApplicationConfigurationException (runtime exception) if the property cannot
     * be loaded.
     *
     * @param propertyName The name of the property to load
     * @param clazz The expected class of the property
     * @return The value of the configuration property, if found.
     */
    public <T> T getProperty(String propertyName, Class<T> clazz)
    {
        T propertyValue = this.environment.getProperty(propertyName, clazz);

        if (propertyValue == null)
        {
            String errorMessage = String.format("Application configuration property %s could not be loaded from appConfig.properties", propertyName);
            LOG.error(errorMessage);
            throw new ApplicationConfigurationException(errorMessage);
        }

        return propertyValue;
    }
}
