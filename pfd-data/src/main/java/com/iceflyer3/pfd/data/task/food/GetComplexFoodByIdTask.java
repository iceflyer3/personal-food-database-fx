package com.iceflyer3.pfd.data.task.food;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.entities.food.ComplexFood;
import com.iceflyer3.pfd.data.entities.views.ComplexFoodNutritionalSummary;
import com.iceflyer3.pfd.data.mapping.food.FoodMapper;
import com.iceflyer3.pfd.data.mapping.summary.NutritionalSummaryMapper;
import com.iceflyer3.pfd.data.repository.FoodRepository;
import com.iceflyer3.pfd.ui.model.viewmodel.food.ComplexFoodViewModel;

import java.util.UUID;

public class GetComplexFoodByIdTask extends AbstractFoodTask<ComplexFoodViewModel>{

    private UUID foodId;

    public GetComplexFoodByIdTask(FoodRepository foodRepository, UUID foodId) {
        super(foodRepository);
        this.foodId = foodId;
    }

    @Override
    protected ComplexFoodViewModel call() throws Exception {
        ComplexFood entity = foodRepository.getComplexFoodById(foodId);
        ComplexFoodNutritionalSummary nutritionalSummaryEntity = foodRepository.getComplexFoodNutritionSummary(foodId);
        return FoodMapper.toMutableModel(entity, NutritionalSummaryMapper.toModel(nutritionalSummaryEntity));
    }
}
