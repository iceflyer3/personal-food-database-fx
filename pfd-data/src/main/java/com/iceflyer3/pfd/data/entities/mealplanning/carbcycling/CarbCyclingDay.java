package com.iceflyer3.pfd.data.entities.mealplanning.carbcycling;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.constants.QueryConstants;
import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanDay;
import com.iceflyer3.pfd.enums.CarbCyclingDayType;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.util.UUID;

@Entity
@Table(name = "carb_cycling_day")
@NamedQuery(
        name = QueryConstants.QUERY_MEAL_PLANNING_CARB_CYCLING_FIND_DAYS_FOR_USER,
        query = "SELECT carbCyclingDay FROM CarbCyclingDay carbCyclingDay WHERE carbCyclingDay.carbCyclingPlan.user.userId = :userId")
@NamedQuery(
        name = QueryConstants.QUERY_MEAL_PLANNING_CARB_CYCLING_FIND_DAY_FOR_ID,
        query = "SELECT carbCyclingDay.mealPlanDay FROM CarbCyclingDay carbCyclingDay WHERE carbCyclingDay.mealPlanDay.dayId = :dayId")
@NamedQuery(
        name = QueryConstants.QUERY_MEAL_PLANNING_FIND_CARB_CYCLING_DAYS_FOR_PLAN,
        query = "SELECT carbCyclingDay.mealPlanDay FROM CarbCyclingDay carbCyclingDay WHERE carbCyclingDay.carbCyclingPlan.planId = :carbCyclingPlanId")
@NamedQuery(
        name = QueryConstants.QUERY_MEAL_PLANNING_REMOVE_CARB_CYCLING_DAYS_FOR_MEAL_PLAN,
        query = "DELETE FROM CarbCyclingDay carbCyclingDay WHERE carbCyclingDay.mealPlanDay.dayId IN (:mealPlanningDays)")
public class CarbCyclingDay
{
    @Id
    protected UUID dayId;

    // See the comment in the SimpleFood entity about the use of a CascadeType here
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @MapsId
    @JoinColumn(name = "day_id")
    protected MealPlanDay mealPlanDay;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "carb_cycling_plan_id", nullable = false)
    protected CarbCyclingPlan carbCyclingPlan;

    @Enumerated(EnumType.STRING)
    @Column(name="day_type", nullable = false)
    protected CarbCyclingDayType dayType;

    @Column(name = "is_training_day", nullable = false)
    protected Boolean isTrainingDay;

    @Column(name = "required_calories", nullable = false, precision = 2)
    protected BigDecimal requiredCalories;

    @Column(name = "required_protein", nullable = false, precision = 2)
    protected BigDecimal requiredProtein;

    @Column(name = "required_carbs", nullable = false, precision = 2)
    protected BigDecimal requiredCarbs;

    @Column(name = "required_fats", nullable = false, precision = 2)
    protected BigDecimal requiredFats;

    public CarbCyclingDay() { }

    public CarbCyclingDay(MealPlanDay forDay, CarbCyclingPlan forCarbCyclingPlan, CarbCyclingDayType dayType)
    {
        this.dayId = null;
        this.carbCyclingPlan = forCarbCyclingPlan;
        this.mealPlanDay = forDay;
        this.dayType = dayType;
    }

    public CarbCyclingDay(
            UUID dayId,
            MealPlanDay forDay,
            CarbCyclingPlan forCarbCyclingPlan,
            CarbCyclingDayType dayType,
            boolean isTrainingDay,
            BigDecimal requiredCalories,
            BigDecimal requiredProtein,
            BigDecimal requiredCarbs,
            BigDecimal requiredFats
    )
    {
        this.dayId = dayId;
        this.mealPlanDay = forDay;
        this.carbCyclingPlan = forCarbCyclingPlan;
        this.dayType = dayType;
        this.isTrainingDay = isTrainingDay;
        this.requiredCalories = requiredCalories;
        this.requiredProtein = requiredProtein;
        this.requiredCarbs = requiredCarbs;
        this.requiredFats = requiredFats;
    }

    public UUID getDayId()
    {
        return dayId;
    }

    public CarbCyclingPlan getCarbCyclingPlan()
    {
        return carbCyclingPlan;
    }

    public MealPlanDay getMealPlanningDay()
    {
        return mealPlanDay;
    }

    public void setMealPlanningDay(MealPlanDay mealPlanDay)
    {
        this.mealPlanDay = mealPlanDay;
    }

    public CarbCyclingDayType getDayType()
    {
        return dayType;
    }

    public void setDayType(CarbCyclingDayType dayType)
    {
        this.dayType = dayType;
    }

    public Boolean isTrainingDay()
    {
        return isTrainingDay;
    }

    public void setIsTrainingDay(Boolean trainingDay)
    {
        isTrainingDay = trainingDay;
    }

    public BigDecimal getRequiredCalories()
    {
        return requiredCalories;
    }

    public void setRequiredCalories(BigDecimal requiredCalories)
    {
        this.requiredCalories = requiredCalories;
    }

    public BigDecimal getRequiredProtein()
    {
        return requiredProtein;
    }

    public void setRequiredProtein(BigDecimal requiredProtein)
    {
        this.requiredProtein = requiredProtein;
    }

    public BigDecimal getRequiredCarbs()
    {
        return requiredCarbs;
    }

    public void setRequiredCarbs(BigDecimal requiredCarbs)
    {
        this.requiredCarbs = requiredCarbs;
    }

    public BigDecimal getRequiredFats()
    {
        return requiredFats;
    }

    public void setRequiredFats(BigDecimal requiredFats)
    {
        this.requiredFats = requiredFats;
    }

    @Override
    public String toString()
    {
        return "CarbCyclingDay{"
                + "dayId=" + dayId
                + ", dayType=" + dayType
                + ", isTrainingDay=" + isTrainingDay
                + ", requiredCalories=" + requiredCalories
                + ", requiredProtein=" + requiredProtein
                + ", requiredCarbs=" + requiredCarbs
                + ", requiredFats=" + requiredFats
                + '}';
    }
}
