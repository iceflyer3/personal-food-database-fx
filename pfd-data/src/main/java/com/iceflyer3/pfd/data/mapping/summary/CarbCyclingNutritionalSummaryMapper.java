package com.iceflyer3.pfd.data.mapping.summary;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.entities.views.AbstractCarbCyclingNutritionalSummary;
import com.iceflyer3.pfd.data.entities.views.CarbCyclingDayNutritionalSummary;
import com.iceflyer3.pfd.data.entities.views.CarbCyclingMealNutritionalSummary;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.CarbCyclingNutritionalSummary;

public class CarbCyclingNutritionalSummaryMapper
{
    private CarbCyclingNutritionalSummaryMapper() { }

    public static CarbCyclingNutritionalSummary toModel(CarbCyclingDayNutritionalSummary entity)
    {
        return getModelFromEntity(entity);
    }

    public static CarbCyclingNutritionalSummary toModel(CarbCyclingMealNutritionalSummary entity)
    {
        return getModelFromEntity(entity);
    }

    private static <T extends AbstractCarbCyclingNutritionalSummary> CarbCyclingNutritionalSummary getModelFromEntity(T entity)
    {
        return new CarbCyclingNutritionalSummary(
                entity.getRequiredCalories(),
                entity.getRequiredProtein(),
                entity.getRequiredCarbohydrates(),
                entity.getRequiredFats(),
                entity.getCalories(),
                entity.getProtein(),
                entity.getTotalFats(),
                entity.getTransFat(),
                entity.getMonounsaturatedFat(),
                entity.getPolyunsaturatedFat(),
                entity.getSaturatedFat(),
                entity.getCarbohydrates(),
                entity.getSugars(),
                entity.getFiber(),
                entity.getCholesterol(),
                entity.getIron(),
                entity.getManganese(),
                entity.getCopper(),
                entity.getZinc(),
                entity.getCalcium(),
                entity.getMagnesium(),
                entity.getPhosphorus(),
                entity.getPotassium(),
                entity.getSodium(),
                entity.getSulfur(),
                entity.getVitaminA(),
                entity.getVitaminC(),
                entity.getVitaminK(),
                entity.getVitaminD(),
                entity.getVitaminE(),
                entity.getVitaminB1(),
                entity.getVitaminB2(),
                entity.getVitaminB3(),
                entity.getVitaminB5(),
                entity.getVitaminB6(),
                entity.getVitaminB7(),
                entity.getVitaminB8(),
                entity.getVitaminB9(),
                entity.getVitaminB12()
        );
    }
}
