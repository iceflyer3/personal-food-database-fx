package com.iceflyer3.pfd.data.request.mealplanning.meal;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import java.util.UUID;

/**
 * Request used to modify a meal after initial creation.
 * The totals of a meal never change after initial creation.
 * The only part of the meal that can change is the name.
 */
public class ModifyMealRequest {

    private final UUID mealId;
    private final String mealName;

    public ModifyMealRequest(UUID mealId, String mealName) {
        this.mealId = mealId;
        this.mealName = mealName;
    }

    public UUID getMealId() {
        return mealId;
    }

    public String getMealName() {
        return mealName;
    }
}
