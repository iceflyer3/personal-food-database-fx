package com.iceflyer3.pfd.data.repository;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.constants.QueryConstants;
import com.iceflyer3.pfd.data.AppConfigUtils;
import com.iceflyer3.pfd.data.FoodImportResult;
import com.iceflyer3.pfd.data.entities.User;
import com.iceflyer3.pfd.data.entities.food.FoodMetaData;
import com.iceflyer3.pfd.data.entities.food.SimpleFood;
import com.iceflyer3.pfd.data.entities.recipe.*;
import com.iceflyer3.pfd.data.entities.views.FoodNutritionalDetails;
import com.iceflyer3.pfd.data.entities.views.RecipeNutritionalSummary;
import com.iceflyer3.pfd.data.json.JsonRecipeData;
import com.iceflyer3.pfd.data.json.food.JsonComplexFoodIngredient;
import com.iceflyer3.pfd.data.json.recipe.*;
import com.iceflyer3.pfd.data.mapping.IngredientMapper;
import com.iceflyer3.pfd.data.mapping.ServingSizeMapper;
import com.iceflyer3.pfd.data.mapping.recipe.RecipeMapper;
import com.iceflyer3.pfd.data.mapping.recipe.RecipeStepMapper;
import com.iceflyer3.pfd.data.mapping.summary.NutritionalSummaryMapper;
import com.iceflyer3.pfd.data.request.recipe.CreateRecipeRequest;
import com.iceflyer3.pfd.exception.DataImportException;
import com.iceflyer3.pfd.exception.InvalidApplicationStateException;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.api.ServingSizeModel;
import com.iceflyer3.pfd.ui.model.api.recipe.RecipeModel;
import com.iceflyer3.pfd.ui.model.api.recipe.RecipeStepModel;
import com.iceflyer3.pfd.ui.model.viewmodel.recipe.RecipeViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;
import com.iceflyer3.pfd.user.UserAuthentication;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Repository
@Transactional
public class JpaRecipeRepository implements RecipeRepository {

    private static final Logger LOG = LoggerFactory.getLogger(JpaRecipeRepository.class);

    private final AppConfigUtils appConfigUtils;

    @PersistenceContext
    private EntityManager entityManager;

    public JpaRecipeRepository(AppConfigUtils appConfigUtils)
    {
        this.appConfigUtils = appConfigUtils;
    }

    @Override
    public Recipe getRecipeById(UUID recipeId) {
        Recipe recipe = entityManager.find(Recipe.class, recipeId);

        if (recipe == null)
        {
            throw new InvalidApplicationStateException(String.format("No recipe with id %s was found", recipeId));
        }
        else
        {
            return recipe;
        }
    }

    @Override
    public List<Recipe> getAllRecipes() {
        LOG.debug("Searching for all recipes...");
        List<Recipe> allRecipes = entityManager
                .createNamedQuery(QueryConstants.QUERY_RECIPE_FIND_ALL, Recipe.class)
                .getResultList();
        LOG.debug("Found {} recipes!", allRecipes.size());
        return allRecipes;
    }

    @Override
    public List<Recipe> getAllRecipesForUser(UUID userId) {
        LOG.debug("Searching for all recipes...");
        List<Recipe> allRecipesForUser = entityManager
                .createNamedQuery(QueryConstants.QUERY_RECIPE_FIND_ALL_FOR_USER, Recipe.class)
                .setParameter(QueryConstants.PARAMETER_USER_ID, userId)
                .getResultList();
        LOG.debug("Found {} recipes for user {}!", allRecipesForUser.size(), userId);
        return allRecipesForUser;
    }

    @Override
    public List<RecipeNutritionalSummary> getAllNutritionalSummaries()
    {
        return entityManager.createNamedQuery(QueryConstants.QUERY_RECIPE_CALCULATE_ALL_NUTRITIONAL_SUMMARIES, RecipeNutritionalSummary.class).getResultList();
    }

    @Override
    public RecipeNutritionalSummary getNutritionalSummaryById(UUID recipeId)
    {
        return entityManager
                .createNamedQuery(QueryConstants.QUERY_RECIPE_CALCULATE_RECIPE_NUTRITIONAL_SUMMARY, RecipeNutritionalSummary.class)
                .setParameter(QueryConstants.PARAMETER_RECIPE_ID, recipeId)
                .getSingleResult();
    }

    @Override
    public List<RecipeIngredient> getIngredientsForRecipe(UUID recipeId)
    {
        return entityManager
                .createNamedQuery(QueryConstants.QUERY_RECIPE_FIND_INGREDIENTS_FOR_RECIPE, RecipeIngredient.class)
                .setParameter(QueryConstants.PARAMETER_RECIPE_ID, recipeId)
                .getResultList();
    }

    @Override
    public List<RecipeStep> getStepsForRecipe(UUID recipeId)
    {
        return entityManager
                .createNamedQuery(QueryConstants.QUERY_RECIPE_FIND_STEPS_FOR_RECIPE, RecipeStep.class)
                .setParameter(QueryConstants.PARAMETER_RECIPE_ID, recipeId)
                .getResultList();
    }

    @Override
    public Recipe createRecipe(CreateRecipeRequest creationRequest) {
        RecipeViewModel recipeViewModel = creationRequest.getRecipe();

        // First, create the new recipe entity
        User createdUser = entityManager.find(User.class, creationRequest.getCreatedUser().getUserId());
        Recipe recipeEntity = new Recipe(recipeViewModel.getName(), recipeViewModel.getSource(), recipeViewModel.getNumberOfServingsMade(), createdUser);
        entityManager.persist(recipeEntity);

        List<RecipeServingSize> updatedServingSizes = updateServingSizesForRecipe(recipeEntity.getRecipeId(), recipeViewModel.getServingSizes());
        recipeEntity.getServingSizes().addAll(updatedServingSizes);

        List<RecipeIngredient> updatedRecipeIngredients = updateIngredientsForRecipe(recipeEntity.getRecipeId(), recipeViewModel.getIngredients());
        recipeEntity.getIngredients().addAll(updatedRecipeIngredients);

        List<RecipeStep> updatedSteps = updateStepsForRecipe(recipeEntity.getRecipeId(), recipeViewModel.getSteps(), false);
        recipeEntity.getSteps().addAll(updatedSteps);

        LOG.debug("CreateRecipe is returning a new recipe entity of {}", recipeEntity);
        return recipeEntity;
    }

    /**
     * Updates a recipe according to the details of the view model.
     *
     * Unlike other functions this needs no request object because all of the
     * information needed to perform the update is contained within the view model.
     *
     * @param recipeViewModel The view model containing the new recipe details
     * @return The update Recipe entity
     */
    @Override
    public Recipe updateRecipe(RecipeViewModel recipeViewModel)
    {
        Recipe recipe = entityManager.find(Recipe.class, recipeViewModel.getId());

        // First, update the basic details of the recipe.
        recipe.setRecipeName(recipeViewModel.getName());
        recipe.setSource(recipeViewModel.getSource());
        recipe.setNumberOfServingsMade(recipeViewModel.getNumberOfServingsMade());
        recipe.setLastModifiedDate(LocalDateTime.now());

        // None of our collections are mapped to cascade changes so we should be fine to clear and refresh those collections with new entities.
        List<RecipeServingSize> updatedServingSizes = updateServingSizesForRecipe(recipeViewModel.getId(), recipeViewModel.getServingSizes());
        recipe.getServingSizes().clear();
        recipe.getServingSizes().addAll(updatedServingSizes);

        // Update steps first due the relationship from step ingredients to recipe ingredients
        List<RecipeStep> updatedSteps = updateStepsForRecipe(recipeViewModel.getId(), recipeViewModel.getSteps(), true);
        recipe.getSteps().clear();
        recipe.getSteps().addAll(updatedSteps);

        List<RecipeIngredient> updatedRecipeIngredients = updateIngredientsForRecipe(recipeViewModel.getId(), recipeViewModel.getIngredients());
        recipe.getIngredients().clear();
        recipe.getIngredients().addAll(updatedRecipeIngredients);

        return recipe;
    }

    @Override
    public void deleteRecipe(UUID recipeId)
    {
        Recipe recipeToDelete = getRecipeById(recipeId);
        List<RecipeStep> recipeSteps = getStepsForRecipe(recipeId);
        List<RecipeIngredient> recipeIngredients = getIngredientsForRecipe(recipeId);

        for(RecipeStep step : recipeSteps)
        {
            step.getStepIngredients().forEach(entityManager::remove);
            entityManager.remove(step);
        }

        recipeToDelete.getServingSizes().forEach(entityManager::remove);
        recipeIngredients.forEach(entityManager::remove);
        entityManager.remove(recipeToDelete);
    }

    @Override
    public boolean isRecipeUnique(RecipeModel recipeModel)
    {
        boolean isUnique = true;

        // Calculate nutritional summary information for the desired new complex food
        List<UUID> recipeIngredientIds = recipeModel
                .getIngredients()
                .stream()
                .map(ingredientModel -> ingredientModel.getIngredientFood().getId())
                .toList();

        List<FoodNutritionalDetails> recipeFoods = entityManager
                .createNamedQuery(QueryConstants.QUERY_FOOD_FIND_ALL_WITH_IDS, FoodNutritionalDetails.class)
                .setParameter(QueryConstants.PARAMETER_FOOD_ID_LIST, recipeIngredientIds)
                .getResultList();

        // Create a map of the simple food entities to the number of servings of the food included
        Map<FoodNutritionalDetails, BigDecimal> foodServingsIncluded = new HashMap<>();
        for(IngredientModel ingredient : recipeModel.getIngredients())
        {
            FoodNutritionalDetails ingredientFoodEntity = recipeFoods.stream().filter(foodEntity -> foodEntity.getFoodId().equals(ingredient.getIngredientFood().getId())).findFirst().orElseThrow();
            foodServingsIncluded.put(ingredientFoodEntity, ingredient.getServingsIncluded());
        }

        NutritionalSummary ingredientsNutritionalSummary = NutritionalSummaryMapper.toModel(foodServingsIncluded);

        // Search for recipes with the same name and, if found, compare nutritional profile
        List<Recipe> duplicateCandidates = findRecipeByName(recipeModel.getName());
        for(Recipe duplicateCandidate : duplicateCandidates)
        {
            RecipeNutritionalSummary candidateNutritionalSummaryEntity = getNutritionalSummaryById(duplicateCandidate.getRecipeId());
            NutritionalSummary candidateNutritionalSummary = NutritionalSummaryMapper.toModel(candidateNutritionalSummaryEntity);
            isUnique = isUnique && !candidateNutritionalSummary.equals(ingredientsNutritionalSummary);
        }

        return isUnique;
    }

    @Override
    public JsonRecipeData exportData()
    {
        JsonRecipeData jsonRecipeData = new JsonRecipeData(appConfigUtils.getProperty("schema.version", Integer.class));

        // Convert recipe entities to json
        List<Recipe> recipes = getAllRecipesForUser(UserAuthentication.instance().getAuthenticatedUser().userId());
        List<JsonRecipe> jsonRecipes = recipes.stream().map(RecipeMapper::toJson).toList();
        jsonRecipeData.setRecipes(jsonRecipes);

        // Convert recipe serving size entities to json
        UUID userId =  UserAuthentication.instance().getAuthenticatedUser().userId();
        List<RecipeServingSize> recipeServingSizes = entityManager
                .createNamedQuery(QueryConstants.QUERY_RECIPE_FIND_SERVING_SIZES_FOR_USER, RecipeServingSize.class)
                .setParameter(QueryConstants.PARAMETER_USER_ID, userId)
                .getResultList();

        List<JsonRecipeServingSize> jsonRecipeServingSizes = recipeServingSizes.stream().map(ServingSizeMapper::toJson).toList();
        jsonRecipeData.setRecipeServingSizes(jsonRecipeServingSizes);

        // Convert recipe ingredient entities to json
        List<RecipeIngredient> recipeIngredients = entityManager
                .createNamedQuery(QueryConstants.QUERY_RECIPE_FIND_INGREDIENTS_FOR_USER, RecipeIngredient.class)
                .setParameter(QueryConstants.PARAMETER_USER_ID, userId)
                .getResultList();

        List<JsonRecipeIngredient> jsonRecipeIngredients = recipeIngredients.stream().map(IngredientMapper::toJson).toList();
        jsonRecipeData.setRecipeIngredients(jsonRecipeIngredients);

        // Convert recipe step entities to json
        List<RecipeStep> recipeSteps = entityManager
                .createNamedQuery(QueryConstants.QUERY_RECIPE_FIND_STEPS_FOR_USER, RecipeStep.class)
                .setParameter(QueryConstants.PARAMETER_USER_ID, userId)
                .getResultList();

        List<JsonRecipeStep> jsonRecipeSteps = recipeSteps.stream().map(RecipeStepMapper::toJson).toList();
        jsonRecipeData.setRecipeSteps(jsonRecipeSteps);

        // Convert recipe step ingredient entities to json
        List<RecipeStepIngredient> recipeStepIngredients = entityManager
                .createNamedQuery(QueryConstants.QUERY_RECIPE_FIND_STEP_INGREDIENTS_FOR_USER, RecipeStepIngredient.class)
                .setParameter(QueryConstants.PARAMETER_USER_ID, userId)
                .getResultList();

        List<JsonRecipeStepIngredient> jsonRecipeStepIngredients = recipeStepIngredients.stream().map(IngredientMapper::toJson).toList();
        jsonRecipeData.setRecipeStepIngredients(jsonRecipeStepIngredients);

        return jsonRecipeData;
    }

    @Override
    public void importData(JsonRecipeData jsonRecipeData, FoodImportResult importedFoods)
    {
        User importUser = entityManager.find(User.class, UserAuthentication.instance().getAuthenticatedUser().userId());
        LOG.debug("Beginning import of recipe data for user {}", importUser);

        if (importUser == null)
        {
            // As with all application state exceptions, this should never happen. But we'll be safe and check anyway.
            throw new InvalidApplicationStateException("The User profile importing this data (%s) could not be found in the database!");
        }

        for(JsonRecipe jsonRecipe : jsonRecipeData.getRecipes())
        {
            LOG.debug("Importing recipe {} ({})", jsonRecipe.getRecipeName(), jsonRecipe.getRecipeId());
            // Find all extended recipe information
            Set<JsonRecipeIngredient> jsonRecipeIngredients = jsonRecipeData
                    .getRecipeIngredients()
                    .stream()
                    .filter(ingredient -> ingredient.getForRecipeId().equals(jsonRecipe.getRecipeId()))
                    .collect(Collectors.toSet());

            Set<JsonRecipeServingSize> jsonRecipeServingSizes = jsonRecipeData
                    .getRecipeServingSizes()
                    .stream()
                    .filter(servingSize -> servingSize.getForRecipeId().equals(jsonRecipe.getRecipeId()))
                    .collect(Collectors.toSet());

            Set<JsonRecipeStep> jsonRecipeSteps = jsonRecipeData
                    .getRecipeSteps()
                    .stream()
                    .filter(recipeStep -> recipeStep.getForRecipeId().equals(jsonRecipe.getRecipeId()))
                    .collect(Collectors.toSet());

            // Look to see if a recipe with the same and nutritional profile already exists
            List<Recipe> existingRecipeCandidates = findRecipeByName(jsonRecipe.getRecipeName());

            Recipe newRecipeEntity = null;
            for(Recipe existingRecipeCandidate : existingRecipeCandidates)
            {
                RecipeNutritionalSummary candidateRecipeNutritionalSummary = getNutritionalSummaryById(existingRecipeCandidate.getRecipeId());
                if (areJsonIngredientsAndRecipeNutritionalSummaryEquivalent(jsonRecipeIngredients, candidateRecipeNutritionalSummary, importedFoods.getAll()))
                {
                    /*
                      * If the recipe we're importing has the same name and nutritional profile as
                      * an existing recipe then update the json to point at the existing recipe and
                      * set up the entity object that will be saved
                     */
                    //
                    jsonRecipe.setRecipeId(existingRecipeCandidate.getRecipeId());
                    newRecipeEntity = RecipeMapper.toEntity(jsonRecipe, importUser, importUser);

                    // Clear out existing extended details. These will be recreated according to the import data.
                    existingRecipeCandidate.getServingSizes().forEach(servingSize -> entityManager.remove(servingSize));
                    existingRecipeCandidate.getServingSizes().clear();

                    existingRecipeCandidate.getSteps().forEach(recipeStep -> recipeStep.getStepIngredients().forEach(stepIngredient -> entityManager.remove(stepIngredient)));
                    existingRecipeCandidate.getSteps().forEach(recipeStep -> entityManager.remove(recipeStep));
                    existingRecipeCandidate.getSteps().clear();

                    existingRecipeCandidate.getIngredients().forEach(ingredient -> entityManager.remove(ingredient));
                    existingRecipeCandidate.getIngredients().clear();
                }
            }

            // If either there were no recipes with the same name or no recipes with same name and nutritional profile
            // then ensure we create a new recipe
            if (newRecipeEntity == null)
            {
                newRecipeEntity = RecipeMapper.toFreshEntity(jsonRecipe, importUser, importUser);
            }
            newRecipeEntity = entityManager.merge(newRecipeEntity);

            Map<UUID, RecipeIngredient> createdRecipeIngredients = new HashMap<>();
            for(JsonRecipeIngredient jsonRecipeIngredient : jsonRecipeIngredients)
            {
                LOG.debug("\tImporting recipe ingredient from source {}", jsonRecipeIngredient);
                UUID ingredientFoodId = importedFoods.getIdForImportedFoodByJsonId(jsonRecipeIngredient.getFoodId());
                FoodMetaData ingredientFoodMetaData = entityManager.find(FoodMetaData.class, ingredientFoodId);

                if (ingredientFoodMetaData == null)
                {
                    throw new DataImportException(
                            String.format(
                                    "Could not find ingredient food meta data with id of %s for recipe %s (%s)",
                                    jsonRecipeIngredient.getFoodId(),
                                    jsonRecipe.getRecipeName(),
                                    jsonRecipe.getRecipeId()
                            )
                    );
                }

                RecipeIngredient newRecipeIngredient = IngredientMapper.toFreshEntity(jsonRecipeIngredient, newRecipeEntity, ingredientFoodMetaData);
                newRecipeIngredient = entityManager.merge(newRecipeIngredient);
                createdRecipeIngredients.put(jsonRecipeIngredient.getRecipeIngredientId(), newRecipeIngredient);
            }

            for(JsonRecipeServingSize jsonRecipeServingSize : jsonRecipeServingSizes)
            {
                LOG.debug("\tImporting recipe serving size from source {}", jsonRecipeServingSize);
                RecipeServingSize newRecipeServingSizeEntity = ServingSizeMapper.toFreshEntity(jsonRecipeServingSize, newRecipeEntity);
                newRecipeServingSizeEntity = entityManager.merge(newRecipeServingSizeEntity);
                LOG.debug("\tImported recipe serving size and generated ID {}", newRecipeServingSizeEntity.getServingSizeId());
            }

            for(JsonRecipeStep jsonRecipeStep : jsonRecipeSteps)
            {
                LOG.debug("\tImporting recipe step from source {}", jsonRecipeSteps);
                RecipeStep newRecipeStepEntity = RecipeStepMapper.toFreshEntity(jsonRecipeStep, newRecipeEntity);
                newRecipeStepEntity = entityManager.merge(newRecipeStepEntity);
                LOG.debug("\tImported recipe step and generated ID {}", newRecipeStepEntity.getStepId());

                List<JsonRecipeStepIngredient> jsonRecipeStepIngredients = jsonRecipeData
                        .getRecipeStepIngredients()
                        .stream()
                        .filter(
                                stepIngredient ->
                                        stepIngredient.getRecipeId().equals(jsonRecipe.getRecipeId())
                                        && stepIngredient.getRecipeStepId().equals(jsonRecipeStep.getStepId())
                        )
                        .toList();

                for(JsonRecipeStepIngredient jsonRecipeStepIngredient : jsonRecipeStepIngredients)
                {
                    LOG.debug("\tImporting recipe step ingredient from source {}", jsonRecipeStepIngredient);
                    RecipeIngredient newRecipeIngredient = createdRecipeIngredients.get(jsonRecipeStepIngredient.getRecipeIngredientId());
                    RecipeStepIngredient newRecipeStepIngredient = IngredientMapper.toEntity(newRecipeEntity, newRecipeStepEntity, newRecipeIngredient);
                    newRecipeStepIngredient = entityManager.merge(newRecipeStepIngredient);
                    LOG.debug("\tImported recipe step ingredient and generated ID {}", newRecipeStepIngredient.getStepIngredientId());
                }
            }
            LOG.debug("\tImported recipe and generated ID {}", newRecipeEntity.getRecipeId());
        }
        LOG.debug("Finished import of recipe data for user {}", importUser);
    }

    private List<Recipe> findRecipeByName(String recipeName)
    {
        return entityManager
                .createNamedQuery(QueryConstants.QUERY_RECIPE_FIND_BY_NAME, Recipe.class)
                .setParameter(QueryConstants.PARAMETER_RECIPE_NAME, recipeName)
                .getResultList();
    }

    /**
     * Updates the list of servings sizes for a recipe to be in sync with the updated list passed in.
     * Serving size database records are added, deleted, and modified as necessary.
     *
     * @param recipeId The id of the recipe to which these serving sizes belong
     * @param updatedServingSizes The list of servings sizes to which the recipe should be synced
     * @return The list of serving size entities that were either added or modified.
     */
    private List<RecipeServingSize> updateServingSizesForRecipe(UUID recipeId, List<ServingSizeModel> updatedServingSizes)
    {
        List<RecipeServingSize> modifiedServingSizes = new ArrayList<>();
        List<RecipeServingSize> outdatedServingSizes = entityManager.createNamedQuery(QueryConstants.QUERY_RECIPE_FIND_SERVING_SIZES_FOR_RECIPE, RecipeServingSize.class)
                                                                    .setParameter(QueryConstants.PARAMETER_RECIPE_ID, recipeId)
                                                                    .getResultList();

        Recipe forRecipe = entityManager.find(Recipe.class, recipeId);

        // Remove servings that are outdated
        List<RecipeServingSize> servingSizesToRemove = outdatedServingSizes
                .stream()
                .filter(outdatedServingSize ->
                        updatedServingSizes
                            .stream()
                            .noneMatch(updatedServingSize -> updatedServingSize.getId().equals(outdatedServingSize.getServingSizeId()))
                )
                .collect(Collectors.toList());
        servingSizesToRemove.forEach(entityManager::remove);

        // Add or update remaining serving sizes
        for(ServingSizeModel servingSizeViewModel : updatedServingSizes)
        {
            RecipeServingSize servingSizeEntity;

            // If this is a new ingredient then add it. Otherwise update it.
            if (servingSizeViewModel.getId() == null)
            {
                servingSizeEntity = new RecipeServingSize(forRecipe, servingSizeViewModel.getUnitOfMeasure(), servingSizeViewModel.getServingSize());
                entityManager.persist(servingSizeEntity);
            }
            else
            {
                servingSizeEntity = entityManager.find(RecipeServingSize.class, servingSizeViewModel.getId());
                servingSizeEntity.setUnitOfMeasure(servingSizeViewModel.getUnitOfMeasure());
                servingSizeEntity.setServingSize(servingSizeViewModel.getServingSize());
            }

            modifiedServingSizes.add(servingSizeEntity);
        }

        return modifiedServingSizes;
    }

    /**
     * Updates the list of ingredients for a recipe to be in sync with the updated list passed in.
     * Ingredient database records are added, deleted, and modified as necessary.
     *
     * @param recipeId The id of the recipe to which these ingredients belong
     * @param updatedIngredients The list of ingredients to which the recipe should be synced
     * @return The list of ingredient entities that were either added or modified.
     */
    private List<RecipeIngredient> updateIngredientsForRecipe(UUID recipeId, List<IngredientModel> updatedIngredients)
    {
        List<RecipeIngredient> modifiedIngredients = new ArrayList<>();
        List<RecipeIngredient> outdatedIngredients = entityManager.createNamedQuery(QueryConstants.QUERY_RECIPE_FIND_INGREDIENTS_FOR_RECIPE, RecipeIngredient.class)
                .setParameter(QueryConstants.PARAMETER_RECIPE_ID, recipeId)
                .getResultList();

        Recipe forRecipe = entityManager.find(Recipe.class, recipeId);

        // Remove ingredients that are outdated
        List<RecipeIngredient> ingredientsToRemove = outdatedIngredients
                .stream()
                .filter(outdatedIngredient ->
                        updatedIngredients
                            .stream()
                            .noneMatch(updatedIngredient -> outdatedIngredient.getRecipeIngredientId().equals(updatedIngredient.getId()))
                )
                .collect(Collectors.toList());
        ingredientsToRemove.forEach(entityManager::remove);

        // Add or update remaining ingredients
        for(IngredientModel ingredientViewModel : updatedIngredients)
        {
            RecipeIngredient ingredientEntity;

            if (ingredientViewModel.getId() == null)
            {
                FoodMetaData foodIngredient = entityManager.find(FoodMetaData.class, ingredientViewModel.getIngredientFood().getId());
                ingredientEntity = new RecipeIngredient(forRecipe, foodIngredient, ingredientViewModel.getServingsIncluded());
                ingredientEntity.setOptional(ingredientViewModel.isOptional());
                ingredientEntity.setAlternate(ingredientViewModel.isAlternate());
                entityManager.persist(ingredientEntity);
            }
            else
            {
                ingredientEntity = entityManager.find(RecipeIngredient.class, ingredientViewModel.getId());
                ingredientEntity.setNumberOfServings(ingredientViewModel.getServingsIncluded());
                ingredientEntity.setOptional(ingredientViewModel.isOptional());
                ingredientEntity.setAlternate(ingredientViewModel.isAlternate());
            }

            modifiedIngredients.add(ingredientEntity);
        }

        return modifiedIngredients;
    }

    /**
     * Updates the list of steps for a recipe to be in sync with the updated list passed in.
     * Recipe step database records are added, deleted, and modified as necessary.
     *
     * @param recipeId The id of the recipe to which these steps belong
     * @param updatedSteps The list of steps to which the recipe should be synced
     * @return The list of step entities that were either added or modified.
     */
    private List<RecipeStep> updateStepsForRecipe(UUID recipeId, List<RecipeStepModel> updatedSteps, boolean isUpdateOperation)
    {
        LOG.debug("Updating {} steps", updatedSteps.size());
        List<RecipeStep> modifiedSteps = new ArrayList<>();

        // First update the ingredients for each step
        Recipe forRecipe = entityManager.find(Recipe.class, recipeId);

        List<RecipeStep> allSteps = entityManager.createNamedQuery(QueryConstants.QUERY_RECIPE_FIND_STEPS_FOR_RECIPE, RecipeStep.class)
                .setParameter(QueryConstants.PARAMETER_RECIPE_ID, recipeId)
                .getResultList();

        // Find the list of all steps that should be removed (have database records but are not in the updated list) and remove them'
        LOG.debug("Found {} existing steps", allSteps.size());
        List<RecipeStep> stepsToRemove = allSteps
                .stream()
                .filter(outdatedStep ->
                        updatedSteps
                            .stream()
                            .noneMatch(updatedStep ->
                                updatedStep.getId() != null // Not a new step
                                        && updatedStep.getId().equals(outdatedStep.getStepId()) // Is not included in the existing list of step ingredients according to the db
                            )
                )
                .collect(Collectors.toList());

        for(RecipeStep stepToRemove: stepsToRemove)
        {
            // Need to be sure to remove any step ingredients for any steps that need removed first so we don't violate referential integrity
            stepToRemove.getStepIngredients().forEach(stepIngredient -> entityManager.remove(stepIngredient));
            entityManager.remove(stepToRemove);
        }

        // Add (for ingredients without an ID) or update the remaining steps
        for(RecipeStepModel recipeStepViewModel : updatedSteps)
        {
            RecipeStep recipeStepEntity;
            if (recipeStepViewModel.getId() == null)
            {
                recipeStepEntity = new RecipeStep(forRecipe, recipeStepViewModel.getStepName(), recipeStepViewModel.getDirections(), recipeStepViewModel.getStepNumber());
                entityManager.persist(recipeStepEntity);
            }
            else
            {
                recipeStepEntity = entityManager.find(RecipeStep.class, recipeStepViewModel.getId());
                recipeStepEntity.setName(recipeStepViewModel.getStepName());
                recipeStepEntity.setDirections(recipeStepViewModel.getDirections());
                recipeStepEntity.setStepNumber(recipeStepViewModel.getStepNumber());
            }

            modifiedSteps.add(recipeStepEntity);

            // Update the list of ingredients for the step after the step has been updated
            List<UUID> stepIngredientIds = new ArrayList<>();
            if (isUpdateOperation)
            {
                // If we're updating then the view models already have the ids loaded so we may compile the list from there
                stepIngredientIds = recipeStepViewModel.getIngredients().stream().map(IngredientModel::getId).collect(Collectors.toList());
            }
            else
            {
                /*
                 * If we're creating a new recipe the view models will not have the ids set on them.
                 *
                 * So we must query for all ingredients for the recipe (both of which have been previously saved)
                 * and match the list of RecipeIngredient records against the list of IngredientViewModels by FoodId
                 * to find the ingredients which should belong to this step.
                */
                List<RecipeIngredient> recipeIngredients = entityManager.createNamedQuery(QueryConstants.QUERY_RECIPE_FIND_INGREDIENTS_FOR_RECIPE, RecipeIngredient.class)
                        .setParameter(QueryConstants.PARAMETER_RECIPE_ID, recipeId)
                        .getResultList();
                stepIngredientIds = recipeIngredients
                        .stream()
                        .filter(recipeIngredient ->
                                recipeStepViewModel.getIngredients().stream().anyMatch(recipeStepIngredient -> recipeStepIngredient.getIngredientFood().getId().equals(recipeIngredient.getFoodMeta().getFoodId()))
                        )
                        .map(RecipeIngredient::getRecipeIngredientId)
                        .collect(Collectors.toList());
            }

            List<RecipeStepIngredient> updatedIngredients = updateIngredientsForRecipeStep(forRecipe, recipeStepEntity, stepIngredientIds);
            recipeStepEntity.getStepIngredients().clear();
            recipeStepEntity.getStepIngredients().addAll(updatedIngredients);
        }

        return modifiedSteps;
    }

    /**
     * Update the list of ingredients for a recipe step to be in sync with the new list passed in.
     * Recipe step ingredient database records are added, deleted, and modified as necessary.
     *
     * We require a list of UUIDs for the RecipeIngredient record(s) that are tied to this RecipeStep as a
     * parameter here because the way in which we must go about acquiring these ids is different depending on
     * if we are creating a new recipe or updating an existing recipe.
     *
     * If we are creating a new recipe then the view models will not have any ids so they will have to be compiled
     * from the newly saved RecipeStep records instead.
     *
     * If we are updating an existing recipe then the ids will have been loaded with the view model when it was
     * created so the ids may be compiled from them.
     *
     * We must have these ids because the list of ingredients that is tied to a particular recipe step is a
     * subset of the total list of ingredients for the recipe. So we must have the ids to look up the corresponding
     * RecipeIngredient database record so that it may be tied to the new RecipeStepIngredient record.
     *
     * @param forRecipe The entity that represents the Recipe record
     * @param step The entity that represents the RecipeStep record
     * @param updatedStepIngredientIds The list of ids for the RecipeIngredient records that are tied to this RecipeStep
     * @return The list of RecipeStepIngredient entities that were added.
     */
    private List<RecipeStepIngredient> updateIngredientsForRecipeStep(Recipe forRecipe, RecipeStep step, List<UUID> updatedStepIngredientIds)
    {
        List<RecipeStepIngredient> modifiedStepIngredients = new ArrayList<>();
        List<RecipeStepIngredient> allStepIngredients = entityManager
                .createNamedQuery(QueryConstants.QUERY_RECIPE_FIND_STEP_INGREDIENTS_FOR_STEP_FOR_RECIPE, RecipeStepIngredient.class)
                .setParameter(QueryConstants.PARAMETER_RECIPE_STEP_ID, step.getStepId())
                .getResultList();

        // Find the list of step ingredients that should be removed (have database records but are not in the updated list) and remove them
        List<RecipeStepIngredient> stepIngredientsToRemove = allStepIngredients
                .stream()
                .filter(outdatedStepIngredient ->
                        updatedStepIngredientIds
                                .stream()
                                .noneMatch(updatedStepIngredientId -> updatedStepIngredientId.equals(outdatedStepIngredient.getRecipeIngredient().getRecipeIngredientId()))
                )
                .collect(Collectors.toList());
        stepIngredientsToRemove.forEach(entityManager::remove);

        // Add (for ingredients without an ID) or update the remaining step ingredients
        for(UUID stepIngredientIngredientId : updatedStepIngredientIds)
        {
            // Find the RecipeIngredient record to which this RecipeStepIngredient is tied
            RecipeIngredient recipeIngredient = entityManager.find(RecipeIngredient.class, stepIngredientIngredientId);

            // The primary key of RecipeStepIngredient is a three-part composite. So we always create the entity before we search for it so we can call the getter for the primary key.
            // Then, if no results were found we save out a new record.
            RecipeStepIngredient recipeStepIngredientEntity = new RecipeStepIngredient(forRecipe, step, recipeIngredient);
            RecipeStepIngredient existingIngredient = entityManager.find(RecipeStepIngredient.class, recipeStepIngredientEntity.getStepIngredientId());
            if (existingIngredient == null)
            {
                entityManager.persist(recipeStepIngredientEntity);
                modifiedStepIngredients.add(recipeStepIngredientEntity);
            }
            else
            {
                modifiedStepIngredients.add(existingIngredient);
            }
        }

        LOG.debug("Updated {} step ingredients", modifiedStepIngredients.size());
        return modifiedStepIngredients;
    }


    // See the comment on the similar function in the food repository for an explanation
    @SuppressWarnings("DuplicatedCode")
    public boolean areJsonIngredientsAndRecipeNutritionalSummaryEquivalent(Set<JsonRecipeIngredient> ingredients, RecipeNutritionalSummary nutritionalSummary, Map<UUID, UUID> importedFoods)
    {
        LOG.debug("\tComparing nutritional summary\n\t\t{}\n\tto ingredient list:\n\t\t{}", nutritionalSummary, ingredients);

        // Find all IDs in our map of imported simple foods that also occur in our set of ingredients
        List<UUID> importedFoodIngredientIds = importedFoods
                .entrySet()
                .stream()
                .filter(entry ->
                        ingredients.stream().anyMatch(jsonRecipeIngredient -> jsonRecipeIngredient.getFoodId().equals(entry.getKey()))
                )
                .map(Map.Entry::getValue)
                .toList();

        if (importedFoodIngredientIds.size() == 0)
        {
            throw new DataImportException(
                    String.format(
                            "Could not find any simple food IDs in the set of imported simple foods for the list of ingredients.\n\tSource Ingredient list:\n\t\t%s\n\tImported simple foods list:\n\t\t%s",
                            ingredients,
                            importedFoodIngredientIds)
            );
        }

        List<FoodNutritionalDetails> recipeFoods = entityManager
                .createNamedQuery(QueryConstants.QUERY_FOOD_FIND_ALL_WITH_IDS, FoodNutritionalDetails.class)
                .setParameter(QueryConstants.PARAMETER_FOOD_ID_LIST, importedFoodIngredientIds)
                .getResultList();

        if (recipeFoods.size() != importedFoodIngredientIds.size())
        {
            throw new DataImportException(
                    String.format(
                            "Could not find simple food(s) for one or more IDs in the list of imported simple foods that are also ingredients for the recipe.\n\tRecipe ingredient imported simple food IDs list:\n\t\t%s",
                            recipeFoods
                    )
            );
        }

        // Create a map of the simple food entities to the number of servings of the food included
        Map<FoodNutritionalDetails, BigDecimal> foodsServingsIncluded = new HashMap<>();
        for(JsonRecipeIngredient jsonRecipeIngredient : ingredients)
        {
            FoodNutritionalDetails simpleFoodEntity = recipeFoods.stream().filter(entity -> entity.getFoodId().equals(importedFoods.get(jsonRecipeIngredient.getFoodId()))).findFirst().orElseThrow();
            foodsServingsIncluded.put(simpleFoodEntity, jsonRecipeIngredient.getNumberOfServings());
        }

        NutritionalSummary ingredientsNutritionalSummary = NutritionalSummaryMapper.toModel(foodsServingsIncluded);
        NutritionalSummary recipeEntityNutritionalSummary = NutritionalSummaryMapper.toModel(nutritionalSummary);
        LOG.debug("Comparing ingredient nutritional summary of\n{}\nto candidate existing recipe entity summary of\n{}", ingredientsNutritionalSummary, recipeEntityNutritionalSummary);

        boolean areNutritionallyEqual = recipeEntityNutritionalSummary.equals(ingredientsNutritionalSummary);
        LOG.debug("\tAre nutritionally equal? {}", areNutritionallyEqual);
        return areNutritionallyEqual;
    }
}
