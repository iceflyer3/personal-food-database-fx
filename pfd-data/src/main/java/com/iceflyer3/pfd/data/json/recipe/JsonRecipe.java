/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.json.recipe;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

public class JsonRecipe
{
    private UUID recipeId;
    private String recipeName;
    private String source;
    private BigDecimal numberOfServingsMade;
    private UUID createdUserId;
    private UUID lastModifiedUserId;
    private LocalDateTime createdDate;
    private LocalDateTime lastModifiedDate;

    @JsonCreator
    public JsonRecipe(
            @JsonProperty("recipeId") UUID recipeId,
            @JsonProperty("recipeName") String recipeName,
            @JsonProperty("source") String source,
            @JsonProperty("numberOfServingsMade") BigDecimal numberOfServingsMade,
            @JsonProperty("createdUserId") UUID createdUserId,
            @JsonProperty("lastModifiedUserId") UUID lastModifiedUserId,
            @JsonProperty("createdDate") LocalDateTime createdDate,
            @JsonProperty("lastModifiedDate") LocalDateTime lastModifiedDate)
    {
        this.recipeId = recipeId;
        this.recipeName = recipeName;
        this.source = source;
        this.numberOfServingsMade = numberOfServingsMade;
        this.createdUserId = createdUserId;
        this.lastModifiedUserId = lastModifiedUserId;
        this.createdDate = createdDate;
        this.lastModifiedDate = lastModifiedDate;
    }

    public UUID getRecipeId()
    {
        return recipeId;
    }

    public void setRecipeId(UUID recipeId)
    {
        this.recipeId = recipeId;
    }

    public String getRecipeName()
    {
        return recipeName;
    }

    public void setRecipeName(String recipeName)
    {
        this.recipeName = recipeName;
    }

    public String getSource()
    {
        return source;
    }

    public void setSource(String source)
    {
        this.source = source;
    }

    public BigDecimal getNumberOfServingsMade()
    {
        return numberOfServingsMade;
    }

    public void setNumberOfServingsMade(BigDecimal numberOfServingsMade)
    {
        this.numberOfServingsMade = numberOfServingsMade;
    }

    public UUID getCreatedUserId()
    {
        return createdUserId;
    }

    public void setCreatedUserId(UUID createdUserId)
    {
        this.createdUserId = createdUserId;
    }

    public UUID getLastModifiedUserId()
    {
        return lastModifiedUserId;
    }

    public void setLastModifiedUserId(UUID lastModifiedUserId)
    {
        this.lastModifiedUserId = lastModifiedUserId;
    }

    public LocalDateTime getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate)
    {
        this.createdDate = createdDate;
    }

    public LocalDateTime getLastModifiedDate()
    {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate)
    {
        this.lastModifiedDate = lastModifiedDate;
    }

    @Override
    public String toString()
    {
        return "JsonRecipe{" + "recipeId=" + recipeId + ", recipeName='" + recipeName + '\'' + ", source='" + source + '\'' + ", numberOfServingsMade=" + numberOfServingsMade + ", createdUserId=" + createdUserId + ", lastModifiedUserId=" + lastModifiedUserId + ", createdDate=" + createdDate + ", lastModifiedDate=" + lastModifiedDate + '}';
    }
}
