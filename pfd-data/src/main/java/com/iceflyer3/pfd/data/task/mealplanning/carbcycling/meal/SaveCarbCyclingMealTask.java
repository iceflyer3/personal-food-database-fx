package com.iceflyer3.pfd.data.task.mealplanning.carbcycling.meal;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.calculator.mealplanning.CalculationResult;
import com.iceflyer3.pfd.data.calculator.mealplanning.CarbCyclingCalculator;
import com.iceflyer3.pfd.data.calculator.mealplanning.CarbCyclingMealCalculationRequest;
import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanMeal;
import com.iceflyer3.pfd.data.entities.mealplanning.carbcycling.CarbCyclingPlan;
import com.iceflyer3.pfd.data.entities.views.CarbCyclingMealNutritionalSummary;
import com.iceflyer3.pfd.data.mapping.mealplanning.carbcycling.CarbCyclingMealMapper;
import com.iceflyer3.pfd.data.mapping.summary.NutritionalSummaryMapper;
import com.iceflyer3.pfd.data.repository.MealPlanningRepository;
import com.iceflyer3.pfd.data.request.mealplanning.meal.CreateCarbCyclingMealRequest;
import com.iceflyer3.pfd.data.request.mealplanning.meal.ModifyMealRequest;
import com.iceflyer3.pfd.data.task.mealplanning.AbstractMealPlanningTask;
import com.iceflyer3.pfd.enums.MealType;
import com.iceflyer3.pfd.enums.Nutrient;
import com.iceflyer3.pfd.exception.InvalidApplicationStateException;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingDayViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingMealViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;

public class SaveCarbCyclingMealTask extends AbstractMealPlanningTask<CarbCyclingMealViewModel>
{
    private final CarbCyclingDayViewModel forDay;
    private final CarbCyclingMealViewModel carbCyclingMeal;

    public SaveCarbCyclingMealTask(MealPlanningRepository mealPlanningRepository, CarbCyclingDayViewModel forDay, CarbCyclingMealViewModel carbCyclingMeal)
    {
        super(mealPlanningRepository);
        this.forDay = forDay;
        this.carbCyclingMeal = carbCyclingMeal;
    }

    @Override
    protected CarbCyclingMealViewModel call() throws Exception
    {
        MealPlanMeal mealEntity;
        NutritionalSummary nutritionalSummary = NutritionalSummary.empty();
        if (carbCyclingMeal.getId() == null)
        {
            CarbCyclingPlan carbCyclingPlan = mealPlanningRepository.getCarbCyclingPlanForId(forDay.getOwningCarbCyclingPlanId());
            /*
             * The calculate functions set up and perform the needed calculations for the new carb cycling meal
             *
             * They have no return value because they populate the passed in CarbCyclingMealViewModel instance
             * with the result of these calculations.
             */
            CalculationResult mealCalcResult;
            if (forDay.isTrainingDay())
            {
                mealCalcResult = calculateCarbCyclingMealRequirementsForTrainingDay(carbCyclingPlan);
            }
            else
            {
                mealCalcResult = calculateCarbCyclingMealRequirementsForNonTrainingDay(carbCyclingPlan);
            }

            carbCyclingMeal.setRequiredCalories(mealCalcResult.getNeededCalories());
            carbCyclingMeal.setRequiredProtein(mealCalcResult.getMacronutrientSuggestion(Nutrient.PROTEIN));
            carbCyclingMeal.setRequiredCarbs(mealCalcResult.getMacronutrientSuggestion(Nutrient.CARBOHYDRATES));
            carbCyclingMeal.setRequiredFats(mealCalcResult.getMacronutrientSuggestion(Nutrient.TOTAL_FATS));

            CreateCarbCyclingMealRequest createCarbCyclingMealRequest = new CreateCarbCyclingMealRequest(
                    forDay.getId(),
                    carbCyclingMeal.getName(),
                    carbCyclingMeal
            );
            mealEntity = mealPlanningRepository.createMealForDay(createCarbCyclingMealRequest);
        }
        else
        {
            ModifyMealRequest modifyMealRequest = new ModifyMealRequest(carbCyclingMeal.getId(), carbCyclingMeal.getName());
            mealEntity = mealPlanningRepository.updateMealForDay(modifyMealRequest);

            Optional<CarbCyclingMealNutritionalSummary> nutritionalSummaryEntity = mealPlanningRepository.getCarbCyclingMealNutritionalSummary(mealEntity.getMealId());

            if (nutritionalSummaryEntity.isPresent())
            {
                nutritionalSummary = NutritionalSummaryMapper.toModel(nutritionalSummaryEntity.get());
            }
        }

        return CarbCyclingMealMapper.toModel(mealEntity, nutritionalSummary);
    }

    private CalculationResult calculateCarbCyclingMealRequirementsForTrainingDay(CarbCyclingPlan carbCyclingPlan)
    {
        /*
         * If the new meal we're saving is not the post-workout meal then find the post-workout meal.
         *
         * The post-workout meal is guaranteed to exist at this point because, while it can be any meal
         * number in the day, it must be entered by the user before all other meals because of the impact
         * it has on how carbs are distributed across other meals.
         */
        int postWorkoutMealNumber;
        if (carbCyclingMeal.getMealType() == MealType.POST_WORKOUT)
        {
            postWorkoutMealNumber = carbCyclingMeal.getMealNumber();
        }
        else
        {
            // Lookup all meals for the day and locate the post-workout meal and which meal number it is in the day
            // The meal number of the post-workout meal is one of the requirements for the carb cycling calculator
            List<MealPlanMeal> mealsForDay = mealPlanningRepository.getCarbCyclingMealsForDay(forDay.getId());

            OptionalInt candidateNumber = mealsForDay.stream()
                    .filter(meal -> meal.getCarbCyclingMeal().getMealType() == MealType.POST_WORKOUT)
                    .mapToInt(meal -> meal.getCarbCyclingMeal().getMealNumber())
                    .findFirst();
            if (candidateNumber.isPresent())
            {
                postWorkoutMealNumber = candidateNumber.getAsInt();
            }
            else
            {
                throw new InvalidApplicationStateException("Tried to save a carb cycling meal before the post-workout meal was saved!");
            }
        }

        CarbCyclingMealCalculationRequest mealCalcRequest = new CarbCyclingMealCalculationRequest(
                carbCyclingPlan.getMealsPerDay(),
                carbCyclingMeal.getMealNumber(),
                postWorkoutMealNumber,
                carbCyclingMeal.getCarbConsumptionPercentage(),
                forDay.getRequiredProtein(),
                forDay.getRequiredCarbs(),
                forDay.getRequiredFats(),
                carbCyclingPlan.getShouldTimeFats(),
                forDay.isTrainingDay());

        return CarbCyclingCalculator.instance().calculateMealRequirements(mealCalcRequest);
    }

    private CalculationResult calculateCarbCyclingMealRequirementsForNonTrainingDay(CarbCyclingPlan carbCyclingPlan)
    {
        /*
         * For non-training days the user only has the ability to enter the carb consumption percentage
         * for the first meal of the day.
         *
         * After that, the system spreads the rest of the carbs out evenly across all remaining meals for
         * the day by calculating the carb consumption percentage for each remaining meal.
         *
         * It does this by taking the remaining percentage of carbs after the first meal and
         * dividing it by the remaining number of meals.
         *
         * Expressed as a formula:
         *  (100 - [meal one carb consumption percentage]) / ([number of meals for day] - 1)
         */
        BigDecimal carbConsumptionPercentage;
        if (carbCyclingMeal.getMealNumber() == 1)
        {
            carbConsumptionPercentage = carbCyclingMeal.getCarbConsumptionPercentage();
        }
        else
        {
            MealPlanMeal firstMeal = mealPlanningRepository.getCarbCyclingMealForDay(forDay.getId(), 1);
            BigDecimal numberOfRemainingMeals = BigDecimal.valueOf((double) carbCyclingPlan.getMealsPerDay() - 1);
            carbConsumptionPercentage = BigDecimal.valueOf(100.0)
                    .subtract(firstMeal.getCarbCyclingMeal().getCarbConsumptionPercentage())
                    .divide(numberOfRemainingMeals, 2, RoundingMode.HALF_UP);
        }
        carbCyclingMeal.setCarbConsumptionPercentage(carbConsumptionPercentage);

        CarbCyclingMealCalculationRequest mealCalcRequest = new CarbCyclingMealCalculationRequest(
                carbCyclingPlan.getMealsPerDay(),
                carbCyclingMeal.getMealNumber(),
                -1,
                carbCyclingMeal.getCarbConsumptionPercentage(),
                forDay.getRequiredProtein(),
                forDay.getRequiredCarbs(),
                forDay.getRequiredFats(),
                carbCyclingPlan.getShouldTimeFats(),
                forDay.isTrainingDay()
        );

        return CarbCyclingCalculator.instance().calculateMealRequirements(mealCalcRequest);
    }
}
