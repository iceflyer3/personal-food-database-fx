package com.iceflyer3.pfd.data.mapping.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023, 2025 Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.entities.User;
import com.iceflyer3.pfd.data.entities.mealplanning.MealPlan;
import com.iceflyer3.pfd.data.json.mealplanning.JsonMealPlan;
import com.iceflyer3.pfd.enums.ActivityLevel;
import com.iceflyer3.pfd.enums.BasalMetabolicRateFormula;
import com.iceflyer3.pfd.enums.FitnessGoal;
import com.iceflyer3.pfd.enums.Sex;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanViewModel;
import com.iceflyer3.pfd.util.NumberUtils;

public class MealPlanMapper {
    private MealPlanMapper() {}

    public static MealPlanViewModel toModel(MealPlan entity)
    {
        MealPlanViewModel viewModel = new MealPlanViewModel(entity.getMealPlanId());

        viewModel.setAge(entity.getAge());
        viewModel.setHeight(NumberUtils.withStandardPrecision(entity.getHeight()));
        viewModel.setWeight(NumberUtils.withStandardPrecision(entity.getWeight()));
        viewModel.setSex(entity.getSex());
        viewModel.setActivityLevel(entity.getActivityLevel());
        viewModel.setFitnessGoal(entity.getFitnessGoal());
        viewModel.setBmrFormula(entity.getBmrFormula());
        viewModel.setDailyCalories(NumberUtils.withStandardPrecision(entity.getDailyCalories()));
        viewModel.setTdeeCalories(NumberUtils.withStandardPrecision(entity.getTdeeCalories()));
        viewModel.getNutrientSuggestions().addAll(entity.getNutrientSuggestions().stream().map(MealPlanNutrientSuggestionMapper::toModel).toList());

        if (entity.getIsActive())
        {
            viewModel.toggleActiveStatus();
        }

        return viewModel;
    }

    public static JsonMealPlan toJson(MealPlan mealPlan)
    {
        return new JsonMealPlan(
                mealPlan.getMealPlanId(),
                mealPlan.getUser().getUserId(),
                mealPlan.getHeight(),
                mealPlan.getWeight(),
                mealPlan.getAge(),
                mealPlan.getSex().toString(),
                mealPlan.getActivityLevel().toString(),
                mealPlan.getFitnessGoal().toString(),
                mealPlan.getBmrFormula().toString(),
                mealPlan.getBodyFatPercentage(),
                mealPlan.getDailyCalories(),
                mealPlan.getTdeeCalories(),
                mealPlan.getIsActive()
        );
    }

    /**
     * Returns a new (and detached) MealPlan instance hydrated from json without a set id.
     * @param jsonMealPlan json from which to hydrate the new instance
     * @param forUser User the plan is for
     * @return MealPlan instance hydrated from json without a set id
     */
    public static MealPlan toFreshEntity(JsonMealPlan jsonMealPlan, User forUser)
    {
        return new MealPlan(
                null,
                forUser,
                jsonMealPlan.height(),
                jsonMealPlan.weight(),
                jsonMealPlan.age(),
                Sex.valueOf(jsonMealPlan.sex()),
                ActivityLevel.valueOf(jsonMealPlan.activityLevel()),
                FitnessGoal.valueOf(jsonMealPlan.fitnessGoal()),
                BasalMetabolicRateFormula.valueOf(jsonMealPlan.bmrFormula()),
                jsonMealPlan.bodyFatPercentage(),
                jsonMealPlan.dailyCalories(),
                jsonMealPlan.tdeeCalories(),
                jsonMealPlan.isActive()
        );
    }
}
