package com.iceflyer3.pfd.data.calculator.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.enums.Nutrient;
import com.iceflyer3.pfd.util.NumberUtils;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Encapsulates the result of the calculation for a Meal Plan
 */
public class CalculationResult
{
    private final BigDecimal tdeeCalories;
    private final BigDecimal neededCalories;
    private final Map<Nutrient, BigDecimal> macroSuggestions;
    // We don't do micro-calculations right now. But they could be added here in the same manner if we decide to.

    public CalculationResult(BigDecimal tdeeCalories, BigDecimal neededCalories, Map<Nutrient, BigDecimal> macroSuggestions)
    {
        this.tdeeCalories = NumberUtils.withStandardPrecision(tdeeCalories);
        this.neededCalories = NumberUtils.withStandardPrecision(neededCalories);
        this.macroSuggestions = macroSuggestions;
    }

    public BigDecimal getTdeeCalories()
    {
        return tdeeCalories;
    }

    public BigDecimal getNeededCalories() {
        return neededCalories;
    }

    public BigDecimal getMacronutrientSuggestion(Nutrient macronutrient)
    {
        return macroSuggestions.get(macronutrient);
    }

    @Override
    public String toString()
    {
        return "CalculationResult{" + "tdeeCalories=" + tdeeCalories + ", neededCalories=" + neededCalories + ", macroSuggestions=" + macroSuggestions + '}';
    }
}
