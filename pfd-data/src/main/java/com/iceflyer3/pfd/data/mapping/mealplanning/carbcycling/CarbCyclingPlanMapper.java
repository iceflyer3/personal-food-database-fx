package com.iceflyer3.pfd.data.mapping.mealplanning.carbcycling;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.data.entities.User;
import com.iceflyer3.pfd.data.entities.mealplanning.MealPlan;
import com.iceflyer3.pfd.data.entities.mealplanning.carbcycling.CarbCyclingPlan;
import com.iceflyer3.pfd.data.json.mealplanning.carbcycling.JsonCarbCyclingPlan;
import com.iceflyer3.pfd.ui.model.api.mealplanning.carbcycling.CarbCyclingMealConfigurationModel;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingPlanViewModel;

import java.util.HashSet;
import java.util.Set;

public class CarbCyclingPlanMapper {

    private CarbCyclingPlanMapper() { }

    public static CarbCyclingPlanViewModel toModel(CarbCyclingPlan entity)
    {
        String[] highLowCarbParts = entity.getHighLowDayRatio().split(":");
        return new CarbCyclingPlanViewModel(
                entity.getPlanId(),
                Integer.parseInt(highLowCarbParts[1]),
                Integer.parseInt(highLowCarbParts[0]),
                entity.getHighCarbDaySurplusPercentage(),
                entity.getLowCarbDayDeficitPercentage(),
                entity.getMealsPerDay(),
                entity.getActive(),
                entity.getShouldTimeFats(),
                new HashSet<>());
    }

    public static CarbCyclingPlanViewModel toModel(CarbCyclingPlan entity, Set<CarbCyclingMealConfigurationModel> mealConfigs)
    {
        String[] highLowCarbParts = entity.getHighLowDayRatio().split(":");
        return new CarbCyclingPlanViewModel(
                entity.getPlanId(),
                Integer.parseInt(highLowCarbParts[0]),
                Integer.parseInt(highLowCarbParts[1]),
                entity.getHighCarbDaySurplusPercentage(),
                entity.getLowCarbDayDeficitPercentage(),
                entity.getMealsPerDay(),
                entity.getActive(),
                entity.getShouldTimeFats(),
                mealConfigs);
    }

    public static JsonCarbCyclingPlan toJson(CarbCyclingPlan carbCyclingPlan)
    {
        return new JsonCarbCyclingPlan(
                carbCyclingPlan.getPlanId(),
                carbCyclingPlan.getMealPlan().getMealPlanId(),
                carbCyclingPlan.getUser().getUserId(),
                carbCyclingPlan.getHighLowDayRatio(),
                carbCyclingPlan.getMealsPerDay(),
                carbCyclingPlan.getHighCarbDaySurplusPercentage(),
                carbCyclingPlan.getLowCarbDayDeficitPercentage(),
                carbCyclingPlan.getShouldTimeFats(),
                carbCyclingPlan.getActive()
        );
    }

    public static CarbCyclingPlan toFreshEntity(JsonCarbCyclingPlan jsonCarbCyclingPlan, MealPlan forMealPlan, User forUser)
    {
        return new CarbCyclingPlan(
                null,
                forMealPlan,
                forUser,
                jsonCarbCyclingPlan.highLowDayRatio(),
                jsonCarbCyclingPlan.highCarbDaySurplusPercentage(),
                jsonCarbCyclingPlan.lowCarbDaySurplusDeficitPercentage(),
                jsonCarbCyclingPlan.shouldTimeFats(),
                jsonCarbCyclingPlan.mealsPerDay(),
                jsonCarbCyclingPlan.isActive()
        );
    }
}
