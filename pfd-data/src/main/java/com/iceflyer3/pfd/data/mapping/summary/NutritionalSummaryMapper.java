package com.iceflyer3.pfd.data.mapping.summary;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.entities.AbstractNutritionalDetails;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;
import com.iceflyer3.pfd.util.NumberUtils;

import java.math.BigDecimal;
import java.util.Map;

public class NutritionalSummaryMapper
{
    private NutritionalSummaryMapper() { }

    // Primarily used for mapping the Summary entities that are mapped to DB views to NutritionalSummary objects
    public static <T extends AbstractNutritionalDetails> NutritionalSummary toModel(T entity)
    {
        return new NutritionalSummary(
                NumberUtils.withStandardPrecision(entity.getCalories()),
                NumberUtils.withStandardPrecision(entity.getProtein()),
                NumberUtils.withStandardPrecision(entity.getTotalFats()),
                NumberUtils.withStandardPrecision(entity.getTransFat()),
                NumberUtils.withStandardPrecision(entity.getMonounsaturatedFat()),
                NumberUtils.withStandardPrecision(entity.getPolyunsaturatedFat()),
                NumberUtils.withStandardPrecision(entity.getSaturatedFat()),
                NumberUtils.withStandardPrecision(entity.getCarbohydrates()),
                NumberUtils.withStandardPrecision(entity.getSugars()),
                NumberUtils.withStandardPrecision(entity.getFiber()),
                NumberUtils.withStandardPrecision(entity.getCholesterol()),
                NumberUtils.withStandardPrecision(entity.getIron()),
                NumberUtils.withStandardPrecision(entity.getManganese()),
                NumberUtils.withStandardPrecision(entity.getCopper()),
                NumberUtils.withStandardPrecision(entity.getZinc()),
                NumberUtils.withStandardPrecision(entity.getCalcium()),
                NumberUtils.withStandardPrecision(entity.getMagnesium()),
                NumberUtils.withStandardPrecision(entity.getPhosphorus()),
                NumberUtils.withStandardPrecision(entity.getPotassium()),
                NumberUtils.withStandardPrecision(entity.getSodium()),
                NumberUtils.withStandardPrecision(entity.getSulfur()),
                NumberUtils.withStandardPrecision(entity.getVitaminA()),
                NumberUtils.withStandardPrecision(entity.getVitaminC()),
                NumberUtils.withStandardPrecision(entity.getVitaminK()),
                NumberUtils.withStandardPrecision(entity.getVitaminD()),
                NumberUtils.withStandardPrecision(entity.getVitaminE()),
                NumberUtils.withStandardPrecision(entity.getVitaminB1()),
                NumberUtils.withStandardPrecision(entity.getVitaminB2()),
                NumberUtils.withStandardPrecision(entity.getVitaminB3()),
                NumberUtils.withStandardPrecision(entity.getVitaminB5()),
                NumberUtils.withStandardPrecision(entity.getVitaminB6()),
                NumberUtils.withStandardPrecision(entity.getVitaminB7()),
                NumberUtils.withStandardPrecision(entity.getVitaminB8()),
                NumberUtils.withStandardPrecision(entity.getVitaminB9()),
                NumberUtils.withStandardPrecision(entity.getVitaminB12())
        );
    }

    // Primarily used for mapping ingredient data (collections of entities that contain food data)
    // to NutritionalSummary objects. Map of Ingredient Food to number of servings of that food.
    public static <T extends AbstractNutritionalDetails> NutritionalSummary toModel(Map<T, BigDecimal> ingredients)
    {
        BigDecimal calories = BigDecimal.ZERO,
                protein = BigDecimal.ZERO,
                totalFats = BigDecimal.ZERO,
                transFat = BigDecimal.ZERO,
                monoFat = BigDecimal.ZERO,
                polyFat = BigDecimal.ZERO,
                satFat = BigDecimal.ZERO,
                carbs = BigDecimal.ZERO,
                sugars = BigDecimal.ZERO,
                fiber = BigDecimal.ZERO,
                cholesterol = BigDecimal.ZERO,
                iron = BigDecimal.ZERO,
                manganese = BigDecimal.ZERO,
                copper = BigDecimal.ZERO,
                zinc = BigDecimal.ZERO,
                calcium = BigDecimal.ZERO,
                magnesium = BigDecimal.ZERO,
                phosphorus = BigDecimal.ZERO,
                potassium = BigDecimal.ZERO,
                sodium = BigDecimal.ZERO,
                sulfur = BigDecimal.ZERO,
                vitaminA = BigDecimal.ZERO,
                vitaminC = BigDecimal.ZERO,
                vitaminK = BigDecimal.ZERO,
                vitaminD = BigDecimal.ZERO,
                vitaminE = BigDecimal.ZERO,
                vitaminB1 = BigDecimal.ZERO,
                vitaminB2 = BigDecimal.ZERO,
                vitaminB3 = BigDecimal.ZERO,
                vitaminB5 = BigDecimal.ZERO,
                vitaminB6 = BigDecimal.ZERO,
                vitaminB7 = BigDecimal.ZERO,
                vitaminB8 = BigDecimal.ZERO,
                vitaminB9 = BigDecimal.ZERO,
                vitaminB12 = BigDecimal.ZERO;

        for(Map.Entry<T, BigDecimal> entry : ingredients.entrySet())
        {
            calories = calories.add(entry.getKey().getCalories().multiply(entry.getValue()));
            protein = protein.add(entry.getKey().getProtein().multiply(entry.getValue()));
            totalFats = totalFats.add(entry.getKey().getTotalFats().multiply(entry.getValue()));
            transFat = transFat.add(entry.getKey().getTransFat().multiply(entry.getValue()));
            monoFat = monoFat.add(entry.getKey().getMonounsaturatedFat().multiply(entry.getValue()));
            polyFat = polyFat.add(entry.getKey().getPolyunsaturatedFat().multiply(entry.getValue()));
            satFat = satFat.add(entry.getKey().getSaturatedFat().multiply(entry.getValue()));
            carbs = carbs.add(entry.getKey().getCarbohydrates().multiply(entry.getValue()));
            sugars = sugars.add(entry.getKey().getSugars().multiply(entry.getValue()));
            fiber = fiber.add(entry.getKey().getFiber().multiply(entry.getValue()));
            cholesterol = cholesterol.add(entry.getKey().getCholesterol().multiply(entry.getValue()));
            iron = iron.add(entry.getKey().getIron().multiply(entry.getValue()));
            manganese = manganese.add(entry.getKey().getManganese().multiply(entry.getValue()));
            copper = copper.add(entry.getKey().getCopper().multiply(entry.getValue()));
            zinc = zinc.add(entry.getKey().getZinc().multiply(entry.getValue()));
            calcium = calcium.add(entry.getKey().getCalcium().multiply(entry.getValue()));
            magnesium = magnesium.add(entry.getKey().getMagnesium().multiply(entry.getValue()));
            phosphorus = phosphorus.add(entry.getKey().getPhosphorus().multiply(entry.getValue()));
            potassium = potassium.add(entry.getKey().getPotassium().multiply(entry.getValue()));
            sodium = sodium.add(entry.getKey().getSodium().multiply(entry.getValue()));
            sulfur = sulfur.add(entry.getKey().getSulfur().multiply(entry.getValue()));
            vitaminA = vitaminA.add(entry.getKey().getVitaminA().multiply(entry.getValue()));
            vitaminC = vitaminC.add(entry.getKey().getVitaminC().multiply(entry.getValue()));
            vitaminK = vitaminK.add(entry.getKey().getVitaminK().multiply(entry.getValue()));
            vitaminD = vitaminD.add(entry.getKey().getVitaminD().multiply(entry.getValue()));
            vitaminE = vitaminE.add(entry.getKey().getVitaminE().multiply(entry.getValue()));
            vitaminB1 = vitaminB1.add(entry.getKey().getVitaminB1().multiply(entry.getValue()));
            vitaminB2 = vitaminB2.add(entry.getKey().getVitaminB2().multiply(entry.getValue()));
            vitaminB3 = vitaminB3.add(entry.getKey().getVitaminB3().multiply(entry.getValue()));
            vitaminB5 = vitaminB5.add(entry.getKey().getVitaminB5().multiply(entry.getValue()));
            vitaminB6 = vitaminB6.add(entry.getKey().getVitaminB6().multiply(entry.getValue()));
            vitaminB7 = vitaminB7.add(entry.getKey().getVitaminB7().multiply(entry.getValue()));
            vitaminB8 = vitaminB8.add(entry.getKey().getVitaminB8().multiply(entry.getValue()));
            vitaminB9 = vitaminB9.add(entry.getKey().getVitaminB9().multiply(entry.getValue()));
            vitaminB12 = vitaminB12.add(entry.getKey().getVitaminB12().multiply(entry.getValue()));
        }

        return new NutritionalSummary(
                calories,
                protein,
                totalFats,
                transFat,
                monoFat,
                polyFat,
                satFat,
                carbs,
                sugars,
                fiber,
                cholesterol,
                iron,
                manganese,
                copper,
                zinc,
                calcium,
                magnesium,
                phosphorus,
                potassium,
                sodium,
                sulfur,
                vitaminA,
                vitaminC,
                vitaminK,
                vitaminD,
                vitaminE,
                vitaminB1,
                vitaminB2,
                vitaminB3,
                vitaminB5,
                vitaminB6,
                vitaminB7,
                vitaminB8,
                vitaminB9,
                vitaminB12
        );
    }
}
