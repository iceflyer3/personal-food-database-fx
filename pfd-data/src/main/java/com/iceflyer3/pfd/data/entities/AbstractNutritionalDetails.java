package com.iceflyer3.pfd.data.entities;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import java.math.BigDecimal;

@MappedSuperclass
public abstract class AbstractNutritionalDetails
{
    @Column(name = "calories", nullable = false, precision = 2)
    protected BigDecimal calories;

    @Column(name = "protein", precision = 2)
    protected BigDecimal protein;

    @Column(name = "carbohydrates", precision = 2)
    protected BigDecimal carbohydrates;

    @Column(name = "total_fats", precision = 2)
    protected BigDecimal totalFats;

    @Column(name = "trans_fat", precision = 2)
    protected BigDecimal transFat;

    @Column(name = "monounsaturated_fat", precision = 2)
    protected BigDecimal monounsaturatedFat;

    @Column(name = "polyunsaturated_fat", precision = 2)
    protected BigDecimal polyunsaturatedFat;

    @Column(name = "saturated_fat", precision = 2)
    protected BigDecimal saturatedFat;

    @Column(name = "sugars", precision = 2)
    protected BigDecimal sugars;

    @Column(name = "fiber", precision = 2)
    protected BigDecimal fiber;

    @Column(name = "cholesterol", precision = 2)
    protected BigDecimal cholesterol;

    @Column(name = "iron", precision = 2)
    protected BigDecimal iron;

    @Column(name = "manganese", precision = 2)
    protected BigDecimal manganese;

    @Column(name = "copper", precision = 2)
    protected BigDecimal copper;

    @Column(name = "zinc", precision = 2)
    protected BigDecimal zinc;

    @Column(name = "calcium", precision = 2)
    protected BigDecimal calcium;

    @Column(name = "magnesium", precision = 2)
    protected BigDecimal magnesium;

    @Column(name = "phosphorus", precision = 2)
    protected BigDecimal phosphorus;

    @Column(name = "potassium", precision = 2)
    protected BigDecimal potassium;

    @Column(name = "sodium", precision = 2)
    protected BigDecimal sodium;

    @Column(name = "sulfur", precision = 2)
    protected BigDecimal sulfur;

    @Column(name = "vitamin_a", precision = 2)
    protected BigDecimal vitaminA;

    @Column(name = "vitamin_c", precision = 2)
    protected BigDecimal vitaminC;

    @Column(name = "vitamin_k", precision = 2)
    protected BigDecimal vitaminK;

    @Column(name = "vitamin_d", precision = 2)
    protected BigDecimal vitaminD;

    @Column(name = "vitamin_e", precision = 2)
    protected BigDecimal vitaminE;

    @Column(name = "vitamin_b1", precision = 2)
    protected BigDecimal vitaminB1;

    @Column(name = "vitamin_b2", precision = 2)
    protected BigDecimal vitaminB2;

    @Column(name = "vitamin_b3", precision = 2)
    protected BigDecimal vitaminB3;

    @Column(name = "vitamin_b5", precision = 2)
    protected BigDecimal vitaminB5;

    @Column(name = "vitamin_b6", precision = 2)
    protected BigDecimal vitaminB6;

    @Column(name = "vitamin_b7", precision = 2)
    protected BigDecimal vitaminB7;

    @Column(name = "vitamin_b8", precision = 2)
    protected BigDecimal vitaminB8;

    @Column(name = "vitamin_b9", precision = 2)
    protected BigDecimal vitaminB9;

    @Column(name = "vitamin_b12", precision = 2)
    protected BigDecimal vitaminB12;

    public BigDecimal getCalories()
    {
        return calories;
    }

    public BigDecimal getProtein()
    {
        return protein;
    }

    public BigDecimal getCarbohydrates()
    {
        return carbohydrates;
    }

    public BigDecimal getTotalFats()
    {
        return totalFats;
    }

    public BigDecimal getTransFat()
    {
        return transFat;
    }

    public BigDecimal getMonounsaturatedFat()
    {
        return monounsaturatedFat;
    }

    public BigDecimal getPolyunsaturatedFat()
    {
        return polyunsaturatedFat;
    }

    public BigDecimal getSaturatedFat()
    {
        return saturatedFat;
    }

    public BigDecimal getSugars()
    {
        return sugars;
    }

    public BigDecimal getFiber()
    {
        return fiber;
    }

    public BigDecimal getCholesterol()
    {
        return cholesterol;
    }

    public BigDecimal getIron()
    {
        return iron;
    }

    public BigDecimal getManganese()
    {
        return manganese;
    }

    public BigDecimal getCopper()
    {
        return copper;
    }

    public BigDecimal getZinc()
    {
        return zinc;
    }

    public BigDecimal getCalcium()
    {
        return calcium;
    }

    public BigDecimal getMagnesium()
    {
        return magnesium;
    }

    public BigDecimal getPhosphorus()
    {
        return phosphorus;
    }

    public BigDecimal getPotassium()
    {
        return potassium;
    }

    public BigDecimal getSodium()
    {
        return sodium;
    }

    public BigDecimal getSulfur()
    {
        return sulfur;
    }

    public BigDecimal getVitaminA()
    {
        return vitaminA;
    }

    public BigDecimal getVitaminC()
    {
        return vitaminC;
    }

    public BigDecimal getVitaminK()
    {
        return vitaminK;
    }

    public BigDecimal getVitaminD()
    {
        return vitaminD;
    }

    public BigDecimal getVitaminE()
    {
        return vitaminE;
    }

    public BigDecimal getVitaminB1()
    {
        return vitaminB1;
    }

    public BigDecimal getVitaminB2()
    {
        return vitaminB2;
    }

    public BigDecimal getVitaminB3()
    {
        return vitaminB3;
    }

    public BigDecimal getVitaminB5()
    {
        return vitaminB5;
    }

    public BigDecimal getVitaminB6()
    {
        return vitaminB6;
    }

    public BigDecimal getVitaminB7()
    {
        return vitaminB7;
    }

    public BigDecimal getVitaminB8()
    {
        return vitaminB8;
    }

    public BigDecimal getVitaminB9()
    {
        return vitaminB9;
    }

    public BigDecimal getVitaminB12()
    {
        return vitaminB12;
    }

    @Override
    public String toString()
    {
        return "AbstractNutritionalDetails{" + "calories=" + calories + ", protein=" + protein + ", carbohydrates=" + carbohydrates + ", totalFats=" + totalFats + ", transFat=" + transFat + ", monounsaturatedFat=" + monounsaturatedFat + ", polyunsaturatedFat=" + polyunsaturatedFat + ", saturatedFat=" + saturatedFat + ", sugars=" + sugars + ", fiber=" + fiber + ", cholesterol=" + cholesterol + ", iron=" + iron + ", manganese=" + manganese + ", copper=" + copper + ", zinc=" + zinc + ", calcium=" + calcium + ", magnesium=" + magnesium + ", phosphorus=" + phosphorus + ", potassium=" + potassium + ", sodium=" + sodium + ", sulfur=" + sulfur + ", vitaminA=" + vitaminA + ", vitaminC=" + vitaminC + ", vitaminK=" + vitaminK + ", vitaminD=" + vitaminD + ", vitaminE=" + vitaminE + ", vitaminB1=" + vitaminB1 + ", vitaminB2=" + vitaminB2 + ", vitaminB3=" + vitaminB3 + ", vitaminB5=" + vitaminB5 + ", vitaminB6=" + vitaminB6 + ", vitaminB7=" + vitaminB7 + ", vitaminB8=" + vitaminB8 + ", vitaminB9=" + vitaminB9 + ", vitaminB12=" + vitaminB12 + '}';
    }
}
