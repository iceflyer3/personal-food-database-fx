package com.iceflyer3.pfd.data.mapping.helper;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.entities.recipe.RecipeIngredient;
import com.iceflyer3.pfd.data.entities.recipe.RecipeStep;
import com.iceflyer3.pfd.data.entities.recipe.RecipeStepIngredient;
import com.iceflyer3.pfd.data.mapping.IngredientMapper;
import com.iceflyer3.pfd.data.mapping.recipe.RecipeStepMapper;
import com.iceflyer3.pfd.data.repository.FoodRepository;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.api.recipe.RecipeStepModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.ReadOnlyFoodViewModel;

import java.util.*;

/**
 * When mapping from Database to ViewModel for ingredients and steps that belong to a recipe
 * there is an intermediate step. The actual foods associated with each recipe ingredient must
 * also be queried and mapped into a ViewModel (ReadOnlyFoodViewModel to be specific).
 *
 * This is a helper class that performs this intermediate step so that we can perform it from
 * the various recipe related Task classes as needed.
 */
public class RecipeMapperHelper
{
    private final FoodRepository foodRepository;

    public RecipeMapperHelper(FoodRepository foodRepository)
    {
        this.foodRepository = foodRepository;
    }

    public List<IngredientModel> getRecipeIngredientModels(Collection<RecipeIngredient> ingredientEntities)
    {
        // Load and map all the ingredients for the recipe
        List<IngredientModel> ingredientViewModels = new ArrayList<>();
        for(RecipeIngredient ingredientEntity : ingredientEntities)
        {
            ReadOnlyFoodViewModel ingredientFood = foodRepository.getFoodForId(ingredientEntity.getFoodMeta().getFoodId());
            ingredientViewModels.add(IngredientMapper.toModel(ingredientEntity, ingredientFood));
        }
        return ingredientViewModels;
    }

    public List<RecipeStepModel> getStepModels(Collection<RecipeStep> stepEntities)
    {
        // Load and map all the steps and their respective ingredients for the recipe
        List<RecipeStepModel> recipeStepModels = new ArrayList<>();
        for(RecipeStep stepEntity : stepEntities)
        {
            if(stepEntity.getStepIngredients().size() > 0)
            {
                // Map the ingredients
                List<IngredientModel> stepIngredients = new ArrayList<>();
                for(RecipeStepIngredient stepIngredient : stepEntity.getStepIngredients())
                {
                    ReadOnlyFoodViewModel ingredientFoodEntity = foodRepository.getFoodForId(stepIngredient.getRecipeIngredient().getFoodMeta().getFoodId());
                    stepIngredients.add(IngredientMapper.toModel(stepIngredient, ingredientFoodEntity));
                }
                recipeStepModels.add(RecipeStepMapper.toModel(stepEntity, stepIngredients));
            }
            else
            {
                recipeStepModels.add(RecipeStepMapper.toModel(stepEntity, new ArrayList<>()));
            }
        }

        recipeStepModels.sort(Comparator.comparingInt(RecipeStepModel::getStepNumber));
        return recipeStepModels;
    }
}
