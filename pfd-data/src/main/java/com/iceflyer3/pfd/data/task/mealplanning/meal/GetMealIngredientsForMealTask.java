package com.iceflyer3.pfd.data.task.mealplanning.meal;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanMealIngredient;
import com.iceflyer3.pfd.data.mapping.IngredientMapper;
import com.iceflyer3.pfd.data.repository.FoodRepository;
import com.iceflyer3.pfd.data.repository.MealPlanningRepository;
import com.iceflyer3.pfd.data.task.mealplanning.AbstractMealPlanningTask;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.viewmodel.IngredientViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.ReadOnlyFoodViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class GetMealIngredientsForMealTask extends AbstractMealPlanningTask<List<IngredientModel>>
{

    private final FoodRepository foodRepository;
    private final UUID forMealId;

    public GetMealIngredientsForMealTask(MealPlanningRepository mealPlanningRepository, FoodRepository foodRepository, UUID forMealId) {
        super(mealPlanningRepository);
        this.foodRepository = foodRepository;
        this.forMealId = forMealId;
    }

    @Override
    protected List<IngredientModel> call() throws Exception {

        List<MealPlanMealIngredient> ingredientEntities = mealPlanningRepository.getIngredientsForMeal(forMealId);

        List<IngredientModel> ingredientViewModels = new ArrayList<>();
        for(MealPlanMealIngredient ingredientEntity : ingredientEntities)
        {
            ReadOnlyFoodViewModel ingredientFood = foodRepository.getFoodForId(ingredientEntity.getFoodMeta().getFoodId());
            ingredientViewModels.add(IngredientMapper.toModel(ingredientEntity, ingredientFood));
        }

        return ingredientViewModels;
    }
}
