package com.iceflyer3.pfd.data.factory;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.repository.FoodRepository;
import com.iceflyer3.pfd.data.repository.MealPlanningRepository;
import com.iceflyer3.pfd.data.request.mealplanning.AssociateMealPlanDayToCalendarDayRequest;
import com.iceflyer3.pfd.data.request.mealplanning.GroceryListForMealPlanCalendarDateRangeSearchRequest;
import com.iceflyer3.pfd.data.task.mealplanning.*;
import com.iceflyer3.pfd.data.task.mealplanning.calendar.DeleteMealPlanCalendarDayTask;
import com.iceflyer3.pfd.data.task.mealplanning.calendar.FindGroceryListForCalendarDateRangeTask;
import com.iceflyer3.pfd.data.task.mealplanning.calendar.GetMealPlanCalendarDaysForUserTask;
import com.iceflyer3.pfd.data.task.mealplanning.calendar.SaveMealPlanCalendarDayTask;
import com.iceflyer3.pfd.data.task.mealplanning.carbcycling.*;
import com.iceflyer3.pfd.data.task.mealplanning.carbcycling.day.GetCarbCyclingDayByIdTask;
import com.iceflyer3.pfd.data.task.mealplanning.carbcycling.day.GetCarbCyclingDaysForCarbCyclingPlanTask;
import com.iceflyer3.pfd.data.task.mealplanning.carbcycling.day.SaveCarbCyclingDayTask;
import com.iceflyer3.pfd.data.task.mealplanning.carbcycling.meal.GetCarbCyclingMealByIdTask;
import com.iceflyer3.pfd.data.task.mealplanning.carbcycling.meal.GetCarbCyclingMealsForDayTask;
import com.iceflyer3.pfd.data.task.mealplanning.carbcycling.meal.SaveCarbCyclingMealTask;
import com.iceflyer3.pfd.data.task.mealplanning.day.DeleteMealPlanDayTask;
import com.iceflyer3.pfd.data.task.mealplanning.day.GetDayForMealPlanByIdTask;
import com.iceflyer3.pfd.data.task.mealplanning.day.GetDaysForMealPlanTask;
import com.iceflyer3.pfd.data.task.mealplanning.day.SaveDayForMealPlanTask;
import com.iceflyer3.pfd.data.task.mealplanning.meal.*;
import com.iceflyer3.pfd.ui.model.api.mealplanning.MealPlanMealModel;
import com.iceflyer3.pfd.ui.model.api.mealplanning.registration.MealPlanningRegistration;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanDayViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanMealViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingDayViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingMealViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingPlanViewModel;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class MealPlanningTaskFactory
{

    private final FoodRepository foodRepository;
    private final MealPlanningRepository mealPlanningRepository;

    public MealPlanningTaskFactory(MealPlanningRepository mealPlanningRepository, FoodRepository foodRepository) {
        this.mealPlanningRepository = mealPlanningRepository;
        this.foodRepository = foodRepository;
    }

    ///////////////////////////////////////////////////////////////////
    //
    //                           MEAL PLANS
    //
    ///////////////////////////////////////////////////////////////////

    public GetMealPlanByIdTask getMealPlanById(UUID planId)
    {
        return new GetMealPlanByIdTask(mealPlanningRepository, planId);
    }

    public GetActiveMealPlanForUserTask getActiveMealPlanForUser(UUID userId)
    {
        return new GetActiveMealPlanForUserTask(mealPlanningRepository, userId);
    }

    public GetMealPlansForUserTask getMealPlansByUserAsync(UUID forUserId)
    {
        return new GetMealPlansForUserTask(mealPlanningRepository, forUserId);
    }

    public SaveMealPlanTask createPlanAsync(UUID forUserId, MealPlanningRegistration registrationDetails)
    {
        return new SaveMealPlanTask(mealPlanningRepository, forUserId, registrationDetails);
    }

    public DeleteMealPlanTask deletePlanAsync(UUID planIdToDelete)
    {
        return new DeleteMealPlanTask(mealPlanningRepository, planIdToDelete);
    }

    public ChangeMealPlanActivationStatusTask changeMealPlanActivationStatusAsync(UUID planToChange, boolean isActive)
    {
        return new ChangeMealPlanActivationStatusTask(mealPlanningRepository, planToChange, isActive);
    }

    ///////////////////////////////////////////////////////////////////
    //
    //                          MEAL PLAN DAYS
    //
    ///////////////////////////////////////////////////////////////////

    public GetDayForMealPlanByIdTask getDayByIdAsync(UUID dayId)
    {
        return new GetDayForMealPlanByIdTask(mealPlanningRepository, dayId);
    }

    public GetCarbCyclingDayByIdTask getCarbCyclingDayByIdTask(UUID dayId)
    {
        return new GetCarbCyclingDayByIdTask(mealPlanningRepository, dayId);
    }

    public GetDaysForMealPlanTask getDaysForMealPlan(UUID forMealPlanId)
    {
        return new GetDaysForMealPlanTask(mealPlanningRepository, forMealPlanId);
    }

    public GetCarbCyclingDaysForCarbCyclingPlanTask getCarbCyclingDaysForMealPlan(UUID forCarbCyclingPlanId)
    {
        return new GetCarbCyclingDaysForCarbCyclingPlanTask(mealPlanningRepository, forCarbCyclingPlanId);
    }

    public SaveDayForMealPlanTask saveDayForMealPlanAsync(MealPlanDayViewModel newDay)
    {
        return new SaveDayForMealPlanTask(mealPlanningRepository, newDay);
    }

    public SaveCarbCyclingDayTask saveCarbCyclingDayForMealPlanAsync(CarbCyclingDayViewModel newDay)
    {
        return new SaveCarbCyclingDayTask(mealPlanningRepository, newDay);
    }

    public DeleteMealPlanDayTask deleteDayAsync(UUID dayId)
    {
        return new DeleteMealPlanDayTask(mealPlanningRepository, dayId);
    }

    ///////////////////////////////////////////////////////////////////
    //
    //                          MEAL PLAN MEALS
    //
    ///////////////////////////////////////////////////////////////////

    public GetMealByIdTask getMealByIdAsync(UUID mealId)
    {
        return new GetMealByIdTask(mealPlanningRepository, mealId);
    }

    public GetMealsForDayTask getMealsForDay(UUID dayId)
    {
        return new GetMealsForDayTask(mealPlanningRepository, dayId);
    }

    public GetCarbCyclingMealByIdTask getCarbCyclingMealByIdAsync(UUID mealId)
    {
        return new GetCarbCyclingMealByIdTask(mealPlanningRepository, mealId);
    }

    public GetCarbCyclingMealsForDayTask getCarbCyclingMealsForDay(UUID dayId)
    {
        return new GetCarbCyclingMealsForDayTask(mealPlanningRepository, dayId);
    }

    public SaveMealForMealPlanTask saveMealAsync(MealPlanDayViewModel forDay, MealPlanMealViewModel mealViewModel)
    {
        return new SaveMealForMealPlanTask(mealPlanningRepository, forDay, mealViewModel);
    }

    public SaveCarbCyclingMealTask saveCarbCyclingMealAsync(CarbCyclingDayViewModel forDay, CarbCyclingMealViewModel carbCyclingMeal)
    {
        return new SaveCarbCyclingMealTask(mealPlanningRepository, forDay, carbCyclingMeal);
    }

    public DeleteMealPlanMealTask deleteMealAsync(UUID mealId)
    {
        return new DeleteMealPlanMealTask(mealPlanningRepository, mealId);
    }

    ///////////////////////////////////////////////////////////////////
    //
    //                      MEAL PLAN MEAL INGREDIENTS
    //
    ///////////////////////////////////////////////////////////////////

    public GetMealIngredientsForMealTask getMealIngredientsAsync(UUID mealId)
    {
        return new GetMealIngredientsForMealTask(mealPlanningRepository, foodRepository, mealId);
    }

    public SaveMealIngredientsForMealPlanTask saveMealIngredientsAsync(MealPlanMealModel mealPlanMealModel)
    {
        return new SaveMealIngredientsForMealPlanTask(mealPlanningRepository, foodRepository, mealPlanMealModel);
    }

    ///////////////////////////////////////////////////////////////////
    //
    //                      CARB CYCLING PLANS
    //
    ///////////////////////////////////////////////////////////////////

    public GetCarbCyclingPlanByIdTask getCarbCyclingPlanByIdAsync(UUID planId)
    {
        return new GetCarbCyclingPlanByIdTask(mealPlanningRepository, planId);
    }

    public GetCarbCyclingPlansForMealPlanTask getCarbCyclingPlanByMealPlanAsync(UUID mealPlanId)
    {
        return new GetCarbCyclingPlansForMealPlanTask(mealPlanningRepository, mealPlanId);
    }

    public GetCarbCyclingMealConfigurationsByPlanIdTask getCarbCyclingPlanMealConfigurations(UUID planId)
    {
        return new GetCarbCyclingMealConfigurationsByPlanIdTask(mealPlanningRepository, planId);
    }

    public SaveCarbCyclingPlanTask saveCarbCyclingPlanAsync(UUID forUserId, UUID forMealPlanId, CarbCyclingPlanViewModel planDetails)
    {
        return new SaveCarbCyclingPlanTask(mealPlanningRepository, forUserId, forMealPlanId, planDetails);
    }

    public ToggleCarbCyclingPlanStatusTask toggleCarbCyclingPlanStatus(UUID planId)
    {
        return new ToggleCarbCyclingPlanStatusTask(mealPlanningRepository, planId);
    }

    public DeleteCarbCyclingPlanTask deleteCarbCyclingPlanAsync(UUID planId)
    {
        return new DeleteCarbCyclingPlanTask(mealPlanningRepository, planId);
    }

    ///////////////////////////////////////////////////////////////////
    //
    //                      MEAL PLAN CALENDAR DAYS
    //
    ///////////////////////////////////////////////////////////////////

    public FindGroceryListForCalendarDateRangeTask getGroceryList(GroceryListForMealPlanCalendarDateRangeSearchRequest searchRequest)
    {
        return new FindGroceryListForCalendarDateRangeTask(mealPlanningRepository, foodRepository, searchRequest);
    }

    public GetMealPlanCalendarDaysForUserTask getCalendarDaysForUser(UUID forUserId)
    {
        return new GetMealPlanCalendarDaysForUserTask(mealPlanningRepository, forUserId);
    }

    public SaveMealPlanCalendarDayTask createCalendarDay(AssociateMealPlanDayToCalendarDayRequest request)
    {
        return new SaveMealPlanCalendarDayTask(mealPlanningRepository, request);
    }

    public DeleteMealPlanCalendarDayTask deleteCalendarDay(UUID calendarDayId)
    {
        return new DeleteMealPlanCalendarDayTask(mealPlanningRepository, calendarDayId);
    }
}
