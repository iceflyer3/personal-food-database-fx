/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.entities;

import com.iceflyer3.pfd.constants.QueryConstants;
import jakarta.persistence.*;

import java.util.UUID;

@Entity
@Table(name="pfd_user")
@NamedQuery(name = QueryConstants.QUERY_USER_FIND_ALL, query = "SELECT user FROM User user")
@NamedQuery(name = QueryConstants.QUERY_USER_BY_USERNAME, query = "SELECT user FROM User user WHERE user.username = :username")
public class User {

    public User() { }

    /**
     * Used when creating a new user
     * @param username The username of the user
     */
    public User(String username) {
        this.username = username;
    }

    @Id
    @GeneratedValue
    @Column(name="user_id")
    protected UUID userId;

    @Column(name="username", nullable = false)
    protected String username;

    public UUID getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", username='" + username + '\'' +
                '}';
    }
}
