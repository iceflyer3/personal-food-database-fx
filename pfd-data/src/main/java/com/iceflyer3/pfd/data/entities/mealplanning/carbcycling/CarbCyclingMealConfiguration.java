/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.entities.mealplanning.carbcycling;

import com.iceflyer3.pfd.constants.QueryConstants;
import com.iceflyer3.pfd.enums.CarbCyclingMealRelation;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.util.UUID;

@Entity
@Table(name = "carb_cycling_meal_configuration")
@NamedQuery(
        name = QueryConstants.QUERY_MEAL_PLANNING_FIND_MEAL_CONFIGURATIONS_FOR_USER,
        query = "SELECT mealConfig FROM CarbCyclingMealConfiguration mealConfig WHERE mealConfig.carbCyclingPlan.user.userId = :userId")
@NamedQuery(
        name = QueryConstants.QUERY_MEAL_PLANNING_FIND_MEAL_CONFIGURATIONS_FOR_CARB_CYCLING_PLAN,
        query = "SELECT mealConfig FROM CarbCyclingMealConfiguration mealConfig WHERE mealConfig.carbCyclingPlan.planId = :carbCyclingPlanId")
public class CarbCyclingMealConfiguration
{
    public CarbCyclingMealConfiguration() { }

    public CarbCyclingMealConfiguration(
            CarbCyclingPlan forPlan,
            BigDecimal carbConsumptionMaximumPercent,
            BigDecimal carbConsumptionMinimumPercent,
            int postWorkoutMealOffset,
            CarbCyclingMealRelation postWorkoutMealRelation,
            boolean isForTrainingDay
    )
    {
        this(
                null,
                forPlan,
                carbConsumptionMaximumPercent,
                carbConsumptionMinimumPercent,
                postWorkoutMealOffset,
                postWorkoutMealRelation,
                isForTrainingDay
        );
    }

    public CarbCyclingMealConfiguration(
            UUID mealConfigurationId,
            CarbCyclingPlan forPlan,
            BigDecimal carbConsumptionMaximumPercent,
            BigDecimal carbConsumptionMinimumPercent,
            int postWorkoutMealOffset,
            CarbCyclingMealRelation postWorkoutMealRelation,
            boolean isForTrainingDay
    )
    {
        this.mealConfigurationId = mealConfigurationId;
        this.carbCyclingPlan = forPlan;
        this.carbConsumptionMaximumPercent = carbConsumptionMaximumPercent;
        this.carbConsumptionMinimumPercent = carbConsumptionMinimumPercent;
        this.postWorkoutMealOffset = postWorkoutMealOffset;
        this.postWorkoutMealRelation = postWorkoutMealRelation;
        this.isForTrainingDay = isForTrainingDay;
    }

    @Id
    @GeneratedValue
    @Column(name = "meal_configuration_id")
    protected UUID mealConfigurationId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "carb_cycling_plan_id")
    protected CarbCyclingPlan carbCyclingPlan;

    @Column(name = "carb_consumption_min_percentage", nullable = false)
    protected BigDecimal carbConsumptionMinimumPercent;

    @Column(name = "carb_consumption_max_percentage", nullable = false)
    protected BigDecimal carbConsumptionMaximumPercent;

    // This will be zero if the meal is the post-workout meal
    @Column(name = "post_workout_meal_offset", nullable = false)
    protected Integer postWorkoutMealOffset;

    @Enumerated(EnumType.STRING)
    @Column(name = "post_workout_meal_relation", nullable = false)
    protected CarbCyclingMealRelation postWorkoutMealRelation;

    @Column(name = "is_for_training_day", nullable = false)
    protected boolean isForTrainingDay;

    public UUID getMealConfigurationId()
    {
        return mealConfigurationId;
    }

    public CarbCyclingPlan getCarbCyclingPlan()
    {
        return carbCyclingPlan;
    }

    public BigDecimal getCarbConsumptionMinimumPercent()
    {
        return carbConsumptionMinimumPercent;
    }

    public BigDecimal getCarbConsumptionMaximumPercent()
    {
        return carbConsumptionMaximumPercent;
    }

    public Integer getPostWorkoutMealOffset()
    {
        return postWorkoutMealOffset;
    }

    public CarbCyclingMealRelation getPostWorkoutMealRelation()
    {
        return postWorkoutMealRelation;
    }

    public boolean isForTrainingDay()
    {
        return isForTrainingDay;
    }
}
