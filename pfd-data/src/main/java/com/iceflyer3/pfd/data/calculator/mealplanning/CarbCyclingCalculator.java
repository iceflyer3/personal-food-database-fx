package com.iceflyer3.pfd.data.calculator.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.enums.CarbCyclingDayType;
import com.iceflyer3.pfd.enums.FitnessGoal;
import com.iceflyer3.pfd.enums.Nutrient;
import com.iceflyer3.pfd.util.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;

public class CarbCyclingCalculator extends AbstractMealPlanCalculator
{
    private static final Logger LOG = LoggerFactory.getLogger(CarbCyclingCalculator.class);
    private static CarbCyclingCalculator calculator;

    private CarbCyclingCalculator() { }

    public static CarbCyclingCalculator instance()
    {
        if (calculator == null)
        {
            calculator = new CarbCyclingCalculator();
        }

        return calculator;
    }

    public CalculationResult calculateDayRequirements(CarbCyclingDayRequirementsCalcuationRequest request)
    {
        // Calculate calories
        BigDecimal requiredCalories = calculateCaloriesForDay(request);

        // Calculate macros
        Map<Nutrient, BigDecimal> macroSuggestions = calculateMacrosForDay(requiredCalories, request);

        CalculationResult calculationResult = new CalculationResult(request.getTdeeCalories(), requiredCalories, macroSuggestions);
        LOG.debug("Carb cycling day calculation results {}", calculationResult);
        return calculationResult;
    }

    public CalculationResult calculateMealRequirements(CarbCyclingMealCalculationRequest request) {
        /*
         * Some explanation about how carb cycling meals work.
         *
         * They don't have a target amount of calories per meal that they strive to hit
         * like the carb cycling day does.
         *
         * It is opposite to the carb cycling day which splits a target number of
         * calories out into macro requirements for the day.
         *
         * Meals instead combine calories from a target number of each macro for the
         * meal to find the total calories for the meal.
         *
         * As for the target numbers of macros for the meal:
         *      - Protein will always be split evenly across all meals.
         *      - Fats will be split evenly across all meals or split evenly
         *        across all meals but two (pre and post-workout) if timing fats.
         *      - Carbs are determined by where the meal falls in the day as well
         *        as where it falls in relation to a workout and are a percentage
         *        of the total carbs that need to be consumed for that day.
         *
         * This functionality has an implicit assumption of a one workout per day schedule
         * and is a current limitation of the carb cycling functionality. More research
         * would need to be conducted to see if the carb cycling approach is even feasible
         * with two or more workouts in a day.
         */

        BigDecimal mealCalories, proteinGrams, carbGrams, fatGrams;
        BigDecimal numMealsPerDay = BigDecimal.valueOf((double)request.getNumMealsForDay());

        // Protein will always be spread evenly across all meals
        proteinGrams = request.getDailyRequiredProtein().divide(numMealsPerDay, RoundingMode.HALF_UP);

        // Calculate carbs based on the percentage that should be consumed for this meal
        BigDecimal carbConsumptionPercentage = request.getCarbConsumptionPercentage().divide(BigDecimal.valueOf(100.0), 2, RoundingMode.HALF_UP);
        carbGrams = request.getDailyRequiredCarbs().multiply(carbConsumptionPercentage);

        // By default fats will be spread evenly across all meals. This can change, see comments below.
        fatGrams = request.getDailyRequiredFats().divide(numMealsPerDay, RoundingMode.HALF_UP);

        if (request.isForTrainingDay() && request.isTimingFats())
        {
            // When timing fats the post-workout meal and the meal prior to the post-workout meal
            // (the two meals surrounding the workout) should contain no fats.
            if (request.getMealNumber() == request.getPostWorkoutMealNumber() || request.getMealNumber() == (request.getPostWorkoutMealNumber() - 1))
            {
                fatGrams = new BigDecimal(0);
            }
            else
            {
                // No fats in the pre or post-workout meals when timing fats
                double numMealsWhenTimingFats = (request.getNumMealsForDay() - 2);
                fatGrams = request.getDailyRequiredFats().divide(BigDecimal.valueOf(numMealsWhenTimingFats), RoundingMode.HALF_UP);
            }
        }

        BigDecimal proteinCalories = proteinGrams.multiply(BigDecimal.valueOf(4.0));
        BigDecimal carbCalories = proteinGrams.multiply(BigDecimal.valueOf(4.0));
        BigDecimal fatCalories = proteinGrams.multiply(BigDecimal.valueOf(4.0));
        mealCalories = proteinCalories.add(carbCalories).add(fatCalories);

        // TDEE calories aren't used here
        Map<Nutrient, BigDecimal> macroSuggestions = Map.of(
                Nutrient.PROTEIN, NumberUtils.withStandardPrecision(proteinGrams),
                Nutrient.CARBOHYDRATES, NumberUtils.withStandardPrecision(carbGrams),
                Nutrient.TOTAL_FATS, NumberUtils.withStandardPrecision(fatGrams)
                );
        return new CalculationResult(BigDecimal.ZERO, mealCalories, macroSuggestions);
    }

    private BigDecimal calculateCaloriesForDay(CarbCyclingDayRequirementsCalcuationRequest requirementsRequest)
    {
        BigDecimal caloriesForDay = new BigDecimal(0);
        switch(requirementsRequest.getFitnessGoal())
        {
            case LOSE:
                BigDecimal deficit;
                if (requirementsRequest.getDayType() == CarbCyclingDayType.HIGH)
                {
                    deficit = requirementsRequest.getTdeeCalories().multiply(BigDecimal.valueOf(0.10));
                }
                else
                {
                    deficit = requirementsRequest.getTdeeCalories().multiply(BigDecimal.valueOf(0.25));
                }
                caloriesForDay = requirementsRequest.getTdeeCalories().subtract(deficit);
                break;
            case GAIN:
                BigDecimal surplus = requirementsRequest.getTdeeCalories().multiply(BigDecimal.valueOf(0.1));
                caloriesForDay = requirementsRequest.getTdeeCalories().add(surplus);
                break;
            case MAINTAIN:
                caloriesForDay = requirementsRequest.getTdeeCalories();
                break;
        }

        return caloriesForDay;
    }

    private Map<Nutrient, BigDecimal> calculateMacrosForDay(BigDecimal requiredCalories, CarbCyclingDayRequirementsCalcuationRequest requirementsRequest)
    {
        // These calculations are all based upon body weight as pounds. However, we store kilograms in the db, so we must convert or the calculation
        // performed here will be wrong
        BigDecimal bodyWeightAsPounds = requirementsRequest.getBodyWeight().multiply(NumberUtils.KG_TO_POUND_CONVERSION_FACTOR);

        BigDecimal caloriesFromCarbs;

        // Our fat calories are just the remaining calories after we calculate our protein and carbs calories
        BigDecimal caloriesFromFats;

        // One gram of protein per body pound of body weight
        BigDecimal caloriesFromProtein = bodyWeightAsPounds.multiply(BigDecimal.valueOf(4.0));
        caloriesFromFats = requiredCalories.subtract(caloriesFromProtein);

        if (requirementsRequest.getFitnessGoal() == FitnessGoal.LOSE)
        {
            if (requirementsRequest.getDayType() == CarbCyclingDayType.LOW)
            {
                caloriesFromCarbs = requiredCalories.multiply(BigDecimal.valueOf(0.2));
            }
            else
            {
                caloriesFromCarbs = requiredCalories.multiply(BigDecimal.valueOf(0.5));
            }
        }
        else
        {
            if (requirementsRequest.getDayType() == CarbCyclingDayType.LOW)
            {
                caloriesFromCarbs = requiredCalories.multiply(BigDecimal.valueOf(0.25));
            }
            else
            {
                caloriesFromCarbs = requiredCalories.multiply(BigDecimal.valueOf(0.5));
            }

        }
        caloriesFromFats = caloriesFromFats.subtract(caloriesFromCarbs);

        return calculateMacronutrientGramsFromCalories(caloriesFromProtein, caloriesFromCarbs, caloriesFromFats);
    }
}
