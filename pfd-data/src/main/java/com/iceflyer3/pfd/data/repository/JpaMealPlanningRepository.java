package com.iceflyer3.pfd.data.repository;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.constants.QueryConstants;
import com.iceflyer3.pfd.data.AppConfigUtils;
import com.iceflyer3.pfd.data.FoodImportResult;
import com.iceflyer3.pfd.data.calculator.mealplanning.CalculationResult;
import com.iceflyer3.pfd.data.calculator.mealplanning.MealPlanningCalculator;
import com.iceflyer3.pfd.data.entities.User;
import com.iceflyer3.pfd.data.entities.food.FoodMetaData;
import com.iceflyer3.pfd.data.entities.mealplanning.*;
import com.iceflyer3.pfd.data.entities.mealplanning.carbcycling.CarbCyclingDay;
import com.iceflyer3.pfd.data.entities.mealplanning.carbcycling.CarbCyclingMeal;
import com.iceflyer3.pfd.data.entities.mealplanning.carbcycling.CarbCyclingMealConfiguration;
import com.iceflyer3.pfd.data.entities.mealplanning.carbcycling.CarbCyclingPlan;
import com.iceflyer3.pfd.data.entities.views.CarbCyclingDayNutritionalSummary;
import com.iceflyer3.pfd.data.entities.views.CarbCyclingMealNutritionalSummary;
import com.iceflyer3.pfd.data.entities.views.MealPlanDayNutritionalSummary;
import com.iceflyer3.pfd.data.entities.views.MealPlanMealNutritionalSummary;
import com.iceflyer3.pfd.data.json.JsonMealPlanningData;
import com.iceflyer3.pfd.data.json.mealplanning.*;
import com.iceflyer3.pfd.data.json.mealplanning.carbcycling.JsonCarbCyclingDay;
import com.iceflyer3.pfd.data.json.mealplanning.carbcycling.JsonCarbCyclingMeal;
import com.iceflyer3.pfd.data.json.mealplanning.carbcycling.JsonCarbCyclingMealConfiguration;
import com.iceflyer3.pfd.data.json.mealplanning.carbcycling.JsonCarbCyclingPlan;
import com.iceflyer3.pfd.data.mapping.IngredientMapper;
import com.iceflyer3.pfd.data.mapping.mealplanning.*;
import com.iceflyer3.pfd.data.mapping.mealplanning.carbcycling.CarbCyclingDayMapper;
import com.iceflyer3.pfd.data.mapping.mealplanning.carbcycling.CarbCyclingMealConfigurationMapper;
import com.iceflyer3.pfd.data.mapping.mealplanning.carbcycling.CarbCyclingMealMapper;
import com.iceflyer3.pfd.data.mapping.mealplanning.carbcycling.CarbCyclingPlanMapper;
import com.iceflyer3.pfd.data.request.mealplanning.*;
import com.iceflyer3.pfd.data.request.mealplanning.day.CreateCarbCyclingDayRequest;
import com.iceflyer3.pfd.data.request.mealplanning.day.CreateDayRequest;
import com.iceflyer3.pfd.data.request.mealplanning.day.ModifyDayRequest;
import com.iceflyer3.pfd.data.request.mealplanning.meal.CreateCarbCyclingMealRequest;
import com.iceflyer3.pfd.data.request.mealplanning.meal.CreateMealRequest;
import com.iceflyer3.pfd.data.request.mealplanning.meal.ModifyMealRequest;
import com.iceflyer3.pfd.enums.*;
import com.iceflyer3.pfd.exception.DataImportException;
import com.iceflyer3.pfd.exception.InvalidApplicationStateException;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.api.mealplanning.MealPlanNutrientSuggestionModel;
import com.iceflyer3.pfd.ui.model.api.mealplanning.carbcycling.CarbCyclingMealConfigurationModel;
import com.iceflyer3.pfd.ui.model.api.mealplanning.registration.MealPlanningRegistration;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingDayViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingPlanViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.registration.MealPlanningCalculatedRegistration;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.registration.MealPlanningManualRegistration;
import com.iceflyer3.pfd.user.UserAuthentication;
import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.persistence.PersistenceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Facilitates database access for all meal planning entities.
 *
 * Due to the fact that both meal plan entities and carb cycling entities share information
 * the database treats carb cycling entities as an extension of meal planning entities (even
 * though most of the rest of the application treats these as distinct concepts; the lone
 * exception being that carb cycling plans belong to a meal plan).
 *
 * This means that some functions may operate on either a meal plan entity or a carb cycling
 * entity and there is no need for two distinct functions in this case.
 *
 * For an example of this behavior see the carb cycling day. The primary key is mapped as a one-to-one
 * with the meal plan day that is created when it is. Next, see the function for deleting a day and
 * then notice that there are no deleteMealPlanDay and deleteCarbCyclingDay functions because there
 * don't need to be.
 */
@Repository
@Transactional
public class JpaMealPlanningRepository implements MealPlanningRepository {

    private static final Logger LOG = LoggerFactory.getLogger(JpaMealPlanningRepository.class);

    private final AppConfigUtils appConfigUtils;

    // Unfortunately it seems like the EntityManager has to use field injection instead of constructor
    // See: https://github.com/spring-projects/spring-framework/issues/15076
    @PersistenceContext
    private EntityManager entityManager;

    public JpaMealPlanningRepository(AppConfigUtils appConfigUtils)
    {
        this.appConfigUtils = appConfigUtils;
    }

    // region Meal Plans
    ///////////////////////////////////////////////////////////////////
    //
    //                           MEAL PLANS
    //
    ///////////////////////////////////////////////////////////////////

    @Override
    public MealPlan getMealPlanForId(UUID planId)
    {
        MealPlan mealPlan = entityManager.find(MealPlan.class, planId);

        if (mealPlan == null)
        {
            throw new NoResultException(String.format("No meal plan with id %s was found", planId));
        }
        else
        {
            return mealPlan;
        }
    }

    @Override
    public Optional<MealPlan> getActiveMealPlanForUser(UUID userId)
    {
        try
        {
            MealPlan entityMealPlan =  entityManager
                    .createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_FIND_ACTIVE_MEAL_PLAN, MealPlan.class)
                    .setParameter(QueryConstants.PARAMETER_USER_ID, userId)
                    .getSingleResult();

            return Optional.of(entityMealPlan);
        }
        catch (NoResultException nre)
        {
            return Optional.empty();
        }
    }

    @Override
    public List<MealPlan> getMealPlansForUser(UUID userId)
    {
        return entityManager
                .createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_FIND_PLANS_FOR_USER, MealPlan.class)
                .setParameter(QueryConstants.PARAMETER_USER_ID, userId)
                .getResultList();
    }

    @Override
    public MealPlan createMealPlan(CreateMealPlanRequest planCreationRequest)
    {
        LOG.debug("Creating new meal plan for user {}...", planCreationRequest.getUserId());
        User user = entityManager.find(User.class, planCreationRequest.getUserId());

        // Find any already active plan and deactivate it as only one meal plan may be active at a time
        Optional<MealPlan> activePlan = getActiveMealPlanForUser(user.getUserId());
        activePlan.ifPresent(mealPlan -> mealPlan.setIsActive(false));

        MealPlan newPlan;
        MealPlanningRegistration registrationDetails = planCreationRequest.getRegistrationDetails();
        if (registrationDetails instanceof MealPlanningCalculatedRegistration calculatedRegistration)
        {
            newPlan = createCalculatedMealPlan(user, calculatedRegistration);
        }
        else
        {
            newPlan = createManualMealPlan(user, (MealPlanningManualRegistration) registrationDetails);
        }

        // Save out any custom nutrient information
        for(MealPlanNutrientSuggestionModel customNutrientDetails : registrationDetails.getCustomNutrients())
        {
            MealPlanNutrientSuggestion newCustomNutrientSuggestion = new MealPlanNutrientSuggestion(newPlan, customNutrientDetails.getNutrient(), customNutrientDetails.getSuggestedAmount(), customNutrientDetails.getUnitOfMeasure());
            entityManager.persist(newCustomNutrientSuggestion);
            newPlan.getNutrientSuggestions().add(newCustomNutrientSuggestion);
        }

        LOG.debug("New meal plan has been persisted!");
        return newPlan;
    }

    @Override
    public MealPlan updateMealPlanActiveStatus(ChangeMealPlanActivationStatusRequest request) {
        MealPlan mealPlan = getMealPlanForId(request.getMealPlanId());
        mealPlan.setIsActive(request.isActive());
        return mealPlan;
    }

    @Override
    public void deleteMealPlan(UUID planId)
    {
        MealPlan entityMealPlan = getMealPlanForId(planId);

        // Kick off the deletion of the carb cycling hierarchy starting with the carb cycling plan for this meal plan
        List<CarbCyclingPlan> carbCyclingPlans = getCarbCyclingPlansForMealPlan(planId);
        carbCyclingPlans.forEach(carbCyclingPlan -> deleteCarbCyclingPlan(carbCyclingPlan.getPlanId()));

        // Kick of the deletion of the meal planning hierarchy starting with the meal planning day for this meal plan
        deleteDaysForMealPlan(planId);

        // Delete the macro suggestions for this meal plan
        entityManager.createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_MACRO_SUGGESTIONS_REMOVE_ALL_FOR_PLAN)
                .setParameter(QueryConstants.PARAMETER_MEAL_PLAN_ID, planId)
                .executeUpdate();

        // Finally, delete the meal plan record itself.
        entityManager.remove(entityMealPlan);
    }
    // endregion

    // region Meal Plan Days
    ///////////////////////////////////////////////////////////////////
    //
    //                          MEAL PLAN DAYS
    //
    ///////////////////////////////////////////////////////////////////

    @Override
    public MealPlanDay getDayForId(UUID dayId)
    {
        MealPlanDay mealPlanDay = entityManager.find(MealPlanDay.class, dayId);

        if (mealPlanDay == null)
        {
            throw new NoResultException(String.format("No meal plan day with id %s was found", dayId));
        }
        else
        {
            return mealPlanDay;
        }
    }

    @Override
    public List<MealPlanDay> getDaysForMealPlan(UUID mealPlanId)
    {
        return entityManager.createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_FIND_DAYS_FOR_MEAL_PLAN, MealPlanDay.class)
                            .setParameter(QueryConstants.PARAMETER_MEAL_PLAN_ID, mealPlanId)
                            .getResultList();
    }

    @Override
    public Optional<MealPlanDayNutritionalSummary> getDayNutritionalSummary(UUID dayId)
    {
        try
        {
            MealPlanDayNutritionalSummary summary = entityManager.createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_NUTRITIONAL_SUMMARY_FOR_DAY, MealPlanDayNutritionalSummary.class)
                    .setParameter(QueryConstants.PARAMETER_MEAL_PLAN_DAY_ID, dayId)
                    .getSingleResult();

            return Optional.of(summary);
        }
        catch(NoResultException nre)
        {
            return Optional.empty();
        }
    }

    @Override
    public List<MealPlanDayNutritionalSummary> getDayNutritionalSummariesForMealPlan(UUID mealPlanId)
    {
        LOG.debug("Searching for nutritional summaries for meal plan {}", mealPlanId);
        List<MealPlanDayNutritionalSummary> summaries = entityManager.createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_NUTRITIONAL_SUMMARIES_FOR_DAYS_FOR_MEAL_PLAN, MealPlanDayNutritionalSummary.class)
                .setParameter(QueryConstants.PARAMETER_MEAL_PLAN_ID, mealPlanId)
                .getResultList();
        LOG.debug("Found {} nutritional summaries for meal plan {}", summaries.size(), mealPlanId);
        return summaries;
    }

    @Override
    public MealPlanDay getCarbCyclingDayForId(UUID dayId)
    {
        return entityManager.createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_CARB_CYCLING_FIND_DAY_FOR_ID, MealPlanDay.class)
                            .setParameter(QueryConstants.PARAMETER_MEAL_PLAN_DAY_ID, dayId)
                            .getSingleResult();
    }

    @Override
    public List<MealPlanDay> getCarbCyclingDaysForPlan(UUID carbCyclingPlanId)
    {
        return entityManager.createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_FIND_CARB_CYCLING_DAYS_FOR_PLAN, MealPlanDay.class)
                            .setParameter(QueryConstants.PARAMETER_CARB_CYCLING_PLAN_ID, carbCyclingPlanId)
                            .getResultList();
    }

    @Override
    public Optional<CarbCyclingDayNutritionalSummary> getCarbCyclingDayNutritionalSummary(UUID dayId)
    {
        try
        {
            CarbCyclingDayNutritionalSummary summary = entityManager
                    .createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_NUTRITIONAL_SUMMARY_FOR_CARB_CYCLING_DAY, CarbCyclingDayNutritionalSummary.class)
                    .setParameter(QueryConstants.PARAMETER_MEAL_PLAN_DAY_ID, dayId)
                    .getSingleResult();

            return Optional.of(summary);
        }
        catch(NoResultException nre)
        {
            return Optional.empty();
        }
    }

    @Override
    public List<CarbCyclingDayNutritionalSummary> getCarbCyclingDayNutritionalSummariesForPlan(UUID carbCyclingPlanId)
    {
        return entityManager.createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_NUTRITIONAL_SUMMARIES_FOR_CARB_CYCLING_DAYS_FOR_PLAN, CarbCyclingDayNutritionalSummary.class)
                            .setParameter(QueryConstants.PARAMETER_CARB_CYCLING_PLAN_ID, carbCyclingPlanId)
                            .getResultList();
    }

    @Override
    public MealPlanDay createDayForMealPlan(CreateDayRequest dayCreationRequest)
    {
        return createDay(dayCreationRequest.getForMealId(), dayCreationRequest.getDay().getDayLabel(), dayCreationRequest.getDay().getFitnessGoal());
    }

    @Override
    public MealPlanDay createDayForMealPlan(CreateCarbCyclingDayRequest dayCreationRequest)
    {
        CarbCyclingDayViewModel carbCyclingDayViewModel = dayCreationRequest.getDay();

        MealPlanDay newDayEntity = createDay(dayCreationRequest.getForMealId(), dayCreationRequest.getDay().getDayLabel(), dayCreationRequest.getDay().getFitnessGoal());

        CarbCyclingPlan carbCyclingPlanEntity = entityManager.find(CarbCyclingPlan.class, dayCreationRequest.getForCarbCyclingPlanId());
        CarbCyclingDay newCarbCyclingDayEntity = new CarbCyclingDay(newDayEntity, carbCyclingPlanEntity, carbCyclingDayViewModel.getDayType());
        newCarbCyclingDayEntity.setIsTrainingDay(carbCyclingDayViewModel.isTrainingDay());
        newCarbCyclingDayEntity.setRequiredCalories(carbCyclingDayViewModel.getRequiredCalories());
        newCarbCyclingDayEntity.setRequiredProtein(carbCyclingDayViewModel.getRequiredProtein());
        newCarbCyclingDayEntity.setRequiredCarbs(carbCyclingDayViewModel.getRequiredCarbs());
        newCarbCyclingDayEntity.setRequiredFats(carbCyclingDayViewModel.getRequiredFats());
        newCarbCyclingDayEntity.setMealPlanningDay(newDayEntity);
        entityManager.persist(newCarbCyclingDayEntity);

        // Now that the new carb cycling day has been saved set the relationship in the other direction as well
        newDayEntity.setCarbCyclingDay(newCarbCyclingDayEntity);

        LOG.debug("Successfully created a carb cycling day for the meal plan {}", dayCreationRequest.getForMealId());
        return newDayEntity;
    }

    @Override
    public MealPlanDay updateDayForMealPlan(ModifyDayRequest modifyDayRequest)
    {
        MealPlanDay dayEntity = entityManager.find(MealPlanDay.class, modifyDayRequest.getDayId());
        dayEntity.setDayLabel(modifyDayRequest.getNewLabel());
        return dayEntity;
    }

    /**
     * Deletes the MealPlanningDay record for the specified day Id.
     *
     * @param dayId The id of the day to delete.
     */
    @Override
    public void deleteDay(UUID dayId)
    {
        LOG.debug("Deleting day {}", dayId);
        deleteMealsForDay(dayId);

        MealPlanDay entityDay = getDayForId(dayId);

        if (entityDay.getCarbCyclingDay() != null)
        {
            entityManager.remove(entityDay.getCarbCyclingDay());
        }

        entityManager.remove(entityDay);
        LOG.debug("Day {} has been deleted", dayId);
    }

    /**
     * Deletes all MealPlanningDay records for the specified meal.
     * @param planId The id of the meal to delete all the days for.
     */
    @Override
    public void deleteDaysForMealPlan(UUID planId)
    {
        // First delete all of the meals for all of the days for that meal plan
        List<MealPlanDay> daysForMealPlan = getDaysForMealPlan(planId);
        daysForMealPlan.forEach(day -> deleteMealsForDay(day.getDayId()));

        LOG.debug("Deleting days for meal plan {}", planId);
        // Then delete all of the days for the meal plan. First carb cycling days, then meal planning days.
        // For days that are not carb cycling days the first delete is simply a no-op
        entityManager.createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_REMOVE_CARB_CYCLING_DAYS_FOR_MEAL_PLAN)
                .setParameter(QueryConstants.PARAMETER_MEAL_PLAN_DAY_ID_LIST, daysForMealPlan.stream().map(MealPlanDay::getDayId).collect(Collectors.toList()))
                .executeUpdate();

        entityManager.createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_REMOVE_DAYS_FOR_MEAL_PLAN)
                .setParameter(1, planId)
                .executeUpdate();
        LOG.debug("Days for meal plan {} have been deleted", planId);
    }
    // endregion

    // region Meal Plan Meals
    ///////////////////////////////////////////////////////////////////
    //
    //                          MEAL PLAN MEALS
    //
    ///////////////////////////////////////////////////////////////////

    @Override
    public MealPlanMeal getMealForId(UUID mealId)
    {
        MealPlanMeal mealPlanMeal = entityManager.find(MealPlanMeal.class, mealId);

        if (mealPlanMeal == null)
        {
            throw new NoResultException(String.format("No meal plan meal with id %s was found", mealId));
        }
        else
        {
            return mealPlanMeal;
        }
    }

    @Override
    public List<MealPlanMeal> getMealsForDay(UUID dayId)
    {
        return entityManager.createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_FIND_MEALS_FOR_DAY, MealPlanMeal.class)
                            .setParameter(QueryConstants.PARAMETER_MEAL_PLAN_DAY_ID, dayId)
                            .getResultList();
    }

    @Override
    public Optional<MealPlanMealNutritionalSummary> getMealNutritionalSummary(UUID mealId)
    {
        try
        {
            MealPlanMealNutritionalSummary summary = entityManager
                    .createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_NUTRITIONAL_SUMMARY_FOR_MEAL, MealPlanMealNutritionalSummary.class)
                    .setParameter(QueryConstants.PARAMETER_MEAL_PLAN_MEAL_ID, mealId)
                    .getSingleResult();

            return Optional.of(summary);
        }
        catch (NoResultException nre)
        {
            return Optional.empty();
        }
    }

    @Override
    public List<MealPlanMealNutritionalSummary> getMealNutritionalSummariesForDay(UUID dayId)
    {
        return entityManager.createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_NUTRITIONAL_SUMMARIES_FOR_MEALS_FOR_DAY, MealPlanMealNutritionalSummary.class)
                            .setParameter(QueryConstants.PARAMETER_MEAL_PLAN_DAY_ID, dayId)
                            .getResultList();
    }

    @Override
    public MealPlanMeal getCarbCyclingMealById(UUID mealId)
    {
        return entityManager.createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_CARB_CYCLING_FIND_MEAL_FOR_ID, MealPlanMeal.class)
                            .setParameter(QueryConstants.PARAMETER_MEAL_PLAN_MEAL_ID, mealId)
                            .getSingleResult();
    }

    @Override
    public List<MealPlanMeal> getCarbCyclingMealsForDay(UUID dayId)
    {
        return entityManager.createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_CARB_CYCLING_FIND_MEALS_FOR_DAY, MealPlanMeal.class)
                            .setParameter(QueryConstants.PARAMETER_MEAL_PLAN_DAY_ID, dayId)
                            .getResultList();
    }

    @Override
    public Optional<CarbCyclingMealNutritionalSummary> getCarbCyclingMealNutritionalSummary(UUID mealId)
    {
        try
        {
            CarbCyclingMealNutritionalSummary summary = entityManager
                    .createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_NUTRITIONAL_SUMMARY_FOR_CARB_CYCLING_MEAL, CarbCyclingMealNutritionalSummary.class)
                    .setParameter(QueryConstants.PARAMETER_MEAL_PLAN_MEAL_ID, mealId)
                    .getSingleResult();

            return Optional.of(summary);
        }
        catch (NoResultException nre)
        {
            return Optional.empty();
        }
    }

    @Override
    public List<CarbCyclingMealNutritionalSummary> getCarbCyclingMealNutritionalSummariesForDay(UUID dayId)
    {
        return entityManager.createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_NUTRITIONAL_SUMMARIES_FOR_CARB_CYCLING_MEALS_FOR_DAY, CarbCyclingMealNutritionalSummary.class)
                            .setParameter(QueryConstants.PARAMETER_MEAL_PLAN_DAY_ID, dayId)
                            .getResultList();
    }

    @Override
    public MealPlanMeal getCarbCyclingMealForDay(UUID dayId, int mealNumber)
    {
        return entityManager.createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_FIND_CARB_CYCLING_MEAL_FOR_DAY_BY_MEAL_NUMBER, MealPlanMeal.class)
                .setParameter(QueryConstants.PARAMETER_MEAL_PLAN_DAY_ID, dayId)
                .setParameter(QueryConstants.PARAMETER_MEAL_PLAN_CARB_CYCLING_MEAL_NUMBER, mealNumber)
                .getSingleResult();
    }

    @Override
    public MealPlanMeal createMealForDay(CreateMealRequest mealCreationRequest) {
        return createMeal(mealCreationRequest.getForDayId(), mealCreationRequest.getMealName());
    }

    @Override
    public MealPlanMeal createMealForDay(CreateCarbCyclingMealRequest mealCreationRequest) {
        LOG.debug("Received a request to create a new carb-cycling meal for a day: {}", mealCreationRequest);
        // First create the MealPlanningMeal record
        MealPlanMeal newMealEntity = createMeal(mealCreationRequest.getForDayId(), mealCreationRequest.getMealName());

        // Next create the CarbCyclingMeal record
        CarbCyclingDay carbCyclingDayEntity = entityManager.find(CarbCyclingDay.class, mealCreationRequest.getForDayId());
        CarbCyclingMeal newCarbCyclingMeal = new CarbCyclingMeal(newMealEntity, carbCyclingDayEntity);
        newCarbCyclingMeal.setMealType(mealCreationRequest.getCarbCyclingMeal().getMealType());
        newCarbCyclingMeal.setMealNumber(mealCreationRequest.getCarbCyclingMeal().getMealNumber());
        newCarbCyclingMeal.setRequiredCalories(mealCreationRequest.getCarbCyclingMeal().getRequiredCalories());
        newCarbCyclingMeal.setRequiredProtein(mealCreationRequest.getCarbCyclingMeal().getRequiredProtein());
        newCarbCyclingMeal.setRequiredCarbs(mealCreationRequest.getCarbCyclingMeal().getRequiredCarbs());
        newCarbCyclingMeal.setRequiredFats(mealCreationRequest.getCarbCyclingMeal().getRequiredFats());
        newCarbCyclingMeal.setCarbConsumptionPercentage(mealCreationRequest.getCarbCyclingMeal().getCarbConsumptionPercentage());
        entityManager.persist(newCarbCyclingMeal);

        // Ensure the association is set before we return
        newMealEntity.setCarbCyclingMeal(newCarbCyclingMeal);

        return newMealEntity;
    }

    @Override
    public MealPlanMeal updateMealForDay(ModifyMealRequest modifyMealRequest) {
        MealPlanMeal meal = entityManager.find(MealPlanMeal.class, modifyMealRequest.getMealId());
        meal.setMealName(modifyMealRequest.getMealName());
        meal.setLastModifiedDate(LocalDateTime.now());
        return meal;
    }

    @Override
    public void deleteMeal(UUID mealId)
    {
        LOG.debug("Deleting meal {}", mealId);
        deleteIngredientsForMeal(mealId);

        MealPlanMeal mealToDelete = getMealForId(mealId);
        /*
         * It is worth noting here, because this is exceedingly subtle, but when you have an entity that
         * uses the @MapsId annotation like the CarbCyclingMeal entity here does you must remove it
         * _before_ you remove the entity it maps the id of.
         *
         * If you fail to do so and attempt to call remove it'll just do nothing. No exception will be
         * thrown and there will be no useful information to be found in the logs.
         */
        if (mealToDelete.getCarbCyclingMeal() != null)
        {
            entityManager.remove(mealToDelete.getCarbCyclingMeal());
        }

        entityManager.remove(mealToDelete);
        LOG.debug("Meal {} has been deleted", mealId);
    }

    @Override
    public void deleteMealsForDay(UUID dayId)
    {
        // First delete all of the ingredients for all of the meals for the day
        List<MealPlanMeal> mealsForDay = getMealsForDay(dayId);
        mealsForDay.forEach(meal -> deleteIngredientsForMeal(meal.getMealId()));

        LOG.debug("Deleting meals for day {}", dayId);
        // Then delete all of the meals for the day. First carb cycling meals, then meal planning meals.
        // For meals that are not carb cycling meals the first delete is simply a no-op
        entityManager.createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_REMOVE_CARB_CYCLING_MEALS_FOR_DAY)
                .setParameter(QueryConstants.PARAMETER_MEAL_PLAN_MEAL_ID_LIST, mealsForDay.stream().map(MealPlanMeal::getMealId).collect(Collectors.toList()))
                .executeUpdate();

        entityManager.createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_REMOVE_MEALS_FOR_DAY)
                .setParameter(QueryConstants.PARAMETER_MEAL_PLAN_DAY_ID, dayId)
                .executeUpdate();
        LOG.debug("Meals for day {} have been deleted", dayId);
    }

    @Override
    public List<MealPlanMealIngredient> getIngredientsForMeal(UUID mealId) {
        return entityManager.createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_FIND_INGREDIENTS_FOR_MEAL, MealPlanMealIngredient.class)
                            .setParameter(QueryConstants.PARAMETER_MEAL_PLAN_MEAL_ID, mealId)
                            .getResultList();
    }

    /**
     * Create or update a list of ingredients for a given meal.
     *
     * If the ingredients do not already exist they will be created.
     *
     * Otherwise they will be updated according to the details of the request.
     * @param modifyMealIngredientsRequest Request object that encapsulates needed details
     * @return The list of ingredients that were modified
     */
    @Override
    public List<MealPlanMealIngredient> updateIngredientsForMeal(ModifyMealIngredientsRequest modifyMealIngredientsRequest) {
        List<MealPlanMealIngredient> modifiedIngredients = new ArrayList<>();

        List<MealPlanMealIngredient> ingredientsForMeal = getIngredientsForMeal(modifyMealIngredientsRequest.getMealId());
        MealPlanMeal meal = entityManager.find(MealPlanMeal.class, modifyMealIngredientsRequest.getMealId());
        meal.setLastModifiedDate(LocalDateTime.now());

        /*
         * Remove any ingredients that are no longer in the ingredients list
         *
         * Do note that the order of the comparison here is important. New ingredients in the
         * ingredients list of the request will not yet have IngredientIds. However, the
         * ingredients that are loaded from the database are guaranteed to have IngredientIds.
         *
         * As such, when searching for ingredients to remove, the IngredientId comparison must
         * always start from the existing ingredient. Otherwise, you may try to call .equals()
         * on a UUID that is actually null.
         */
        List<MealPlanMealIngredient> ingredientsToRemove = ingredientsForMeal
                .stream()
                .filter(existingIngredient ->
                        modifyMealIngredientsRequest
                                .getIngredients()
                                .stream()
                                .noneMatch(modifiedIngredient -> existingIngredient.getIngredientId().equals(modifiedIngredient.getId()))
                )
                .collect(Collectors.toList());
        ingredientsToRemove.forEach(entityManager::remove);

        // Add or update ingredients
        for(IngredientModel ingredient : modifyMealIngredientsRequest.getIngredients())
        {
            MealPlanMealIngredient ingredientEntity;
            if (ingredient.getId() == null)
            {
                FoodMetaData foodForIngredient = entityManager.find(FoodMetaData.class, ingredient.getIngredientFood().getId());
                ingredientEntity = new MealPlanMealIngredient(foodForIngredient, meal);
            }
            else
            {
                ingredientEntity = entityManager.find(MealPlanMealIngredient.class, ingredient.getId());
            }

            ingredientEntity.setServingsIncluded(ingredient.getServingsIncluded());
            ingredientEntity.setIsOptional(ingredient.isOptional());
            ingredientEntity.setIsAlternate(ingredient.isAlternate());
            entityManager.persist(ingredientEntity);
            modifiedIngredients.add(ingredientEntity);
        }

        return modifiedIngredients;
    }

    @Override
    public void deleteIngredientsForMeal(UUID mealId) {
        LOG.debug("Deleting ingredients for meal {}", mealId);
        entityManager.createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_REMOVE_INGREDIENTS_FOR_MEAL)
                .setParameter(QueryConstants.PARAMETER_MEAL_PLAN_MEAL_ID, mealId)
                .executeUpdate();
        LOG.debug("Ingredients for meal {} have been deleted", mealId);
    }
    // endregion

    // region Carb Cycling Plans
    ///////////////////////////////////////////////////////////////////
    //
    //                      CARB CYCLING PLANS
    //
    ///////////////////////////////////////////////////////////////////

    @Override
    public CarbCyclingPlan getCarbCyclingPlanForId(UUID planId) {
        CarbCyclingPlan carbCyclingPlan = entityManager.find(CarbCyclingPlan.class, planId);

        if (carbCyclingPlan == null)
        {
            throw new NoResultException(String.format("No carb cycling plan with id %s was found", planId));
        }
        else
        {
            return carbCyclingPlan;
        }
    }

    @Override
    public List<CarbCyclingPlan> getCarbCyclingPlansForMealPlan(UUID mealPlanId)
    {
        return entityManager
                .createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_FIND_CARB_CYCLING_PLANS_FOR_MEAL_PLAN, CarbCyclingPlan.class)
                .setParameter(QueryConstants.PARAMETER_MEAL_PLAN_ID, mealPlanId)
                .getResultList();
    }

    @Override
    public List<CarbCyclingMealConfiguration> getMealConfigurationsForCarbCyclingPlan(UUID carbCyclingPlanId)
    {
        return entityManager
                .createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_FIND_MEAL_CONFIGURATIONS_FOR_CARB_CYCLING_PLAN, CarbCyclingMealConfiguration.class)
                .setParameter(QueryConstants.PARAMETER_CARB_CYCLING_PLAN_ID, carbCyclingPlanId)
                .getResultList();
    }

    @Override
    public CarbCyclingPlan createCarbCyclingPlan(CreateCarbCyclingPlanRequest creationRequest) {
        LOG.debug("Create new carb cycling plan for user {}...", creationRequest.getForUserId());

        User userEntity = entityManager.find(User.class, creationRequest.getForUserId());
        MealPlan mealPlanEntity = entityManager.find(MealPlan.class, creationRequest.getForMealPlanId());
        CarbCyclingPlanViewModel planDetails = creationRequest.getPlanDetails();

        String dayRatioString = String.format("%s:%s", planDetails.getHighCarbDays(), planDetails.getLowCarbDays());
        CarbCyclingPlan newPlan = new CarbCyclingPlan(
                mealPlanEntity,
                userEntity,
                dayRatioString,
                planDetails.getHighCarbDaySurplusPercentage(),
                planDetails.getLowCarbDayDeficitPercentage(),
                planDetails.isShouldTimeFats(),
                planDetails.getMealsPerDay());
        entityManager.persist(newPlan);

        // If the user has defined meal configurations for use with the plan save them too
        if (planDetails.hasConfiguredMeals())
        {
            for(CarbCyclingMealConfigurationModel mealConfig : planDetails.getMealConfigurations())
            {
                CarbCyclingMealConfiguration mealConfigurationEntity = new CarbCyclingMealConfiguration(
                        newPlan,
                        mealConfig.getCarbConsumptionMaximumPercent(),
                        mealConfig.getCarbConsumptionMinimumPercent(),
                        mealConfig.getPostWorkoutMealOffset(),
                        mealConfig.getPostWorkoutMealRelation(),
                        mealConfig.isForTrainingDay()
                );
                entityManager.persist(mealConfigurationEntity);
            }
        }

        return newPlan;
    }

    /**
     * Toggles the active status of a carb cycling plan
     * @param planId The Id of the plan to toggle the status for
     * @return The CarbCyclingPlan that had its status toggled
     */
    @Override
    public CarbCyclingPlan toggleCarbCyclingPlanStatus(UUID planId) {
        CarbCyclingPlan planToToggle = getCarbCyclingPlanForId(planId);
        planToToggle.setActive(!planToToggle.getActive());
        return planToToggle;
    }

    @Override
    public void deleteCarbCyclingPlan(UUID planId) {
        CarbCyclingPlan planToRemove = getCarbCyclingPlanForId(planId);

        // First delete all the days for the plan
        List<MealPlanDay> daysForPlan = getCarbCyclingDaysForPlan(planId);
        daysForPlan.forEach(day -> deleteDay(day.getDayId()));

        // Second delete all the meal configs for the plan (if any)
        List<CarbCyclingMealConfiguration> mealConfigurations = getMealConfigurationsForCarbCyclingPlan(planId);
        mealConfigurations.forEach(config -> entityManager.remove(config));

        entityManager.remove(planToRemove);
    }

    /**
     * Creates one or more MealPlanCalendarDay entities as specified by the request and persists them.
     * @param request Request that specifies the creation details
     * @return The list of created MealPlanCalendarDay instances
     */
    @Override
    public List<MealPlanCalendarDay> createCalendarDays(AssociateMealPlanDayToCalendarDayRequest request)
    {
        List<MealPlanCalendarDay> createdCalendarDays = new ArrayList<>();
        User forUser = entityManager.find(User.class, request.getUser().getUserId());

        for(DayOfWeek dayOfWeek : request.getForDaysOfWeek())
        {
            MealPlanDay plannedDayEntity;
            plannedDayEntity = getDayForId(request.getSelectedDay().getId());

            LocalDate newCalendarDayDate = getDateOffsetFromSunday(request.getWeekStartDate(), dayOfWeek);
            createdCalendarDays.add(new MealPlanCalendarDay(plannedDayEntity, forUser, newCalendarDayDate, request.getWeeklyInterval(), request.getWeeklyIntervalMonths(), false));
            LOG.debug("Creating calendar entry for {}", newCalendarDayDate);

            if (request.getWeeklyInterval() != WeeklyInterval.NONE)
            {
                LocalDate recurrenceEndDate = newCalendarDayDate.plusMonths(request.getWeeklyIntervalMonths());
                LocalDate recurrenceDate = getNextDateForWeeklyInterval(request.getWeeklyInterval(), newCalendarDayDate);

                while(recurrenceEndDate.isAfter(recurrenceDate))
                {
                    LOG.debug("Creating recurrence entry for {}", recurrenceDate);
                    createdCalendarDays.add(new MealPlanCalendarDay(plannedDayEntity, forUser, recurrenceDate, request.getWeeklyInterval(), request.getWeeklyIntervalMonths(), true));
                    recurrenceDate = getNextDateForWeeklyInterval(request.getWeeklyInterval(), recurrenceDate);
                }
            }
        }

        LOG.debug("Saving {} calendar days", createdCalendarDays.size());
        createdCalendarDays.forEach(newEntity -> entityManager.persist(newEntity));
        return createdCalendarDays;
    }
    // endregion

    // region Meal Plan Calendar Days
    ///////////////////////////////////////////////////////////////////
    //
    //                      MEAL PLAN CALENDAR DAYS
    //
    ///////////////////////////////////////////////////////////////////

    @Override
    public void deleteMealPlanCalendarDay(UUID calendarDayId)
    {
        MealPlanCalendarDay calendarDayToDelete = entityManager.find(MealPlanCalendarDay.class, calendarDayId);

        if (calendarDayToDelete == null)
        {
            throw new NoResultException("No calendar day with the id of {} was found");
        }
        else
        {
            entityManager.remove(calendarDayToDelete);
        }
    }

    @Override
    public List<MealPlanCalendarDay> getMealPlanCalendarDaysForUser(UUID userId)
    {
        return entityManager
                .createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_FIND_ALL_CALENDAR_DAYS_FOR_USER, MealPlanCalendarDay.class)
                .setParameter(QueryConstants.PARAMETER_USER_ID, userId)
                .getResultList();
    }

    @Override
    public List<MealPlanCalendarDay> getMealPlanCalendarDaysInRange(LocalDate startDate, LocalDate endDate)
    {
        return entityManager
                .createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_FIND_CALENDAR_DAYS_IN_RANGE, MealPlanCalendarDay.class)
                .setParameter(QueryConstants.PARAMETER_CALENDAR_START_DATE, startDate)
                .setParameter(QueryConstants.PARAMETER_CALENDAR_END_DATE, endDate)
                .getResultList();
    }

    @Override
    public JsonMealPlanningData exportData()
    {
        JsonMealPlanningData jsonMealPlanningData = new JsonMealPlanningData(appConfigUtils.getProperty("schema.version", Integer.class));
        populateJsonDataForMealPlans(jsonMealPlanningData);
        populateJsonDataForCarbCyclingPlans(jsonMealPlanningData);

        return jsonMealPlanningData;
    }

    @Override
    public void importData(JsonMealPlanningData jsonMealPlanningData, FoodImportResult importedFoods)
    {
        User importUser = entityManager.find(User.class, UserAuthentication.instance().getAuthenticatedUser().userId());
        LOG.debug("Beginning import of meal planning data for user {}", importUser);

        if (importUser == null)
        {
            // As with all application state exceptions, this should never happen. But we'll be safe and check anyway.
            throw new InvalidApplicationStateException("The User profile importing this data (%s) could not be found in the database!");
        }

        for(JsonMealPlan jsonMealPlan : jsonMealPlanningData.getMealPlans())
        {
            LOG.debug("Importing meal plan with json id {}", jsonMealPlan.mealPlanId());
            MealPlan mealPlanEntity = MealPlanMapper.toFreshEntity(jsonMealPlan, importUser);
            mealPlanEntity.setIsActive(false);
            mealPlanEntity = entityManager.merge(mealPlanEntity);

            MealPlanImportContext mealPlanImportContext = new MealPlanImportContext(importUser, mealPlanEntity);
            importMealPlanningDataForMealPlan(importedFoods, jsonMealPlanningData, jsonMealPlan.mealPlanId(), mealPlanImportContext);
            importCarbCyclingDataForMealPlan(importedFoods, jsonMealPlanningData, jsonMealPlan.mealPlanId(), mealPlanImportContext);
            LOG.debug("Finished importing meal plan {}. Newly generated plan ID is {}", jsonMealPlan.mealPlanId(), mealPlanEntity.getMealPlanId());
        }
        LOG.debug("Finished import of meal planning data for user {}", importUser);
    }
    // endregion

    // region Private Impl Details
    ///////////////////////////////////////////////////////////////////
    //
    //                PRIVATE IMPLEMENTATION DETAILS
    //
    ///////////////////////////////////////////////////////////////////

    /**
     * Initialize and save a new meal plan using a calculated registration
     * @param forUser The user entity who the meal plan is for
     * @param calculatedRegistration The registration details
     * @return The new meal plan entity
     */
    private MealPlan createCalculatedMealPlan(User forUser, MealPlanningCalculatedRegistration calculatedRegistration)
    {
        // Calculate the needs for and create the new meal plan
        CalculationResult newPlanDetails = MealPlanningCalculator.instance().calculatePlanRequirements(calculatedRegistration);
        MealPlan newPlan = new MealPlan(
                forUser,
                calculatedRegistration.getHeight(),
                calculatedRegistration.getWeight(),
                calculatedRegistration.getAge(),
                calculatedRegistration.getSex(),
                calculatedRegistration.getActivityLevel(),
                calculatedRegistration.getFitnessGoal(),
                calculatedRegistration.getBmrFormula(),
                newPlanDetails.getNeededCalories(),
                newPlanDetails.getTdeeCalories());
        newPlan.setIsActive(true);
        entityManager.persist(newPlan);

        // Next, create the macronutrient suggestion records from the calculations
        MealPlanNutrientSuggestion proteinSuggestion = new MealPlanNutrientSuggestion(newPlan, Nutrient.PROTEIN, newPlanDetails.getMacronutrientSuggestion(Nutrient.PROTEIN), UnitOfMeasure.GRAM);
        MealPlanNutrientSuggestion carbsSuggestion = new MealPlanNutrientSuggestion(newPlan, Nutrient.CARBOHYDRATES, newPlanDetails.getMacronutrientSuggestion(Nutrient.CARBOHYDRATES), UnitOfMeasure.GRAM);
        MealPlanNutrientSuggestion fatsSuggestion = new MealPlanNutrientSuggestion(newPlan, Nutrient.TOTAL_FATS, newPlanDetails.getMacronutrientSuggestion(Nutrient.TOTAL_FATS), UnitOfMeasure.GRAM);
        entityManager.persist(proteinSuggestion);
        entityManager.persist(carbsSuggestion);
        entityManager.persist(fatsSuggestion);

        newPlan.getNutrientSuggestions().add(proteinSuggestion);
        newPlan.getNutrientSuggestions().add(carbsSuggestion);
        newPlan.getNutrientSuggestions().add(fatsSuggestion);

        return newPlan;
    }

    /**
     * Initialize and save a new meal plan using a manual registration
     * @param forUser The user entity who the meal plan is for
     * @param manualRegistration The registration details
     * @return The new meal plan entity
     */
    private MealPlan createManualMealPlan(User forUser, MealPlanningManualRegistration manualRegistration)
    {
        MealPlan newPlan = new MealPlan(
                forUser,
                BigDecimal.valueOf(-1),
                BigDecimal.valueOf(-1),
                -1,
                Sex.UNUSED,
                ActivityLevel.UNUSED,
                FitnessGoal.UNUSED,
                BasalMetabolicRateFormula.UNUSED,
                manualRegistration.getDailyCalories(),
                manualRegistration.getTdeeCalories());
        newPlan.setIsActive(true);
        entityManager.persist(newPlan);

        MealPlanNutrientSuggestion proteinSuggestion = new MealPlanNutrientSuggestion(newPlan, Nutrient.PROTEIN, manualRegistration.getProteinAmount(), UnitOfMeasure.GRAM);
        MealPlanNutrientSuggestion carbsSuggestion = new MealPlanNutrientSuggestion(newPlan, Nutrient.CARBOHYDRATES, manualRegistration.getCarbsAmount(), UnitOfMeasure.GRAM);
        MealPlanNutrientSuggestion fatsSuggestion = new MealPlanNutrientSuggestion(newPlan, Nutrient.TOTAL_FATS, manualRegistration.getTotalFatsAmount(), UnitOfMeasure.GRAM);
        entityManager.persist(proteinSuggestion);
        entityManager.persist(carbsSuggestion);
        entityManager.persist(fatsSuggestion);

        newPlan.getNutrientSuggestions().add(proteinSuggestion);
        newPlan.getNutrientSuggestions().add(carbsSuggestion);
        newPlan.getNutrientSuggestions().add(fatsSuggestion);

        return newPlan;
    }

    private MealPlanDay createDay(UUID forMealPlanId, String dayLabel, FitnessGoal fitnessGoal)
    {
        MealPlan forPlan = entityManager.find(MealPlan.class, forMealPlanId);
        MealPlanDay newDayEntity = new MealPlanDay(dayLabel, fitnessGoal);
        newDayEntity.setMealPlan(forPlan);
        entityManager.persist(newDayEntity);
        return newDayEntity;
    }

    private MealPlanMeal createMeal(UUID forDayId, String mealName)
    {
        MealPlanDay owningDay = entityManager.find(MealPlanDay.class, forDayId);
        MealPlanMeal newMeal = new MealPlanMeal(owningDay, mealName);
        entityManager.persist(newMeal);
        return newMeal;
    }

    /**
     * Returns the date of the day in relation to the preceding Sunday.
     * @param sundayDate The sunday from which to calculate the date for the day of the week
     * @param dayOfWeek The day of the week you wish to calculate the date for
     * @return LocalDate that is the offset from the provided Sunday date
     */
    private LocalDate getDateOffsetFromSunday(LocalDate sundayDate, DayOfWeek dayOfWeek)
    {
        if (sundayDate.getDayOfWeek() != DayOfWeek.SUNDAY)
        {
            throw new IllegalArgumentException("The provided date must fall on a Sunday!");
        }

        LocalDate newDate = sundayDate;

        switch(dayOfWeek)
        {
            case MONDAY:
                newDate = newDate.plusDays(1);
                break;
            case TUESDAY:
                newDate = newDate.plusDays(2);
                break;
            case WEDNESDAY:
                newDate = newDate.plusDays(3);
                break;
            case THURSDAY:
                newDate = newDate.plusDays(4);
                break;
            case FRIDAY:
                newDate = newDate.plusDays(5);
                break;
            case SATURDAY:
                newDate = newDate.plusDays(6);
                break;
        }

        return newDate;
    }

    private LocalDate getNextDateForWeeklyInterval(WeeklyInterval interval, LocalDate forDate)
    {
        LOG.debug("getNextDateForWeeklyInterval received WeeklyInterval {} and Date {}", interval, forDate);
        LocalDate newDate = null;
        switch(interval)
        {
            case WEEKLY -> newDate = forDate.plusWeeks(1);
            case BI_WEEKLY -> newDate = forDate.plusWeeks(2);
            default -> throw new IllegalArgumentException("Unsupported weekly interval");
        }
        LOG.debug("getNextDateForWeeklyInterval returned {}", newDate);
        return newDate;
    }

    private void populateJsonDataForMealPlans(JsonMealPlanningData jsonMealPlanningData)
    {
        UUID userId = UserAuthentication.instance().getAuthenticatedUser().userId();

        // Plans
        List<MealPlan> mealPlans = entityManager
                .createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_FIND_PLANS_FOR_USER, MealPlan.class)
                .setParameter(QueryConstants.PARAMETER_USER_ID, userId)
                .getResultList();

        List<JsonMealPlan> jsonMealPlan = mealPlans.stream().map(MealPlanMapper::toJson).toList();
        jsonMealPlanningData.setMealPlans(jsonMealPlan);

        // Calendar days
        List<MealPlanCalendarDay> mealPlanCalendarDays = entityManager
                .createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_FIND_PLANNED_CALENDAR_DAYS_FOR_USER, MealPlanCalendarDay.class)
                .setParameter(QueryConstants.PARAMETER_USER_ID, userId)
                .getResultList();

        List<JsonMealPlanCalendarDay> jsonMealPlanCalendarDays = mealPlanCalendarDays.stream().map(MealPlanCalendarDayMapper::toJson).toList();
        jsonMealPlanningData.setMealPlanCalendarDays(jsonMealPlanCalendarDays);

        // Macro suggestions
        List<MealPlanNutrientSuggestion> macroSuggestions = entityManager
                .createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_MACRO_SUGGESTIONS_FIND_FOR_USER, MealPlanNutrientSuggestion.class)
                .setParameter(QueryConstants.PARAMETER_USER_ID, userId)
                .getResultList();

        List<JsonMealPlanNutrientSuggestion> jsonMacroSuggestions = macroSuggestions.stream().map(MealPlanNutrientSuggestionMapper::toJson).toList();
        jsonMealPlanningData.setNutrientSuggestions(jsonMacroSuggestions);

        // Plan days
        List<MealPlanDay> mealPlanDays = entityManager
                .createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_FIND_ALL_DAYS_FOR_USER, MealPlanDay.class)
                .setParameter(QueryConstants.PARAMETER_USER_ID, userId)
                .getResultList();

        List<JsonMealPlanDay> jsonMealPlanDays = mealPlanDays.stream().map(MealPlanDayMapper::toJson).toList();
        jsonMealPlanningData.setMealPlanDays(jsonMealPlanDays);

        // Plan meals
        List<MealPlanMeal> mealPlanMeals = entityManager
                .createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_FIND_ALL_MEALS_FOR_USER, MealPlanMeal.class)
                .setParameter(QueryConstants.PARAMETER_USER_ID, userId)
                .getResultList();

        List<JsonMealPlanMeal> jsonMealPlanMeals = mealPlanMeals.stream().map(MealPlanMealMapper::toJson).toList();
        jsonMealPlanningData.setMealPlanMeals(jsonMealPlanMeals);

        // Meal ingredients
        List<MealPlanMealIngredient> mealPlanMealIngredients = entityManager
                .createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_FIND_MEAL_INGREDIENTS_FOR_USER, MealPlanMealIngredient.class)
                .setParameter(QueryConstants.PARAMETER_USER_ID, userId)
                .getResultList();

        List<JsonMealPlanMealIngredient> jsonMealPlanMealIngredients = mealPlanMealIngredients.stream().map(IngredientMapper::toJson).toList();
        jsonMealPlanningData.setMealPlanMealIngredients(jsonMealPlanMealIngredients);
    }

    private void populateJsonDataForCarbCyclingPlans(JsonMealPlanningData jsonMealPlanningData)
    {
        UUID userId = UserAuthentication.instance().getAuthenticatedUser().userId();

        // Plans
        List<CarbCyclingPlan> carbCyclingPlans = entityManager
                .createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_FIND_CARB_CYCLING_PLANS_FOR_USER, CarbCyclingPlan.class)
                .setParameter(QueryConstants.PARAMETER_USER_ID, userId)
                .getResultList();

        List<JsonCarbCyclingPlan> jsonCarbCyclingPlans = carbCyclingPlans.stream().map(CarbCyclingPlanMapper::toJson).toList();
        jsonMealPlanningData.setCarbCyclingPlans(jsonCarbCyclingPlans);

        // Days
        List<CarbCyclingDay> carbCyclingDays = entityManager
                .createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_CARB_CYCLING_FIND_DAYS_FOR_USER, CarbCyclingDay.class)
                .setParameter(QueryConstants.PARAMETER_USER_ID, userId)
                .getResultList();

        List<JsonCarbCyclingDay> jsonCarbCyclingDays = carbCyclingDays.stream().map(CarbCyclingDayMapper::toJson).toList();
        jsonMealPlanningData.setCarbCyclingDays(jsonCarbCyclingDays);

        // Meals
        List<CarbCyclingMeal> carbCyclingMeals = entityManager
                .createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_CARB_CYCLING_FIND_MEALS_FOR_USER, CarbCyclingMeal.class)
                .setParameter(QueryConstants.PARAMETER_USER_ID, userId)
                .getResultList();

        List<JsonCarbCyclingMeal> jsonCarbCyclingMeals = carbCyclingMeals.stream().map(CarbCyclingMealMapper::toJson).toList();
        jsonMealPlanningData.setCarbCyclingMeals(jsonCarbCyclingMeals);

        // Ingredients
        List<MealPlanMealIngredient> carbCyclingMealIngredients = entityManager
                .createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_FIND_CARB_CYCLING_MEAL_INGREDIENTS_FOR_USER, MealPlanMealIngredient.class)
                .setParameter(QueryConstants.PARAMETER_USER_ID, userId)
                .getResultList();
        List<JsonMealPlanMealIngredient> jsonCarbCyclingMealIngredients = carbCyclingMealIngredients.stream().map(IngredientMapper::toJson).toList();
        jsonMealPlanningData.setCarbCyclingPlanMealIngredients(jsonCarbCyclingMealIngredients);

        // Meal configs
        List<CarbCyclingMealConfiguration> mealConfigurations = entityManager
                .createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_FIND_MEAL_CONFIGURATIONS_FOR_USER, CarbCyclingMealConfiguration.class)
                .setParameter(QueryConstants.PARAMETER_USER_ID, userId)
                .getResultList();

        List<JsonCarbCyclingMealConfiguration> jsonCarbCyclingMealConfigurations = mealConfigurations.stream().map(CarbCyclingMealConfigurationMapper::toJson).toList();
        jsonMealPlanningData.setCarbCyclingMealConfigurations(jsonCarbCyclingMealConfigurations);

        /*
         * As a note:
         * For the purposes of data import and export if a planned calendar day is for a meal plan
         * day or a carb cycling plan day is largely a distinction without a difference. From the
         * db's point of view both kinds of planned days reside in the same table.
         */
        List<MealPlanCalendarDay> carbCyclingCalendarDays = entityManager
                .createNamedQuery(QueryConstants.QUERY_MEAL_PLANNING_FIND_PLANNED_CARB_CYCLING_CALENDAR_DAYS_FOR_USER, MealPlanCalendarDay.class)
                .setParameter(QueryConstants.PARAMETER_USER_ID, userId)
                .getResultList();

        List<JsonMealPlanCalendarDay> jsonCarbCyclingCalendarDays = carbCyclingCalendarDays.stream().map(MealPlanCalendarDayMapper::toJson).toList();
        jsonMealPlanningData.setCarbCyclingPlanCalendarDays(jsonCarbCyclingCalendarDays);
    }

    private void importMealPlanningDataForMealPlan(FoodImportResult importedFoods, JsonMealPlanningData jsonMealPlanningData, UUID jsonMealPlanId, MealPlanImportContext importContext)
    {
        // Find and import macro suggestions for plan
        List<JsonMealPlanNutrientSuggestion> jsonMealPlanNutrientSuggestions = jsonMealPlanningData
                .getNutrientSuggestions()
                .stream()
                .filter(suggestion -> suggestion.mealPlanId().equals(jsonMealPlanId))
                .toList();

        for(JsonMealPlanNutrientSuggestion jsonMealPlanNutrientSuggestion : jsonMealPlanNutrientSuggestions)
        {
            LOG.debug("\tImporting meal plan macro suggestion from source {}", jsonMealPlanNutrientSuggestion);
            MealPlanNutrientSuggestion macroSuggestionEntity = MealPlanNutrientSuggestionMapper.toFreshEntity(jsonMealPlanNutrientSuggestion, importContext.importedMealPlan);
            entityManager.merge(macroSuggestionEntity);
            LOG.debug("\tImported meal plan macro suggestion and generated ID {}", macroSuggestionEntity.getMacroId());
        }

        // Find and import days for plan
        List<JsonMealPlanDay> jsonMealPlanDays = jsonMealPlanningData
                .getMealPlanDays()
                .stream()
                .filter(day -> day.mealPlanId().equals(jsonMealPlanId))
                .toList();

        for(JsonMealPlanDay jsonMealPlanDay : jsonMealPlanDays)
        {
            LOG.debug("\tImporting meal plan day from source {}", jsonMealPlanDay);
            MealPlanDay mealPlanDayEntity = MealPlanDayMapper.toFreshEntity(jsonMealPlanDay, importContext.importedMealPlan);
            mealPlanDayEntity = entityManager.merge(mealPlanDayEntity);
            LOG.debug("\tImported meal plan day and generated ID {}", mealPlanDayEntity.getDayId());

            if (jsonMealPlanDay.associatedCarbCyclingDayId() != null)
            {
                LOG.debug("\tAssociated carb cycling day json ID {} to newly created meal plan day ID {}", jsonMealPlanDay.associatedCarbCyclingDayId(), mealPlanDayEntity.getDayId());
                importContext.carbCyclingDayToMealPlanDayAssociations.put(jsonMealPlanDay.associatedCarbCyclingDayId(), mealPlanDayEntity);
            }

            // Find and import calendar days for day
            List<JsonMealPlanCalendarDay> jsonMealPlanCalendarDays = jsonMealPlanningData
                    .getMealPlanCalendarDays()
                    .stream()
                    .filter(calendarDay -> calendarDay.plannedDayId().equals(jsonMealPlanDay.dayId()))
                    .toList();

            for(JsonMealPlanCalendarDay jsonMealPlanCalendarDay : jsonMealPlanCalendarDays)
            {
                LOG.debug("\tImporting meal plan calendar day from source {}", jsonMealPlanCalendarDay);
                MealPlanCalendarDay mealPlanCalendarDayEntity = MealPlanCalendarDayMapper.toFreshEntity(jsonMealPlanCalendarDay, mealPlanDayEntity, importContext.importUser);
                mealPlanCalendarDayEntity = entityManager.merge(mealPlanCalendarDayEntity);
                LOG.debug("\tImported meal plan calendar day and generated ID {}", mealPlanCalendarDayEntity.getCalendarDayId());
            }

            // Find and import meals for day
            List<JsonMealPlanMeal> jsonMealPlanMeals = jsonMealPlanningData
                    .getMealPlanMeals()
                    .stream()
                    .filter(meal -> meal.forDayId().equals(jsonMealPlanDay.dayId()))
                    .toList();

            for (JsonMealPlanMeal jsonMealPlanMeal : jsonMealPlanMeals)
            {
                LOG.debug("\tImporting meal plan meal from source {}", jsonMealPlanMeal);
                MealPlanMeal mealPlanMealEntity = MealPlanMealMapper.toFreshEntity(jsonMealPlanMeal, mealPlanDayEntity);
                mealPlanMealEntity = entityManager.merge(mealPlanMealEntity);
                LOG.debug("\tImported meal plan meal and generated ID {}", mealPlanMealEntity.getMealId());

                if (jsonMealPlanMeal.associatedCarbCyclingMealId() != null)
                {
                    LOG.debug("\tAssociated carb cycling meal json ID {} to newly created meal plan meal ID {}", jsonMealPlanMeal.associatedCarbCyclingMealId(), mealPlanMealEntity.getMealId());
                    importContext.carbCyclingMealToMealPlanMealAssociations.put(jsonMealPlanMeal.associatedCarbCyclingMealId(), mealPlanMealEntity);
                }

                // Find and import ingredients for meal
                List<JsonMealPlanMealIngredient> jsonMealPlanMealIngredients = jsonMealPlanningData
                        .getMealPlanMealIngredients()
                        .stream()
                        .filter(ingredient -> ingredient.mealId().equals(jsonMealPlanMeal.mealId()))
                        .toList();
                writeJsonMealIngredientsForMeal(importedFoods, jsonMealPlanMealIngredients, mealPlanMealEntity);
            }
        }
    }

    private void importCarbCyclingDataForMealPlan(FoodImportResult importedFoods, JsonMealPlanningData jsonMealPlanningData, UUID jsonMealPlanId, MealPlanImportContext importContext)
    {
        // Find and import carb cycling plans for meal plan
        List<JsonCarbCyclingPlan> jsonCarbCyclingPlans = jsonMealPlanningData
                .getCarbCyclingPlans()
                .stream()
                .filter(carbCyclingPlan -> carbCyclingPlan.mealPlanId().equals(jsonMealPlanId))
                .toList();

        for(JsonCarbCyclingPlan jsonCarbCyclingPlan : jsonCarbCyclingPlans)
        {
            LOG.debug("Importing carb cycling plan with json id {}", jsonCarbCyclingPlan.planId());
            CarbCyclingPlan carbCyclingPlanEntity = CarbCyclingPlanMapper.toFreshEntity(jsonCarbCyclingPlan, importContext.importedMealPlan, importContext.importUser);
            carbCyclingPlanEntity.setActive(false);
            carbCyclingPlanEntity = entityManager.merge(carbCyclingPlanEntity);

            // Find and import meal configurations for carb cycling plan
            List<JsonCarbCyclingMealConfiguration> jsonCarbCyclingMealConfigurations = jsonMealPlanningData
                    .getCarbCyclingMealConfigurations()
                    .stream()
                    .filter(mealConfig -> mealConfig.carbCyclingPlanId().equals(jsonCarbCyclingPlan.planId()))
                    .toList();

            for(JsonCarbCyclingMealConfiguration jsonCarbCyclingMealConfiguration : jsonCarbCyclingMealConfigurations)
            {
                CarbCyclingMealConfiguration carbCyclingMealConfigurationEntity = CarbCyclingMealConfigurationMapper.toFreshEntity(jsonCarbCyclingMealConfiguration, carbCyclingPlanEntity);
                carbCyclingMealConfigurationEntity = entityManager.merge(carbCyclingMealConfigurationEntity);
                LOG.debug("\tImported carb cycling meal configuration and generated ID {}\n\t\tSource: {}", carbCyclingMealConfigurationEntity.getMealConfigurationId(), jsonCarbCyclingMealConfiguration);
            }

            // Find and import days for carb cycling plan
            List<JsonCarbCyclingDay> jsonCarbCyclingDays = jsonMealPlanningData
                    .getCarbCyclingDays()
                    .stream()
                    .filter(carbCyclingDay -> carbCyclingDay.carbCyclingPlanId().equals(jsonCarbCyclingPlan.planId()))
                    .toList();

            for(JsonCarbCyclingDay jsonCarbCyclingDay : jsonCarbCyclingDays)
            {
                MealPlanDay associatedMealPlanDay = importContext.carbCyclingDayToMealPlanDayAssociations.get(jsonCarbCyclingDay.carbCyclingDayId());
                LOG.debug("\tLooking up associated meal plan day from import context for carb cycling day with json ID {}", jsonCarbCyclingDay.carbCyclingDayId());

                if (associatedMealPlanDay == null)
                {
                    throw new DataImportException(
                            String.format(
                                    "Could not find associated meal plan day with id %s for carb cycling day with id %s for carb cycling plan %s",
                                    jsonCarbCyclingDay.mealPlanDayId(),
                                    jsonCarbCyclingDay.carbCyclingDayId(),
                                    jsonCarbCyclingDay.carbCyclingPlanId()
                            )
                    );
                }

                CarbCyclingDay carbCyclingDayEntity = CarbCyclingDayMapper.toFreshEntity(jsonCarbCyclingDay, associatedMealPlanDay, carbCyclingPlanEntity);
                carbCyclingDayEntity = entityManager.merge(carbCyclingDayEntity);
                LOG.debug("\tImported carb cycling day and generated ID {}\n\t\tSource: {}", carbCyclingDayEntity.getDayId(), jsonCarbCyclingDay);

                // Find and import meals for carb cycling day
                List<JsonCarbCyclingMeal> jsonCarbCyclingMeals = jsonMealPlanningData
                        .getCarbCyclingMeals()
                        .stream()
                        .filter(carbCyclingMeal -> carbCyclingMeal.carbCyclingDayId().equals(jsonCarbCyclingDay.carbCyclingDayId()))
                        .toList();

                for(JsonCarbCyclingMeal jsonCarbCyclingMeal : jsonCarbCyclingMeals)
                {
                    MealPlanMeal associatedMealPlanMeal = importContext.carbCyclingMealToMealPlanMealAssociations.get(jsonCarbCyclingMeal.carbCyclingMealId());
                    LOG.debug("\tLooking up associated meal plan meal from import context for carb cycling meal with json ID {}", jsonCarbCyclingMeal.carbCyclingMealId());

                    if (associatedMealPlanMeal == null)
                    {
                        throw new DataImportException(
                                String.format(
                                        "Could not find associated meal plan meal with id %s for carb cycling day %s (%s) for carb cycling plan %s",
                                        jsonCarbCyclingMeal.mealPlanMealId(),
                                        associatedMealPlanDay.getDayLabel(),
                                        jsonCarbCyclingMeal.carbCyclingDayId(),
                                        jsonCarbCyclingDay.carbCyclingPlanId()
                                )
                        );
                    }

                    CarbCyclingMeal carbCyclingMealEntity = CarbCyclingMealMapper.toFreshEntity(jsonCarbCyclingMeal, carbCyclingDayEntity, associatedMealPlanMeal);
                    carbCyclingMealEntity = entityManager.merge(carbCyclingMealEntity);
                    LOG.debug("\tImported carb cycling meal and generated ID {}\n\t\tSource: {}", carbCyclingMealEntity.getMealId(), jsonCarbCyclingMeal);

                    // Find and import meal ingredients for carb cycling meal
                    List<JsonMealPlanMealIngredient> jsonCarbCyclingMealIngredients = jsonMealPlanningData
                            .getCarbCyclingPlanMealIngredients()
                            .stream()
                            .filter(mealIngredient -> mealIngredient.mealId().equals(jsonCarbCyclingMeal.carbCyclingMealId()))
                            .toList();
                    writeJsonMealIngredientsForMeal(importedFoods, jsonCarbCyclingMealIngredients, associatedMealPlanMeal);
                }
            }
            LOG.debug("Finished importing carb cycling plan {}. Newly generated plan ID is {}", jsonCarbCyclingPlan.planId(), carbCyclingPlanEntity.getPlanId());
        }
    }

    private void writeJsonMealIngredientsForMeal(FoodImportResult importedFoods, List<JsonMealPlanMealIngredient> jsonMealIngredients, MealPlanMeal forMealPlanMeal)
    {
        for(JsonMealPlanMealIngredient jsonMealIngredient : jsonMealIngredients)
        {
            LOG.debug("\tImporting meal plan meal ingredient from source {}", jsonMealIngredient);
            FoodMetaData ingredientFoodMetaData = entityManager.find(FoodMetaData.class, importedFoods.getIdForImportedFoodByJsonId(jsonMealIngredient.foodId()));

            if (ingredientFoodMetaData == null)
            {
                throw new DataImportException(
                        String.format(
                                "Could not find ingredient food meta data with id of %s for meal %s (%s)",
                                jsonMealIngredient.foodId(),
                                forMealPlanMeal.getMealName(),
                                forMealPlanMeal.getMealId()
                        )
                );
            }

            MealPlanMealIngredient mealPlanMealIngredient = IngredientMapper.toFreshEntity(jsonMealIngredient, forMealPlanMeal, ingredientFoodMetaData);
            mealPlanMealIngredient = entityManager.merge(mealPlanMealIngredient);
            LOG.debug("\tImported meal plan meal ingredient and generated ID {}", mealPlanMealIngredient.getIngredientId());
        }
    }

    /**
     * Contains details about the data of the meal plan hierarchy that is being imported that are
     * needed during subsequent parts of the data import process after the meal plan itself has
     * been imported.
     */
    private static class MealPlanImportContext
    {
        private final User importUser;
        private final MealPlan importedMealPlan;
        private final HashMap<UUID, MealPlanDay> carbCyclingDayToMealPlanDayAssociations;
        private final HashMap<UUID, MealPlanMeal> carbCyclingMealToMealPlanMealAssociations;

        public MealPlanImportContext(User importUser, MealPlan importedMealPlan)
        {
            this.importUser = importUser;
            this.importedMealPlan = importedMealPlan;
            this.carbCyclingDayToMealPlanDayAssociations = new HashMap<>();
            this.carbCyclingMealToMealPlanMealAssociations = new HashMap<>();
        }
    }
    // endregion
}
