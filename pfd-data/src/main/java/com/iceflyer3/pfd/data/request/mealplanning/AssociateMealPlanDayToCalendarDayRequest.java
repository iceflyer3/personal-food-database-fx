/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.request.mealplanning;

import com.iceflyer3.pfd.enums.WeeklyInterval;
import com.iceflyer3.pfd.ui.model.api.viewmodel.ValidatingViewModel;
import com.iceflyer3.pfd.ui.model.api.mealplanning.MealPlanDayModel;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.validation.*;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * Contains the needed information to associate a day for a meal plan or carb cycling plan to
 * be consumed to one or more calendar days in the Meal Plan Calendar.
 *
 * This request object is a blend of a view model and a request object compared to most of the
 * other request objects found in the application that don't interact with the UI at all.
 */
public class AssociateMealPlanDayToCalendarDayRequest implements ValidatingViewModel
{
    private MealPlanDayModel selectedDay;

    private final UserViewModel user;
    private final ObjectProperty<LocalDate> weekStartDate; // This date should always be a Sunday
    private final ObjectProperty<WeeklyInterval> weeklyInterval;
    private final IntegerProperty weeklyIntervalMonths;
    private final Set<DayOfWeek> forDaysOfWeek;

    private final ObservableValuePropertyRestrictionBinding<Number> weeklyIntervalRestriction;

    public AssociateMealPlanDayToCalendarDayRequest(UserViewModel forUser) {
        user = forUser;
        weekStartDate = new SimpleObjectProperty<>();
        weeklyInterval = new SimpleObjectProperty<>(WeeklyInterval.NONE);
        weeklyIntervalMonths = new SimpleIntegerProperty(0);
        forDaysOfWeek = new HashSet<>();

        /*
         * Establish property invariants
         *  - If the association is to be used on an interval then the interval must be provided
         */
        weeklyIntervalRestriction = new ObservableValuePropertyRestrictionBinding<>(this.weeklyIntervalMonths);
        weeklyIntervalRestriction.bindRestriction(new NonZeroDatumRestriction());
    }

    public UserViewModel getUser()
    {
        return user;
    }

    public MealPlanDayModel getSelectedDay()
    {
        return selectedDay;
    }

    public void setSelectedDay(MealPlanDayModel selectedDay)
    {
        this.selectedDay = selectedDay;
    }

    public LocalDate getWeekStartDate()
    {
        return weekStartDate.get();
    }

    public ObjectProperty<LocalDate> weekStartDateProperty()
    {
        return weekStartDate;
    }

    public void setWeekStartDate(LocalDate weekStartDate)
    {
        this.weekStartDate.set(weekStartDate);
    }

    public WeeklyInterval getWeeklyInterval()
    {
        return weeklyInterval.get();
    }

    public ObjectProperty<WeeklyInterval> weeklyIntervalProperty()
    {
        return weeklyInterval;
    }

    public void setWeeklyInterval(WeeklyInterval weeklyInterval)
    {
        this.weeklyInterval.set(weeklyInterval);
    }

    public int getWeeklyIntervalMonths()
    {
        return weeklyIntervalMonths.get();
    }

    public IntegerProperty weeklyIntervalMonthsProperty()
    {
        return weeklyIntervalMonths;
    }

    public void setWeeklyIntervalMonths(int weeklyIntervalMonths)
    {
        this.weeklyIntervalMonths.set(weeklyIntervalMonths);
    }

    public Set<DayOfWeek> getForDaysOfWeek()
    {
        return forDaysOfWeek;
    }

    /**
     * Convenience function for the setting the days of the week to which
     * the association should apply. Passing in null as the parameter will
     * clear the list.
     * @param forDaysOfWeek The set containing the new days of the week
     */
    public void setForDaysOfWeek(Set<DayOfWeek> forDaysOfWeek)
    {
        this.forDaysOfWeek.clear();

        if (forDaysOfWeek != null)
        {
            this.forDaysOfWeek.addAll(forDaysOfWeek);
        }
    }

    public void reset()
    {
        weekStartDate.set(null);
        weeklyInterval.set(WeeklyInterval.NONE);
        weeklyIntervalMonths.set(0);
        forDaysOfWeek.clear();
    }

    @Override
    public ValidationResult validate()
    {
        /*
         * Invariants:
         *  - A meal plan day must be selected
         *  - The date of the week must be selected
         *  - At least one calendar day must be selected
         *
         * This class is a bit different in that it doesn't need to make heavy use of
         * JavaFx properties and built in JavaFx UI controls to gather its data. As such
         * validation is performed a bit differently from the rest of the application.
         */
        ValidationResult validationResult = ValidationResult
                .create(() ->  selectedDay != null, "A meal plan day must be selected before it can be associated to a calendar day")
                .and(() ->  weekStartDate.get() != null, "The date of the start of the week must be specified before a meal plan day may be associated to a calendar day")
                .and(() -> forDaysOfWeek.size() > 0, "One or more days of the indicated week must be specified before a meal plan day may be associated to a calendar day");

        // If the user has opted to use this association on an interval then said interval must be provided
        if (weeklyInterval.get() != WeeklyInterval.NONE)
        {
            validationResult = ValidationResult.flatten(validationResult, weeklyIntervalRestriction.applyRestrictions());
        }
        return validationResult;
    }

    @Override
    public ValidationResult validateProperty(int propertyHashcode)
    {
        if (weeklyInterval.get() == WeeklyInterval.NONE)
        {
            return ValidationResult.success();
        }
        else
        {
            return weeklyIntervalRestriction.applyRestrictions();
        }
    }

    @Override
    public String toString()
    {
        return "AssociateMealPlanDayToCalendarDayRequest{" + "selectedDay=" + selectedDay + ", calendarDate=" + weekStartDate + ", weeklyInterval=" + weeklyInterval + ", weeklyIntervalMonths=" + weeklyIntervalMonths + ", forDaysOfWeek=" + forDaysOfWeek + '}';
    }
}
