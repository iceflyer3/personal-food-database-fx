package com.iceflyer3.pfd.data.task.mealplanning.meal;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanMeal;
import com.iceflyer3.pfd.data.entities.views.MealPlanMealNutritionalSummary;
import com.iceflyer3.pfd.data.mapping.mealplanning.MealPlanMealMapper;
import com.iceflyer3.pfd.data.mapping.summary.NutritionalSummaryMapper;
import com.iceflyer3.pfd.data.repository.MealPlanningRepository;
import com.iceflyer3.pfd.data.task.mealplanning.AbstractMealPlanningTask;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanMealViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;

import java.util.Optional;
import java.util.UUID;

public class GetMealByIdTask extends AbstractMealPlanningTask<MealPlanMealViewModel>
{

    private final UUID mealId;

    public GetMealByIdTask(MealPlanningRepository mealPlanningRepository, UUID mealId) {
        super(mealPlanningRepository);
        this.mealId = mealId;
    }

    @Override
    protected MealPlanMealViewModel call() throws Exception {
        MealPlanMeal mealEntity = mealPlanningRepository.getMealForId(mealId);
        Optional<MealPlanMealNutritionalSummary> nutritionalSummaryEntity = mealPlanningRepository.getMealNutritionalSummary(mealId);

        if (nutritionalSummaryEntity.isPresent())
        {
            return MealPlanMealMapper.toModel(mealEntity, NutritionalSummaryMapper.toModel(nutritionalSummaryEntity.get()));
        }
        else
        {
            return MealPlanMealMapper.toModel(mealEntity, NutritionalSummary.empty());
        }
    }
}
