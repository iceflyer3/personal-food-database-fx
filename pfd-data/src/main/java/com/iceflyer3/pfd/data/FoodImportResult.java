/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data;

import java.util.*;

/**
 * Provides access to the results of the import of food data. This data will be needed for any
 * meal planning / recipe imports that should also occur.
  */
public class FoodImportResult
{
    /*
     * Map of the simple food IDs from the previous database (as indicated by the json) to the
     * new IDs generated during import to the target database.
     */
    private final Map<UUID, UUID> importedSimpleFoods;

    /*
     * Map of the complex food IDs from the previous database (as indicated by the json) to the
     * new IDs generated during import to the target database.
     */
    private final Map<UUID, UUID> importedComplexFoods;

    public FoodImportResult()
    {
        this.importedSimpleFoods = new HashMap<>();
        this.importedComplexFoods = new HashMap<>();
    }

    public void addImportedSimpleFoods(Map<UUID, UUID> simpleFoods)
    {
        this.importedSimpleFoods.putAll(simpleFoods);
    }

    public void addImportedComplexFoods(Map<UUID, UUID> complexFoods)
    {
        this.importedComplexFoods.putAll(complexFoods);
    }

    /**
     * Returns the new ID for an imported food given the previous ID as indicated by the
     * json within an import file.
     *
     * @param jsonId The ID from the json import file
     */
    public UUID getIdForImportedFoodByJsonId(UUID jsonId)
    {
        if (importedSimpleFoods.containsKey(jsonId))
        {
            return importedSimpleFoods.get(jsonId);
        }
        else if (importedComplexFoods.containsKey(jsonId))
        {
            return importedComplexFoods.get(jsonId);
        }

        throw new IllegalArgumentException(String.format("No imported food originally had a json id of %s", jsonId));
    }

    /**
     * Gets an immutable map of all imported foods.
     *
     * The key of the map is the ID from the previous database that was read in from the json file
     * during import.
     *
     * The value of the map is the new ID that was generated when the food was written to the database.
     */
    public Map<UUID, UUID> getAll()
    {
        Map<UUID, UUID> allImportedFoods = new HashMap<>();
        allImportedFoods.putAll(importedSimpleFoods);
        allImportedFoods.putAll(importedComplexFoods);
        return Collections.unmodifiableMap(allImportedFoods);
    }
}
