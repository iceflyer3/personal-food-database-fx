/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.task.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.iceflyer3.pfd.data.json.JsonFoodData;
import com.iceflyer3.pfd.data.json.JsonMealPlanningData;
import com.iceflyer3.pfd.data.json.JsonRecipeData;
import com.iceflyer3.pfd.data.repository.FoodRepository;
import com.iceflyer3.pfd.data.repository.MealPlanningRepository;
import com.iceflyer3.pfd.data.repository.RecipeRepository;
import com.iceflyer3.pfd.data.request.backup.ExportDataRequest;
import javafx.concurrent.Task;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

public class ExportDataTask extends Task<Void>
{
    private final FoodRepository foodRepository;
    private final MealPlanningRepository mealPlanningRepository;
    private final RecipeRepository recipeRepository;
    private final ExportDataRequest request;

    private final ObjectMapper jacksonMapper;
    private final String dateString;

    public ExportDataTask(FoodRepository foodRepository, MealPlanningRepository mealPlanningRepository, RecipeRepository recipeRepository, ExportDataRequest request)
    {
        this.foodRepository = foodRepository;
        this.mealPlanningRepository = mealPlanningRepository;
        this.recipeRepository = recipeRepository;
        this.request = request;
        this.jacksonMapper = new ObjectMapper().registerModule(new JavaTimeModule());

        LocalDateTime today = LocalDateTime.now();
        dateString = String.format("%s-%s-%s", today.getYear(), today.getMonthValue(), today.getDayOfMonth());
    }

    @Override
    protected Void call() throws Exception
    {
        updateMessage("Preparing to start data export...");
        // Verify the directory to which data files will be written exists
        File exportDirectory = new File("../data-export");
        if (!exportDirectory.exists())
        {
            if (!exportDirectory.mkdirs())
            {
                throw new IOException(String.format("Could not create the directory to which files should be exported! Ensure the application has permissions to write to the directory %s", exportDirectory));
            }
        }

        // Export food data
        if (request.shouldIncludeFoods())
        {
            updateMessage("Exporting food data...");
            JsonFoodData jsonFoodData = foodRepository.exportData();

            try
            {
                jacksonMapper.writeValue(new File(exportDirectory, String.format("%s_%s.json", "foods", dateString)), jsonFoodData);
            }
            catch (IOException e)
            {
                throw new IOException("An error has occurred while writing the json file for food data", e);
            }
        }

        // Export recipe data
        if (request.shouldIncludeRecipes())
        {
            updateMessage("Exporting recipe data...");
            JsonRecipeData jsonRecipeData = recipeRepository.exportData();

            try
            {
                jacksonMapper.writeValue(new File(exportDirectory, String.format("%s_%s.json", "recipes", dateString)), jsonRecipeData);
            }
            catch (IOException e)
            {
                throw new IOException("An error has occurred while writing the json file for recipe data", e);
            }
        }

        if (request.shouldIncludeMealPlans())
        {
            updateMessage("Exporting meal planning data...");
            JsonMealPlanningData jsonMealPlanningData = mealPlanningRepository.exportData();

            try
            {
                jacksonMapper.writeValue(new File(exportDirectory, String.format("%s_%s.json", "meal-planning", dateString)), jsonMealPlanningData);
            }
            catch (IOException e)
            {
                throw new IOException("An error has occurred while writing the json file for recipe data", e);
            }
        }

        return null;
    }
}
