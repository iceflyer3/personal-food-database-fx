/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * A collection of classes that facilitate transforming a view model instance into a new
 * persistence layer entity instance and vice versa.
 *
 * Some mappers will make use of functions provided by other mappers
 * while some will be entirely self-contained.
 *
 * <strong>Notes about mapping entities that you should be aware of:</strong>
 * <ul>
 *     <li>
 *         Any entity associations that are required only by the persistence layer will not be set if the mapping
 *         function does not accept them as a parameter.
 *
 *         For example, this means that any MealPlanningMealIngredient entity instances that are returned will
 *         not have the meal to which they belong set on them even though the entity will require this data
 *         due to the Hibernate mappings.
 *
 *         In these instances clients will be responsible for setting these entity associations on the returned
 *         entities themselves where needed.
 *     </li>
 *     <li>
 *         Entities returned from any of the mapper classes will need re-attached to the persistence context
 *         before you can do any useful work with them in the persistence layer due to how short lived the
 *         persistence context is.
 *     </li>
 *     <li>
 *         The toFreshEntity functions found in some mappers are to eliminate any chances at id collisions and
 *         should be used when you want to ensure that a new entity will always be created. These functions
 *         exclude the id when performing the requested mapping.
 *
 *         The chances of a UUIDv4 collision actually occurring are astronomically unlikely. So this could be
 *         seen as overkill for something that should, in all likelihood, never actually happen.
 *
 *         However, on the exceedingly rare chance that it does actually happen, it would be the source of an
 *         exceptionally subtle bug that would be immensely difficult to reproduce or troubleshoot.
 *
 *         So out of an overabundance of caution we'll play it safe and provide a facility to always force a
 *         new id to be generated. Even if this should theoretically never really matter in the first place.
 *     </li>
 * </ul>
 */
package com.iceflyer3.pfd.data.mapping;