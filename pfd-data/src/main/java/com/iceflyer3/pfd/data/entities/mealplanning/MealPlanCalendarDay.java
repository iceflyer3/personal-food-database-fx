/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


package com.iceflyer3.pfd.data.entities.mealplanning;

import com.iceflyer3.pfd.constants.QueryConstants;
import com.iceflyer3.pfd.data.entities.User;
import com.iceflyer3.pfd.enums.WeeklyInterval;

import jakarta.persistence.*;
import java.time.LocalDate;
import java.util.UUID;

@Entity
@Table(name="meal_plan_calendar_day")
@NamedQuery(
        name = QueryConstants.QUERY_MEAL_PLANNING_FIND_PLANNED_CALENDAR_DAYS_FOR_USER,
        query = """
                SELECT calDay
                FROM MealPlanCalendarDay calDay
                LEFT JOIN calDay.plannedDay.carbCyclingDay carbCyclingDay
                WHERE carbCyclingDay IS NULL AND calDay.user.userId = :userId
                """)
@NamedQuery(
        name = QueryConstants.QUERY_MEAL_PLANNING_FIND_PLANNED_CARB_CYCLING_CALENDAR_DAYS_FOR_USER,
        query = """
                SELECT calDay
                FROM MealPlanCalendarDay calDay
                LEFT JOIN calDay.plannedDay.carbCyclingDay carbCyclingDay
                WHERE carbCyclingDay IS NOT NULL AND calDay.user.userId = :userId
                """)
@NamedQuery(
        name = QueryConstants.QUERY_MEAL_PLANNING_FIND_ALL_CALENDAR_DAYS_FOR_USER,
        query = "SELECT calDay FROM MealPlanCalendarDay calDay WHERE calDay.user.userId = :userId")
@NamedQuery(
        name = QueryConstants.QUERY_MEAL_PLANNING_FIND_CALENDAR_DAYS_IN_RANGE,
        query = """
                SELECT calDay
                FROM MealPlanCalendarDay calDay
                WHERE calDay.calendarDate >= :startDate
                AND calDay.calendarDate <= :endDate
                """)
public class MealPlanCalendarDay
{
    @Id
    @GeneratedValue
    @Column(name = "calendar_day_id")
    protected UUID calendarDayId;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "day_id", nullable = false)
    protected MealPlanDay plannedDay;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    protected User user;

    @Column(name = "calendar_date", nullable = false)
    protected LocalDate calendarDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "weekly_interval", nullable = false)
    protected WeeklyInterval weeklyInterval;

    @Column(name = "interval_months_duration", nullable = false)
    protected Integer intervalMonthsDuration;

    @Column(name = "is_recurrence", nullable = false)
    protected Boolean recurrence;

    public MealPlanCalendarDay() { }

    public MealPlanCalendarDay(MealPlanDay plannedDay, User user, LocalDate calendarDate, WeeklyInterval weeklyInterval, Integer intervalMonthsDuration, Boolean isRecurrence)
    {
        this(
                null,
                plannedDay,
                user,
                calendarDate,
                weeklyInterval,
                intervalMonthsDuration,
                isRecurrence
        );
    }

    public MealPlanCalendarDay(UUID calendarDayId, MealPlanDay plannedDay, User user, LocalDate calendarDate, WeeklyInterval weeklyInterval, Integer intervalMonthsDuration, Boolean isRecurrence)
    {
        this.calendarDayId = calendarDayId;
        this.plannedDay = plannedDay;
        this.user = user;
        this.calendarDate = calendarDate;
        this.weeklyInterval = weeklyInterval;
        this.intervalMonthsDuration = intervalMonthsDuration;
        this.recurrence = isRecurrence;
    }

    public UUID getCalendarDayId()
    {
        return calendarDayId;
    }

    public MealPlanDay getPlannedDay()
    {
        return plannedDay;
    }

    public LocalDate getCalendarDate()
    {
        return calendarDate;
    }

    public WeeklyInterval getWeeklyInterval()
    {
        return weeklyInterval;
    }

    public Integer getIntervalMonthsDuration()
    {
        return intervalMonthsDuration;
    }

    public Boolean isRecurrence()
    {
        return recurrence;
    }
}
