/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.json.recipe;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.UUID;

public class JsonRecipeIngredient
{
    private UUID recipeIngredientId;
    private UUID forRecipeId;
    private UUID foodId;
    private BigDecimal numberOfServings;
    private boolean isAlternate;
    private boolean isOptional;

    @JsonCreator
    public JsonRecipeIngredient(
            @JsonProperty("recipeIngredientId") UUID recipeIngredientId,
            @JsonProperty("forRecipeId") UUID forRecipeId,
            @JsonProperty("foodId") UUID foodId,
            @JsonProperty("numberOfServings") BigDecimal numberOfServings,
            @JsonProperty("isAlternate") boolean isAlternate,
            @JsonProperty("isOptional") boolean isOptional)
    {
        this.recipeIngredientId = recipeIngredientId;
        this.forRecipeId = forRecipeId;
        this.foodId = foodId;
        this.numberOfServings = numberOfServings;
        this.isAlternate = isAlternate;
        this.isOptional = isOptional;
    }

    public UUID getRecipeIngredientId()
    {
        return recipeIngredientId;
    }

    public void setRecipeIngredientId(UUID recipeIngredientId)
    {
        this.recipeIngredientId = recipeIngredientId;
    }

    public UUID getForRecipeId()
    {
        return forRecipeId;
    }

    public void setForRecipeId(UUID forRecipeId)
    {
        this.forRecipeId = forRecipeId;
    }

    public UUID getFoodId()
    {
        return foodId;
    }

    public void setFoodId(UUID foodId)
    {
        this.foodId = foodId;
    }

    public BigDecimal getNumberOfServings()
    {
        return numberOfServings;
    }

    public void setNumberOfServings(BigDecimal numberOfServings)
    {
        this.numberOfServings = numberOfServings;
    }

    public boolean isAlternate()
    {
        return isAlternate;
    }

    public void setAlternate(boolean alternate)
    {
        isAlternate = alternate;
    }

    public boolean isOptional()
    {
        return isOptional;
    }

    public void setOptional(boolean optional)
    {
        isOptional = optional;
    }

    @Override
    public String toString()
    {
        return "JsonRecipeIngredient{" + "recipeIngredientId=" + recipeIngredientId + ", forRecipeId=" + forRecipeId + ", foodId=" + foodId + ", numberOfServings=" + numberOfServings + ", isAlternate=" + isAlternate + ", isOptional=" + isOptional + '}';
    }
}
