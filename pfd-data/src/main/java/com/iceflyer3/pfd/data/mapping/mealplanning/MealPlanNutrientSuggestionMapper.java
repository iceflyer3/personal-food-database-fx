package com.iceflyer3.pfd.data.mapping.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023, 2025  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.entities.mealplanning.MealPlan;
import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanNutrientSuggestion;
import com.iceflyer3.pfd.data.json.mealplanning.JsonMealPlanNutrientSuggestion;
import com.iceflyer3.pfd.enums.Nutrient;
import com.iceflyer3.pfd.enums.UnitOfMeasure;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanNutrientSuggestionViewModel;
import com.iceflyer3.pfd.util.NumberUtils;

public class MealPlanNutrientSuggestionMapper
{

    private MealPlanNutrientSuggestionMapper() { }

    public static MealPlanNutrientSuggestionViewModel toModel(MealPlanNutrientSuggestion suggestionEntity)
    {
        return new MealPlanNutrientSuggestionViewModel(
                suggestionEntity.getMacroId(),
                suggestionEntity.getNutrient(),
                NumberUtils.withStandardPrecision(suggestionEntity.getSuggestedAmount()),
                suggestionEntity.getUnitOfMeasure());
    }

    public static JsonMealPlanNutrientSuggestion toJson(MealPlanNutrientSuggestion nutrientSuggestion)
    {
        return new JsonMealPlanNutrientSuggestion(
                nutrientSuggestion.getMacroId(),
                nutrientSuggestion.getMealPlan().getMealPlanId(),
                nutrientSuggestion.getNutrient().toString(),
                nutrientSuggestion.getSuggestedAmount(),
                nutrientSuggestion.getUnitOfMeasure().toString()
        );
    }

    /**
     * Returns a new (and detached) MealPlanNutrientSuggestion instance hydrated from json without a set id.
     * @param jsonMealPlanNutrientSuggestion json from which to hydrate the new instance
     * @param forMealPlan Meal plan the day is for
     * @return MealPlanNutrientSuggestion instance hydrated from json without a set id
     */
    public static MealPlanNutrientSuggestion toFreshEntity(JsonMealPlanNutrientSuggestion jsonMealPlanNutrientSuggestion, MealPlan forMealPlan)
    {
        return new MealPlanNutrientSuggestion(
                null,
                forMealPlan,
                Nutrient.valueOf(jsonMealPlanNutrientSuggestion.macronutrient()),
                jsonMealPlanNutrientSuggestion.suggestedAmount(),
                UnitOfMeasure.valueOf(jsonMealPlanNutrientSuggestion.unitOfMeasure())
        );
    }
}
