package com.iceflyer3.pfd.data.mapping.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.entities.mealplanning.MealPlan;
import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanDay;
import com.iceflyer3.pfd.data.json.mealplanning.JsonMealPlanDay;
import com.iceflyer3.pfd.enums.FitnessGoal;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanDayViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;

public class MealPlanDayMapper
{
    private MealPlanDayMapper() { }

    public static MealPlanDayViewModel toModel(MealPlanDay entity, NutritionalSummary nutritionalSummary)
    {
        MealPlanDayViewModel mealPlanDayViewModel = new MealPlanDayViewModel(entity.getDayId(), entity.getMealPlan().getMealPlanId(), nutritionalSummary);
        mealPlanDayViewModel.setDayLabel(entity.getDayLabel());
        mealPlanDayViewModel.setFitnessGoal(entity.getDayFitnessGoal());
        mealPlanDayViewModel.setLastModifiedDate(entity.getLastModifiedDate());
        return mealPlanDayViewModel;
    }

    public static JsonMealPlanDay toJson(MealPlanDay mealPlanDay)
    {
        return new JsonMealPlanDay(
                mealPlanDay.getDayId(),
                mealPlanDay.getCarbCyclingDay() == null ? null : mealPlanDay.getCarbCyclingDay().getDayId(),
                mealPlanDay.getMealPlan().getMealPlanId(),
                mealPlanDay.getDayLabel(),
                mealPlanDay.getDayFitnessGoal().toString(),
                mealPlanDay.getLastModifiedDate()
        );
    }

    /**
     * Returns a new (and detached) MealPlanDay instance hydrated from json without a set id.
     * @param jsonMealPlanDay json from which to hydrate the new instance
     * @param forMealPlan Meal plan the day is for
     * @return MealPlanDay instance hydrated from json without a set id
     */
    public static MealPlanDay toFreshEntity(JsonMealPlanDay jsonMealPlanDay, MealPlan forMealPlan)
    {
        return new MealPlanDay(
                null,
                forMealPlan,
                FitnessGoal.valueOf(jsonMealPlanDay.dayFitnessGoal()),
                jsonMealPlanDay.dayLabel(),
                jsonMealPlanDay.lastModifiedDate()
        );
    }
}
