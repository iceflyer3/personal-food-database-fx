/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.entities.recipe;

import com.iceflyer3.pfd.constants.QueryConstants;

import jakarta.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "recipe_step_ingredient")
@NamedQuery(
        name = QueryConstants.QUERY_RECIPE_FIND_STEP_INGREDIENTS_FOR_STEP_FOR_RECIPE,
        query = "SELECT recipeStepIngredient FROM RecipeStepIngredient recipeStepIngredient WHERE recipeStepIngredient.recipeStep.stepId = :recipeStepId")
@NamedQuery(
        name = QueryConstants.QUERY_RECIPE_FIND_STEP_INGREDIENTS_FOR_USER,
        query = "SELECT recipeStepIngredient FROM RecipeStepIngredient recipeStepIngredient WHERE recipeStepIngredient.recipe.createdUser.userId = :userId")
public class RecipeStepIngredient {

    public RecipeStepIngredient() { }

    public RecipeStepIngredient(Recipe forRecipe, RecipeStep forStep, RecipeIngredient ingredient)
    {
        this.stepIngredientId = new RecipeStepIngredientId(forRecipe.getRecipeId(), forStep.getStepId(), ingredient.getRecipeIngredientId());
        this.recipe = forRecipe;
        this.recipeStep = forStep;
        this.recipeIngredient = ingredient;
    }

    @EmbeddedId
    protected RecipeStepIngredientId stepIngredientId = new RecipeStepIngredientId();

    // These are already mapped by the primary key class so they must be immutable.
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "recipe_id", insertable = false, updatable = false)
    protected Recipe recipe;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "recipe_step_id", insertable = false, updatable = false)
    protected RecipeStep recipeStep;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "recipe_ingredient_id", insertable = false, updatable = false)
    protected RecipeIngredient recipeIngredient;

    public RecipeStepIngredientId getStepIngredientId() {
        return stepIngredientId;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public RecipeStep getRecipeStep() {
        return recipeStep;
    }

    public RecipeIngredient getRecipeIngredient() {
        return recipeIngredient;
    }

    @Override
    public String toString() {
        return "RecipeStepIngredient{" +
                "stepIngredientId=" + stepIngredientId +
                ", recipe=" + recipe +
                ", recipeStep=" + recipeStep +
                ", recipeIngredient=" + recipeIngredient +
                '}';
    }

    @Embeddable
    public static class RecipeStepIngredientId implements Serializable {
        public RecipeStepIngredientId() { }

        public RecipeStepIngredientId(UUID recipeId, UUID recipeStepId, UUID recipeIngredientId)
        {
            this.recipeId = recipeId;
            this.recipeStepId = recipeStepId;
            this.recipeIngredientId = recipeIngredientId;
        }

        @Column(name = "recipe_id")
        protected UUID recipeId;

        @Column(name = "recipe_step_id")
        protected UUID recipeStepId;

        @Column(name = "recipe_ingredient_id")
        protected UUID recipeIngredientId;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            RecipeStepIngredientId that = (RecipeStepIngredientId) o;
            return recipeId.equals(that.recipeId) &&
                    recipeStepId.equals(that.recipeStepId) &&
                    recipeIngredientId.equals(that.recipeIngredientId);
        }

        @Override
        public int hashCode() {
            return Objects.hash(recipeId, recipeStepId, recipeIngredientId);
        }
    }
}
