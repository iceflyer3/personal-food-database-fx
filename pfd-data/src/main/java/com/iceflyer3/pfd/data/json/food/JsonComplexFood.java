/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.json.food;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;
import java.util.UUID;

public class JsonComplexFood
{
    private UUID foodId;
    private String foodName;
    private UUID createdUserId;
    private UUID lastModifiedUserId;
    private LocalDateTime createdDate;
    private LocalDateTime lastModifiedDate;

    @JsonCreator
    public JsonComplexFood(
            @JsonProperty("foodId") UUID foodId,
            @JsonProperty("foodName") String foodName,
            @JsonProperty("createdUserId") UUID createdUserId,
            @JsonProperty("lastModifiedUserId") UUID lastModifiedUserId,
            @JsonProperty("createdDate") LocalDateTime createdDate,
            @JsonProperty("lastModifiedDate") LocalDateTime lastModifiedDate)
    {
        this.foodId = foodId;
        this.foodName = foodName;
        this.createdUserId = createdUserId;
        this.lastModifiedUserId = lastModifiedUserId;
        this.createdDate = createdDate;
        this.lastModifiedDate = lastModifiedDate;
    }

    public UUID getFoodId()
    {
        return foodId;
    }

    public void setFoodId(UUID foodId)
    {
        this.foodId = foodId;
    }

    public String getFoodName()
    {
        return foodName;
    }

    public void setFoodName(String foodName)
    {
        this.foodName = foodName;
    }

    public UUID getCreatedUserId()
    {
        return createdUserId;
    }

    public void setCreatedUserId(UUID createdUserId)
    {
        this.createdUserId = createdUserId;
    }

    public UUID getLastModifiedUserId()
    {
        return lastModifiedUserId;
    }

    public void setLastModifiedUserId(UUID lastModifiedUserId)
    {
        this.lastModifiedUserId = lastModifiedUserId;
    }

    public LocalDateTime getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate)
    {
        this.createdDate = createdDate;
    }

    public LocalDateTime getLastModifiedDate()
    {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate)
    {
        this.lastModifiedDate = lastModifiedDate;
    }
}
