package com.iceflyer3.pfd.data.mapping;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.entities.food.FoodMetaData;
import com.iceflyer3.pfd.data.entities.food.FoodServingSize;
import com.iceflyer3.pfd.data.entities.recipe.Recipe;
import com.iceflyer3.pfd.data.entities.recipe.RecipeServingSize;
import com.iceflyer3.pfd.data.json.food.JsonFoodServingSize;
import com.iceflyer3.pfd.data.json.recipe.JsonRecipeServingSize;
import com.iceflyer3.pfd.enums.UnitOfMeasure;
import com.iceflyer3.pfd.ui.model.viewmodel.ServingSizeViewModel;

import java.util.UUID;

public class ServingSizeMapper {

    private ServingSizeMapper() {}

    public static ServingSizeViewModel toModel(FoodServingSize entity) {
        return new ServingSizeViewModel(entity.getServingSizeId(), entity.getUnitOfMeasure(), entity.getServingSize());
    }

    public static ServingSizeViewModel toModel(RecipeServingSize entity)
    {
        return new ServingSizeViewModel(entity.getServingSizeId(), entity.getUnitOfMeasure(), entity.getServingSize());
    }

    public static JsonFoodServingSize toJson(FoodServingSize foodServingSize, UUID foodId)
    {
        return new JsonFoodServingSize(
                foodServingSize.getServingSizeId(),
                foodId,
                foodServingSize.getUnitOfMeasure().toString(),
                foodServingSize.getServingSize()
        );
    }

    public static JsonRecipeServingSize toJson(RecipeServingSize recipeServingSize)
    {
        return new JsonRecipeServingSize(
                recipeServingSize.getServingSizeId(),
                recipeServingSize.getRecipe().getRecipeId(),
                recipeServingSize.getUnitOfMeasure().toString(),
                recipeServingSize.getServingSize()
        );
    }

    /**
     * Returns a new (and detached) FoodServingSize instance hydrated from json without a set id.
     * @param jsonFoodServingSize json from which to hydrate the new instance
     * @param foodMetaData Associated food metadata entity
     * @return FoodServingSize instance hydrated from json without a set id
     */
    public static FoodServingSize toFreshEntity(JsonFoodServingSize jsonFoodServingSize, FoodMetaData foodMetaData)
    {
        return new FoodServingSize(
                null,
                UnitOfMeasure.valueOf(jsonFoodServingSize.getUnitOfMeasure()),
                jsonFoodServingSize.getServingSize(),
                foodMetaData
        );
    }

    public static RecipeServingSize toFreshEntity(JsonRecipeServingSize jsonRecipeServingSize, Recipe forRecipe)
    {
        return new RecipeServingSize(
                null,
                forRecipe,
                UnitOfMeasure.valueOf(jsonRecipeServingSize.getUnitOfMeasure()),
                jsonRecipeServingSize.getServingSize()
        );
    }
}
