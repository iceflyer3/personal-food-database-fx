package com.iceflyer3.pfd.data.task.mealplanning.carbcycling;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.entities.mealplanning.carbcycling.CarbCyclingMealConfiguration;
import com.iceflyer3.pfd.data.mapping.mealplanning.carbcycling.CarbCyclingMealConfigurationMapper;
import com.iceflyer3.pfd.data.repository.MealPlanningRepository;
import com.iceflyer3.pfd.data.task.mealplanning.AbstractMealPlanningTask;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingMealConfigurationViewModel;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

public class GetCarbCyclingMealConfigurationsByPlanIdTask extends AbstractMealPlanningTask<Set<CarbCyclingMealConfigurationViewModel>>
{
    private final UUID planId;

    public GetCarbCyclingMealConfigurationsByPlanIdTask(MealPlanningRepository mealPlanningRepository, UUID planId)
    {
        super(mealPlanningRepository);
        this.planId = planId;
    }

    @Override
    protected Set<CarbCyclingMealConfigurationViewModel> call() throws Exception
    {
        List<CarbCyclingMealConfiguration> mealConfigurations = mealPlanningRepository.getMealConfigurationsForCarbCyclingPlan(planId);

        if (mealConfigurations.isEmpty())
        {
            return new HashSet<>();
        }
        else
        {
            return mealConfigurations.stream().map(CarbCyclingMealConfigurationMapper::toModel).collect(Collectors.toSet());
        }
    }
}
