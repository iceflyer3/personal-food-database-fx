/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.entities.food;

import com.iceflyer3.pfd.constants.QueryConstants;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.util.UUID;

@Entity
@Table(name = "complex_food_ingredient")
@NamedQuery(name = QueryConstants.QUERY_FOOD_FIND_COMPLEX_FOOD_INGREDIENTS, query = "SELECT ingredient FROM ComplexFoodIngredient ingredient WHERE ingredient.complexFood.foodId = :foodId")
public class ComplexFoodIngredient {

    public ComplexFoodIngredient() { }

    public ComplexFoodIngredient(SimpleFood simpleFood,
                                 ComplexFood complexFood,
                                 BigDecimal simpleFoodServingsIncluded)
    {
        this.simpleFood = simpleFood;
        this.complexFood = complexFood;

        this.isAlternate = false;
        this.isOptional = false;

        this.simpleFoodServingsIncluded = simpleFoodServingsIncluded;
    }

    public ComplexFoodIngredient(
            UUID ingredientId,
            SimpleFood simpleFood,
            ComplexFood complexFood,
            BigDecimal simpleFoodServingsIncluded,
            boolean isAlternate,
            boolean isOptional)
    {
        this.ingredientId = ingredientId;
        this.simpleFood = simpleFood;
        this.complexFood = complexFood;

        this.isAlternate = isAlternate;
        this.isOptional = isOptional;

        this.simpleFoodServingsIncluded = simpleFoodServingsIncluded;
    }

    @Id
    @GeneratedValue
    @Column(name = "ingredient_id")
    protected UUID ingredientId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "simple_food_id")
    protected SimpleFood simpleFood;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "complex_food_id")
    protected ComplexFood complexFood;

    @Column(name = "simple_food_servings_included", precision = 2)
    protected BigDecimal simpleFoodServingsIncluded;

    @Column(name = "is_alternate", nullable = false)
    protected Boolean isAlternate;

    @Column(name = "is_optional", nullable = false)
    protected Boolean isOptional;

    public UUID getIngredientId() {
        return ingredientId;
    }

    public SimpleFood getSimpleFood() {
        return simpleFood;
    }

    public ComplexFood getComplexFood() {
        return complexFood;
    }

    public BigDecimal getSimpleFoodServingsIncluded() {
        return simpleFoodServingsIncluded;
    }

    public void setSimpleFoodServingsIncluded(BigDecimal simpleFoodServingsIncluded) {
        this.simpleFoodServingsIncluded = simpleFoodServingsIncluded;
    }

    public Boolean getIsAlternate() {
        return isAlternate;
    }

    public void setIsAlternate(Boolean alternate) {
        isAlternate = alternate;
    }

    public Boolean getIsOptional() {
        return isOptional;
    }

    public void setIsOptional(Boolean optional) {
        isOptional = optional;
    }
}
