/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.entities.recipe;

import com.iceflyer3.pfd.constants.QueryConstants;
import com.iceflyer3.pfd.data.entities.User;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "recipe")
@NamedQuery(name = QueryConstants.QUERY_RECIPE_FIND_ALL, query = "SELECT recipe FROM Recipe recipe")
@NamedQuery(name = QueryConstants.QUERY_RECIPE_FIND_BY_NAME, query = "SELECT recipe FROM Recipe recipe WHERE recipe.recipeName = :recipeName")
@NamedQuery(name = QueryConstants.QUERY_RECIPE_FIND_ALL_FOR_USER, query = "SELECT recipe FROM Recipe recipe WHERE recipe.createdUser.userId = :userId")
public class Recipe {

    public Recipe() { }

    public Recipe(String recipeName,
                  String source,
                  BigDecimal numberOfServingsMade,
                  User createdUser)
    {
        this.recipeId = null;
        this.recipeName = recipeName;
        this.source = source;
        this.numberOfServingsMade = numberOfServingsMade;

        this.createdUser = createdUser;
        this.createdDate = LocalDateTime.now();

        this.lastModifiedUser = createdUser;
        this.lastModifiedDate = LocalDateTime.now();
    }

    public Recipe(
            UUID recipeId,
            User createdUser,
            User lastModifiedUser,
            String recipeName,
            String source,
            BigDecimal numberOfServingsMade,
            LocalDateTime createdDate,
            LocalDateTime lastModifiedDate
    )
    {
        this.recipeId = recipeId;
        this.recipeName = recipeName;
        this.source = source;
        this.numberOfServingsMade = numberOfServingsMade;

        this.createdUser = createdUser;
        this.createdDate = createdDate;

        this.lastModifiedUser = lastModifiedUser;
        this.lastModifiedDate = lastModifiedDate;
    }

    @Id
    @GeneratedValue
    @Column(name = "recipe_id")
    protected UUID recipeId;

    @Column(name = "recipe_name", nullable = false)
    protected String recipeName;

    @OneToMany(mappedBy = "recipe", fetch = FetchType.LAZY)
    protected Set<RecipeIngredient> ingredients = new HashSet<>();

    @OneToMany(mappedBy = "recipe", fetch = FetchType.EAGER)
    protected Set<RecipeServingSize> servingSizes = new HashSet<>();

    @OneToMany(mappedBy = "recipe", fetch = FetchType.LAZY)
    protected Set<RecipeStep> steps = new HashSet<>();

    @Column(name = "source", nullable = false)
    protected String source;

    @Column(name = "number_of_servings_made", precision = 2, nullable = false)
    protected BigDecimal numberOfServingsMade;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "created_user_id", nullable = false)
    protected User createdUser;

    @Column(name="created_date", nullable = false, columnDefinition = "timestamp")
    protected LocalDateTime createdDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "last_modified_user_id", nullable = false)
    protected User lastModifiedUser;

    @Column(name="last_modified_date", nullable = false, columnDefinition = "timestamp")
    protected LocalDateTime lastModifiedDate;

    public UUID getRecipeId() {
        return recipeId;
    }

    public Set<RecipeServingSize> getServingSizes() {
        return servingSizes;
    }

    public Set<RecipeIngredient> getIngredients() {
        return ingredients;
    }

    public Set<RecipeStep> getSteps() {
        return steps;
    }

    public User getCreatedUser() {
        return createdUser;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public String getRecipeName() {
        return recipeName;
    }

    public void setRecipeName(String recipeName) {
        this.recipeName = recipeName;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public BigDecimal getNumberOfServingsMade() {
        return numberOfServingsMade;
    }

    public void setNumberOfServingsMade(BigDecimal numberOfServingsMade) {
        this.numberOfServingsMade = numberOfServingsMade;
    }

    public User getLastModifiedUser() {
        return lastModifiedUser;
    }

    public void setLastModifiedUser(User lastModifiedUser) {
        this.lastModifiedUser = lastModifiedUser;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @Override
    public String toString() {
        return "Recipe{" +
                "recipeId=" + recipeId +
                ", recipeName='" + recipeName + '\'' +
                ", ingredients=" + ingredients.size() + " items" +
                ", servingSizes=" + servingSizes.size() + " items" +
                ", steps=" + steps.size() + " items" +
                ", source='" + source + '\'' +
                ", numberOfServingsMade=" + numberOfServingsMade +
                ", createdUser=" + createdUser +
                ", createdDate=" + createdDate +
                ", lastModifiedUser=" + lastModifiedUser +
                ", lastModifiedDate=" + lastModifiedDate +
                '}';
    }
}
