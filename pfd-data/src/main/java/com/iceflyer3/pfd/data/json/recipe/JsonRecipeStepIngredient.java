/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.json.recipe;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class JsonRecipeStepIngredient
{
    private UUID recipeId;
    private UUID recipeStepId;
    private UUID recipeIngredientId;

    @JsonCreator
    public JsonRecipeStepIngredient(
            @JsonProperty("recipeId") UUID recipeId,
            @JsonProperty("recipeStepId") UUID recipeStepId,
            @JsonProperty("recipeIngredientId") UUID recipeIngredientId)
    {
        this.recipeId = recipeId;
        this.recipeStepId = recipeStepId;
        this.recipeIngredientId = recipeIngredientId;
    }

    public UUID getRecipeId()
    {
        return recipeId;
    }

    public void setRecipeId(UUID recipeId)
    {
        this.recipeId = recipeId;
    }

    public UUID getRecipeStepId()
    {
        return recipeStepId;
    }

    public void setRecipeStepId(UUID recipeStepId)
    {
        this.recipeStepId = recipeStepId;
    }

    public UUID getRecipeIngredientId()
    {
        return recipeIngredientId;
    }

    public void setRecipeIngredientId(UUID recipeIngredientId)
    {
        this.recipeIngredientId = recipeIngredientId;
    }

    @Override
    public String toString()
    {
        return "JsonRecipeStepIngredient{" + "recipeId=" + recipeId + ", recipeStepId=" + recipeStepId + ", recipeIngredientId=" + recipeIngredientId + '}';
    }
}
