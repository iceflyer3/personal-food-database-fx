package com.iceflyer3.pfd.data.mapping.food;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.entities.User;
import com.iceflyer3.pfd.data.entities.food.ComplexFood;
import com.iceflyer3.pfd.data.entities.food.FoodMetaData;
import com.iceflyer3.pfd.data.entities.food.SimpleFood;
import com.iceflyer3.pfd.data.json.food.JsonComplexFood;
import com.iceflyer3.pfd.data.json.food.JsonFoodMetaData;
import com.iceflyer3.pfd.data.json.food.JsonSimpleFood;
import com.iceflyer3.pfd.enums.FoodType;
import com.iceflyer3.pfd.data.mapping.ServingSizeMapper;
import com.iceflyer3.pfd.ui.model.api.ServingSizeModel;
import com.iceflyer3.pfd.ui.model.viewmodel.ServingSizeViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.ComplexFoodViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.ReadOnlyFoodViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.SimpleFoodViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Mapper for mapping between the various food entities and
 * view models
 */
@SuppressWarnings("DuplicatedCode")
public class FoodMapper
{

    private FoodMapper()
    {
    }

    /**
     * Maps a simple food to read-only view model.
     *
     * @param entity The entity to map
     * @return A new ReadOnlyFoodViewModel instance
     */
    public static ReadOnlyFoodViewModel toModel(SimpleFood entity)
    {
        SimpleFoodViewModel mutableModel = toMutableModel(entity);
        return new ReadOnlyFoodViewModel(mutableModel);
    }

    /**
     * Maps a complex to a read-only view model.
     *
     * @param complexFood The entity to map
     * @return A new ReadOnlyFoodViewModel instance
     */
    public static ReadOnlyFoodViewModel toModel(ComplexFood complexFood, NutritionalSummary nutritionalSummary)
    {
        ComplexFoodViewModel mutableModel = toMutableModel(complexFood, nutritionalSummary);
        return new ReadOnlyFoodViewModel(mutableModel);
    }

    /**
     * Maps a ComplexFood entity to a mutable view-model
     *
     * @param complexFood The ComplexFood instance to map
     * @return A new ComplexViewModel instance
     */
    public static ComplexFoodViewModel toMutableModel(ComplexFood complexFood, NutritionalSummary nutritionalSummary)
    {
        List<ServingSizeModel> servingSizeViewModelList = complexFood.getFoodMetaData().getServingSizes().stream().map(ServingSizeMapper::toModel).collect(Collectors.toList());
        ComplexFoodViewModel complexFoodViewModel = new ComplexFoodViewModel(complexFood.getFoodId(), complexFood.getName(), complexFood.getCreatedUser().getUsername(), complexFood.getCreatedDate(), complexFood.getLastModifiedUser().getUsername(), complexFood.getLastModifiedDate(), servingSizeViewModelList, nutritionalSummary);

        return complexFoodViewModel;
    }

    /**
     * Maps a SimpleFood entity to a mutable view-model.
     *
     * @param entity The SimpleFood instance to map
     * @return A new SimpleFoodViewModel instance
     */
    public static SimpleFoodViewModel toMutableModel(SimpleFood entity)
    {
        Set<ServingSizeViewModel> servingSizes = entity.getFoodMetaData().getServingSizes().stream().map(ServingSizeMapper::toModel).collect(Collectors.toSet());
        SimpleFoodViewModel mappedModel = new SimpleFoodViewModel(entity.getFoodId(), entity.getCreatedUser().getUsername(), entity.getCreatedDate(), entity.getName(), entity.getBrand(), entity.getSource(), servingSizes, entity.getCalories());
        mappedModel.setFoodType(FoodType.SIMPLE);
        mappedModel.setGlycemicIndex(entity.getGlycemicIndex());
        mappedModel.setLastModifiedDate(entity.getLastModifiedDate());
        mappedModel.setLastModifiedUser(entity.getLastModifiedUser().getUsername());

        // Macronutrients
        mappedModel.setProtein(entity.getProtein());
        mappedModel.setTotalFats(entity.getTotalFats());
        mappedModel.setTransFat(entity.getTransFat());
        mappedModel.setMonounsaturatedFat(entity.getMonounsaturatedFat());
        mappedModel.setPolyunsaturatedFat(entity.getPolyunsaturatedFat());
        mappedModel.setSaturatedFat(entity.getSaturatedFat());
        mappedModel.setCarbohydrates(entity.getCarbohydrates());
        mappedModel.setSugars(entity.getSugars());
        mappedModel.setFiber(entity.getFiber());

        // Micronutrients
        mappedModel.setCholesterol(entity.getCholesterol());
        mappedModel.setIron(entity.getIron());
        mappedModel.setManganese(entity.getManganese());
        mappedModel.setCopper(entity.getCopper());
        mappedModel.setZinc(entity.getZinc());
        mappedModel.setCalcium(entity.getCalcium());
        mappedModel.setMagnesium(entity.getMagnesium());
        mappedModel.setPhosphorus(entity.getPhosphorus());
        mappedModel.setPotassium(entity.getPotassium());
        mappedModel.setSodium(entity.getSodium());
        mappedModel.setSulfur(entity.getSulfur());
        mappedModel.setVitaminA(entity.getVitaminA());
        mappedModel.setVitaminC(entity.getVitaminC());
        mappedModel.setVitaminK(entity.getVitaminK());
        mappedModel.setVitaminD(entity.getVitaminD());
        mappedModel.setVitaminE(entity.getVitaminE());
        mappedModel.setVitaminB1(entity.getVitaminB1());
        mappedModel.setVitaminB2(entity.getVitaminB2());
        mappedModel.setVitaminB3(entity.getVitaminB3());
        mappedModel.setVitaminB5(entity.getVitaminB5());
        mappedModel.setVitaminB6(entity.getVitaminB6());
        mappedModel.setVitaminB7(entity.getVitaminB7());
        mappedModel.setVitaminB8(entity.getVitaminB8());
        mappedModel.setVitaminB9(entity.getVitaminB9());
        mappedModel.setVitaminB12(entity.getVitaminB12());
        return mappedModel;
    }

    public static JsonSimpleFood toJson(SimpleFood simpleFood)
    {
        return new JsonSimpleFood(simpleFood.getFoodId(), simpleFood.getName(), simpleFood.getBrand(), simpleFood.getSource(), simpleFood.getCreatedUser().getUserId(), simpleFood.getLastModifiedUser().getUserId(), simpleFood.getCreatedDate(), simpleFood.getLastModifiedDate(), simpleFood.getGlycemicIndex(), simpleFood.getCalories(), simpleFood.getProtein(), simpleFood.getCarbohydrates(), simpleFood.getTotalFats(), simpleFood.getTransFat(), simpleFood.getMonounsaturatedFat(), simpleFood.getPolyunsaturatedFat(), simpleFood.getSaturatedFat(), simpleFood.getSugars(), simpleFood.getFiber(), simpleFood.getCholesterol(), simpleFood.getIron(), simpleFood.getManganese(), simpleFood.getCopper(), simpleFood.getZinc(), simpleFood.getCalcium(), simpleFood.getMagnesium(), simpleFood.getPhosphorus(), simpleFood.getPotassium(), simpleFood.getSodium(), simpleFood.getSulfur(), simpleFood.getVitaminA(), simpleFood.getVitaminC(), simpleFood.getVitaminK(), simpleFood.getVitaminD(), simpleFood.getVitaminE(), simpleFood.getVitaminB1(), simpleFood.getVitaminB2(), simpleFood.getVitaminB3(), simpleFood.getVitaminB5(), simpleFood.getVitaminB6(), simpleFood.getVitaminB7(), simpleFood.getVitaminB8(), simpleFood.getVitaminB9(), simpleFood.getVitaminB12());
    }

    public static JsonComplexFood toJson(ComplexFood complexFood)
    {
        return new JsonComplexFood(complexFood.getFoodId(), complexFood.getName(), complexFood.getCreatedUser().getUserId(), complexFood.getLastModifiedUser().getUserId(), complexFood.getCreatedDate(), complexFood.getLastModifiedDate());
    }

    public static JsonFoodMetaData metaDataToJson(FoodMetaData foodMetaData)
    {
        return new JsonFoodMetaData(foodMetaData.getFoodId(), foodMetaData.getFoodType().toString());
    }

    public static SimpleFood toEntity(JsonSimpleFood jsonSimpleFood, FoodMetaData foodMetaData, User createdUser, User lastModifiedUser)
    {
        return mapJsonSimpleFoodToEntity(jsonSimpleFood, jsonSimpleFood.getFoodId(), foodMetaData, createdUser, lastModifiedUser);
    }

    /**
     * Returns a new (and detached) SimpleFood instance hydrated from json without a set id.
     * @param jsonSimpleFood json from which to hydrate the new instance
     * @param foodMetaData Associated food metadata
     * @param createdUser User who created the food
     * @param lastModifiedUser Last user to modify the food
     * @return MealPlanMealIngredient instance hydrated from json without a set id
     */
    public static SimpleFood toFreshEntity(JsonSimpleFood jsonSimpleFood, FoodMetaData foodMetaData, User createdUser, User lastModifiedUser)
    {
        return mapJsonSimpleFoodToEntity(jsonSimpleFood, null, foodMetaData, createdUser, lastModifiedUser);
    }

    public static ComplexFood toEntity(JsonComplexFood jsonComplexFood, FoodMetaData foodMetaData, User createdUser, User lastModifiedUser)
    {
        return mapJsonComplexFoodToEntity(jsonComplexFood, jsonComplexFood.getFoodId(), foodMetaData, createdUser, lastModifiedUser);
    }

    /**
     * Returns a new (and detached) SimpleFood instance hydrated from json without a set id.
     * @param jsonComplexFood json from which to hydrate the new instance
     * @param foodMetaData Associated food metadata
     * @param createdUser User who created the food
     * @param lastModifiedUser Last user to modify the food
     * @return MealPlanMealIngredient instance hydrated from json without a set id
     */
    public static ComplexFood toFreshEntity(JsonComplexFood jsonComplexFood, FoodMetaData foodMetaData, User createdUser, User lastModifiedUser)
    {
        return mapJsonComplexFoodToEntity(jsonComplexFood, null, foodMetaData, createdUser, lastModifiedUser);
    }

    public static FoodMetaData metaDataToEntity(JsonFoodMetaData jsonMetaData)
    {
        return new FoodMetaData(jsonMetaData.getFoodId(), FoodType.valueOf(jsonMetaData.getFoodType()));
    }

    public static FoodMetaData metaDataToFreshEntity(JsonFoodMetaData jsonMetaData)
    {
        return new FoodMetaData(null, FoodType.valueOf(jsonMetaData.getFoodType()));
    }

    private static SimpleFood mapJsonSimpleFoodToEntity(JsonSimpleFood jsonSimpleFood, UUID foodId, FoodMetaData foodMetaData, User createdUser, User lastModifiedUser)
    {
        return new SimpleFood(
                foodId,
                foodMetaData,
                jsonSimpleFood.getFoodName(),
                jsonSimpleFood.getBrandName(),
                jsonSimpleFood.getSource(),
                jsonSimpleFood.getCalories(),
                createdUser,
                lastModifiedUser,
                jsonSimpleFood.getCreatedDate(),
                LocalDateTime.now(),
                jsonSimpleFood.getGlycemicIndex(),
                jsonSimpleFood.getProtein(),
                jsonSimpleFood.getCarbohydrates(),
                jsonSimpleFood.getTotalFats(),
                jsonSimpleFood.getTransFat(),
                jsonSimpleFood.getMonounsaturatedFat(),
                jsonSimpleFood.getPolyunsaturatedFat(),
                jsonSimpleFood.getSaturatedFat(),
                jsonSimpleFood.getSugars(),
                jsonSimpleFood.getFiber(),
                jsonSimpleFood.getCholesterol(),
                jsonSimpleFood.getIron(),
                jsonSimpleFood.getManganese(),
                jsonSimpleFood.getCopper(),
                jsonSimpleFood.getZinc(),
                jsonSimpleFood.getCalcium(),
                jsonSimpleFood.getMagnesium(),
                jsonSimpleFood.getPhosphorus(),
                jsonSimpleFood.getPotassium(),
                jsonSimpleFood.getSodium(),
                jsonSimpleFood.getSulfur(),
                jsonSimpleFood.getVitaminA(),
                jsonSimpleFood.getVitaminC(),
                jsonSimpleFood.getVitaminK(),
                jsonSimpleFood.getVitaminD(),
                jsonSimpleFood.getVitaminE(),
                jsonSimpleFood.getVitaminB1(),
                jsonSimpleFood.getVitaminB2(),
                jsonSimpleFood.getVitaminB3(),
                jsonSimpleFood.getVitaminB5(),
                jsonSimpleFood.getVitaminB6(),
                jsonSimpleFood.getVitaminB7(),
                jsonSimpleFood.getVitaminB8(),
                jsonSimpleFood.getVitaminB9(),
                jsonSimpleFood.getVitaminB12()
        );
    }

    private static ComplexFood mapJsonComplexFoodToEntity(JsonComplexFood jsonComplexFood, UUID foodId, FoodMetaData foodMetaData, User createdUser, User lastModifiedUser)
    {
        return new ComplexFood(
                foodId,
                foodMetaData,
                jsonComplexFood.getFoodName(),
                createdUser,
                lastModifiedUser,
                jsonComplexFood.getCreatedDate(),
                LocalDateTime.now()
        );
    }
}
