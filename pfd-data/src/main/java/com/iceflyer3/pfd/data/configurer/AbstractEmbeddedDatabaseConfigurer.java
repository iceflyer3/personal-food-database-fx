/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.configurer;

import com.iceflyer3.pfd.data.AppConfigUtils;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseConfigurer;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public abstract class AbstractEmbeddedDatabaseConfigurer implements EmbeddedDatabaseConfigurer {

    protected final Logger LOG = LoggerFactory.getLogger(AbstractEmbeddedDatabaseConfigurer.class);
    protected AppConfigUtils appConfigUtils;

    public AbstractEmbeddedDatabaseConfigurer(AppConfigUtils appConfigUtils)
    {
        this.appConfigUtils = appConfigUtils;
    }

    @Override
    public void shutdown(DataSource dataSource, String databaseName) {
        LOG.debug("Attempting to shut down the embedded database...");
        try(Connection connection = dataSource.getConnection())
        {
            if (connection != null)
            {
                try(Statement sqlStatement = connection.createStatement())
                {
                    sqlStatement.execute("SHUTDOWN");
                }
            }
        }
        catch (SQLException sqlException)
        {
            LOG.error("An unexpected exception has occurred while attempting to shutdown the embedded database", sqlException);
        }
    }
}
