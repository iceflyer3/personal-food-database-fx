/*
 *  Personal Food Database
 *  Copyright (C) 2023 Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


package com.iceflyer3.pfd.data.factory;

import com.iceflyer3.pfd.data.repository.FoodRepository;
import com.iceflyer3.pfd.data.repository.MealPlanningRepository;
import com.iceflyer3.pfd.data.repository.RecipeRepository;
import com.iceflyer3.pfd.data.request.backup.ExportDataRequest;
import com.iceflyer3.pfd.data.task.data.ExportDataTask;
import com.iceflyer3.pfd.data.task.data.ImportDataTask;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ImportExportDataTaskFactory
{
    private final FoodRepository foodRepository;
    private final RecipeRepository recipeRepository;
    private final MealPlanningRepository mealPlanningRepository;

    public ImportExportDataTaskFactory(FoodRepository foodRepository, RecipeRepository recipeRepository, MealPlanningRepository mealPlanningRepository)
    {
        this.foodRepository = foodRepository;
        this.recipeRepository = recipeRepository;
        this.mealPlanningRepository = mealPlanningRepository;
    }

    public ExportDataTask exportData(ExportDataRequest exportDataRequest)
    {
        return new ExportDataTask(foodRepository, mealPlanningRepository, recipeRepository, exportDataRequest);
    }

    public ImportDataTask importData(List<String> importFilenames)
    {
        return new ImportDataTask(foodRepository, mealPlanningRepository, recipeRepository, importFilenames);
    }
}
