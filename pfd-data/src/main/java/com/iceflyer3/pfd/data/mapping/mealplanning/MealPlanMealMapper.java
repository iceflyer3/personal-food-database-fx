package com.iceflyer3.pfd.data.mapping.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanDay;
import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanMeal;
import com.iceflyer3.pfd.data.json.mealplanning.JsonMealPlanMeal;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanMealViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;

public class MealPlanMealMapper {

    private MealPlanMealMapper() { }

    public static MealPlanMealViewModel toModel(MealPlanMeal entity, NutritionalSummary nutritionalSummary)
    {
        /*
         * The ingredients for the meal view model are not mapped here because it is a bit involved
         * due to needing to look up the actual food for each ingredient. The mappings in the entity
         * only store the FoodMetaData which is insufficient to do the mapping for the ingredients.
         *
         * The nutritional summaries are also calculated via a view in the DB.
         */
        MealPlanMealViewModel viewModel = new MealPlanMealViewModel(entity.getMealId(), nutritionalSummary);
        viewModel.setName(entity.getMealName());
        viewModel.setLastModifiedDate(entity.getLastModifiedDate());

        return viewModel;
    }

    public static JsonMealPlanMeal toJson(MealPlanMeal meal)
    {
        return new JsonMealPlanMeal(
                meal.getMealId(),
                meal.getCarbCyclingMeal() == null ? null : meal.getCarbCyclingMeal().getMealId(),
                meal.getDay().getDayId(),
                meal.getMealName(),
                meal.getLastModifiedDate()
        );
    }

    /**
     * Returns a new (and detached) MealPlanMeal instance hydrated from json without a set id.
     * @param jsonMealPlanMeal json from which to hydrate the new instance
     * @param forMealPlanDay Associated meal plan day
     * @return MealPlanMeal instance hydrated from json without a set id
     */
    public static MealPlanMeal toFreshEntity(JsonMealPlanMeal jsonMealPlanMeal, MealPlanDay forMealPlanDay)
    {
        return new MealPlanMeal(
                null,
                forMealPlanDay,
                jsonMealPlanMeal.mealName(),
                jsonMealPlanMeal.lastModifiedDate()
        );
    }
}
