package com.iceflyer3.pfd.data.task.mealplanning.meal;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanMeal;
import com.iceflyer3.pfd.data.entities.views.MealPlanMealNutritionalSummary;
import com.iceflyer3.pfd.data.mapping.mealplanning.MealPlanMealMapper;
import com.iceflyer3.pfd.data.mapping.summary.NutritionalSummaryMapper;
import com.iceflyer3.pfd.data.repository.MealPlanningRepository;
import com.iceflyer3.pfd.data.request.mealplanning.meal.CreateMealRequest;
import com.iceflyer3.pfd.data.request.mealplanning.meal.ModifyMealRequest;
import com.iceflyer3.pfd.data.task.mealplanning.AbstractMealPlanningTask;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanDayViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanMealViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;

import java.util.Optional;

public class SaveMealForMealPlanTask extends AbstractMealPlanningTask<MealPlanMealViewModel>
{
    private final MealPlanDayViewModel forDay;
    private final MealPlanMealViewModel mealViewModel;

    public SaveMealForMealPlanTask(MealPlanningRepository mealPlanningRepository, MealPlanDayViewModel forDay, MealPlanMealViewModel mealViewModel)
    {
        super(mealPlanningRepository);
        this.forDay = forDay;
        this.mealViewModel = mealViewModel;
    }

    @Override
    protected MealPlanMealViewModel call() throws Exception
    {
        MealPlanMeal mealEntity;
        NutritionalSummary nutritionalSummary = NutritionalSummary.empty();
        // New meal?
        if (mealViewModel.getId() == null)
        {
            CreateMealRequest createMealRequest = new CreateMealRequest(forDay.getId(), mealViewModel.getName());
            mealEntity = mealPlanningRepository.createMealForDay(createMealRequest);
        }
        else
        {
            ModifyMealRequest modifyMealRequest = new ModifyMealRequest(mealViewModel.getId(), mealViewModel.getName());
            mealEntity = mealPlanningRepository.updateMealForDay(modifyMealRequest);

            Optional<MealPlanMealNutritionalSummary> mealNutritionalSummary = mealPlanningRepository.getMealNutritionalSummary(mealEntity.getMealId());

            if (mealNutritionalSummary.isPresent())
            {
                nutritionalSummary = NutritionalSummaryMapper.toModel(mealNutritionalSummary.get());
            }
        }

        return MealPlanMealMapper.toModel(mealEntity, nutritionalSummary);
    }


}
