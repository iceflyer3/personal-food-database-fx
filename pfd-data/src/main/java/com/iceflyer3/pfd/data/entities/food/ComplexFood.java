/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.entities.food;

import com.iceflyer3.pfd.constants.QueryConstants;
import com.iceflyer3.pfd.data.entities.User;

import jakarta.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "complex_food")
@NamedQuery(name = QueryConstants.QUERY_FOOD_FIND_ALL_COMPLEX, query = "SELECT food FROM ComplexFood food")
@NamedQuery(
        name = QueryConstants.QUERY_FOOD_FIND_COMPLEX_FOR_USER,
        query = "SELECT food FROM ComplexFood food WHERE food.createdUser.userId = :userId")
@NamedQuery(
        name = QueryConstants.QUERY_FOOD_FIND_COMPLEX_BY_NAME_FOR_USER,
        query = "SELECT food FROM ComplexFood food WHERE food.name = :foodName AND food.createdUser.userId = :userId")
public class ComplexFood {

    public ComplexFood() { }

    public ComplexFood(String foodName, User createdUser)
    {
        this.foodId = null;
        this.name = foodName;

        this.createdUser = createdUser;
        this.createdDate = LocalDateTime.now();

        this.lastModifiedUser = createdUser;
        this.lastModifiedDate = LocalDateTime.now();
    }

    public ComplexFood(UUID foodId,
                       FoodMetaData foodMetaData,
                       String foodName,
                       User createdUser,
                       User lastModifiedUser,
                       LocalDateTime createdDate,
                       LocalDateTime lastModifiedDate)
    {
        this.foodId = foodId;
        this.foodMetaData = foodMetaData;

        this.name = foodName;

        this.createdUser = createdUser;
        this.createdDate = createdDate;

        this.lastModifiedUser = lastModifiedUser;
        this.lastModifiedDate = lastModifiedDate;
    }

    @Id
    protected UUID foodId;

    // See the comment in the SimpleFood entity about the use of a CascadeType here
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @MapsId
    @JoinColumn(name = "food_id")
    protected FoodMetaData foodMetaData;

    @Column(name = "food_name", nullable = false)
    protected String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "created_user_id", nullable = false)
    protected User createdUser;

    @Column(name="created_date", nullable = false, columnDefinition = "timestamp")
    protected LocalDateTime createdDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "last_modified_user_id", nullable = false)
    protected User lastModifiedUser;

    @Column(name="last_modified_date", nullable = false, columnDefinition = "timestamp")
    protected LocalDateTime lastModifiedDate;

    @OneToMany(mappedBy = "complexFood", fetch = FetchType.LAZY)
    protected Set<ComplexFoodIngredient> ingredients;

    public FoodMetaData getFoodMetaData() {
        return foodMetaData;
    }

    public void setFoodMetaData(FoodMetaData foodMetaData) {
        this.foodMetaData = foodMetaData;
    }

    public UUID getFoodId() {
        return foodId;
    }

    public User getCreatedUser() {
        return createdUser;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getLastModifiedUser() {
        return lastModifiedUser;
    }

    public void setLastModifiedUser(User lastModifiedUser) {
        this.lastModifiedUser = lastModifiedUser;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Set<ComplexFoodIngredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(Set<ComplexFoodIngredient> ingredients) {
        this.ingredients = ingredients;
    }

    @Override
    public String toString() {
        return "ComplexFood{" +
                "foodId=" + foodId +
                ", foodMetaData=" + foodMetaData +
                ", name='" + name + '\'' +
                ", createdUser=" + createdUser +
                ", createdDate=" + createdDate +
                ", lastModifiedUser=" + lastModifiedUser +
                ", lastModifiedDate=" + lastModifiedDate +
                ", ingredients=" + ingredients +
                '}';
    }
}
