/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.task.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.iceflyer3.pfd.data.FoodImportResult;
import com.iceflyer3.pfd.data.json.JsonFoodData;
import com.iceflyer3.pfd.data.json.JsonMealPlanningData;
import com.iceflyer3.pfd.data.json.JsonRecipeData;
import com.iceflyer3.pfd.data.repository.FoodRepository;
import com.iceflyer3.pfd.data.repository.MealPlanningRepository;
import com.iceflyer3.pfd.data.repository.RecipeRepository;
import javafx.concurrent.Task;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class ImportDataTask extends Task<Void>
{
    private final Logger LOG = LoggerFactory.getLogger(ImportDataTask.class);
    private final FoodRepository foodRepository;
    private final MealPlanningRepository mealPlanningRepository;
    private final RecipeRepository recipeRepository;

    private final List<String> importFilenames;

    private final ObjectMapper jacksonMapper;

    public ImportDataTask(FoodRepository foodRepository, MealPlanningRepository mealPlanningRepository, RecipeRepository recipeRepository, List<String> importFilenames)
    {
        this.foodRepository = foodRepository;
        this.mealPlanningRepository = mealPlanningRepository;
        this.recipeRepository = recipeRepository;
        this.importFilenames = importFilenames;
        this.jacksonMapper = new ObjectMapper().registerModule(new JavaTimeModule());
    }

    @Override
    protected Void call() throws Exception
    {
        updateMessage("Preparing to start data import...");
        LOG.debug("Attempting to import data from files: {}", importFilenames);

        // Verify the directory where exported data files are expected to be located exists
        File exportDirectory = new File("../data-export");
        if (!exportDirectory.exists())
        {
            throw new IOException("Could not find the directory from which files should be imported!");
        }

        /*
         * Both the Meal Planning and Recipe sections depend heavily on food data in order to
         * be able to actually accomplish anything.
         *
         * So an import of food data is not optional for the sake of data integrity. Additionally,
         * the food data file is also always the first file to be processed in the import.
         */
        File importFile = new File(exportDirectory, importFilenames.get(0));
        importFilenames.remove(0);

        updateMessage("Importing food data...");
        JsonFoodData jsonFoodData = jacksonMapper.readValue(importFile, JsonFoodData.class);
        FoodImportResult foodImportResult = foodRepository.importData(jsonFoodData);

        for(String filename : importFilenames)
        {
            importFile = new File(exportDirectory, filename);

            if (filename.contains("meal-planning"))
            {
                updateMessage("Importing meal planning data...");
                JsonMealPlanningData jsonMealPlanningData = jacksonMapper.readValue(importFile, JsonMealPlanningData.class);
                mealPlanningRepository.importData(jsonMealPlanningData, foodImportResult);
            }

            if (filename.contains("recipes"))
            {
                updateMessage("Importing recipe data...");
                JsonRecipeData jsonRecipeData = jacksonMapper.readValue(importFile, JsonRecipeData.class);
                recipeRepository.importData(jsonRecipeData, foodImportResult);
            }
        }

        return null;
    }
}
