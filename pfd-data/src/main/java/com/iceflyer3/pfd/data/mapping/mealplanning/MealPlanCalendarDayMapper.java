/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


package com.iceflyer3.pfd.data.mapping.mealplanning;

import com.iceflyer3.pfd.data.entities.User;
import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanCalendarDay;
import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanDay;
import com.iceflyer3.pfd.data.json.mealplanning.JsonMealPlanCalendarDay;
import com.iceflyer3.pfd.enums.WeeklyInterval;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanCalendarDayViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanDayViewModel;

public class MealPlanCalendarDayMapper
{
    private MealPlanCalendarDayMapper() { }

    public static MealPlanCalendarDayViewModel toModel(MealPlanCalendarDay calendarDayEntity, MealPlanDayViewModel associatedPlanDay)
    {
        return new MealPlanCalendarDayViewModel(
                calendarDayEntity.getCalendarDayId(),
                associatedPlanDay,
                calendarDayEntity.getCalendarDate(),
                calendarDayEntity.getWeeklyInterval(),
                calendarDayEntity.getIntervalMonthsDuration(),
                calendarDayEntity.isRecurrence()
        );
    }

    public static JsonMealPlanCalendarDay toJson(MealPlanCalendarDay calendarDay)
    {
        return new JsonMealPlanCalendarDay(
                calendarDay.getCalendarDayId(),
                calendarDay.getPlannedDay().getDayId(),
                calendarDay.getPlannedDay().getMealPlan().getUser().getUserId(),
                calendarDay.getCalendarDate(),
                calendarDay.getWeeklyInterval().toString(),
                calendarDay.getIntervalMonthsDuration(),
                calendarDay.isRecurrence()
        );
    }

    public static MealPlanCalendarDay toFreshEntity(JsonMealPlanCalendarDay jsonMealPlanCalendarDay, MealPlanDay plannedDay, User forUser)
    {
        return new MealPlanCalendarDay(
                null,
                plannedDay,
                forUser,
                jsonMealPlanCalendarDay.calendarDate(),
                WeeklyInterval.valueOf(jsonMealPlanCalendarDay.weeklyInterval()),
                jsonMealPlanCalendarDay.intervalMonthsDuration(),
                jsonMealPlanCalendarDay.recurrence()
        );
    }
}
