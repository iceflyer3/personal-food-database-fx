package com.iceflyer3.pfd.data.repository;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.FoodImportResult;
import com.iceflyer3.pfd.data.entities.mealplanning.*;
import com.iceflyer3.pfd.data.entities.mealplanning.carbcycling.CarbCyclingMealConfiguration;
import com.iceflyer3.pfd.data.entities.mealplanning.carbcycling.CarbCyclingPlan;
import com.iceflyer3.pfd.data.entities.views.CarbCyclingDayNutritionalSummary;
import com.iceflyer3.pfd.data.entities.views.CarbCyclingMealNutritionalSummary;
import com.iceflyer3.pfd.data.entities.views.MealPlanDayNutritionalSummary;
import com.iceflyer3.pfd.data.entities.views.MealPlanMealNutritionalSummary;
import com.iceflyer3.pfd.data.json.JsonMealPlanningData;
import com.iceflyer3.pfd.data.request.mealplanning.*;
import com.iceflyer3.pfd.data.request.mealplanning.day.CreateCarbCyclingDayRequest;
import com.iceflyer3.pfd.data.request.mealplanning.day.CreateDayRequest;
import com.iceflyer3.pfd.data.request.mealplanning.day.ModifyDayRequest;
import com.iceflyer3.pfd.data.request.mealplanning.meal.CreateCarbCyclingMealRequest;
import com.iceflyer3.pfd.data.request.mealplanning.meal.CreateMealRequest;
import com.iceflyer3.pfd.data.request.mealplanning.meal.ModifyMealRequest;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface MealPlanningRepository {
    // Meal plan functions
    MealPlan getMealPlanForId(UUID planId);
    Optional<MealPlan> getActiveMealPlanForUser(UUID userId);
    List<MealPlan> getMealPlansForUser(UUID userId);

    MealPlan createMealPlan(CreateMealPlanRequest planCreationRequest);

    MealPlan updateMealPlanActiveStatus(ChangeMealPlanActivationStatusRequest request);

    void deleteMealPlan(UUID planId);

    // Meal Plan Day functions
    MealPlanDay getDayForId(UUID dayId);
    List<MealPlanDay> getDaysForMealPlan(UUID mealPlanId);
    Optional<MealPlanDayNutritionalSummary> getDayNutritionalSummary(UUID dayId);
    List<MealPlanDayNutritionalSummary> getDayNutritionalSummariesForMealPlan(UUID mealPlanId);

    MealPlanDay getCarbCyclingDayForId(UUID dayId);
    List<MealPlanDay> getCarbCyclingDaysForPlan(UUID carbCyclingPlanId);

    Optional<CarbCyclingDayNutritionalSummary> getCarbCyclingDayNutritionalSummary(UUID dayId);
    List<CarbCyclingDayNutritionalSummary> getCarbCyclingDayNutritionalSummariesForPlan(UUID carbCyclingPlanId);

    MealPlanDay createDayForMealPlan(CreateDayRequest dayCreationRequest);
    MealPlanDay createDayForMealPlan(CreateCarbCyclingDayRequest dayCreationRequest);
    MealPlanDay updateDayForMealPlan(ModifyDayRequest modifyDayRequest);

    void deleteDay(UUID dayId);
    void deleteDaysForMealPlan(UUID mealId);

    // Meal Plan Meal functions
    List<MealPlanMeal> getMealsForDay(UUID dayId);
    Optional<MealPlanMealNutritionalSummary> getMealNutritionalSummary(UUID mealId);
    List<MealPlanMealNutritionalSummary> getMealNutritionalSummariesForDay(UUID dayId);

    MealPlanMeal getCarbCyclingMealById(UUID mealId);
    List<MealPlanMeal> getCarbCyclingMealsForDay(UUID dayId);

    Optional<CarbCyclingMealNutritionalSummary> getCarbCyclingMealNutritionalSummary(UUID mealId);
    List<CarbCyclingMealNutritionalSummary> getCarbCyclingMealNutritionalSummariesForDay(UUID dayId);

    MealPlanMeal getCarbCyclingMealForDay(UUID dayId, int mealNumber);
    MealPlanMeal getMealForId(UUID mealId);

    MealPlanMeal createMealForDay(CreateMealRequest mealCreationRequest);
    MealPlanMeal createMealForDay(CreateCarbCyclingMealRequest mealCreationRequest);
    MealPlanMeal updateMealForDay(ModifyMealRequest modifyMealRequest);

    void deleteMeal(UUID mealId);
    void deleteMealsForDay(UUID dayId);

    // Meal Plan Meal Ingredient functions
    List<MealPlanMealIngredient> getIngredientsForMeal(UUID mealId);
    List<MealPlanMealIngredient> updateIngredientsForMeal(ModifyMealIngredientsRequest modifyMealIngredientsRequest);

    void deleteIngredientsForMeal(UUID mealId);

    // Carb cycling plan functions
    CarbCyclingPlan getCarbCyclingPlanForId(UUID planId);
    List<CarbCyclingPlan> getCarbCyclingPlansForMealPlan(UUID mealPlanId);
    List<CarbCyclingMealConfiguration> getMealConfigurationsForCarbCyclingPlan(UUID carbCyclingPlanId);

    CarbCyclingPlan createCarbCyclingPlan(CreateCarbCyclingPlanRequest creationRequest);

    CarbCyclingPlan toggleCarbCyclingPlanStatus(UUID planId);

    void deleteCarbCyclingPlan(UUID planId);

    // Meal Plan Calendar Day functions
    List<MealPlanCalendarDay> createCalendarDays(AssociateMealPlanDayToCalendarDayRequest request);
    void deleteMealPlanCalendarDay(UUID calendarDayId);
    List<MealPlanCalendarDay> getMealPlanCalendarDaysForUser(UUID userId);
    List<MealPlanCalendarDay> getMealPlanCalendarDaysInRange(LocalDate startDate, LocalDate endDate);

    JsonMealPlanningData exportData();
    void importData(JsonMealPlanningData jsonMealPlanningData, FoodImportResult importedFoods);
}
