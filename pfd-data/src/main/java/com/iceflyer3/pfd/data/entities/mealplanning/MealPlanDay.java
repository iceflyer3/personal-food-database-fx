/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.entities.mealplanning;

import com.iceflyer3.pfd.constants.QueryConstants;
import com.iceflyer3.pfd.data.entities.mealplanning.carbcycling.CarbCyclingDay;
import com.iceflyer3.pfd.enums.FitnessGoal;

import jakarta.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "meal_planning_day")
@NamedQuery(
        name = QueryConstants.QUERY_MEAL_PLANNING_FIND_ALL_DAYS_FOR_USER,
        query = """
                SELECT mealPlanDay
                FROM MealPlanDay mealPlanDay
                LEFT JOIN mealPlanDay.carbCyclingDay carbCyclingDay
                WHERE mealPlanDay.mealPlan.user.userId = :userId
                """)
@NamedQuery(
        name = QueryConstants.QUERY_MEAL_PLANNING_FIND_DAYS_FOR_MEAL_PLAN,
        query = """
                SELECT mealPlanDay
                FROM MealPlanDay mealPlanDay
                LEFT JOIN mealPlanDay.carbCyclingDay carbCyclingDay
                WHERE mealPlanDay.mealPlan.mealPlanId = :mealPlanId AND carbCyclingDay IS NULL
                """)
@NamedNativeQuery(name = QueryConstants.QUERY_MEAL_PLANNING_REMOVE_DAYS_FOR_MEAL_PLAN, query = QueryConstants.NATIVE_DELETE_DAYS_FOR_MEAL_PLAN)
public class MealPlanDay
{

    public MealPlanDay() { }

    public MealPlanDay(String dayLabel,
                       FitnessGoal fitnessGoal)
    {
        this.dayId = null;
        this.dayLabel = dayLabel;
        this.dayFitnessGoal = fitnessGoal;
        this.lastModifiedDate = LocalDateTime.now();
    }

    public MealPlanDay(
            UUID dayId,
            MealPlan forMealPlan,
            FitnessGoal fitnessGoal,
            String dayLabel,
            LocalDateTime lastModifiedDate)
    {
        this(
                dayId,
                forMealPlan,
                null,
                fitnessGoal,
                dayLabel,
                lastModifiedDate
        );
    }

    public MealPlanDay(
            UUID dayId,
            MealPlan forMealPlan,
            CarbCyclingDay associatedCarbCyclingDay,
            FitnessGoal fitnessGoal,
            String dayLabel,
            LocalDateTime lastModifiedDate)
    {
        this.dayId = dayId;
        this.mealPlan = forMealPlan;
        this.carbCyclingDay = associatedCarbCyclingDay;
        this.dayFitnessGoal = fitnessGoal;
        this.dayLabel = dayLabel;
        this.lastModifiedDate = lastModifiedDate;
    }

    @Id
    @GeneratedValue
    @Column(name = "day_id")
    protected UUID dayId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "meal_plan_id", nullable = false)
    protected MealPlan mealPlan;

    @OneToOne(mappedBy = "mealPlanDay", fetch = FetchType.EAGER)
    protected CarbCyclingDay carbCyclingDay;

    @Enumerated(EnumType.STRING)
    @Column(name = "day_fitness_goal", nullable = false)
    protected FitnessGoal dayFitnessGoal;

    @Column(name = "day_label", nullable = false)
    protected String dayLabel;

    //TODO: Perhaps we should see about adding a created date column here too for consistency sake
    @Column(name = "last_modified_date", nullable = false, columnDefinition = "timestamp")
    protected LocalDateTime lastModifiedDate;

    public UUID getDayId() {
        return dayId;
    }

    public MealPlan getMealPlan() {
        return mealPlan;
    }

    public void setMealPlan(MealPlan mealPlan)
    {
        this.mealPlan = mealPlan;
    }

    public CarbCyclingDay getCarbCyclingDay()
    {
        return carbCyclingDay;
    }

    public void setCarbCyclingDay(CarbCyclingDay carbCyclingDay)
    {
        this.carbCyclingDay = carbCyclingDay;
    }

    public FitnessGoal getDayFitnessGoal() {
        return dayFitnessGoal;
    }

    public String getDayLabel() {
        return dayLabel;
    }

    public void setDayLabel(String dayLabel) {
        this.dayLabel = dayLabel;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @Override
    public String toString()
    {
        return "MealPlanningDay{"
                + "dayId=" + dayId
                + ", carbCyclingDay=" + carbCyclingDay
                + ", dayFitnessGoal=" + dayFitnessGoal
                + ", dayLabel='" + dayLabel
                + ", lastModifiedDate=" + lastModifiedDate + '}';
    }
}
