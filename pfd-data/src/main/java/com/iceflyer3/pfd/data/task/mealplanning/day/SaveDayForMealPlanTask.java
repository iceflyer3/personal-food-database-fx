package com.iceflyer3.pfd.data.task.mealplanning.day;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanDay;
import com.iceflyer3.pfd.data.entities.views.MealPlanDayNutritionalSummary;
import com.iceflyer3.pfd.data.mapping.mealplanning.MealPlanDayMapper;
import com.iceflyer3.pfd.data.mapping.summary.NutritionalSummaryMapper;
import com.iceflyer3.pfd.data.repository.MealPlanningRepository;
import com.iceflyer3.pfd.data.request.mealplanning.day.CreateDayRequest;
import com.iceflyer3.pfd.data.request.mealplanning.day.ModifyDayRequest;
import com.iceflyer3.pfd.data.task.mealplanning.AbstractMealPlanningTask;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanDayViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;

import java.util.Optional;


/**
 * Creates a new or updates an existing Meal Plan Day or Carb Cycling Day depending on if
 * the day already exists and if it is attached to a carb cycling plan or not.
 */
public class SaveDayForMealPlanTask extends AbstractMealPlanningTask<MealPlanDayViewModel>
{
    private final MealPlanDayViewModel dayViewModel;

    /**
     * Initializes a new task for saving or updating a day.
     * @param mealPlanningRepository The repository used to perform needed database tasks
     * @param dayViewModel The view model that contains the details of the day to save
     */
    public SaveDayForMealPlanTask(MealPlanningRepository mealPlanningRepository,
                                  MealPlanDayViewModel dayViewModel)
    {
        super(mealPlanningRepository);
        this.dayViewModel = dayViewModel;
    }

    @Override
    protected MealPlanDayViewModel call() throws Exception
    {
        MealPlanDay dayEntity;

        // Days will not have any meals upon initial creation so no summary will be found for new days
        // when searched for
        NutritionalSummary nutritionalSummary = NutritionalSummary.empty();

        if (dayViewModel.getId() == null)
        {
            CreateDayRequest createDayRequest = new CreateDayRequest(dayViewModel.getOwningMealPlanId(), dayViewModel);
            dayEntity = mealPlanningRepository.createDayForMealPlan(createDayRequest);
        }
        else
        {
            ModifyDayRequest modifyDayRequest = new ModifyDayRequest(dayViewModel.getId(), dayViewModel.getDayLabel());
            dayEntity = mealPlanningRepository.updateDayForMealPlan(modifyDayRequest);

            Optional<MealPlanDayNutritionalSummary> summaryEntity = mealPlanningRepository.getDayNutritionalSummary(dayEntity.getDayId());

            if (summaryEntity.isPresent())
            {
                nutritionalSummary = NutritionalSummaryMapper.toModel(summaryEntity.get());
            }
            else
            {
                nutritionalSummary = NutritionalSummary.empty();
            }
        }

        return MealPlanDayMapper.toModel(dayEntity, nutritionalSummary);
    }
}
