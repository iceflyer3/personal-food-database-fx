package com.iceflyer3.pfd.data.task.food;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.entities.food.ComplexFood;
import com.iceflyer3.pfd.data.entities.views.ComplexFoodNutritionalSummary;
import com.iceflyer3.pfd.data.mapping.food.FoodMapper;
import com.iceflyer3.pfd.data.mapping.summary.NutritionalSummaryMapper;
import com.iceflyer3.pfd.data.repository.FoodRepository;
import com.iceflyer3.pfd.data.request.food.SaveComplexFoodRequest;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.ComplexFoodViewModel;

public class SaveComplexFoodTask extends AbstractFoodTask<ComplexFoodViewModel>{

    private final UserViewModel createdUser;
    private final ComplexFoodViewModel foodToSave;

    public SaveComplexFoodTask(FoodRepository foodRepository, UserViewModel createdUser, ComplexFoodViewModel complexFoodToSave)
    {
        super(foodRepository);
        this.createdUser = createdUser;
        this.foodToSave = complexFoodToSave;
    }

    @Override
    protected ComplexFoodViewModel call() throws Exception {
        SaveComplexFoodRequest saveRequest = new SaveComplexFoodRequest(createdUser, foodToSave);

        ComplexFood complexFoodEntity;
        if (foodToSave.getId() == null)
        {
            complexFoodEntity = foodRepository.createComplexFood(saveRequest);
        }
        else
        {
            complexFoodEntity = foodRepository.updateComplexFood(saveRequest);
        }

        ComplexFoodNutritionalSummary nutritionalSummaryEntity = foodRepository.getComplexFoodNutritionSummary(complexFoodEntity.getFoodId());
        return FoodMapper.toMutableModel(complexFoodEntity, NutritionalSummaryMapper.toModel(nutritionalSummaryEntity));
    }
}
