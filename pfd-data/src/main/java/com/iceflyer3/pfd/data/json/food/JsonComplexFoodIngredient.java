/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.json.food;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.UUID;

public class JsonComplexFoodIngredient
{
    private UUID ingredientId;
    private UUID simpleFoodId;
    private String simpleFoodName;
    private String simpleFoodBrand;
    private UUID complexFoodId;
    private BigDecimal simpleFoodServingsIncluded;
    private boolean isAlternate;
    private boolean isOptional;

    @JsonCreator
    public JsonComplexFoodIngredient(
            @JsonProperty("ingredientId") UUID ingredientId,
            @JsonProperty("simpleFoodId") UUID simpleFoodId,
            @JsonProperty("simpleFoodName") String simpleFoodName,
            @JsonProperty("simpleFoodBrand") String simpleFoodBrand,
            @JsonProperty("complexFoodId") UUID complexFoodId,
            @JsonProperty("simpleFoodServingsIncluded") BigDecimal simpleFoodServingsIncluded,
            @JsonProperty("isAlternate") boolean isAlternate,
            @JsonProperty("isOptional") boolean isOptional)
    {
        this.ingredientId = ingredientId;
        this.simpleFoodId = simpleFoodId;
        this.simpleFoodName = simpleFoodName;
        this.simpleFoodBrand = simpleFoodBrand;
        this.complexFoodId = complexFoodId;
        this.simpleFoodServingsIncluded = simpleFoodServingsIncluded;
        this.isAlternate = isAlternate;
        this.isOptional = isOptional;
    }

    public UUID getIngredientId()
    {
        return ingredientId;
    }

    public void setIngredientId(UUID ingredientId)
    {
        this.ingredientId = ingredientId;
    }

    public UUID getSimpleFoodId()
    {
        return simpleFoodId;
    }

    public void setSimpleFoodId(UUID simpleFoodId)
    {
        this.simpleFoodId = simpleFoodId;
    }

    public String getSimpleFoodName()
    {
        return simpleFoodName;
    }

    public void setSimpleFoodName(String simpleFoodName)
    {
        this.simpleFoodName = simpleFoodName;
    }

    public String getSimpleFoodBrand()
    {
        return simpleFoodBrand;
    }

    public void setSimpleFoodBrand(String simpleFoodBrand)
    {
        this.simpleFoodBrand = simpleFoodBrand;
    }

    public UUID getComplexFoodId()
    {
        return complexFoodId;
    }

    public void setComplexFoodId(UUID complexFoodId)
    {
        this.complexFoodId = complexFoodId;
    }

    public BigDecimal getSimpleFoodServingsIncluded()
    {
        return simpleFoodServingsIncluded;
    }

    public void setSimpleFoodServingsIncluded(BigDecimal simpleFoodServingsIncluded)
    {
        this.simpleFoodServingsIncluded = simpleFoodServingsIncluded;
    }

    public boolean isAlternate()
    {
        return isAlternate;
    }

    public void setAlternate(boolean alternate)
    {
        isAlternate = alternate;
    }

    public boolean isOptional()
    {
        return isOptional;
    }

    public void setOptional(boolean optional)
    {
        isOptional = optional;
    }

    @Override
    public String toString()
    {
        return "JsonComplexFoodIngredient{" + "ingredientId=" + ingredientId + ", simpleFoodId=" + simpleFoodId + ", simpleFoodName='" + simpleFoodName + '\'' + ", simpleFoodBrand='" + simpleFoodBrand + '\'' + ", complexFoodId=" + complexFoodId + ", simpleFoodServingsIncluded=" + simpleFoodServingsIncluded + ", isAlternate=" + isAlternate + ", isOptional=" + isOptional + '}';
    }
}