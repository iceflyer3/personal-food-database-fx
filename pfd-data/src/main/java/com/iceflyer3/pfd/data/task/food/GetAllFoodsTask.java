package com.iceflyer3.pfd.data.task.food;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.entities.food.ComplexFood;
import com.iceflyer3.pfd.data.entities.food.SimpleFood;
import com.iceflyer3.pfd.data.entities.views.ComplexFoodNutritionalSummary;
import com.iceflyer3.pfd.data.mapping.food.FoodMapper;
import com.iceflyer3.pfd.data.mapping.summary.NutritionalSummaryMapper;
import com.iceflyer3.pfd.data.repository.FoodRepository;
import com.iceflyer3.pfd.ui.model.viewmodel.food.ComplexFoodViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.ReadOnlyFoodViewModel;

import java.util.List;
import java.util.stream.Collectors;

public class GetAllFoodsTask extends AbstractFoodTask<List<ReadOnlyFoodViewModel>>{

    public GetAllFoodsTask(FoodRepository foodRepository)
    {
        super(foodRepository);
    }

    @Override
    protected List<ReadOnlyFoodViewModel> call() throws Exception {

        List<SimpleFood> simpleFoods = foodRepository.getAllSimpleFoods();
        List<ComplexFood> complexFoods = foodRepository.getAllComplexFoods();

        List<ReadOnlyFoodViewModel> allFoods = simpleFoods.stream().map(FoodMapper::toModel).collect(Collectors.toList());

        // For complex foods we also have to load the nutrition data from the view that calculates it
        for (ComplexFood complexFoodEntity : complexFoods)
        {
            ComplexFoodNutritionalSummary nutritionalSummaryEntity = foodRepository.getComplexFoodNutritionSummary(complexFoodEntity.getFoodId());
            ReadOnlyFoodViewModel complexFoodViewModel = FoodMapper.toModel(complexFoodEntity, NutritionalSummaryMapper.toModel(nutritionalSummaryEntity));
            allFoods.add(complexFoodViewModel);
        }

        return allFoods;
    }
}
