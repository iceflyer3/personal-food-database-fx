package com.iceflyer3.pfd.data.task.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.entities.mealplanning.MealPlan;
import com.iceflyer3.pfd.data.mapping.mealplanning.MealPlanMapper;
import com.iceflyer3.pfd.data.repository.MealPlanningRepository;
import com.iceflyer3.pfd.data.request.mealplanning.CreateMealPlanRequest;
import com.iceflyer3.pfd.ui.model.api.mealplanning.registration.MealPlanningRegistration;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanViewModel;

import java.util.UUID;

public class SaveMealPlanTask extends AbstractMealPlanningTask<MealPlanViewModel> {

    private final UUID forUserId;
    private final MealPlanningRegistration registration;

    public SaveMealPlanTask(MealPlanningRepository mealPlanningRepository, UUID forUserId, MealPlanningRegistration registration) {
        super(mealPlanningRepository);
        this.forUserId = forUserId;
        this.registration = registration;
    }

    @Override
    protected MealPlanViewModel call() throws Exception
    {
        CreateMealPlanRequest creationRequest = new CreateMealPlanRequest(forUserId, registration);
        MealPlan newPlan = mealPlanningRepository.createMealPlan(creationRequest);
        return MealPlanMapper.toModel(newPlan);
    }
}
