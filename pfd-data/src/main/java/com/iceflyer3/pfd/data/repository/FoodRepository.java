package com.iceflyer3.pfd.data.repository;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.FoodImportResult;
import com.iceflyer3.pfd.data.entities.food.ComplexFood;
import com.iceflyer3.pfd.data.entities.food.ComplexFoodIngredient;
import com.iceflyer3.pfd.data.entities.food.SimpleFood;
import com.iceflyer3.pfd.data.entities.views.ComplexFoodNutritionalSummary;
import com.iceflyer3.pfd.data.entities.views.FoodAsIngredient;
import com.iceflyer3.pfd.data.json.JsonFoodData;
import com.iceflyer3.pfd.data.request.food.SaveComplexFoodRequest;
import com.iceflyer3.pfd.data.request.food.SaveSimpleFoodRequest;
import com.iceflyer3.pfd.ui.model.api.food.ComplexFoodModel;
import com.iceflyer3.pfd.ui.model.api.food.SimpleFoodModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.ReadOnlyFoodViewModel;

import java.util.List;
import java.util.UUID;

public interface FoodRepository {
    ReadOnlyFoodViewModel getFoodForId(UUID foodId);

    SimpleFood getSimpleFoodById(UUID foodId);
    List<SimpleFood> getAllSimpleFoods();

    ComplexFood getComplexFoodById(UUID foodId);
    List<ComplexFood> getAllComplexFoods();
    List<ComplexFood> findComplexFoodsWithNameForUser(String name);
    List<ComplexFoodIngredient> getIngredientsForComplexFood(UUID foodId);
    ComplexFoodNutritionalSummary getComplexFoodNutritionSummary(UUID foodId);

    SimpleFood createSimpleFood(SaveSimpleFoodRequest saveSimpleFoodRequest);
    SimpleFood updateSimpleFood(SaveSimpleFoodRequest saveSimpleFoodRequest);
    boolean isSimpleFoodUnique(SimpleFoodModel simpleFoodModel);
    ComplexFood createComplexFood(SaveComplexFoodRequest saveComplexFoodRequest);
    ComplexFood updateComplexFood(SaveComplexFoodRequest saveComplexFoodRequest);
    boolean isComplexFoodUnique(ComplexFoodModel complexFoodModel);

    void deleteFood(UUID foodId);

    List<FoodAsIngredient> findFoodAsIngredient(UUID simpleFoodID);

    JsonFoodData exportData();
    FoodImportResult importData(JsonFoodData jsonFoodData);
}
