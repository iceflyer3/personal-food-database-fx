/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.request.mealplanning;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;

import java.time.LocalDate;

/**
 * Contains the parameters needed to perform a search for the list of raw ingredients (simple foods)
 * for all planned days that have been associated to calendar days within a date range.
 */
public class GroceryListForMealPlanCalendarDateRangeSearchRequest
{
    private final BooleanProperty includeAlternateIngredients;
    private final BooleanProperty includeOptionalIngredients;
    private final ObjectProperty<LocalDate> startDate;
    private final ObjectProperty<LocalDate> endDate;

    public GroceryListForMealPlanCalendarDateRangeSearchRequest()
    {
        includeAlternateIngredients = new SimpleBooleanProperty(false);
        includeOptionalIngredients = new SimpleBooleanProperty(false);
        startDate = new SimpleObjectProperty<>(LocalDate.now());
        endDate = new SimpleObjectProperty<>(LocalDate.now());
    }

    public boolean shouldIncludeAlternateIngredients()
    {
        return includeAlternateIngredients.get();
    }

    public BooleanProperty includeAlternateIngredientsProperty()
    {
        return includeAlternateIngredients;
    }

    public void setIncludeAlternateIngredients(boolean includeAlternateIngredients)
    {
        this.includeAlternateIngredients.set(includeAlternateIngredients);
    }

    public boolean shouldIncludeOptionalIngredients()
    {
        return includeOptionalIngredients.get();
    }

    public BooleanProperty includeOptionalIngredientsProperty()
    {
        return includeOptionalIngredients;
    }

    public void setIncludeOptionalIngredients(boolean includeOptionalIngredients)
    {
        this.includeOptionalIngredients.set(includeOptionalIngredients);
    }

    public LocalDate getStartDate()
    {
        return startDate.get();
    }

    public ObjectProperty<LocalDate> startDateProperty()
    {
        return startDate;
    }

    public void setStartDate(LocalDate startDate)
    {
        this.startDate.set(startDate);
    }

    public LocalDate getEndDate()
    {
        return endDate.get();
    }

    public ObjectProperty<LocalDate> endDateProperty()
    {
        return endDate;
    }

    public void setEndDate(LocalDate endDate)
    {
        this.endDate.set(endDate);
    }
}
