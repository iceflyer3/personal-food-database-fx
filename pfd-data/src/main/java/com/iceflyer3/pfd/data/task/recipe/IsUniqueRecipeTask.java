/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.task.recipe;

import com.iceflyer3.pfd.data.repository.FoodRepository;
import com.iceflyer3.pfd.data.repository.RecipeRepository;
import com.iceflyer3.pfd.ui.model.api.recipe.RecipeModel;

public class IsUniqueRecipeTask extends AbstractRecipeTask<Boolean>
{
    private final RecipeModel recipe;

    public IsUniqueRecipeTask(RecipeRepository recipeRepository, FoodRepository foodRepository, RecipeModel recipe)
    {
        super(recipeRepository, foodRepository);
        this.recipe = recipe;
    }

    @Override
    protected Boolean call() throws Exception
    {
        return recipeRepository.isRecipeUnique(recipe);
    }
}
