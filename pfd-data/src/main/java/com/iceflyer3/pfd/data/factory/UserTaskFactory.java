/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.factory;

import com.iceflyer3.pfd.data.repository.UserRepository;
import com.iceflyer3.pfd.data.task.user.AddUserTask;
import com.iceflyer3.pfd.data.task.user.GetAllUsersTask;
import com.iceflyer3.pfd.data.task.user.GetUserByUsernameTask;

import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;

import org.springframework.stereotype.Service;


@Service
public class UserTaskFactory
{

    private final UserRepository userRepository;

    /* TODO: As a general note for the task factories we might want to investigate adding some
     *       kind of delay after which the task fails or providing some kind of option to the user
     *       to cancel a given task if it exceeds a certain threshold.
     *
     *       I've not given any thought as to how to do this.
     *
     *       This application is the only one that uses the db because it is embedded so
     *       theoretically query timeouts should never be a thing in the first place. But I
     *       generally believe in covering all of your bases and this is a (admittedly low
     *       priority) one that we should probably cover eventually.
     */

    public UserTaskFactory(UserRepository userRepository)
    {
        this.userRepository = userRepository;
    }

    public AddUserTask addAsync(UserViewModel newUser)
    {
        return new AddUserTask(userRepository, newUser);
    }

    public GetAllUsersTask getAllAsync()
    {
        return new GetAllUsersTask(userRepository);
    }

    public GetUserByUsernameTask getByUsernameAsync(String username)
    {
        return new GetUserByUsernameTask(userRepository, username);
    }
}
