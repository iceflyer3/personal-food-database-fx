/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Representations of entity objects for the purposes of serialization / deserialization to and from
 * JSON.
 *
 * Classes in this package are POJOs that are used for the database import / export functionality
 * allowing precise control over the shape of the data to be serialized while also allowing the
 * avoidance of bloating the entity classes with various annotations that are serialization centric
 * and not data centric or having to bother with writing custom serializers / deserializers.
 *
 * These classes are written from the point of view of the database which means that, as opposed
 * to proper entity classes that represent relationships via fields of other entity types, these
 * classes represent relationships as the database does (e.g. containing FK ids from other objects
 * instead of an instance of the related object itself)
 */

package com.iceflyer3.pfd.data.json;