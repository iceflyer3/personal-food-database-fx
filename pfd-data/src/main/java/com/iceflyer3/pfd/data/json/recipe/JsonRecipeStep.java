/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.json.recipe;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class JsonRecipeStep
{
    private UUID stepId;
    private UUID forRecipeId;
    private String stepName;
    private String directions;
    private int stepNumber;

    @JsonCreator
    public JsonRecipeStep(
            @JsonProperty("stepId") UUID stepId,
            @JsonProperty("forRecipeId") UUID forRecipeId,
            @JsonProperty("stepName") String stepName,
            @JsonProperty("directions") String directions,
            @JsonProperty("stepNumber") int stepNumber)
    {
        this.stepId = stepId;
        this.forRecipeId = forRecipeId;
        this.stepName = stepName;
        this.directions = directions;
        this.stepNumber = stepNumber;
    }

    public UUID getStepId()
    {
        return stepId;
    }

    public void setStepId(UUID stepId)
    {
        this.stepId = stepId;
    }

    public UUID getForRecipeId()
    {
        return forRecipeId;
    }

    public void setForRecipeId(UUID forRecipeId)
    {
        this.forRecipeId = forRecipeId;
    }

    public String getStepName()
    {
        return stepName;
    }

    public void setStepName(String stepName)
    {
        this.stepName = stepName;
    }

    public String getDirections()
    {
        return directions;
    }

    public void setDirections(String directions)
    {
        this.directions = directions;
    }

    public int getStepNumber()
    {
        return stepNumber;
    }

    public void setStepNumber(int stepNumber)
    {
        this.stepNumber = stepNumber;
    }

    @Override
    public String toString()
    {
        return "JsonRecipeStep{" + "stepId=" + stepId + ", forRecipeId=" + forRecipeId + ", stepName='" + stepName + '\'' + ", directions='" + directions + '\'' + ", stepNumber=" + stepNumber + '}';
    }
}
