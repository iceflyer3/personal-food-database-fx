/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * The request classes serve to bridge the disconnect between the database representation of the data and the
 * view model representation of the data when creating and updating entities.
 *
 * These objects are used by the various Tasks to issue commands to the Repositories to perform some work
 * on the database.
 *
 * For example, consider the creation of new foods. In the UI these all consist of FoodViewModel but to the
 * database they all consist of records in at least two different tables.
 *
 * In the case of Simple Foods these tables are SimpleFood and FoodMetaData. In the case of Complex Foods
 * there are even more tables involved. ComplexFood, ComplexFoodIngredient, and FoodMetaData. The number
 * of database tables involved grows even further by the time you start creating meal plans and carb cycling
 * plans or recipes.
 *
 * The request objects are used to take the user input data that has been collected via the view model and
 * then allow a Task to transform it into a state that the Repositories can use to accomplish something like
 * "save a food", "add an ingredient to a food", or "create a meal plan".
 *
 * Often times this involves directly wrapping a view model object (because they are the primary means by which
 * user input is collected) and then pairing with some data that the database / persistence layer cares about but
 * the view model / UI layer does not.
 */

package com.iceflyer3.pfd.data.request;