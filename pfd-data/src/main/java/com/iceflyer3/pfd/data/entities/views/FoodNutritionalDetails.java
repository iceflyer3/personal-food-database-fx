/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.entities.views;

import com.iceflyer3.pfd.constants.QueryConstants;
import com.iceflyer3.pfd.data.entities.AbstractNutritionalDetails;
import jakarta.persistence.*;
import org.hibernate.annotations.Immutable;

import java.util.UUID;

/**
 * Convenience class mapped to a database view for querying of food nutritional information
 *
 * Mostly intended for the use cases where there is a need to look up the nutritional
 * information in bulk. The JPA EntityManager can handle single food lookups itself
 * but provides no facility for finding more than a single entity at a time.
 */
@Entity
@Immutable
@Table(name = "all_food_nutritional_details")
@NamedQuery(name = QueryConstants.QUERY_FOOD_FIND_ALL_WITH_IDS, query = "SELECT food FROM FoodNutritionalDetails food WHERE food.foodId IN (:foodIds)")
public class FoodNutritionalDetails extends AbstractNutritionalDetails
{
    @Id
    @Column(name = "food_id")
    private UUID foodId;

    public UUID getFoodId()
    {
        return foodId;
    }
}
