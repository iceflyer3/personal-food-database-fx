package com.iceflyer3.pfd.data.request.food;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.SimpleFoodViewModel;

public class SaveSimpleFoodRequest
{

    private final UserViewModel createdUser;
    private final SimpleFoodViewModel foodDetails;

    public SaveSimpleFoodRequest(UserViewModel createdUser, SimpleFoodViewModel foodDetails) {
        this.createdUser = createdUser;
        this.foodDetails = foodDetails;
    }

    public UserViewModel getCreatedUser() {
        return createdUser;
    }

    public SimpleFoodViewModel getFoodDetails() {
        return foodDetails;
    }
}
