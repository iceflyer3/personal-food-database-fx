/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.calculator.mealplanning;

import com.iceflyer3.pfd.enums.Nutrient;
import com.iceflyer3.pfd.util.NumberUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;

/**
 * Base class for Meal Calculators.
 *
 * Contains common functionality needed by all implementations.
 */
public abstract class AbstractMealPlanCalculator
{
        protected Map<Nutrient, BigDecimal> calculateMacronutrientGramsFromCalories(BigDecimal proteinCals, BigDecimal carbCals, BigDecimal fatCals)
        {
            BigDecimal calsPerGramOfProteinAndCarb = BigDecimal.valueOf(4.0);
            BigDecimal calsPerGramOfFat = BigDecimal.valueOf(9.0);
            BigDecimal gramsOfProtein = NumberUtils.withStandardPrecision(proteinCals.divide(calsPerGramOfProteinAndCarb, RoundingMode.HALF_UP));
            BigDecimal gramsOfCarbs = NumberUtils.withStandardPrecision(carbCals.divide(calsPerGramOfProteinAndCarb, RoundingMode.HALF_UP));
            BigDecimal gramsOfFats = NumberUtils.withStandardPrecision(fatCals.divide(calsPerGramOfFat, RoundingMode.HALF_UP));
            return Map.of(Nutrient.PROTEIN, gramsOfProtein, Nutrient.CARBOHYDRATES, gramsOfCarbs, Nutrient.TOTAL_FATS, gramsOfFats);
        }
}
