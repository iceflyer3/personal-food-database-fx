package com.iceflyer3.pfd.data.task.mealplanning.meal;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanMeal;
import com.iceflyer3.pfd.data.entities.views.MealPlanMealNutritionalSummary;
import com.iceflyer3.pfd.data.mapping.mealplanning.MealPlanMealMapper;
import com.iceflyer3.pfd.data.mapping.summary.NutritionalSummaryMapper;
import com.iceflyer3.pfd.data.repository.MealPlanningRepository;
import com.iceflyer3.pfd.data.task.mealplanning.AbstractMealPlanningTask;
import com.iceflyer3.pfd.exception.InvalidApplicationStateException;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanMealViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class GetMealsForDayTask extends AbstractMealPlanningTask<List<MealPlanMealViewModel>>
{

    private final static Logger LOG = LoggerFactory.getLogger(GetMealsForDayTask.class);
    private final UUID forDayId;

    public GetMealsForDayTask(MealPlanningRepository mealPlanningRepository, UUID forDayId) {
        super(mealPlanningRepository);
        this.forDayId = forDayId;
    }

    @Override
    protected List<MealPlanMealViewModel> call() throws Exception {
        List<MealPlanMeal> mealEntities = mealPlanningRepository.getMealsForDay(forDayId);
        List<MealPlanMealNutritionalSummary> mealNutritionalSummaries = mealPlanningRepository.getMealNutritionalSummariesForDay(forDayId);

        List<MealPlanMealViewModel> mealViewModels = new ArrayList<>();
        for(MealPlanMeal mealEntity : mealEntities)
        {
            Optional<MealPlanMealNutritionalSummary> mealNutritionalSummary = mealNutritionalSummaries.stream().filter(summary -> summary.getMealId().equals(mealEntity.getMealId())).findFirst();
            if (mealNutritionalSummary.isPresent())
            {
                mealViewModels.add(MealPlanMealMapper.toModel(mealEntity, NutritionalSummaryMapper.toModel(mealNutritionalSummary.get())));
            }
            else
            {
                mealViewModels.add(MealPlanMealMapper.toModel(mealEntity, NutritionalSummary.empty()));
            }
        }

        LOG.debug("Found {} meals for day {}", mealEntities.size(), forDayId);
        return mealViewModels;
    }
}
