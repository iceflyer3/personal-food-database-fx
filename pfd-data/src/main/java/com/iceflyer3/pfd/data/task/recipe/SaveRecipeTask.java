package com.iceflyer3.pfd.data.task.recipe;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.entities.recipe.Recipe;
import com.iceflyer3.pfd.data.entities.views.RecipeNutritionalSummary;
import com.iceflyer3.pfd.data.mapping.helper.RecipeMapperHelper;
import com.iceflyer3.pfd.data.mapping.recipe.RecipeMapper;
import com.iceflyer3.pfd.data.mapping.summary.NutritionalSummaryMapper;
import com.iceflyer3.pfd.data.repository.FoodRepository;
import com.iceflyer3.pfd.data.repository.RecipeRepository;
import com.iceflyer3.pfd.data.request.recipe.CreateRecipeRequest;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.api.recipe.RecipeStepModel;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.recipe.RecipeViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.util.Collection;

public class SaveRecipeTask extends AbstractRecipeTask<RecipeViewModel>{

    private final static Logger LOG = LoggerFactory.getLogger(SaveRecipeTask.class);

    private final UserViewModel createdUser;
    private final RecipeViewModel recipeViewModel;

    public SaveRecipeTask(RecipeRepository recipeRepository, FoodRepository foodRepository, UserViewModel createdUser, RecipeViewModel recipeViewModel) {
        super(recipeRepository, foodRepository);
        this.createdUser = createdUser;
        this.recipeViewModel = recipeViewModel;
    }

    @Override
    protected RecipeViewModel call() throws Exception {

        Recipe recipeEntity;

        // New recipe?
        if(recipeViewModel.getId() == null)
        {
            LOG.debug("Creating new recipe...");
            CreateRecipeRequest createRecipeRequest = new CreateRecipeRequest(createdUser, recipeViewModel);
            recipeEntity = recipeRepository.createRecipe(createRecipeRequest);
        }
        else
        {
            LOG.debug("Updating recipe...");
            recipeEntity = recipeRepository.updateRecipe(recipeViewModel);
        }

        // Now that the entity has been updated perform the mapping to the view model.
        // In this case, we want to return a fully hydrated view model because we just saved the whole entity hierarchy.
        RecipeMapperHelper mapperHelper = new RecipeMapperHelper(foodRepository);
        Collection<IngredientModel> ingredientViewModels = mapperHelper.getRecipeIngredientModels(recipeEntity.getIngredients());
        Collection<RecipeStepModel> stepViewModels = mapperHelper.getStepModels(recipeEntity.getSteps());

        RecipeNutritionalSummary entityNutritionalSummary = recipeRepository.getNutritionalSummaryById(recipeEntity.getRecipeId());
        NutritionalSummary nutritionalSummary = NutritionalSummaryMapper.toModel(entityNutritionalSummary);
        return RecipeMapper.toModel(recipeEntity, nutritionalSummary, ingredientViewModels, stepViewModels);
    }
}
