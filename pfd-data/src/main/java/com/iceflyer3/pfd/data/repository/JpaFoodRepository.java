package com.iceflyer3.pfd.data.repository;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.constants.QueryConstants;
import com.iceflyer3.pfd.data.AppConfigUtils;
import com.iceflyer3.pfd.data.FoodImportResult;
import com.iceflyer3.pfd.data.entities.User;
import com.iceflyer3.pfd.data.entities.food.*;
import com.iceflyer3.pfd.data.entities.views.ComplexFoodNutritionalSummary;
import com.iceflyer3.pfd.data.entities.views.FoodAsIngredient;
import com.iceflyer3.pfd.data.json.JsonFoodData;
import com.iceflyer3.pfd.data.json.food.*;
import com.iceflyer3.pfd.data.mapping.IngredientMapper;
import com.iceflyer3.pfd.data.mapping.ServingSizeMapper;
import com.iceflyer3.pfd.enums.FoodType;
import com.iceflyer3.pfd.data.mapping.food.FoodMapper;
import com.iceflyer3.pfd.data.mapping.summary.NutritionalSummaryMapper;
import com.iceflyer3.pfd.data.request.food.SaveComplexFoodRequest;
import com.iceflyer3.pfd.data.request.food.SaveSimpleFoodRequest;
import com.iceflyer3.pfd.exception.DataImportException;
import com.iceflyer3.pfd.exception.InvalidApplicationStateException;
import com.iceflyer3.pfd.exception.NonUniqueFoodException;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.api.ServingSizeModel;
import com.iceflyer3.pfd.ui.model.api.food.ComplexFoodModel;
import com.iceflyer3.pfd.ui.model.api.food.SimpleFoodModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.ReadOnlyFoodViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.SimpleFoodViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;
import com.iceflyer3.pfd.user.UserAuthentication;
import javafx.collections.ObservableList;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.persistence.PersistenceContext;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Repository
@Transactional
public class JpaFoodRepository implements FoodRepository{

    private static final Logger LOG = LoggerFactory.getLogger(JpaFoodRepository.class);

    private final AppConfigUtils appConfigUtils;

    // Unfortunately it seems like the EntityManager has to use field injection instead of constructor
    // See: https://github.com/spring-projects/spring-framework/issues/15076
    @PersistenceContext
    private EntityManager entityManager;

    public JpaFoodRepository(AppConfigUtils appConfigUtils)
    {
        this.appConfigUtils = appConfigUtils;
    }

    /**
     * Gets a food for a given food id.
     *
     * This method is unique in that it actually returns a ViewModel object which is, generally speaking,
     * something we try to stay away from so that clients may transform the raw data as needed.
     *
     * However, if we are querying for a food this is always the representation we will want across the vast
     * majority of most of the application. The only exceptions to this being the two food creation screens.
     * For these exceptions you should see the toMutableModel function of the FoodMapper class.
     * @param foodId The id of the food to retrieve
     * @return The FoodViewModel representing the food.
     */
    @Override
    public ReadOnlyFoodViewModel getFoodForId(UUID foodId) {
        ReadOnlyFoodViewModel result;
        FoodMetaData metaData = entityManager.find(FoodMetaData.class, foodId);

        if (metaData == null)
        {
            throw new InvalidApplicationStateException(String.format("No food with id %s was found", foodId));
        }

        if (metaData.getFoodType() == FoodType.SIMPLE)
        {
            SimpleFood simpleFoodEntity = entityManager.find(SimpleFood.class, metaData.getFoodId());
            result = FoodMapper.toModel(simpleFoodEntity);
        }
        else
        {
            ComplexFood complexFoodEntity = entityManager.find(ComplexFood.class, metaData.getFoodId());
            ComplexFoodNutritionalSummary nutritionalSummaryEntity = getComplexFoodNutritionSummary(foodId);
            result = FoodMapper.toModel(complexFoodEntity, NutritionalSummaryMapper.toModel(nutritionalSummaryEntity));
        }

        return result;
    }

    @Override
    public SimpleFood getSimpleFoodById(UUID foodId) {
        SimpleFood simpleFood = entityManager.find(SimpleFood.class, foodId);

        if (simpleFood == null)
        {
            // By the time we're looking up a food by its id it should always already exist. It has to have been saved to have an id.
            throw new InvalidApplicationStateException(String.format("No simple food with id %s was found", foodId));
        }
        else
        {
            return simpleFood;
        }
    }

    @Override
    public List<SimpleFood> getAllSimpleFoods() {
        LOG.debug("Searching for all simple foods...");
        List<SimpleFood> simpleFoods = entityManager
                .createNamedQuery(QueryConstants.QUERY_FOOD_FIND_SIMPLE_FOR_USER, SimpleFood.class)
                .setParameter(QueryConstants.PARAMETER_USER_ID, UserAuthentication.instance().getAuthenticatedUser().userId())
                .getResultList();
        LOG.debug("Found {} simple foods", simpleFoods.size());
        return simpleFoods;
    }

    @Override
    public ComplexFood getComplexFoodById(UUID foodId) {
        ComplexFood complexFood = entityManager.find(ComplexFood.class, foodId);

        if (complexFood == null)
        {
            // By the time we're looking up a food by its id it should always already exist. It has to have been saved to have an id.
            throw new InvalidApplicationStateException(String.format("No simple food with id %s was found", foodId));
        }
        else
        {
            return complexFood;
        }
    }

    @Override
    public List<ComplexFood> getAllComplexFoods() {
        LOG.debug("Searching for all complex foods...");
        List<ComplexFood> complexFoods = entityManager
                .createNamedQuery(QueryConstants.QUERY_FOOD_FIND_COMPLEX_FOR_USER, ComplexFood.class)
                .setParameter(QueryConstants.PARAMETER_USER_ID, UserAuthentication.instance().getAuthenticatedUser().userId())
                .getResultList();
        LOG.debug("Found {} complex foods", complexFoods.size());
        return complexFoods;
    }

    /**
     * Find all complex foods with the specified name.
     * @param name The name to search for
     * @return A List of all complex foods that have the specified name
     */
    @Override
    public List<ComplexFood> findComplexFoodsWithNameForUser(String name)
    {
        return entityManager.createNamedQuery(QueryConstants.QUERY_FOOD_FIND_COMPLEX_BY_NAME_FOR_USER, ComplexFood.class)
                .setParameter(QueryConstants.PARAMETER_FOOD_NAME, name)
                .setParameter(QueryConstants.PARAMETER_USER_ID, UserAuthentication.instance().getAuthenticatedUser().userId())
                .getResultList();
    }

    @Override
    public List<ComplexFoodIngredient> getIngredientsForComplexFood(UUID foodId)
    {
        List<ComplexFoodIngredient> ingredients = entityManager.createNamedQuery(QueryConstants.QUERY_FOOD_FIND_COMPLEX_FOOD_INGREDIENTS, ComplexFoodIngredient.class)
                                                               .setParameter(QueryConstants.PARAMETER_FOOD_ID, foodId)
                                                               .getResultList();

        if (ingredients.isEmpty())
        {
            throw new InvalidApplicationStateException(String.format("Could not find any ingredients for complex food %s", foodId));
        }
        else
        {
            return ingredients;
        }
    }

    @Override
    public ComplexFoodNutritionalSummary getComplexFoodNutritionSummary(UUID foodId)
    {
        try
        {
            return entityManager
                    .createNamedQuery(QueryConstants.QUERY_FOOD_CALCULATE_COMPLEX_FOOD_NUTRITIONAL_SUMMARY, ComplexFoodNutritionalSummary.class)
                    .setParameter(QueryConstants.PARAMETER_FOOD_ID, foodId)
                    .getSingleResult();
        }
        catch(NoResultException nre)
        {
            throw new InvalidApplicationStateException(String.format("No nutritional summary was found for the complex food %s", foodId), nre);
        }
    }

    @Override
    public SimpleFood createSimpleFood(SaveSimpleFoodRequest simpleFoodCreationRequest) {
        LOG.debug("Received request to save new simple food... Saving metadata...");
        SimpleFoodViewModel foodViewModel = simpleFoodCreationRequest.getFoodDetails();

        if (isFoodWithNameAndBrandUnique(foodViewModel.getName(), foodViewModel.getBrand()))
        {
            FoodMetaData foodMetaData = createAndSaveMetaDataForFoodType(FoodType.SIMPLE, foodViewModel.getServingSizes());

            // Setup relationships on the new food before we save it
            User user =  entityManager.find(User.class, simpleFoodCreationRequest.getCreatedUser().getUserId());

            LOG.debug("Saving new simple food...");
            SimpleFood entityFood = new SimpleFood(
                    foodViewModel.getName(),
                    foodViewModel.getBrand(),
                    foodViewModel.getSource(),
                    foodViewModel.getCalories(),
                    user);
            entityFood.setFoodMetaData(foodMetaData);
            updateSimpleFoodInformation(entityFood, foodViewModel, user);

            entityManager.persist(entityFood);

            return entityFood;
        }
        else
        {
            throw new NonUniqueFoodException(String.format("Attempted to create a duplicate simple food %s with brand %s", foodViewModel.getName(), foodViewModel.getBrand()));
        }
    }

    @Override
    public SimpleFood updateSimpleFood(SaveSimpleFoodRequest saveSimpleFoodRequest)
    {
        SimpleFoodViewModel foodViewModel = saveSimpleFoodRequest.getFoodDetails();
        SimpleFood foodEntityToUpdate = entityManager.find(SimpleFood.class, foodViewModel.getId());
        User user =  entityManager.find(User.class, saveSimpleFoodRequest.getCreatedUser().getUserId());

        // If name / brand hasn't changed then we can safely proceed with the update
        if (foodEntityToUpdate.getBrand().equals(foodViewModel.getBrand()) && foodEntityToUpdate.getName().equals(foodViewModel.getName()))
        {
            updateSimpleFoodInformation(foodEntityToUpdate, foodViewModel, user);
            return foodEntityToUpdate;
        }
        else
        {
            // If the name or brand of the food has been updated then we must ensure that the new
            // name / brand combination is still unique so that we don't end up with duplicated foods
            if (isFoodWithNameAndBrandUnique(foodViewModel.getName(), foodViewModel.getBrand()))
            {
                updateSimpleFoodInformation(foodEntityToUpdate, foodViewModel, user);
                return foodEntityToUpdate;
            }
            else
            {
                throw new NonUniqueFoodException(String.format("Attempted to update simple food name / brand combination to a combination that already exists (%s/%s)", foodViewModel.getName(), foodViewModel.getBrand()));
            }
        }
    }

    @Override
    public boolean isSimpleFoodUnique(SimpleFoodModel simpleFoodModel)
    {
        return isFoodWithNameAndBrandUnique(simpleFoodModel.getName(), simpleFoodModel.getBrand());
    }

    @Override
    public ComplexFood createComplexFood(SaveComplexFoodRequest complexFoodCreationRequest) {
        LOG.debug("Creating a new complex food...");
        FoodMetaData foodMetaData = createAndSaveMetaDataForFoodType(FoodType.COMPLEX, complexFoodCreationRequest.getFoodDetails().getServingSizes());

        User user =  entityManager.find(User.class, complexFoodCreationRequest.getCreatedUser().getUserId());
        ComplexFood complexFood = new ComplexFood(complexFoodCreationRequest.getFoodDetails().getName(), user);
        complexFood.setFoodMetaData(foodMetaData);
        entityManager.persist(complexFood);

        Set<ComplexFoodIngredient> ingredients = new HashSet<>();
        for(IngredientModel ingredientViewModel : complexFoodCreationRequest.getFoodDetails().getIngredients())
        {
            SimpleFood ingredientSimpleFood = entityManager.find(SimpleFood.class, ingredientViewModel.getIngredientFood().getId());
            ComplexFoodIngredient ingredient = new ComplexFoodIngredient(ingredientSimpleFood, complexFood, ingredientViewModel.getServingsIncluded());
            ingredient.setIsAlternate(ingredientViewModel.isAlternate());
            ingredient.setIsOptional(ingredientViewModel.isOptional());
            entityManager.persist(ingredient);
            ingredients.add(ingredient);
        }

        complexFood.setIngredients(ingredients);

        LOG.debug("Complex food has been saved successfully!!");

        return complexFood;
    }

    @Override
    public ComplexFood updateComplexFood(SaveComplexFoodRequest saveComplexFoodRequest)
    {
        User user =  entityManager.find(User.class, saveComplexFoodRequest.getCreatedUser().getUserId());

        // Update the complex food entity itself before updating ingredients
        ComplexFood complexFoodEntity = entityManager.find(ComplexFood.class, saveComplexFoodRequest.getFoodDetails().getId());
        complexFoodEntity.setName(saveComplexFoodRequest.getFoodDetails().getName());
        complexFoodEntity.setLastModifiedDate(saveComplexFoodRequest.getFoodDetails().getLastModifiedDate());
        complexFoodEntity.setLastModifiedUser(user);

        // Update the ingredients for the complex food
        // Fist, remove any ingredients that are no longer in the ingredients list
        List<IngredientModel> updatedIngredientList = saveComplexFoodRequest.getFoodDetails().getIngredients();
        LOG.debug("Updated ingredients list is {}", updatedIngredientList);
        List<ComplexFoodIngredient> ingredientsToRemove = complexFoodEntity.getIngredients()
                .stream()
                .filter(existingIngredient ->
                        updatedIngredientList.stream().noneMatch(modifiedIngredient -> existingIngredient.getIngredientId().equals(modifiedIngredient.getId()))
                )
                .collect(Collectors.toList());
        ingredientsToRemove.forEach(entityManager::remove);

        // Next, add or update ingredients as needed
        for (IngredientModel updatedIngredientViewModel : updatedIngredientList)
        {
            ComplexFoodIngredient ingredientEntity;

            // New ingredient?
            if (updatedIngredientViewModel.getId() == null)
            {
                SimpleFood ingredientSimpleFood = entityManager.find(SimpleFood.class, updatedIngredientViewModel.getIngredientFood().getId());
                ingredientEntity = new ComplexFoodIngredient(ingredientSimpleFood, complexFoodEntity, updatedIngredientViewModel.getServingsIncluded());
                ingredientEntity.setSimpleFoodServingsIncluded(updatedIngredientViewModel.getServingsIncluded());
                ingredientEntity.setIsOptional(updatedIngredientViewModel.isOptional());
                ingredientEntity.setIsAlternate(updatedIngredientViewModel.isAlternate());
                entityManager.persist(ingredientEntity);
            }
            else
            {
                ingredientEntity = entityManager.find(ComplexFoodIngredient.class, updatedIngredientViewModel.getId());
                ingredientEntity.setSimpleFoodServingsIncluded(updatedIngredientViewModel.getServingsIncluded());
                ingredientEntity.setIsOptional(updatedIngredientViewModel.isOptional());
                ingredientEntity.setIsAlternate(updatedIngredientViewModel.isAlternate());
            }
        }

        return complexFoodEntity;
    }

    @Override
    public boolean isComplexFoodUnique(ComplexFoodModel complexFoodModel)
    {
        boolean isUnique = true;

        // Calculate nutritional summary information for the desired new complex food
        List<UUID> simpleFoodIngredientIds = complexFoodModel
                .getIngredients()
                .stream()
                .map(ingredientModel -> ingredientModel.getIngredientFood().getId())
                .toList();

        List<SimpleFood> ingredientFoodEntities = entityManager
                .createNamedQuery(QueryConstants.QUERY_FOOD_FIND_ALL_SIMPLE_WITH_IDS, SimpleFood.class)
                .setParameter(QueryConstants.PARAMETER_FOOD_ID_LIST, simpleFoodIngredientIds)
                .getResultList();

        // Create a map of the simple food entities to the number of servings of the food included
        Map<SimpleFood, BigDecimal> simpleFoodsServingsIncluded = new HashMap<>();
        for(IngredientModel ingredient : complexFoodModel.getIngredients())
        {
            SimpleFood ingredientFoodEntity = ingredientFoodEntities.stream().filter(foodEntity -> foodEntity.getFoodId().equals(ingredient.getIngredientFood().getId())).findFirst().orElseThrow();
            simpleFoodsServingsIncluded.put(ingredientFoodEntity, ingredient.getServingsIncluded());
        }

        NutritionalSummary ingredientsNutritionalSummary = NutritionalSummaryMapper.toModel(simpleFoodsServingsIncluded);

        // Search for complex foods with the same name and, if found, compare nutritional profile
        List<ComplexFood> duplicateComplexFoods = findComplexFoodsWithNameForUser(complexFoodModel.getName());
        for(ComplexFood duplicateCandidate : duplicateComplexFoods)
        {
            ComplexFoodNutritionalSummary nutritionalSummaryEntity = getComplexFoodNutritionSummary(duplicateCandidate.getFoodId());
            NutritionalSummary duplicateCandidateNutritionalSummary = NutritionalSummaryMapper.toModel(nutritionalSummaryEntity);

            isUnique = isUnique && !duplicateCandidateNutritionalSummary.equals(ingredientsNutritionalSummary);
        }

        return isUnique;
    }

    @Override
    public void deleteFood(UUID foodId) {
        /*
         * Steps for deletion:
         *      1.) Look up FoodMetaData record for food ID
         *      2.) Determine type of food and lookup in appropriate table
         *      3.) Delete records as needed to delete the food
         *              - SimpleFoods this is just the SimpleFood record
         *              - ComplexFoods this is the ComplexFood record and all
         *                ComplexFoodIngredient records
         */
        FoodMetaData foodMetaData = entityManager.find(FoodMetaData.class, foodId);

        if (foodMetaData.getFoodType() == FoodType.SIMPLE)
        {
            SimpleFood simpleFood = entityManager.find(SimpleFood.class, foodId);
            entityManager.remove(simpleFood);
        }
        else
        {
            ComplexFood complexFood = entityManager.find(ComplexFood.class, foodId);
            complexFood.getIngredients().forEach(entityManager::remove);
            entityManager.remove(complexFood);
        }

        foodMetaData.getServingSizes().forEach(entityManager::remove);
        entityManager.remove(foodMetaData);
    }

    @Override
    public List<FoodAsIngredient> findFoodAsIngredient(UUID simpleFoodID)
    {
        return entityManager
                .createNamedQuery(QueryConstants.QUERY_FOOD_FIND_FOOD_AS_INGREDIENT, FoodAsIngredient.class)
                .setParameter(QueryConstants.PARAMETER_FOOD_ID, simpleFoodID)
                .getResultList();
    }

    @Override
    public JsonFoodData exportData()
    {
        JsonFoodData jsonFoodData = new JsonFoodData(appConfigUtils.getProperty("schema.version", Integer.class));
        List<JsonFoodMetaData> jsonFoodMetaData = new ArrayList<>();
        List<JsonFoodServingSize> jsonFoodServingSizes = new ArrayList<>();
        List<JsonSimpleFood> jsonSimpleFoods = new ArrayList<>();

        // Export simple foods
        List<SimpleFood> simpleFoods = entityManager
                .createNamedQuery(QueryConstants.QUERY_FOOD_FIND_SIMPLE_FOR_USER, SimpleFood.class)
                .setParameter(QueryConstants.PARAMETER_USER_ID, UserAuthentication.instance().getAuthenticatedUser().userId())
                .getResultList();

        for(SimpleFood simpleFood : simpleFoods)
        {
            // Export food meta data and serving size data
            FoodMetaData foodMetaData = simpleFood.getFoodMetaData();
            jsonFoodMetaData.add(FoodMapper.metaDataToJson(foodMetaData));
            jsonFoodServingSizes.addAll(getJsonServingsSizesForMetaData(foodMetaData));

            // Export simple food data
            jsonSimpleFoods.add(FoodMapper.toJson(simpleFood));
        }

        // Export complex foods
        List<ComplexFood> complexFoods = entityManager
                .createNamedQuery(QueryConstants.QUERY_FOOD_FIND_COMPLEX_FOR_USER, ComplexFood.class)
                .setParameter(QueryConstants.PARAMETER_USER_ID, UserAuthentication.instance().getAuthenticatedUser().userId())
                .getResultList();
        List<JsonComplexFood> jsonComplexFoods = new ArrayList<>();
        List<JsonComplexFoodIngredient> jsonComplexFoodIngredients = new ArrayList<>();

        for(ComplexFood complexFood : complexFoods)
        {
            // Export food meta data and serving size data
            FoodMetaData foodMetaData = complexFood.getFoodMetaData();
            jsonFoodMetaData.add(FoodMapper.metaDataToJson(foodMetaData));
            jsonFoodServingSizes.addAll(getJsonServingsSizesForMetaData(foodMetaData));

            // Export complex food data
            jsonComplexFoods.add(FoodMapper.toJson(complexFood));

            for(ComplexFoodIngredient ingredient : complexFood.getIngredients())
            {
                jsonComplexFoodIngredients.add(IngredientMapper.toJson(ingredient));
            }
        }

        jsonFoodData.setComplexFoods(jsonComplexFoods);
        jsonFoodData.setComplexFoodIngredients(jsonComplexFoodIngredients);


        jsonFoodData.setSimpleFoods(jsonSimpleFoods);
        jsonFoodData.setFoodMetaData(jsonFoodMetaData);
        jsonFoodData.setFoodServingSizes(jsonFoodServingSizes);
        return jsonFoodData;
    }

    @Override
    public FoodImportResult importData(JsonFoodData jsonFoodData)
    {
        User importUser = entityManager.find(User.class, UserAuthentication.instance().getAuthenticatedUser().userId());
        LOG.debug("Beginning import of food data for user {}", importUser);

        if (importUser == null)
        {
            // As with all application state exceptions, this should never happen. But we'll be safe and check anyway.
            throw new InvalidApplicationStateException("The User profile importing this data (%s) could not be found in the database!");
        }

        Map<UUID, UUID> importedSimpleFoods = importSimpleFoodData(jsonFoodData, importUser);
        Map<UUID, UUID> importedComplexFoods = importComplexFoodData(jsonFoodData, importUser, importedSimpleFoods);

        FoodImportResult foodImportResult = new FoodImportResult();
        foodImportResult.addImportedSimpleFoods(importedSimpleFoods);
        foodImportResult.addImportedComplexFoods(importedComplexFoods);
        LOG.debug("Finished import of food data for user {}", importUser);
        return foodImportResult;
    }

    private FoodMetaData createAndSaveMetaDataForFoodType(FoodType foodType, ObservableList<ServingSizeModel> servingSizes)
    {
        FoodMetaData foodMetaData = new FoodMetaData();
        foodMetaData.setFoodType(foodType);
        entityManager.persist(foodMetaData);

        for(ServingSizeModel servingSizeViewModel : servingSizes)
        {
            FoodServingSize servingSizeEntity = new FoodServingSize(servingSizeViewModel.getUnitOfMeasure(), servingSizeViewModel.getServingSize(), foodMetaData);
            entityManager.persist(servingSizeEntity);
            foodMetaData.getServingSizes().add(servingSizeEntity);
        }

        return foodMetaData;
    }

    /**
     * Updates a SimpleFood instance according to a SimpleFoodViewModel instance.
     *
     * Convenience function to be called by the create and update operations because
     * the bulk of the work they need to do is the same.
     *
     * @param entityFood The SimpleFood instance to update
     * @param foodViewModel The SimpleFoodViewModel containing the updates to apply
     * @param user The user who is request the updates
     * @return The entity food that was updated
     */
    private SimpleFood updateSimpleFoodInformation(SimpleFood entityFood, SimpleFoodViewModel foodViewModel, User user)
    {
        // Basic info
        entityFood.setName(foodViewModel.getName());
        entityFood.setBrand(foodViewModel.getBrand());
        entityFood.setSource(foodViewModel.getSource());
        entityFood.setCalories(foodViewModel.getCalories());

        // Modification info
        entityFood.setLastModifiedUser(user);
        entityFood.setLastModifiedDate(foodViewModel.getLastModifiedDate());

        // Nutritional info
        entityFood.setGlycemicIndex(foodViewModel.getGlycemicIndex());
        entityFood.setProtein(foodViewModel.getProtein());
        entityFood.setCarbohydrates(foodViewModel.getCarbohydrates());
        entityFood.setTotalFats(foodViewModel.getTotalFats());
        entityFood.setTransFat(foodViewModel.getTransFat());
        entityFood.setMonounsaturatedFat(foodViewModel.getMonounsaturatedFat());
        entityFood.setPolyunsaturatedFat(foodViewModel.getPolyunsaturatedFat());
        entityFood.setSaturatedFat(foodViewModel.getSaturatedFat());
        entityFood.setSugars(foodViewModel.getSugars());
        entityFood.setFiber(foodViewModel.getFiber());
        entityFood.setCholesterol(foodViewModel.getCholesterol());
        entityFood.setIron(foodViewModel.getIron());
        entityFood.setManganese(foodViewModel.getManganese());
        entityFood.setCopper(foodViewModel.getCopper());
        entityFood.setZinc(foodViewModel.getZinc());
        entityFood.setCalcium(foodViewModel.getCalcium());
        entityFood.setMagnesium(foodViewModel.getMagnesium());
        entityFood.setPhosphorus(foodViewModel.getPhosphorus());
        entityFood.setPotassium(foodViewModel.getPotassium());
        entityFood.setSodium(foodViewModel.getSodium());
        entityFood.setSulfur(foodViewModel.getSulfur());
        entityFood.setVitaminA(foodViewModel.getVitaminA());
        entityFood.setVitaminC(foodViewModel.getVitaminC());
        entityFood.setVitaminK(foodViewModel.getVitaminK());
        entityFood.setVitaminD(foodViewModel.getVitaminD());
        entityFood.setVitaminE(foodViewModel.getVitaminE());
        entityFood.setVitaminB1(foodViewModel.getVitaminB1());
        entityFood.setVitaminB2(foodViewModel.getVitaminB2());
        entityFood.setVitaminB3(foodViewModel.getVitaminB3());
        entityFood.setVitaminB5(foodViewModel.getVitaminB5());
        entityFood.setVitaminB6(foodViewModel.getVitaminB6());
        entityFood.setVitaminB7(foodViewModel.getVitaminB7());
        entityFood.setVitaminB8(foodViewModel.getVitaminB8());
        entityFood.setVitaminB9(foodViewModel.getVitaminB9());
        entityFood.setVitaminB12(foodViewModel.getVitaminB12());
        return entityFood;
    }

    private boolean isFoodWithNameAndBrandUnique(String foodName, String foodBrand)
    {
        long duplicateCount = entityManager.createNamedQuery(QueryConstants.QUERY_FOOD_FIND_SIMPLE_BY_NAME_AND_BRAND, SimpleFood.class)
                .setParameter(QueryConstants.PARAMETER_FOOD_NAME, foodName)
                .setParameter(QueryConstants.PARAMETER_FOOD_BRAND, foodBrand)
                .getResultStream()
                .count();

        return duplicateCount == 0;
    }

    private JsonFoodMetaData findJsonFoodMetaDataForFood(UUID foodId, Collection<JsonFoodMetaData> foodMetaDataCollection)
    {
        Optional<JsonFoodMetaData> jsonFoodMetaData = foodMetaDataCollection
                .stream()
                .filter(jsonMetaData -> jsonMetaData.getFoodId().equals(foodId))
                .findFirst();

        if (jsonFoodMetaData.isPresent())
        {
            return jsonFoodMetaData.get();
        }
        else
        {
            throw new DataImportException(String.format("Could not find the associated JsonFoodMetaData for food with id %s", foodId));
        }
    }

    private Set<JsonFoodServingSize> findJsonFoodServingSizesForFood(UUID foodId, Collection<JsonFoodServingSize> foodServingSizeCollection)
    {
        Set<JsonFoodServingSize> jsonFoodServingSizes = foodServingSizeCollection
                .stream()
                .filter(jsonServingSize -> jsonServingSize.getFoodMetaDataId().equals(foodId))
                .collect(Collectors.toSet());

        if (jsonFoodServingSizes.size() > 0)
        {
            return jsonFoodServingSizes;
        }
        else
        {
            throw new DataImportException(String.format("Could not find any JsonFoodServingSize data for food with id %s", foodId));
        }
    }

    private List<JsonFoodServingSize> getJsonServingsSizesForMetaData(FoodMetaData foodMetaData)
    {
        return foodMetaData
                .getServingSizes()
                .stream()
                .map(servingSize -> ServingSizeMapper.toJson(servingSize, foodMetaData.getFoodId()))
                .toList();
    }

    /*
     * This is an identical operation to that found in NutritionalSummaryReporter but this one
     * operates on data at a lower level of abstraction so unfortunately reuse wasn't an option
     *
     * This also gets tagged as duplicated code in the similar function over in the recipe
     * repository.
     *
     * The big decimal aggregation/reduction and comparison operations certainly are duplicated.
     * However, the type of the streams that the two functions operate on are different types of
     * entities and share no common interface. As such, I see no immediately apparent way to
     * refactor this to a single method for code reuse.
     *
     * So, duplicated? Yeah, partially, kind of. But there isn't really anything to be done. So
     * tell it to hush.
     */
    @SuppressWarnings("DuplicatedCode")
    public boolean areJsonIngredientsAndFoodNutritionalSummaryEquivalent(Set<JsonComplexFoodIngredient> ingredients, ComplexFoodNutritionalSummary nutritionalSummary, Map<UUID, UUID> importedSimpleFoods)
    {
        LOG.debug("\tComparing nutritional summary\n\t\t{}\n\tto ingredient list:\n\t\t{}", nutritionalSummary, ingredients);

        // Find all IDs in our map of imported simple foods that also occur in our set of ingredients
        List<UUID> importedSimpleFoodIngredientIds = importedSimpleFoods
                .entrySet()
                .stream()
                .filter(entry ->
                        ingredients.stream().anyMatch(jsonComplexFoodIngredient -> jsonComplexFoodIngredient.getSimpleFoodId().equals(entry.getKey()))
                )
                .map(Map.Entry::getValue)
                .toList();

        if (importedSimpleFoodIngredientIds.size() == 0)
        {
            throw new DataImportException(
                    String.format(
                            "Could not find any simple food IDs in the set of imported simple foods for the list of ingredients.\n\tSource Ingredient list:\n\t\t%s\n\tImported simple foods list:\n\t\t%s",
                            ingredients,
                            importedSimpleFoods)
            );
        }

        // Look up the simple food records for those IDs
        List<SimpleFood> ingredientFoodEntities = entityManager
                .createNamedQuery(QueryConstants.QUERY_FOOD_FIND_ALL_SIMPLE_WITH_IDS, SimpleFood.class)
                .setParameter(QueryConstants.PARAMETER_FOOD_ID_LIST, importedSimpleFoodIngredientIds)
                .getResultList();

        if (ingredientFoodEntities.size() != importedSimpleFoodIngredientIds.size())
        {
            throw new DataImportException(
                    String.format(
                            "Could not find simple food(s) for one or more IDs in the list of imported simple foods that are also ingredients for the complex food.\n\tComplex food ingredient imported simple food IDs list:\n\t\t%s",
                            importedSimpleFoodIngredientIds
                    )
            );
        }

        // Create a map of the simple food entities to the number of servings of the food included
        Map<SimpleFood, BigDecimal> simpleFoodsServingsIncluded = new HashMap<>();
        for(JsonComplexFoodIngredient jsonComplexFoodIngredient : ingredients)
        {
            SimpleFood simpleFoodEntity = ingredientFoodEntities.stream().filter(entity -> entity.getFoodId().equals(importedSimpleFoods.get(jsonComplexFoodIngredient.getSimpleFoodId()))).findFirst().orElseThrow();
            simpleFoodsServingsIncluded.put(simpleFoodEntity, jsonComplexFoodIngredient.getSimpleFoodServingsIncluded());
        }

        LOG.debug("Found ingredient list of food entities of size {}\n{}", ingredientFoodEntities.size(), ingredientFoodEntities);
        NutritionalSummary ingredientsNutritionalSummary = NutritionalSummaryMapper.toModel(simpleFoodsServingsIncluded);
        NutritionalSummary complexFoodEntitySummary = NutritionalSummaryMapper.toModel(nutritionalSummary);
        LOG.debug("Comparing ingredient nutritional summary of\n{}\nto candidate existing complex food entity summary of\n{}", ingredientsNutritionalSummary, complexFoodEntitySummary);

        boolean areNutritionallyEqual = ingredientsNutritionalSummary.equals(complexFoodEntitySummary);
        LOG.debug("\tAre nutritionally equal? {}", areNutritionallyEqual);
        return areNutritionallyEqual;
    }

    /**
     * Performs the import of simple food data by creating and updating data in the database
     * as needed.
     *
     * @param importData JsonFoodData instance that contains the details about the foods to import
     * @param importUser The user performing the import
     */
    private Map<UUID, UUID> importSimpleFoodData(JsonFoodData importData, User importUser)
    {
        Map<UUID, UUID> importedSimpleFoods = new HashMap<>();
        for(JsonSimpleFood jsonSimpleFood : importData.getSimpleFoods())
        {
            LOG.debug(String.format("Importing simple food %s/%s (%s)", jsonSimpleFood.getBrandName(), jsonSimpleFood.getFoodName(), jsonSimpleFood.getFoodId()));
            UUID jsonSourceFoodId = jsonSimpleFood.getFoodId(); // Cache the ID from the json. The ID will be updated if we're updating a duplicate food.
            FoodMetaData newFoodMetaDataEntity;
            SimpleFood newSimpleFoodEntity;

            // Find the associated json metadata and serving sizes to import
            JsonFoodMetaData jsonFoodMetaData = findJsonFoodMetaDataForFood(jsonSimpleFood.getFoodId(), importData.getFoodMetaData());
            Set<JsonFoodServingSize> jsonFoodServingSizes = findJsonFoodServingSizesForFood(jsonSimpleFood.getFoodId(), importData.getFoodServingSizes());

            /*
             * Check if this food is new and unique to the database or not.
             *
             * If it is not a new and unique food then update the food ids on the related json
             * objects to point to the existing food so that existing data will be updated instead
             * of new duplicate data being created.
             */
            try
            {
                SimpleFood existingSimpleFood = entityManager.createNamedQuery(QueryConstants.QUERY_FOOD_FIND_SIMPLE_BY_NAME_AND_BRAND, SimpleFood.class)
                        .setParameter(QueryConstants.PARAMETER_FOOD_NAME, jsonSimpleFood.getFoodName())
                        .setParameter(QueryConstants.PARAMETER_FOOD_BRAND, jsonSimpleFood.getBrandName())
                        .getSingleResult();

                LOG.debug(String.format("\tAn existing simple food was found. Simple food with id %s will be updated", existingSimpleFood.getFoodId()));

                // If we're going to be updating an existing food we'll just recreate the serving
                // sizes for it rather than try and deal with locating and then updating them
                existingSimpleFood.getFoodMetaData().getServingSizes().forEach(foodServingSize -> entityManager.remove(foodServingSize));
                existingSimpleFood.getFoodMetaData().getServingSizes().clear();

                // Update metadata, serving size, and simple food json data to point to the existing food
                jsonFoodMetaData.setFoodId(existingSimpleFood.getFoodId());
                jsonSimpleFood.setFoodId(existingSimpleFood.getFoodId());
                jsonFoodServingSizes.forEach(foodServingSize -> foodServingSize.setFoodMetaDataId(existingSimpleFood.getFoodId()));

                newFoodMetaDataEntity = FoodMapper.metaDataToEntity(jsonFoodMetaData);
                newSimpleFoodEntity = FoodMapper.toEntity(jsonSimpleFood, newFoodMetaDataEntity, importUser, importUser);
            }
            catch(NoResultException noResultException)
            {
                LOG.debug("\tNo existing simple food was found. A new simple food will be created.");
                newFoodMetaDataEntity = FoodMapper.metaDataToFreshEntity(jsonFoodMetaData);
                newSimpleFoodEntity = FoodMapper.toFreshEntity(jsonSimpleFood, newFoodMetaDataEntity, importUser, importUser);
            }

            // Write out the data to the database
            newFoodMetaDataEntity = entityManager.merge(newFoodMetaDataEntity);

            for(JsonFoodServingSize jsonFoodServingSize : jsonFoodServingSizes)
            {
                LOG.debug("\tCreating serving size data for simple food");
                FoodServingSize servingSizeEntity = ServingSizeMapper.toFreshEntity(jsonFoodServingSize, newFoodMetaDataEntity);
                entityManager.merge(servingSizeEntity);
            }

            newSimpleFoodEntity.setFoodMetaData(newFoodMetaDataEntity);
            newSimpleFoodEntity = entityManager.merge(newSimpleFoodEntity);
            importedSimpleFoods.put(jsonSourceFoodId, newSimpleFoodEntity.getFoodId());
            LOG.debug("Import of simple food has completed (with generated ID {})", newSimpleFoodEntity.getFoodId());
        }

        return importedSimpleFoods;
    }

    private Map<UUID, UUID> importComplexFoodData(JsonFoodData importData, User importUser, Map<UUID, UUID> importedSimpleFoods)
    {
        /*
         * Import complex food data
         *
         * It is worth noting that complex foods don't have the same uniqueness restrictions that
         * simple foods have.
         *
         * It doesn't make sense (and, in fact, undermines one of the goals of the application) to
         * have two simple foods with the same name and brand but different nutritional information
         * as that can only actually possibly be a single discrete item.
         *
         * However, since complex foods are composed of simple foods they don't share that uniqueness
         * restriction. Consider a complex food created from a recipe. You can have two different recipes
         * that create the same food that use slightly different ingredients. Complex foods also lack a
         * brand as they are often items that you make yourself and thus have no brand.
         *
         * With all of that said, a best effort attempt is made to detect existing complex foods and update
         * them by using their name and nutritional profile as the basis for determining equality.
         */
        Map<UUID, UUID> importedComplexFoods = new HashMap<>();
        for(JsonComplexFood jsonComplexFood : importData.getComplexFoods())
        {
            LOG.debug(String.format("Importing complex food %s (%s)", jsonComplexFood.getFoodName(), jsonComplexFood.getFoodId()));
            FoodMetaData newFoodMetaDataEntity = null;
            ComplexFood newComplexFoodEntity = null;

            // Find the associated metadata record, serving sizes, and list of ingredients to import
            JsonFoodMetaData jsonFoodMetaData = findJsonFoodMetaDataForFood(jsonComplexFood.getFoodId(), importData.getFoodMetaData());
            Set<JsonFoodServingSize> jsonFoodServingSizes = findJsonFoodServingSizesForFood(jsonComplexFood.getFoodId(), importData.getFoodServingSizes());
            Set<JsonComplexFoodIngredient> jsonComplexFoodIngredients = importData
                    .getComplexFoodIngredients()
                    .stream()
                    .filter(ingredient -> ingredient.getComplexFoodId().equals(jsonComplexFood.getFoodId()))
                    .collect(Collectors.toSet());

            // Check if a complex food with the same name and nutritional profile already exists
            List<ComplexFood> candidateExistingComplexFoods = findComplexFoodsWithNameForUser(jsonComplexFood.getFoodName());
            for(ComplexFood candidateExistingComplexFood : candidateExistingComplexFoods)
            {
                ComplexFoodNutritionalSummary nutritionalSummary = getComplexFoodNutritionSummary(candidateExistingComplexFood.getFoodId());

                if(areJsonIngredientsAndFoodNutritionalSummaryEquivalent(jsonComplexFoodIngredients, nutritionalSummary, importedSimpleFoods))
                {
                    candidateExistingComplexFood.getFoodMetaData().getServingSizes().forEach(servingSize -> entityManager.remove(servingSize));
                    candidateExistingComplexFood.getFoodMetaData().getServingSizes().clear();

                    candidateExistingComplexFood.getIngredients().forEach(ingredient -> entityManager.remove(ingredient));
                    candidateExistingComplexFood.getIngredients().clear();

                    // We apparently have to manually trigger a flush here or Hibernate won't actually
                    // execute any "delete" statements against the ComplexFoodIngredient table
                    entityManager.flush();

                    // Update the json data (food, metadata, serving sizes, ingredients) to point to the existing complex food
                    jsonComplexFood.setFoodId(candidateExistingComplexFood.getFoodId());
                    jsonFoodMetaData.setFoodId(candidateExistingComplexFood.getFoodId());
                    jsonFoodServingSizes.forEach(jsonServingSize -> jsonServingSize.setFoodMetaDataId(candidateExistingComplexFood.getFoodId()));
                    jsonComplexFoodIngredients.forEach(jsonIngredient -> jsonIngredient.setComplexFoodId(candidateExistingComplexFood.getFoodId()));

                    newFoodMetaDataEntity = FoodMapper.metaDataToEntity(jsonFoodMetaData);
                    newComplexFoodEntity = FoodMapper.toEntity(jsonComplexFood, newFoodMetaDataEntity, importUser, importUser);
                }
            }

            // If either there were no complex foods with the same name or no complex foods with same name and nutritional profile
            // then ensure we create a new complex food
            if (newFoodMetaDataEntity == null)
            {
                LOG.debug("\tNo existing complex food was found. A new complex food will be created.");
                newFoodMetaDataEntity = FoodMapper.metaDataToFreshEntity(jsonFoodMetaData);
                newComplexFoodEntity = FoodMapper.toFreshEntity(jsonComplexFood, newFoodMetaDataEntity, importUser, importUser);
            }

            newFoodMetaDataEntity = entityManager.merge(newFoodMetaDataEntity);

            newComplexFoodEntity.setFoodMetaData(newFoodMetaDataEntity);
            newComplexFoodEntity = entityManager.merge(newComplexFoodEntity);

            for (JsonComplexFoodIngredient jsonComplexFoodIngredient : jsonComplexFoodIngredients)
            {
                LOG.debug("\tCreating ingredient data for complex food ingredient {}/{} (source id: {})", jsonComplexFoodIngredient.getSimpleFoodBrand(), jsonComplexFoodIngredient.getSimpleFoodName(), jsonComplexFoodIngredient.getSimpleFoodId());
                /*
                 * Exported complex foods always include the simple foods from which they are composed
                 * at export time. So we should always be able to look up the new id of the food from
                 * the map of old ids to new ids.
                 */
                SimpleFood ingredientSimpleFoodEntity = entityManager.find(SimpleFood.class, importedSimpleFoods.get(jsonComplexFoodIngredient.getSimpleFoodId()));

                if (ingredientSimpleFoodEntity != null)
                {
                    ComplexFoodIngredient complexFoodIngredientEntity = IngredientMapper.toFreshEntity(jsonComplexFoodIngredient, ingredientSimpleFoodEntity, newComplexFoodEntity);
                    entityManager.merge(complexFoodIngredientEntity);
                }
                else
                {
                    throw new DataImportException(String.format("No simple food was found for brand/name %s/%s", jsonComplexFoodIngredient.getSimpleFoodBrand(), jsonComplexFoodIngredient.getSimpleFoodName()));
                }
            }

            for(JsonFoodServingSize jsonFoodServingSize : jsonFoodServingSizes)
            {
                LOG.debug("\tCreating serving size data for complex food");
                FoodServingSize servingSizeEntity = ServingSizeMapper.toFreshEntity(jsonFoodServingSize, newFoodMetaDataEntity);
                entityManager.merge(servingSizeEntity);
            }

            importedComplexFoods.put(jsonComplexFood.getFoodId(), newComplexFoodEntity.getFoodId());
            LOG.debug("Import of simple food has completed (with generated ID {})", newComplexFoodEntity.getFoodId());
        }

        return importedComplexFoods;
    }
}
