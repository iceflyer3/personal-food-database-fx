package com.iceflyer3.pfd.data.request.mealplanning.meal;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingMealViewModel;

import java.util.UUID;

/**
 * Request used to create a meal for a day on a meal plan
 * that is also utilizing a carb cycling plan.
 */
public class CreateCarbCyclingMealRequest extends CreateMealRequest {

    private final CarbCyclingMealViewModel carbCyclingMeal;

    /**
     * Instantiate a new request for creation of a meal that should
     * enforce carb cycling restrictions.
     * @param forDayId The Id of the day to which the meal belongs
     * @param mealName The name of the meal
     * @param carbCyclingMealViewModel The view model that contains the details of the
     *                                 carb cycling meal to create along side the meal
     *                                 planning meal.
     */
    public CreateCarbCyclingMealRequest(UUID forDayId, String mealName, CarbCyclingMealViewModel carbCyclingMealViewModel) {
        super(forDayId, mealName);
        this.carbCyclingMeal = carbCyclingMealViewModel;
    }

    public CarbCyclingMealViewModel getCarbCyclingMeal() {
        return carbCyclingMeal;
    }

    @Override
    public String toString()
    {
        return "CreateCarbCyclingMealRequest{" + "forDayId=" + forDayId + ", mealName='" + mealName + '\'' + ", carbCyclingMeal=" + carbCyclingMeal + '}';
    }
}
