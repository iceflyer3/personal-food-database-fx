/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.entities.recipe;

import com.iceflyer3.pfd.constants.QueryConstants;
import com.iceflyer3.pfd.enums.UnitOfMeasure;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.util.UUID;

@Entity
@Table(name = "recipe_serving_size")
@NamedQuery(
        name = QueryConstants.QUERY_RECIPE_FIND_SERVING_SIZES_FOR_RECIPE,
        query = "SELECT recipeServingSize FROM RecipeServingSize recipeServingSize WHERE recipeServingSize.recipe.recipeId = :recipeId")
@NamedQuery(
        name = QueryConstants.QUERY_RECIPE_FIND_SERVING_SIZES_FOR_USER,
        query = "SELECT recipeServingSize FROM RecipeServingSize recipeServingSize WHERE recipeServingSize.recipe.createdUser.userId = :userId")
public class RecipeServingSize {

    public RecipeServingSize() { }

    public RecipeServingSize(Recipe forRecipe, UnitOfMeasure uom, BigDecimal servingSize)
    {
        this(null, forRecipe, uom, servingSize);
    }

    public RecipeServingSize(
            UUID servingSizeId,
            Recipe forRecipe,
            UnitOfMeasure uom,
            BigDecimal servingSize
    )
    {
        this.servingSizeId = servingSizeId;
        this.recipe = forRecipe;
        this.unitOfMeasure = uom;
        this.servingSize = servingSize;
    }

    @Id
    @GeneratedValue
    @Column(name = "serving_size_id")
    protected UUID servingSizeId;

    @Enumerated(EnumType.STRING)
    @Column(name = "unit_of_measure", nullable = false)
    protected UnitOfMeasure unitOfMeasure;

    @Column(name = "serving_size", nullable = false, precision = 2)
    protected BigDecimal servingSize;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "recipe_id", nullable = false)
    protected Recipe recipe;

    public Recipe getRecipe() {
        return recipe;
    }

    public UUID getServingSizeId() {
        return servingSizeId;
    }

    public void setServingSizeId(UUID servingSizeId) {
        this.servingSizeId = servingSizeId;
    }

    public UnitOfMeasure getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(UnitOfMeasure unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    public BigDecimal getServingSize() {
        return servingSize;
    }

    public void setServingSize(BigDecimal servingSize) {
        this.servingSize = servingSize;
    }
}
