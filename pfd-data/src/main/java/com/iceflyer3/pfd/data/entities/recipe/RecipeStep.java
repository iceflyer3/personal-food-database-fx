/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.entities.recipe;

import com.iceflyer3.pfd.constants.QueryConstants;

import jakarta.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "recipe_step")
@NamedQuery(
        name = QueryConstants.QUERY_RECIPE_FIND_STEPS_FOR_RECIPE,
        query = "SELECT recipeStep FROM RecipeStep recipeStep WHERE recipeStep.recipe.recipeId = :recipeId")
@NamedQuery(
        name = QueryConstants.QUERY_RECIPE_FIND_STEPS_FOR_USER,
        query = "SELECT recipeStep FROM RecipeStep recipeStep WHERE recipeStep.recipe.createdUser.userId = :userId")
public class RecipeStep {

    public RecipeStep() { }

    public RecipeStep(Recipe forRecipe, String stepName, String directions, Integer stepNumber)
    {
        this(null, forRecipe, stepName, directions, stepNumber);
    }

    public RecipeStep(UUID stepId, Recipe forRecipe, String stepName, String directions, Integer stepNumber)
    {
        this.stepId = stepId;
        this.recipe = forRecipe;
        this.name = stepName;
        this.directions = directions;
        this.stepNumber = stepNumber;
    }

    @Id
    @GeneratedValue
    @Column(name = "recipe_step_id")
    protected UUID stepId;

    @Column(name = "name", nullable = false)
    protected String name;

    @Column(name = "step_number", nullable = false)
    protected Integer stepNumber;

    @Column(name = "directions", nullable = false)
    protected String directions;

    @OneToMany(mappedBy = "recipeStep", fetch = FetchType.EAGER)
    protected Set<RecipeStepIngredient> stepIngredients = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "recipe_id")
    protected Recipe recipe;

    public UUID getStepId() {
        return stepId;
    }

    public Set<RecipeStepIngredient> getStepIngredients() {
        return stepIngredients;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStepNumber() {
        return stepNumber;
    }

    public void setStepNumber(Integer stepNumber) {
        this.stepNumber = stepNumber;
    }

    public String getDirections() {
        return directions;
    }

    public void setDirections(String directions) {
        this.directions = directions;
    }
}
