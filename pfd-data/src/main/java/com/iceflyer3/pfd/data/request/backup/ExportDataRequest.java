/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.request.backup;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

public class ExportDataRequest
{
    private final BooleanProperty includeFoods;
    private final BooleanProperty includeMealPlans;
    private final BooleanProperty includeRecipes;

    public ExportDataRequest()
    {
        includeFoods = new SimpleBooleanProperty(false);
        includeMealPlans = new SimpleBooleanProperty(false);
        includeRecipes = new SimpleBooleanProperty(false);
    }

    public boolean shouldIncludeFoods()
    {
        return includeFoods.get();
    }

    public BooleanProperty includeFoodsProperty()
    {
        return includeFoods;
    }

    public void setIncludeFoods(boolean includeFoods)
    {
        this.includeFoods.set(includeFoods);
    }

    public boolean shouldIncludeMealPlans()
    {
        return includeMealPlans.get();
    }

    public void setIncludeMealPlans(boolean includeMealPlans)
    {
        this.includeMealPlans.set(includeMealPlans);
    }

    public BooleanProperty includeMealPlansProperty()
    {
        return includeMealPlans;
    }

    public boolean shouldIncludeRecipes()
    {
        return includeRecipes.get();
    }

    public void setIncludeRecipes(boolean includeRecipes)
    {
        this.includeRecipes.set(includeRecipes);
    }

    public BooleanProperty includeRecipesProperty()
    {
        return includeRecipes;
    }

    /**
     * Returns true if any data has been selected for export or false otherwise. 
     */
    public boolean hasExportSelections()
    {
        return includeFoods.get() || includeMealPlans.get() || includeRecipes.get();
    }
}
