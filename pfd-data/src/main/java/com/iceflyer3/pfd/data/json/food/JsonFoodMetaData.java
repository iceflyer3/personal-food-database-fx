/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.json.food;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class JsonFoodMetaData
{
    private UUID foodId;
    private String foodType;

    @JsonCreator
    public JsonFoodMetaData(
            @JsonProperty("foodId") UUID foodId,
            @JsonProperty("foodType") String foodType)
    {
        this.foodId = foodId;
        this.foodType = foodType;
    }

    public UUID getFoodId()
    {
        return foodId;
    }

    public void setFoodId(UUID foodId)
    {
        this.foodId = foodId;
    }

    public String getFoodType()
    {
        return foodType;
    }

    public void setFoodType(String foodType)
    {
        this.foodType = foodType;
    }
}
