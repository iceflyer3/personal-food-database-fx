package com.iceflyer3.pfd.data.task.recipe;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.entities.recipe.Recipe;
import com.iceflyer3.pfd.data.entities.views.RecipeNutritionalSummary;
import com.iceflyer3.pfd.data.mapping.recipe.RecipeMapper;
import com.iceflyer3.pfd.data.mapping.summary.NutritionalSummaryMapper;
import com.iceflyer3.pfd.data.repository.FoodRepository;
import com.iceflyer3.pfd.data.repository.RecipeRepository;
import com.iceflyer3.pfd.exception.InvalidApplicationStateException;
import com.iceflyer3.pfd.ui.model.viewmodel.recipe.RecipeViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * A task that loads all of the recipes created by a user.
 *
 * The ingredients list for each recipe is not included with
 * any of the loaded recipes.
 */
public class GetAllRecipesForUserTask extends AbstractRecipeTask<List<RecipeViewModel>> {

    private final UUID forUserId;

    public GetAllRecipesForUserTask(RecipeRepository recipeRepository, FoodRepository foodRepository, UUID forUserId) {
        super(recipeRepository, foodRepository);
        this.forUserId = forUserId;
    }

    @Override
    protected List<RecipeViewModel> call() throws Exception {
        List<Recipe> recipeEntities = recipeRepository.getAllRecipesForUser(forUserId);
        List<RecipeNutritionalSummary> nutritionalSummaryEntities = recipeRepository.getAllNutritionalSummaries();

        List<RecipeViewModel> viewModels = new ArrayList<>();
        for(Recipe recipeEntity : recipeEntities)
        {
            Optional<RecipeNutritionalSummary> nutritionalSummaryEntity = nutritionalSummaryEntities.stream().filter(summary -> summary.getRecipeId().equals(recipeEntity.getRecipeId())).findFirst();
            if (nutritionalSummaryEntity.isPresent())
            {
                viewModels.add(RecipeMapper.toModel(recipeEntity, NutritionalSummaryMapper.toModel(nutritionalSummaryEntity.get())));
            }
            else
            {
                // Unlike meal planning meals (and subsequently days) by the time a recipe has been
                // saved it should always have ingredients from which to derive the nutritional summary.
                // If it doesn't then something has gone wrong.
                throw new InvalidApplicationStateException(String.format("No nutritional summary found for recipe %s", recipeEntity.getRecipeId()));
            }
        }

        return viewModels;
    }
}
