package com.iceflyer3.pfd.data.entities.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.constants.QueryConstants;
import com.iceflyer3.pfd.enums.Nutrient;
import com.iceflyer3.pfd.enums.UnitOfMeasure;
import jakarta.persistence.*;

import java.math.BigDecimal;
import java.util.UUID;

@Entity
@Table(name = "meal_plan_nutrient_suggestion")
@NamedQuery(
        name = QueryConstants.QUERY_MEAL_PLANNING_MACRO_SUGGESTIONS_FIND_FOR_USER,
        query = "SELECT macroSuggestion FROM MealPlanNutrientSuggestion macroSuggestion WHERE macroSuggestion.mealPlan.user.userId = :userId")
@NamedQuery(
        name = QueryConstants.QUERY_MEAL_PLANNING_MACRO_SUGGESTIONS_REMOVE_ALL_FOR_PLAN,
        query = "DELETE FROM MealPlanNutrientSuggestion macroSuggestion WHERE macroSuggestion.mealPlan.mealPlanId = :mealPlanId")
public class MealPlanNutrientSuggestion
{

    public MealPlanNutrientSuggestion() { }

    public MealPlanNutrientSuggestion(MealPlan mealPlan, Nutrient nutrient, BigDecimal suggestedAmount, UnitOfMeasure unitOfMeasure)
    {
        this(null, mealPlan, nutrient, suggestedAmount, unitOfMeasure);
    }

    public MealPlanNutrientSuggestion(UUID macroId, MealPlan mealPlan, Nutrient nutrient, BigDecimal suggestedAmount, UnitOfMeasure unitOfMeasure)
    {
        this.macroId = macroId;
        this.mealPlan = mealPlan;
        this.nutrient = nutrient;
        this.suggestedAmount = suggestedAmount;
        this.unitOfMeasure = unitOfMeasure;
    }

    @Id
    @GeneratedValue
    @Column(name = "macro_id")
    protected UUID macroId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "meal_plan_id", nullable = false)
    protected MealPlan mealPlan;

    @Enumerated(EnumType.STRING)
    @Column(name = "nutrient_name", nullable = false)
    protected Nutrient nutrient;

    @Column(name = "suggested_amount", nullable = false, precision = 2)
    protected BigDecimal suggestedAmount;

    @Enumerated(EnumType.STRING)
    @Column(name = "unit_of_measure", nullable = false)
    protected UnitOfMeasure unitOfMeasure;

    public UUID getMacroId() {
        return macroId;
    }

    public MealPlan getMealPlan() {
        return mealPlan;
    }

    public void setMealPlan(MealPlan mealPlan) {
        this.mealPlan = mealPlan;
    }

    public Nutrient getNutrient() {
        return nutrient;
    }

    public void setNutrient(Nutrient nutrient) {
        this.nutrient = nutrient;
    }

    public BigDecimal getSuggestedAmount() {
        return suggestedAmount;
    }

    public void setSuggestedAmount(BigDecimal suggestedAmount) {
        this.suggestedAmount = suggestedAmount;
    }

    public UnitOfMeasure getUnitOfMeasure()
    {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(UnitOfMeasure unitOfMeasure)
    {
        this.unitOfMeasure = unitOfMeasure;
    }

    @Override
    public String toString() {
        return "MealPlanNutrientSuggestion{" +
                "macroId=" + macroId +
                ", mealPlan=" + mealPlan +
                ", nutrient=" + nutrient +
                ", suggestedAmount=" + suggestedAmount +
                '}';
    }
}
