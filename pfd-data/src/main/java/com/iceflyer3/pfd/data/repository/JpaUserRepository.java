/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.repository;

import com.iceflyer3.pfd.constants.QueryConstants;
import com.iceflyer3.pfd.data.entities.User;
import com.iceflyer3.pfd.exception.InvalidApplicationStateException;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.persistence.NonUniqueResultException;
import jakarta.persistence.PersistenceContext;
import java.util.List;

@Repository
@Transactional
public class JpaUserRepository implements UserRepository{

    private static final Logger LOG = LoggerFactory.getLogger(JpaUserRepository.class);

    // Unfortunately it seems like the EntityManager has to use field injection instead of constructor
    // See: https://github.com/spring-projects/spring-framework/issues/15076
    @PersistenceContext
    private EntityManager entityManager;

    public JpaUserRepository() { }

    @Override
    public User add(UserViewModel newUser) {
        LOG.debug("UserRepository is saving a new user...");
        User newUserEntity = new User(newUser.getUsername());
        entityManager.persist(newUserEntity);
        LOG.debug("UserRepository has persisted the new user!");
        return newUserEntity;
    }

    @Override
    public List<User> getAll() {
        LOG.debug("Fetching the list of all users");
        return entityManager.createNamedQuery(QueryConstants.QUERY_USER_FIND_ALL, User.class).getResultList();
    }

    @Override
    public User getByUsername(String username) {
        try
        {
            return entityManager
                    .createNamedQuery(QueryConstants.QUERY_USER_BY_USERNAME, User.class)
                    .setParameter(QueryConstants.PARAMETER_USERNAME, username)
                    .getSingleResult();
        }
        catch(NoResultException noResultException)
        {
            LOG.error("No user found for requested username of {}", username);
            throw new InvalidApplicationStateException(String.format("User with username %s was not found", username));
        }
        catch(NonUniqueResultException nonUniqueResultException)
        {
            // Theoretically, this should never happen.
            LOG.error("Found more than one result for the requested username of {}", username);
            throw nonUniqueResultException;
        }
    }
}
