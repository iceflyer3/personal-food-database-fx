package com.iceflyer3.pfd.data.task.mealplanning.day;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanDay;
import com.iceflyer3.pfd.data.entities.views.MealPlanDayNutritionalSummary;
import com.iceflyer3.pfd.data.mapping.mealplanning.MealPlanDayMapper;
import com.iceflyer3.pfd.data.mapping.summary.NutritionalSummaryMapper;
import com.iceflyer3.pfd.data.repository.MealPlanningRepository;
import com.iceflyer3.pfd.data.task.mealplanning.AbstractMealPlanningTask;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanDayViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;

import java.util.Optional;
import java.util.UUID;

public class GetDayForMealPlanByIdTask extends AbstractMealPlanningTask<MealPlanDayViewModel>
{

    private final UUID dayId;

    public GetDayForMealPlanByIdTask(MealPlanningRepository mealPlanningRepository, UUID dayId) {
        super(mealPlanningRepository);
        this.dayId = dayId;
    }

    @Override
    protected MealPlanDayViewModel call() throws Exception {
        MealPlanDay dayEntity = mealPlanningRepository.getDayForId(dayId);
        Optional<MealPlanDayNutritionalSummary> nutritionalSummaryEntity = mealPlanningRepository.getDayNutritionalSummary(dayId);

        if (nutritionalSummaryEntity.isPresent())
        {
            return MealPlanDayMapper.toModel(dayEntity, NutritionalSummaryMapper.toModel(nutritionalSummaryEntity.get()));
        }
        else
        {
            return MealPlanDayMapper.toModel(dayEntity, NutritionalSummary.empty());
        }

    }
}
