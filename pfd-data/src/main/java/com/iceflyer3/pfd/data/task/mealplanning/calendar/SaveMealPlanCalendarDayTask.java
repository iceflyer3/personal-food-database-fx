/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.task.mealplanning.calendar;

import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanCalendarDay;
import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanDay;
import com.iceflyer3.pfd.data.entities.views.CarbCyclingDayNutritionalSummary;
import com.iceflyer3.pfd.data.entities.views.MealPlanDayNutritionalSummary;
import com.iceflyer3.pfd.data.mapping.mealplanning.MealPlanCalendarDayMapper;
import com.iceflyer3.pfd.data.mapping.mealplanning.MealPlanDayMapper;
import com.iceflyer3.pfd.data.mapping.summary.NutritionalSummaryMapper;
import com.iceflyer3.pfd.data.repository.MealPlanningRepository;
import com.iceflyer3.pfd.data.task.mealplanning.AbstractMealPlanningTask;
import com.iceflyer3.pfd.data.request.mealplanning.AssociateMealPlanDayToCalendarDayRequest;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanCalendarDayViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanDayViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SaveMealPlanCalendarDayTask extends AbstractMealPlanningTask<List<MealPlanCalendarDayViewModel>>
{
    private final AssociateMealPlanDayToCalendarDayRequest request;

    public SaveMealPlanCalendarDayTask(MealPlanningRepository mealPlanningRepository, AssociateMealPlanDayToCalendarDayRequest request)
    {
        super(mealPlanningRepository);
        this.request = request;
    }

    @Override
    protected List<MealPlanCalendarDayViewModel> call()
    {
        List<MealPlanCalendarDay> savedDays = mealPlanningRepository.createCalendarDays(request);
        List<MealPlanCalendarDayViewModel> viewModels = new ArrayList<>();

        for(MealPlanCalendarDay savedDay : savedDays)
        {
            NutritionalSummary nutritionalSummary;
            MealPlanDay plannedDay = savedDay.getPlannedDay();
            if (plannedDay.getCarbCyclingDay() == null)
            {
                Optional<MealPlanDayNutritionalSummary> summaryEntity = mealPlanningRepository.getDayNutritionalSummary(plannedDay.getDayId());
                nutritionalSummary = summaryEntity.isPresent() ? NutritionalSummaryMapper.toModel(summaryEntity.get()) : NutritionalSummary.empty();
            }
            else
            {
                Optional<CarbCyclingDayNutritionalSummary> summaryEntity = mealPlanningRepository.getCarbCyclingDayNutritionalSummary(plannedDay.getDayId());
                nutritionalSummary = summaryEntity.isPresent() ? NutritionalSummaryMapper.toModel(summaryEntity.get()) : NutritionalSummary.empty();
            }

            MealPlanDayViewModel plannedDayViewModel = MealPlanDayMapper.toModel(plannedDay, nutritionalSummary);
            viewModels.add(MealPlanCalendarDayMapper.toModel(savedDay, plannedDayViewModel));
        }

        return viewModels;
    }
}
