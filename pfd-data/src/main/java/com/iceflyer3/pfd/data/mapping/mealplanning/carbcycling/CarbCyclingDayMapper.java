package com.iceflyer3.pfd.data.mapping.mealplanning.carbcycling;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanDay;
import com.iceflyer3.pfd.data.entities.mealplanning.carbcycling.CarbCyclingDay;
import com.iceflyer3.pfd.data.entities.mealplanning.carbcycling.CarbCyclingPlan;
import com.iceflyer3.pfd.data.json.mealplanning.carbcycling.JsonCarbCyclingDay;
import com.iceflyer3.pfd.enums.CarbCyclingDayType;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingDayViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;

public class CarbCyclingDayMapper
{
    private CarbCyclingDayMapper() {}

    public static CarbCyclingDayViewModel toModel(MealPlanDay entity, NutritionalSummary nutritionalSummary)
    {
        CarbCyclingDayViewModel carbCyclingDayViewModel = new CarbCyclingDayViewModel(entity.getDayId(), entity.getMealPlan().getMealPlanId(), entity.getCarbCyclingDay().getCarbCyclingPlan().getPlanId(), nutritionalSummary);
        carbCyclingDayViewModel.setDayLabel(entity.getDayLabel());
        carbCyclingDayViewModel.setFitnessGoal(entity.getDayFitnessGoal());
        carbCyclingDayViewModel.setLastModifiedDate(entity.getLastModifiedDate());
        carbCyclingDayViewModel.setDayType(entity.getCarbCyclingDay().getDayType());
        carbCyclingDayViewModel.setRequiredCalories(entity.getCarbCyclingDay().getRequiredCalories());
        carbCyclingDayViewModel.setRequiredProtein(entity.getCarbCyclingDay().getRequiredProtein());
        carbCyclingDayViewModel.setRequiredCarbs(entity.getCarbCyclingDay().getRequiredCarbs());
        carbCyclingDayViewModel.setRequiredFats(entity.getCarbCyclingDay().getRequiredFats());

        if (entity.getCarbCyclingDay().isTrainingDay())
        {
            carbCyclingDayViewModel.toggleTrainingDay();
        }

        return carbCyclingDayViewModel;
    }

    public static JsonCarbCyclingDay toJson(CarbCyclingDay day)
    {
        return new JsonCarbCyclingDay(
            day.getDayId(),
            day.getMealPlanningDay().getDayId(),
            day.getCarbCyclingPlan().getPlanId(),
            day.getDayType().toString(),
            day.isTrainingDay(),
            day.getRequiredCalories(),
            day.getRequiredProtein(),
            day.getRequiredCarbs(),
            day.getRequiredFats()
        );
    }

    public static CarbCyclingDay toFreshEntity(JsonCarbCyclingDay jsonCarbCyclingDay, MealPlanDay forMealPlanDay, CarbCyclingPlan forCarbCyclingPlan)
    {
        return new CarbCyclingDay(
                null,
                forMealPlanDay,
                forCarbCyclingPlan,
                CarbCyclingDayType.valueOf(jsonCarbCyclingDay.dayType()),
                jsonCarbCyclingDay.isTrainingDay(),
                jsonCarbCyclingDay.requiredCalories(),
                jsonCarbCyclingDay.requiredProtein(),
                jsonCarbCyclingDay.requiredCarbs(),
                jsonCarbCyclingDay.requiredFats()
        );
    }
}
