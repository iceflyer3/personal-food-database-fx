package com.iceflyer3.pfd.data.calculator.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.enums.CarbCyclingDayType;
import com.iceflyer3.pfd.enums.FitnessGoal;

import java.math.BigDecimal;

public class CarbCyclingDayRequirementsCalcuationRequest
{
    private final FitnessGoal fitnessGoal;
    private final CarbCyclingDayType dayType;
    private final BigDecimal tdeeCalories;
    private final BigDecimal bodyWeight;

    public CarbCyclingDayRequirementsCalcuationRequest(FitnessGoal fitnessGoal, CarbCyclingDayType dayType, BigDecimal tdeeCalories, BigDecimal bodyWeight)
    {
        this.fitnessGoal = fitnessGoal;
        this.dayType = dayType;
        this.tdeeCalories = tdeeCalories;
        this.bodyWeight = bodyWeight;
    }

    public FitnessGoal getFitnessGoal()
    {
        return fitnessGoal;
    }

    public CarbCyclingDayType getDayType()
    {
        return dayType;
    }

    public BigDecimal getTdeeCalories()
    {
        return tdeeCalories;
    }

    public BigDecimal getBodyWeight()
    {
        return bodyWeight;
    }
}
