package com.iceflyer3.pfd.data.request.mealplanning.day;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import java.util.UUID;

/**
 * Request used to modify a meal after initial creation.
 * The only mutable part of the meal after initial creation
 * is the label / name of the day.
 */
public class ModifyDayRequest {

    private final UUID dayId;
    private final String newLabel;

    public ModifyDayRequest(UUID dayId, String newLabel) {
        this.dayId = dayId;
        this.newLabel = newLabel;
    }

    public UUID getDayId() {
        return dayId;
    }

    public String getNewLabel() {
        return newLabel;
    }
}
