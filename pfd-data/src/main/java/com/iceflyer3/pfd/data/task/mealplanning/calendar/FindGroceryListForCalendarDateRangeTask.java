/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.data.task.mealplanning.calendar;

import com.iceflyer3.pfd.data.entities.food.ComplexFoodIngredient;
import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanCalendarDay;
import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanMeal;
import com.iceflyer3.pfd.data.entities.mealplanning.MealPlanMealIngredient;
import com.iceflyer3.pfd.enums.FoodType;
import com.iceflyer3.pfd.data.mapping.IngredientMapper;
import com.iceflyer3.pfd.data.repository.FoodRepository;
import com.iceflyer3.pfd.data.repository.MealPlanningRepository;
import com.iceflyer3.pfd.data.request.mealplanning.GroceryListForMealPlanCalendarDateRangeSearchRequest;
import com.iceflyer3.pfd.data.task.mealplanning.AbstractMealPlanningTask;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.viewmodel.IngredientViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.ReadOnlyFoodViewModel;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public class FindGroceryListForCalendarDateRangeTask extends AbstractMealPlanningTask<List<IngredientModel>>
{
    private final static Logger LOG = LoggerFactory.getLogger(FindGroceryListForCalendarDateRangeTask.class);

    private final FoodRepository foodRepository;
    private final GroceryListForMealPlanCalendarDateRangeSearchRequest searchRequest;

    public FindGroceryListForCalendarDateRangeTask(
            MealPlanningRepository mealPlanningRepository,
            FoodRepository foodRepository,
            GroceryListForMealPlanCalendarDateRangeSearchRequest searchRequest)
    {
        super(mealPlanningRepository);
        this.foodRepository = foodRepository;
        this.searchRequest = searchRequest;
    }

    @Override
    protected List<IngredientModel> call() throws Exception
    {
        // Find the list of all raw ingredients
        List<IngredientModel> rawIngredientList = getRawIngredientsList();

        // Combine any ingredients that use the same food into a single ingredient
        List<IngredientModel> groceryList = new ArrayList<>();
        List<UUID> distinctIngredientFoodIds = rawIngredientList.stream().map(ingredient -> ingredient.getIngredientFood().getId()).distinct().toList();

        for(UUID foodId : distinctIngredientFoodIds)
        {
            List<IngredientModel> ingredients = rawIngredientList.stream().filter(ingredient -> ingredient.getIngredientFood().getId().equals(foodId)).toList();

            // Calculate total servings
            BigDecimal servingsIncluded = ingredients.stream().map(IngredientModel::getServingsIncluded).reduce(BigDecimal.ZERO, BigDecimal::add);

            // Create new condensed view model
            IngredientModel firstIngredient = ingredients.get(0);
            IngredientModel distinctIngredient = new IngredientViewModel(firstIngredient.getIngredientFood());
            distinctIngredient.setServingsIncluded(servingsIncluded);
            // TODO: When functionality for Alternate and Optional ingredients is added this should probably be changed.
            //       For now it works fine because there isn't yet functionality that uses this information.
            distinctIngredient.setIsAlternate(firstIngredient.isAlternate());
            distinctIngredient.setIsOptional(firstIngredient.isOptional());

            groceryList.add(distinctIngredient);
        }

        return groceryList;
    }

    private List<IngredientModel> getRawIngredientsList()
    {
        List<IngredientModel> rawIngredientList = new ArrayList<>();
        List<MealPlanCalendarDay> daysInRange = mealPlanningRepository.getMealPlanCalendarDaysInRange(searchRequest.getStartDate(), searchRequest.getEndDate());
        LOG.debug("Found {} MealPlanCalendarDays in date range {} to {}", daysInRange.size(), searchRequest.getStartDate(), searchRequest.getEndDate());

        // Look up all ingredients for all meals for all planned days within the date range that have been associated to a calendar day
        for(MealPlanCalendarDay calendarDay : daysInRange)
        {
            LOG.debug("Processing day: {}", calendarDay.getPlannedDay().getDayLabel());
            List<MealPlanMeal> mealsForDay = mealPlanningRepository.getMealsForDay(calendarDay.getPlannedDay().getDayId());
            LOG.debug("Found {} MealPlanMeals for day {}", mealsForDay.size(), calendarDay.getPlannedDay().getDayLabel());
            for(MealPlanMeal mealForDay : mealsForDay)
            {
                List<MealPlanMealIngredient> ingredientEntities = mealPlanningRepository.getIngredientsForMeal(mealForDay.getMealId());
                LOG.debug("Found {} ingredients for meal {}", ingredientEntities.size(), mealForDay.getMealName());
                for(MealPlanMealIngredient mealIngredient : ingredientEntities)
                {
                    LOG.debug("Processing meal ingredient...");
                    if (mealIngredient.getFoodMeta().getFoodType() == FoodType.SIMPLE)
                    {
                        ReadOnlyFoodViewModel foodIngredientViewModel = foodRepository.getFoodForId(mealIngredient.getFoodMeta().getFoodId());
                        LOG.debug("Ingredient is simple food. Found read only food of {}", foodIngredientViewModel.getName());
                        LOG.debug("Adding ingredient to raw ingredient list: {}",IngredientMapper.toModel(mealIngredient, foodIngredientViewModel));
                        rawIngredientList.add(IngredientMapper.toModel(mealIngredient, foodIngredientViewModel));
                    }
                    else
                    {
                        List<ComplexFoodIngredient> complexFoodIngredients = foodRepository.getIngredientsForComplexFood(mealIngredient.getFoodMeta().getFoodId());
                        LOG.debug("Ingredient is complex food. Found {} complex food ingredients", complexFoodIngredients.size());
                        for (ComplexFoodIngredient complexFoodIngredient : complexFoodIngredients)
                        {
                            LOG.debug("Adding ingredient to raw ingredient list: {}",IngredientMapper.toModel(complexFoodIngredient));
                            rawIngredientList.add(IngredientMapper.toModel(complexFoodIngredient));
                        }
                    }
                }
            }
        }

        // Filter out ingredients as needed based upon request search criteria
        if (!searchRequest.shouldIncludeAlternateIngredients())
        {
            rawIngredientList = rawIngredientList.stream().filter(ingredient -> !ingredient.isAlternate()).toList();
            LOG.debug("Raw ingredient list after removing alternate ingredients has size {}", rawIngredientList.size());
        }

        if (!searchRequest.shouldIncludeOptionalIngredients())
        {
            rawIngredientList = rawIngredientList.stream().filter(ingredient -> !ingredient.isOptional()).toList();
            LOG.debug("Raw ingredient list after removing optional ingredients has size {}", rawIngredientList.size());
        }

        return rawIngredientList;
    }
}
