package com.iceflyer3.pfd.data.mapping.recipe;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.data.entities.User;
import com.iceflyer3.pfd.data.entities.recipe.Recipe;
import com.iceflyer3.pfd.data.json.recipe.JsonRecipe;
import com.iceflyer3.pfd.data.mapping.ServingSizeMapper;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.api.recipe.RecipeStepModel;
import com.iceflyer3.pfd.ui.model.viewmodel.ServingSizeViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.recipe.RecipeViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;

import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class RecipeMapper {

    private RecipeMapper() {}

    /**
     * Maps a recipe to a mutable view model that does not include the ingredients
     * or the steps.
     *
     * @param recipe The recipe instance to map
     * @return A new RecipeViewModel instance
     */
    public static RecipeViewModel toModel(Recipe recipe, NutritionalSummary nutritionalSummary)
    {
        return toModel(recipe, nutritionalSummary,null, null);
    }

    /**
     * Maps a recipe to a mutable view model that also includes the ingredients
     * and the steps.
     *
     * Due to extra database lookups that are needed for the actual foods included
     * in each ingredient in the recipe a list of the already mapped ingredients
     * must be passed in separately.
     *
     * @param nutritionalSummary The nutritional summary of the recipe
     * @param recipe The recipe instance to map
     * @param ingredients The mapped list of ingredients for the recipe
     * @param steps The mapped list of steps for the recipe
     * @return A new RecipeViewModel instance
     */
    public static RecipeViewModel toModel(
            Recipe recipe,
            NutritionalSummary nutritionalSummary,
            Collection<IngredientModel> ingredients,
            Collection<RecipeStepModel> steps)
    {
        /*
         * The ingredients for the meal view model are not mapped here because it is a bit involved
         * due to needing to look up the actual food for each ingredient. The mappings in the entity
         * only store the FoodMetaData which is insufficient to do the mapping for the ingredients.
         */
        List<ServingSizeViewModel> servingSizes = recipe.getServingSizes().stream().map(ServingSizeMapper::toModel).collect(Collectors.toList());
        RecipeViewModel recipeViewModel = new RecipeViewModel(
                recipe.getRecipeId(),
                recipe.getRecipeName(),
                recipe.getSource(),
                recipe.getNumberOfServingsMade(),
                recipe.getCreatedUser().getUsername(),
                recipe.getCreatedDate(),
                recipe.getLastModifiedUser().getUsername(),
                recipe.getLastModifiedDate(),
                servingSizes,
                nutritionalSummary);

        if (ingredients != null)
        {
            recipeViewModel.setIngredients(ingredients);
        }

        if (steps != null)
        {
            recipeViewModel.setSteps(steps);
        }

        return recipeViewModel;
    }

    public static JsonRecipe toJson(Recipe recipe)
    {
        return new JsonRecipe(
                recipe.getRecipeId(),
                recipe.getRecipeName(),
                recipe.getSource(),
                recipe.getNumberOfServingsMade(),
                recipe.getCreatedUser().getUserId(),
                recipe.getLastModifiedUser().getUserId(),
                recipe.getCreatedDate(),
                recipe.getLastModifiedDate()
        );
    }

    public static Recipe toEntity(JsonRecipe jsonRecipe, User createdUser, User lastModifiedUser)
    {
        return mapJsonToEntity(jsonRecipe, jsonRecipe.getRecipeId(), createdUser, lastModifiedUser);
    }

    /**
     * Returns a new (and detached) Recipe instance hydrated from json without a set id.
     * @param jsonRecipe json from which to hydrate the new instance
     * @param createdUser Entity that represents the user who created the recipe
     * @param lastModifiedUser Entity that represents the user who last modified the recipe
     * @return Recipe instance hydrated from json without a set id.
     */
    public static Recipe toFreshEntity(JsonRecipe jsonRecipe, User createdUser, User lastModifiedUser)
    {
        return mapJsonToEntity(jsonRecipe, null, createdUser, lastModifiedUser);
    }

    private static Recipe mapJsonToEntity(JsonRecipe jsonRecipe, UUID recipeId, User createdUser, User lastModifiedUser)
    {
        return new Recipe(
                recipeId,
                createdUser,
                lastModifiedUser,
                jsonRecipe.getRecipeName(),
                jsonRecipe.getSource(),
                jsonRecipe.getNumberOfServingsMade(),
                jsonRecipe.getCreatedDate(),
                jsonRecipe.getLastModifiedDate()
        );
    }
}
