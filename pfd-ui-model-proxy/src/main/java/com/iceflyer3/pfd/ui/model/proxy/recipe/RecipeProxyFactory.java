package com.iceflyer3.pfd.ui.model.proxy.recipe;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.data.factory.RecipeTaskFactory;
import com.iceflyer3.pfd.data.task.recipe.GetAllRecipesForUserTask;
import com.iceflyer3.pfd.data.task.recipe.GetMutableRecipeByIdTask;
import com.iceflyer3.pfd.data.task.recipe.GetReadOnlyRecipeByIdTask;
import com.iceflyer3.pfd.ui.model.api.proxy.ProxyDataResultCallback;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class RecipeProxyFactory {

    private final static Logger LOG = LoggerFactory.getLogger(RecipeProxyFactory.class);

    private final TaskExecutor taskExecutor;
    private final RecipeTaskFactory recipeTaskFactory;

    public RecipeProxyFactory(TaskExecutor taskExecutor, RecipeTaskFactory recipeTaskFactory) {
        this.taskExecutor = taskExecutor;
        this.recipeTaskFactory = recipeTaskFactory;
    }

    /**
     * Gets the list of all recipes for a user as ReadOnlyRecipeModelProxy instances
     * @param forUserId UserId of the user to look up the recipes for
     * @param callback Callback used to receive the result
     */
    public void getAllRecipesForUser(UUID forUserId, ProxyDataResultCallback<List<ReadOnlyRecipeModelProxy>> callback)
    {
        GetAllRecipesForUserTask getAllRecipesForUserTask = recipeTaskFactory.getAllRecipesForUserAsync(forUserId);
        getAllRecipesForUserTask.setOnSucceeded(event -> {
            List<ReadOnlyRecipeModelProxy> proxies = getAllRecipesForUserTask
                                                            .getValue()
                                                            .stream()
                                                            .map(readOnlyRecipeViewModel -> new ReadOnlyRecipeModelProxy(taskExecutor, recipeTaskFactory, readOnlyRecipeViewModel))
                                                            .collect(Collectors.toList());
            callback.receiveResult(true, proxies);
        });
        getAllRecipesForUserTask.setOnFailed(event -> {
            LOG.error("An error has occurred while retrieving the list of all recipes", getAllRecipesForUserTask.getException());
            callback.receiveResult(false, null);
        });
        taskExecutor.execute(getAllRecipesForUserTask);
    }

    public void getMutableRecipe(UUID recipeId, UserViewModel createdUser, ProxyDataResultCallback<RecipeModelProxy> callback)
    {
        if (recipeId == null)
        {
            RecipeModelProxy proxy = new RecipeModelProxy(taskExecutor, recipeTaskFactory, createdUser);
            callback.receiveResult(true, proxy);
        }
        else
        {
            GetMutableRecipeByIdTask getMutableRecipeByIdTask = recipeTaskFactory.getMutableRecipeByIdAsync(recipeId);
            getMutableRecipeByIdTask.setOnSucceeded(event -> {
                RecipeModelProxy proxy = new RecipeModelProxy(taskExecutor, recipeTaskFactory, createdUser, getMutableRecipeByIdTask.getValue());
                callback.receiveResult(true, proxy);
            });
            getMutableRecipeByIdTask.setOnFailed(event -> {
                LOG.error("An error has occurred while retrieving a recipe ({})", recipeId, getMutableRecipeByIdTask.getException());
                callback.receiveResult(false, null);
            });
            taskExecutor.execute(getMutableRecipeByIdTask);
        }
    }

    public void getReadOnlyRecipe(UUID recipeId, ProxyDataResultCallback<ReadOnlyRecipeModelProxy> callback)
    {
        GetReadOnlyRecipeByIdTask getReadOnlyRecipeByIdTask = recipeTaskFactory.getReadOnlyRecipeByIdAsync(recipeId);
        getReadOnlyRecipeByIdTask.setOnSucceeded(event -> {
            ReadOnlyRecipeModelProxy proxy = new ReadOnlyRecipeModelProxy(taskExecutor, recipeTaskFactory, getReadOnlyRecipeByIdTask.getValue());
            callback.receiveResult(true, proxy);
        });
        getReadOnlyRecipeByIdTask.setOnFailed(event -> {
            LOG.error("An error has occurred while retrieving a recipe ({})", recipeId, getReadOnlyRecipeByIdTask.getException());
            callback.receiveResult(false, null);
        });
        taskExecutor.execute(getReadOnlyRecipeByIdTask);
    }
}
