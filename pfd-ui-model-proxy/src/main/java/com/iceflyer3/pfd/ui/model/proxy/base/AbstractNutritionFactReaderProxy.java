package com.iceflyer3.pfd.ui.model.proxy.base;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.ui.model.api.NutritionFactsReader;
import javafx.beans.property.ReadOnlyObjectProperty;
import org.springframework.core.task.TaskExecutor;

import java.math.BigDecimal;

/**
 * Base class for proxy types that also need to report nutritional fact information from
 * the view model which they wrap.
 *
 * @param <ModelType> The type of model that this proxy is for
 */
public abstract class AbstractNutritionFactReaderProxy<ModelType extends NutritionFactsReader>
        extends AbstractModelProxy<ModelType>
        implements NutritionFactsReader
{
    protected AbstractNutritionFactReaderProxy(TaskExecutor taskExecutor)
    {
        super(taskExecutor);
    }

    @Override
    public BigDecimal getCalories()
    {
        validateProxyPropertyInvocation();
        return viewModel.getCalories();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> caloriesProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.caloriesProperty();
    }

    @Override
    public BigDecimal getProtein()
    {
        validateProxyPropertyInvocation();
        return viewModel.getProtein();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> proteinProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.proteinProperty();
    }

    @Override
    public BigDecimal getTotalFats()
    {
        validateProxyPropertyInvocation();
        return viewModel.getTotalFats();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> totalFatsProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.totalFatsProperty();
    }

    @Override
    public BigDecimal getTransFat()
    {
        validateProxyPropertyInvocation();
        return viewModel.getTransFat();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> transFatProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.transFatProperty();
    }

    @Override
    public BigDecimal getMonounsaturatedFat()
    {
        validateProxyPropertyInvocation();
        return viewModel.getMonounsaturatedFat();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> monounsaturatedFatProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.monounsaturatedFatProperty();
    }

    @Override
    public BigDecimal getPolyunsaturatedFat()
    {
        validateProxyPropertyInvocation();
        return viewModel.getPolyunsaturatedFat();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> polyunsaturatedFatProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.polyunsaturatedFatProperty();
    }

    @Override
    public BigDecimal getSaturatedFat()
    {
        validateProxyPropertyInvocation();
        return viewModel.getSaturatedFat();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> saturatedFatProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.saturatedFatProperty();
    }

    @Override
    public BigDecimal getCarbohydrates()
    {
        validateProxyPropertyInvocation();
        return viewModel.getCarbohydrates();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> carbohydratesProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.carbohydratesProperty();
    }

    @Override
    public BigDecimal getSugars()
    {
        validateProxyPropertyInvocation();
        return viewModel.getSugars();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> sugarsProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.sugarsProperty();
    }

    @Override
    public BigDecimal getFiber()
    {
        validateProxyPropertyInvocation();
        return viewModel.getFiber();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> fiberProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.fiberProperty();
    }

    @Override
    public BigDecimal getCholesterol()
    {
        validateProxyPropertyInvocation();
        return viewModel.getCholesterol();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> cholesterolProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.cholesterolProperty();
    }

    @Override
    public BigDecimal getIron()
    {
        validateProxyPropertyInvocation();
        return viewModel.getIron();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> ironProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.ironProperty();
    }

    @Override
    public BigDecimal getManganese()
    {
        validateProxyPropertyInvocation();
        return viewModel.getManganese();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> manganeseProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.manganeseProperty();
    }

    @Override
    public BigDecimal getCopper()
    {
        validateProxyPropertyInvocation();
        return viewModel.getCopper();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> copperProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.copperProperty();
    }

    @Override
    public BigDecimal getZinc()
    {
        validateProxyPropertyInvocation();
        return viewModel.getZinc();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> zincProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.zincProperty();
    }

    @Override
    public BigDecimal getCalcium()
    {
        validateProxyPropertyInvocation();
        return viewModel.getCalcium();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> calciumProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.calciumProperty();
    }

    @Override
    public BigDecimal getMagnesium()
    {
        validateProxyPropertyInvocation();
        return viewModel.getMagnesium();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> magnesiumProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.magnesiumProperty();
    }

    @Override
    public BigDecimal getPhosphorus()
    {
        validateProxyPropertyInvocation();
        return viewModel.getPhosphorus();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> phosphorusProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.phosphorusProperty();
    }

    @Override
    public BigDecimal getPotassium()
    {
        validateProxyPropertyInvocation();
        return viewModel.getPotassium();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> potassiumProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.potassiumProperty();
    }

    @Override
    public BigDecimal getSodium()
    {
        validateProxyPropertyInvocation();
        return viewModel.getSodium();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> sodiumProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.sodiumProperty();
    }

    @Override
    public BigDecimal getSulfur()
    {
        validateProxyPropertyInvocation();
        return viewModel.getSulfur();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> sulfurProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.sulfurProperty();
    }

    @Override
    public BigDecimal getVitaminA()
    {
        validateProxyPropertyInvocation();
        return viewModel.getVitaminA();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> vitaminAProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.vitaminAProperty();
    }

    @Override
    public BigDecimal getVitaminC()
    {
        validateProxyPropertyInvocation();
        return viewModel.getVitaminC();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> vitaminCProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.vitaminCProperty();
    }

    @Override
    public BigDecimal getVitaminK()
    {
        validateProxyPropertyInvocation();
        return viewModel.getVitaminK();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> vitaminKProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.vitaminKProperty();
    }

    @Override
    public BigDecimal getVitaminD()
    {
        validateProxyPropertyInvocation();
        return viewModel.getVitaminD();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> vitaminDProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.vitaminDProperty();
    }

    @Override
    public BigDecimal getVitaminE()
    {
        validateProxyPropertyInvocation();
        return viewModel.getVitaminE();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> vitaminEProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.vitaminEProperty();
    }

    @Override
    public BigDecimal getVitaminB1()
    {
        validateProxyPropertyInvocation();
        return viewModel.getVitaminB1();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> vitaminB1Property()
    {
        validateProxyPropertyInvocation();
        return viewModel.vitaminB1Property();
    }

    @Override
    public BigDecimal getVitaminB2()
    {
        validateProxyPropertyInvocation();
        return viewModel.getVitaminB2();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> vitaminB2Property()
    {
        validateProxyPropertyInvocation();
        return viewModel.vitaminB2Property();
    }

    @Override
    public BigDecimal getVitaminB3()
    {
        validateProxyPropertyInvocation();
        return viewModel.getVitaminB3();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> vitaminB3Property()
    {
        validateProxyPropertyInvocation();
        return viewModel.vitaminB3Property();
    }

    @Override
    public BigDecimal getVitaminB5()
    {
        validateProxyPropertyInvocation();
        return viewModel.getVitaminB5();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> vitaminB5Property()
    {
        validateProxyPropertyInvocation();
        return viewModel.vitaminB5Property();
    }

    @Override
    public BigDecimal getVitaminB6()
    {
        validateProxyPropertyInvocation();
        return viewModel.getVitaminB6();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> vitaminB6Property()
    {
        validateProxyPropertyInvocation();
        return viewModel.vitaminB6Property();
    }

    @Override
    public BigDecimal getVitaminB7()
    {
        validateProxyPropertyInvocation();
        return viewModel.getVitaminB7();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> vitaminB7Property()
    {
        validateProxyPropertyInvocation();
        return viewModel.vitaminB7Property();
    }

    @Override
    public BigDecimal getVitaminB8()
    {
        validateProxyPropertyInvocation();
        return viewModel.getVitaminB8();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> vitaminB8Property()
    {
        validateProxyPropertyInvocation();
        return viewModel.vitaminB8Property();
    }

    @Override
    public BigDecimal getVitaminB9()
    {
        validateProxyPropertyInvocation();
        return viewModel.getVitaminB9();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> vitaminB9Property()
    {
        validateProxyPropertyInvocation();
        return viewModel.vitaminB9Property();
    }

    @Override
    public BigDecimal getVitaminB12()
    {
        validateProxyPropertyInvocation();
        return viewModel.getVitaminB12();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> vitaminB12Property()
    {
        validateProxyPropertyInvocation();
        return viewModel.vitaminB12Property();
    }
}
