/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


package com.iceflyer3.pfd.ui.model.proxy.mealplanning;

import com.iceflyer3.pfd.data.factory.MealPlanningTaskFactory;
import com.iceflyer3.pfd.data.task.mealplanning.meal.DeleteMealPlanMealTask;
import com.iceflyer3.pfd.data.task.mealplanning.meal.GetMealIngredientsForMealTask;
import com.iceflyer3.pfd.data.task.mealplanning.meal.SaveMealIngredientsForMealPlanTask;
import com.iceflyer3.pfd.ui.model.api.mealplanning.MealPlanMealModel;
import com.iceflyer3.pfd.ui.model.api.proxy.IngredientModelProxy;
import com.iceflyer3.pfd.ui.model.api.proxy.ProxyOperationResultCallback;
import com.iceflyer3.pfd.ui.model.api.viewmodel.IngredientLazyLoadingViewModel;
import com.iceflyer3.pfd.ui.model.proxy.ProxyOperation;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.core.task.TaskExecutor;

/**
 * Base class for meal planning and carb cycling meal proxies
 *
 * Because carb cycling meals are actually just an extension of a meal planning meal this class
 * contains shared functionality for loading and saving ingredients for the meal as well as for
 * deleting the meal.
 *
 * @param <T> The concrete type of the view model that this proxy sits on top of
 */
public abstract class AbstractMealPlanningMealModelProxy<T extends MealPlanMealModel & IngredientLazyLoadingViewModel>
        extends AbstractMealPlanningNutritionFactReaderModelProxy<T>
        implements IngredientModelProxy
{
    private static final Logger LOG = LoggerFactory.getLogger(AbstractMealPlanningMealModelProxy.class);

    public AbstractMealPlanningMealModelProxy(TaskExecutor taskExecutor, MealPlanningTaskFactory mealPlanningTaskFactory)
    {
        super(taskExecutor, mealPlanningTaskFactory);
    }

    @Override
    public void saveIngredients(ProxyOperationResultCallback saveResultCallback) {
        validateProxyOperationInvocation(ProxyOperation.SAVE);

        SaveMealIngredientsForMealPlanTask saveIngredientsTask = mealPlanningTaskFactory.saveMealIngredientsAsync(viewModel);
        saveIngredientsTask.setOnSucceeded(event -> {
            viewModel.setIngredients(saveIngredientsTask.getValue());
            saveResultCallback.receiveResult(true, "The ingredients for the meal have been saved!");
        });
        saveIngredientsTask.setOnFailed(event -> {
            LOG.error("An error has occurred while saving the ingredients for the meal {}", viewModel.getId(), saveIngredientsTask.getException());
            saveResultCallback.receiveResult(false, "Failed to save the ingredients for the meal. An error has occurred.");
        });
        taskExecutor.execute(saveIngredientsTask);
    }

    @Override
    public void loadIngredients(ProxyOperationResultCallback loadCallback) {
        validateProxyOperationInvocation(ProxyOperation.LOAD);

        GetMealIngredientsForMealTask getIngredientsTask = mealPlanningTaskFactory.getMealIngredientsAsync(viewModel.getId());
        getIngredientsTask.setOnSucceeded(event -> {
            viewModel.setIngredients(getIngredientsTask.getValue());
            viewModel.refreshNutritionDetails();
            loadCallback.receiveResult(true, "Meal ingredients loaded successfully!");
        });
        getIngredientsTask.setOnFailed(event -> {
            LOG.error("An error has occurred while loading the ingredients for the meal {}", viewModel.getId(), getIngredientsTask.getException());
            loadCallback.receiveResult(false, "Failed to load the ingredients for the meal. An error has occurred.");
        });
        taskExecutor.execute(getIngredientsTask);
    }

    @Override
    public void delete(ProxyOperationResultCallback deleteResultCallback) {
        validateProxyOperationInvocation(ProxyOperation.DELETE);

        DeleteMealPlanMealTask deleteMealPlanMealTask = mealPlanningTaskFactory.deleteMealAsync(viewModel.getId());
        deleteMealPlanMealTask.setOnSucceeded(event -> {
            isDeleted = true;
            viewModel = null;
            deleteResultCallback.receiveResult(true, "The meal has been deleted!");
        });
        deleteMealPlanMealTask.setOnFailed(event -> {
            LOG.error("An error has occurred while deleting the meal {}", viewModel.getId(), deleteMealPlanMealTask.getException());
            deleteResultCallback.receiveResult(false, "Failed to delete the meal. An error has occurred.");
        });
        taskExecutor.execute(deleteMealPlanMealTask);
    }
}
