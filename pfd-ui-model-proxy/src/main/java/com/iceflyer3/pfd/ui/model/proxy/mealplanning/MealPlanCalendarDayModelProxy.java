/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.model.proxy.mealplanning;

import com.iceflyer3.pfd.enums.WeeklyInterval;
import com.iceflyer3.pfd.data.factory.MealPlanningTaskFactory;
import com.iceflyer3.pfd.data.task.mealplanning.calendar.DeleteMealPlanCalendarDayTask;
import com.iceflyer3.pfd.ui.model.api.proxy.ProxyOperationResultCallback;
import com.iceflyer3.pfd.ui.model.api.mealplanning.MealPlanCalendarDayModel;
import com.iceflyer3.pfd.ui.model.proxy.ProxyOperation;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanCalendarDayViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanDayViewModel;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.core.task.TaskExecutor;

import java.time.LocalDate;
import java.util.UUID;

public class MealPlanCalendarDayModelProxy
        extends AbstractMealPlanningModelProxy<MealPlanCalendarDayViewModel>
        implements MealPlanCalendarDayModel
{
    private final static Logger LOG = LoggerFactory.getLogger(MealPlanCalendarDayModelProxy.class);

    public MealPlanCalendarDayModelProxy(TaskExecutor taskExecutor, MealPlanningTaskFactory mealPlanningTaskFactory, MealPlanCalendarDayViewModel viewModel)
    {
        super(taskExecutor, mealPlanningTaskFactory);
        this.viewModel = viewModel;
    }

    @Override
    public UUID getId()
    {
        validateProxyPropertyInvocation();
        return viewModel.getCalendarDayId();
    }

    @Override
    public MealPlanDayViewModel getPlannedDay()
    {
        validateProxyPropertyInvocation();
        return viewModel.getPlannedDay();
    }

    @Override
    public LocalDate getCalendarDate()
    {
        validateProxyPropertyInvocation();
        return viewModel.getCalendarDate();
    }

    @Override
    public WeeklyInterval getWeeklyInterval()
    {
        validateProxyPropertyInvocation();
        return viewModel.getWeeklyInterval();
    }

    @Override
    public int getIntervalMonthsDuration()
    {
        validateProxyPropertyInvocation();
        return viewModel.getIntervalMonthsDuration();
    }

    /**
     * @deprecated
     *
     * MealPlanCalendarDay instances are immutable and as such do not support the save operation.
     * Invoking this function will cause an UnsupportedOperationException to be thrown.
     * @param saveResultCallback Unused
     */
    @Override
    @Deprecated
    public void save(ProxyOperationResultCallback saveResultCallback)
    {
        throw new UnsupportedOperationException("Meal Plan Calendar Days may not be saved");
    }

    @Override
    public void delete(ProxyOperationResultCallback deleteResultCallback)
    {
        validateProxyOperationInvocation(ProxyOperation.DELETE);
        DeleteMealPlanCalendarDayTask deleteCalendarDayTask = mealPlanningTaskFactory.deleteCalendarDay(getId());
        deleteCalendarDayTask.setOnSucceeded(event -> {
            this.isDeleted = true;
            this.viewModel = null;
            deleteResultCallback.receiveResult(true, "The meal plan day to calendar day association has been deleted!");
        });
        deleteCalendarDayTask.setOnFailed(event -> {
            LOG.error("An error has occurred while deleting the meal plan calendar day with ID {}", getId());
            deleteResultCallback.receiveResult(false, "Failed to delete the meal plan day to calendar day association. An error has occurred.");
        });
        taskExecutor.execute(deleteCalendarDayTask);
    }

    @Override
    public String toString()
    {
        return viewModel.toString();
    }
}
