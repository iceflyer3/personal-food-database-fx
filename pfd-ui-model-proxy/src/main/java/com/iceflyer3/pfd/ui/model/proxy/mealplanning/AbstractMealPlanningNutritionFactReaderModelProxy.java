package com.iceflyer3.pfd.ui.model.proxy.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.data.factory.MealPlanningTaskFactory;
import com.iceflyer3.pfd.ui.model.api.NutritionFactsReader;
import com.iceflyer3.pfd.ui.model.proxy.base.AbstractNutritionFactReaderProxy;
import org.springframework.core.task.TaskExecutor;

/**
 * Base class for meal planning proxy classes that wrap a view model which has nutritional
 * information to report
 * @param <ModelType> The type of model that this proxy is for
 */
public abstract class AbstractMealPlanningNutritionFactReaderModelProxy<ModelType extends NutritionFactsReader>
        extends AbstractNutritionFactReaderProxy<ModelType>
{
    protected final MealPlanningTaskFactory mealPlanningTaskFactory;

    public AbstractMealPlanningNutritionFactReaderModelProxy(TaskExecutor taskExecutor, MealPlanningTaskFactory mealPlanningTaskFactory) {
        super(taskExecutor);
        this.mealPlanningTaskFactory = mealPlanningTaskFactory;
    }
}
