package com.iceflyer3.pfd.ui.model.proxy.mealplanning.carbcycling;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.enums.CarbCyclingDayType;
import com.iceflyer3.pfd.enums.FitnessGoal;
import com.iceflyer3.pfd.data.factory.MealPlanningTaskFactory;
import com.iceflyer3.pfd.data.task.mealplanning.day.DeleteMealPlanDayTask;
import com.iceflyer3.pfd.data.task.mealplanning.carbcycling.day.SaveCarbCyclingDayTask;
import com.iceflyer3.pfd.ui.model.api.proxy.ProxyOperationResultCallback;
import com.iceflyer3.pfd.ui.model.api.mealplanning.carbcycling.CarbCyclingDayModel;
import com.iceflyer3.pfd.ui.model.proxy.base.AbstractModelProxy;
import com.iceflyer3.pfd.ui.model.proxy.ProxyOperation;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.AbstractMealPlanningNutritionFactReaderModelProxy;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingDayViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingMealViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingPlanViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;
import com.iceflyer3.pfd.validation.ValidationResult;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.core.task.TaskExecutor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

public class CarbCyclingDayModelProxy extends AbstractMealPlanningNutritionFactReaderModelProxy<CarbCyclingDayViewModel> implements CarbCyclingDayModel
{
    private final static Logger LOG = LoggerFactory.getLogger(CarbCyclingDayModelProxy.class);

    /**
     * Constructor for a new meal plan day that has not yet been persisted and is also utilizing the active carb
     * cycling plan
     * @param taskExecutor Task executor used for loading, saving, and deleting
     * @param mealPlanningTaskFactory Manager that provides access to the various tasks used for loading, saving, and deleting
     */
    public CarbCyclingDayModelProxy(TaskExecutor taskExecutor,
                                 MealPlanningTaskFactory mealPlanningTaskFactory,
                                 MealPlanViewModel forMealPlan,
                                 CarbCyclingPlanViewModel forCarbCyclingPlan)
    {
        super(taskExecutor, mealPlanningTaskFactory);
        this.viewModel = new CarbCyclingDayViewModel(null, forMealPlan.getId(), forCarbCyclingPlan.getId(), NutritionalSummary.empty());
    }

    public CarbCyclingDayModelProxy(TaskExecutor taskExecutor,
                                 MealPlanningTaskFactory mealPlanningTaskFactory,
                                 CarbCyclingDayViewModel viewModel)
    {
        super(taskExecutor, mealPlanningTaskFactory);
        this.viewModel = viewModel;
    }

    @Override
    public ValidationResult validate()
    {
        validateProxyPropertyInvocation();
        return viewModel.validate();
    }

    @Override
    public ValidationResult validateProperty(int propertyHashcode)
    {
        validateProxyPropertyInvocation();
        return viewModel.validateProperty(propertyHashcode);
    }

    /**
     * Save the day. Use this version when there is no active carb cycling plan.
     * @param saveResultCallback Callback to invoke when the save operation has either errored or completed
     */
    @Override
    public void save(ProxyOperationResultCallback saveResultCallback)
    {
        validateProxyOperationInvocation(ProxyOperation.SAVE);

        SaveCarbCyclingDayTask saveDayTask = mealPlanningTaskFactory.saveCarbCyclingDayForMealPlanAsync(viewModel);
        saveDayTask.setOnSucceeded(event -> {
            viewModel = saveDayTask.getValue();
            saveResultCallback.receiveResult(true, "The carb cycling day has been saved!");
        });
        saveDayTask.setOnFailed(event -> {
            LOG.error("An error has occurred while saving a carb cycling day for carb cycling plan {}", viewModel.getOwningCarbCyclingPlanId(), saveDayTask.getException());
            saveResultCallback.receiveResult(false, "Failed to save the carb cycling day. An error has occurred.");
        });
        taskExecutor.execute(saveDayTask);
    }

    // Same task is used for deleting both carb cycling and meal plan days
    @SuppressWarnings("DuplicatedCode")
    @Override
    public void delete(ProxyOperationResultCallback deleteResultCallback)
    {
        validateProxyOperationInvocation(ProxyOperation.DELETE);

        DeleteMealPlanDayTask deleteDayTask = mealPlanningTaskFactory.deleteDayAsync(viewModel.getId());
        deleteDayTask.setOnSucceeded(event -> {
            this.viewModel = null;
            this.isDeleted = true;
            deleteResultCallback.receiveResult(true, "The carb cycling day has been deleted!");
        });
        deleteDayTask.setOnFailed(event -> {
            LOG.error("An error has occurred while deleting day {} for the meal plan {}", viewModel.getId(), viewModel.getOwningMealPlanId(), deleteDayTask.getException());
            deleteResultCallback.receiveResult(false, "Failed to delete the carb cycling day. An error has occurred.");
        });
        taskExecutor.execute(deleteDayTask);
    }

    @Override
    public UUID getId()
    {
        validateProxyPropertyInvocation();
        return viewModel.getId();
    }

    @Override
    public FitnessGoal getFitnessGoal()
    {
        validateProxyPropertyInvocation();
        return viewModel.getFitnessGoal();
    }

    @Override
    public ObjectProperty<FitnessGoal> fitnessGoalProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.fitnessGoalProperty();
    }

    @Override
    public void setFitnessGoal(FitnessGoal fitnessGoal)
    {
        validateProxyPropertyInvocation();
        viewModel.setFitnessGoal(fitnessGoal);
    }

    @Override
    public String getDayLabel()
    {
        validateProxyPropertyInvocation();
        return viewModel.getDayLabel();
    }

    @Override
    public StringProperty dayLabelProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.dayLabelProperty();
    }

    @Override
    public void setDayLabel(String dayLabel)
    {
        validateProxyPropertyInvocation();
        viewModel.setDayLabel(dayLabel);
    }

    @Override
    public LocalDateTime getLastModifiedDate()
    {
        validateProxyPropertyInvocation();
        return viewModel.getLastModifiedDate();
    }

    @Override
    public ObjectProperty<LocalDateTime> lastModifiedDateProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.lastModifiedDateProperty();
    }

    @Override
    public void setLastModifiedDate(LocalDateTime lastModifiedDate)
    {
        validateProxyPropertyInvocation();
        viewModel.setLastModifiedDate(lastModifiedDate);
    }

    @Override
    public CarbCyclingDayType getDayType()
    {
        validateProxyPropertyInvocation();
        return viewModel.getDayType();
    }

    @Override
    public ObjectProperty<CarbCyclingDayType> dayTypeProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.dayTypeProperty();
    }

    @Override
    public void setDayType(CarbCyclingDayType dayType)
    {
        validateProxyPropertyInvocation();
        viewModel.setDayType(dayType);
    }

    @Override
    public boolean isTrainingDay()
    {
        validateProxyPropertyInvocation();
        return viewModel.isTrainingDay();
    }

    @Override
    public BooleanProperty isTrainingDayProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.isTrainingDayProperty();
    }

    @Override
    public void toggleTrainingDay()
    {
        validateProxyPropertyInvocation();
        viewModel.toggleTrainingDay();
    }

    @Override
    public BigDecimal getRequiredCalories()
    {
        validateProxyPropertyInvocation();
        return viewModel.getRequiredCalories();
    }

    @Override
    public ObjectProperty<BigDecimal> requiredCaloriesProperty() {
        validateProxyPropertyInvocation();
        return viewModel.requiredCaloriesProperty();
    }

    @Override
    public BigDecimal getRequiredProtein()
    {
        validateProxyPropertyInvocation();
        return viewModel.getRequiredProtein();
    }

    @Override
    public ObjectProperty<BigDecimal> requiredProteinProperty() {
        validateProxyPropertyInvocation();
        return viewModel.requiredProteinProperty();
    }

    @Override
    public BigDecimal getRequiredCarbs()
    {
        validateProxyPropertyInvocation();
        return viewModel.getRequiredCarbs();
    }

    @Override
    public ObjectProperty<BigDecimal> requiredCarbsProperty() {
        validateProxyPropertyInvocation();
        return viewModel.requiredCarbsProperty();
    }

    @Override
    public BigDecimal getRequiredFats()
    {
        validateProxyPropertyInvocation();
        return viewModel.getRequiredFats();
    }

    @Override
    public ObjectProperty<BigDecimal> requiredFatsProperty() {
        validateProxyPropertyInvocation();
        return viewModel.requiredFatsProperty();
    }

    @Override
    public String toString()
    {
        validateProxyPropertyInvocation();
        return "CarbCyclingDayModelProxy{" + "viewModel=" + viewModel + '}';
    }

    @Override
    @Deprecated
    public void refreshNutritionDetails()
    {
        validateProxyPropertyInvocation();
        viewModel.refreshNutritionDetails();
    }

    public void refreshNutritionDetails(Collection<CarbCyclingMealModelProxy> meals)
    {
        validateProxyPropertyInvocation();
        LOG.debug("CarbCyclingDayModelProxy is refreshing nutritional details...");
        List<CarbCyclingMealViewModel> viewModels = meals.stream().map(AbstractModelProxy::getViewModel).toList();
        viewModel.refreshNutritionDetails(viewModels);
    }
}
