package com.iceflyer3.pfd.ui.model.proxy.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.enums.FitnessGoal;
import com.iceflyer3.pfd.data.factory.MealPlanningTaskFactory;
import com.iceflyer3.pfd.data.task.mealplanning.day.DeleteMealPlanDayTask;
import com.iceflyer3.pfd.data.task.mealplanning.day.SaveDayForMealPlanTask;
import com.iceflyer3.pfd.ui.model.api.proxy.ProxyOperationResultCallback;
import com.iceflyer3.pfd.ui.model.api.mealplanning.MealPlanDayModel;
import com.iceflyer3.pfd.ui.model.proxy.base.AbstractModelProxy;
import com.iceflyer3.pfd.ui.model.proxy.ProxyOperation;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanDayViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;
import com.iceflyer3.pfd.validation.ValidationResult;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.core.task.TaskExecutor;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public class MealPlanDayModelProxy
        extends AbstractMealPlanningNutritionFactReaderModelProxy<MealPlanDayViewModel>
        implements MealPlanDayModel
{
    private final static Logger LOG = LoggerFactory.getLogger(MealPlanDayModelProxy.class);

    /**
     * Constructor for a new meal plan day that has not yet been persisted
     * @param taskExecutor Task executor used for loading, saving, and deleting
     * @param mealPlanningTaskFactory Manager that provides access to the various tasks used for loading, saving, and deleting
     * @param forMealPlan The meal plan to which this day will belong
     */
    public MealPlanDayModelProxy(TaskExecutor taskExecutor,
                                 MealPlanningTaskFactory mealPlanningTaskFactory,
                                 MealPlanViewModel forMealPlan)
    {
        super(taskExecutor, mealPlanningTaskFactory);
        this.viewModel = new MealPlanDayViewModel(null, forMealPlan.getId(), NutritionalSummary.empty());
    }

    /**
     * Constructor for a meal plan that has already been persisted.
     * @param taskExecutor Task executor used for loading, saving, and deleting
     * @param mealPlanningTaskFactory Manager that provides access to the various tasks used for loading, saving, and deleting
     */
    public MealPlanDayModelProxy(TaskExecutor taskExecutor,
                                 MealPlanningTaskFactory mealPlanningTaskFactory,
                                 MealPlanDayViewModel viewModel)
    {
        super(taskExecutor, mealPlanningTaskFactory);
        this.viewModel = viewModel;
    }

    @Override
    public ValidationResult validate()
    {
        validateProxyPropertyInvocation();
        return viewModel.validate();
    }

    @Override
    public ValidationResult validateProperty(int propertyHashcode)
    {
        validateProxyPropertyInvocation();
        return viewModel.validateProperty(propertyHashcode);
    }

    /**
     * Save the day. Use this version when there is no active carb cycling plan.
     * @param saveResultCallback Callback to invoke when the save operation has either errored or completed
     */
    @Override
    public void save(ProxyOperationResultCallback saveResultCallback)
    {
        validateProxyOperationInvocation(ProxyOperation.SAVE);
        SaveDayForMealPlanTask saveDayTask = mealPlanningTaskFactory.saveDayForMealPlanAsync(viewModel);
        saveDayTask.setOnSucceeded(event -> {
            viewModel = saveDayTask.getValue();
            saveResultCallback.receiveResult(true, "The day has been saved!");
        });
        saveDayTask.setOnFailed(event -> {
            LOG.error("An error has occurred while saving a day for the meal plan {}", viewModel.getOwningMealPlanId(), saveDayTask.getException());
            saveResultCallback.receiveResult(false, "Failed to save the day. An error has occurred.");
        });
        taskExecutor.execute(saveDayTask);
    }

    @Override
    public void delete(ProxyOperationResultCallback deleteResultCallback)
    {
        validateProxyOperationInvocation(ProxyOperation.DELETE);

        DeleteMealPlanDayTask deleteDayTask = mealPlanningTaskFactory.deleteDayAsync(viewModel.getId());
        deleteDayTask.setOnSucceeded(event -> {
            viewModel = null;
            isDeleted = true;
            deleteResultCallback.receiveResult(true, "The day has been deleted!");
        });
        deleteDayTask.setOnFailed(event -> {
            LOG.error("An error has occurred while deleting day {} for the meal plan {}", viewModel.getId(), viewModel.getOwningMealPlanId(), deleteDayTask.getException());
            deleteResultCallback.receiveResult(false, "Failed to delete the day. An error has occurred.");
        });
        taskExecutor.execute(deleteDayTask);
    }

    // Meal Plan Day Model interface implementation
    @Override
    public UUID getId()
    {
        validateProxyPropertyInvocation();
        return viewModel.getId();
    }

    @Override
    public FitnessGoal getFitnessGoal()
    {
        validateProxyPropertyInvocation();
        return viewModel.getFitnessGoal();
    }

    @Override
    public ObjectProperty<FitnessGoal> fitnessGoalProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.fitnessGoalProperty();
    }

    @Override
    public void setFitnessGoal(FitnessGoal fitnessGoal)
    {
        validateProxyPropertyInvocation();
        viewModel.setFitnessGoal(fitnessGoal);
    }

    @Override
    public String getDayLabel()
    {
        validateProxyPropertyInvocation();
        return viewModel.getDayLabel();
    }

    @Override
    public StringProperty dayLabelProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.dayLabelProperty();
    }

    @Override
    public void setDayLabel(String dayLabel)
    {
        validateProxyPropertyInvocation();
        viewModel.setDayLabel(dayLabel);
    }

    @Override
    public LocalDateTime getLastModifiedDate()
    {
        validateProxyPropertyInvocation();
        return viewModel.getLastModifiedDate();
    }

    @Override
    public ObjectProperty<LocalDateTime> lastModifiedDateProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.lastModifiedDateProperty();
    }

    @Override
    public void setLastModifiedDate(LocalDateTime lastModifiedDate)
    {
        validateProxyPropertyInvocation();
        viewModel.setLastModifiedDate(lastModifiedDate);
    }

    /**
     * The nutritional information for a day is defined by the meals which belong to it.
     * As such, this method is not supported. Use the variant that accepts meals as a parameter.
     *
     * If invoked this will throw a UnsupportedOperationException.
     */
    @Override
    @Deprecated
    public void refreshNutritionDetails()
    {
        validateProxyPropertyInvocation();
        viewModel.refreshNutritionDetails();
    }

    public void refreshNutritionDetails(List<MealPlanMealModelProxy> meals)
    {
        validateProxyPropertyInvocation();
        viewModel.refreshNutritionDetails(meals.stream().map(AbstractModelProxy::getViewModel).toList());
    }
}
