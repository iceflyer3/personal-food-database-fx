package com.iceflyer3.pfd.ui.model.proxy.recipe;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.factory.RecipeTaskFactory;
import com.iceflyer3.pfd.data.task.recipe.DeleteRecipeTask;
import com.iceflyer3.pfd.data.task.recipe.GetIngredientsForRecipeTask;
import com.iceflyer3.pfd.data.task.recipe.GetStepsForRecipeTask;
import com.iceflyer3.pfd.data.task.recipe.IsUniqueRecipeTask;
import com.iceflyer3.pfd.ui.model.api.proxy.ProxyDataResultCallback;
import com.iceflyer3.pfd.ui.model.api.viewmodel.IngredientLazyLoadingViewModel;
import com.iceflyer3.pfd.ui.model.api.proxy.ProxyOperationResultCallback;
import com.iceflyer3.pfd.ui.model.api.viewmodel.StepLazyLoadingViewModel;
import com.iceflyer3.pfd.ui.model.api.recipe.RecipeModel;
import com.iceflyer3.pfd.ui.model.proxy.base.AbstractNutritionFactReaderProxy;
import javafx.collections.FXCollections;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.core.task.TaskExecutor;

public abstract class AbstractRecipeModelProxy<ModelType extends RecipeModel> extends AbstractNutritionFactReaderProxy<ModelType>
{
    private final static Logger LOG = LoggerFactory.getLogger(AbstractRecipeModelProxy.class);

    protected RecipeTaskFactory recipeTaskFactory;

    protected AbstractRecipeModelProxy(TaskExecutor taskExecutor, RecipeTaskFactory recipeTaskFactory) {
        super(taskExecutor);
        this.recipeTaskFactory = recipeTaskFactory;
    }

    /**
     * Load and populate a recipe instance that implements the IngredientLazyLoadingViewModel interface
     * with the list of ingredients that belong to this recipe
     *
     * The loading of ingredients is opt-in because we may not care about the individual ingredients
     * when we load a recipe.
     */
    public void loadIngredients(ProxyOperationResultCallback loadIngredientsCallback)
    {
        if (viewModel instanceof IngredientLazyLoadingViewModel)
        {
            GetIngredientsForRecipeTask getIngredientsTask = recipeTaskFactory.getIngredientsForRecipeAsync(getId());
            getIngredientsTask.setOnSucceeded(event -> {
                ((IngredientLazyLoadingViewModel) viewModel).setIngredients(FXCollections.observableArrayList(getIngredientsTask.getValue()));
                viewModel.refreshNutritionDetails();
                loadIngredientsCallback.receiveResult(true, "The ingredients for the recipe have been loaded!");
            });
            getIngredientsTask.setOnFailed(event -> {
                LOG.error("An error has occurred while loading ingredients for the recipe", getIngredientsTask.getException());
                loadIngredientsCallback.receiveResult(false, "Failed to load the ingredients for the recipe. An error has occurred.");
            });
            taskExecutor.execute(getIngredientsTask);
        }
        else
        {
            throw new UnsupportedOperationException(String.format("The ViewModel of type %s must implement the IngredientLazyLoadingViewModel interface to be able to lazy load ingredients", viewModel.getClass()));
        }
    }

    /**
     * Load and populate the recipe instance that implements the StepLazyLoadingViewModel
     * interface with the list of steps to make this recipe.
     *
     * The loading of steps is opt-in because we may not care about the individual steps
     * when we load a recipe.
     */
    public void loadSteps(ProxyOperationResultCallback loadStepsCallback)
    {
        if (viewModel instanceof StepLazyLoadingViewModel)
        {
            GetStepsForRecipeTask getStepsTask = recipeTaskFactory.getStepsForRecipeAsync(getId());
            getStepsTask.setOnSucceeded(event -> {
                ((StepLazyLoadingViewModel) viewModel).setSteps(FXCollections.observableArrayList(getStepsTask.getValue()));
                loadStepsCallback.receiveResult(true, "The steps for the recipe has been loaded!");
            });
            getStepsTask.setOnFailed(event -> {
                LOG.error("An error has occurred while loading steps for the recipe", getStepsTask.getException());
                loadStepsCallback.receiveResult(false, "Failed to load the steps for the recipe. An error has occurred.");
            });
            taskExecutor.execute(getStepsTask);
        }
        else
        {
            throw new UnsupportedOperationException(String.format("The ViewModel of type %s must implement the StepLazyLoadingViewModel interface to be able to lazy load steps", viewModel.getClass()));
        }
    }

    /*
     * This is basically the same as the delete process in AbstractFoodModelProxy which is why
     * IntelliJ detects it as "duplicated code".
     *
     * But it isn't actually. The tasks that are being used are different and specific to the
     * "section" to which the proxy is the base class for. I dunno why it can't tell this, but
     * apparently it can't.
     */
    @SuppressWarnings("DuplicatedCode")
    @Override
    public void delete(ProxyOperationResultCallback deleteResultCallback)
    {
        DeleteRecipeTask deleteRecipeTask = recipeTaskFactory.deleteRecipeAsync(getId());
        deleteRecipeTask.setOnSucceeded(event -> {
            this.isDeleted = true;
            this.viewModel = null;
            deleteResultCallback.receiveResult(true, "The recipe has been deleted!");
        });
        deleteRecipeTask.setOnFailed(event -> {
            LOG.error("An error has occurred while deleting the recipe {}", getId(), deleteRecipeTask.getException());
            deleteResultCallback.receiveResult(false, "Failed to delete the recipe. An error has occurred.");
        });
        taskExecutor.execute(deleteRecipeTask);
    }

    public void isUnique(ProxyDataResultCallback<Boolean> isUniqueCallback)
    {
        IsUniqueRecipeTask isUniqueRecipeTask = recipeTaskFactory.isUniqueAsync(viewModel);
        isUniqueRecipeTask.setOnSucceeded(event -> {
            isUniqueCallback.receiveResult(true, isUniqueRecipeTask.getValue());
        });
        isUniqueRecipeTask.setOnFailed(event -> {
            LOG.error("An error has occurred while checking if the recipe {} is unique", getId(), isUniqueRecipeTask.getException());
            isUniqueCallback.receiveResult(false, false);
        });
        taskExecutor.execute(isUniqueRecipeTask);
    }
}
