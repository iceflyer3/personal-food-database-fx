package com.iceflyer3.pfd.ui.model.proxy.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.factory.MealPlanningTaskFactory;
import com.iceflyer3.pfd.data.task.mealplanning.meal.SaveMealForMealPlanTask;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.api.food.ReadOnlyFoodModel;
import com.iceflyer3.pfd.ui.model.api.proxy.ProxyOperationResultCallback;
import com.iceflyer3.pfd.ui.model.api.mealplanning.MealPlanMealModel;
import com.iceflyer3.pfd.ui.model.proxy.ProxyOperation;
import com.iceflyer3.pfd.ui.model.proxy.food.ReadOnlyFoodModelProxy;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanDayViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanMealViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;
import com.iceflyer3.pfd.validation.ValidationResult;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.core.task.TaskExecutor;

import java.time.LocalDateTime;
import java.util.UUID;

public class MealPlanMealModelProxy
        extends AbstractMealPlanningMealModelProxy<MealPlanMealViewModel>
        implements MealPlanMealModel
{
    private final static Logger LOG = LoggerFactory.getLogger(MealPlanMealModelProxy.class);

    private final MealPlanViewModel forMealPlan;
    private final MealPlanDayViewModel owningDay;

    public MealPlanMealModelProxy(TaskExecutor taskExecutor,
                                  MealPlanningTaskFactory mealPlanningTaskFactory,
                                  MealPlanViewModel forMealPlan,
                                  MealPlanDayViewModel forDay)
    {
        super(taskExecutor, mealPlanningTaskFactory);

        this.forMealPlan = forMealPlan;
        this.owningDay = forDay;
        this.viewModel = new MealPlanMealViewModel(null, NutritionalSummary.empty());
    }

    public MealPlanMealModelProxy(TaskExecutor taskExecutor,
                                  MealPlanningTaskFactory mealPlanningTaskFactory,
                                  MealPlanViewModel forMealPlan,
                                  MealPlanDayViewModel forDay,
                                  MealPlanMealViewModel viewModel)
    {
        super(taskExecutor, mealPlanningTaskFactory);

        this.forMealPlan = forMealPlan;
        this.owningDay = forDay;

        this.viewModel = viewModel;
    }

    @Override
    public ValidationResult validate() {
        validateProxyPropertyInvocation();
        return viewModel.validate();
    }

    @Override
    public ValidationResult validateProperty(int propertyHashcode)
    {
        validateProxyPropertyInvocation();
        return viewModel.validateProperty(propertyHashcode);
    }

    /**
     * Save function that should be used when this meal IS NOT for a carb cycling plan.
     * @param saveResultCallback The callback used to report success or failure
     */
    @Override
    public void save(ProxyOperationResultCallback saveResultCallback)
    {
        validateProxyOperationInvocation(ProxyOperation.SAVE);

        SaveMealForMealPlanTask saveMealTask = mealPlanningTaskFactory.saveMealAsync(owningDay, viewModel);
        saveMealTask.setOnSucceeded(event -> {
            viewModel = saveMealTask.getValue();
            saveResultCallback.receiveResult(true, "The meal has been saved!");
        });
        saveMealTask.setOnFailed(event -> {
            LOG.error("An error has occurred while saving a new meal for day {}", owningDay.getId(), saveMealTask.getException());
            saveResultCallback.receiveResult(false, "Failed to save the meal. An error has occurred.");
        });
        taskExecutor.execute(saveMealTask);
    }

    @Override
    public UUID getId() {
        validateProxyPropertyInvocation();
        return viewModel.getId();
    }


    // Meal Plan Meal interface implementation
    @Override
    public String getName() {
        validateProxyPropertyInvocation();
        return viewModel.getName();
    }

    @Override
    public void setName(String newName)
    {
        validateProxyPropertyInvocation();
        viewModel.setName(newName);
    }

    @Override
    public StringProperty nameProperty() {
        validateProxyPropertyInvocation();
        return viewModel.nameProperty();
    }

    @Override
    public LocalDateTime getLastModifiedDate() {
        validateProxyPropertyInvocation();
        return viewModel.getLastModifiedDate();
    }

    @Override
    public void setLastModifiedDate(LocalDateTime lastModDate) {
        validateProxyPropertyInvocation();
        viewModel.setLastModifiedDate(lastModDate);
    }

    @Override
    public ObjectProperty<LocalDateTime> lastModifiedDateProperty() {
        validateProxyPropertyInvocation();
        return viewModel.lastModifiedDateProperty();
    }

    // Ingredient Detail Reporter implementation
    @Override
    public void refreshNutritionDetails()
    {
        validateProxyPropertyInvocation();
        viewModel.refreshNutritionDetails();
    }

    @Override
    public ObservableList<IngredientModel> getIngredients() {
        validateProxyPropertyInvocation();
        return viewModel.getIngredients();
    }

    /**
     * Add a new ingredient to the list of ingredients. The provided ingredient should be of the proxy
     * implementation type which is ReadOnlyFoodModelProxy.
     *
     * @param ingredient The ReadOnlyFoodModelProxy instance that represents the ingredient food
     */
    @Override
    public void addIngredient(ReadOnlyFoodModel ingredient) {
        validateProxyPropertyInvocation();

        if (ingredient instanceof ReadOnlyFoodModelProxy proxy)
        {
            viewModel.addIngredient(proxy.getViewModel());
        }
        else
        {
            throw new IllegalArgumentException("ingredient must be an instance of ReadOnlyFoodModelProxy");
        }
    }

    @Override
    public void removeIngredient(IngredientModel ingredient)
    {
        validateProxyPropertyInvocation();
        viewModel.removeIngredient(ingredient);
    }
}
