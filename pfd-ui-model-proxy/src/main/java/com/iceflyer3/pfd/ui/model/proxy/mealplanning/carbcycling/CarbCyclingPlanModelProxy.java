package com.iceflyer3.pfd.ui.model.proxy.mealplanning.carbcycling;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.data.factory.MealPlanningTaskFactory;
import com.iceflyer3.pfd.data.task.mealplanning.carbcycling.GetCarbCyclingMealConfigurationsByPlanIdTask;
import com.iceflyer3.pfd.data.task.mealplanning.carbcycling.SaveCarbCyclingPlanTask;
import com.iceflyer3.pfd.data.task.mealplanning.carbcycling.DeleteCarbCyclingPlanTask;
import com.iceflyer3.pfd.ui.model.api.mealplanning.carbcycling.CarbCyclingMealConfigurationModel;
import com.iceflyer3.pfd.ui.model.api.proxy.ProxyDataResultCallback;
import com.iceflyer3.pfd.ui.model.api.proxy.ProxyOperationResultCallback;
import com.iceflyer3.pfd.ui.model.api.mealplanning.carbcycling.CarbCyclingPlanModel;
import com.iceflyer3.pfd.ui.model.proxy.ProxyOperation;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.AbstractMealPlanningModelProxy;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.MealPlanModelProxy;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingPlanViewModel;
import com.iceflyer3.pfd.validation.ValidationResult;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.core.task.TaskExecutor;

import java.math.BigDecimal;
import java.util.Set;
import java.util.UUID;

public class CarbCyclingPlanModelProxy extends AbstractMealPlanningModelProxy<CarbCyclingPlanViewModel> implements CarbCyclingPlanModel {

    private static final Logger LOG = LoggerFactory.getLogger(CarbCyclingPlanModelProxy.class);

    /**
     * Constructor for use when we don't have a ViewModel already loaded from the persistence layer
     * @param taskExecutor Task Executor used for executing save / delete tasks
     * @param mealPlanningTaskFactory Task Factory used to acquire instances of the tasks to execute
     */
    public CarbCyclingPlanModelProxy(TaskExecutor taskExecutor,
                                     MealPlanningTaskFactory mealPlanningTaskFactory) {
        super(taskExecutor, mealPlanningTaskFactory);
        this.viewModel = new CarbCyclingPlanViewModel();
    }

    /**
     * Constructor for use when we have already loaded a ViewModel from the persistence layer.
     * @param taskExecutor Task Executor used for executing save / delete tasks
     * @param mealPlanningTaskFactory Task Factory used to acquire instances of the tasks to execute
     * @param viewModel The view model as loaded from the persistence layer
     */
    public CarbCyclingPlanModelProxy(TaskExecutor taskExecutor,
                                     MealPlanningTaskFactory mealPlanningTaskFactory,
                                     CarbCyclingPlanViewModel viewModel) {
        super(taskExecutor, mealPlanningTaskFactory);
        this.viewModel = viewModel;
    }

    @Override
    public int getLowCarbDays() {
        validateProxyPropertyInvocation();
        return viewModel.getLowCarbDays();
    }

    @Override
    public IntegerProperty lowCarbDaysProperty() {
        validateProxyPropertyInvocation();
        return viewModel.lowCarbDaysProperty();
    }

    @Override
    public void setLowCarbDays(int lowCarbDays) {
        validateProxyPropertyInvocation();
        viewModel.setLowCarbDays(lowCarbDays);
    }

    @Override
    public int getHighCarbDays() {
        validateProxyPropertyInvocation();
        return viewModel.getHighCarbDays();
    }

    @Override
    public IntegerProperty highCarbDaysProperty() {
        validateProxyPropertyInvocation();
        return viewModel.highCarbDaysProperty();
    }

    @Override
    public void setHighCarbDays(int highCarbDays) {
        validateProxyPropertyInvocation();
        viewModel.setHighCarbDays(highCarbDays);
    }

    @Override
    public BigDecimal getHighCarbDaySurplusPercentage()
    {
        validateProxyPropertyInvocation();
        return viewModel.getHighCarbDaySurplusPercentage();
    }

    @Override
    public ObjectProperty<BigDecimal> highCarbDaySurplusPercentageProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.highCarbDaySurplusPercentageProperty();
    }

    @Override
    public void setHighCarbDaySurplusPercentage(BigDecimal newPercentage)
    {
        validateProxyPropertyInvocation();
        viewModel.setHighCarbDaySurplusPercentage(newPercentage);
    }

    @Override
    public BigDecimal getLowCarbDayDeficitPercentage()
    {
        validateProxyPropertyInvocation();
        return viewModel.getLowCarbDayDeficitPercentage();
    }

    @Override
    public ObjectProperty<BigDecimal> lowCarbDayDeficitPercentageProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.lowCarbDayDeficitPercentageProperty();
    }

    @Override
    public void setLowCarbDayDeficitPercentage(BigDecimal newPercentage)
    {
        validateProxyPropertyInvocation();
        viewModel.setLowCarbDayDeficitPercentage(newPercentage);
    }

    @Override
    public int getMealsPerDay() {
        validateProxyPropertyInvocation();
        return viewModel.getMealsPerDay();
    }

    @Override
    public IntegerProperty mealsPerDayProperty() {
        validateProxyPropertyInvocation();
        return viewModel.mealsPerDayProperty();
    }

    @Override
    public void setMealsPerDay(int mealsPerDay) {
        validateProxyPropertyInvocation();
        viewModel.setMealsPerDay(mealsPerDay);
    }

    @Override
    public boolean isActive()
    {
        validateProxyPropertyInvocation();
        return viewModel.isActive();
    }

    @Override
    public BooleanProperty isActiveProperty() {
        validateProxyPropertyInvocation();
        return viewModel.isActiveProperty();
    }

    @Override
    public void toggleShouldTimeFats() {
        validateProxyPropertyInvocation();
        viewModel.toggleShouldTimeFats();
    }

    @Override
    public boolean isShouldTimeFats()
    {
        validateProxyPropertyInvocation();
        return viewModel.isShouldTimeFats();
    }

    @Override
    public BooleanProperty shouldTimeFatsProperty() {
        validateProxyPropertyInvocation();
        return viewModel.shouldTimeFatsProperty();
    }

    /**
     * Meal configuration for a carb cycling plan is opt-in.
     *
     * This function initializes the list of meal configurations for this plan based upon the
     * specified number of meals per day
     *
     * @throws IllegalStateException If invoked before the number of meals per day has been specified.
     */
    @Override
    public void initMealConfigurations()
    {
        validateProxyPropertyInvocation();
        viewModel.initMealConfigurations();
    }

    @Override
    public void clearMealConfigurations()
    {
        validateProxyPropertyInvocation();
        viewModel.clearMealConfigurations();
    }

    @Override
    public boolean hasConfiguredMeals()
    {
        validateProxyPropertyInvocation();
        return viewModel.hasConfiguredMeals();
    }

    @Override
    public Set<CarbCyclingMealConfigurationModel> getMealConfigurations()
    {
        validateProxyPropertyInvocation();
        return viewModel.getMealConfigurations();
    }

    @Override
    public UUID getId()
    {
        validateProxyPropertyInvocation();
        return viewModel.getId();
    }

    @Override
    public ValidationResult validate()
    {
        validateProxyPropertyInvocation();
        return viewModel.validate();
    }

    @Override
    public ValidationResult validateProperty(int propertyHashcode)
    {
        validateProxyPropertyInvocation();
        return viewModel.validateProperty(propertyHashcode);
    }

    @Override
    public ValidationResult validateMealConfigurations()
    {
        validateProxyPropertyInvocation();
        return viewModel.validateMealConfigurations();
    }

    public void loadMealConfigurations(ProxyDataResultCallback<Void> callback)
    {
        validateProxyOperationInvocation(ProxyOperation.LOAD);

        GetCarbCyclingMealConfigurationsByPlanIdTask getMealConfigsTask = mealPlanningTaskFactory.getCarbCyclingPlanMealConfigurations(viewModel.getId());
        getMealConfigsTask.setOnSucceeded(event -> {
            viewModel.initMealConfigurations(getMealConfigsTask.getValue());
            callback.receiveResult(true, null);
        });
        getMealConfigsTask.setOnFailed(event -> {
            LOG.error("An error has occurred while loading the meal configurations for the carb cycling plan", getMealConfigsTask.getException());
            callback.receiveResult(false, null);
        });
        taskExecutor.execute(getMealConfigsTask);
    }

    /**
     * This function is not supported on this proxy.
     *
     * If you wish to save a new carb cycling plan you must provide the user the
     * plan is for by using the save(User, ProxyDataResultCallback) overload.
     * @param saveResultCallback Not used
     */
    @Deprecated
    @Override
    public void save(ProxyOperationResultCallback saveResultCallback) {
        throw new UnsupportedOperationException("Before you may save a carb cycling plan you must provide the user the plan is for.");
    }

    /**
     * Call to save the Carb Cycling Plan. If the plan already exists the status
     * of it will instead be toggled.
     * @param saveResultCallback Callback to be notified of the result.
     */
    public void save(UserViewModel forUser, MealPlanModelProxy forMealPlan, ProxyDataResultCallback<CarbCyclingPlanModelProxy> saveResultCallback)
    {
        validateProxyOperationInvocation(ProxyOperation.SAVE);

        /*
         * The view model isn't concerned with which user this carb cycling plan is for.
         * The plans that are accessible to the UI layer are _always_ for the current user.
         *
         * Only the persistence layer cares about that. So there is no need to store the
         * user as a field on this object because we don't have use for it elsewhere.
         *
         * The same applies to meal plans. Only one meal plan is only ever "active" at a time
         * so the new carb cycling plan is always for the currently active meal plan.
         */
        SaveCarbCyclingPlanTask savePlanTask = mealPlanningTaskFactory.saveCarbCyclingPlanAsync(forUser.getUserId(), forMealPlan.getId(), viewModel);
        savePlanTask.setOnSucceeded(event -> {
            viewModel = savePlanTask.getValue();
            saveResultCallback.receiveResult(true, this);
        });
        savePlanTask.setOnFailed(event -> {
            LOG.error("An error has occurred while saving the carb cycling plan", savePlanTask.getException());
            saveResultCallback.receiveResult(false, null);
        });
        taskExecutor.execute(savePlanTask);
    }

    @Override
    public void delete(ProxyOperationResultCallback deleteResultCallback)
    {
        validateProxyOperationInvocation(ProxyOperation.DELETE);

        DeleteCarbCyclingPlanTask deletePlanTask = mealPlanningTaskFactory.deleteCarbCyclingPlanAsync(viewModel.getId());
        deletePlanTask.setOnSucceeded(event -> {
            this.isDeleted = true;
            this.viewModel = null;
            deleteResultCallback.receiveResult(true, "The carb cycling plan has been deleted!");
        });
        deletePlanTask.setOnFailed(event -> {
            LOG.error("An error has occurred while deleting the carb cycling plan", deletePlanTask.getException());
            deleteResultCallback.receiveResult(false, "An error has occurred while deleting the carb cycling plan");
        });
        taskExecutor.execute(deletePlanTask);
    }

    @Override
    public String toString()
    {
        // Used by the choicebox when creating a new calendar entry
        String highLowDayRatio = String.format("%s/%s", viewModel.getHighCarbDays(), viewModel.getLowCarbDays());
        String timingFats = viewModel.isShouldTimeFats() ? "Yes" : "No";
        String configuredMeals = viewModel.hasConfiguredMeals() ? "Yes" : "No";
        return String.format("Meals Per Day: %s | High / Low Day Ratio: %s | Timing Fats: %s | Configured Meals: %s", viewModel.getMealsPerDay(), highLowDayRatio, timingFats, configuredMeals);
    }
}
