package com.iceflyer3.pfd.ui.model.proxy.food;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.data.factory.FoodTaskFactory;
import com.iceflyer3.pfd.data.task.food.CanDeleteFoodTask;
import com.iceflyer3.pfd.data.task.food.DeleteFoodTask;
import com.iceflyer3.pfd.data.task.food.IsUniqueComplexFoodTask;
import com.iceflyer3.pfd.data.task.food.IsUniqueSimpleFoodTask;
import com.iceflyer3.pfd.ui.model.api.NutritionFactsReader;
import com.iceflyer3.pfd.ui.model.api.proxy.ProxyDataResultCallback;
import com.iceflyer3.pfd.ui.model.api.proxy.ProxyOperationResultCallback;
import com.iceflyer3.pfd.ui.model.proxy.base.AbstractNutritionFactReaderProxy;
import com.iceflyer3.pfd.ui.model.proxy.ProxyOperation;
import com.iceflyer3.pfd.ui.model.viewmodel.FoodAsIngredientViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.ComplexFoodViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.SimpleFoodViewModel;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.core.task.TaskExecutor;

import java.util.List;

public abstract class AbstractFoodModelProxy<ModelType extends NutritionFactsReader> extends AbstractNutritionFactReaderProxy<ModelType>
{
    private final static Logger LOG = LoggerFactory.getLogger(AbstractFoodModelProxy.class);

    protected final FoodTaskFactory foodTaskFactory;

    protected AbstractFoodModelProxy(TaskExecutor taskExecutor, FoodTaskFactory foodTaskFactory) {
        super(taskExecutor);
        this.foodTaskFactory = foodTaskFactory;
    }

    @Override
    public void delete(ProxyOperationResultCallback deleteResultCallback) {
        validateProxyOperationInvocation(ProxyOperation.DELETE);
        DeleteFoodTask deleteFoodTask = foodTaskFactory.deleteFoodAsync(getId());
        deleteFoodTask.setOnSucceeded(event -> {
            this.isDeleted = true;
            this.viewModel = null;
            deleteResultCallback.receiveResult(true, "The food has been deleted.");
        });
        deleteFoodTask.setOnFailed(event -> {
            LOG.error("An error has occurred while deleting the food {}", getId(), deleteFoodTask.getException());
            deleteResultCallback.receiveResult(false, "Failed to delete the food. An error has occurred.");
        });
        taskExecutor.execute(deleteFoodTask);
    }

    /**
     * Determines if a food may be deleted or not. The food may not be deleted if it used as an
     * ingredient anywhere else. This includes as an ingredient in other foods, meal planning meals,
     * or recipes.
     *
     * This is done as its own function instead of transparently during delete so that feedback may be
     * provided via the UI detailing to which owners this food serves as an ingredient, if any.
     *
     * If the food may be deleted and it is not used as an ingredient anywhere the resulting list will
     * be empty (have a size of zero). Otherwise the list will contain the names and types (recipe, food, etc..)
     * of the owners for which this food serves as an ingredient.
     *
     * @param canDeleteCallback Callback which returns the results of if the food may be deleted or not
     */
    public void canDelete(ProxyDataResultCallback<List<FoodAsIngredientViewModel>> canDeleteCallback)
    {
        CanDeleteFoodTask canDeleteFoodTask = foodTaskFactory.checkCanDeleteFood(getId());
        canDeleteFoodTask.setOnSucceeded(event -> {
            canDeleteCallback.receiveResult(true, canDeleteFoodTask.getValue());
        });
        canDeleteFoodTask.setOnFailed(event -> {
            LOG.error("An error has occurred while checking if the food {} may be deleted", getId(), canDeleteFoodTask.getException());
            canDeleteCallback.receiveResult(false, null);
        });
        taskExecutor.execute(canDeleteFoodTask);
    }

    public void isUnique(ProxyDataResultCallback<Boolean> isUniqueCallback)
    {
        if (viewModel instanceof SimpleFoodViewModel simpleFood)
        {
            IsUniqueSimpleFoodTask isUniqueSimpleFoodTask = foodTaskFactory.isSimpleFoodUnique(simpleFood);
            isUniqueSimpleFoodTask.setOnSucceeded(event -> {
                isUniqueCallback.receiveResult(true, isUniqueSimpleFoodTask.getValue());
            });
            isUniqueSimpleFoodTask.setOnFailed(event -> {
                LOG.error("An error has occurred while checking if the simple food {} is unique", getId(), isUniqueSimpleFoodTask.getException());
                isUniqueCallback.receiveResult(false, null);
            });
            taskExecutor.execute(isUniqueSimpleFoodTask);
        }
        else if (viewModel instanceof ComplexFoodViewModel complexFood)
        {
            IsUniqueComplexFoodTask isUniqueComplexFoodTask = foodTaskFactory.isComplexFoodUnique(complexFood);
            isUniqueComplexFoodTask.setOnSucceeded(event -> {
                isUniqueCallback.receiveResult(true, isUniqueComplexFoodTask.getValue());
            });
            isUniqueComplexFoodTask.setOnFailed(event -> {
                LOG.error("An error has occurred while checking if the complex food {} is unique", getId(), isUniqueComplexFoodTask.getException());
                isUniqueCallback.receiveResult(false, false);
            });
            taskExecutor.execute(isUniqueComplexFoodTask);
        }
    }
}
