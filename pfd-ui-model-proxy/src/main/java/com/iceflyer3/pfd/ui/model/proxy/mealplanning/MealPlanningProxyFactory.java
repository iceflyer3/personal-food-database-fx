package com.iceflyer3.pfd.ui.model.proxy.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.data.factory.MealPlanningTaskFactory;
import com.iceflyer3.pfd.data.task.mealplanning.GetActiveMealPlanForUserTask;
import com.iceflyer3.pfd.data.task.mealplanning.GetMealPlanByIdTask;
import com.iceflyer3.pfd.data.task.mealplanning.GetMealPlansForUserTask;
import com.iceflyer3.pfd.data.task.mealplanning.calendar.GetMealPlanCalendarDaysForUserTask;
import com.iceflyer3.pfd.data.task.mealplanning.carbcycling.GetCarbCyclingPlanByIdTask;
import com.iceflyer3.pfd.data.task.mealplanning.carbcycling.GetCarbCyclingPlansForMealPlanTask;
import com.iceflyer3.pfd.data.task.mealplanning.carbcycling.day.GetCarbCyclingDayByIdTask;
import com.iceflyer3.pfd.data.task.mealplanning.carbcycling.day.GetCarbCyclingDaysForCarbCyclingPlanTask;
import com.iceflyer3.pfd.data.task.mealplanning.carbcycling.meal.GetCarbCyclingMealByIdTask;
import com.iceflyer3.pfd.data.task.mealplanning.carbcycling.meal.GetCarbCyclingMealsForDayTask;
import com.iceflyer3.pfd.data.task.mealplanning.day.GetDayForMealPlanByIdTask;
import com.iceflyer3.pfd.data.task.mealplanning.day.GetDaysForMealPlanTask;
import com.iceflyer3.pfd.data.task.mealplanning.meal.GetMealByIdTask;
import com.iceflyer3.pfd.data.task.mealplanning.meal.GetMealsForDayTask;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.carbcycling.CarbCyclingDayModelProxy;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.carbcycling.CarbCyclingMealModelProxy;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.carbcycling.CarbCyclingPlanModelProxy;
import com.iceflyer3.pfd.ui.model.api.proxy.ProxyDataResultCallback;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.carbcycling.ReadOnlyCarbCyclingMealModelProxy;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class MealPlanningProxyFactory {

    private static final Logger LOG = LoggerFactory.getLogger(MealPlanningProxyFactory.class);

    private final TaskExecutor taskExecutor;
    private final MealPlanningTaskFactory mealPlanningTaskFactory;

    public MealPlanningProxyFactory(TaskExecutor taskExecutor,
                                    MealPlanningTaskFactory mealPlanningTaskFactory)
    {
        this.taskExecutor = taskExecutor;
        this.mealPlanningTaskFactory = mealPlanningTaskFactory;
    }

    public void getMealPlan(UUID planId, UserViewModel forUser, ProxyDataResultCallback<MealPlanModelProxy> callback)
    {
        if (planId == null)
        {
            MealPlanModelProxy proxy = new MealPlanModelProxy(taskExecutor, mealPlanningTaskFactory, forUser);
            callback.receiveResult(true, proxy);
        }
        else
        {
            GetMealPlanByIdTask getMealPlanByIdTask = mealPlanningTaskFactory.getMealPlanById(planId);
            getMealPlanByIdTask.setOnSucceeded(event -> {
                MealPlanModelProxy proxy = new MealPlanModelProxy(taskExecutor, mealPlanningTaskFactory, getMealPlanByIdTask.getValue());
                callback.receiveResult(true, proxy);
            });
            getMealPlanByIdTask.setOnFailed(event -> {
                LOG.error("An error has occurred while loading the meal plan with ID {}", planId, getMealPlanByIdTask.getException());
                callback.receiveResult(false, null);
            });
            taskExecutor.execute(getMealPlanByIdTask);
        }
    }

    public void getActiveMealPlanForUser(UUID userId, ProxyDataResultCallback<Optional<MealPlanModelProxy>> callback)
    {
        GetActiveMealPlanForUserTask getActivePlanTask = mealPlanningTaskFactory.getActiveMealPlanForUser(userId);
        getActivePlanTask.setOnSucceeded(event -> {
            if (getActivePlanTask.getValue().isPresent())
            {
                MealPlanModelProxy proxy = new MealPlanModelProxy(taskExecutor, mealPlanningTaskFactory, getActivePlanTask.getValue().get());
                callback.receiveResult(true, Optional.of(proxy));
            }
            else
            {
                callback.receiveResult(true, Optional.empty());
            }
        });
        getActivePlanTask.setOnFailed(event -> {
            LOG.error("An error has occurred while searching for an active meal plan for user {}", userId, getActivePlanTask.getException());
            callback.receiveResult(false, null);
        });
        taskExecutor.execute(getActivePlanTask);
    }

    public void getMealPlansForUser(UserViewModel forUser, ProxyDataResultCallback<List<MealPlanModelProxy>> callback)
    {
        GetMealPlansForUserTask getMealPlansForUserTask = mealPlanningTaskFactory.getMealPlansByUserAsync(forUser.getUserId());
        getMealPlansForUserTask.setOnSucceeded(event -> {
            List<MealPlanModelProxy> proxies = getMealPlansForUserTask
                                                    .getValue()
                                                    .stream()
                                                    .map(viewModel -> new MealPlanModelProxy(taskExecutor, mealPlanningTaskFactory, viewModel))
                                                    .collect(Collectors.toList());
            callback.receiveResult(true, proxies);
        });
        getMealPlansForUserTask.setOnFailed(event -> {
            LOG.error("An error has occurred while loading the meal plans for user {}", forUser.getUserId(), getMealPlansForUserTask.getException());
            callback.receiveResult(false, null);
        });
        taskExecutor.execute(getMealPlansForUserTask);
    }

    /**
     * Get a CarbCyclingProxy for the given Carb Cycling Plan ID. If the Plan ID is null you will receive back a
     * proxy for a new Carb Cycling Plan that has not yet been persisted.
     * @param planId The ID of the carb cycling plan to retrieve
     * @param callback Callback to be used by clients to receive the proxy that is generated
     */
    public void getCarbCyclingPlan(UUID planId, ProxyDataResultCallback<CarbCyclingPlanModelProxy> callback)
    {
        if (planId == null)
        {
            CarbCyclingPlanModelProxy proxy = new CarbCyclingPlanModelProxy(taskExecutor, mealPlanningTaskFactory);
            callback.receiveResult(true, proxy);
        }
        else
        {
            // Otherwise we want to load from the DB
            GetCarbCyclingPlanByIdTask getPlanTask = mealPlanningTaskFactory.getCarbCyclingPlanByIdAsync(planId);
            getPlanTask.setOnSucceeded(event -> {
                CarbCyclingPlanModelProxy proxy = new CarbCyclingPlanModelProxy(taskExecutor, mealPlanningTaskFactory, getPlanTask.getValue());
                callback.receiveResult(true, proxy);
            });
            getPlanTask.setOnFailed(event -> {
                LOG.error("An error has occurred while loading the carb cycling plan", getPlanTask.getException());
                callback.receiveResult(false, null);
            });
            taskExecutor.execute(getPlanTask);
        }
    }

    /**
     * Get a list of CarbCyclingPlanModelProx instances for all carb cycling plans for the specified meal plan
     * @param mealPlanId The id of the meal plan to search for carb cycling plans for
     * @param callback Callback to be used by clients to receive the proxies that are generated
     */
    public void getCarbCyclingPlansForMealPlan(UUID mealPlanId, ProxyDataResultCallback<List<CarbCyclingPlanModelProxy>> callback)
    {
        GetCarbCyclingPlansForMealPlanTask getPlansForUserTask = mealPlanningTaskFactory.getCarbCyclingPlanByMealPlanAsync(mealPlanId);
        getPlansForUserTask.setOnSucceeded(event -> {
            List<CarbCyclingPlanModelProxy> proxies = getPlansForUserTask
                                                            .getValue()
                                                            .stream()
                                                            .map(viewModel -> new CarbCyclingPlanModelProxy(taskExecutor, mealPlanningTaskFactory, viewModel))
                                                            .collect(Collectors.toList());
            callback.receiveResult(true, proxies);
        });
        getPlansForUserTask.setOnFailed(event -> {
            LOG.error("An error has occurred while loading the carb cycling plans for meal plan {}", mealPlanId, getPlansForUserTask.getException());
            callback.receiveResult(false, null);
        });
        taskExecutor.execute(getPlansForUserTask);
    }

    // Getters for getting a meal plan day and all meal plan days. Strictly speaking the single probably doesn't need to retrieve by ID but
    // we might as well support it anyways for consistency sake

    // When there is no active carb cycling plan during day creation OR we're loading a single day from the DB
    /*
     * TODO: At some point we should investigate splitting methods like this out into "get" and "create" functions. This would
     *       make for a cleaner and clearer / more explicit api. I don't really think there is any reason to tie them to together
     *       like this and we should know which once we're doing anyways.
     *
     *       However, this works well enough for the moment and will wait until a later time when it is more worth the effort.
     */
    public void getMealPlanDayById(UUID dayId, MealPlanModelProxy mealPlan, ProxyDataResultCallback<MealPlanDayModelProxy> callback)
    {
        if (dayId == null)
        {
            MealPlanDayModelProxy newProxy;
            newProxy = new MealPlanDayModelProxy(taskExecutor, mealPlanningTaskFactory, mealPlan.getViewModel());
            callback.receiveResult(true, newProxy);
        }
        else
        {
            GetDayForMealPlanByIdTask getDayForMealPlanByIdTask = mealPlanningTaskFactory.getDayByIdAsync(dayId);
            getDayForMealPlanByIdTask.setOnScheduled(event -> {
                MealPlanDayModelProxy newProxy = new MealPlanDayModelProxy(taskExecutor, mealPlanningTaskFactory, getDayForMealPlanByIdTask.getValue());
                callback.receiveResult(true, newProxy);
            });
            getDayForMealPlanByIdTask.setOnFailed(event -> {
                LOG.error("An error has occurred while loading the day {} for meal plan {}", dayId, mealPlan.getId(), getDayForMealPlanByIdTask.getException());
                callback.receiveResult(false, null);
            });
            taskExecutor.execute(getDayForMealPlanByIdTask);
        }
    }

    // When there is an active carb cycling day during day creation
    public void getCarbCyclingDayForMealPlanById(UUID dayId, MealPlanModelProxy mealPlan, CarbCyclingPlanModelProxy activeCarbCyclingPlan, ProxyDataResultCallback<CarbCyclingDayModelProxy> callback)
    {
        if (dayId == null)
        {
            CarbCyclingDayModelProxy newProxy;
            newProxy = new CarbCyclingDayModelProxy(taskExecutor, mealPlanningTaskFactory, mealPlan.getViewModel(), activeCarbCyclingPlan.getViewModel());
            callback.receiveResult(true, newProxy);
        }
        else
        {
            GetCarbCyclingDayByIdTask getCarbCyclingDayByIdTask = mealPlanningTaskFactory.getCarbCyclingDayByIdTask(dayId);
            getCarbCyclingDayByIdTask.setOnScheduled(event -> {
                CarbCyclingDayModelProxy newProxy = new CarbCyclingDayModelProxy(taskExecutor, mealPlanningTaskFactory, getCarbCyclingDayByIdTask.getValue());
                callback.receiveResult(true, newProxy);
            });
            getCarbCyclingDayByIdTask.setOnFailed(event -> {
                LOG.error("An error has occurred while loading the day {} for meal plan {}", dayId, mealPlan.getId(), getCarbCyclingDayByIdTask.getException());
                callback.receiveResult(false, null);
            });
            taskExecutor.execute(getCarbCyclingDayByIdTask);
        }
    }

    public void getDaysForMealPlan(MealPlanModelProxy mealPlan, ProxyDataResultCallback<List<MealPlanDayModelProxy>> callback)
    {
        GetDaysForMealPlanTask getDaysForMealPlanTask = mealPlanningTaskFactory.getDaysForMealPlan(mealPlan.getId());
        getDaysForMealPlanTask.setOnSucceeded(event -> {
            List<MealPlanDayModelProxy> proxies = getDaysForMealPlanTask
                                                        .getValue()
                                                        .stream()
                                                        .map(viewModel -> new MealPlanDayModelProxy(taskExecutor, mealPlanningTaskFactory, viewModel))
                                                        .collect(Collectors.toList());
            callback.receiveResult(true, proxies);
        });
        getDaysForMealPlanTask.setOnFailed(event -> {
            LOG.error("An error has occurred while loading the days for the meal plan with ID {}", mealPlan.getId(), getDaysForMealPlanTask.getException());
            callback.receiveResult(false, null);
        });
        taskExecutor.execute(getDaysForMealPlanTask);
    }

    public void getCarbCyclingDaysForMealPlan(CarbCyclingPlanModelProxy carbCyclingPlan, ProxyDataResultCallback<List<CarbCyclingDayModelProxy>> callback)
    {
        GetCarbCyclingDaysForCarbCyclingPlanTask getCarbCyclingDaysTask = mealPlanningTaskFactory.getCarbCyclingDaysForMealPlan(carbCyclingPlan.getId());
        getCarbCyclingDaysTask.setOnSucceeded(event -> {
            List<CarbCyclingDayModelProxy> proxies = getCarbCyclingDaysTask
                                                            .getValue()
                                                            .stream()
                                                            .map(viewModel -> new CarbCyclingDayModelProxy(taskExecutor, mealPlanningTaskFactory, viewModel))
                                                            .collect(Collectors.toList());
            callback.receiveResult(true, proxies);
        });
        getCarbCyclingDaysTask.setOnFailed(event -> {
            LOG.error("An error has occurred while loading the carb cycling days for the carb cycling plan {}", carbCyclingPlan.getId(), getCarbCyclingDaysTask.getException());
            callback.receiveResult(false, null);
        });
        taskExecutor.execute(getCarbCyclingDaysTask);
    }

    public void getReadOnlyMealByMealId(UUID mealId, ProxyDataResultCallback<ReadOnlyMealPlanMealModelProxy> callback)
    {
        if (mealId == null)
        {
            throw new IllegalArgumentException("The ID of the meal to retrieve must be provided");
        }
        else
        {
            GetMealByIdTask getMealByIdTask = mealPlanningTaskFactory.getMealByIdAsync(mealId);
            getMealByIdTask.setOnSucceeded(event -> {
                ReadOnlyMealPlanMealModelProxy readOnlyMealProxy = new ReadOnlyMealPlanMealModelProxy(taskExecutor, mealPlanningTaskFactory, getMealByIdTask.getValue());
                callback.receiveResult(true, readOnlyMealProxy);
            });
            getMealByIdTask.setOnFailed(event -> {
                LOG.error("An error has occurred while loading the meal with ID {}", mealId, getMealByIdTask.getException());
                callback.receiveResult(false, null);
            });
            taskExecutor.execute(getMealByIdTask);
        }
    }

    public void getReadOnlyCarbCyclingMealById(UUID mealId, ProxyDataResultCallback<ReadOnlyCarbCyclingMealModelProxy> callback)
    {
        if (mealId == null)
        {
            throw new IllegalArgumentException("The ID of the meal to retrieve must be provided");
        }
        else
        {
            GetCarbCyclingMealByIdTask getMealByIdTask = mealPlanningTaskFactory.getCarbCyclingMealByIdAsync(mealId);
            getMealByIdTask.setOnSucceeded(event -> {
                ReadOnlyCarbCyclingMealModelProxy readOnlyMealProxy = new ReadOnlyCarbCyclingMealModelProxy(taskExecutor, mealPlanningTaskFactory, getMealByIdTask.getValue());
                callback.receiveResult(true, readOnlyMealProxy);
            });
            getMealByIdTask.setOnFailed(event -> {
                LOG.error("An error has occurred while loading the meal with ID {}", mealId, getMealByIdTask.getException());
                callback.receiveResult(false, null);
            });
            taskExecutor.execute(getMealByIdTask);
        }
    }

    public void getMealForDayById(UUID mealId, MealPlanModelProxy mealPlan, MealPlanDayModelProxy forDay, ProxyDataResultCallback<MealPlanMealModelProxy> callback)
    {
        if (mealId == null)
        {
            MealPlanMealModelProxy newProxy = new MealPlanMealModelProxy(taskExecutor, mealPlanningTaskFactory, mealPlan.getViewModel(), forDay.getViewModel());
            callback.receiveResult(true, newProxy);
        }
        else
        {
            GetMealByIdTask getMealByIdTask = mealPlanningTaskFactory.getMealByIdAsync(mealId);
            getMealByIdTask.setOnSucceeded(event -> {
                MealPlanMealModelProxy newProxy = new MealPlanMealModelProxy(taskExecutor, mealPlanningTaskFactory, mealPlan.getViewModel(), forDay.getViewModel(), getMealByIdTask.getValue());
                callback.receiveResult(true, newProxy);
            });
            getMealByIdTask.setOnFailed(event -> {
                LOG.error("An error has occurred while loading the meal {} for day {} and meal plan {}", mealId, forDay.getId(), mealPlan.getId(), getMealByIdTask.getException());
                callback.receiveResult(false, null);
            });
            taskExecutor.execute(getMealByIdTask);
        }
    }

    public void getCarbCyclingMealForDayById(UUID mealId, MealPlanModelProxy mealPlan, CarbCyclingDayModelProxy forDay, ProxyDataResultCallback<CarbCyclingMealModelProxy> callback)
    {
        if (mealId == null)
        {
            CarbCyclingMealModelProxy newProxy = new CarbCyclingMealModelProxy(taskExecutor, mealPlanningTaskFactory, forDay.getViewModel());
            callback.receiveResult(true, newProxy);
        }
        else
        {
            GetCarbCyclingMealByIdTask getMealByIdTask = mealPlanningTaskFactory.getCarbCyclingMealByIdAsync(mealId);
            getMealByIdTask.setOnSucceeded(event -> {
                CarbCyclingMealModelProxy newProxy = new CarbCyclingMealModelProxy(taskExecutor, mealPlanningTaskFactory, forDay.getViewModel(), getMealByIdTask.getValue());
                callback.receiveResult(true, newProxy);
            });
            getMealByIdTask.setOnFailed(event -> {
                LOG.error("An error has occurred while loading the meal {} for day {} and meal plan {}", mealId, forDay.getId(), mealPlan.getId(), getMealByIdTask.getException());
                callback.receiveResult(false, null);
            });
            taskExecutor.execute(getMealByIdTask);
        }
    }

    public void getMealsForDay(MealPlanModelProxy mealPlan, MealPlanDayModelProxy forDay, ProxyDataResultCallback<List<MealPlanMealModelProxy>> callback)
    {
        GetMealsForDayTask getMealsForDayTask = mealPlanningTaskFactory.getMealsForDay(forDay.getId());
        getMealsForDayTask.setOnSucceeded(event -> {
            List<MealPlanMealModelProxy> proxies = getMealsForDayTask.getValue()
                    .stream()
                    .map(viewModel ->
                            new MealPlanMealModelProxy(taskExecutor, mealPlanningTaskFactory,
                                    mealPlan.getViewModel(),
                                    forDay.getViewModel(),
                                    viewModel))
                    .collect(Collectors.toList());
            callback.receiveResult(true, proxies);
        });
        getMealsForDayTask.setOnFailed(event -> {
            LOG.error("An error has occurred while loading the meals for the day {} and meal plan {}", forDay.getId(), mealPlan.getId(), getMealsForDayTask.getException());
            callback.receiveResult(false, null);
        });
        taskExecutor.execute(getMealsForDayTask);
    }

    public void getCarbCyclingMealsForDay(CarbCyclingDayModelProxy forDay, ProxyDataResultCallback<List<CarbCyclingMealModelProxy>> callback)
    {
        GetCarbCyclingMealsForDayTask getCarbCyclingMealsForDayTask = mealPlanningTaskFactory.getCarbCyclingMealsForDay(forDay.getId());
        getCarbCyclingMealsForDayTask.setOnSucceeded(event -> {
            List<CarbCyclingMealModelProxy> proxies = getCarbCyclingMealsForDayTask
                                                            .getValue()
                                                            .stream()
                                                            .map(viewModel -> new CarbCyclingMealModelProxy(taskExecutor, mealPlanningTaskFactory, forDay.getViewModel(), viewModel))
                                                            .collect(Collectors.toList());
            callback.receiveResult(true, proxies);
        });
        getCarbCyclingMealsForDayTask.setOnFailed(event -> {
            LOG.error("An error has occurred while loading the carb cycling meals for the carb cycling day day {}", forDay.getId(), getCarbCyclingMealsForDayTask.getException());
            callback.receiveResult(false, null);
        });
        taskExecutor.execute(getCarbCyclingMealsForDayTask);
    }

    public void getCalendarDaysForUser(UUID userId, ProxyDataResultCallback<Set<MealPlanCalendarDayModelProxy>> callback)
    {
        GetMealPlanCalendarDaysForUserTask getCalendarDaysTask = mealPlanningTaskFactory.getCalendarDaysForUser(userId);
        getCalendarDaysTask.setOnSucceeded(event -> {
            Set<MealPlanCalendarDayModelProxy> proxies = getCalendarDaysTask
                    .getValue()
                    .stream()
                    .map(viewModel -> new MealPlanCalendarDayModelProxy(taskExecutor, mealPlanningTaskFactory, viewModel))
                    .collect(Collectors.toSet());
            callback.receiveResult(true, proxies);
        });
        getCalendarDaysTask.setOnFailed(event -> {
            LOG.error("An error has occurred while loading the meal plan calendar days for user {}", userId, getCalendarDaysTask.getException());
            callback.receiveResult(false, null);
        });
        taskExecutor.execute(getCalendarDaysTask);
    }
}
