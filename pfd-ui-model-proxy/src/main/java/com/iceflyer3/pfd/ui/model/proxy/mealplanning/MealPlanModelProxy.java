package com.iceflyer3.pfd.ui.model.proxy.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.factory.MealPlanningTaskFactory;
import com.iceflyer3.pfd.data.task.mealplanning.ChangeMealPlanActivationStatusTask;
import com.iceflyer3.pfd.data.task.mealplanning.DeleteMealPlanTask;
import com.iceflyer3.pfd.data.task.mealplanning.SaveMealPlanTask;
import com.iceflyer3.pfd.enums.*;
import com.iceflyer3.pfd.ui.model.api.mealplanning.MealPlanModel;
import com.iceflyer3.pfd.ui.model.api.mealplanning.MealPlanNutrientSuggestionModel;
import com.iceflyer3.pfd.ui.model.api.mealplanning.registration.MealPlanningRegistration;
import com.iceflyer3.pfd.ui.model.api.proxy.ProxyDataResultCallback;
import com.iceflyer3.pfd.ui.model.api.proxy.ProxyOperationResultCallback;
import com.iceflyer3.pfd.ui.model.proxy.ProxyOperation;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanViewModel;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.task.TaskExecutor;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public class MealPlanModelProxy extends AbstractMealPlanningModelProxy<MealPlanViewModel> implements MealPlanModel
{
    private static final Logger LOG = LoggerFactory.getLogger(MealPlanModelProxy.class);

    /**
     * Constructor for use when we don't have a ViewModel already loaded from the persistence layer
     * @param taskExecutor Task Executor used for executing save / delete tasks
     * @param mealPlanningTaskFactory Task Factory used to acquire instances of the tasks to execute
     * @param forUser The user that this plan is for
     */
    public MealPlanModelProxy(TaskExecutor taskExecutor,
                              MealPlanningTaskFactory mealPlanningTaskFactory,
                              UserViewModel forUser)
    {
        super(taskExecutor, mealPlanningTaskFactory);
        this.viewModel = new MealPlanViewModel(null);
    }

    /**
     * Constructor for use when we have already loaded a ViewModel from the persistence layer.
     * @param taskExecutor Task Executor used for executing save / delete tasks
     * @param mealPlanningTaskFactory Task Factory used to acquire instances of the tasks to execute
     * @param viewModel The view model as loaded from the persistence layer
     */
    public MealPlanModelProxy(TaskExecutor taskExecutor,
                              MealPlanningTaskFactory mealPlanningTaskFactory,
                              MealPlanViewModel viewModel)
    {
        super(taskExecutor, mealPlanningTaskFactory);
        this.viewModel = viewModel;
    }

    /**
     * @deprecated
     * You should use the version that accepts an instance of MealPlanCalculationDetails instead as these details
     * are required to perform the calculations needed for saving the meal plan.
     *
     * This method will throw an UnsupportedOperationException if used.
     */
    @Override
    @Deprecated()
    public void save(ProxyOperationResultCallback saveResultCallback) {
        // For meal plans specifically we always require an instance of MealPlanCalcDetails that inform how to calculate some of the information contained within the meal plan.
        throw new UnsupportedOperationException("This version of save is not supported for this proxy. You must use the version that provides the meal plan calculation details.");
    }

    public void save(UserViewModel forUser, MealPlanningRegistration mealPlanRegistration, ProxyDataResultCallback<MealPlanModelProxy> saveResultCallback)
    {
        validateProxyOperationInvocation(ProxyOperation.SAVE);
        // Comment here about how the only part of a meal plan that can be updated is active status and why (cause of invalidation of meals otherwise)
        if (getId() == null)
        {
            SaveMealPlanTask saveMealPlanTask = mealPlanningTaskFactory.createPlanAsync(forUser.getUserId(), mealPlanRegistration);
            saveMealPlanTask.setOnSucceeded(event -> {
                this.viewModel = saveMealPlanTask.getValue();
                saveResultCallback.receiveResult(true, this);
            });
            saveMealPlanTask.setOnFailed(event -> {
                LOG.error("An error has occurred while saving the meal plan", saveMealPlanTask.getException());
                saveResultCallback.receiveResult(false, null);
            });
            taskExecutor.execute(saveMealPlanTask);
        }
    }

    @Override
    public void delete(ProxyOperationResultCallback deleteResultCallback)
    {
        validateProxyOperationInvocation(ProxyOperation.DELETE);

        DeleteMealPlanTask deletePlanTask = mealPlanningTaskFactory.deletePlanAsync(viewModel.getId());
        deletePlanTask.setOnSucceeded(event -> {
            isDeleted = true;
            viewModel = null;
            deleteResultCallback.receiveResult(true, "The meal has been deleted!");
        });
        deletePlanTask.setOnFailed(event -> {
            LOG.error("An error has occurred while deleting the meal plan with ID {}", viewModel.getId(), deletePlanTask.getException());
            deleteResultCallback.receiveResult(false, "Failed to delete the meal. An error has occurred.");
        });
        taskExecutor.execute(deletePlanTask);
    }

    public void activate(ProxyDataResultCallback<MealPlanModelProxy> activateResultCallback)
    {
        validateProxyOperationInvocation(ProxyOperation.SAVE);

        ChangeMealPlanActivationStatusTask changeStatusTask = mealPlanningTaskFactory.changeMealPlanActivationStatusAsync(viewModel.getId(), true);
        changeStatusTask.setOnSucceeded(event -> {
            this.viewModel = changeStatusTask.getValue();
            activateResultCallback.receiveResult(true, this);
        });
        changeStatusTask.setOnFailed(event -> {
            LOG.error("An error has occurred while activating the meal plan with ID {}", viewModel.getId(), changeStatusTask.getException());
            activateResultCallback.receiveResult(false, null);
        });
        taskExecutor.execute(changeStatusTask);
    }

    public void deactivate(ProxyDataResultCallback<MealPlanModelProxy> deactivateResultCallback)
    {
        validateProxyOperationInvocation(ProxyOperation.SAVE);

        ChangeMealPlanActivationStatusTask changeStatusTask = mealPlanningTaskFactory.changeMealPlanActivationStatusAsync(viewModel.getId(), false);
        changeStatusTask.setOnSucceeded(event -> {
            this.viewModel = changeStatusTask.getValue();
            deactivateResultCallback.receiveResult(true, this);
        });
        changeStatusTask.setOnFailed(event -> {
            LOG.error("An error has occurred while deactivating the meal plan with ID {}", viewModel.getId(), changeStatusTask.getException());
            deactivateResultCallback.receiveResult(false, null);
        });
        taskExecutor.execute(changeStatusTask);
    }

    @Override
    public UUID getId()
    {
        validateProxyPropertyInvocation();
        return viewModel.getId();
    }

    @Override
    public int getAge()
    {
        validateProxyPropertyInvocation();
        return viewModel.getAge();
    }

    @Override
    public IntegerProperty ageProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.ageProperty();
    }

    @Override
    public void setAge(int age)
    {
        validateProxyPropertyInvocation();
        viewModel.setAge(age);
    }

    @Override
    public BigDecimal getHeight()
    {
        validateProxyPropertyInvocation();
        return viewModel.getHeight();
    }

    @Override
    public ObjectProperty<BigDecimal> heightProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.heightProperty();
    }

    @Override
    public void setHeight(BigDecimal height)
    {
        validateProxyPropertyInvocation();
        viewModel.setHeight(height);
    }

    @Override
    public BigDecimal getWeight()
    {
        validateProxyPropertyInvocation();
        return viewModel.getWeight();
    }

    @Override
    public ObjectProperty<BigDecimal> weightProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.weightProperty();
    }

    @Override
    public void setWeight(BigDecimal weight)
    {
        validateProxyPropertyInvocation();
        viewModel.setWeight(weight);
    }

    @Override
    public Sex getSex()
    {
        validateProxyPropertyInvocation();
        return viewModel.getSex();
    }

    @Override
    public ObjectProperty<Sex> sexProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.sexProperty();
    }

    @Override
    public void setSex(Sex sex)
    {
        validateProxyPropertyInvocation();
        viewModel.setSex(sex);
    }

    @Override
    public BigDecimal getBodyFatPercentage()
    {
        validateProxyPropertyInvocation();
        return viewModel.getBodyFatPercentage();
    }

    @Override
    public ObjectProperty<BigDecimal> bodyFatPercentageProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.bodyFatPercentageProperty();
    }

    @Override
    public void setBodyFatPercentage(BigDecimal bodyFatPercentage)
    {
        validateProxyPropertyInvocation();
        viewModel.setBodyFatPercentage(bodyFatPercentage);
    }

    @Override
    public ActivityLevel getActivityLevel()
    {
        validateProxyPropertyInvocation();
        return viewModel.getActivityLevel();
    }

    @Override
    public ObjectProperty<ActivityLevel> activityLevelProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.activityLevelProperty();
    }

    @Override
    public void setActivityLevel(ActivityLevel activityLevel)
    {
        validateProxyPropertyInvocation();
        viewModel.setActivityLevel(activityLevel);
    }

    @Override
    public FitnessGoal getFitnessGoal()
    {
        validateProxyPropertyInvocation();
        return viewModel.getFitnessGoal();
    }

    @Override
    public ObjectProperty<FitnessGoal> fitnessGoalProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.fitnessGoalProperty();
    }

    @Override
    public void setFitnessGoal(FitnessGoal fitnessGoal)
    {
        validateProxyPropertyInvocation();
        viewModel.setFitnessGoal(fitnessGoal);
    }

    @Override
    public BasalMetabolicRateFormula getBmrFormula()
    {
        validateProxyPropertyInvocation();
        return viewModel.getBmrFormula();
    }

    @Override
    public ObjectProperty<BasalMetabolicRateFormula> bmrFormulaProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.bmrFormulaProperty();
    }

    @Override
    public void setBmrFormula(BasalMetabolicRateFormula bmrFormula)
    {
        validateProxyPropertyInvocation();
        viewModel.setBmrFormula(bmrFormula);
    }

    @Override
    public BigDecimal getDailyCalories()
    {
        validateProxyPropertyInvocation();
        return viewModel.getDailyCalories();
    }

    @Override
    public ObjectProperty<BigDecimal> dailyCaloriesProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.dailyCaloriesProperty();
    }

    @Override
    public BigDecimal getTdeeCalories()
    {
        validateProxyPropertyInvocation();
        return viewModel.getTdeeCalories();
    }

    @Override
    public ObjectProperty<BigDecimal> tdeeCaloriesProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.tdeeCaloriesProperty();
    }

    @Override
    public BigDecimal getNutrientSuggestedAmount(Nutrient nutrient)
    {
        validateProxyPropertyInvocation();
        return viewModel.getNutrientSuggestedAmount(nutrient);
    }

    @Override
    public List<MealPlanNutrientSuggestionModel> getCustomNutrients()
    {
        validateProxyPropertyInvocation();
        return viewModel.getCustomNutrients();
    }

    @Override
    public boolean isActive()
    {
        validateProxyPropertyInvocation();
        return viewModel.isActive();
    }

    @Override
    public BooleanProperty isActiveProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.isActiveProperty();
    }

    @Override
    public void toggleActiveStatus()
    {
        validateProxyPropertyInvocation();
        viewModel.toggleActiveStatus();
    }
}
