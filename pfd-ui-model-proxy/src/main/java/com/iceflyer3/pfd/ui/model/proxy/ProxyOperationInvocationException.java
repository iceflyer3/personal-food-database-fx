package com.iceflyer3.pfd.ui.model.proxy;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * Exception that is to be thrown when a client makes a request for the proxy to perform
 * an operation but the operation is invalid for some reason.
 *
 * This is not a checked exception because it will only be thrown when an invalid operation
 * is requested of the proxy by the client.
 *
 * It is also in the same category as an NPE where the client should ensure that the proxy
 * is in a state where it is ready to be and can be used before attempting to use it.
 */
public class ProxyOperationInvocationException extends RuntimeException
{
    public ProxyOperationInvocationException(String message) {
        super(message);
    }

    public ProxyOperationInvocationException(Throwable cause) {
        super(cause);
    }

    public ProxyOperationInvocationException(String message, Throwable cause) {
        super(message, cause);
    }
}
