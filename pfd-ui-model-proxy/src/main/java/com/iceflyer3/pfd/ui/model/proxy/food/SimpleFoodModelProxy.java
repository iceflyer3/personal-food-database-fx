package com.iceflyer3.pfd.ui.model.proxy.food;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.enums.FoodType;
import com.iceflyer3.pfd.enums.UnitOfMeasure;
import com.iceflyer3.pfd.data.factory.FoodTaskFactory;
import com.iceflyer3.pfd.data.task.food.SaveSimpleFoodTask;
import com.iceflyer3.pfd.exception.NonUniqueFoodException;
import com.iceflyer3.pfd.ui.model.api.ServingSizeModel;
import com.iceflyer3.pfd.ui.model.api.proxy.ProxyOperationResultCallback;
import com.iceflyer3.pfd.ui.model.api.food.SimpleFoodModel;
import com.iceflyer3.pfd.ui.model.proxy.ProxyOperation;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.SimpleFoodViewModel;
import com.iceflyer3.pfd.validation.ValidationResult;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.core.task.TaskExecutor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Simple Foods share the same shape as the primary food view model
 * that we use throughout the application and the proxy that sits on
 * top of that view model where needed.
 *
 * As such, this class is just an extension of that proxy that adds
 * support for saving simple foods.
 *
 * This is also the lone class where raw food nutritional data is writable
 * so setters are provided and properties are overridden to be writable.
 */
public class SimpleFoodModelProxy extends AbstractFoodModelProxy<SimpleFoodViewModel> implements SimpleFoodModel {

    private final static Logger LOG = LoggerFactory.getLogger(SimpleFoodModelProxy.class);

    private final UserViewModel createdUser;

    public SimpleFoodModelProxy(TaskExecutor taskExecutor, FoodTaskFactory foodTaskFactory, UserViewModel createdUser) {
        super(taskExecutor, foodTaskFactory);
        this.createdUser = createdUser;
        this.viewModel = new SimpleFoodViewModel(createdUser.getUsername());
    }

    public SimpleFoodModelProxy(TaskExecutor taskExecutor, FoodTaskFactory foodTaskFactory, UserViewModel createdUser, SimpleFoodViewModel viewModel)
    {
        super(taskExecutor, foodTaskFactory);
        this.createdUser = createdUser;
        this.viewModel = viewModel;
    }

    //region Functionality
    ///////////////////////////////////////////////////////////////////
    //
    //                           Functionality
    //
    ///////////////////////////////////////////////////////////////////

    @Override
    public ValidationResult validate() {
        validateProxyPropertyInvocation();
        return viewModel.validate();
    }

    @Override
    public ValidationResult validateProperty(int propertyHashcode)
    {
        validateProxyPropertyInvocation();
        return viewModel.validateProperty(propertyHashcode);
    }

    @Override
    public void save(ProxyOperationResultCallback saveResultCallback) {
        validateProxyOperationInvocation(ProxyOperation.SAVE);
        SaveSimpleFoodTask saveSimpleFoodTask = foodTaskFactory.saveSimpleFoodAsync(createdUser, viewModel);
        saveSimpleFoodTask.setOnSucceeded(event -> {
            viewModel = saveSimpleFoodTask.getValue();
            saveResultCallback.receiveResult(true, "The simple food has been saved!");
        });
        saveSimpleFoodTask.setOnFailed(event -> {
            Throwable exception = saveSimpleFoodTask.getException();
            LOG.error("An error has occurred while saving a new simple food", exception);

            String resultMessage = exception instanceof NonUniqueFoodException ? exception.getMessage() : "Failed to save the simple food. An error has occurred.";
            saveResultCallback.receiveResult(false, resultMessage);
        });
        taskExecutor.execute(saveSimpleFoodTask);
    }

    @Override
    public void addServingSize()
    {
        validateProxyPropertyInvocation();
        viewModel.addServingSize();
    }

    @Override
    public void addServingSize(UnitOfMeasure uom, BigDecimal quantity)
    {
        validateProxyPropertyInvocation();
        viewModel.addServingSize(uom, quantity);
    }

    @Override
    public void removeServingSize(ServingSizeModel servingSize)
    {
        validateProxyPropertyInvocation();
        viewModel.removeServingSize(servingSize);
    }

    @Override
    public void refreshNutritionDetails()
    {
       validateProxyPropertyInvocation();
       viewModel.refreshNutritionDetails();
    }

    //endregion

    //region Getters
    ///////////////////////////////////////////////////////////////////
    //
    //                           Getters
    //
    ///////////////////////////////////////////////////////////////////
    @Override
    public UUID getId() {
        validateProxyPropertyInvocation();
        return viewModel.getId();
    }

    @Override
    public FoodType getFoodType() {
        validateProxyPropertyInvocation();
        return viewModel.getFoodType();
    }

    @Override
    public String getName() {
        validateProxyPropertyInvocation();
        return viewModel.getName();
    }

    @Override
    public String getBrand() {
        validateProxyPropertyInvocation();
        return viewModel.getBrand();
    }

    @Override
    public String getSource() {
        validateProxyPropertyInvocation();
        return viewModel.getSource();
    }

    @Override
    public int getGlycemicIndex() {
        validateProxyPropertyInvocation();
        return viewModel.getGlycemicIndex();
    }

    @Override
    public ObservableList<ServingSizeModel> getServingSizes() {
        validateProxyPropertyInvocation();
        return viewModel.getServingSizes();
    }

    @Override
    public String getCreatedUser() {
        validateProxyPropertyInvocation();
        return viewModel.getCreatedUser();
    }

    @Override
    public LocalDateTime getCreatedDate() {
        validateProxyPropertyInvocation();
        return viewModel.getCreatedDate();
    }

    @Override
    public String getLastModifiedUser() {
        validateProxyPropertyInvocation();
        return viewModel.getLastModifiedUser();
    }

    @Override
    public LocalDateTime getLastModifiedDate() {
        validateProxyPropertyInvocation();
        return viewModel.getLastModifiedDate();
    }
    //endregion

    //region Properties
    ///////////////////////////////////////////////////////////////////
    //
    //                           Properties
    //
    ///////////////////////////////////////////////////////////////////

    @Override
    public StringProperty nameProperty() {
        validateProxyPropertyInvocation();
        return viewModel.nameProperty();
    }

    @Override
    public StringProperty brandProperty() {
        validateProxyPropertyInvocation();
        return viewModel.brandProperty();
    }

    @Override
    public StringProperty sourceProperty() {
        validateProxyPropertyInvocation();
        return viewModel.sourceProperty();
    }

    @Override
    public IntegerProperty glycemicIndexProperty() {
        validateProxyPropertyInvocation();
        return viewModel.glycemicIndexProperty();
    }

    @Override
    public ObjectProperty<FoodType> foodTypeProperty() {
        validateProxyPropertyInvocation();
        return viewModel.foodTypeProperty();
    }

    @Override
    public StringProperty createdUserProperty() {
        validateProxyPropertyInvocation();
        return viewModel.createdUserProperty();
    }

    @Override
    public ObjectProperty<LocalDateTime> createdDateProperty() {
        validateProxyPropertyInvocation();
        return viewModel.createdDateProperty();
    }

    @Override
    public StringProperty lastModifiedUserProperty() {
        validateProxyPropertyInvocation();
        return viewModel.lastModifiedUserProperty();
    }

    @Override
    public ObjectProperty<LocalDateTime> lastModifiedDateProperty() {
        validateProxyPropertyInvocation();
        return viewModel.lastModifiedDateProperty();
    }

    @Override
    public ObjectProperty<BigDecimal> caloriesProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.caloriesProperty();
    }


    @Override
    public ObjectProperty<BigDecimal> proteinProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.proteinProperty();
    }

    @Override
    public ObjectProperty<BigDecimal> totalFatsProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.totalFatsProperty();
    }

    @Override
    public ObjectProperty<BigDecimal> transFatProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.transFatProperty();
    }

    @Override
    public ObjectProperty<BigDecimal> monounsaturatedFatProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.monounsaturatedFatProperty();
    }

    @Override
    public ObjectProperty<BigDecimal> polyunsaturatedFatProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.polyunsaturatedFatProperty();
    }

    @Override
    public ObjectProperty<BigDecimal> saturatedFatProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.saturatedFatProperty();
    }

    @Override
    public ObjectProperty<BigDecimal> carbohydratesProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.carbohydratesProperty();
    }

    @Override
    public ObjectProperty<BigDecimal> sugarsProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.sugarsProperty();
    }

    @Override
    public ObjectProperty<BigDecimal> fiberProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.fiberProperty();
    }

    @Override
    public ObjectProperty<BigDecimal> cholesterolProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.cholesterolProperty();
    }

    @Override
    public ObjectProperty<BigDecimal> ironProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.ironProperty();
    }

    @Override
    public ObjectProperty<BigDecimal> manganeseProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.manganeseProperty();
    }

    @Override
    public ObjectProperty<BigDecimal> copperProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.copperProperty();
    }

    @Override
    public ObjectProperty<BigDecimal> zincProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.zincProperty();
    }

    @Override
    public ObjectProperty<BigDecimal> calciumProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.calciumProperty();
    }

    @Override
    public ObjectProperty<BigDecimal> magnesiumProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.magnesiumProperty();
    }

    @Override
    public ObjectProperty<BigDecimal> phosphorusProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.phosphorusProperty();
    }

    @Override
    public ObjectProperty<BigDecimal> potassiumProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.potassiumProperty();
    }

    @Override
    public ObjectProperty<BigDecimal> sodiumProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.sodiumProperty();
    }

    @Override
    public ObjectProperty<BigDecimal> sulfurProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.sulfurProperty();
    }

    @Override
    public ObjectProperty<BigDecimal> vitaminAProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.vitaminAProperty();
    }

    @Override
    public ObjectProperty<BigDecimal> vitaminCProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.vitaminCProperty();
    }

    @Override
    public ObjectProperty<BigDecimal> vitaminKProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.vitaminKProperty();
    }

    @Override
    public ObjectProperty<BigDecimal> vitaminDProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.vitaminDProperty();
    }

    @Override
    public ObjectProperty<BigDecimal> vitaminEProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.vitaminEProperty();
    }

    @Override
    public ObjectProperty<BigDecimal> vitaminB1Property()
    {
        validateProxyPropertyInvocation();
        return viewModel.vitaminB1Property();
    }

    @Override
    public ObjectProperty<BigDecimal> vitaminB2Property()
    {
        validateProxyPropertyInvocation();
        return viewModel.vitaminB2Property();
    }

    @Override
    public ObjectProperty<BigDecimal> vitaminB3Property()
    {
        validateProxyPropertyInvocation();
        return viewModel.vitaminB3Property();
    }

    @Override
    public ObjectProperty<BigDecimal> vitaminB5Property()
    {
        validateProxyPropertyInvocation();
        return viewModel.vitaminB5Property();
    }

    @Override
    public ObjectProperty<BigDecimal> vitaminB6Property()
    {
        validateProxyPropertyInvocation();
        return viewModel.vitaminB6Property();
    }

    @Override
    public ObjectProperty<BigDecimal> vitaminB7Property()
    {
        validateProxyPropertyInvocation();
        return viewModel.vitaminB7Property();
    }

    @Override
    public ObjectProperty<BigDecimal> vitaminB8Property()
    {
        validateProxyPropertyInvocation();
        return viewModel.vitaminB8Property();
    }

    @Override
    public ObjectProperty<BigDecimal> vitaminB9Property()
    {
        validateProxyPropertyInvocation();
        return viewModel.vitaminB9Property();
    }

    @Override
    public ObjectProperty<BigDecimal> vitaminB12Property()
    {
        validateProxyPropertyInvocation();
        return viewModel.vitaminB12Property();
    }
    //endregion

    //region Setters
    ///////////////////////////////////////////////////////////////////
    //
    //                           Setters
    //
    ///////////////////////////////////////////////////////////////////
    @Override
    public void setName(String foodName) {
        validateProxyPropertyInvocation();
        viewModel.setName(foodName);
    }

    @Override
    public void setBrand(String brandName) {
        validateProxyPropertyInvocation();
        viewModel.setBrand(brandName);
    }

    @Override
    public void setSource(String source) {
        validateProxyPropertyInvocation();
        viewModel.setSource(source);
    }

    @Override
    public void setGlycemicIndex(int glycemicIndex) {
        validateProxyPropertyInvocation();
        viewModel.setGlycemicIndex(glycemicIndex);
    }

    @Override
    public void setFoodType(FoodType foodType) {
        validateProxyPropertyInvocation();
        viewModel.setFoodType(foodType);
    }

    @Override
    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        validateProxyPropertyInvocation();
        viewModel.setLastModifiedDate(lastModifiedDate);
    }

    @Override
    public void setLastModifiedUser(String lastModifiedUser) {
        validateProxyPropertyInvocation();
        viewModel.setLastModifiedUser(lastModifiedUser);
    }

    @Override
    public void setCalories(BigDecimal calories) {
        validateProxyPropertyInvocation();
        viewModel.setCalories(calories);
    }

    @Override
    public void setProtein(BigDecimal protein) {
        validateProxyPropertyInvocation();
        viewModel.setProtein(protein);
    }

    @Override
    public void setTotalFats(BigDecimal totalFats) {
        validateProxyPropertyInvocation();
        viewModel.setTotalFats(totalFats);
    }

    @Override
    public void setTransFat(BigDecimal transFat) {
        validateProxyPropertyInvocation();
        viewModel.setTransFat(transFat);
    }

    @Override
    public void setMonounsaturatedFat(BigDecimal monounsaturatedFat) {
        validateProxyPropertyInvocation();
        viewModel.setMonounsaturatedFat(monounsaturatedFat);
    }

    @Override
    public void setPolyunsaturatedFat(BigDecimal polyunsaturatedFat) {
        validateProxyPropertyInvocation();
        viewModel.setPolyunsaturatedFat(polyunsaturatedFat);
    }

    @Override
    public void setSaturatedFat(BigDecimal saturatedFat) {
        validateProxyPropertyInvocation();
        viewModel.setSaturatedFat(saturatedFat);
    }

    @Override
    public void setCarbohydrates(BigDecimal carbohydrates) {
        validateProxyPropertyInvocation();
        viewModel.setCarbohydrates(carbohydrates);
    }

    @Override
    public void setSugars(BigDecimal sugars) {
        validateProxyPropertyInvocation();
        viewModel.setSugars(sugars);
    }

    @Override
    public void setFiber(BigDecimal fiber) {
        validateProxyPropertyInvocation();
        viewModel.setFiber(fiber);
    }

    @Override
    public void setCholesterol(BigDecimal cholesterol) {
        validateProxyPropertyInvocation();
        viewModel.setCholesterol(cholesterol);
    }

    @Override
    public void setIron(BigDecimal iron) {
        validateProxyPropertyInvocation();
        viewModel.setIron(iron);
    }

    @Override
    public void setManganese(BigDecimal manganese) {
        validateProxyPropertyInvocation();
        viewModel.setManganese(manganese);
    }

    @Override
    public void setCopper(BigDecimal copper) {
        validateProxyPropertyInvocation();
        viewModel.setCopper(copper);
    }

    @Override
    public void setZinc(BigDecimal zinc) {
        validateProxyPropertyInvocation();
        viewModel.setZinc(zinc);
    }

    @Override
    public void setCalcium(BigDecimal calcium) {
        validateProxyPropertyInvocation();
        viewModel.setCalcium(calcium);
    }

    @Override
    public void setMagnesium(BigDecimal magnesium) {
        validateProxyPropertyInvocation();
        viewModel.setMagnesium(magnesium);
    }

    @Override
    public void setPhosphorus(BigDecimal phosphorus) {
        validateProxyPropertyInvocation();
        viewModel.setPhosphorus(phosphorus);
    }

    @Override
    public void setPotassium(BigDecimal potassium) {
        validateProxyPropertyInvocation();
        viewModel.setPotassium(potassium);
    }

    @Override
    public void setSodium(BigDecimal sodium) {
        validateProxyPropertyInvocation();
        viewModel.setSodium(sodium);
    }


    @Override
    public void setSulfur(BigDecimal sulfur) {
        validateProxyPropertyInvocation();
        viewModel.setSulfur(sulfur);
    }

    @Override
    public void setVitaminA(BigDecimal vitaminA) {
        validateProxyPropertyInvocation();
        viewModel.setVitaminA(vitaminA);
    }

    @Override
    public void setVitaminC(BigDecimal vitaminC) {
        validateProxyPropertyInvocation();
        viewModel.setVitaminC(vitaminC);
    }

    @Override
    public void setVitaminK(BigDecimal vitaminK) {
        validateProxyPropertyInvocation();
        viewModel.setVitaminK(vitaminK);
    }

    @Override
    public void setVitaminD(BigDecimal vitaminD) {
        validateProxyPropertyInvocation();
        viewModel.setVitaminD(vitaminD);
    }

    @Override
    public void setVitaminE(BigDecimal vitaminE) {
        validateProxyPropertyInvocation();
        viewModel.setVitaminE(vitaminE);
    }

    @Override
    public void setVitaminB1(BigDecimal vitaminB1) {
        validateProxyPropertyInvocation();
        viewModel.setVitaminB1(vitaminB1);
    }

    @Override
    public void setVitaminB2(BigDecimal vitaminB2) {
        validateProxyPropertyInvocation();
        viewModel.setVitaminB2(vitaminB2);
    }

    @Override
    public void setVitaminB3(BigDecimal vitaminB3) {
        validateProxyPropertyInvocation();
        viewModel.setVitaminB3(vitaminB3);
    }

    @Override
    public void setVitaminB5(BigDecimal vitaminB5) {
        validateProxyPropertyInvocation();
        viewModel.setVitaminB5(vitaminB5);
    }


    @Override
    public void setVitaminB6(BigDecimal vitaminB6) {
        validateProxyPropertyInvocation();
        viewModel.setVitaminB6(vitaminB6);
    }

    @Override
    public void setVitaminB7(BigDecimal vitaminB7) {
        validateProxyPropertyInvocation();
        viewModel.setVitaminB7(vitaminB7);
    }

    @Override
    public void setVitaminB8(BigDecimal vitaminB8) {
        validateProxyPropertyInvocation();
        viewModel.setVitaminB8(vitaminB8);
    }

    @Override
    public void setVitaminB9(BigDecimal vitaminB9) {
        validateProxyPropertyInvocation();
        viewModel.setVitaminB9(vitaminB9);
    }

    @Override
    public void setVitaminB12(BigDecimal vitaminB12) {
        validateProxyPropertyInvocation();
        viewModel.setVitaminB12(vitaminB12);
    }
    //endregion
}
