package com.iceflyer3.pfd.ui.model.proxy.recipe;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.enums.UnitOfMeasure;
import com.iceflyer3.pfd.data.factory.RecipeTaskFactory;
import com.iceflyer3.pfd.data.task.recipe.SaveRecipeTask;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.api.ServingSizeModel;
import com.iceflyer3.pfd.ui.model.api.food.ReadOnlyFoodModel;
import com.iceflyer3.pfd.ui.model.api.proxy.ProxyOperationResultCallback;
import com.iceflyer3.pfd.ui.model.api.recipe.RecipeModel;
import com.iceflyer3.pfd.ui.model.api.recipe.RecipeStepModel;
import com.iceflyer3.pfd.ui.model.proxy.ProxyOperation;
import com.iceflyer3.pfd.ui.model.proxy.food.ReadOnlyFoodModelProxy;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.recipe.RecipeViewModel;
import com.iceflyer3.pfd.validation.ValidationResult;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.core.task.TaskExecutor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

public class RecipeModelProxy extends AbstractRecipeModelProxy<RecipeViewModel> implements RecipeModel
{
    private final static Logger LOG = LoggerFactory.getLogger(RecipeModelProxy.class);

    private final UserViewModel createdUser;

    protected RecipeModelProxy(TaskExecutor taskExecutor,
                               RecipeTaskFactory recipeTaskFactory,
                               UserViewModel createdUser) {
        super(taskExecutor, recipeTaskFactory);
        this.createdUser = createdUser;
        viewModel = new RecipeViewModel(createdUser.getUsername());
    }

    protected RecipeModelProxy(TaskExecutor taskExecutor,
                               RecipeTaskFactory recipeTaskFactory,
                               UserViewModel createdUser,
                               RecipeViewModel viewModel) {
        super(taskExecutor, recipeTaskFactory);
        this.createdUser = createdUser;
        this.viewModel = viewModel;
    }

    @Override
    public void save(ProxyOperationResultCallback saveResultCallback)
    {
        validateProxyOperationInvocation(ProxyOperation.SAVE);
        SaveRecipeTask saveRecipeTask = recipeTaskFactory.saveRecipeAsync(createdUser, viewModel);
        saveRecipeTask.setOnSucceeded(event -> {
            viewModel = saveRecipeTask.getValue();
            saveResultCallback.receiveResult(true, "The recipe has been saved!");
        });
        saveRecipeTask.setOnFailed(event -> {
            LOG.error("An error has occurred while saving the recipe", saveRecipeTask.getException());
            saveResultCallback.receiveResult(false, "Failed to save the recipe. An error has occurred.");
        });
        taskExecutor.execute(saveRecipeTask);
    }

    @Override
    public UUID getId() {
        validateProxyPropertyInvocation();
        return viewModel.getId();
    }

    @Override
    public ValidationResult validate() {
        validateProxyPropertyInvocation();
        return viewModel.validate();
    }

    @Override
    public ValidationResult validateProperty(int propertyHashcode)
    {
        validateProxyPropertyInvocation();
        return viewModel.validateProperty(propertyHashcode);
    }

    @Override
    public String getName() {
        validateProxyPropertyInvocation();
        return viewModel.getName();
    }

    @Override
    public StringProperty nameProperty() {
        validateProxyPropertyInvocation();
        return viewModel.nameProperty();
    }

    @Override
    public void setName(String recipeName) {
        validateProxyPropertyInvocation();
        viewModel.setName(recipeName);
    }

    @Override
    public String getSource() {
        validateProxyPropertyInvocation();
        return viewModel.getSource();
    }

    @Override
    public StringProperty sourceProperty() {
        validateProxyPropertyInvocation();
        return viewModel.sourceProperty();
    }

    @Override
    public void setSource(String source) {
        validateProxyPropertyInvocation();
        viewModel.setSource(source);
    }

    @Override
    public BigDecimal getNumberOfServingsMade() {
        validateProxyPropertyInvocation();
        return viewModel.getNumberOfServingsMade();
    }

    @Override
    public ObjectProperty<BigDecimal> numberOfServingsMadeProperty() {
        validateProxyPropertyInvocation();
        return viewModel.numberOfServingsMadeProperty();
    }

    @Override
    public void setNumberOfServingsMade(BigDecimal numberOfServings) {
        validateProxyPropertyInvocation();
        viewModel.setNumberOfServingsMade(numberOfServings);
    }

    @Override
    public String getCreatedUser() {
        validateProxyPropertyInvocation();
        return viewModel.getCreatedUser();
    }

    @Override
    public StringProperty createdUserProperty() {
        validateProxyPropertyInvocation();
        return viewModel.createdUserProperty();
    }

    @Override
    public LocalDateTime getCreatedDate() {
        validateProxyPropertyInvocation();
        return viewModel.getCreatedDate();
    }

    @Override
    public ObjectProperty<LocalDateTime> createdDateProperty() {
        validateProxyPropertyInvocation();
        return viewModel.createdDateProperty();
    }

    @Override
    public String getLastModifiedUser() {
        validateProxyPropertyInvocation();
        return viewModel.getLastModifiedUser();
    }

    @Override
    public StringProperty lastModifiedUserProperty() {
        validateProxyPropertyInvocation();
        return viewModel.createdUserProperty();
    }

    @Override
    public void setLastModifiedUser(String lastModifiedUser) {
        validateProxyPropertyInvocation();
        viewModel.setLastModifiedUser(lastModifiedUser);
    }

    @Override
    public LocalDateTime getLastModifiedDate() {
        validateProxyPropertyInvocation();
        return viewModel.getLastModifiedDate();
    }

    @Override
    public ObjectProperty<LocalDateTime> lastModifiedDateProperty() {
        validateProxyPropertyInvocation();
        return viewModel.lastModifiedDateProperty();
    }

    @Override
    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        validateProxyPropertyInvocation();
        viewModel.setLastModifiedDate(lastModifiedDate);
    }

    @Override
    public ObservableList<IngredientModel> getIngredients() {
        validateProxyPropertyInvocation();
        return viewModel.getIngredients();
    }


    /**
     * Add a new ingredient to the list of ingredients. The provided ingredient should be of the proxy
     * implementation type which is ReadOnlyFoodModelProxy.
     *
     * @param ingredient The ReadOnlyFoodModelProxy instance that represents the ingredient food
     */
    @Override
    public void addIngredient(ReadOnlyFoodModel ingredient) {
        validateProxyPropertyInvocation();

        if (ingredient instanceof ReadOnlyFoodModelProxy proxy)
        {
            viewModel.addIngredient(proxy.getViewModel());
        }
        else
        {
            throw new IllegalArgumentException("ingredient must be an instance of ReadOnlyFoodModelProxy");
        }
    }

    @Override
    public void removeIngredient(IngredientModel ingredient)
    {
        validateProxyPropertyInvocation();
        viewModel.removeIngredient(ingredient);
    }

    @Override
    public ObservableList<ServingSizeModel> getServingSizes() {
        validateProxyPropertyInvocation();
        return viewModel.getServingSizes();
    }

    @Override
    public void addServingSize()
    {
        validateProxyPropertyInvocation();
        viewModel.addServingSize();
    }

    @Override
    public void addServingSize(UnitOfMeasure uom, BigDecimal quantity)
    {
        validateProxyPropertyInvocation();
        viewModel.addServingSize(uom, quantity);
    }

    @Override
    public void removeServingSize(ServingSizeModel servingSize)
    {
        validateProxyPropertyInvocation();
        viewModel.removeServingSize(servingSize);
    }

    @Override
    public ObservableList<RecipeStepModel> getSteps() {
        validateProxyPropertyInvocation();
        return viewModel.getSteps();
    }

    @Override
    public ListProperty<RecipeStepModel> stepsProperty() {
        validateProxyPropertyInvocation();
        return viewModel.stepsProperty();
    }

    @Override
    public String toString() {
        validateProxyPropertyInvocation();
        return viewModel.toString();
    }

    @Override
    public void refreshNutritionDetails()
    {
        validateProxyPropertyInvocation();
        viewModel.refreshNutritionDetails();
    }
}
