package com.iceflyer3.pfd.ui.model.proxy.recipe;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.factory.RecipeTaskFactory;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.api.ServingSizeModel;
import com.iceflyer3.pfd.ui.model.api.proxy.ProxyOperationResultCallback;
import com.iceflyer3.pfd.ui.model.api.recipe.ReadOnlyRecipeModel;
import com.iceflyer3.pfd.ui.model.api.recipe.RecipeStepModel;
import com.iceflyer3.pfd.ui.model.viewmodel.recipe.RecipeViewModel;
import javafx.beans.property.ReadOnlyListProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.springframework.core.task.TaskExecutor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Proxy implementation that is read-only and sits on top of the RecipeViewModel.
 *
 * Unlike other proxies, this does not support saving. It only supports the delete operation.
 */
public class ReadOnlyRecipeModelProxy extends AbstractRecipeModelProxy<RecipeViewModel> implements ReadOnlyRecipeModel
{

    protected ReadOnlyRecipeModelProxy(
            TaskExecutor taskExecutor,
            RecipeTaskFactory recipeTaskFactory,
            RecipeViewModel viewModel) {
        super(taskExecutor, recipeTaskFactory);
        this.viewModel = viewModel;
    }

    /**
     * @deprecated
     * This proxy is read-only and thus does not support saving.
     * Invoking this function will cause an UnsupportedOperationException to be thrown.
     */
    @Deprecated
    @Override
    public void save(ProxyOperationResultCallback saveResultCallback) {
        throw new UnsupportedOperationException("This proxy is read-only and thus does not support saving.");
    }


    @Override
    public UUID getId() {
        validateProxyPropertyInvocation();
        return viewModel.getId();
    }

    @Override
    public String getName() {
        validateProxyPropertyInvocation();
        return viewModel.getName();
    }

    @Override
    public ReadOnlyStringProperty nameProperty() {
        validateProxyPropertyInvocation();
        return viewModel.nameProperty();
    }

    @Override
    public String getSource() {
        validateProxyPropertyInvocation();
        return viewModel.getSource();
    }

    @Override
    public ReadOnlyStringProperty sourceProperty() {
        validateProxyPropertyInvocation();
        return viewModel.sourceProperty();
    }

    @Override
    public BigDecimal getNumberOfServingsMade() {
        validateProxyPropertyInvocation();
        return viewModel.getNumberOfServingsMade();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> numberOfServingsMadeProperty() {
        validateProxyPropertyInvocation();
        return viewModel.numberOfServingsMadeProperty();
    }

    @Override
    public String getCreatedUser() {
        validateProxyPropertyInvocation();
        return viewModel.getLastModifiedUser();
    }

    @Override
    public ReadOnlyStringProperty createdUserProperty() {
        validateProxyPropertyInvocation();
        return viewModel.createdUserProperty();
    }

    @Override
    public LocalDateTime getCreatedDate() {
        validateProxyPropertyInvocation();
        return viewModel.getCreatedDate();
    }

    @Override
    public ReadOnlyObjectProperty<LocalDateTime> createdDateProperty() {
        validateProxyPropertyInvocation();
        return viewModel.createdDateProperty();
    }

    @Override
    public String getLastModifiedUser() {
        validateProxyPropertyInvocation();
        return viewModel.getLastModifiedUser();
    }

    @Override
    public ReadOnlyStringProperty lastModifiedUserProperty() {
        validateProxyPropertyInvocation();
        return viewModel.lastModifiedUserProperty();
    }

    @Override
    public LocalDateTime getLastModifiedDate() {
        validateProxyPropertyInvocation();
        return viewModel.getLastModifiedDate();
    }

    @Override
    public ReadOnlyObjectProperty<LocalDateTime> lastModifiedDateProperty() {
        validateProxyPropertyInvocation();
        return viewModel.lastModifiedDateProperty();
    }

    @Override
    public ObservableList<IngredientModel> getIngredients() {
        validateProxyPropertyInvocation();
        return FXCollections.unmodifiableObservableList(viewModel.getIngredients());
    }

    @Override
    public ObservableList<ServingSizeModel> getServingSizes() {
        validateProxyPropertyInvocation();
        return FXCollections.unmodifiableObservableList(viewModel.getServingSizes());
    }

    @Override
    public ObservableList<RecipeStepModel> getSteps() {
        validateProxyPropertyInvocation();
        return FXCollections.unmodifiableObservableList(viewModel.getSteps());
    }

    @Override
    public ReadOnlyListProperty<RecipeStepModel> stepsProperty() {
        validateProxyPropertyInvocation();
        return viewModel.stepsProperty();
    }

    @Override
    public void refreshNutritionDetails()
    {
        validateProxyPropertyInvocation();
        viewModel.refreshNutritionDetails();
    }
}
