package com.iceflyer3.pfd.ui.model.proxy.food;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.enums.UnitOfMeasure;
import com.iceflyer3.pfd.data.factory.FoodTaskFactory;
import com.iceflyer3.pfd.data.task.food.GetComplexFoodIngredientsTask;
import com.iceflyer3.pfd.data.task.food.SaveComplexFoodTask;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.api.ServingSizeModel;
import com.iceflyer3.pfd.ui.model.api.food.ReadOnlyFoodModel;
import com.iceflyer3.pfd.ui.model.api.proxy.IngredientModelProxy;
import com.iceflyer3.pfd.ui.model.api.proxy.ProxyOperationResultCallback;
import com.iceflyer3.pfd.ui.model.api.food.ComplexFoodModel;
import com.iceflyer3.pfd.ui.model.proxy.ProxyOperation;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.ComplexFoodViewModel;
import com.iceflyer3.pfd.validation.ValidationResult;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.core.task.TaskExecutor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

public class ComplexFoodModelProxy
        extends AbstractFoodModelProxy<ComplexFoodViewModel>
        implements ComplexFoodModel, IngredientModelProxy
{

    private final static Logger LOG = LoggerFactory.getLogger(ComplexFoodModelProxy.class);

    private final UserViewModel createdUser;

    protected ComplexFoodModelProxy(TaskExecutor taskExecutor, FoodTaskFactory foodTaskFactory, UserViewModel createdUser) {
        super(taskExecutor, foodTaskFactory);
        this.createdUser = createdUser;
        this.viewModel = new ComplexFoodViewModel(createdUser.getUsername());
    }

    protected ComplexFoodModelProxy(TaskExecutor taskExecutor, FoodTaskFactory foodTaskFactory, UserViewModel createdUser, ComplexFoodViewModel viewModel) {
        super(taskExecutor, foodTaskFactory);
        this.createdUser = createdUser;
        this.viewModel = viewModel;
    }

    @Override
    public void save(ProxyOperationResultCallback saveResultCallback) {
        validateProxyOperationInvocation(ProxyOperation.SAVE);
        SaveComplexFoodTask saveComplexFoodTask = foodTaskFactory.addComplexFoodAsync(createdUser, viewModel);
        saveComplexFoodTask.setOnSucceeded(event -> {
            this.viewModel = saveComplexFoodTask.getValue();
            saveResultCallback.receiveResult(true, "The complex food has been saved!");
        });
        saveComplexFoodTask.setOnFailed(event -> {
            LOG.debug("An error has occurred while saving a new complex food", saveComplexFoodTask.getException());
            saveResultCallback.receiveResult(false, "Failed to save the complex food. An error has occurred.");
        });
        taskExecutor.execute(saveComplexFoodTask);
    }

    @Override
    public ValidationResult validate() {
        validateProxyPropertyInvocation();
        return viewModel.validate();
    }

    @Override
    public ValidationResult validateProperty(int propertyHashcode)
    {
        validateProxyPropertyInvocation();
        return viewModel.validateProperty(propertyHashcode);
    }

    @Override
    public UUID getId() {
        validateProxyPropertyInvocation();
        return viewModel.getId();
    }

    @Override
    public String getName() {
        validateProxyPropertyInvocation();
        return viewModel.getName();
    }

    @Override
    public StringProperty nameProperty() {
        validateProxyPropertyInvocation();
        return viewModel.nameProperty();
    }

    @Override
    public void setName(String foodName) {
        validateProxyPropertyInvocation();
        viewModel.setName(foodName);
    }

    @Override
    public String getCreatedUser() {
        validateProxyPropertyInvocation();
        return viewModel.getCreatedUser();
    }

    @Override
    public StringProperty createdUserProperty() {
        validateProxyPropertyInvocation();
        return viewModel.createdUserProperty();
    }

    @Override
    public LocalDateTime getCreatedDate() {
        validateProxyPropertyInvocation();
        return viewModel.getCreatedDate();
    }

    @Override
    public ObjectProperty<LocalDateTime> createdDateProperty() {
        validateProxyPropertyInvocation();
        return viewModel.createdDateProperty();
    }

    @Override
    public String getLastModifiedUser() {
        validateProxyPropertyInvocation();
        return viewModel.getLastModifiedUser();
    }

    @Override
    public StringProperty lastModifiedUserProperty() {
        validateProxyPropertyInvocation();
        return viewModel.lastModifiedUserProperty();
    }

    @Override
    public void setLastModifiedUser(String lastModifiedUserName) {
        validateProxyPropertyInvocation();
        viewModel.setLastModifiedUser(lastModifiedUserName);
    }

    @Override
    public LocalDateTime getLastModifiedDate() {
        validateProxyPropertyInvocation();
        return viewModel.getLastModifiedDate();
    }

    @Override
    public ObjectProperty<LocalDateTime> lastModifiedDateProperty() {
        validateProxyPropertyInvocation();
        return viewModel.lastModifiedDateProperty();
    }

    @Override
    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        validateProxyPropertyInvocation();
        viewModel.setLastModifiedDate(lastModifiedDate);
    }

    @Override
    public ObservableList<ServingSizeModel> getServingSizes() {
        validateProxyPropertyInvocation();
        return viewModel.getServingSizes();
    }

    @Override
    public void addServingSize()
    {
        validateProxyPropertyInvocation();
        viewModel.addServingSize();
    }

    @Override
    public void addServingSize(UnitOfMeasure uom, BigDecimal quantity)
    {
        validateProxyPropertyInvocation();
        viewModel.addServingSize(uom, quantity);
    }

    @Override
    public void removeServingSize(ServingSizeModel servingSize)
    {
        validateProxyPropertyInvocation();
        viewModel.removeServingSize(servingSize);
    }

    @Override
    public String toString() {
        validateProxyPropertyInvocation();
        return viewModel.toString();
    }

    // Ingredient Detail Reporter implementation
    @Override
    public void refreshNutritionDetails()
    {
        validateProxyPropertyInvocation();
        viewModel.refreshNutritionDetails();
    }

    @Override
    public ObservableList<IngredientModel> getIngredients() {
        validateProxyPropertyInvocation();
        return viewModel.getIngredients();
    }


    /**
     * Add a new ingredient to the list of ingredients. The provided ingredient should be of the proxy
     * implementation type which is ReadOnlyFoodModelProxy.
     *
     * @param ingredientFood The ReadOnlyFoodModelProxy instance that represents the ingredient food
     */
    @Override
    public void addIngredient(ReadOnlyFoodModel ingredientFood)
    {
        validateProxyPropertyInvocation();
        if (ingredientFood instanceof ReadOnlyFoodModelProxy ingredient)
        {
            validateProxyPropertyInvocation();
            viewModel.addIngredient(ingredient.getViewModel());
        }
        else
        {
            throw new IllegalArgumentException("ingredientFood must be an instance of ReadOnlyFoodModelProxy");
        }
    }

    @Override
    public void removeIngredient(IngredientModel ingredient)
    {
        validateProxyPropertyInvocation();
        viewModel.removeIngredient(ingredient);
    }


    /**
     * Complex foods do not support saving ingredients independent of the food itself.
     * In order to save the ingredients for a complex food use the save() function instead.
     *
     * @param saveResultCallback result callback
     */
    @Override
    @Deprecated
    public void saveIngredients(ProxyOperationResultCallback saveResultCallback)
    {
        throw new UnsupportedOperationException("To save the ingredients for a complex food call save() instead");
    }

    @Override
    public void loadIngredients(ProxyOperationResultCallback loadCallback)
    {
        validateProxyOperationInvocation(ProxyOperation.LOAD);

        GetComplexFoodIngredientsTask getIngredientsTask = foodTaskFactory.getComplexFoodIngredientsAsync(viewModel.getId());
        getIngredientsTask.setOnSucceeded(event -> {
            viewModel.setIngredients(getIngredientsTask.getValue());
            viewModel.refreshNutritionDetails();
            loadCallback.receiveResult(true, "Complex food ingredients loaded successfully!");
        });
        getIngredientsTask.setOnFailed(event -> {
            LOG.error("An error has occurred while loading the ingredients for complex food {}", viewModel.getId(), getIngredientsTask.getException());
            loadCallback.receiveResult(false, "Failed to load the complex food. An error has occurred while loading the ingredients.");
        });
        taskExecutor.execute(getIngredientsTask);
    }
}
