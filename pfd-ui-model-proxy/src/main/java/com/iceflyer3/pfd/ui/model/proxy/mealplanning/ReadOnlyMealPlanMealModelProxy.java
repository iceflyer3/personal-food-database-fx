/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.model.proxy.mealplanning;

import com.iceflyer3.pfd.data.factory.MealPlanningTaskFactory;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.api.mealplanning.ReadOnlyMealPlanMealModel;
import com.iceflyer3.pfd.ui.model.api.proxy.ProxyOperationResultCallback;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanMealViewModel;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.springframework.core.task.TaskExecutor;

import java.time.LocalDateTime;
import java.util.UUID;

public class ReadOnlyMealPlanMealModelProxy extends AbstractMealPlanningMealModelProxy<MealPlanMealViewModel> implements ReadOnlyMealPlanMealModel
{
    public ReadOnlyMealPlanMealModelProxy(TaskExecutor taskExecutor, MealPlanningTaskFactory mealPlanningTaskFactory, MealPlanMealViewModel viewModel)
    {
        super(taskExecutor, mealPlanningTaskFactory);
        this.viewModel = viewModel;
    }

    @Override
    public UUID getId()
    {
        validateProxyPropertyInvocation();
        return viewModel.getId();
    }

    @Override
    public String getName()
    {
        validateProxyPropertyInvocation();
        return viewModel.getName();
    }

    @Override
    public ReadOnlyStringProperty nameProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.nameProperty();
    }

    @Override
    public LocalDateTime getLastModifiedDate()
    {
        validateProxyPropertyInvocation();
        return viewModel.getLastModifiedDate();
    }

    @Override
    public ReadOnlyObjectProperty<LocalDateTime> lastModifiedDateProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.lastModifiedDateProperty();
    }

    @Override
    public ObservableList<IngredientModel> getIngredients()
    {
        validateProxyPropertyInvocation();
        return FXCollections.unmodifiableObservableList(viewModel.getIngredients());
    }

    /**
     * @deprecated
     * This proxy is read-only and thus does not support saving.
     * Invoking this function will cause an UnsupportedOperationException to be thrown.
     */
    @Deprecated
    @Override
    public void save(ProxyOperationResultCallback saveResultCallback)
    {
        throw new UnsupportedOperationException("This proxy is read-only and thus does not support saving.");
    }

    @Override
    public void refreshNutritionDetails()
    {
        validateProxyPropertyInvocation();
        viewModel.refreshNutritionDetails();
    }
}
