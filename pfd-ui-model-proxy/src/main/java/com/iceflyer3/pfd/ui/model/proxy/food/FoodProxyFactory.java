package com.iceflyer3.pfd.ui.model.proxy.food;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.data.factory.FoodTaskFactory;
import com.iceflyer3.pfd.data.task.food.*;
import com.iceflyer3.pfd.ui.model.api.proxy.ProxyDataResultCallback;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.ComplexFoodViewModel;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class FoodProxyFactory {

    private static final Logger LOG = LoggerFactory.getLogger(FoodProxyFactory.class);

    private final TaskExecutor taskExecutor;
    private final FoodTaskFactory foodTaskFactory;

    public FoodProxyFactory(TaskExecutor taskExecutor, FoodTaskFactory foodTaskFactory) {
        this.taskExecutor = taskExecutor;
        this.foodTaskFactory = foodTaskFactory;
    }

    public void getAllFoods(ProxyDataResultCallback<List<ReadOnlyFoodModelProxy>> callback) {
        GetAllFoodsTask getAllFoodsTask = foodTaskFactory.getAllFoods();
        getAllFoodsTask.setOnSucceeded(event -> {
            List<ReadOnlyFoodModelProxy> proxies = getAllFoodsTask
                                                        .getValue()
                                                        .stream()
                                                        .map(foodViewModel -> new ReadOnlyFoodModelProxy(taskExecutor, foodTaskFactory, foodViewModel))
                                                        .collect(Collectors.toList());
            callback.receiveResult(true, proxies);
        });
        getAllFoodsTask.setOnFailed(event -> {
            LOG.error("An error has occurred while retrieving all foods", getAllFoodsTask.getException());
            callback.receiveResult(false, null);
        });
        taskExecutor.execute(getAllFoodsTask);
    }

    /**
     * Retrieve the FoodModelProxy for a specified food.
     *
     * Unlike with some other proxy operations the FoodId passed here may
     * not be null.
     *
     * The proxy returned via the callback is read-only and only provides
     * the ability to delete the specified food. Saving the food is not
     * supported.
     *
     * @param foodId The food to retrieve the proxy for
     * @param callback Result callback
     */
    public void getFood(UUID foodId, ProxyDataResultCallback<ReadOnlyFoodModelProxy> callback)
    {
        if (foodId == null)
        {
            throw new IllegalArgumentException("FoodId was null. The caller must provide the id of the food to retrieve.");
        }
        else
        {
            GetReadOnlyFoodByIdTask getFoodByIdTask = foodTaskFactory.getFoodByIdAsync(foodId);
            getFoodByIdTask.setOnSucceeded(event -> {
                ReadOnlyFoodModelProxy proxy = new ReadOnlyFoodModelProxy(taskExecutor, foodTaskFactory, getFoodByIdTask.getValue());
                callback.receiveResult(true, proxy);
            });
            getFoodByIdTask.setOnFailed(event -> {
                LOG.error("An error has occurred while trying to retrieve a food", getFoodByIdTask.getException());
                callback.receiveResult(false, null);
            });
            taskExecutor.execute(getFoodByIdTask);
        }
    }

    /**
     * Retrieve the SimpleFoodModelProxy for specified food.
     *
     * If no food is found for the specified food id a proxy that wraps
     * a new instance of SimpleFoodViewModel will be returned.
     *
     * @param foodId The id of the food to retrieve
     * @param createdUser The user who created the food
     * @param callback The callback to receive the result of this operation
     */
    public void getSimpleFood(UUID foodId, UserViewModel createdUser, ProxyDataResultCallback<SimpleFoodModelProxy> callback)
    {
        if (foodId == null)
        {
            SimpleFoodModelProxy proxy = new SimpleFoodModelProxy(taskExecutor, foodTaskFactory, createdUser);
            callback.receiveResult(true, proxy);
        }
        else
        {
            GetSimpleFoodByIdTask getSimpleFoodByIdTask = foodTaskFactory.getSimpleFoodByIdAsync(foodId);
            getSimpleFoodByIdTask.setOnSucceeded(event -> {
                SimpleFoodModelProxy proxy = new SimpleFoodModelProxy(taskExecutor, foodTaskFactory, createdUser, getSimpleFoodByIdTask.getValue());
                callback.receiveResult(true, proxy);
            });
            getSimpleFoodByIdTask.setOnFailed(event -> {
                LOG.error("An error has occurred while trying to retrieve a simple food", getSimpleFoodByIdTask.getException());
                callback.receiveResult(false, null);
            });
            taskExecutor.execute(getSimpleFoodByIdTask);
        }
    }

    public void getComplexFood(UUID foodId, UserViewModel createdUser, ProxyDataResultCallback<ComplexFoodModelProxy> callback)
    {
        if (foodId == null)
        {
            ComplexFoodModelProxy proxy = new ComplexFoodModelProxy(taskExecutor, foodTaskFactory, createdUser);
            callback.receiveResult(true, proxy);
        }
        else
        {
            GetComplexFoodByIdTask getComplexFoodByIdTask = foodTaskFactory.getComplexFoodByIdAsync(foodId);
            getComplexFoodByIdTask.setOnSucceeded(event -> {
                ComplexFoodModelProxy proxy = new ComplexFoodModelProxy(taskExecutor, foodTaskFactory, createdUser, getComplexFoodByIdTask.getValue());
                callback.receiveResult(true, proxy);
            });
            getComplexFoodByIdTask.setOnFailed(event -> {
                LOG.error("An error has occurred while trying to retrieve a complex food", getComplexFoodByIdTask.getException());
                callback.receiveResult(false, null);
            });
            taskExecutor.execute(getComplexFoodByIdTask);
        }
    }

    /*
     * TODO: I'd like to find a way to untether these functions from requiring the UserViewModel
     *       representing the user who created the foods as well.
     *
     *       It doesn't really feel like, if the goal is to either retrieve a specific food or
     *       search for foods meeting some criteria that you should really have to care about the
     *       user who created the food (unless that is part of the search criteria)
     *
     *       That's likely to a bigger change than I'm trying to make at the moment so we'll have to
     *       revisit it in the future and just be okay with it for the current moment.
     */
    public void findComplexFoodsWithName(String name, UserViewModel createdUser, ProxyDataResultCallback<List<ComplexFoodModelProxy>> callback)
    {
        FindComplexFoodsWithNameTask searchTask = foodTaskFactory.findComplexFoodsWithNameAsync(name);
        searchTask.setOnSucceeded(event -> {
            List<ComplexFoodModelProxy> proxies = searchTask.getValue().stream().map(viewModel -> new ComplexFoodModelProxy(taskExecutor, foodTaskFactory, createdUser, viewModel)).toList();
            callback.receiveResult(true, proxies);
        });
        searchTask.setOnFailed(event -> {
            LOG.error(String.format("An error has occurred while trying to search for complex foods with the name %s", name), searchTask.getException());
            callback.receiveResult(false, null);
        });
        taskExecutor.execute(searchTask);
    }

    /**
     * Get a ComplexFoodModelProxy that wraps the SimpleFoodViewModel.
     *
     * @param viewModel The view model the proxy will wrap
     * @param createdUser The user creating the food
     * @param callback The callback to receive the result of this operation
     */
    public void getComplexFoodFromModel(ComplexFoodViewModel viewModel, UserViewModel createdUser, ProxyDataResultCallback<ComplexFoodModelProxy> callback)
    {
        ComplexFoodModelProxy proxy = new ComplexFoodModelProxy(taskExecutor, foodTaskFactory, createdUser, viewModel);
        callback.receiveResult(true, proxy);
    }
}
