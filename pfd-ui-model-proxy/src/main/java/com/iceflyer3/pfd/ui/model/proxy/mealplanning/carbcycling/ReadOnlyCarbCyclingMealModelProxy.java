/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.model.proxy.mealplanning.carbcycling;

import com.iceflyer3.pfd.data.factory.MealPlanningTaskFactory;
import com.iceflyer3.pfd.enums.MealType;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.api.mealplanning.carbcycling.ReadOnlyCarbCyclingMealModel;
import com.iceflyer3.pfd.ui.model.api.proxy.ProxyOperationResultCallback;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.AbstractMealPlanningMealModelProxy;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingMealViewModel;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.springframework.core.task.TaskExecutor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

public class ReadOnlyCarbCyclingMealModelProxy extends AbstractMealPlanningMealModelProxy<CarbCyclingMealViewModel> implements ReadOnlyCarbCyclingMealModel
{
    public ReadOnlyCarbCyclingMealModelProxy(TaskExecutor taskExecutor, MealPlanningTaskFactory mealPlanningTaskFactory, CarbCyclingMealViewModel viewModel)
    {
        super(taskExecutor, mealPlanningTaskFactory);
        this.viewModel = viewModel;
    }

    @Override
    public UUID getId()
    {
        validateProxyPropertyInvocation();
        return viewModel.getId();
    }

    @Override
    public String getName()
    {
        validateProxyPropertyInvocation();
        return viewModel.getName();
    }

    @Override
    public ReadOnlyStringProperty nameProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.nameProperty();
    }

    @Override
    public LocalDateTime getLastModifiedDate()
    {
        validateProxyPropertyInvocation();
        return viewModel.getLastModifiedDate();
    }

    @Override
    public ReadOnlyObjectProperty<LocalDateTime> lastModifiedDateProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.lastModifiedDateProperty();
    }

    @Override
    public MealType getMealType()
    {
        validateProxyPropertyInvocation();
        return viewModel.getMealType();
    }

    @Override
    public ReadOnlyObjectProperty<MealType> mealTypeProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.mealTypeProperty();
    }

    @Override
    public int getMealNumber()
    {
        validateProxyPropertyInvocation();
        return viewModel.getMealNumber();
    }

    @Override
    public ReadOnlyIntegerProperty mealNumberProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.mealNumberProperty();
    }

    @Override
    public BigDecimal getCarbConsumptionPercentage()
    {
        validateProxyPropertyInvocation();
        return viewModel.getCarbConsumptionPercentage();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> carbConsumptionPercentageProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.carbConsumptionPercentageProperty();
    }

    @Override
    public BigDecimal getRequiredCalories()
    {
        validateProxyPropertyInvocation();
        return viewModel.getRequiredCalories();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> requiredCaloriesProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.requiredCaloriesProperty();
    }

    @Override
    public BigDecimal getRequiredProtein()
    {
        validateProxyPropertyInvocation();
        return viewModel.getRequiredProtein();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> requiredProteinProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.requiredProteinProperty();
    }

    @Override
    public BigDecimal getRequiredCarbs()
    {
        validateProxyPropertyInvocation();
        return viewModel.getRequiredCarbs();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> requiredCarbsProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.requiredCarbsProperty();
    }

    @Override
    public BigDecimal getRequiredFats()
    {
        validateProxyPropertyInvocation();
        return viewModel.getRequiredFats();
    }

    @Override
    public ReadOnlyObjectProperty<BigDecimal> requiredFatsProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.requiredFatsProperty();
    }

    @Override
    public ObservableList<IngredientModel> getIngredients()
    {
        validateProxyPropertyInvocation();
        return FXCollections.unmodifiableObservableList(viewModel.getIngredients());
    }

    /**
     * @deprecated
     * This proxy is read-only and thus does not support saving.
     * Invoking this function will cause an UnsupportedOperationException to be thrown.
     */
    @Deprecated
    @Override
    public void save(ProxyOperationResultCallback saveResultCallback)
    {
        throw new UnsupportedOperationException("This proxy is read-only and thus does not support saving.");
    }

    @Override
    public void refreshNutritionDetails()
    {
        validateProxyPropertyInvocation();
        viewModel.refreshNutritionDetails();
    }
}
