package com.iceflyer3.pfd.ui.model.proxy.base;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.ui.model.proxy.*;
import com.iceflyer3.pfd.ui.model.api.proxy.ModelProxy;
import com.iceflyer3.pfd.ui.model.api.proxy.ProxyOperationResultCallback;
import org.springframework.core.task.TaskExecutor;

import java.util.UUID;

/**
 * Base class for all model proxy implementations.
 *
 * A proxy may no longer be used after a call to delete() has been successful.
 * Any further attempts to use the proxy will throw exceptions.
 *
 * @param <ModelType> The type of model that this proxy is for
 */
public abstract class AbstractModelProxy<ModelType> implements ModelProxy {

    protected final TaskExecutor taskExecutor;

    // Used only internally / no client access so this doesn't need to be a property
    protected boolean isDeleted;

    protected ModelType viewModel;

    protected AbstractModelProxy(TaskExecutor taskExecutor) {
        this.taskExecutor = taskExecutor;
        this.isDeleted = false;
    }

    /**
     * This is public so that we may make use of packages for organization.
     * This function is intended only for use with packages in the proxy
     * package and sub-packages.
     *
     * Unfortunately, Java doesn't have a way to express "allow access to
     * this function for this package and all sub-packages".
     *
     * @return The view model that this proxy sits on top of
     */
    public ModelType getViewModel()
    {
        return viewModel;
    }

    @Override
    public abstract void save(ProxyOperationResultCallback saveResultCallback);

    @Override
    public abstract void delete(ProxyOperationResultCallback deleteResultCallback);

    // Proxies should already implement this method from the appropriate Model interface anyways
    public abstract UUID getId();

    /**
     * Convenience method used by sub-classes to verify that property access requested by
     * the client is valid.
     *
     * The default implementation only ensures that the view model the proxy encapsulates
     * has been loaded and that the proxy has not previously been deleted.
     *
     * If either of these conditions are true a ProxyPropertyInvocationException is thrown.
     */
    protected void validateProxyPropertyInvocation()
    {
        if (viewModel == null)
        {
            if (isDeleted)
            {
                throw new ProxyPropertyInvocationException("Could not access property of the proxy because it has already been deleted.");
            }
        }
    }

    /**
     * Convenience method used by sub-classes to verify that an operation requested by the client
     * is valid.
     *
     * The default implementation ensures that a call to delete() has not previously been successful
     * as well as that we aren't trying to delete a proxy that has not yet saved the ViewModel.
     *
     * If this either of these conditions are true a ProxyOperationInvocationException is thrown.
     *
     * @param operation The operation to validate the invocation of
     */
    protected void validateProxyOperationInvocation(ProxyOperation operation)
    {
        // In theory this should never happen. But we cover it anyways just to be sure.
        if (operation == ProxyOperation.DELETE || operation == ProxyOperation.LOAD)
        {
            if (getId() == null)
            {
                switch (operation)
                {
                    case DELETE -> throw new ProxyOperationInvocationException("Load operation is not valid on a proxy that has not yet been saved.");
                    case LOAD -> throw new ProxyOperationInvocationException("Delete operation is not valid on a proxy that has not yet been saved.");
                }
            }
        }

        if (isDeleted)
        {
            switch (operation)
            {
                case SAVE -> throw new ProxyOperationInvocationException("Can not save a proxy which has previously been deleted. Save failed.");
                case DELETE -> throw new ProxyOperationInvocationException("Can not delete a proxy which has previously been deleted. Delete failed.");
                case LOAD -> throw new ProxyOperationInvocationException("Can not load a proxy which has previously been deleted. Load failed.");
            }
        }
    }
}
