package com.iceflyer3.pfd.ui.model.proxy.food;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.enums.FoodType;
import com.iceflyer3.pfd.data.factory.FoodTaskFactory;
import com.iceflyer3.pfd.ui.model.api.ServingSizeModel;
import com.iceflyer3.pfd.ui.model.api.proxy.ProxyOperationResultCallback;
import com.iceflyer3.pfd.ui.model.api.food.ReadOnlyFoodModel;
import com.iceflyer3.pfd.ui.model.proxy.ProxyOperationInvocationException;
import com.iceflyer3.pfd.ui.model.viewmodel.food.ReadOnlyFoodViewModel;
import javafx.beans.property.*;
import javafx.collections.ObservableList;
import org.springframework.core.task.TaskExecutor;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Proxy implementation that sits on top of the read-only ReadOnlyFoodViewModel.
 *
 * Unlike other proxies, this does not support saving. It only supports the delete operation.
 *
 * Also, as this is read-only and not used for collecting data input there are no validations
 * that need performed by this view model.
 */
public class ReadOnlyFoodModelProxy extends AbstractFoodModelProxy<ReadOnlyFoodViewModel> implements ReadOnlyFoodModel
{

    protected ReadOnlyFoodModelProxy(TaskExecutor taskExecutor, FoodTaskFactory foodTaskFactory, ReadOnlyFoodViewModel food) {
        super(taskExecutor, foodTaskFactory);
        viewModel = food;
    }

    /**
     * @deprecated
     * This proxy is read-only and thus does not support saving.
     */
    @Deprecated
    @Override
    public void save(ProxyOperationResultCallback saveResultCallback) {
        throw new ProxyOperationInvocationException("This proxy is read-only and does not support saving");
    }

    @Override
    public UUID getId() {
        validateProxyPropertyInvocation();
        return viewModel.getId();
    }

    @Override
    public FoodType getFoodType() {
        validateProxyPropertyInvocation();
        return viewModel.getFoodType();
    }
    @Override
    public ReadOnlyObjectProperty<FoodType> foodTypeProperty() {
        validateProxyPropertyInvocation();
        return viewModel.foodTypeProperty();
    }

    @Override
    public String getName() {
        validateProxyPropertyInvocation();
        return viewModel.getName();
    }
    @Override
    public ReadOnlyStringProperty nameProperty() {
        validateProxyPropertyInvocation();
        return viewModel.nameProperty();
    }

    @Override
    public String getBrand() {
        validateProxyPropertyInvocation();
        return viewModel.getBrand();
    }
    @Override
    public ReadOnlyStringProperty brandProperty() {
        validateProxyPropertyInvocation();
        return viewModel.brandProperty();
    }

    @Override
    public String getSource() {
        validateProxyPropertyInvocation();
        return viewModel.getSource();
    }
    @Override
    public ReadOnlyStringProperty sourceProperty() {
        validateProxyPropertyInvocation();
        return viewModel.sourceProperty();
    }

    @Override
    public int getGlycemicIndex() {
        validateProxyPropertyInvocation();
        return viewModel.getGlycemicIndex();
    }
    @Override
    public ReadOnlyIntegerProperty glycemicIndexProperty() {
        validateProxyPropertyInvocation();
        return viewModel.glycemicIndexProperty();
    }

    @Override
    public ObservableList<ServingSizeModel> getServingSizes() {
        validateProxyPropertyInvocation();
        return viewModel.getServingSizes();
    }

    @Override
    public String getCreatedUser() {
        validateProxyPropertyInvocation();
        return viewModel.getCreatedUser();
    }
    @Override
    public ReadOnlyStringProperty createdUserProperty() {
        validateProxyPropertyInvocation();
        return viewModel.createdUserProperty();
    }

    @Override
    public LocalDateTime getCreatedDate() {
        validateProxyPropertyInvocation();
        return viewModel.getCreatedDate();
    }
    @Override
    public ReadOnlyObjectProperty<LocalDateTime> createdDateProperty() {
        validateProxyPropertyInvocation();
        return viewModel.createdDateProperty();
    }

    @Override
    public String getLastModifiedUser() {
        validateProxyPropertyInvocation();
        return viewModel.getLastModifiedUser();
    }
    @Override
    public ReadOnlyStringProperty lastModifiedUserProperty() {
        validateProxyPropertyInvocation();
        return viewModel.lastModifiedUserProperty();
    }

    @Override
    public LocalDateTime getLastModifiedDate() {
        validateProxyPropertyInvocation();
        return viewModel.getLastModifiedDate();
    }
    @Override
    public ReadOnlyObjectProperty<LocalDateTime> lastModifiedDateProperty() {
        validateProxyPropertyInvocation();
        return viewModel.lastModifiedDateProperty();
    }

    @Override
    public String toString() {
        validateProxyPropertyInvocation();
        // This is needed by ControlsFx autocomplete functionality.
        return viewModel.toString();
    }
}
