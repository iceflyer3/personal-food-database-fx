package com.iceflyer3.pfd.ui.model.proxy.mealplanning.carbcycling;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.enums.MealType;
import com.iceflyer3.pfd.data.factory.MealPlanningTaskFactory;
import com.iceflyer3.pfd.data.task.mealplanning.carbcycling.meal.SaveCarbCyclingMealTask;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.api.food.ReadOnlyFoodModel;
import com.iceflyer3.pfd.ui.model.api.proxy.ProxyOperationResultCallback;
import com.iceflyer3.pfd.ui.model.api.mealplanning.carbcycling.CarbCyclingMealModel;
import com.iceflyer3.pfd.ui.model.proxy.ProxyOperation;
import com.iceflyer3.pfd.ui.model.proxy.food.ReadOnlyFoodModelProxy;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.AbstractMealPlanningMealModelProxy;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingDayViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingMealViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;
import com.iceflyer3.pfd.validation.ValidationResult;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.core.task.TaskExecutor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

public class CarbCyclingMealModelProxy
        extends AbstractMealPlanningMealModelProxy<CarbCyclingMealViewModel>
        implements CarbCyclingMealModel
{
    private final CarbCyclingDayViewModel owningDay;
    private final static Logger LOG = LoggerFactory.getLogger(CarbCyclingMealModelProxy.class);

    public CarbCyclingMealModelProxy(TaskExecutor taskExecutor, MealPlanningTaskFactory mealPlanningTaskFactory, CarbCyclingDayViewModel forDay)
    {
        super(taskExecutor, mealPlanningTaskFactory);
        this.owningDay = forDay;
        this.viewModel = new CarbCyclingMealViewModel(null, NutritionalSummary.empty());
    }

    public CarbCyclingMealModelProxy(TaskExecutor taskExecutor, MealPlanningTaskFactory mealPlanningTaskFactory, CarbCyclingDayViewModel forDay, CarbCyclingMealViewModel viewModel)
    {
        super(taskExecutor, mealPlanningTaskFactory);
        this.owningDay = forDay;
        this.viewModel = viewModel;
    }

    @Override
    public ValidationResult validate()
    {
        validateProxyPropertyInvocation();
        return viewModel.validate();
    }

    @Override
    public ValidationResult validateProperty(int propertyHashcode)
    {
        validateProxyPropertyInvocation();
        return viewModel.validateProperty(propertyHashcode);
    }

    @Override
    public void save(ProxyOperationResultCallback saveResultCallback)
    {
        validateProxyOperationInvocation(ProxyOperation.SAVE);

        SaveCarbCyclingMealTask saveMealTask = mealPlanningTaskFactory.saveCarbCyclingMealAsync(owningDay, viewModel);
        saveMealTask.setOnSucceeded(event -> {
            viewModel = saveMealTask.getValue();
            saveResultCallback.receiveResult(true, "The carb cycling meal has been saved!");
        });
        saveMealTask.setOnFailed(event -> {
            LOG.error("An error has occurred while saving a new carb cycling meal for day {}", owningDay.getId(), saveMealTask.getException());
            saveResultCallback.receiveResult(false, "Failed to save the carb cycling meal. An error has occurred.");
        });
        taskExecutor.execute(saveMealTask);
    }

    @Override
    public UUID getId()
    {
        validateProxyPropertyInvocation();
        return viewModel.getId();
    }

    @Override
    public LocalDateTime getLastModifiedDate()
    {
        validateProxyPropertyInvocation();
        return viewModel.getLastModifiedDate();
    }

    @Override
    public void setLastModifiedDate(LocalDateTime lastModDate)
    {
        validateProxyPropertyInvocation();
        viewModel.setLastModifiedDate(lastModDate);
    }

    @Override
    public ObjectProperty<LocalDateTime> lastModifiedDateProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.lastModifiedDateProperty();
    }

    @Override
    public MealType getMealType() {
        validateProxyPropertyInvocation();
        return viewModel.getMealType();
    }

    @Override
    public ObjectProperty<MealType> mealTypeProperty() {
        validateProxyPropertyInvocation();
        return viewModel.mealTypeProperty();
    }

    @Override
    public void setMealType(MealType mealType) {
        validateProxyPropertyInvocation();
        viewModel.setMealType(mealType);
    }

    @Override
    public int getMealNumber() {
        validateProxyPropertyInvocation();
        return viewModel.getMealNumber();
    }

    @Override
    public IntegerProperty mealNumberProperty() {
        validateProxyPropertyInvocation();
        return viewModel.mealNumberProperty();
    }

    @Override
    public void setMealNumber(int mealNumber) {
        validateProxyPropertyInvocation();
        viewModel.setMealNumber(mealNumber);
    }

    @Override
    public BigDecimal getCarbConsumptionPercentage()
    {
        validateProxyPropertyInvocation();
        return viewModel.getCarbConsumptionPercentage();
    }

    @Override
    public void setCarbConsumptionPercentage(BigDecimal newPercentage)
    {
        validateProxyPropertyInvocation();
        viewModel.setCarbConsumptionPercentage(newPercentage);
    }

    @Override
    public ObjectProperty<BigDecimal> carbConsumptionPercentageProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.carbConsumptionPercentageProperty();
    }

    @Override
    public BigDecimal getRequiredCalories() {
        validateProxyPropertyInvocation();
        return viewModel.getRequiredCalories();
    }

    @Override
    public void setRequiredCalories(BigDecimal requiredCalories) {
        validateProxyPropertyInvocation();
        viewModel.setRequiredCalories(requiredCalories);
    }

    @Override
    public ObjectProperty<BigDecimal> requiredCaloriesProperty() {
        validateProxyPropertyInvocation();
        return viewModel.requiredCaloriesProperty();
    }

    @Override
    public BigDecimal getRequiredProtein() {
        validateProxyPropertyInvocation();
        return viewModel.getRequiredProtein();
    }

    @Override
    public void setRequiredProtein(BigDecimal requiredProtein) {
        validateProxyPropertyInvocation();
        viewModel.setRequiredProtein(requiredProtein);
    }

    @Override
    public ObjectProperty<BigDecimal> requiredProteinProperty() {
        validateProxyPropertyInvocation();
        return viewModel.requiredProteinProperty();
    }

    @Override
    public BigDecimal getRequiredCarbs() {
        validateProxyPropertyInvocation();
        return viewModel.getRequiredCarbs();
    }

    @Override
    public void setRequiredCarbs(BigDecimal requiredCarbs) {
        validateProxyPropertyInvocation();
        viewModel.setRequiredCarbs(requiredCarbs);
    }

    @Override
    public ObjectProperty<BigDecimal> requiredCarbsProperty() {
        validateProxyPropertyInvocation();
        return viewModel.requiredCarbsProperty();
    }

    @Override
    public BigDecimal getRequiredFats() {
        validateProxyPropertyInvocation();
        return viewModel.getRequiredFats();
    }

    @Override
    public void setRequiredFats(BigDecimal requiredFats) {
        validateProxyPropertyInvocation();
        viewModel.setRequiredFats(requiredFats);
    }

    @Override
    public ObjectProperty<BigDecimal> requiredFatsProperty() {
        validateProxyPropertyInvocation();
        return viewModel.requiredFatsProperty();
    }

    // Composable Proxy interface implementation
    @Override
    public String getName()
    {
        validateProxyPropertyInvocation();
        return viewModel.getName();
    }

    @Override
    public void setName(String name)
    {
        validateProxyPropertyInvocation();
        viewModel.setName(name);
    }

    @Override
    public StringProperty nameProperty()
    {
        validateProxyPropertyInvocation();
        return viewModel.nameProperty();
    }

    // Ingredient Detail Reporter implementation
    @Override
    public void refreshNutritionDetails()
    {
        validateProxyPropertyInvocation();
        viewModel.refreshNutritionDetails();
    }

    @Override
    public ObservableList<IngredientModel> getIngredients() {
        validateProxyPropertyInvocation();
        return viewModel.getIngredients();
    }

    /**
     * Add a new ingredient to the list of ingredients. The provided ingredient should be of the view model
     * implementation type which is ReadOnlyFoodViewModel.
     *
     * @param ingredient The ReadOnlyFoodViewModel instance that represents the ingredient food
     */
    @Override
    public void addIngredient(ReadOnlyFoodModel ingredient) {
        validateProxyPropertyInvocation();

        if (ingredient instanceof ReadOnlyFoodModelProxy proxy)
        {
            viewModel.addIngredient(proxy.getViewModel());
        }
        else
        {
            throw new IllegalArgumentException("ingredient must be an instance of ReadOnlyFoodModelProxy");
        }
    }

    @Override
    public void removeIngredient(IngredientModel ingredient)
    {
        validateProxyPropertyInvocation();
        viewModel.removeIngredient(ingredient);
    }
}
