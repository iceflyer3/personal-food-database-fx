/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

CREATE SCHEMA IF NOT EXISTS PFD_V1;
SET SCHEMA PFD_V1;

CREATE TABLE IF NOT EXISTS pfd_user(
    user_id UUID PRIMARY KEY,
    username VARCHAR(MAX) NOT NULL   
);

---------------------------------------
--             FOOD TABLES
---------------------------------------
CREATE TABLE IF NOT EXISTS food_meta_data(
    food_id UUID PRIMARY KEY,
    food_type VARCHAR(MAX)
);

CREATE TABLE IF NOT EXISTS food_serving_size(
    serving_size_id UUID PRIMARY KEY,
    unit_of_measure VARCHAR(50) NOT NULL,
    serving_size DECIMAL(18, 2) NOT NULL,
    food_id UUID NOT NULL,

    CONSTRAINT FK_ServingSize_FoodMetaData FOREIGN KEY (food_id) REFERENCES food_meta_data(food_id)
);

CREATE TABLE IF NOT EXISTS simple_food(
    food_id UUID PRIMARY KEY,
    food_name VARCHAR(MAX) NOT NULL,
    brand VARCHAR(MAX) NOT NULL,
    info_src VARCHAR(MAX) NOT NULL,
    created_user_id UUID NOT NULL,
    created_date TIMESTAMP NOT NULL,
    last_modified_user_id UUID NOT NULL,
    last_modified_date TIMESTAMP NOT NULL,
    glycemic_index INT NOT NULL,
    calories DECIMAL(18, 2) NOT NULL,
    protein DECIMAL(18, 2),
    carbohydrates DECIMAL(18, 2),
    total_fats DECIMAL(18, 2),
    trans_fat DECIMAL(18, 2),
    monounsaturated_fat DECIMAL(18, 2),
    polyunsaturated_fat DECIMAL(18, 2),
    saturated_fat DECIMAL(18, 2),
    sugars DECIMAL(18, 2),
    fiber DECIMAL(18, 2),
    cholesterol DECIMAL(18, 2),
    iron DECIMAL(18, 2),
    manganese DECIMAL(18, 2),
    copper DECIMAL(18, 2),
    zinc DECIMAL(18, 2),
    calcium DECIMAL(18, 2),
    magnesium DECIMAL(18, 2),
    phosphorus DECIMAL(18, 2),
    potassium DECIMAL(18, 2),
    sodium DECIMAL(18, 2),
    sulfur DECIMAL(18, 2),
    vitamin_a DECIMAL(18, 2),
    vitamin_c DECIMAL(18, 2),
    vitamin_k DECIMAL(18, 2),
    vitamin_d DECIMAL(18, 2),
    vitamin_e DECIMAL(18, 2),
    vitamin_b1 DECIMAL(18, 2),
    vitamin_b2 DECIMAL(18, 2),
    vitamin_b3 DECIMAL(18, 2),
    vitamin_b5 DECIMAL(18, 2),
    vitamin_b6 DECIMAL(18, 2),
    vitamin_b7 DECIMAL(18, 2),
    vitamin_b8 DECIMAL(18, 2),
    vitamin_b9 DECIMAL(18, 2),
    vitamin_b12 DECIMAL(18, 2),

    CONSTRAINT FK_FoodMetaData_SimpleFood FOREIGN KEY (food_id) REFERENCES food_meta_data(food_id),
    CONSTRAINT FK_CreatedUser_SimpleFood FOREIGN KEY (created_user_id) REFERENCES pfd_user(user_id),
    CONSTRAINT FK_LastModifiedUser_SimpleFood FOREIGN KEY (last_modified_user_id) REFERENCES pfd_user(user_id)
);


CREATE TABLE IF NOT EXISTS complex_food(
    food_id UUID PRIMARY KEY,
    food_name VARCHAR(MAX) NOT NULL,
    created_user_id UUID NOT NULL,
    created_date TIMESTAMP NOT NULL,
    last_modified_user_id UUID NOT NULL,
    last_modified_date TIMESTAMP NOT NULL,

    CONSTRAINT FK_FoodMetaData_ComplexFood FOREIGN KEY (food_id) REFERENCES food_meta_data(food_id),
    CONSTRAINT FK_CreatedUser_ComplexFood FOREIGN KEY (created_user_id) REFERENCES pfd_user(user_id),
    CONSTRAINT FK_LastModifiedUser_ComplexFood FOREIGN KEY (last_modified_user_id) REFERENCES pfd_user(user_id)
);

CREATE TABLE IF NOT EXISTS complex_food_ingredient(
    ingredient_id UUID PRIMARY KEY,
    simple_food_id UUID,
    complex_food_id UUID,
    simple_food_servings_included DECIMAL(18, 2) NOT NULL,
    is_alternate BOOLEAN NOT NULL,
    is_optional BOOLEAN NOT NULL,

    CONSTRAINT UK_SimpleFood_ComplexFood UNIQUE (simple_food_id, complex_food_id),
    CONSTRAINT FK_Ingredient_SimpleFood FOREIGN KEY (simple_food_id) REFERENCES simple_food(food_id),
    CONSTRAINT FK_Ingredient_ComplexFood FOREIGN KEY (complex_food_id) REFERENCES complex_food(food_id)
);

---------------------------------------
--         MEAL PLANNING TABLES
---------------------------------------
CREATE TABLE IF NOT EXISTS meal_plan(
    meal_plan_id UUID PRIMARY KEY,
    user_id UUID NOT NULL,
    height DECIMAL(18, 2) NOT NULL,
    weight DECIMAL(18, 2) NOT NULL,
    age INT NOT NULL,
    sex VARCHAR(6) NOT NULL,
    activity_level VARCHAR(MAX) NOT NULL,
    fitness_goal VARCHAR(MAX) NOT NULL,
    bmr_formula VARCHAR(MAX) NOT NULL,
    body_fat_percentage DECIMAL(18, 2),
    -- Daily calories are TDEE calories + any surplus or deficit dependent upon goal
    daily_calories DECIMAL(18, 2) NOT NULL,
    -- TDEE calories are the base calories needed per day based upon energy expenditure.
    -- This number does not contain any surpluses or deficits like daily_calories does.
    tdee_calories DECIMAL(18, 2) NOT NULL,
    is_active BOOLEAN NOT NULL,

    CONSTRAINT FK_User_MealPlan FOREIGN KEY (user_id) REFERENCES pfd_user(user_id)
);

CREATE TABLE IF NOT EXISTS meal_plan_nutrient_suggestion(
    macro_id UUID PRIMARY KEY,
    meal_plan_id UUID NOT NULL,
    nutrient_name VARCHAR(MAX) NOT NULL,
    suggested_amount DECIMAL(18, 2) NOT NULL,
    unit_of_measure VARCHAR(9) NOT NULL,

    CONSTRAINT FK_MealPlan_MealPlanMacroSuggestion FOREIGN KEY (meal_plan_id) REFERENCES meal_plan(meal_plan_id)
);

-- Carb cycling plans are effectively strategies for how to split out the macronutrients needed for a day
CREATE TABLE IF NOT EXISTS carb_cycling_plan(
    carb_cycling_plan_id UUID PRIMARY KEY,
    user_id UUID NOT NULL,
    meal_plan_id UUID NOT NULL,
    -- This will always be in the format of high:low, so like 1:2.
    -- Only seven days in a week so always single digit numbers.
    -- Means it can't ever possibly be longer than three characters.
    high_low_day_ratio VARCHAR(3) NOT NULL,
    meals_per_day INT NOT NULL,
    high_day_surplus_percentage DECIMAL(18, 2) NOT NULL,
    low_day_deficit_percentage DECIMAL(18, 2) NOT NULL,
    should_time_fats BOOLEAN NOT NULL,
    is_active BOOLEAN NOT NULL,

    CONSTRAINT FK_User_CarbCyclingPlan FOREIGN KEY (user_id) REFERENCES pfd_user(user_id),
    CONSTRAINT FK_MealPlan_CarbCyclingPlan FOREIGN KEY (meal_plan_id) REFERENCES meal_plan(meal_plan_id)
);

-- Contains the carb cycling percentage restrictions to be used when a new meal for a carb cycling
-- plan is created. This is optional so a carb cycling plan may not have any meal configuration if
-- the user didn't set it up at plan creation time.
CREATE TABLE IF NOT EXISTS carb_cycling_meal_configuration(
  meal_configuration_id UUID PRIMARY KEY,
  carb_cycling_plan_id UUID NOT NULL,
  carb_consumption_min_percentage DECIMAL(18, 2) NOT NULL,
  carb_consumption_max_percentage DECIMAL(18, 2) NOT NULL,
    -- This will be zero if the meal is the post-workout meal
    -- For meals on non-training days this will be the number of the meal in the day
  post_workout_meal_offset INT NOT NULL,
    -- This will be BEFORE, AFTER for meals before or after post-workout on training days
    -- or NONE if the meal is the post-workout meal or for all meals for non-training days
  post_workout_meal_relation VARCHAR(6) NOT NULL,
  is_for_training_day BOOLEAN NOT NULL
);

CREATE TABLE IF NOT EXISTS meal_planning_day(
    day_id UUID PRIMARY KEY,
    meal_plan_id UUID NOT NULL,
    day_fitness_goal VARCHAR(MAX) NOT NULL,
    day_label VARCHAR(MAX) NOT NULL,
    last_modified_date TIMESTAMP NOT NULL,

    CONSTRAINT FK_MealPlanningDay_MealPlan FOREIGN KEY (meal_plan_id) REFERENCES meal_plan(meal_plan_id)
);


CREATE TABLE IF NOT EXISTS carb_cycling_day(
    day_id UUID PRIMARY KEY,
    carb_cycling_plan_id UUID NOT NULL,
    day_type VARCHAR(10),
    is_training_day BOOLEAN NOT NULL,
    required_calories DECIMAL(18, 2) NOT NULL,
    required_protein DECIMAL(18, 2) NOT NULL,
    required_carbs DECIMAL(18, 2) NOT NULL,
    required_fats DECIMAL(18, 2) NOT NULL,

    CONSTRAINT FK_CarbCyclingDay_MealPlanningDay FOREIGN KEY (day_id) REFERENCES meal_planning_day(day_id),
    CONSTRAINT FK_CarbCyclingDay_CarbCyclingPlan FOREIGN KEY (carb_cycling_plan_id) REFERENCES carb_cycling_plan(carb_cycling_plan_id)
);

-- Associates a planned day for either a meal plan or carb cycling plan to a calendar day
CREATE TABLE IF NOT EXISTS meal_plan_calendar_day(
    calendar_day_id UUID PRIMARY KEY,
    day_id UUID NOT NULL,
    user_id UUID NOT NULL,
    calendar_date DATE NOT NULL,
    weekly_interval VARCHAR(9) NOT NULL, -- One of NONE, WEEKLY, or BI-WEEKLY
    interval_months_duration INT NOT NULL, -- The number of months that the interval should repeat
    is_recurrence BOOLEAN NOT NULL,

    CONSTRAINT FK_User_MealPlanCalendarDay FOREIGN KEY (user_id) REFERENCES pfd_user(user_id),
    CONSTRAINT FK_MealPlanningDay_MealPlanCalendarDay FOREIGN KEY (day_id) REFERENCES meal_planning_day(day_id)
);

CREATE TABLE IF NOT EXISTS meal_planning_meal(
    meal_id UUID PRIMARY KEY,
    day_id UUID NOT NULL,
    meal_name VARCHAR(MAX) NOT NULL,
    last_modified_date TIMESTAMP NOT NULL,

    CONSTRAINT FK_MealPlanningDay_MealPlanningMeal FOREIGN KEY (day_id) REFERENCES meal_planning_day(day_id)
);

CREATE TABLE IF NOT EXISTS carb_cycling_meal(
    meal_id UUID PRIMARY KEY,
    carb_cycling_day_id UUID NOT NULL,
    meal_type VARCHAR(MAX) NOT NULL,
    meal_number INT NOT NULL,
    carb_consumption_percentage DECIMAL(18, 2) NOT NULL,
    required_calories DECIMAL(18, 2) NOT NULL,
    required_protein DECIMAL(18, 2) NOT NULL,
    required_carbs DECIMAL(18, 2) NOT NULL,
    required_fats DECIMAL(18, 2) NOT NULL,

    CONSTRAINT FK_CarbCyclingMeal_MealPlanningMeal FOREIGN KEY (meal_id) REFERENCES meal_planning_meal(meal_id),
    CONSTRAINT FK_CarbCyclingMeal_CarbCyclingDay FOREIGN KEY (carb_cycling_day_id) REFERENCES carb_cycling_day(day_id)
);

CREATE TABLE IF NOT EXISTS meal_planning_meal_ingredient(
    ingredient_id UUID PRIMARY KEY,
    food_id UUID NOT NULL,
    meal_id UUID NOT NULL,
    food_servings_included DECIMAL(18, 2) NOT NULL,
    is_alternate BOOLEAN NOT NULL,
    is_optional BOOLEAN NOT NULL,

    CONSTRAINT UK_Food_Meal UNIQUE (food_id, meal_id),
    CONSTRAINT FK_FoodMetaData_MealPlanningIngredient FOREIGN KEY (food_id) REFERENCES food_meta_data(food_id),
    CONSTRAINT FK_MealPlanningMeal_MealPlanningIngredient FOREIGN KEY (meal_id) REFERENCES meal_planning_meal(meal_id)
);

---------------------------------------
--             RECIPE TABLES
---------------------------------------
CREATE TABLE IF NOT EXISTS recipe(
    recipe_id UUID PRIMARY KEY,
    recipe_name VARCHAR(MAX) NOT NULL, 
    source VARCHAR(MAX) NOT NULL,
    number_of_servings_made DECIMAL(18, 2) NOT NULL,
    created_user_id UUID NOT NULL,
    created_date TIMESTAMP NOT NULL,
    last_modified_user_id UUID NOT NULL,
    last_modified_date TIMESTAMP NOT NULL,

    CONSTRAINT FK_CreatedUser_Recipe FOREIGN KEY (created_user_id) REFERENCES pfd_user(user_id),
    CONSTRAINT FK_LastModifiedUser_Recipe FOREIGN KEY (last_modified_user_id) REFERENCES pfd_user(user_id)
);

CREATE TABLE IF NOT EXISTS recipe_ingredient(
    recipe_ingredient_id UUID PRIMARY KEY,
    recipe_id UUID NOT NULL,
    food_id UUID NOT NULL,
    number_of_servings DECIMAL(18, 2) NOT NULL,
    is_alternate BOOLEAN NOT NULL,
    is_optional BOOLEAN NOT NULL,

    CONSTRAINT FK_Recipe_RecipeIngredient FOREIGN KEY (recipe_id) REFERENCES recipe(recipe_id),
    CONSTRAINT FK_FoodMetaData_RecipeIngredient FOREIGN KEY (food_id) REFERENCES food_meta_data(food_id)
);

CREATE TABLE IF NOT EXISTS recipe_serving_size(
    serving_size_id UUID PRIMARY KEY,
    recipe_id UUID NOT NULL,
    unit_of_measure VARCHAR(MAX) NOT NULL,
    serving_size DECIMAL(18, 2) NOT NULL,

    CONSTRAINT FK_Recipe_RecipeServingSize FOREIGN KEY (recipe_id) REFERENCES recipe(recipe_id)
);

CREATE TABLE IF NOT EXISTS recipe_step(
    recipe_step_id UUID PRIMARY KEY,
    recipe_id UUID NOT NULL,
    name VARCHAR(MAX) NOT NULL,
    step_number INT NOT NULL,
    directions VARCHAR(MAX) NOT NULL,
    
    CONSTRAINT FK_Recipe_RecipeStep FOREIGN KEY (recipe_id) REFERENCES recipe(recipe_id)
);

CREATE TABLE IF NOT EXISTS recipe_step_ingredient(
    recipe_id UUID,
    recipe_step_id UUID,
    recipe_ingredient_id UUID,

    CONSTRAINT FK_Recipe_RecipeStepIngredient FOREIGN KEY (recipe_id) REFERENCES recipe(recipe_id),
    CONSTRAINT FK_RecipeStep_RecipeStepIngredient FOREIGN KEY (recipe_step_id) REFERENCES recipe_step(recipe_step_id),
    CONSTRAINT FK_RecipeIngredient_RecipeStepIngredient FOREIGN KEY (recipe_ingredient_id) REFERENCES recipe_ingredient(recipe_ingredient_id)
);


---------------------------------------
--               VIEWS
---------------------------------------

-- Used to determine if a simple food is an ingredient in either a complex food or a recipe
CREATE VIEW IF NOT EXISTS Simple_Foods_As_Ingredients (Food_Id, Name, Owner_Type)
AS
SELECT
       *
FROM
(
    SELECT
        FOOD_ID AS Food_Id, simpleFood.FOOD_NAME AS Name, 'Complex Food' AS Owner_Type
    FROM
        COMPLEX_FOOD_INGREDIENT complexFoodIngredient
    INNER JOIN
        SIMPLE_FOOD simpleFood ON complexFoodIngredient.SIMPLE_FOOD_ID = simpleFood.FOOD_ID

    UNION

    SELECT
        recipeIngredient.FOOD_ID AS Food_Id, recipe.RECIPE_NAME AS Name, 'Recipe' AS Owner_Type
    FROM
        RECIPE_INGREDIENT recipeIngredient
    INNER JOIN
        RECIPE recipe ON recipeIngredient.RECIPE_ID = recipe.RECIPE_ID

    UNION

    SELECT
        FOOD_ID AS Food_Id, 'Meal Plan' AS Name, 'Meal Plan' AS Owner_Type
    FROM
        MEAL_PLANNING_MEAL_INGREDIENT mealPlanMealIngredient
)
ORDER BY Owner_Type;


-- Calculates nutritional information for complex foods
CREATE VIEW IF NOT EXISTS Complex_Food_Nutritional_Details (
        Food_Id,
        Calories,
        Protein,
        Carbohydrates,
        Total_Fats,
        Trans_Fat,
        Monounsaturated_Fat,
        Polyunsaturated_Fat,
        Saturated_Fat,
        Sugars,
        Fiber,
        Cholesterol,
        Iron,
        Manganese,
        Copper,
        Zinc,
        Calcium,
        Magnesium,
        Phosphorus,
        Potassium,
        Sodium,
        Sulfur,
        Vitamin_A,
        Vitamin_C,
        Vitamin_K,
        Vitamin_D,
        Vitamin_E,
        Vitamin_B1,
        Vitamin_B2,
        Vitamin_B3,
        Vitamin_B5,
        Vitamin_B6,
        Vitamin_B7,
        Vitamin_B8,
        Vitamin_B9,
        Vitamin_B12) AS
SELECT
    complexFood.FOOD_ID,
    ROUND(SUM(ingredientFood.CALORIES * complexFoodIngredient.SIMPLE_FOOD_SERVINGS_INCLUDED), 2) AS Calories,
    ROUND(SUM(ingredientFood.PROTEIN * complexFoodIngredient.SIMPLE_FOOD_SERVINGS_INCLUDED), 2) AS Protein,
    ROUND(SUM(ingredientFood.CARBOHYDRATES * complexFoodIngredient.SIMPLE_FOOD_SERVINGS_INCLUDED), 2) AS Carbohydrates,
    ROUND(SUM(ingredientFood.TOTAL_FATS * complexFoodIngredient.SIMPLE_FOOD_SERVINGS_INCLUDED), 2) AS Total_Fats,
    ROUND(SUM(ingredientFood.TRANS_FAT * complexFoodIngredient.SIMPLE_FOOD_SERVINGS_INCLUDED), 2) AS Trans_Fat,
    ROUND(SUM(ingredientFood.MONOUNSATURATED_FAT * complexFoodIngredient.SIMPLE_FOOD_SERVINGS_INCLUDED), 2) AS Monounsaturated_Fat,
    ROUND(SUM(ingredientFood.POLYUNSATURATED_FAT * complexFoodIngredient.SIMPLE_FOOD_SERVINGS_INCLUDED), 2) AS Polyunsaturated_Fat,
    ROUND(SUM(ingredientFood.SATURATED_FAT * complexFoodIngredient.SIMPLE_FOOD_SERVINGS_INCLUDED), 2) AS Saturated_Fat,
    ROUND(SUM(ingredientFood.SUGARS * complexFoodIngredient.SIMPLE_FOOD_SERVINGS_INCLUDED), 2) AS Sugars,
    ROUND(SUM(ingredientFood.FIBER * complexFoodIngredient.SIMPLE_FOOD_SERVINGS_INCLUDED), 2) AS Fiber,
    ROUND(SUM(ingredientFood.CHOLESTEROL * complexFoodIngredient.SIMPLE_FOOD_SERVINGS_INCLUDED), 2) AS Cholesterol,
    ROUND(SUM(ingredientFood.IRON * complexFoodIngredient.SIMPLE_FOOD_SERVINGS_INCLUDED), 2) AS Iron,
    ROUND(SUM(ingredientFood.MANGANESE * complexFoodIngredient.SIMPLE_FOOD_SERVINGS_INCLUDED), 2) AS Manganese,
    ROUND(SUM(ingredientFood.COPPER * complexFoodIngredient.SIMPLE_FOOD_SERVINGS_INCLUDED), 2) AS Copper,
    ROUND(SUM(ingredientFood.ZINC * complexFoodIngredient.SIMPLE_FOOD_SERVINGS_INCLUDED), 2) AS Zinc,
    ROUND(SUM(ingredientFood.CALCIUM * complexFoodIngredient.SIMPLE_FOOD_SERVINGS_INCLUDED), 2) AS Calcium,
    ROUND(SUM(ingredientFood.MAGNESIUM * complexFoodIngredient.SIMPLE_FOOD_SERVINGS_INCLUDED), 2) AS Magnesium,
    ROUND(SUM(ingredientFood.PHOSPHORUS * complexFoodIngredient.SIMPLE_FOOD_SERVINGS_INCLUDED), 2) AS Phosphorus,
    ROUND(SUM(ingredientFood.POTASSIUM * complexFoodIngredient.SIMPLE_FOOD_SERVINGS_INCLUDED), 2) AS Potassium,
    ROUND(SUM(ingredientFood.SODIUM * complexFoodIngredient.SIMPLE_FOOD_SERVINGS_INCLUDED), 2) AS Sodium,
    ROUND(SUM(ingredientFood.SULFUR * complexFoodIngredient.SIMPLE_FOOD_SERVINGS_INCLUDED), 2) AS Sulfur,
    ROUND(SUM(ingredientFood.VITAMIN_A * complexFoodIngredient.SIMPLE_FOOD_SERVINGS_INCLUDED), 2) AS Vitamin_A,
    ROUND(SUM(ingredientFood.VITAMIN_C * complexFoodIngredient.SIMPLE_FOOD_SERVINGS_INCLUDED), 2) AS Vitamin_C,
    ROUND(SUM(ingredientFood.VITAMIN_K * complexFoodIngredient.SIMPLE_FOOD_SERVINGS_INCLUDED), 2) AS Vitamin_K,
    ROUND(SUM(ingredientFood.VITAMIN_D * complexFoodIngredient.SIMPLE_FOOD_SERVINGS_INCLUDED), 2) AS Vitamin_D,
    ROUND(SUM(ingredientFood.VITAMIN_E * complexFoodIngredient.SIMPLE_FOOD_SERVINGS_INCLUDED), 2) AS Vitamin_E,
    ROUND(SUM(ingredientFood.VITAMIN_B1 * complexFoodIngredient.SIMPLE_FOOD_SERVINGS_INCLUDED), 2) AS Vitamin_B1,
    ROUND(SUM(ingredientFood.VITAMIN_B2 * complexFoodIngredient.SIMPLE_FOOD_SERVINGS_INCLUDED), 2) AS Vitamin_B2,
    ROUND(SUM(ingredientFood.VITAMIN_B3 * complexFoodIngredient.SIMPLE_FOOD_SERVINGS_INCLUDED), 2) AS Vitamin_B3,
    ROUND(SUM(ingredientFood.VITAMIN_B5 * complexFoodIngredient.SIMPLE_FOOD_SERVINGS_INCLUDED), 2) AS Vitamin_B5,
    ROUND(SUM(ingredientFood.VITAMIN_B6 * complexFoodIngredient.SIMPLE_FOOD_SERVINGS_INCLUDED), 2) AS Vitamin_B6,
    ROUND(SUM(ingredientFood.VITAMIN_B7 * complexFoodIngredient.SIMPLE_FOOD_SERVINGS_INCLUDED), 2) AS Vitamin_B7,
    ROUND(SUM(ingredientFood.VITAMIN_B8 * complexFoodIngredient.SIMPLE_FOOD_SERVINGS_INCLUDED), 2) AS Vitamin_B8,
    ROUND(SUM(ingredientFood.VITAMIN_B9 * complexFoodIngredient.SIMPLE_FOOD_SERVINGS_INCLUDED), 2) AS Vitamin_B9,
    ROUND(SUM(ingredientFood.VITAMIN_B12 * complexFoodIngredient.SIMPLE_FOOD_SERVINGS_INCLUDED), 2) AS Vitamin_B12
FROM
    COMPLEX_FOOD complexFood
INNER JOIN
    COMPLEX_FOOD_INGREDIENT complexFoodIngredient ON complexFood.FOOD_ID = complexFoodIngredient.COMPLEX_FOOD_ID
INNER JOIN
    SIMPLE_FOOD ingredientFood ON complexFoodIngredient.SIMPLE_FOOD_ID = ingredientFood.FOOD_ID
GROUP BY
    complexFood.FOOD_ID;


-- Find the nutritional information for any food regardless of it is a simple or complex food
CREATE VIEW IF NOT EXISTS All_Food_Nutritional_Details (
        Food_Id,
        Calories,
        Protein,
        Carbohydrates,
        Total_Fats,
        Trans_Fat,
        Monounsaturated_Fat,
        Polyunsaturated_Fat,
        Saturated_Fat,
        Sugars,
        Fiber,
        Cholesterol,
        Iron,
        Manganese,
        Copper,
        Zinc,
        Calcium,
        Magnesium,
        Phosphorus,
        Potassium,
        Sodium,
        Sulfur,
        Vitamin_A,
        Vitamin_C,
        Vitamin_K,
        Vitamin_D,
        Vitamin_E,
        Vitamin_B1,
        Vitamin_B2,
        Vitamin_B3,
        Vitamin_B5,
        Vitamin_B6,
        Vitamin_B7,
        Vitamin_B8,
        Vitamin_B9,
        Vitamin_B12) AS
SELECT
    foodMetaData.FOOD_ID,
    CASE WHEN foodMetaData.FOOD_TYPE = 'SIMPLE' THEN simpleFood.CALORIES ELSE complexFood.CALORIES END AS Calories,
    CASE WHEN foodMetaData.FOOD_TYPE = 'SIMPLE' THEN simpleFood.PROTEIN ELSE complexFood.PROTEIN END AS Protein,
    CASE WHEN foodMetaData.FOOD_TYPE = 'SIMPLE' THEN simpleFood.CARBOHYDRATES ELSE complexFood.CARBOHYDRATES END AS Carbohydrates,
    CASE WHEN foodMetaData.FOOD_TYPE = 'SIMPLE' THEN simpleFood.TOTAL_FATS ELSE complexFood.TOTAL_FATS END AS Total_Fats,
    CASE WHEN foodMetaData.FOOD_TYPE = 'SIMPLE' THEN simpleFood.TRANS_FAT ELSE complexFood.TRANS_FAT END AS Trans_Fat,
    CASE WHEN foodMetaData.FOOD_TYPE = 'SIMPLE' THEN simpleFood.MONOUNSATURATED_FAT ELSE complexFood.MONOUNSATURATED_FAT END AS Monounsaturated_Fat,
    CASE WHEN foodMetaData.FOOD_TYPE = 'SIMPLE' THEN simpleFood.POLYUNSATURATED_FAT ELSE complexFood.POLYUNSATURATED_FAT END AS Polyunsaturated_Fat,
    CASE WHEN foodMetaData.FOOD_TYPE = 'SIMPLE' THEN simpleFood.SATURATED_FAT ELSE complexFood.SATURATED_FAT END AS Saturated_Fat,
    CASE WHEN foodMetaData.FOOD_TYPE = 'SIMPLE' THEN simpleFood.SUGARS ELSE complexFood.SUGARS END AS Sugars,
    CASE WHEN foodMetaData.FOOD_TYPE = 'SIMPLE' THEN simpleFood.FIBER ELSE complexFood.FIBER END AS Fiber,
    CASE WHEN foodMetaData.FOOD_TYPE = 'SIMPLE' THEN simpleFood.CHOLESTEROL ELSE complexFood.CHOLESTEROL END AS Cholesterol,
    CASE WHEN foodMetaData.FOOD_TYPE = 'SIMPLE' THEN simpleFood.IRON ELSE complexFood.IRON END AS Iron,
    CASE WHEN foodMetaData.FOOD_TYPE = 'SIMPLE' THEN simpleFood.MANGANESE ELSE complexFood.MANGANESE END AS Manganese,
    CASE WHEN foodMetaData.FOOD_TYPE = 'SIMPLE' THEN simpleFood.COPPER ELSE complexFood.COPPER END AS Copper,
    CASE WHEN foodMetaData.FOOD_TYPE = 'SIMPLE' THEN simpleFood.ZINC ELSE complexFood.ZINC END AS Zinc,
    CASE WHEN foodMetaData.FOOD_TYPE = 'SIMPLE' THEN simpleFood.CALCIUM ELSE complexFood.CALCIUM END AS Calcium,
    CASE WHEN foodMetaData.FOOD_TYPE = 'SIMPLE' THEN simpleFood.MAGNESIUM ELSE complexFood.MAGNESIUM END AS Magnesium,
    CASE WHEN foodMetaData.FOOD_TYPE = 'SIMPLE' THEN simpleFood.PHOSPHORUS ELSE complexFood.PHOSPHORUS END AS Phosphorus,
    CASE WHEN foodMetaData.FOOD_TYPE = 'SIMPLE' THEN simpleFood.POTASSIUM ELSE complexFood.POTASSIUM END AS Potassium,
    CASE WHEN foodMetaData.FOOD_TYPE = 'SIMPLE' THEN simpleFood.SODIUM ELSE complexFood.SODIUM END AS Sodium,
    CASE WHEN foodMetaData.FOOD_TYPE = 'SIMPLE' THEN simpleFood.SULFUR ELSE complexFood.SULFUR END AS Sulfur,
    CASE WHEN foodMetaData.FOOD_TYPE = 'SIMPLE' THEN simpleFood.VITAMIN_A ELSE complexFood.VITAMIN_A END AS Vitamin_A,
    CASE WHEN foodMetaData.FOOD_TYPE = 'SIMPLE' THEN simpleFood.VITAMIN_C ELSE complexFood.VITAMIN_C END AS Vitamin_C,
    CASE WHEN foodMetaData.FOOD_TYPE = 'SIMPLE' THEN simpleFood.VITAMIN_K ELSE complexFood.VITAMIN_K END AS Vitamin_K,
    CASE WHEN foodMetaData.FOOD_TYPE = 'SIMPLE' THEN simpleFood.VITAMIN_D ELSE complexFood.VITAMIN_D END AS Vitamin_D,
    CASE WHEN foodMetaData.FOOD_TYPE = 'SIMPLE' THEN simpleFood.VITAMIN_E ELSE complexFood.VITAMIN_E END AS Vitamin_E,
    CASE WHEN foodMetaData.FOOD_TYPE = 'SIMPLE' THEN simpleFood.VITAMIN_B1 ELSE complexFood.VITAMIN_B1 END AS Vitamin_B1,
    CASE WHEN foodMetaData.FOOD_TYPE = 'SIMPLE' THEN simpleFood.VITAMIN_B2 ELSE complexFood.VITAMIN_B2 END AS Vitamin_B2,
    CASE WHEN foodMetaData.FOOD_TYPE = 'SIMPLE' THEN simpleFood.VITAMIN_B3 ELSE complexFood.VITAMIN_B3 END AS Vitamin_B3,
    CASE WHEN foodMetaData.FOOD_TYPE = 'SIMPLE' THEN simpleFood.VITAMIN_B5 ELSE complexFood.VITAMIN_B5 END AS Vitamin_B5,
    CASE WHEN foodMetaData.FOOD_TYPE = 'SIMPLE' THEN simpleFood.VITAMIN_B6 ELSE complexFood.VITAMIN_B6 END AS Vitamin_B6,
    CASE WHEN foodMetaData.FOOD_TYPE = 'SIMPLE' THEN simpleFood.VITAMIN_B7 ELSE complexFood.VITAMIN_B7 END AS Vitamin_B7,
    CASE WHEN foodMetaData.FOOD_TYPE = 'SIMPLE' THEN simpleFood.VITAMIN_B8 ELSE complexFood.VITAMIN_B8 END AS Vitamin_B8,
    CASE WHEN foodMetaData.FOOD_TYPE = 'SIMPLE' THEN simpleFood.VITAMIN_B9 ELSE complexFood.VITAMIN_B9 END AS Vitamin_B9,
    CASE WHEN foodMetaData.FOOD_TYPE = 'SIMPLE' THEN simpleFood.VITAMIN_B12 ELSE complexFood.VITAMIN_B12 END AS Vitamin_B12
FROM
    FOOD_META_DATA foodMetaData
LEFT JOIN
    SIMPLE_FOOD simpleFood ON foodMetaData.FOOD_ID = simpleFood.FOOD_ID
LEFT JOIN
    COMPLEX_FOOD_NUTRITIONAL_DETAILS complexFood ON foodMetaData.FOOD_ID = complexFood.FOOD_ID;

/*
 * Find the nutritional summary of the list of ingredients for meals.
 *
 * As of this writing H2 only appears to have support for recursive CTEs. This view is actually part of the same
 * query as the Meal_Plan_Meal_Nutritional_Summary view. This was only made a view because it couldn't be made
 * as a CTE, I find sub-queries typically hurt readability, and there is no additional cost to this being a view.
*/
CREATE VIEW IF NOT EXISTS Meal_Plan_Meal_Nutritional_Summary_CTE (
    Meal_Id,
    Calories,
    Protein,
    Carbohydrates,
    Total_Fats,
    Trans_Fat,
    Monounsaturated_Fat,
    Polyunsaturated_Fat,
    Saturated_Fat,
    Sugars,
    Fiber,
    Cholesterol,
    Iron,
    Manganese,
    Copper,
    Zinc,
    Calcium,
    Magnesium,
    Phosphorus,
    Potassium,
    Sodium,
    Sulfur,
    Vitamin_A,
    Vitamin_C,
    Vitamin_K,
    Vitamin_D,
    Vitamin_E,
    Vitamin_B1,
    Vitamin_B2,
    Vitamin_B3,
    Vitamin_B5,
    Vitamin_B6,
    Vitamin_B7,
    Vitamin_B8,
    Vitamin_B9,
    Vitamin_B12) AS
SELECT
    ingredient.MEAL_ID,
    ROUND(SUM(ingredientNutrionalDetails.CALORIES * ingredient.FOOD_SERVINGS_INCLUDED), 2) AS Calories,
    ROUND(SUM(ingredientNutrionalDetails.PROTEIN * ingredient.FOOD_SERVINGS_INCLUDED), 2) AS Protein,
    ROUND(SUM(ingredientNutrionalDetails.CARBOHYDRATES * ingredient.FOOD_SERVINGS_INCLUDED), 2) AS Carbohydrates,
    ROUND(SUM(ingredientNutrionalDetails.TOTAL_FATS * ingredient.FOOD_SERVINGS_INCLUDED), 2) AS Total_Fats,
    ROUND(SUM(ingredientNutrionalDetails.TRANS_FAT * ingredient.FOOD_SERVINGS_INCLUDED), 2) AS Trans_Fat,
    ROUND(SUM(ingredientNutrionalDetails.MONOUNSATURATED_FAT * ingredient.FOOD_SERVINGS_INCLUDED), 2) AS Monounsaturated_Fat,
    ROUND(SUM(ingredientNutrionalDetails.POLYUNSATURATED_FAT * ingredient.FOOD_SERVINGS_INCLUDED), 2) AS Polyunsaturated_Fat,
    ROUND(SUM(ingredientNutrionalDetails.SATURATED_FAT * ingredient.FOOD_SERVINGS_INCLUDED), 2) AS Saturated_Fat,
    ROUND(SUM(ingredientNutrionalDetails.SUGARS * ingredient.FOOD_SERVINGS_INCLUDED), 2) AS Sugars,
    ROUND(SUM(ingredientNutrionalDetails.FIBER * ingredient.FOOD_SERVINGS_INCLUDED), 2) AS Fiber,
    ROUND(SUM(ingredientNutrionalDetails.CHOLESTEROL * ingredient.FOOD_SERVINGS_INCLUDED), 2) AS Cholesterol,
    ROUND(SUM(ingredientNutrionalDetails.IRON * ingredient.FOOD_SERVINGS_INCLUDED), 2) AS Iron,
    ROUND(SUM(ingredientNutrionalDetails.MANGANESE * ingredient.FOOD_SERVINGS_INCLUDED), 2) AS Manganese,
    ROUND(SUM(ingredientNutrionalDetails.COPPER * ingredient.FOOD_SERVINGS_INCLUDED), 2) AS Copper,
    ROUND(SUM(ingredientNutrionalDetails.ZINC * ingredient.FOOD_SERVINGS_INCLUDED), 2) AS Zinc,
    ROUND(SUM(ingredientNutrionalDetails.CALCIUM * ingredient.FOOD_SERVINGS_INCLUDED), 2) AS Calcium,
    ROUND(SUM(ingredientNutrionalDetails.MAGNESIUM * ingredient.FOOD_SERVINGS_INCLUDED), 2) AS Magnesium,
    ROUND(SUM(ingredientNutrionalDetails.PHOSPHORUS * ingredient.FOOD_SERVINGS_INCLUDED), 2) AS Phosphorus,
    ROUND(SUM(ingredientNutrionalDetails.POTASSIUM * ingredient.FOOD_SERVINGS_INCLUDED), 2) AS Potassium,
    ROUND(SUM(ingredientNutrionalDetails.SODIUM * ingredient.FOOD_SERVINGS_INCLUDED), 2) AS Sodium,
    ROUND(SUM(ingredientNutrionalDetails.SULFUR * ingredient.FOOD_SERVINGS_INCLUDED), 2) AS Sulfur,
    ROUND(SUM(ingredientNutrionalDetails.VITAMIN_A * ingredient.FOOD_SERVINGS_INCLUDED), 2) AS Vitamin_A,
    ROUND(SUM(ingredientNutrionalDetails.VITAMIN_C * ingredient.FOOD_SERVINGS_INCLUDED), 2) AS Vitamin_C,
    ROUND(SUM(ingredientNutrionalDetails.VITAMIN_K * ingredient.FOOD_SERVINGS_INCLUDED), 2) AS Vitamin_K,
    ROUND(SUM(ingredientNutrionalDetails.VITAMIN_D * ingredient.FOOD_SERVINGS_INCLUDED), 2) AS Vitamin_D,
    ROUND(SUM(ingredientNutrionalDetails.VITAMIN_E * ingredient.FOOD_SERVINGS_INCLUDED), 2) AS Vitamin_E,
    ROUND(SUM(ingredientNutrionalDetails.VITAMIN_B1 * ingredient.FOOD_SERVINGS_INCLUDED), 2) AS Vitamin_B1,
    ROUND(SUM(ingredientNutrionalDetails.VITAMIN_B2 * ingredient.FOOD_SERVINGS_INCLUDED), 2) AS Vitamin_B2,
    ROUND(SUM(ingredientNutrionalDetails.VITAMIN_B3 * ingredient.FOOD_SERVINGS_INCLUDED), 2) AS Vitamin_B3,
    ROUND(SUM(ingredientNutrionalDetails.VITAMIN_B5 * ingredient.FOOD_SERVINGS_INCLUDED), 2) AS Vitamin_B5,
    ROUND(SUM(ingredientNutrionalDetails.VITAMIN_B6 * ingredient.FOOD_SERVINGS_INCLUDED), 2) AS Vitamin_B6,
    ROUND(SUM(ingredientNutrionalDetails.VITAMIN_B7 * ingredient.FOOD_SERVINGS_INCLUDED), 2) AS Vitamin_B7,
    ROUND(SUM(ingredientNutrionalDetails.VITAMIN_B8 * ingredient.FOOD_SERVINGS_INCLUDED), 2) AS Vitamin_B8,
    ROUND(SUM(ingredientNutrionalDetails.VITAMIN_B9 * ingredient.FOOD_SERVINGS_INCLUDED), 2) AS Vitamin_B9,
    ROUND(SUM(ingredientNutrionalDetails.VITAMIN_B12 * ingredient.FOOD_SERVINGS_INCLUDED), 2) AS Vitamin_B12
FROM
    MEAL_PLANNING_MEAL_INGREDIENT ingredient
INNER JOIN
    ALL_FOOD_NUTRITIONAL_DETAILS ingredientNutrionalDetails ON ingredientNutrionalDetails.FOOD_ID = ingredient.FOOD_ID
GROUP BY
    ingredient.MEAL_ID;

-- Find the nutritional summary of meal plan meals, defaulting to 0 if the meal has no ingredients.
CREATE VIEW IF NOT EXISTS Meal_Plan_Meal_Nutritional_Summary (
        Meal_Id,
        Day_Id,
        Calories,
        Protein,
        Carbohydrates,
        Total_Fats,
        Trans_Fat,
        Monounsaturated_Fat,
        Polyunsaturated_Fat,
        Saturated_Fat,
        Sugars,
        Fiber,
        Cholesterol,
        Iron,
        Manganese,
        Copper,
        Zinc,
        Calcium,
        Magnesium,
        Phosphorus,
        Potassium,
        Sodium,
        Sulfur,
        Vitamin_A,
        Vitamin_C,
        Vitamin_K,
        Vitamin_D,
        Vitamin_E,
        Vitamin_B1,
        Vitamin_B2,
        Vitamin_B3,
        Vitamin_B5,
        Vitamin_B6,
        Vitamin_B7,
        Vitamin_B8,
        Vitamin_B9,
        Vitamin_B12) AS
SELECT
    mealPlanMeal.MEAL_ID,
    mealPlanMeal.DAY_ID,
    mealIngredient.Calories AS Calories,
    mealIngredient.Protein AS Protein,
    mealIngredient.Carbohydrates AS Carbohydrates,
    mealIngredient.Total_Fats AS Total_Fats,
    mealIngredient.Trans_Fat AS Trans_Fat,
    mealIngredient.Monounsaturated_Fat AS Monounsaturated_Fat,
    mealIngredient.Polyunsaturated_Fat AS Polyunsaturated_Fat,
    mealIngredient.Saturated_Fat AS Saturated_Fat,
    mealIngredient.Sugars AS Sugars,
    mealIngredient.Fiber AS Fiber,
    mealIngredient.Cholesterol AS Cholesterol,
    mealIngredient.Iron AS Iron,
    mealIngredient.Manganese AS Manganese,
    mealIngredient.Copper AS Copper,
    mealIngredient.Zinc AS Zinc,
    mealIngredient.Calcium AS Calcium,
    mealIngredient.Magnesium AS Magnesium,
    mealIngredient.Phosphorus AS Phosphorus,
    mealIngredient.Potassium AS Potassium,
    mealIngredient.Sodium AS Sodium,
    mealIngredient.Sulfur AS Sulfur,
    mealIngredient.Vitamin_A AS Vitamin_A,
    mealIngredient.Vitamin_C AS Vitamin_C,
    mealIngredient.Vitamin_K AS Vitamin_K,
    mealIngredient.Vitamin_D AS Vitamin_D,
    mealIngredient.Vitamin_E AS Vitamin_E,
    mealIngredient.Vitamin_B1 AS Vitamin_B1,
    mealIngredient.Vitamin_B2 AS Vitamin_B2,
    mealIngredient.Vitamin_B3 AS Vitamin_B3,
    mealIngredient.Vitamin_B5 AS Vitamin_B5,
    mealIngredient.Vitamin_B6 AS Vitamin_B6,
    mealIngredient.Vitamin_B7 AS Vitamin_B7,
    mealIngredient.Vitamin_B8 AS Vitamin_B8,
    mealIngredient.Vitamin_B9 AS Vitamin_B9,
    mealIngredient.Vitamin_B12  AS Vitamin_B12
FROM
    MEAL_PLANNING_MEAL mealPlanMeal
INNER JOIN
    MEAL_PLAN_MEAL_NUTRITIONAL_SUMMARY_CTE mealIngredient ON mealPlanMeal.MEAL_ID = mealIngredient.MEAL_ID;


-- Find the nutritional summary of meal plan days
CREATE VIEW IF NOT EXISTS Meal_Plan_Day_Nutritional_Summary (
        Day_Id,
        Meal_Plan_Id,
        Calories,
        Protein,
        Carbohydrates,
        Total_Fats,
        Trans_Fat,
        Monounsaturated_Fat,
        Polyunsaturated_Fat,
        Saturated_Fat,
        Sugars,
        Fiber,
        Cholesterol,
        Iron,
        Manganese,
        Copper,
        Zinc,
        Calcium,
        Magnesium,
        Phosphorus,
        Potassium,
        Sodium,
        Sulfur,
        Vitamin_A,
        Vitamin_C,
        Vitamin_K,
        Vitamin_D,
        Vitamin_E,
        Vitamin_B1,
        Vitamin_B2,
        Vitamin_B3,
        Vitamin_B5,
        Vitamin_B6,
        Vitamin_B7,
        Vitamin_B8,
        Vitamin_B9,
        Vitamin_B12) AS
SELECT
    mealNutrionalSummary.DAY_ID,
    mealPlanDay.MEAL_PLAN_ID,
    ROUND(SUM(mealNutrionalSummary.CALORIES), 2) AS Calories,
    ROUND(SUM(mealNutrionalSummary.PROTEIN), 2) AS Protein,
    ROUND(SUM(mealNutrionalSummary.CARBOHYDRATES), 2) AS Carbohydrates,
    ROUND(SUM(mealNutrionalSummary.TOTAL_FATS), 2) AS Total_Fats,
    ROUND(SUM(mealNutrionalSummary.TRANS_FAT), 2) AS Trans_Fat,
    ROUND(SUM(mealNutrionalSummary.MONOUNSATURATED_FAT), 2) AS Monounsaturated_Fat,
    ROUND(SUM(mealNutrionalSummary.POLYUNSATURATED_FAT), 2) AS Polyunsaturated_Fat,
    ROUND(SUM(mealNutrionalSummary.SATURATED_FAT), 2) AS Saturated_Fat,
    ROUND(SUM(mealNutrionalSummary.SUGARS), 2) AS Sugars,
    ROUND(SUM(mealNutrionalSummary.FIBER), 2) AS Fiber,
    ROUND(SUM(mealNutrionalSummary.CHOLESTEROL), 2) AS Cholesterol,
    ROUND(SUM(mealNutrionalSummary.IRON), 2) AS Iron,
    ROUND(SUM(mealNutrionalSummary.MANGANESE), 2) AS Manganese,
    ROUND(SUM(mealNutrionalSummary.COPPER), 2) AS Copper,
    ROUND(SUM(mealNutrionalSummary.ZINC), 2) AS Zinc,
    ROUND(SUM(mealNutrionalSummary.CALCIUM), 2) AS Calcium,
    ROUND(SUM(mealNutrionalSummary.MAGNESIUM), 2) AS Magnesium,
    ROUND(SUM(mealNutrionalSummary.PHOSPHORUS), 2) AS Phosphorus,
    ROUND(SUM(mealNutrionalSummary.POTASSIUM), 2) AS Potassium,
    ROUND(SUM(mealNutrionalSummary.SODIUM), 2) AS Sodium,
    ROUND(SUM(mealNutrionalSummary.SULFUR), 2) AS Sulfur,
    ROUND(SUM(mealNutrionalSummary.VITAMIN_A), 2) AS Vitamin_A,
    ROUND(SUM(mealNutrionalSummary.VITAMIN_C), 2) AS Vitamin_C,
    ROUND(SUM(mealNutrionalSummary.VITAMIN_K), 2) AS Vitamin_K,
    ROUND(SUM(mealNutrionalSummary.VITAMIN_D), 2) AS Vitamin_D,
    ROUND(SUM(mealNutrionalSummary.VITAMIN_E), 2) AS Vitamin_E,
    ROUND(SUM(mealNutrionalSummary.VITAMIN_B1), 2) AS Vitamin_B1,
    ROUND(SUM(mealNutrionalSummary.VITAMIN_B2), 2) AS Vitamin_B2,
    ROUND(SUM(mealNutrionalSummary.VITAMIN_B3), 2) AS Vitamin_B3,
    ROUND(SUM(mealNutrionalSummary.VITAMIN_B5), 2) AS Vitamin_B5,
    ROUND(SUM(mealNutrionalSummary.VITAMIN_B6), 2) AS Vitamin_B6,
    ROUND(SUM(mealNutrionalSummary.VITAMIN_B7), 2) AS Vitamin_B7,
    ROUND(SUM(mealNutrionalSummary.VITAMIN_B8), 2) AS Vitamin_B8,
    ROUND(SUM(mealNutrionalSummary.VITAMIN_B9), 2) AS Vitamin_B9,
    ROUND(SUM(mealNutrionalSummary.VITAMIN_B12), 2) AS Vitamin_B12
FROM
    MEAL_PLANNING_DAY mealPlanDay
INNER JOIN
    MEAL_PLAN_MEAL_NUTRITIONAL_SUMMARY mealNutrionalSummary ON mealPlanDay.DAY_ID = mealNutrionalSummary.DAY_ID
GROUP BY
    mealNutrionalSummary.DAY_ID;


-- Find the nutritional summary of carb cycling meals
CREATE VIEW IF NOT EXISTS Carb_Cycling_Meal_Nutritional_Summary (
        Meal_Id,
        Day_Id,
        Required_Calories,
        Required_Protein,
        Required_Carbs,
        Required_Fats,
        Calories,
        Protein,
        Carbohydrates,
        Total_Fats,
        Trans_Fat,
        Monounsaturated_Fat,
        Polyunsaturated_Fat,
        Saturated_Fat,
        Sugars,
        Fiber,
        Cholesterol,
        Iron,
        Manganese,
        Copper,
        Zinc,
        Calcium,
        Magnesium,
        Phosphorus,
        Potassium,
        Sodium,
        Sulfur,
        Vitamin_A,
        Vitamin_C,
        Vitamin_K,
        Vitamin_D,
        Vitamin_E,
        Vitamin_B1,
        Vitamin_B2,
        Vitamin_B3,
        Vitamin_B5,
        Vitamin_B6,
        Vitamin_B7,
        Vitamin_B8,
        Vitamin_B9,
        Vitamin_B12) AS
SELECT
    mealSummary.MEAL_ID,
    mealSummary.DAY_ID,
    carbCyclingMeal.REQUIRED_CALORIES AS Required_Calories,
    carbCyclingMeal.REQUIRED_PROTEIN AS Required_Protein,
    carbCyclingMeal.REQUIRED_CARBS AS Required_Carbs,
    carbCyclingMeal.REQUIRED_FATS AS Required_Fats,
    mealSummary.Calories,
    mealSummary.Protein,
    mealSummary.Carbohydrates,
    mealSummary.Total_Fats,
    mealSummary.Trans_Fat,
    mealSummary.Monounsaturated_Fat,
    mealSummary.Polyunsaturated_Fat,
    mealSummary.Saturated_Fat,
    mealSummary.Sugars,
    mealSummary.Fiber,
    mealSummary.Cholesterol,
    mealSummary.Iron,
    mealSummary.Manganese,
    mealSummary.Copper,
    mealSummary.Zinc,
    mealSummary.Calcium,
    mealSummary.Magnesium,
    mealSummary.Phosphorus,
    mealSummary.Potassium,
    mealSummary.Sodium,
    mealSummary.Sulfur,
    mealSummary.Vitamin_A,
    mealSummary.Vitamin_C,
    mealSummary.Vitamin_K,
    mealSummary.Vitamin_D,
    mealSummary.Vitamin_E,
    mealSummary.Vitamin_B1,
    mealSummary.Vitamin_B2,
    mealSummary.Vitamin_B3,
    mealSummary.Vitamin_B5,
    mealSummary.Vitamin_B6,
    mealSummary.Vitamin_B7,
    mealSummary.Vitamin_B8,
    mealSummary.Vitamin_B9,
    mealSummary.Vitamin_B12
FROM
    CARB_CYCLING_MEAL carbCyclingMeal
INNER JOIN
    MEAL_PLAN_MEAL_NUTRITIONAL_SUMMARY mealSummary ON carbCyclingMeal.MEAL_ID = mealSummary.MEAL_ID;


-- Find the nutritional summary of carb cycling days
CREATE VIEW IF NOT EXISTS Carb_Cycling_Day_Nutritional_Summary (
        Day_Id,
        Carb_Cycling_Plan_Id,
        Required_Calories,
        Required_Protein,
        Required_Carbs,
        Required_Fats,
        Calories,
        Protein,
        Carbohydrates,
        Total_Fats,
        Trans_Fat,
        Monounsaturated_Fat,
        Polyunsaturated_Fat,
        Saturated_Fat,
        Sugars,
        Fiber,
        Cholesterol,
        Iron,
        Manganese,
        Copper,
        Zinc,
        Calcium,
        Magnesium,
        Phosphorus,
        Potassium,
        Sodium,
        Sulfur,
        Vitamin_A,
        Vitamin_C,
        Vitamin_K,
        Vitamin_D,
        Vitamin_E,
        Vitamin_B1,
        Vitamin_B2,
        Vitamin_B3,
        Vitamin_B5,
        Vitamin_B6,
        Vitamin_B7,
        Vitamin_B8,
        Vitamin_B9,
        Vitamin_B12) AS
SELECT
    daySummary.DAY_ID,
    carbCyclingDay.CARB_CYCLING_PLAN_ID AS Carb_Cycling_Plan_Id,
    carbCyclingDay.REQUIRED_CALORIES AS Required_Calories,
    carbCyclingDay.REQUIRED_PROTEIN AS Required_Protein,
    carbCyclingDay.REQUIRED_CARBS AS Required_Carbs,
    carbCyclingDay.REQUIRED_FATS AS Required_Fats,
    daySummary.Calories,
    daySummary.Protein,
    daySummary.Carbohydrates,
    daySummary.Total_Fats,
    daySummary.Trans_Fat,
    daySummary.Monounsaturated_Fat,
    daySummary.Polyunsaturated_Fat,
    daySummary.Saturated_Fat,
    daySummary.Sugars,
    daySummary.Fiber,
    daySummary.Cholesterol,
    daySummary.Iron,
    daySummary.Manganese,
    daySummary.Copper,
    daySummary.Zinc,
    daySummary.Calcium,
    daySummary.Magnesium,
    daySummary.Phosphorus,
    daySummary.Potassium,
    daySummary.Sodium,
    daySummary.Sulfur,
    daySummary.Vitamin_A,
    daySummary.Vitamin_C,
    daySummary.Vitamin_K,
    daySummary.Vitamin_D,
    daySummary.Vitamin_E,
    daySummary.Vitamin_B1,
    daySummary.Vitamin_B2,
    daySummary.Vitamin_B3,
    daySummary.Vitamin_B5,
    daySummary.Vitamin_B6,
    daySummary.Vitamin_B7,
    daySummary.Vitamin_B8,
    daySummary.Vitamin_B9,
    daySummary.Vitamin_B12
FROM
    CARB_CYCLING_DAY carbCyclingDay
INNER JOIN
    MEAL_PLAN_DAY_NUTRITIONAL_SUMMARY daySummary ON carbCyclingDay.DAY_ID = daySummary.DAY_ID;


-- Find the nutritional summary of recipes
-- As with Complex Foods it is invalid / not possible to have a recipe without any ingredients
CREATE VIEW IF NOT EXISTS Recipe_Nutritional_Summary (
    Recipe_Id,
    Calories,
    Protein,
    Carbohydrates,
    Total_Fats,
    Trans_Fat,
    Monounsaturated_Fat,
    Polyunsaturated_Fat,
    Saturated_Fat,
    Sugars,
    Fiber,
    Cholesterol,
    Iron,
    Manganese,
    Copper,
    Zinc,
    Calcium,
    Magnesium,
    Phosphorus,
    Potassium,
    Sodium,
    Sulfur,
    Vitamin_A,
    Vitamin_C,
    Vitamin_K,
    Vitamin_D,
    Vitamin_E,
    Vitamin_B1,
    Vitamin_B2,
    Vitamin_B3,
    Vitamin_B5,
    Vitamin_B6,
    Vitamin_B7,
    Vitamin_B8,
    Vitamin_B9,
    Vitamin_B12) AS
SELECT
    recipe.RECIPE_ID,
    ROUND(SUM(ingredientNutrionalDetails.CALORIES * ingredient.NUMBER_OF_SERVINGS), 2) AS Calories,
    ROUND(SUM(ingredientNutrionalDetails.PROTEIN * ingredient.NUMBER_OF_SERVINGS), 2) AS Protein,
    ROUND(SUM(ingredientNutrionalDetails.CARBOHYDRATES * ingredient.NUMBER_OF_SERVINGS), 2) AS Carbohydrates,
    ROUND(SUM(ingredientNutrionalDetails.TOTAL_FATS * ingredient.NUMBER_OF_SERVINGS), 2) AS Total_Fats,
    ROUND(SUM(ingredientNutrionalDetails.TRANS_FAT * ingredient.NUMBER_OF_SERVINGS), 2) AS Trans_Fat,
    ROUND(SUM(ingredientNutrionalDetails.MONOUNSATURATED_FAT * ingredient.NUMBER_OF_SERVINGS), 2) AS Monounsaturated_Fat,
    ROUND(SUM(ingredientNutrionalDetails.POLYUNSATURATED_FAT * ingredient.NUMBER_OF_SERVINGS), 2) AS Polyunsaturated_Fat,
    ROUND(SUM(ingredientNutrionalDetails.SATURATED_FAT * ingredient.NUMBER_OF_SERVINGS), 2) AS Saturated_Fat,
    ROUND(SUM(ingredientNutrionalDetails.SUGARS * ingredient.NUMBER_OF_SERVINGS), 2) AS Sugars,
    ROUND(SUM(ingredientNutrionalDetails.FIBER * ingredient.NUMBER_OF_SERVINGS), 2) AS Fiber,
    ROUND(SUM(ingredientNutrionalDetails.CHOLESTEROL * ingredient.NUMBER_OF_SERVINGS), 2) AS Cholesterol,
    ROUND(SUM(ingredientNutrionalDetails.IRON * ingredient.NUMBER_OF_SERVINGS), 2) AS Iron,
    ROUND(SUM(ingredientNutrionalDetails.MANGANESE * ingredient.NUMBER_OF_SERVINGS), 2) AS Manganese,
    ROUND(SUM(ingredientNutrionalDetails.COPPER * ingredient.NUMBER_OF_SERVINGS), 2) AS Copper,
    ROUND(SUM(ingredientNutrionalDetails.ZINC * ingredient.NUMBER_OF_SERVINGS), 2) AS Zinc,
    ROUND(SUM(ingredientNutrionalDetails.CALCIUM * ingredient.NUMBER_OF_SERVINGS), 2) AS Calcium,
    ROUND(SUM(ingredientNutrionalDetails.MAGNESIUM * ingredient.NUMBER_OF_SERVINGS), 2) AS Magnesium,
    ROUND(SUM(ingredientNutrionalDetails.PHOSPHORUS * ingredient.NUMBER_OF_SERVINGS), 2) AS Phosphorus,
    ROUND(SUM(ingredientNutrionalDetails.POTASSIUM * ingredient.NUMBER_OF_SERVINGS), 2) AS Potassium,
    ROUND(SUM(ingredientNutrionalDetails.SODIUM * ingredient.NUMBER_OF_SERVINGS), 2) AS Sodium,
    ROUND(SUM(ingredientNutrionalDetails.SULFUR * ingredient.NUMBER_OF_SERVINGS), 2) AS Sulfur,
    ROUND(SUM(ingredientNutrionalDetails.VITAMIN_A * ingredient.NUMBER_OF_SERVINGS), 2) AS Vitamin_A,
    ROUND(SUM(ingredientNutrionalDetails.VITAMIN_C * ingredient.NUMBER_OF_SERVINGS), 2) AS Vitamin_C,
    ROUND(SUM(ingredientNutrionalDetails.VITAMIN_K * ingredient.NUMBER_OF_SERVINGS), 2) AS Vitamin_K,
    ROUND(SUM(ingredientNutrionalDetails.VITAMIN_D * ingredient.NUMBER_OF_SERVINGS), 2) AS Vitamin_D,
    ROUND(SUM(ingredientNutrionalDetails.VITAMIN_E * ingredient.NUMBER_OF_SERVINGS), 2) AS Vitamin_E,
    ROUND(SUM(ingredientNutrionalDetails.VITAMIN_B1 * ingredient.NUMBER_OF_SERVINGS), 2) AS Vitamin_B1,
    ROUND(SUM(ingredientNutrionalDetails.VITAMIN_B2 * ingredient.NUMBER_OF_SERVINGS), 2) AS Vitamin_B2,
    ROUND(SUM(ingredientNutrionalDetails.VITAMIN_B3 * ingredient.NUMBER_OF_SERVINGS), 2) AS Vitamin_B3,
    ROUND(SUM(ingredientNutrionalDetails.VITAMIN_B5 * ingredient.NUMBER_OF_SERVINGS), 2) AS Vitamin_B5,
    ROUND(SUM(ingredientNutrionalDetails.VITAMIN_B6 * ingredient.NUMBER_OF_SERVINGS), 2) AS Vitamin_B6,
    ROUND(SUM(ingredientNutrionalDetails.VITAMIN_B7 * ingredient.NUMBER_OF_SERVINGS), 2) AS Vitamin_B7,
    ROUND(SUM(ingredientNutrionalDetails.VITAMIN_B8 * ingredient.NUMBER_OF_SERVINGS), 2) AS Vitamin_B8,
    ROUND(SUM(ingredientNutrionalDetails.VITAMIN_B9 * ingredient.NUMBER_OF_SERVINGS), 2) AS Vitamin_B9,
    ROUND(SUM(ingredientNutrionalDetails.VITAMIN_B12 * ingredient.NUMBER_OF_SERVINGS), 2) AS Vitamin_B12
FROM
    RECIPE recipe
INNER JOIN
    RECIPE_INGREDIENT ingredient ON recipe.RECIPE_ID = ingredient.RECIPE_ID
INNER JOIN
    ALL_FOOD_NUTRITIONAL_DETAILS ingredientNutrionalDetails ON ingredientNutrionalDetails.FOOD_ID = ingredient.FOOD_ID
GROUP BY
    ingredient.RECIPE_ID;