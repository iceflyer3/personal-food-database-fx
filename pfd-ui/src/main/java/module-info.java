/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


module pfd.ui {
    requires java.sql;
    requires com.google.common;
    requires org.slf4j;
    requires com.calendarfx.view;

    requires javafx.fxml;
    requires javafx.controls;

    requires spring.context;
    requires spring.core;
    requires spring.beans;
    requires spring.orm;
    requires spring.tx;
    requires spring.jdbc;
    requires spring.boot;
    requires spring.boot.autoconfigure;

    // Required to be able to execute database migrations with Flyway
    requires org.flywaydb.core;

    // Needed for the FailureAnalyzer
    requires com.h2database;

    requires pfd.base;
    requires pfd.data;
    requires pfd.ui.model.api;
    requires pfd.ui.model.proxy;
    requires pfd.ui.model.viewmodel;

    opens com.iceflyer3.pfd.config;
    opens com.iceflyer3.pfd.config.analyzer;

    opens com.iceflyer3.pfd.ui;
    opens com.iceflyer3.pfd.ui.filter;
    opens com.iceflyer3.pfd.ui.manager;
    opens com.iceflyer3.pfd.ui.util;
    opens com.iceflyer3.pfd.ui.util.builder;
    opens com.iceflyer3.pfd.ui.controller;
    opens com.iceflyer3.pfd.ui.controller.about;
    opens com.iceflyer3.pfd.ui.controller.dialog;
    opens com.iceflyer3.pfd.ui.controller.settings;
    opens com.iceflyer3.pfd.ui.controller.steppable;
    opens com.iceflyer3.pfd.ui.controller.foods;
    opens com.iceflyer3.pfd.ui.controller.recipes;
    opens com.iceflyer3.pfd.ui.controller.mealplanning;
    opens com.iceflyer3.pfd.ui.controller.mealplanning.carbcycling;
    opens com.iceflyer3.pfd.ui.widget.stepper.series;

    /*
     * Allow reflective access to module resources. This is necessary so that classes in
     * 3rd party libraries can access them. These resources are mostly accessed by classes
     * in said 3rd party libraries like JavaFx and Spring.
     */
    opens fonts;
    opens fxml;
    opens license;
    opens styles;

    exports com.iceflyer3.pfd.config to spring.core;
    exports com.iceflyer3.pfd.config.runner to spring.beans;

    exports com.iceflyer3.pfd.ui to spring.beans;
    exports com.iceflyer3.pfd.ui.manager to spring.beans, pfd.base;
    exports com.iceflyer3.pfd.ui.util to spring.beans;
    exports com.iceflyer3.pfd.ui.util.builder to spring.beans;
    exports com.iceflyer3.pfd.ui.widget.builder to spring.beans;
    exports com.iceflyer3.pfd.ui.controller to spring.beans;
    exports com.iceflyer3.pfd.ui.controller.about to spring.beans;
    exports com.iceflyer3.pfd.ui.controller.dialog to spring.beans;
    exports com.iceflyer3.pfd.ui.controller.settings to spring.beans;
    exports com.iceflyer3.pfd.ui.controller.foods to spring.beans;
    exports com.iceflyer3.pfd.ui.controller.recipes to spring.beans;
    exports com.iceflyer3.pfd.ui.controller.mealplanning to spring.beans;
    exports com.iceflyer3.pfd.ui.controller.mealplanning.carbcycling to spring.beans;

    exports com.iceflyer3.pfd to javafx.graphics;
    exports com.iceflyer3.pfd.ui.controller.steppable to spring.beans;
}