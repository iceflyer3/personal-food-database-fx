package com.iceflyer3.pfd.config.analyzer;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.h2.jdbc.JdbcSQLNonTransientConnectionException;
import org.springframework.boot.diagnostics.AbstractFailureAnalyzer;
import org.springframework.boot.diagnostics.FailureAnalysis;

public class H2DatabaseConnectionFailureAnalyzer extends AbstractFailureAnalyzer<JdbcSQLNonTransientConnectionException>
{
    private final static Logger LOG = LoggerFactory.getLogger(H2DatabaseConnectionFailureAnalyzer.class);

    @Override
    protected FailureAnalysis analyze(Throwable rootFailure, JdbcSQLNonTransientConnectionException cause)
    {
        LOG.error("Failed to establish a connection with the embedded H2 database.", rootFailure);
        String action = "Ensure the database is not already in use. Then review the additional details of the error above for a clue as to what went wrong.";
        return new FailureAnalysis(getAnalysisDescription(cause), action, cause);
    }

    private String getAnalysisDescription(JdbcSQLNonTransientConnectionException originalCause)
    {
        StringBuilder descriptionBuilder = new StringBuilder("Failed to connect to the embedded H2 database.\n\n");
        descriptionBuilder.append("---Additional details---");

        Throwable cause = originalCause;
        while(cause != null)
        {
            descriptionBuilder.append(String.format("\n%s", cause.getMessage()));
            cause = cause.getCause();
        }

        return descriptionBuilder.toString();
    }
}
