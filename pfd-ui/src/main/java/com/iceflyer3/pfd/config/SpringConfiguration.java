/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.config;

import com.google.common.eventbus.EventBus;
import com.iceflyer3.pfd.config.runner.VerifyDatabaseConnectivityApplicationRunner;
import com.iceflyer3.pfd.data.AppConfigUtils;
import com.iceflyer3.pfd.data.configurer.H2EmbeddedDatabaseConfigurer;
import com.iceflyer3.pfd.data.factory.UserTaskFactory;
import com.iceflyer3.pfd.data.repository.UserRepository;
import com.iceflyer3.pfd.ui.controller.ProfileSelectController;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import com.iceflyer3.pfd.ui.model.proxy.food.FoodProxyFactory;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.MealPlanningProxyFactory;
import com.iceflyer3.pfd.ui.model.proxy.recipe.RecipeProxyFactory;
import com.iceflyer3.pfd.ui.util.ControllerUtils;
import com.iceflyer3.pfd.ui.widget.builder.WidgetNodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.flyway.FlywayAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.task.TaskExecutor;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseFactory;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.support.PersistenceAnnotationBeanPostProcessor;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@EnableTransactionManagement
@PropertySource("classpath:appConfig.properties")
@SpringBootApplication(
        scanBasePackageClasses = {
                VerifyDatabaseConnectivityApplicationRunner.class,
                AppConfigUtils.class,
                ProfileSelectController.class,
                ControllerUtils.class,
                WindowManager.class,
                WidgetNodeBuilder.class,
                UserTaskFactory.class,
                UserRepository.class,
                MealPlanningProxyFactory.class,
                FoodProxyFactory.class,
                RecipeProxyFactory.class
        },
        // We're only doing simple migrations which is simple enough to set up.
        // So, we don't need Spring Boot to configure this for us.
        exclude = FlywayAutoConfiguration.class)
public class SpringConfiguration
{
    private static final Logger LOG = LoggerFactory.getLogger(SpringConfiguration.class);

    @Bean
    public EventBus eventBus()
    {
        return new EventBus();
    }

    @Bean
    public TaskExecutor taskExecutor()
    {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(5);
        taskExecutor.setMaxPoolSize(10);
        taskExecutor.setQueueCapacity(10);
        return taskExecutor;
    }

    @Bean
    public DataSource dataSource(AppConfigUtils appConfigUtils)
    {
        /*
         * Configure the embedded H2 database. This consists of the following steps:
         *
         * 1. Create a new instance of our custom H2EmbeddedDatabaseConfigurer class
         *
         * 2. Create a new instance of EmbeddedDatabaseFactory and set the configurer
         *     to be our custom H2 configurer.
         *
         * 3. Create a new instance of EmbeddedDatabase (which extends DataSource)
         *     by using the EmbeddedDatabaseFactory and calling getDatabase().
         *
         * The H2 configurer does the database configuration via it's url
         * which is read in from appConfig.properties. This includes defining the
         * name and startup scripts for the database. While Spring offers its own
         * way to do this H2 has it built in, so we just use that.
         */
        LOG.debug("Initializing embedded database data source...");

        H2EmbeddedDatabaseConfigurer dbConfigurer = new H2EmbeddedDatabaseConfigurer(appConfigUtils);
        EmbeddedDatabaseFactory dbFactory = new EmbeddedDatabaseFactory();
        dbFactory.setDatabaseConfigurer(dbConfigurer);

        return dbFactory.getDatabase();
    }

    @Bean
    public JpaVendorAdapter jpaVendorAdapter()
    {
        /*
         * To be honest I'm not entirely clear on what the distinction between this and configuring
         * Hibernate vendor properties via a Properties object is supposed to be.
         *
         * But if this doesn't support any of the vendor properties we care about we can always
         * just take that approach and set them on the EntityManager via setJpaProperties.
         */
        HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
        adapter.setDatabase(Database.H2);


        // TODO: Turn this off and configure loggers for Hibernate instead via the log4j2 config
        // Show generated SQL in the logs
        adapter.setShowSql(true);

        // Generate the DB schema
        adapter.setGenerateDdl(false);

        return adapter;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource, JpaVendorAdapter jpaVendorAdapter)
    {
        /*
         * This is needed to configure the JPA Entity Manager Factory that we will use to get an Entity Manager via injection from Spring.
         *
         * Per the docs: "FactoryBean that creates a JPA EntityManagerFactory according to JPA's standard container bootstrap contract"
         *
         * We're using the container based variation instead of the non-container based variation despite not being deployed in a J2EE
         * container because, as noted in the docs for LocalEntityManagerFactoryBean, the container variant is less limited and more flexible.
         * In this instance allowing us to pass in a Spring managed JDBC DataSource to our JPA provider.
         *
         * https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/orm/jpa/LocalContainerEntityManagerFactoryBean.html
         */
        LOG.debug("Initializing Spring JPA LocalContainerEntityManagerFactoryBean...");
        Properties hibernateProperties = new Properties();
        hibernateProperties.setProperty("hibernate.default_schema", "PFD_V1");

        /*
         * We have some views in the database that will query tables to provide us with a
         * particular view of some entity (or entities) of the application that is of interest
         * to us.
         *
         * These views are mapped to their own Hibernate entities. There are times when we
         * expect data to be present for these views based on other actions we have taken
         * upon the persistence context (such as persisting new records, for example).
         *
         * Because these views have their own distinct entities, and are not the entities that
         * we are actually changing, Hibernate (and more specifically a FlushMode of AUTO) will
         * not always flush changes to the database before we attempt to make use of the data
         * (via these views) that we have indicated to be placed or changed within the database.
         *
         * For this reason we use a FlushMode of ALWAYS. Which ensures that any pending changes
         * in the persistence context will be flushed and applied to the database before any
         * query executes against it.
         *
         * For more on the various FlushModes available to us and what they do see:
         * https://docs.jboss.org/hibernate/orm/5.6/userguide/html_single/Hibernate_User_Guide.html#flushing
         *
         * Namely, you may see the criteria for how Hibernate decides when to flush for
         * a FlushMode of AUTO. For an example of why this mode doesn't work here you
         * should then consider any of the Nutritional Summary views alongside those
         * criteria.
         */
        hibernateProperties.setProperty("org.hibernate.flushMode", "ALWAYS");

        LocalContainerEntityManagerFactoryBean entityManager = new LocalContainerEntityManagerFactoryBean();
        entityManager.setDataSource(dataSource);
        entityManager.setJpaVendorAdapter(jpaVendorAdapter);
        entityManager.setPackagesToScan("com.iceflyer3.pfd.data.entities");
        entityManager.setJpaProperties(hibernateProperties);

        return entityManager;
    }

    @Bean
    public PlatformTransactionManager transactionManager(LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean)
    {
        /*
         * Needed to be able to use @Transactional.
         *
         * Per the docs: "This is the central interface in Spring's imperative transaction infrastructure"
         *
         * See https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/transaction/PlatformTransactionManager.html
         * or, for our specific transaction manager in use here,
         * https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/orm/jpa/JpaTransactionManager.html
         */
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(localContainerEntityManagerFactoryBean.getObject());
        return transactionManager;
    }

    @Bean
    public PersistenceAnnotationBeanPostProcessor paPostProcessor() {
        /*
         * Needed to be able to use @PersistenceUnit and @PersistenceContext annotations
         *
         * Per the docs: "BeanPostProcessor that processes PersistenceUnit and
         *                PersistenceContext annotations, for injection of the corresponding
         *                JPA resources EntityManagerFactory and EntityManager"
         *
         * https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/orm/jpa/support/PersistenceAnnotationBeanPostProcessor.html
         */
        return new PersistenceAnnotationBeanPostProcessor();
    }
}
