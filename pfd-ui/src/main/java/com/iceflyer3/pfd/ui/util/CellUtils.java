package com.iceflyer3.pfd.ui.util;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.ui.cell.ActionButtonProvider;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;

import java.util.List;

public class CellUtils {

    /**
     * Utility function for getting an HBox containing the buttons as declared by an
     * ActionButtonProvider for a cell.
     *
     * @param buttonProvider The proivder used to generate the buttons
     * @param rowItem The item for the cell
     * @param <T> The type of data contained in the cell to which these buttons will belong
     * @return An HBox with the buttons defined by the provider
     */
    public static <T> HBox getButtonsForButtonProvider(ActionButtonProvider<T> buttonProvider, T rowItem)
    {
        HBox buttonContainer = new HBox();
        buttonContainer.setPadding(new Insets(2, 2, 2, 2));
        List<Button> buttons = buttonProvider.get(rowItem);
        buttons.forEach(button -> {
            button.setUserData(rowItem);
            HBox.setMargin(button, new Insets(0, 2, 0, 2));
            buttonContainer.getChildren().addAll(button);
        });

        return buttonContainer;
    }
}
