/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023, 2025  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.util;

import com.iceflyer3.pfd.PfdApplication;
import com.iceflyer3.pfd.exception.InvalidApplicationStateException;
import com.iceflyer3.pfd.ui.enums.ScreenType;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class that serves as a container for a collection of
 * convenience methods for FXML controller related operations
 */
@Component
public class ControllerUtils implements ApplicationContextAware {
    private final static Logger LOG = LoggerFactory.getLogger(ControllerUtils.class);

    private final Pattern regexPattern;
    private ApplicationContext applicationContext;
    private WindowManager windowManager;

    public ControllerUtils()
    {
        this.regexPattern = Pattern.compile("[A-Z][a-z]+");
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
        this.windowManager = applicationContext.getBean(WindowManager.class);
    }

    public <T> FxmlLoadResult<T> loadFxml(String setToLoad)
    {
        return getLoadResult(setToLoad, ScreenType.APPLICATION);
    }

    public <T> FxmlLoadResult<T> loadDialogFxml(String dialogSetToLoad)
    {
        return getLoadResult(dialogSetToLoad, ScreenType.DIALOG);
    }

    public <T> FxmlLoadResult<T> loadSteppableFxml(String steppableSetToLoad)
    {
        return getLoadResult(steppableSetToLoad, ScreenType.STEPPER);
    }

    public String parseFxmlFileNameFromControllerClass(Class<?> controllerClass)
    {
        StringBuilder fileNameBuilder = new StringBuilder();
        Matcher regexMatcher = regexPattern.matcher(controllerClass.getSimpleName());

        while(regexMatcher.find())
        {
            if (!regexMatcher.group().equals("Controller"))
            {
                fileNameBuilder.append(regexMatcher.group().toLowerCase());
                fileNameBuilder.append("-");
            }
            else
            {
                // If the last match was "Controller" then the previous match was the last part of the filename, so remove the trailing dash.
                fileNameBuilder.deleteCharAt(fileNameBuilder.length() -1);
            }
        }

        return fileNameBuilder.toString();
    }

    // TODO: Bigger change than I wish to make at the moment but we can probably refactor to just using this function
    //       instead of the several "loadFooFxml" functions above. Let's revaluate this idea in the future.
    private <T> FxmlLoadResult<T> getLoadResult(String setToLoad, ScreenType screenType)
    {
        try
        {
            String urlRoot;
            switch(screenType)
            {
                case APPLICATION -> urlRoot = "/fxml";
                case DIALOG -> urlRoot = "/fxml/dialog";
                case STEPPER -> urlRoot = "/fxml/steppable";
                default -> throw new InvalidApplicationStateException("Attempted to load FXML for an unsupported screen type");
            }

            String fxmlFullUrl = String.format("%s/%s.fxml", urlRoot, setToLoad);
            LOG.debug("Attempting to load the fxml file located at {}", fxmlFullUrl);
            FXMLLoader fxmlLoader = new FXMLLoader(PfdApplication.class.getResource(fxmlFullUrl));
            fxmlLoader.setControllerFactory(applicationContext::getBean);

            Parent root = fxmlLoader.load();
            T controller = fxmlLoader.getController();

            return new FxmlLoadResult<>(root, controller);
        }
        catch (Exception e)
        {
            InvalidApplicationStateException wrapped = new InvalidApplicationStateException(e);
            LOG.error("An error occurred while loading the controller", wrapped);
            windowManager.openFatalErrorDialog();
            throw wrapped;
        }
    }
}
