package com.iceflyer3.pfd.ui.controller;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.iceflyer3.pfd.ui.Breadcrumb;
import com.iceflyer3.pfd.ui.event.ShowNotificationEvent;
import com.iceflyer3.pfd.ui.listener.Destroyable;
import com.iceflyer3.pfd.ui.listener.ObservableListListenerBinding;
import com.iceflyer3.pfd.ui.manager.BreadcrumbManager;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.util.ControllerUtils;
import com.iceflyer3.pfd.ui.util.NotificationUtils;
import javafx.fxml.FXML;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.controlsfx.control.NotificationPane;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PopoutRootController extends AbstractHomeController implements Destroyable
{
    private final static Logger LOG = LoggerFactory.getLogger(PopoutRootController.class);

    public final static String DATA_SECTION_TO_OPEN = "sectionToOpen";

    private final EventBus eventBus;

    @FXML
    public NotificationPane notificationPane;

    private NotificationUtils notificationUtils;
    private ObservableListListenerBinding<Breadcrumb> breadcrumbListenerBinding;

    @FXML
    private StackPane popoutRootContainer;

    @FXML
    private HBox breadcrumbsHbox;

    public PopoutRootController(EventBus eventBus, BreadcrumbManager breadcrumbManager, WindowManager windowManager, ControllerUtils controllerUtils)
    {
        super(eventBus, breadcrumbManager, windowManager, controllerUtils);
        this.eventBus = eventBus;
        this.eventBus.register(this);
    }

    @Override
    protected void registerDefaultListeners() { }

    @Override
    public void initialize() {
        initRootContainer(popoutRootContainer);
        notificationUtils = new NotificationUtils(notificationPane);
        notificationUtils.initPaneDefaultSettings();
        breadcrumbListenerBinding = new ObservableListListenerBinding<>(breadcrumbs);
        breadcrumbListenerBinding.bindChangeListener(change -> breadcrumbManager.updateBreadcrumbBar(change, breadcrumbsHbox, user));
    }

    @Override
    public void destroy()
    {
        this.eventBus.unregister(this);
        breadcrumbListenerBinding.unbind();
        timer.cancel();
    }

    @Override
    public void initControllerData(Map<String, Object> controllerData) {
        // The name of the application section that should be loaded in this popout
        String sectionToOpen = (String) controllerData.get(DATA_SECTION_TO_OPEN);
        navigateToAppSection(sectionToOpen);
    }

    @Override
    public void initUser(UserViewModel user)
    {
        this.user = user;
    }

    @Subscribe
    protected void handleNotificationRequest(ShowNotificationEvent event)
    {
        LOG.debug("Received request to show notification: {}", event);
        if (event.getForStage().equals(hostingStageName))
        {
            notificationUtils.showNotification(event);
        }
    }
}
