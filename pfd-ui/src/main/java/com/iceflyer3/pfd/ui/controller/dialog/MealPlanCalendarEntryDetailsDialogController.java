/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.controller.dialog;

import com.iceflyer3.pfd.ui.controller.sections.MacronutrientBreakdownScreenSection;
import com.iceflyer3.pfd.ui.controller.sections.MicronutrientBreakdownScreenSection;
import com.iceflyer3.pfd.ui.controller.sections.ScreenSection;
import com.iceflyer3.pfd.enums.WeeklyInterval;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.MealPlanCalendarDayModelProxy;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class MealPlanCalendarEntryDetailsDialogController extends AbstractDialogController<Void>
{
    public static final String DATA_CALENDAR_DAY = "mealPlanCalendarDay";

    @FXML
    public VBox rootContainer;

    @FXML
    public Label plannedDayLabelText;

    @FXML
    public Label entryDateText;

    @FXML
    public Label recurrenceIntervalText;

    @FXML
    public Button closeButton;

    public MealPlanCalendarEntryDetailsDialogController(WindowManager windowManager)
    {
        super(windowManager);
    }

    @Override
    public void initialize()
    {

    }

    @Override
    protected void registerDefaultListeners()
    {
        closeButton.setOnAction(event -> windowManager.close(WindowManager.DIALOG_WINDOW));
    }

    @Override
    public void initControllerData(Map<String, Object> controllerData)
    {
        MealPlanCalendarDayModelProxy mealPlanCalendarDayProxy = (MealPlanCalendarDayModelProxy) controllerData.get(DATA_CALENDAR_DAY);

        plannedDayLabelText.setText(mealPlanCalendarDayProxy.getPlannedDay().getDayLabel());
        entryDateText.setText(mealPlanCalendarDayProxy.getCalendarDate().toString());

        if (mealPlanCalendarDayProxy.getWeeklyInterval() == WeeklyInterval.NONE)
        {
            recurrenceIntervalText.setText("None");
        }
        else
        {
            recurrenceIntervalText.setText(String.format("%s for %s month(s)", mealPlanCalendarDayProxy.getWeeklyInterval().getName(), mealPlanCalendarDayProxy.getIntervalMonthsDuration()));
        }

        ScreenSection macroBreakdownSection = new MacronutrientBreakdownScreenSection<>(mealPlanCalendarDayProxy.getPlannedDay());
        rootContainer.getChildren().add(2, macroBreakdownSection.create());

        ScreenSection microBreakdownSection = new MicronutrientBreakdownScreenSection<>(mealPlanCalendarDayProxy.getPlannedDay());
        rootContainer.getChildren().add(3, microBreakdownSection.create());
    }
}
