package com.iceflyer3.pfd.ui.controller.dialog;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.ui.manager.WindowManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import org.springframework.stereotype.Component;

@Component
public class ConfirmActionDialogController extends AbstractDialogController<Boolean>{

    @FXML
    public Button confirmButton;

    @FXML
    public Hyperlink cancelButton;

    public ConfirmActionDialogController(WindowManager windowManager) {
        super(windowManager);
    }

    @Override
    public void initialize() {

    }

    @Override
    protected void registerDefaultListeners() {
        confirmButton.setOnAction(this::confirm);
        cancelButton.setOnAction(this::cancel);
    }

    private void cancel(ActionEvent event) {
        callback.complete(false);
        windowManager.close(WindowManager.DIALOG_WINDOW);
    }

    private void confirm(ActionEvent event) {
        callback.complete(true);
        windowManager.close(WindowManager.DIALOG_WINDOW);
    }
}
