package com.iceflyer3.pfd.ui.controller.recipes;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.google.common.eventbus.EventBus;
import com.iceflyer3.pfd.constants.ApplicationSections;
import com.iceflyer3.pfd.constants.FontAwesomeIcons;
import com.iceflyer3.pfd.constants.SceneSets;
import com.iceflyer3.pfd.ui.event.PublishBreadcrumbEvent;
import com.iceflyer3.pfd.ui.event.ScreenNavigationEvent;
import com.iceflyer3.pfd.ui.cell.ServingSizeChoiceBoxTableCell;
import com.iceflyer3.pfd.ui.controller.AbstractTaskController;
import com.iceflyer3.pfd.ui.controller.HostStageAware;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import com.iceflyer3.pfd.ui.model.proxy.recipe.ReadOnlyRecipeModelProxy;
import com.iceflyer3.pfd.ui.model.proxy.recipe.RecipeProxyFactory;
import com.iceflyer3.pfd.ui.model.viewmodel.IngredientViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.ServingSizeViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.util.LayoutUtils;
import com.iceflyer3.pfd.ui.util.builder.ActionNodeBuilder;
import com.iceflyer3.pfd.ui.util.builder.NotificationBuilder;
import com.iceflyer3.pfd.ui.util.ScreenLoadEventUtils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.core.task.TaskExecutor;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ViewAllRecipesController extends AbstractTaskController implements HostStageAware {

    private final RecipeProxyFactory recipeProxyFactory;
    private final WindowManager windowManager;
    private final ObservableList<ReadOnlyRecipeModelProxy> recipeList;

    private UserViewModel user;
    private String hostingStageName;

    @FXML
    public BorderPane rootContainer;

    @FXML
    public AnchorPane buttonContainerAnchorPane;

    @FXML
    public Label screenLabel;

    @FXML
    public VBox tableViewContainer;

    public ViewAllRecipesController(EventBus eventBus,
                                    WindowManager windowManager,
                                    NotificationBuilder notificationBuilder,
                                    ScreenLoadEventUtils loadEventBuilder,
                                    TaskExecutor taskExecutor,
                                    RecipeProxyFactory recipeProxyFactory) {
        super(eventBus, notificationBuilder, loadEventBuilder, taskExecutor);
        this.recipeProxyFactory = recipeProxyFactory;
        this.windowManager = windowManager;
        this.recipeList = FXCollections.observableArrayList();
    }

    @Override
    public void initialize() {
        // TODO: We might want to look into taking this and the emitting of the breadcrumb event and putting them in a function in the base class.
        VBox.setMargin(screenLabel, LayoutUtils.getHeaderLabelMarginInsets());
    }

    @Override
    protected void registerDefaultListeners() {

    }

    @Override
    public void initControllerData(Map<String, Object> controllerData)
    {
        user = (UserViewModel) controllerData.get(UserViewModel.class.getName());

        refreshRecipes();
        setupTableView();
        setupButtons();
    }

    @Override
    public void initHostingStageName(String stageName) {
        hostingStageName = stageName;

        PublishBreadcrumbEvent landingScreenBreadcrumb = new PublishBreadcrumbEvent("View All Recipes", ApplicationSections.RECIPES, SceneSets.RECIPES_OVERVIEW, hostingStageName, 1);
        eventBus.post(landingScreenBreadcrumb);
    }

    private void setupButtons()
    {
        Button addRecipeButton = ActionNodeBuilder
                .createIconButton("NEW RECIPE",  FontAwesomeIcons.PLUS)
                .withAction(event -> {
                    // Navigate to edit day screen
                    navigateToComposeScreenForRecipe(null);
                })
                .build();

        AnchorPane.setLeftAnchor(addRecipeButton, 0.0);
        buttonContainerAnchorPane.getChildren().addAll(addRecipeButton);
    }

    private void setupTableView()
    {
        // Set up the table view itself
        TableView<ReadOnlyRecipeModelProxy> recipesTable = new TableView<>();
        recipesTable.setPlaceholder(new Label("No recipe data available"));


        TableColumn<ReadOnlyRecipeModelProxy, String> nameColumn = new TableColumn<>("Name");
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));

        TableColumn<ReadOnlyRecipeModelProxy, String> sourceColumn = new TableColumn<>("Source");
        sourceColumn.setCellValueFactory(new PropertyValueFactory<>("source"));

        TableColumn<ReadOnlyRecipeModelProxy, List<ServingSizeViewModel>> servingSizeColumn = new TableColumn<>("Serving Sizes");
        servingSizeColumn.setCellFactory(col -> new ServingSizeChoiceBoxTableCell<>());
        servingSizeColumn.setCellValueFactory(new PropertyValueFactory<>("servingSizes"));

        TableColumn<ReadOnlyRecipeModelProxy, List<IngredientViewModel>> totalCaloriesColumn = new TableColumn<>("Calories");
        totalCaloriesColumn.setCellValueFactory(new PropertyValueFactory<>("calories"));

        TableColumn<ReadOnlyRecipeModelProxy, List<IngredientViewModel>> totalProteinColumn = new TableColumn<>("Protein");
        totalProteinColumn.setCellValueFactory(new PropertyValueFactory<>("protein"));

        TableColumn<ReadOnlyRecipeModelProxy, List<IngredientViewModel>> totalCarbsColumn = new TableColumn<>("Carbs");
        totalCarbsColumn.setCellValueFactory(new PropertyValueFactory<>("carbohydrates"));

        TableColumn<ReadOnlyRecipeModelProxy, List<IngredientViewModel>> totalFatsColumn = new TableColumn<>("Fats");
        totalFatsColumn.setCellValueFactory(new PropertyValueFactory<>("totalFats"));

        recipesTable.getColumns().addAll(nameColumn, sourceColumn, servingSizeColumn, totalCaloriesColumn, totalProteinColumn, totalCarbsColumn, totalFatsColumn);
        recipesTable.setItems(recipeList);

        // Set up the context menu for the tableview
        EventHandler<ActionEvent> viewActionHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ReadOnlyRecipeModelProxy selectedRecipe = recipesTable.getSelectionModel().getSelectedItem();
                ScreenNavigationEvent navigationEvent = new ScreenNavigationEvent(ApplicationSections.RECIPES, SceneSets.VIEW_RECIPE, hostingStageName, user);
                navigationEvent.getControllerData().put(ViewRecipeController.DATA_RECIPE, selectedRecipe);
                eventBus.post(navigationEvent);
            }
        };

        EventHandler<ActionEvent> editActionHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ReadOnlyRecipeModelProxy selectedRecipe = recipesTable.getSelectionModel().getSelectedItem();
                navigateToComposeScreenForRecipe(selectedRecipe);
            }
        };

        EventHandler<ActionEvent> deleteActionHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                // Show pop-up dialog to confirm deletion
                windowManager.openConfirmationDialog(rootContainer.getScene().getWindow(), () -> {
                    notificationBuilder.create("Deleting recipe...", hostingStageName).show();
                    ReadOnlyRecipeModelProxy selectedRecipe = recipesTable.getSelectionModel().getSelectedItem();
                    selectedRecipe.delete((wasSuccessful, resultMessage) -> {
                        notificationBuilder.create(resultMessage, hostingStageName).show();

                        if (wasSuccessful)
                        {
                            refreshRecipes();
                        }
                    });
                });
            }
        };

        ContextMenu contextMenu = LayoutUtils.getDefaultContextMenu(viewActionHandler, editActionHandler, deleteActionHandler);
        recipesTable.setContextMenu(contextMenu);

        tableViewContainer.getChildren().add(recipesTable);
    }

    private void navigateToComposeScreenForRecipe(@Nullable ReadOnlyRecipeModelProxy selectedRecipe)
    {
        ScreenNavigationEvent navigationEvent = new ScreenNavigationEvent(ApplicationSections.RECIPES, SceneSets.COMPOSE_RECIPE, hostingStageName, user);

        // If there was no selected recipe then we aren't editing an existing recipe.
        if (selectedRecipe != null)
        {
            navigationEvent.getControllerData().put(ComposeRecipeController.DATA_RECIPE_ID, selectedRecipe.getId());
        }

        eventBus.post(navigationEvent);
    }

    private void refreshRecipes()
    {
        loadEventBuilder.emitBegin("Searching for recipes", hostingStageName);
        recipeProxyFactory.getAllRecipesForUser(user.getUserId(), (wasSuccessful, proxyResult) -> {
            if (wasSuccessful)
            {
                recipeList.clear();
                recipeList.addAll(proxyResult);
            }
            else
            {
                notificationBuilder.create("An error has occurred while retrieving the recipes", hostingStageName).show();
            }
            loadEventBuilder.emitCompleted(hostingStageName);
        });
    }
}
