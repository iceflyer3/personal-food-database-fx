/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.controller.dialog;

import com.iceflyer3.pfd.constants.CssConstants;
import com.iceflyer3.pfd.data.request.mealplanning.GroceryListForMealPlanCalendarDateRangeSearchRequest;
import com.iceflyer3.pfd.ui.cell.CollectionChoiceBoxTableCell;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.api.ServingSizeModel;
import com.iceflyer3.pfd.ui.util.CalendarUtils;
import com.iceflyer3.pfd.util.NumberUtils;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MealPlanCalendarGroceryListDialog extends AbstractDialogController<Void>
{
    private final CalendarUtils calendarUtils;
    private final GroceryListForMealPlanCalendarDateRangeSearchRequest searchRequest;
    private final ObservableList<IngredientModel> groceryListResults;
    
    @FXML
    public DatePicker startDatePicker;

    @FXML
    public DatePicker endDatePicker;

    @FXML
    public Button searchButton;

    @FXML
    public Button closeButton;

    @FXML
    public CheckBox alternateIngredientsCheckbox;

    @FXML
    public CheckBox optionalIngredientsCheckbox;

    @FXML
    public Label searchErrorLabel;

    @FXML
    public VBox loadStatusContainer;

    @FXML
    public VBox mainContentContainer;

    @FXML
    public VBox resultsTableContainer;

    public MealPlanCalendarGroceryListDialog(WindowManager windowManager, CalendarUtils calendarUtils)
    {
        super(windowManager);
        this.calendarUtils = calendarUtils;

        groceryListResults = FXCollections.observableArrayList();
        searchRequest = new GroceryListForMealPlanCalendarDateRangeSearchRequest();
    }

    @Override
    public void initialize()
    {
        // Ignore the invisible container for the purposes of layout so it won't take up any space
        loadStatusContainer.managedProperty().bind(loadStatusContainer.visibleProperty());
        mainContentContainer.managedProperty().bind(mainContentContainer.visibleProperty());
    }

    @Override
    protected void registerDefaultListeners()
    {
        searchButton.setOnAction(event -> search());
        closeButton.setOnAction(event -> windowManager.close(WindowManager.DIALOG_WINDOW));

        Bindings.bindBidirectional(startDatePicker.valueProperty(), searchRequest.startDateProperty());
        Bindings.bindBidirectional(endDatePicker.valueProperty(), searchRequest.endDateProperty());
        Bindings.bindBidirectional(alternateIngredientsCheckbox.selectedProperty(), searchRequest.includeAlternateIngredientsProperty());
        Bindings.bindBidirectional(optionalIngredientsCheckbox.selectedProperty(), searchRequest.includeOptionalIngredientsProperty());
    }

    private void search()
    {
        toggleLoadingScreen(true);
        searchErrorLabel.setVisible(false);
        calendarUtils.getGroceryList(searchRequest, (wasSuccessful, groceryList) -> {
            if (wasSuccessful)
            {
                LOG.debug("Found {} items for the grocery list", groceryList.size());
                groceryListResults.clear();
                groceryListResults.addAll(groceryList);
                setupResultsView();
            }
            else
            {
                searchErrorLabel.setVisible(true);
            }
            toggleLoadingScreen(false);
        });
    }

    private void setupResultsView()
    {
        List<IngredientModel> requiredIngredients = groceryListResults
                .stream()
                .filter(ingredient -> !ingredient.isOptional() && !ingredient.isAlternate())
                .toList();
        LOG.debug("Found {} required ingredients", requiredIngredients.size());

        List<IngredientModel> optionalIngredients = groceryListResults
                .stream()
                .filter(IngredientModel::isOptional)
                .toList();

        List<IngredientModel> alternateIngredients = groceryListResults
                .stream()
                .filter(IngredientModel::isAlternate)
                .toList();

        String tableNoResultsPlaceholder = "No results found that match this criteria";

        resultsTableContainer.getChildren().clear();
        Label requiredIngredientsLabel = new Label("REQUIRED ITEMS");
        requiredIngredientsLabel.getStyleClass().add(CssConstants.CLASS_BOLD);
        TableView<IngredientModel> requiredIngredientsTable = createTableView();
        requiredIngredientsTable.setPlaceholder(new Label(tableNoResultsPlaceholder));
        requiredIngredientsTable.setItems(FXCollections.observableArrayList(requiredIngredients));
        resultsTableContainer.getChildren().addAll(requiredIngredientsLabel, requiredIngredientsTable);

        Label optionalIngredientsLabel = new Label("OPTIONAL ITEMS");
        optionalIngredientsLabel.getStyleClass().add(CssConstants.CLASS_BOLD);
        TableView<IngredientModel> optionalIngredientsTable = createTableView();
        optionalIngredientsTable.setPlaceholder(new Label(tableNoResultsPlaceholder));
        optionalIngredientsTable.setItems(FXCollections.observableArrayList(optionalIngredients));
        resultsTableContainer.getChildren().addAll(optionalIngredientsLabel, optionalIngredientsTable);

        Label alternateIngredientsLabel = new Label("ALTERNATE ITEMS");
        alternateIngredientsLabel.getStyleClass().add(CssConstants.CLASS_BOLD);
        TableView<IngredientModel> alternateIngredientsTable = createTableView();
        alternateIngredientsTable.setPlaceholder(new Label(tableNoResultsPlaceholder));
        alternateIngredientsTable.setItems(FXCollections.observableArrayList(alternateIngredients));
        resultsTableContainer.getChildren().addAll(alternateIngredientsLabel, alternateIngredientsTable);
    }

    private TableView<IngredientModel> createTableView()
    {
        TableView<IngredientModel> ingredientsTable = new TableView<>();

        TableColumn<IngredientModel, String> nameColumn = new TableColumn<>("Name");
        nameColumn.setCellValueFactory(cellDataFeatures -> cellDataFeatures.getValue().getIngredientFood().nameProperty());

        TableColumn<IngredientModel, BigDecimal> servingsColumn = new TableColumn<>("Total Servings");
        servingsColumn.setCellValueFactory(new PropertyValueFactory<>("servingsIncluded"));

        TableColumn<IngredientModel, Collection<ServingSizeModel>> quantityColumn = new TableColumn<>("Total Quantity Needed");
        quantityColumn.setCellFactory(tableColumn -> {
            Function<IngredientModel, Collection<ServingSizeModel>> collectionProvider = ingredient -> ingredient.getIngredientFood().getServingSizes();
            BiFunction<IngredientModel, ServingSizeModel, String> cellTextProvider = (ingredient, servingSize) -> {
                BigDecimal totalQuantity = NumberUtils.withStandardPrecision(ingredient.getServingsIncluded().multiply(servingSize.getServingSize()));
                return String.format("%s %s", totalQuantity.toPlainString(), servingSize.getUnitOfMeasure().getName());
            };

            return new CollectionChoiceBoxTableCell<>(collectionProvider, cellTextProvider);
        });
        quantityColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<IngredientModel, Collection<ServingSizeModel>>, ObservableValue<Collection<ServingSizeModel>>>()
        {
            @Override
            public ObservableValue<Collection<ServingSizeModel>> call(TableColumn.CellDataFeatures<IngredientModel, Collection<ServingSizeModel>> param)
            {
                List<ServingSizeModel> servingSizeViewModels = new ArrayList<>(param.getValue().getIngredientFood().getServingSizes());
                return new ReadOnlyObjectWrapper<>(servingSizeViewModels);
            }
        });

        ingredientsTable.getColumns().addAll(nameColumn, servingsColumn, quantityColumn);
        return ingredientsTable;
    }

    private void toggleLoadingScreen(boolean isLoading)
    {
        loadStatusContainer.setVisible(isLoading);
        mainContentContainer.setVisible(!isLoading);
    }

}
