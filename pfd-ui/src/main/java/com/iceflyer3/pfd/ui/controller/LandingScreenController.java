/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.controller;

import com.google.common.eventbus.EventBus;
import com.iceflyer3.pfd.constants.ApplicationSections;
import com.iceflyer3.pfd.constants.CssConstants;
import com.iceflyer3.pfd.constants.SceneSets;
import com.iceflyer3.pfd.ui.event.PublishBreadcrumbEvent;
import com.iceflyer3.pfd.ui.util.LayoutUtils;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class LandingScreenController extends AbstractController implements HostStageAware {

    private static final Logger LOG = LoggerFactory.getLogger(LandingScreenController.class);

    private final EventBus eventBus;

    @FXML
    public VBox rootVbox;

    public LandingScreenController(EventBus eventBus)
    {
        this.eventBus = eventBus;
    }

    @Override
    public void initialize() {
        LOG.debug("LandingScreenController is initializing");

        Label headerLabel = new Label("Welcome to your Personal Food Database!");
        headerLabel.getStyleClass().add(CssConstants.CLASS_TEXT_HEADLINE);
        VBox.setMargin(headerLabel, LayoutUtils.getHeaderLabelMarginInsets());

        Label descriptionLabel = new Label("To begin using the application select an application section from the navigation on the left hand side.");
        VBox.setMargin(descriptionLabel, new Insets(5, 0 ,0, 0));

        rootVbox.getChildren().addAll(headerLabel, descriptionLabel);
    }

    @Override
    protected void registerDefaultListeners() {
        // Not used with this screen. Just a simple landing screen with no interactivity.
    }

    @Override
    public void initHostingStageName(String stageName) {
        PublishBreadcrumbEvent landingScreenBreadcrumb = new PublishBreadcrumbEvent("Home", ApplicationSections.HOME, SceneSets.LANDING_SCREEN, stageName, 0);
        eventBus.post(landingScreenBreadcrumb);
    }
}
