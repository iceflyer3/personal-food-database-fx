/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.controller.foods;

import com.google.common.eventbus.EventBus;
import com.iceflyer3.pfd.constants.ApplicationSections;
import com.iceflyer3.pfd.constants.FontAwesomeIcons;
import com.iceflyer3.pfd.constants.SceneSets;
import com.iceflyer3.pfd.enums.FoodType;
import com.iceflyer3.pfd.ui.event.PublishBreadcrumbEvent;
import com.iceflyer3.pfd.ui.event.ScreenNavigationEvent;
import com.iceflyer3.pfd.ui.cell.ServingSizeChoiceBoxTableCell;
import com.iceflyer3.pfd.ui.controller.AbstractTaskController;
import com.iceflyer3.pfd.ui.controller.HostStageAware;
import com.iceflyer3.pfd.ui.enums.PfdActionType;
import com.iceflyer3.pfd.ui.filter.FoodFilterRequest;
import com.iceflyer3.pfd.ui.filter.FoodFilterer;
import com.iceflyer3.pfd.ui.manager.DialogCallback;
import com.iceflyer3.pfd.ui.manager.OpenDialogRequest;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import com.iceflyer3.pfd.ui.model.proxy.food.FoodProxyFactory;
import com.iceflyer3.pfd.ui.model.proxy.food.ReadOnlyFoodModelProxy;
import com.iceflyer3.pfd.ui.model.viewmodel.FoodAsIngredientViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.ServingSizeViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.util.LayoutUtils;
import com.iceflyer3.pfd.ui.util.ScreenLoadEventUtils;
import com.iceflyer3.pfd.ui.util.TextUtils;
import com.iceflyer3.pfd.ui.util.builder.ActionNodeBuilder;
import com.iceflyer3.pfd.ui.util.builder.NotificationBuilder;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.core.task.TaskExecutor;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ViewFoodsController extends AbstractTaskController implements HostStageAware
{

    private static final Logger LOG = LoggerFactory.getLogger(ViewFoodsController.class);

    private final FoodProxyFactory foodProxyFactory;
    private final WindowManager windowManager;
    private final HashMap<String, BooleanProperty> columnVisibilityStatus;

    private final List<ReadOnlyFoodModelProxy> allFoods;
    private final ObservableList<ReadOnlyFoodModelProxy> filteredFoods;

    private UserViewModel user;
    private String hostingStageName;
    private FoodFilterRequest activeFilterRequest;

    @FXML
    public Label screenLabel;

    @FXML
    public BorderPane rootContainer;

    @FXML
    public VBox tableViewContainer;

    @FXML
    public AnchorPane buttonContainerAnchorPane;



    public ViewFoodsController(EventBus eventBus,
                               WindowManager windowManager,
                               NotificationBuilder notificationBuilder,
                               ScreenLoadEventUtils loadEventBuilder,
                               TaskExecutor taskExecutor,
                               FoodProxyFactory foodProxyFactory)
    {
        super(eventBus, notificationBuilder, loadEventBuilder, taskExecutor);
        this.foodProxyFactory = foodProxyFactory;
        this.windowManager = windowManager;
        this.columnVisibilityStatus = new HashMap<>();

        this.allFoods = new ArrayList<>();
        this.filteredFoods = FXCollections.observableArrayList();
    }

    @Override
    public void initialize() {
        LOG.debug("ViewFoodsController is initializing...");
        VBox.setMargin(screenLabel, LayoutUtils.getHeaderLabelMarginInsets());
    }

    @Override
    protected void registerDefaultListeners() {

    }

    @Override
    public void initControllerData(Map<String, Object> controllerData)
    {
        user = (UserViewModel) controllerData.get(UserViewModel.class.getName());
        setupButtons();
        setupTableView();
        refreshFoods();
    }

    @Override
    public void initHostingStageName(String stageName) {
        hostingStageName = stageName;

        PublishBreadcrumbEvent landingScreenBreadcrumb = new PublishBreadcrumbEvent("View Foods", ApplicationSections.FOOD_DATABASE, SceneSets.VIEW_FOODS, hostingStageName, 1);
        eventBus.post(landingScreenBreadcrumb);
    }

    private void navigateToAddFood(ActionEvent event)
    {
        DialogCallback<FoodType> cb = (FoodType foodType) -> {
            navigateToComposeScreenForFood(foodType, null);
        };

        this.windowManager.openDialog(new OpenDialogRequest<>(rootContainer.getScene().getWindow(), SceneSets.DIALOG_NEW_FOOD_TYPE, cb));
    }

    private void showTableOptions(ActionEvent actionEvent)
    {
        DialogCallback<FoodFilterRequest> callback = new DialogCallback<FoodFilterRequest>() {
            @Override
            public void complete(FoodFilterRequest dialogOutput) {
                /*
                 * Since java is pass by reference and we're using properties we don't have any work to do here
                 * as it pertains to the column visibilities. They will live update when changed upon change in
                 * the dialog (while it is still open even).
                 *
                 * However, we do have work to do with processing any user defined filters here.
                 */
                // If there are no filters then restore the full list of foods. Otherwise apply the filters.
                filteredFoods.clear();
                if (dialogOutput.getTextFilters().size() == 0 && dialogOutput.getNumericFilters().size() == 0)
                {
                    activeFilterRequest = null;
                }
                else
                {
                    activeFilterRequest = dialogOutput;
                }
                refreshFilter();
            }
        };

        OpenDialogRequest<FoodFilterRequest> request = new OpenDialogRequest<>(rootContainer.getScene().getWindow(), SceneSets.DIALOG_VIEW_FOODS_OPTIONS, callback);
        request.getDialogData().put(HashMap.class.getName(), this.columnVisibilityStatus);

        if (activeFilterRequest != null)
        {
            request.getDialogData().put(FoodFilterRequest.class.getName(), activeFilterRequest);
        }

        request.setDialogTitle("View Foods Table Options");
        request.setHeight(650);
        request.setWidth(500);

        windowManager.openDialog(request);
    }

    private void setupButtons()
    {
        Button addFoodsButton = ActionNodeBuilder
                .createIconButton("NEW FOOD",  FontAwesomeIcons.PLUS)
                .withAction(this::navigateToAddFood)
                .build();

        Button tableOptionsButton = ActionNodeBuilder
                .createIconButton("OPTIONS", FontAwesomeIcons.SLIDERS_H)
                .withAction(this::showTableOptions)
                .forActionType(PfdActionType.TERTIARY)
                .build();

        AnchorPane.setLeftAnchor(addFoodsButton, 0.0);
        AnchorPane.setRightAnchor(tableOptionsButton, 0.0);
        buttonContainerAnchorPane.getChildren().addAll(addFoodsButton, tableOptionsButton);
    }

    private void setupTableView()
    {
        // Setup the table
        TableView<ReadOnlyFoodModelProxy> foodsTable = new TableView<>();
        foodsTable.setPlaceholder(new Label("No foods found"));
        VBox.setVgrow(foodsTable, Priority.ALWAYS);

        // Setup the context menu for the table
        EventHandler<ActionEvent> viewActionHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                // Navigate to view screen after it has been created
                ReadOnlyFoodModelProxy selectedFood = foodsTable.getSelectionModel().getSelectedItem();
                ScreenNavigationEvent navigationEvent = new ScreenNavigationEvent(ApplicationSections.FOOD_DATABASE, SceneSets.VIEW_FOOD, hostingStageName, user);
                navigationEvent.getControllerData().put(ReadOnlyFoodModelProxy.class.getName(), selectedFood);
                eventBus.post(navigationEvent);
            }
        };

        EventHandler<ActionEvent> editActionHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ReadOnlyFoodModelProxy selectedFood = foodsTable.getSelectionModel().getSelectedItem();
                navigateToComposeScreenForFood(selectedFood.getFoodType(), selectedFood);
            }
        };

        EventHandler<ActionEvent> deleteActionHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ReadOnlyFoodModelProxy selectedFood = foodsTable.getSelectionModel().getSelectedItem();

                // First, verify that we can delete the food and it isn't used as an ingredient elsewhere
                selectedFood.canDelete((wasSuccessful, proxyResult) -> {
                    if (wasSuccessful)
                    {
                        if (proxyResult.isEmpty())
                        {
                            // Second, verify we actually want to delete the food and meant to click the delete button
                            windowManager.openConfirmationDialog(rootContainer.getScene().getWindow(), () -> {
                                // Finally, actually delete the food.
                                notificationBuilder.create("Deleting food...", hostingStageName).show();
                                selectedFood.delete((wasDeleteSuccessful, resultMessage) -> {
                                    notificationBuilder.create(resultMessage, hostingStageName).show();

                                    if (wasDeleteSuccessful)
                                    {
                                        refreshFoods();
                                    }
                                });
                            });
                        }
                        else
                        {
                            // Show the error dialog details to which other entities this food belongs as an ingredient
                            LOG.debug("Could not delete food. It is used an ingredient elsewhere.");
                            OpenDialogRequest<Void> request = new OpenDialogRequest<>(rootContainer.getScene().getWindow(), SceneSets.DIALOG_VIEW_FOODS_INVALID_FOOD_DELETION, dialogOutput -> {
                                // No work to do when this dialog closes. It is informational only.
                            });
                            request.setHeight(400);
                            request.setWidth(700);
                            request.setDialogTitle("Invalid Deletion Action");
                            request.getDialogData().put(FoodAsIngredientViewModel.class.getName(), proxyResult);
                            windowManager.openDialog(request);
                        }
                    }
                    else
                    {
                        notificationBuilder.create("An error has occurred while attempting to delete the food", hostingStageName);
                    }
                });
            }
        };

        ContextMenu contextMenu = LayoutUtils.getDefaultContextMenu(viewActionHandler, editActionHandler, deleteActionHandler);
        foodsTable.setContextMenu(contextMenu);

        // Serving size column displays numeric serving size and unit of measure abbreviation
        // so requires manual setup
        String servingSizeColName = "Serving Size";
        TableColumn<ReadOnlyFoodModelProxy, List<ServingSizeViewModel>> servingSizeNameCol = new TableColumn<>(servingSizeColName);
        servingSizeNameCol.setCellFactory(col -> new ServingSizeChoiceBoxTableCell<>());
        servingSizeNameCol.setCellValueFactory(new PropertyValueFactory<>("servingSizes"));
        columnVisibilityStatus.put(servingSizeColName, new SimpleBooleanProperty(true));
        Bindings.bindBidirectional(servingSizeNameCol.visibleProperty(), columnVisibilityStatus.get(servingSizeColName));

        TableColumn<ReadOnlyFoodModelProxy, String> foodNameCol = getTableColumn("Name", true);
        TableColumn<ReadOnlyFoodModelProxy, String> brandNameCol = getTableColumn("Brand", true);
        TableColumn<ReadOnlyFoodModelProxy, String> sourceCol = getTableColumn("Source", true);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> giCol = getTableColumn("Glycemic Index", false);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> caloriesCol = getTableColumn("Calories", true);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> proteinCol = getTableColumn("Protein", true);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> carbohydratesCol = getTableColumn("Carbohydrates", true);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> totalFatsCol = getTableColumn("Total Fats", true);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> transFatCol = getTableColumn("Trans Fat", false);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> monoFatCol = getTableColumn("Monounsaturated Fat", false);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> polyFatCol = getTableColumn("Polyunsaturated Fat", false);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> satFatCol = getTableColumn("Saturated Fat", false);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> sugarsCol = getTableColumn("Sugars", true);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> fiberCol = getTableColumn("Fiber", true);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> cholesterolCol = getTableColumn("Cholesterol", false);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> ironCol = getTableColumn("Iron", false);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> manganeseCol = getTableColumn("Manganese", false);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> copperCol = getTableColumn("Copper", false);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> zincCol = getTableColumn("Zinc", false);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> calciumCol = getTableColumn("Calcium", false);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> magnesiumCol = getTableColumn("Magnesium", false);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> phosphorusCol = getTableColumn("Phosphorus", false);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> potassiumCol = getTableColumn("Potassium", false);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> sodiumCol = getTableColumn("Sodium", false);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> sulfurCol = getTableColumn("Sulfur", false);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> vitACol = getTableColumn("Vitamin A", false);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> vitCCol = getTableColumn("Vitamin C", false);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> vitKCol = getTableColumn("Vitamin K", false);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> vitDCol = getTableColumn("Vitamin D", false);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> vitECol = getTableColumn("Vitamin E", false);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> vitB1Col = getTableColumn("Vitamin B1", false);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> vitB2Col = getTableColumn("Vitamin B2", false);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> vitB3Col = getTableColumn("Vitamin B3", false);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> vitB5Col = getTableColumn("Vitamin B5", false);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> vitB6Col = getTableColumn("Vitamin B6", false);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> vitB7Col = getTableColumn("Vitamin B7", false);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> vitB8Col = getTableColumn("Vitamin B8", false);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> vitB9Col = getTableColumn("Vitamin B9", false);
        TableColumn<ReadOnlyFoodModelProxy, BigDecimal> vitB12Col = getTableColumn("Vitamin B12", false);
        TableColumn<ReadOnlyFoodModelProxy, String> lastModifiedUserCol = getTableColumn("Last Modified User", false);

        // This column is also special and requires manual setup due to the need to transform the date
        // to a string and LocalDateTime toString() not being the prettiest
        String lastModDateColumnName = "Last Modified Date";
        TableColumn<ReadOnlyFoodModelProxy, String> lastModifiedDateCol = getTableColumn(lastModDateColumnName, false);
        columnVisibilityStatus.put(lastModDateColumnName, new SimpleBooleanProperty(false));
        Bindings.bindBidirectional(lastModifiedDateCol.visibleProperty(), columnVisibilityStatus.get(lastModDateColumnName));
        lastModifiedDateCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ReadOnlyFoodModelProxy, String>, ObservableValue<String>>()
        {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<ReadOnlyFoodModelProxy, String> param)
            {
                return new ReadOnlyStringWrapper(TextUtils.getDateTimeString(param.getValue().getLastModifiedDate()));
            }
        });

        foodsTable.getColumns().addAll(
                foodNameCol,
                brandNameCol,
                sourceCol,
                servingSizeNameCol,
                giCol,
                caloriesCol,
                proteinCol,
                carbohydratesCol,
                totalFatsCol,
                transFatCol,
                monoFatCol,
                polyFatCol,
                satFatCol,
                sugarsCol,
                fiberCol,
                cholesterolCol,
                ironCol,
                manganeseCol,
                copperCol,
                zincCol,
                calciumCol,
                magnesiumCol,
                phosphorusCol,
                potassiumCol,
                sodiumCol,
                sulfurCol,
                vitACol,
                vitCCol,
                vitKCol,
                vitDCol,
                vitECol,
                vitB1Col,
                vitB2Col,
                vitB3Col,
                vitB5Col,
                vitB6Col,
                vitB7Col,
                vitB8Col,
                vitB9Col,
                vitB12Col,
                lastModifiedUserCol,
                lastModifiedDateCol);

        foodsTable.setItems(filteredFoods);

        tableViewContainer.getChildren().add(foodsTable);
    }

    private <T> TableColumn<ReadOnlyFoodModelProxy, T> getTableColumn(String cellName, Boolean isVisible)
    {
        String propertyName;
        String[] cellNameParts = cellName.split("\\s");

        if (cellNameParts.length == 1)
        {
            propertyName = cellNameParts[0].toLowerCase();
        }
        else
        {
            propertyName = cellNameParts[0].toLowerCase();
            propertyName = propertyName.concat(Arrays.stream(cellNameParts).skip(1).collect(Collectors.joining()));
        }

        TableColumn<ReadOnlyFoodModelProxy, T> col =  new TableColumn<>(cellName);
        col.setCellValueFactory(new PropertyValueFactory<>(propertyName));
        columnVisibilityStatus.put(cellName, new SimpleBooleanProperty(isVisible));
        Bindings.bindBidirectional(col.visibleProperty(), columnVisibilityStatus.get(cellName));
        return col;
    }

    private void navigateToComposeScreenForFood(FoodType foodType, @Nullable ReadOnlyFoodModelProxy selectedFood)
    {
        ScreenNavigationEvent navigationEvent;
        if (foodType == FoodType.SIMPLE)
        {
            navigationEvent = new ScreenNavigationEvent(ApplicationSections.FOOD_DATABASE, SceneSets.COMPOSE_SIMPLE_FOOD, hostingStageName, user);
        }
        else
        {
            navigationEvent = new ScreenNavigationEvent(ApplicationSections.FOOD_DATABASE, SceneSets.COMPOSE_COMPLEX_FOOD, hostingStageName, user);
        }

        // If there was no selected food then we aren't viewing or editing an existing food.
        if (selectedFood != null)
        {
            navigationEvent.getControllerData().put(ReadOnlyFoodModelProxy.class.getName(), selectedFood);
        }

        eventBus.post(navigationEvent);
    }

    private void refreshFoods()
    {
        loadEventBuilder.emitBegin("Searching for foods", hostingStageName);
        foodProxyFactory.getAllFoods((wasSuccessful, proxyResult) -> {
            if (wasSuccessful)
            {
                loadEventBuilder.emitCompleted(hostingStageName);
                allFoods.clear();
                allFoods.addAll(proxyResult);
                refreshFilter();
            }
            else
            {
                notificationBuilder.create("An error has occurred while loading the food data", hostingStageName).show();
            }
        });
    }

    private void refreshFilter()
    {
        filteredFoods.clear();

        // If there was a filter applied then re-apply the filter
        if (activeFilterRequest == null)
        {
            filteredFoods.addAll(allFoods);
        }
        else
        {
            FoodFilterer filterer = new FoodFilterer(activeFilterRequest);
            filteredFoods.addAll(filterer.applyFilters(allFoods));
        }
    }
}
