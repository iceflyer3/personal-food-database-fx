package com.iceflyer3.pfd.ui;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023, 2025 Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.enums.Nutrient;
import com.iceflyer3.pfd.enums.UnitOfMeasure;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

import java.math.BigDecimal;

/**
 * Represents the combination of the required and consumed amounts of a nutrient that a
 * user has specified custom requirements for as part of a meal plan.
 */
public class CustomNutrientRequirementConsumptionProgress
{
    private final ObjectProperty<Nutrient> nutrient;
    private final ObjectProperty<BigDecimal> consumedAmount;
    private final ObjectProperty<BigDecimal> requiredAmount;
    private final ObjectProperty<UnitOfMeasure> unitOfMeasure;

    public CustomNutrientRequirementConsumptionProgress(Nutrient nutrient, BigDecimal consumed, BigDecimal required, UnitOfMeasure unitOfMeasure)
    {
        this.nutrient = new SimpleObjectProperty<>(nutrient);
        this.consumedAmount = new SimpleObjectProperty<>(consumed);
        this.requiredAmount = new SimpleObjectProperty<>(required);
        this.unitOfMeasure = new SimpleObjectProperty<>(unitOfMeasure);
    }

    public Nutrient getNutrient()
    {
        return nutrient.get();
    }

    public ObjectProperty<Nutrient> nutrientProperty()
    {
        return nutrient;
    }

    public BigDecimal getConsumedAmount()
    {
        return consumedAmount.get();
    }

    public ObjectProperty<BigDecimal> consumedAmountProperty()
    {
        return consumedAmount;
    }

    public BigDecimal getRequiredAmount()
    {
        return requiredAmount.get();
    }

    public ObjectProperty<BigDecimal> requiredAmountProperty()
    {
        return requiredAmount;
    }

    public UnitOfMeasure getUnitOfMeasure()
    {
        return unitOfMeasure.get();
    }

    public ObjectProperty<UnitOfMeasure> unitOfMeasureProperty()
    {
        return unitOfMeasure;
    }
}
