/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.controller.dialog;

import com.iceflyer3.pfd.ui.controller.Savable;
import com.iceflyer3.pfd.ui.listener.Destroyable;
import com.iceflyer3.pfd.ui.listener.ObservableValueListenerBinding;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.util.BindingUtils;
import com.iceflyer3.pfd.ui.listener.ModelPropertyValidationEventConfig;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TextField;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class NamePromptDialogController extends AbstractDialogController<UserViewModel> implements Savable, Destroyable
{
    private final UserViewModel newUser;
    private ObservableValueListenerBinding<String> profileNameBinding;

    public NamePromptDialogController(WindowManager windowManager)
    {
        super(windowManager);
        this.newUser = new UserViewModel(null,"User");
    }

    @FXML
    private Button confirmButton;

    @FXML
    private Hyperlink cancelButton;

    @FXML
    private TextField profileNameTextbox;

    @Override
    public void initialize() { }

    @Override
    protected void registerDefaultListeners() {
        cancelButton.setOnAction(this::cancel);
        confirmButton.setOnAction(this::save);

        Bindings.bindBidirectional(profileNameTextbox.textProperty(), newUser.usernameProperty());
        profileNameBinding = BindingUtils.bindModelObservableValueToGuiControl(new ModelPropertyValidationEventConfig<>(newUser, newUser.usernameProperty(), profileNameTextbox, this::updateSaveValidity));
    }

    @Override
    public void save(ActionEvent event)
    {
        callback.complete(newUser);
        windowManager.close(WindowManager.DIALOG_WINDOW);
    }

    @Override
    public void updateSaveValidity()
    {
        confirmButton.setDisable(!newUser.validate().wasSuccessful());
    }

    @Override
    public void destroy()
    {
        profileNameBinding.unbind();
    }

    private void cancel(ActionEvent event) {
        windowManager.close(WindowManager.DIALOG_WINDOW);
    }


}
