package com.iceflyer3.pfd.ui.filter;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.ui.model.proxy.food.ReadOnlyFoodModelProxy;
import com.iceflyer3.pfd.validation.ComparisonOperator;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class FoodFilterer {

    private final static Logger LOG = LoggerFactory.getLogger(FoodFilterer.class);

    private final FoodFilterRequest foodFilterRequest;
    private final List<ReadOnlyFoodModelProxy> filteredFoods;

    public FoodFilterer(FoodFilterRequest foodFilterRequest) {
        this.foodFilterRequest = foodFilterRequest;
        this.filteredFoods = new ArrayList<>();
    }

    /**
     * Applies the filters defined by the request object to the supplied list.
     * @param listToFilter The list to apply the filters to
     * @return The filtered list
     */
    public List<ReadOnlyFoodModelProxy> applyFilters(List<ReadOnlyFoodModelProxy> listToFilter)
    {
        if (foodFilterRequest.getLogicalOperation() == ComparisonOperator.AND)
        {
            filteredFoods.addAll(listToFilter);
        }

        applyTextFilters(listToFilter);
        applyNumericFilters(listToFilter);
        return filteredFoods;
    }

    /**
     * Applies a list of text filters
     *
     * @param listToFilter The list to filter
     */
    private void applyTextFilters(List<ReadOnlyFoodModelProxy> listToFilter)
    {
        // Loop through all the filters
        for(TextFoodFilter filter : foodFilterRequest.getTextFilters())
        {
            // Loop through each food to apply the filter to.
            // We may be able to investigate having this use streams, but for now this fine for a first attempt while getting it to work.
            for(ReadOnlyFoodModelProxy food : listToFilter)
            {
                switch(filter.getFoodProperty())
                {
                    case NAME:
                        applyFilterForProperty(filter, food, food.getName());
                        break;
                    case BRAND:
                        applyFilterForProperty(filter, food, food.getBrand());
                        break;
                    case SOURCE:
                        applyFilterForProperty(filter, food, food.getSource());
                        break;
                    default:
                        throw new UnsupportedOperationException(String.format("Food property %s is not supported with text filters", filter.getFoodProperty()));
                }
            }
        }
    }

    private void applyNumericFilters(List<ReadOnlyFoodModelProxy> listToFilter)
    {
        for(NumericFoodFilter filter : foodFilterRequest.getNumericFilters())
        {
            for(ReadOnlyFoodModelProxy food : listToFilter)
            {
                switch(filter.getFoodProperty())
                {
                    case CALORIES:
                        applyFilterForProperty(filter, food, food.getCalories());
                        break;
                    case GLYCEMIC_INDEX:
                        applyFilterForProperty(filter, food, new BigDecimal(food.getGlycemicIndex()));
                        break;
                    case PROTEIN:
                        applyFilterForProperty(filter, food, food.getProtein());
                        break;
                    case TOTAL_FATS:
                        applyFilterForProperty(filter, food, food.getTotalFats());
                        break;
                    case TRANS_FAT:
                        applyFilterForProperty(filter, food, food.getTransFat());
                        break;
                    case MONO_FAT:
                        applyFilterForProperty(filter, food, food.getMonounsaturatedFat());
                        break;
                    case POLY_FAT:
                        applyFilterForProperty(filter, food, food.getPolyunsaturatedFat());
                        break;
                    case SAT_FAT:
                        applyFilterForProperty(filter, food, food.getSaturatedFat());
                        break;
                    case CARBS:
                        applyFilterForProperty(filter, food, food.getCarbohydrates());
                        break;
                    case SUGARS:
                        applyFilterForProperty(filter, food, food.getSugars());
                        break;
                    case FIBER:
                        applyFilterForProperty(filter, food, food.getFiber());
                        break;
                    case CHOLESTEROL:
                        applyFilterForProperty(filter, food, food.getCholesterol());
                        break;
                    case IRON:
                        applyFilterForProperty(filter, food, food.getIron());
                        break;
                    case MANGANESE:
                        applyFilterForProperty(filter, food, food.getManganese());
                        break;
                    case COPPER:
                        applyFilterForProperty(filter, food, food.getCopper());
                        break;
                    case ZINC:
                        applyFilterForProperty(filter, food, food.getZinc());
                        break;
                    case CALCIUM:
                        applyFilterForProperty(filter, food, food.getCalcium());
                        break;
                    case MAGNESIUM:
                        applyFilterForProperty(filter, food, food.getMagnesium());
                        break;
                    case PHOSPHORUS:
                        applyFilterForProperty(filter, food, food.getPhosphorus());
                        break;
                    case SODIUM:
                        applyFilterForProperty(filter, food, food.getSodium());
                        break;
                    case SULFUR:
                        applyFilterForProperty(filter, food, food.getSulfur());
                        break;
                    case VITAMIN_A:
                        applyFilterForProperty(filter, food, food.getVitaminA());
                        break;
                    case VITAMIN_C:
                        applyFilterForProperty(filter, food, food.getVitaminC());
                        break;
                    case VITAMIN_K:
                        applyFilterForProperty(filter, food, food.getVitaminK());
                        break;
                    case VITAMIN_D:
                        applyFilterForProperty(filter, food, food.getVitaminD());
                        break;
                    case VITAMIN_E:
                        applyFilterForProperty(filter, food, food.getVitaminE());
                        break;
                    case VITAMIN_B1:
                        applyFilterForProperty(filter, food, food.getVitaminB1());
                        break;
                    case VITAMIN_B2:
                        applyFilterForProperty(filter, food, food.getVitaminB2());
                        break;
                    case VITAMIN_B3:
                        applyFilterForProperty(filter, food, food.getVitaminB3());
                        break;
                    case VITAMIN_B5:
                        applyFilterForProperty(filter, food, food.getVitaminB5());
                        break;
                    case VITAMIN_B6:
                        applyFilterForProperty(filter, food, food.getVitaminB6());
                        break;
                    case VITAMIN_B7:
                        applyFilterForProperty(filter, food, food.getVitaminB7());
                        break;
                    case VITAMIN_B8:
                        applyFilterForProperty(filter, food, food.getVitaminB8());
                        break;
                    case VITAMIN_B9:
                        applyFilterForProperty(filter, food, food.getVitaminB9());
                        break;
                    case VITAMIN_B12:
                        applyFilterForProperty(filter, food, food.getVitaminB12());
                        break;
                    default:
                        throw new UnsupportedOperationException(String.format("Food property %s is not supported with numeric filters", filter.getFoodProperty()));
                }
            }
        }
    }

    private <T> void applyFilterForProperty(Filter<T> filter, ReadOnlyFoodModelProxy food, T value)
    {
        /*
         * If the logical operation is an AND then we remove from the master list as foods fail to pass the filters
         * If the logical operation is an OR then add to the master list as foods successfully pass the filters
         */
        LOG.debug("Applying a filter {} to food {} and value {}", filter, food, value);
        boolean filterResult = filter.apply(value);
        if (foodFilterRequest.getLogicalOperation() == ComparisonOperator.AND)
        {
            if (!filterResult)
            {
                filteredFoods.remove(food);
            }
        }
        else
        {
            if (filterResult)
            {
                // Don't add any foods multiple times if they pass multiple criteria
                if (!filteredFoods.contains(food))
                {
                    filteredFoods.add(food);
                }
            }
        }
    }
}
