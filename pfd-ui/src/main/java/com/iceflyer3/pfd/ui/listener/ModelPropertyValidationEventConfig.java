/*
 *  Personal Food Database
 *  Copyright (C) 2023 Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.listener;

import com.iceflyer3.pfd.ui.controller.Action;
import com.iceflyer3.pfd.ui.model.api.viewmodel.ValidatingViewModel;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Control;

/**
 * Describes the relationship between a model's JavaFx property and the GUI control to which it is bidirectionally
 * bound such that the control may be updated as needed upon validity state changes of the property's value.
 *
 * Additionally, allows specification of an action that should occur after a validation pass has completed.
 *
 * @param <ModelType> The type of the model
 * @param <PropertyType> The type of the property
 */
public class ModelPropertyValidationEventConfig<ModelType extends ValidatingViewModel, PropertyType>
{
    private final ModelType model;
    private final ObservableValue<PropertyType> modelProperty;
    private final Control control;
    private final Action afterModelPropertyValidationAction;

    /**
     * Creates a new configuration object. Does not specify an additional action to take
     * upon the property becoming valid.
     *
     * @param model The model object to which the property belongs
     * @param modelProperty The property of the model that is bidirectionally bound
     * @param control The control to which the model property is bidirectionally bound
     */
    public ModelPropertyValidationEventConfig(ModelType model, ObservableValue<PropertyType> modelProperty, Control control)
    {
        this(model, modelProperty, control, null);
    }

    /**
     * Creates a new configuration object. Also specifies an additional action to take
     * upon the property becoming valid.
     *
     * @param model The model object to which the property belongs
     * @param modelProperty The property of the model that is bidirectionally bound
     * @param control The control to which the model property is bidirectionally bound
     * @param afterModelPropertyValidationAction An optional additional action to be taken after model property validation has completed
     */
    public ModelPropertyValidationEventConfig(ModelType model, ObservableValue<PropertyType> modelProperty, Control control, Action afterModelPropertyValidationAction)
    {
        this.model = model;
        this.modelProperty = modelProperty;
        this.control = control;
        this.afterModelPropertyValidationAction = afterModelPropertyValidationAction;
    }

    public ModelType getModel()
    {
        return model;
    }

    public ObservableValue<PropertyType> getModelProperty()
    {
        return modelProperty;
    }

    public Control getControl()
    {
        return control;
    }

    public Action getAfterModelPropertyValidationAction()
    {
        return afterModelPropertyValidationAction;
    }
}
