package com.iceflyer3.pfd.ui.controller.recipes;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.google.common.eventbus.EventBus;
import com.iceflyer3.pfd.constants.ApplicationSections;
import com.iceflyer3.pfd.constants.FontAwesomeIcons;
import com.iceflyer3.pfd.constants.SceneSets;
import com.iceflyer3.pfd.ui.controller.sections.MacronutrientBreakdownScreenSection;
import com.iceflyer3.pfd.ui.controller.sections.MicronutrientBreakdownScreenSection;
import com.iceflyer3.pfd.ui.controller.sections.ViewServingSizeScreenSection;
import com.iceflyer3.pfd.ui.event.PublishBreadcrumbEvent;
import com.iceflyer3.pfd.ui.event.ScreenNavigationEvent;
import com.iceflyer3.pfd.ui.cell.RecipeStepListCell;
import com.iceflyer3.pfd.ui.controller.AbstractTaskController;
import com.iceflyer3.pfd.ui.controller.HostStageAware;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.api.recipe.RecipeStepModel;
import com.iceflyer3.pfd.ui.model.proxy.food.ComplexFoodModelProxy;
import com.iceflyer3.pfd.ui.model.proxy.food.FoodProxyFactory;
import com.iceflyer3.pfd.ui.model.proxy.recipe.ReadOnlyRecipeModelProxy;
import com.iceflyer3.pfd.ui.model.viewmodel.IngredientViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.ComplexFoodViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;
import com.iceflyer3.pfd.ui.util.LayoutUtils;
import com.iceflyer3.pfd.ui.util.builder.ActionNodeBuilder;
import com.iceflyer3.pfd.ui.util.builder.NotificationBuilder;
import com.iceflyer3.pfd.ui.util.ScreenLoadEventUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

// For now this is just a simple screen to be able to view the details of a recipe
// At some point in the future it's gonna get re-written to be.... something else.

// This does detect duplicated code with the ViewFoodController for setting macro / micro label values
// Which there technically is. But the values come from different places. So there probably isn't a way around this.
@SuppressWarnings("DuplicatedCode")
@Component
public class ViewRecipeController extends AbstractTaskController implements HostStageAware {

    public static final String DATA_RECIPE = "recipeToView";

    private final FoodProxyFactory foodProxyFactory;

    private UserViewModel userViewModel;
    private ReadOnlyRecipeModelProxy recipeProxy;
    private String hostingStageName;

    private Button createComplexFoodButton;

    @FXML
    public VBox rootContainer;

    @FXML
    public Label screenLabel;

    @FXML
    public AnchorPane buttonContainerAnchorPane;

    @FXML
    public TitledPane recipeStepsPane;

    @FXML
    public Label nameLabel;

    @FXML
    public Label caloriesLabel;

    @FXML
    public Label numberOfServingsMadeLabel;

    @FXML
    public Label recipeSourceLabel;

    public ViewRecipeController(EventBus eventBus,
                                NotificationBuilder notificationBuilder,
                                ScreenLoadEventUtils loadEventBuilder,
                                TaskExecutor taskExecutor,
                                FoodProxyFactory foodProxyFactory)
    {
        super(eventBus, notificationBuilder, loadEventBuilder, taskExecutor);
        this.foodProxyFactory = foodProxyFactory;
    }

    @Override
    public void initialize() {
        VBox.setMargin(screenLabel, LayoutUtils.getHeaderLabelMarginInsets());

        createComplexFoodButton = ActionNodeBuilder
                .createIconButton("CREATE COMPLEX FOOD", FontAwesomeIcons.PLUS)
                .withAction(this::createComplexFoodFromRecipe)
                .build();
        AnchorPane.setRightAnchor(createComplexFoodButton, 0.0);
        buttonContainerAnchorPane.getChildren().add(createComplexFoodButton);
    }

    @Override
    public void initControllerData(Map<String, Object> controllerData) {
        /*
         * We have to reload the recipe selected from the view all screen here so we can get a fully hydrated recipe.
         *
         * Recipes on the view recipe screen don't load the lists of ingredients or steps because these are potentially
         * large and there are likely several recipes.
         */
        userViewModel = (UserViewModel) controllerData.get(UserViewModel.class.getName());
        recipeProxy = (ReadOnlyRecipeModelProxy) controllerData.get(DATA_RECIPE);

        loadEventBuilder.emitBegin("Searching for recipe details", hostingStageName);

        // First load the ingredients, then the steps
        recipeProxy.loadIngredients((ingredientLoadSuccessful, ingredientLoadResultMessage) -> {
            if (ingredientLoadSuccessful)
            {
                recipeProxy.loadSteps((stepsLoadSuccessful, stepLoadResultMessage) -> {
                    if (stepsLoadSuccessful)
                    {
                        checkIfComplexFoodHasBeenCreated();
                        createBasicInformationSection();
                        createStepsTitledPanes();

                        ViewServingSizeScreenSection<ReadOnlyRecipeModelProxy> servingSizeSection = new ViewServingSizeScreenSection<>(recipeProxy);
                        MacronutrientBreakdownScreenSection<ReadOnlyRecipeModelProxy> macroBreakdownSection = new MacronutrientBreakdownScreenSection<>(recipeProxy);
                        MicronutrientBreakdownScreenSection<ReadOnlyRecipeModelProxy> microBreakdownSection = new MicronutrientBreakdownScreenSection<>(recipeProxy);
                        rootContainer.getChildren().add(3, servingSizeSection.create());
                        rootContainer.getChildren().add(4, macroBreakdownSection.create());
                        rootContainer.getChildren().add(5, microBreakdownSection.create());
                    }
                    else
                    {
                        notificationBuilder.create(stepLoadResultMessage, hostingStageName).show();
                    }
                    loadEventBuilder.emitCompleted(hostingStageName);
                });
            }
            else
            {
                notificationBuilder.create(ingredientLoadResultMessage, hostingStageName).show();
                loadEventBuilder.emitCompleted(hostingStageName);
            }
        });
    }

    @Override
    protected void registerDefaultListeners() {

    }

    @Override
    public void initHostingStageName(String stageName) {
        hostingStageName = stageName;

        PublishBreadcrumbEvent landingScreenBreadcrumb = new PublishBreadcrumbEvent("View Recipe", ApplicationSections.RECIPES, SceneSets.VIEW_RECIPE, hostingStageName, 2);
        eventBus.post(landingScreenBreadcrumb);
    }

    private void createBasicInformationSection()
    {
        nameLabel.setText(recipeProxy.getName());
        caloriesLabel.setText(recipeProxy.getCalories().toPlainString());
        numberOfServingsMadeLabel.setText(recipeProxy.getNumberOfServingsMade().toPlainString());
        recipeSourceLabel.setText(recipeProxy.getSource());
    }

    private void createStepsTitledPanes()
    {
        ListView<RecipeStepModel> stepsListView = new ListView<>(recipeProxy.getSteps());
        stepsListView.setPlaceholder(new Label("There are no steps for this recipe."));
        stepsListView.setCellFactory(col -> new RecipeStepListCell());
        recipeStepsPane.setContent(stepsListView);
    }

    private void createComplexFoodFromRecipe(ActionEvent event)
    {
        /*
         * Divide the serving size of each ingredient by the number of servings made.
         *
         * We do this because a food always represents a single serving of that food
         * while a recipe can make multiple servings of the food that it makes.
         */
        List<IngredientModel> foodIngredients = recipeProxy
                .getIngredients()
                .stream()
                .map(recipeIngredient -> {
                    BigDecimal servingsIncluded = recipeIngredient.getServingsIncluded().divide(recipeProxy.getNumberOfServingsMade(), RoundingMode.HALF_UP).setScale(2, RoundingMode.HALF_UP);
                    IngredientViewModel foodIngredient = new IngredientViewModel(recipeIngredient.getIngredientFood());
                    foodIngredient.setServingsIncluded(servingsIncluded);
                    foodIngredient.setIsAlternate(recipeIngredient.isAlternate());
                    foodIngredient.setIsOptional(recipeIngredient.isOptional());
                    return foodIngredient;
                })
                .collect(Collectors.toList());

        NutritionalSummary nutritionalSummary =  NutritionalSummary.deriveUpdateFrom(recipeProxy.getIngredients());
        ComplexFoodViewModel complexFoodViewModel = new ComplexFoodViewModel(
                null,
                recipeProxy.getName(),
                recipeProxy.getCreatedUser(),
                LocalDateTime.now(),
                recipeProxy.getCreatedUser(),
                LocalDateTime.now(),
                recipeProxy.getServingSizes(),
                nutritionalSummary
        );
        complexFoodViewModel.setIngredients(foodIngredients);

        // With details of the food filled out navigate to the compose simple food screen where any final
        // changes may be made and the food may be saved.
        ScreenNavigationEvent navigationEvent = new ScreenNavigationEvent(ApplicationSections.FOOD_DATABASE, SceneSets.COMPOSE_COMPLEX_FOOD, hostingStageName, userViewModel);
        navigationEvent.getControllerData().put(ComplexFoodViewModel.class.getName(), complexFoodViewModel);
        eventBus.post(navigationEvent);
    }

    /*
     * Checks if a complex food has already been created for this recipe. If so, then it hides
     * the "Create Complex Food" button.
     *
     * We can have multiple complex foods with the same name. As such, the existence of a complex
     * food with the same name alone will not be sufficient to determine if this recipe has already
     * been made into a complex food or not. We'll have to examine any complex foods with the same
     * name a bit more closely.
     *
     * This is done by looking at the combination of name and the nutritional details for the recipe
     * and candidate complex foods that share the name of the recipe.
     *
     * If the complex food has the same name as the recipe and all the nutritional details match
     * then it seems overwhelmingly likely that the complex food was created based upon the recipe.
     *
     * If any deeper analysis than this is needed, well, we'll cross that bridge when come to it. But
     * I think it unlikely that we arrive there.
     */
    private void checkIfComplexFoodHasBeenCreated()
    {
        foodProxyFactory.findComplexFoodsWithName(recipeProxy.getName(), userViewModel, (wasSuccessful, matchingFoods) -> {
            if (wasSuccessful)
            {
                for(ComplexFoodModelProxy matchingFood : matchingFoods)
                {
                    if (matchingFood.isNutritionallyEqualTo(recipeProxy))
                    {
                        createComplexFoodButton.setVisible(false);
                        break;
                    }
                }
            }
            else
            {
                notificationBuilder.create("An error has occurred while loading the recipe", hostingStageName).show();
            }
        });
    }
}
