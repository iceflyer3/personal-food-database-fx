/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


package com.iceflyer3.pfd.ui.controller.dialog;

import com.iceflyer3.pfd.enums.CarbCyclingDayType;
import com.iceflyer3.pfd.ui.controller.Savable;
import com.iceflyer3.pfd.ui.format.CarbCyclingDayTypeStringConverter;
import com.iceflyer3.pfd.ui.listener.Destroyable;
import com.iceflyer3.pfd.ui.listener.ObservableValueListenerBinding;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.*;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.carbcycling.CarbCyclingDayModelProxy;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.carbcycling.CarbCyclingPlanModelProxy;
import com.iceflyer3.pfd.ui.util.BindingUtils;
import com.iceflyer3.pfd.ui.listener.ModelPropertyValidationEventConfig;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CreateCarbCyclingDayDialogController extends AbstractDialogController<CarbCyclingDayModelProxy> implements Savable, Destroyable
{
    public final static String DATA_MEAL_PLAN = "mealPlan";
    public final static String DATA_CARB_CYCLING_PLAN = "carbCyclingPlan";

    private final MealPlanningProxyFactory mealPlanningProxyFactory;

    private CarbCyclingDayModelProxy newDay;

    private ObservableValueListenerBinding<String> dayLabelBinding;

    @FXML
    public TextField dayNameInput;

    @FXML
    public VBox rootContainer;

    @FXML
    public ChoiceBox<CarbCyclingDayType> dayTypeChoiceBox;

    @FXML
    public CheckBox isTrainingDayCheckbox;

    @FXML
    public Button saveButton;

    @FXML
    public Hyperlink cancelButton;

    public CreateCarbCyclingDayDialogController(WindowManager windowManager, MealPlanningProxyFactory mealPlanningProxyFactory)
    {
        super(windowManager);
        this.mealPlanningProxyFactory = mealPlanningProxyFactory;
    }

    @Override
    public void initialize()
    {

    }

    @Override
    public void initControllerData(Map<String, Object> dialogData)
    {
        MealPlanModelProxy mealPlan = (MealPlanModelProxy) dialogData.get(DATA_MEAL_PLAN);
        CarbCyclingPlanModelProxy carbCyclingPlan = (CarbCyclingPlanModelProxy) dialogData.get(DATA_CARB_CYCLING_PLAN);

        this.mealPlanningProxyFactory.getCarbCyclingDayForMealPlanById(null, mealPlan, carbCyclingPlan, (wasSuccessful, proxyResult) -> {
            if (wasSuccessful)
            {
                newDay = proxyResult;
                newDay.setFitnessGoal(mealPlan.getFitnessGoal());

                // Setup the bindings to the UI controls
                Bindings.bindBidirectional(newDay.dayLabelProperty(), dayNameInput.textProperty());
                dayLabelBinding = BindingUtils.bindModelObservableValueToGuiControl(new ModelPropertyValidationEventConfig<>(newDay, newDay.dayLabelProperty(), dayNameInput, this::updateSaveValidity));

                ObservableList<CarbCyclingDayType> carbCyclingDayTypeOptions = FXCollections.observableArrayList(CarbCyclingDayType.values());
                dayTypeChoiceBox.setItems(carbCyclingDayTypeOptions);
                dayTypeChoiceBox.setConverter(new CarbCyclingDayTypeStringConverter());
                Bindings.bindBidirectional(dayTypeChoiceBox.valueProperty(), newDay.dayTypeProperty());

                Bindings.bindBidirectional(isTrainingDayCheckbox.selectedProperty(), newDay.isTrainingDayProperty());
            }
            else
            {
                // Theoretically this should never actually happen
                windowManager.openFatalErrorDialog();
            }
        });
    }

    @Override
    protected void registerDefaultListeners()
    {
        saveButton.setOnAction(this::save);
        cancelButton.setOnAction(this::cancel);
    }

    @Override
    public void updateSaveValidity()
    {
        saveButton.setDisable(!newDay.validate().wasSuccessful());
    }

    @Override
    public void save(ActionEvent event)
    {
        callback.complete(newDay);
        windowManager.close(WindowManager.DIALOG_WINDOW);
    }

    @Override
    public void destroy()
    {
        dayLabelBinding.unbind();
    }

    private void cancel(ActionEvent event)
    {
        windowManager.close(WindowManager.DIALOG_WINDOW);
    }
}
