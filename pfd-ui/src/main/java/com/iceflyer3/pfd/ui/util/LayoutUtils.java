/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.util;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.*;

/**
 * Serves as a factory for producing objects needed to set layout properties that need
 * to be the same application wide or that are not available via CSS.
 *
 * For example:
 *      - The padding for all main screen containers should be the same across the entire
 *        application
 *      - There is no -fx-margin CSS property. You must use Insets and set the margin
 *        programmatically.
 *
 * Additionally provides convenient application wide access to a collection of layout
 * convenience functions that ensure
 */
public final class LayoutUtils {


    /***************************************************
     *
     *      LAYOUT FUNCTIONS
     *
     ****************************************************/

    /**
     * Get the insets that all the headers at the top of each screen should use
     *
     * @return Insets for use with screen headers
     */
    public static Insets getHeaderLabelMarginInsets()
    {
        return new Insets(0, 0, 20, 0);
    }

    /**
     * Gets the insets that all non-header nodes that appear on the left hand side of
     * the screen should use so that they are vertically in line with the header.
     *
     * @return Insets for use with a node on the left hand side of the screen
     */
    public static Insets getLeftHandNodeInsets()
    {
        return new Insets(0, 40, 0, 40);
    }

    /**
     * Gets the insets that the top level container nodes for all screens that are part
     * of one of the five sections should use.
     *
     * @return Insets for use with a node that is the container for an application section screen
     */
    public static Insets getSectionContainerNodePadding()
    {
        return new Insets(25, 40, 25, 40);
    }

    public static Insets getSectionElementVerticalSpacing() {
        return new Insets(20, 0, 20, 0);
    }

    /**
     * Gets a context menu with the default menu options of "View", "Edit", and "Delete". If null is
     * passed in for any of the event handlers the corresponding option will be left out of the returned
     * context menu.
     *
     * @param viewActionHandler Event handler for action of the View menu item
     * @param editActionHandler Event handler for action of the Edit menu item
     * @param deleteActionHandler Event handler for action of the Delete menu item
     * @return A new ContextMenu instance
     */
    public static ContextMenu getDefaultContextMenu(EventHandler<ActionEvent> viewActionHandler, EventHandler<ActionEvent> editActionHandler, EventHandler<ActionEvent> deleteActionHandler)
    {
        ContextMenu contextMenu = new ContextMenu();

        if (viewActionHandler != null)
        {
            MenuItem viewItem = new MenuItem("View");
            viewItem.setOnAction(viewActionHandler);

            contextMenu.getItems().add(viewItem);
        }

        if (editActionHandler != null)
        {
            MenuItem editItem = new MenuItem("Edit");
            editItem.setOnAction(editActionHandler);

            contextMenu.getItems().add(editItem);
        }

        if (deleteActionHandler != null)
        {
            MenuItem deleteItem = new MenuItem("Delete");
            deleteItem.setOnAction(deleteActionHandler);

            contextMenu.getItems().add(deleteItem);
        }

        return contextMenu;
    }
}
