package com.iceflyer3.pfd.ui.widget.stepper.series;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023, 2025 Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.exception.IllegalCallbackInvocationException;
import com.iceflyer3.pfd.ui.controller.AbstractController;
import com.iceflyer3.pfd.ui.controller.Action;
import com.iceflyer3.pfd.ui.listener.Destroyable;
import com.iceflyer3.pfd.ui.util.ControllerUtils;
import com.iceflyer3.pfd.ui.util.FxmlLoadResult;
import com.iceflyer3.pfd.ui.widget.stepper.StepOrchestrator;
import com.iceflyer3.pfd.ui.widget.stepper.Steppable;
import javafx.fxml.FXML;
import javafx.scene.layout.VBox;

import java.util.Collections;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * A screen series is a loosely defined collection of screens to be stepped through with no statically defined navigation
 * order. The navigation order for a screen series is decided by user decisions made on each screen (except for the last
 * screen from which there are no further screens to navigate to).
 *
 * These decisions decide the next screen in the series that should be navigated to. As a side effect, this means that
 * it is very likely to have screens registered with the series which will sometimes remain unvisited during a given
 * trip through the series.
 *
 * This is as opposed to declaring a specific static order that you would find with, for example, a Wizard/Stepper where
 * there is a well-defined order of the first step, second step, and so on that does not change from one iteration of
 * the steps to the next.
 *
 * @param <T> The return type of the screen series widget.
 */
public class ScreenSeriesWidgetController<T> implements StepOrchestrator
{
    @FXML
    public VBox screenSeriesContentContainer;

    private final ControllerUtils controllerUtils;
    private final Set<ScreenSeriesScreen> screens;
    private final Consumer<T> resultCallback;

    private Action stepChangeCallback;
    private ScreenSeriesScreen currentScreen;
    private AbstractController loadedScreenController;


    public ScreenSeriesWidgetController(ControllerUtils controllerUtils, Set<ScreenSeriesScreen> screens, Consumer<T> resultCallback)
    {
        this.screens = screens;
        this.controllerUtils = controllerUtils;
        this.resultCallback = resultCallback;
        this.stepChangeCallback = () -> {};
    }

    @Override
    public void initialize()
    {
        // Load the initial screen
        ScreenSeriesScreen initialScreen = screens.stream().filter(ScreenSeriesScreen::isInitial).findFirst().orElseThrow();
        loadScreen(initialScreen);
    }

    @Override
    public void onStepChange(Action action)
    {
        this.stepChangeCallback = action;
    }

    /**
     * Indicate that the series should advance to the next screen in the series.
     * @throws UnsupportedOperationException This variant is unsupported for screen series. Use the overload that accepts a return value instead.
     */
    @Override
    public void completeStep() throws UnsupportedOperationException
    {
        throw new UnsupportedOperationException("Screens that are part of a series must always return a result indicating the next action to take. The lone exception to this is the final screen in a series, which may return null");
    }

    /**
     * Indicate that the series should advance to the next screen in the series.
     * @param stepResult The result of the step as reported by the associated Steppable component
     */
    @Override
    public void completeStep(Object stepResult)
    {
        // Ensure the controller for the current step is disposed of before moving on to the next step (if any)
        if (loadedScreenController != null)
        {
            if (loadedScreenController instanceof Destroyable destroyable)
            {
                destroyable.destroy();
            }

            loadedScreenController = null;
        }

        // If the loaded screen is the final screen then don't attempt to load a new screen (because there aren't any more)
        if (!currentScreen.isFinal())
        {
            Set<ScreenSeriesScreen> availableScreens = screens.stream()
                    .filter(screen -> !screen.getScreenComponentClass().equals(currentScreen.getScreenComponentClass()))
                    .collect(Collectors.toSet());
            ScreenSeriesScreen nextScreen = currentScreen.getNavigationDecision().makeScreenChoice(Collections.unmodifiableSet(availableScreens), stepResult);
            loadScreen(nextScreen);
        }
        else
        {
            // Each step can return a different result type, with only the result type of the final step actually being
            // known. So, although we lose some type safety, there isn't a way around this unchecked cast that I'm aware of
            try
            {
                this.resultCallback.accept((T) stepResult);
            }
            catch(ClassCastException cce)
            {
                throw new IllegalCallbackInvocationException("A screen from a screen series attempted to return an unexpected type via the callback.");
            }
        }
    }

    /**
     * Cancelling a step in a screen series terminates the series and returns a result of null.
     */
    @Override
    public void cancelStep()
    {
        this.resultCallback.accept(null);
    }

    private void loadScreen(ScreenSeriesScreen screenToLoad)
    {
        String fxmlFileName = controllerUtils.parseFxmlFileNameFromControllerClass(screenToLoad.getScreenComponentClass());
        FxmlLoadResult<? extends Steppable> fxmlLoadResult = controllerUtils.loadSteppableFxml(fxmlFileName);
        fxmlLoadResult.getController().usingStepOrchestrator(this);

        currentScreen = screenToLoad;
        loadedScreenController = (AbstractController) fxmlLoadResult.getController();

        if (screenToLoad.hasInitData())
        {
            loadedScreenController.initControllerData(screenToLoad.getComponentInitData());
        }

        screenSeriesContentContainer.getChildren().clear();
        screenSeriesContentContainer.getChildren().add(fxmlLoadResult.getRootNode());

        stepChangeCallback.perform();
    }
}
