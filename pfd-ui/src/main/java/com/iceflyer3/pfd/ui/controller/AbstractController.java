/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.controller;

import javafx.fxml.Initializable;

import java.net.URL;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * Abstract base class that defines the interface for all application controllers.
 * Functions defined here should apply to all controllers across the application.
 *
 * If functionality should not apply strictly to all controllers it will not be
 * included here.
 *
 * There are also interfaces, such as HostStageAware or Savable, that also augment
 * the interface defined by this class where needed.
 *
 * When loading a controller for one of the screens in the main application sections
 * initialization will follow the following process:
 *      - Create the controller via FXMLLoader. This automatically calls initialize
 *        as noted below
 *      - Check if the concrete controller instance implements HostStageAware. If
 *        it does then populate the hosting stage name via initHostingStageName
 *        Any screens that may be used from multiple stages (windows) will implement
 *        this interface
 *      - Populate the data for the controller from the ScreenNavigationEvent
 *        via initControllerData
 */
public abstract class AbstractController implements Initializable {

    /**
     * Hook for controllers to perform any needed screen specific initialization
     */
    public abstract void initialize();

    /**
     * Hook to perform setup of listeners for objects which should always need them.
     *
     * This SHOULD NOT be called by implementing classes themselves. It is instead called
     * by the initialize function that is an implementation of the Initializable interface
     * and thus automatically called by the FXMLLoader.
     */
    protected abstract void registerDefaultListeners();

    /*
     * Due to being an implementation of the Initializable interface this will
     * be called automatically by FXMLLoader upon controller creation.
     *
     * Having it call our own initialize function also gives us the ability to
     * reinitialize ourselves in response to events (such as navigation to the
     * screen which uses this controller)
     *
     * Most of the time I suspect we won't care about the two arguments here but
     * if we ever do care then we additionally have the ability to override this
     * implementation per controller that does care to provide its own implementation.
     */
    /**
     * Implementation of the Initializable interface function that is a hook for the
     * FXMLLoader.
     *
     * @param location
     * The location used to resolve relative paths for the root object, or
     * {@code null} if the location is not known.
     *
     * @param resources
     * The resources used to localize the root object, or {@code null} if
     * the root object was not localized.
     */
    public void initialize(URL location, ResourceBundle resources)
    {
        initialize();
        registerDefaultListeners();
    }

    /**
     * Function to be overridden by subclasses that wish to be able to receive data passed in from outside of the controller.
     *
     * Subclasses that do override this function are responsible for doing explicit casts of the objects retrieved from the map
     * according to the types they expect.
     *
     * @param controllerData A Map containing the data for the controller. The String key here is the name of the class as acquired by calling Class.getName
     *                       and the value is the instance of that object that is being passed into the controller.
     */
    public void initControllerData(Map<String, Object> controllerData)
    {
        // Not all screens care about or need to make use of additional data. So the default implementation is a no-op.
    }
}
