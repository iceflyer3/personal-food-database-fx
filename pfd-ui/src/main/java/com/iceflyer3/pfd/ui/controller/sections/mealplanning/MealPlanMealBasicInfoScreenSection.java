/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.controller.sections.mealplanning;

import com.iceflyer3.pfd.constants.CssConstants;
import com.iceflyer3.pfd.ui.controller.sections.ScreenSection;
import com.iceflyer3.pfd.ui.model.api.mealplanning.ReadOnlyMealPlanMealModel;
import com.iceflyer3.pfd.ui.util.TextUtils;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

public class MealPlanMealBasicInfoScreenSection<T extends ReadOnlyMealPlanMealModel> implements ScreenSection
{
    T entity;

    public MealPlanMealBasicInfoScreenSection(T entity)
    {
        this.entity = entity;
    }

    @Override
    public Node create()
    {
        Label mealNameLabel = new Label(entity.getName());
        mealNameLabel.getStyleClass().addAll(
                CssConstants.CLASS_BOLD,
                CssConstants.CLASS_UNDERLINE,
                CssConstants.CLASS_TEXT_SUB_HEADER
        );

        Label lastUpdatedHeader = new Label("Last Updated:");
        lastUpdatedHeader.getStyleClass().add(CssConstants.CLASS_BOLD);

        Label lastUpdatedContent = new Label(TextUtils.getDateTimeString(entity.getLastModifiedDate()));

        GridPane gridPane = new GridPane();
        gridPane.setVgap(10);
        gridPane.setHgap(20);
        gridPane.add(lastUpdatedHeader, 0, 0);
        gridPane.add(lastUpdatedContent, 1, 0);

        VBox paneContentContainer = new VBox();
        paneContentContainer.getChildren().addAll(
                mealNameLabel,
                gridPane
        );

        TitledPane titledPane = new TitledPane("Meal Overview", paneContentContainer);
        VBox.setMargin(titledPane, new Insets(20, 0, 20, 0));
        VBox paneContainer = new VBox();
        paneContainer.getChildren().add(titledPane);

        return paneContainer;
    }
}
