package com.iceflyer3.pfd.ui.controller.dialog;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.ui.controller.Savable;
import com.iceflyer3.pfd.ui.listener.Destroyable;
import com.iceflyer3.pfd.ui.listener.ObservableValueListenerBinding;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.*;
import com.iceflyer3.pfd.ui.util.BindingUtils;
import com.iceflyer3.pfd.ui.listener.ModelPropertyValidationEventConfig;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CreateMealPlanMealDialogController extends AbstractDialogController<MealPlanMealModelProxy> implements Savable, Destroyable
{
    public static final String DATA_FOR_DAY = "forDay";
    public static final String DATA_MEAL_PLAN = "mealPlan";

    private final MealPlanningProxyFactory mealPlanningProxyFactory;

    private MealPlanMealModelProxy mealProxy;
    private ObservableValueListenerBinding<String> mealNameListenerBinding;

    @FXML
    public VBox rootContainer;

    @FXML
    public TextField mealNameInput;

    @FXML
    public Button saveButton;

    @FXML
    public Hyperlink cancelButton;

    public CreateMealPlanMealDialogController(WindowManager windowManager, MealPlanningProxyFactory mealPlanningProxyFactory) {
        super(windowManager);
        this.mealPlanningProxyFactory = mealPlanningProxyFactory;
    }

    @Override
    public void initialize() {

    }

    @Override
    public void initControllerData(Map<String, Object> dialogData) {
        MealPlanDayModelProxy forDay = (MealPlanDayModelProxy) dialogData.get(DATA_FOR_DAY);
        // See the note in SaveMealForMealPlanTask about the meal plan proxy. For now, it can stay. Later it'll be removed from here.
        MealPlanModelProxy planModelProxy = (MealPlanModelProxy) dialogData.get(DATA_MEAL_PLAN);

        mealPlanningProxyFactory.getMealForDayById(null, planModelProxy, forDay, (wasSuccessful, proxyResult) -> {
            if (wasSuccessful)
            {
                mealProxy = proxyResult;
                initBindingsAndValidations();
                updateSaveValidity();
            }
            else
            {
                // Theoretically this should never actually happen
                windowManager.openFatalErrorDialog();
            }
        });
    }

    @Override
    protected void registerDefaultListeners() {
        saveButton.setOnAction(this::save);
        cancelButton.setOnAction(this::cancel);
    }

    @Override
    public void updateSaveValidity()
    {
        saveButton.setDisable(!mealProxy.validate().wasSuccessful());
    }

    @Override
    public void save(ActionEvent event)
    {
        callback.complete(mealProxy);
        windowManager.close(WindowManager.DIALOG_WINDOW);
    }

    @Override
    public void destroy()
    {
        mealNameListenerBinding.unbind();
    }

    private void cancel(ActionEvent event)
    {
        windowManager.close(WindowManager.DIALOG_WINDOW);
    }

    private void initBindingsAndValidations() {
        Bindings.bindBidirectional(mealNameInput.textProperty(), mealProxy.nameProperty());
        mealNameListenerBinding = BindingUtils.bindModelObservableValueToGuiControl(new ModelPropertyValidationEventConfig<>(mealProxy, mealProxy.nameProperty(), mealNameInput, this::updateSaveValidity));
    }
}
