package com.iceflyer3.pfd.ui.controller.sections;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.constants.CssConstants;
import com.iceflyer3.pfd.ui.listener.Destroyable;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.api.NutritionalSummaryReporter;
import com.iceflyer3.pfd.ui.model.api.owner.IngredientReader;
import com.iceflyer3.pfd.ui.model.viewmodel.IngredientViewModel;
import com.iceflyer3.pfd.ui.util.LayoutUtils;
import javafx.beans.binding.Bindings;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TitledPane;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import java.math.BigDecimal;
import java.util.List;

/**
 * This screen section is a Titled Pane that contains two tables the breakdown of micronutrient
 * information. There is one table for Grand Total micronutrients and one table for per ingredient
 * micronutrient information.
 *
 * In the event the entity on which this section reports doesn't support ingredients then the per
 * ingredient section will not be shown
 *
 * @param <T> The concrete type of the entity this pane reports on
 */
public class MicronutrientBreakdownScreenSection<T extends NutritionalSummaryReporter> implements ScreenSection, Destroyable
{
    private final T entity;

    private final Label cholesterolLabel;
    private final Label ironLabel;
    private final Label manganeseLabel;
    private final Label copperLabel;
    private final Label zincLabel;
    private final Label calciumLabel;
    private final Label magnesiumLabel;
    private final Label phosphorusLabel;
    private final Label potassiumLabel;
    private final Label sodiumLabel;
    private final Label sulfurLabel;
    private final Label vitaminALabel;
    private final Label vitaminCLabel;
    private final Label vitaminDLabel;
    private final Label vitaminELabel;
    private final Label vitaminKLabel;
    private final Label vitaminB1Label;
    private final Label vitaminB2Label;
    private final Label vitaminB3Label;
    private final Label vitaminB5Label;
    private final Label vitaminB6Label;
    private final Label vitaminB7Label;
    private final Label vitaminB8Label;
    private final Label vitaminB9Label;
    private final Label vitaminB12Label;

    /**
     * Creates a new micronutrient breakdown pane
     *
     * @param entity The entity this screen section will display the information of
     */
    public MicronutrientBreakdownScreenSection(T entity)
    {
        this.entity = entity;

        cholesterolLabel = new Label();
        ironLabel = new Label();
        manganeseLabel = new Label();
        copperLabel = new Label();
        zincLabel = new Label();
        calciumLabel = new Label();
        magnesiumLabel = new Label();
        phosphorusLabel = new Label();
        potassiumLabel = new Label();
        sodiumLabel = new Label();
        sulfurLabel = new Label();
        vitaminALabel = new Label();
        vitaminCLabel = new Label();
        vitaminDLabel = new Label();
        vitaminELabel = new Label();
        vitaminKLabel = new Label();
        vitaminB1Label = new Label();
        vitaminB2Label = new Label();
        vitaminB3Label = new Label();
        vitaminB5Label = new Label();
        vitaminB6Label = new Label();
        vitaminB7Label = new Label();
        vitaminB8Label = new Label();
        vitaminB9Label = new Label();
        vitaminB12Label = new Label();
    }

    /*
     * Though there are actually differences in content and what precisely they display both macro
     * and micro sections share the same layout so this gets detected as duplicate code
     *
     * Technically the warning isn't wrong. We could base class to avoid the duplication. But
     * I don't want or see any real need to do that here.
     */
    @SuppressWarnings("DuplicatedCode")
    @Override
    public Node create()
    {
        TitledPane microBreakdownPane = new TitledPane();
        microBreakdownPane.setText("Micronutrient Breakdown");
        VBox.setMargin(microBreakdownPane, LayoutUtils.getSectionElementVerticalSpacing());

        VBox paneContentContainer = new VBox(10);

        HBox grandTotalsHeader = getSubSectionHeader("GRAND TOTALS");
        GridPane grandTotalsGridPane = setupGrandTotalsGrid();
        paneContentContainer.getChildren().addAll(grandTotalsHeader, grandTotalsGridPane);

        // Ingredients section isn't shown if there isn't support for them
        if (entity instanceof IngredientReader ingredientOwner)
        {
            HBox individualIngredientsHeader = getSubSectionHeader("INDIVIDUAL INGREDIENTS");
            TableView<IngredientModel> ingredientMicrosTable = setupMicronutrientsPerIngredientTableView(ingredientOwner.getIngredients());
            paneContentContainer.getChildren().addAll(individualIngredientsHeader, ingredientMicrosTable);
        }

        microBreakdownPane.setContent(paneContentContainer);
        return microBreakdownPane;
    }

    @Override
    public void destroy()
    {
        List<Label> boundScreenLabels = List.of(
                cholesterolLabel,
                ironLabel,
                manganeseLabel,
                copperLabel,
                zincLabel,
                calciumLabel,
                magnesiumLabel,
                phosphorusLabel,
                potassiumLabel,
                sodiumLabel,
                sulfurLabel,
                vitaminALabel,
                vitaminCLabel,
                vitaminDLabel,
                vitaminELabel,
                vitaminKLabel,
                vitaminB1Label,
                vitaminB2Label,
                vitaminB3Label,
                vitaminB5Label,
                vitaminB6Label,
                vitaminB7Label,
                vitaminB8Label,
                vitaminB9Label,
                vitaminB12Label
        );

        // There, code dup warning, we're still doing it once per label so please be quiet now
        for (Label label : boundScreenLabels)
        {
            label.textProperty().unbind();
        }
    }

    private GridPane setupGrandTotalsGrid()
    {
        GridPane grandTotalsContainer = new GridPane();
        grandTotalsContainer.setHgap(20);
        grandTotalsContainer.setVgap(10);

        // Lipids
        Label lipidsSubHeader = getContentHeaderLabel("Lipids", true);
        Label cholesterolHeader = getContentHeaderLabel("Cholesterol", false);
        cholesterolLabel.textProperty().bind(Bindings.convert(entity.cholesterolProperty()));

        grandTotalsContainer.add(lipidsSubHeader, 0, 0);
        grandTotalsContainer.add(cholesterolHeader, 0, 1);
        grandTotalsContainer.add(cholesterolLabel, 0, 2);

        // Minerals
        Label mineralsSubHeader = getContentHeaderLabel("Minerals", true);
        grandTotalsContainer.add(mineralsSubHeader, 0, 3);

        Label ironHeader = getContentHeaderLabel("Iron", false);
        ironLabel.textProperty().bind(Bindings.convert(entity.ironProperty()));
        grandTotalsContainer.add(ironHeader, 0, 4);
        grandTotalsContainer.add(ironLabel, 0, 5);

        Label manganeseHeader = getContentHeaderLabel("Manganese", false);
        manganeseLabel.textProperty().bind(Bindings.convert(entity.manganeseProperty()));
        grandTotalsContainer.add(manganeseHeader, 1, 4);
        grandTotalsContainer.add(manganeseLabel, 1, 5);

        Label copperHeader = getContentHeaderLabel("Copper", false);
        copperLabel.textProperty().bind(Bindings.convert(entity.copperProperty()));
        grandTotalsContainer.add(copperHeader, 2, 4);
        grandTotalsContainer.add(copperLabel, 2, 5);

        Label zincHeader = getContentHeaderLabel("Zinc", false);
        zincLabel.textProperty().bind(Bindings.convert(entity.zincProperty()));
        grandTotalsContainer.add(zincHeader, 3, 4);
        grandTotalsContainer.add(zincLabel, 3, 5);

        Label calciumHeader = getContentHeaderLabel("Calcium", false);
        calciumLabel.textProperty().bind(Bindings.convert(entity.calciumProperty()));
        grandTotalsContainer.add(calciumHeader, 4, 4);
        grandTotalsContainer.add(calciumLabel, 4, 5);

        Label magnesiumHeader = getContentHeaderLabel("Magnesium", false);
        magnesiumLabel.textProperty().bind(Bindings.convert(entity.magnesiumProperty()));
        grandTotalsContainer.add(magnesiumHeader, 0, 6);
        grandTotalsContainer.add(magnesiumLabel, 0, 7);

        Label phosphorusHeader = getContentHeaderLabel("Phosphorus", false);
        phosphorusLabel.textProperty().bind(Bindings.convert(entity.phosphorusProperty()));
        grandTotalsContainer.add(phosphorusHeader, 1, 6);
        grandTotalsContainer.add(phosphorusLabel, 1, 7);

        Label potassiumHeader = getContentHeaderLabel("Potassium", false);
        potassiumLabel.textProperty().bind(Bindings.convert(entity.potassiumProperty()));
        grandTotalsContainer.add(potassiumHeader, 2, 6);
        grandTotalsContainer.add(potassiumLabel, 2, 7);

        Label sodiumHeader = getContentHeaderLabel("Sodium", false);
        sodiumLabel.textProperty().bind(Bindings.convert(entity.sodiumProperty()));
        grandTotalsContainer.add(sodiumHeader, 3, 6);
        grandTotalsContainer.add(sodiumLabel, 3, 7);

        Label sulfurHeader = getContentHeaderLabel("Sulfur", false);
        sulfurLabel.textProperty().bind(Bindings.convert(entity.sulfurProperty()));
        grandTotalsContainer.add(sulfurHeader, 4, 6);
        grandTotalsContainer.add(sulfurLabel, 4, 7);

        // Vitamins
        Label vitaminsSubHeader = getContentHeaderLabel("Vitamins", true);
        grandTotalsContainer.add(vitaminsSubHeader, 0, 8);

        Label vitaminAHeader = getContentHeaderLabel("Vitamin A", false);
        vitaminALabel.textProperty().bind(Bindings.convert(entity.vitaminAProperty()));
        grandTotalsContainer.add(vitaminAHeader, 0, 9);
        grandTotalsContainer.add(vitaminALabel, 0, 10);

        Label vitaminCHeader = getContentHeaderLabel("Vitamin C", false);
        vitaminCLabel.textProperty().bind(Bindings.convert(entity.vitaminCProperty()));
        grandTotalsContainer.add(vitaminCHeader, 1, 9);
        grandTotalsContainer.add(vitaminCLabel, 1, 10);

        Label vitaminDHeader = getContentHeaderLabel("Vitamin D", false);
        vitaminDLabel.textProperty().bind(Bindings.convert(entity.vitaminDProperty()));
        grandTotalsContainer.add(vitaminDHeader, 2, 9);
        grandTotalsContainer.add(vitaminDLabel, 2, 10);

        Label vitaminEHeader = getContentHeaderLabel("Vitamin E", false);
        vitaminELabel.textProperty().bind(Bindings.convert(entity.vitaminEProperty()));
        grandTotalsContainer.add(vitaminEHeader, 3, 9);
        grandTotalsContainer.add(vitaminELabel, 3, 10);

        Label vitaminKHeader = getContentHeaderLabel("Vitamin K", false);

        vitaminKLabel.textProperty().bind(Bindings.convert(entity.vitaminKProperty()));
        grandTotalsContainer.add(vitaminKHeader, 4, 9);
        grandTotalsContainer.add(vitaminKLabel, 4, 10);

        Label vitaminB1Header = getContentHeaderLabel("Vitamin B1", false);
        vitaminB1Label.textProperty().bind(Bindings.convert(entity.vitaminB1Property()));
        grandTotalsContainer.add(vitaminB1Header, 0, 11);
        grandTotalsContainer.add(vitaminB1Label, 0, 12);

        Label vitaminB2Header = getContentHeaderLabel("Vitamin B2", false);
        vitaminB2Label.textProperty().bind(Bindings.convert(entity.vitaminB2Property()));
        grandTotalsContainer.add(vitaminB2Header, 1, 11);
        grandTotalsContainer.add(vitaminB2Label, 1, 12);

        Label vitaminB3Header = getContentHeaderLabel("Vitamin B3", false);
        vitaminB3Label.textProperty().bind(Bindings.convert(entity.vitaminB3Property()));
        grandTotalsContainer.add(vitaminB3Header, 2, 11);
        grandTotalsContainer.add(vitaminB3Label, 2, 12);

        Label vitaminB5Header = getContentHeaderLabel("Vitamin B5", false);
        vitaminB5Label.textProperty().bind(Bindings.convert(entity.vitaminB5Property()));
        grandTotalsContainer.add(vitaminB5Header, 3, 11);
        grandTotalsContainer.add(vitaminB5Label, 3, 12);

        Label vitaminB6Header = getContentHeaderLabel("Vitamin B6", false);
        vitaminB6Label.textProperty().bind(Bindings.convert(entity.vitaminB6Property()));
        grandTotalsContainer.add(vitaminB6Header, 4, 11);
        grandTotalsContainer.add(vitaminB6Label, 4, 12);

        Label vitaminB7Header = getContentHeaderLabel("Vitamin B7", false);
        vitaminB7Label.textProperty().bind(Bindings.convert(entity.vitaminB7Property()));
        grandTotalsContainer.add(vitaminB7Header, 0, 13);
        grandTotalsContainer.add(vitaminB7Label, 0, 14);

        Label vitaminB8Header = getContentHeaderLabel("Vitamin B8", false);
        vitaminB8Label.textProperty().bind(Bindings.convert(entity.vitaminB8Property()));
        grandTotalsContainer.add(vitaminB8Header, 1, 13);
        grandTotalsContainer.add(vitaminB8Label, 1, 14);

        Label vitaminB9Header = getContentHeaderLabel("Vitamin B9", false);
        vitaminB9Label.textProperty().bind(Bindings.convert(entity.vitaminB9Property()));
        grandTotalsContainer.add(vitaminB9Header, 2, 13);
        grandTotalsContainer.add(vitaminB9Label, 2, 14);

        Label vitaminB12Header = getContentHeaderLabel("Vitamin B12", false);
        vitaminB12Label.textProperty().bind(Bindings.convert(entity.vitaminB12Property()));
        grandTotalsContainer.add(vitaminB12Header, 3, 13);
        grandTotalsContainer.add(vitaminB12Label, 3, 14);

        return grandTotalsContainer;
    }

    private TableView<IngredientModel> setupMicronutrientsPerIngredientTableView(ObservableList<IngredientModel> ingredients)
    {
        TableView<IngredientModel> microTotalsTable = new TableView<>();
        microTotalsTable.setPlaceholder(new Label("No ingredient data to display"));

        TableColumn<IngredientModel, String> nameCol = new TableColumn<>("Name");
        nameCol.setCellValueFactory(cellDataFeatures -> cellDataFeatures.getValue().getIngredientFood().nameProperty());

        TableColumn<IngredientModel, BigDecimal> cholesterolTotalCol = new TableColumn<>("Cholesterol");
        TableColumn<IngredientModel, BigDecimal> ironTotalCol = new TableColumn<>("Iron");
        TableColumn<IngredientModel, BigDecimal> manganeseTotalCol = new TableColumn<>("Manganese");
        TableColumn<IngredientModel, BigDecimal> copperTotalCol = new TableColumn<>("Copper");
        TableColumn<IngredientModel, BigDecimal> zincTotalCol = new TableColumn<>("Zinc");
        TableColumn<IngredientModel, BigDecimal> calciumTotalCol = new TableColumn<>("Calcium");
        TableColumn<IngredientModel, BigDecimal> magnesiumTotalCol = new TableColumn<>("Magnesium");
        TableColumn<IngredientModel, BigDecimal> phosphorusTotalCol = new TableColumn<>("Phosphorus");
        TableColumn<IngredientModel, BigDecimal> potassiumTotalCol = new TableColumn<>("Potassium");
        TableColumn<IngredientModel, BigDecimal> sodiumTotalCol = new TableColumn<>("Sodium");
        TableColumn<IngredientModel, BigDecimal> sulfurTotalCol = new TableColumn<>("Sulfur");

        TableColumn<IngredientModel, BigDecimal> vitATotalCol = new TableColumn<>("Vitamin A");
        TableColumn<IngredientModel, BigDecimal> vitCTotalCol = new TableColumn<>("Vitamin C");
        TableColumn<IngredientModel, BigDecimal> vitKTotalCol = new TableColumn<>("Vitamin K");
        TableColumn<IngredientModel, BigDecimal> vitDTotalCol = new TableColumn<>("Vitamin D");
        TableColumn<IngredientModel, BigDecimal> vitETotalCol = new TableColumn<>("Vitamin E");
        TableColumn<IngredientModel, BigDecimal> vitB1TotalCol = new TableColumn<>("Vitamin B1");
        TableColumn<IngredientModel, BigDecimal> vitB2TotalCol = new TableColumn<>("Vitamin B2");
        TableColumn<IngredientModel, BigDecimal> vitB3TotalCol = new TableColumn<>("Vitamin B3");
        TableColumn<IngredientModel, BigDecimal> vitB5TotalCol = new TableColumn<>("Vitamin B5");
        TableColumn<IngredientModel, BigDecimal> vitB6TotalCol = new TableColumn<>("Vitamin B6");
        TableColumn<IngredientModel, BigDecimal> vitB7TotalCol = new TableColumn<>("Vitamin B7");
        TableColumn<IngredientModel, BigDecimal> vitB8TotalCol = new TableColumn<>("Vitamin B8");
        TableColumn<IngredientModel, BigDecimal> vitB9TotalCol = new TableColumn<>("Vitamin B9");
        TableColumn<IngredientModel, BigDecimal> vitB12TotalCol = new TableColumn<>("Vitamin B12");

        cholesterolTotalCol.setCellValueFactory(new PropertyValueFactory<>("cholesterol"));
        ironTotalCol.setCellValueFactory(new PropertyValueFactory<>("iron"));
        manganeseTotalCol.setCellValueFactory(new PropertyValueFactory<>("manganese"));
        copperTotalCol.setCellValueFactory(new PropertyValueFactory<>("copper"));
        zincTotalCol.setCellValueFactory(new PropertyValueFactory<>("zinc"));
        calciumTotalCol.setCellValueFactory(new PropertyValueFactory<>("calcium"));
        magnesiumTotalCol.setCellValueFactory(new PropertyValueFactory<>("magnesium"));
        phosphorusTotalCol.setCellValueFactory(new PropertyValueFactory<>("phosphorus"));
        potassiumTotalCol.setCellValueFactory(new PropertyValueFactory<>("potassium"));
        sodiumTotalCol.setCellValueFactory(new PropertyValueFactory<>("sodium"));
        sulfurTotalCol.setCellValueFactory(new PropertyValueFactory<>("sulfur"));

        vitATotalCol.setCellValueFactory(new PropertyValueFactory<>("vitaminA"));
        vitCTotalCol.setCellValueFactory(new PropertyValueFactory<>("vitaminC"));
        vitKTotalCol.setCellValueFactory(new PropertyValueFactory<>("vitaminK"));
        vitDTotalCol.setCellValueFactory(new PropertyValueFactory<>("vitaminD"));
        vitETotalCol.setCellValueFactory(new PropertyValueFactory<>("vitaminE"));
        vitB1TotalCol.setCellValueFactory(new PropertyValueFactory<>("vitaminB1"));
        vitB2TotalCol.setCellValueFactory(new PropertyValueFactory<>("vitaminB2"));
        vitB3TotalCol.setCellValueFactory(new PropertyValueFactory<>("vitaminB3"));
        vitB5TotalCol.setCellValueFactory(new PropertyValueFactory<>("vitaminB5"));
        vitB6TotalCol.setCellValueFactory(new PropertyValueFactory<>("vitaminB6"));
        vitB7TotalCol.setCellValueFactory(new PropertyValueFactory<>("vitaminB7"));
        vitB8TotalCol.setCellValueFactory(new PropertyValueFactory<>("vitaminB8"));
        vitB9TotalCol.setCellValueFactory(new PropertyValueFactory<>("vitaminB9"));
        vitB12TotalCol.setCellValueFactory(new PropertyValueFactory<>("vitaminB12"));

        microTotalsTable.getColumns().addAll(
                nameCol,
                cholesterolTotalCol,
                ironTotalCol,
                manganeseTotalCol,
                copperTotalCol,
                zincTotalCol,
                calciumTotalCol,
                magnesiumTotalCol,
                phosphorusTotalCol,
                potassiumTotalCol,
                sodiumTotalCol,
                sulfurTotalCol,
                vitATotalCol,
                vitKTotalCol,
                vitCTotalCol,
                vitDTotalCol,
                vitETotalCol,
                vitB1TotalCol,
                vitB2TotalCol,
                vitB3TotalCol,
                vitB5TotalCol,
                vitB6TotalCol,
                vitB7TotalCol,
                vitB8TotalCol,
                vitB9TotalCol,
                vitB12TotalCol
        );

        microTotalsTable.setItems(ingredients);

        return microTotalsTable;
    }

    /**
     * Convenience function for wrapping some header text in an HBox with a white border
     * to demarcate it as a header
     * @param subSectionTitle The text to wrap
     * @return HBox with a white border
     */
    // Both macro and micro breakdown sections share this convenience function
    @SuppressWarnings("DuplicatedCode")
    private HBox getSubSectionHeader(String subSectionTitle)
    {
        Label label = new Label(subSectionTitle);
        label.getStyleClass().add(CssConstants.CLASS_TEXT_SUB_HEADER);
        HBox container = new HBox();
        container.setMaxWidth(Double.MAX_VALUE);
        container.setPadding(LayoutUtils.getSectionElementVerticalSpacing());
        container.getStyleClass().add(CssConstants.CLASS_BORDER_WHITE);
        container.setAlignment(Pos.CENTER);
        HBox.setHgrow(container, Priority.ALWAYS);
        container.getChildren().add(label);
        return container;
    }

    /**
     * Convenience function for getting a label with the appropriate classes applied
     * @param labelText The text of the header label
     * @param isSubHeader If the label is one of the "Lipids", "Minerals", or "Vitamins" subheaders
     * @return A Label with the appropriate classes applied
     */
    private Label getContentHeaderLabel(String labelText, boolean isSubHeader)
    {
        Label label = new Label(labelText);

        if (isSubHeader)
        {
            label.getStyleClass().add(CssConstants.CLASS_ITALICIZE);
        }
        else
        {
            label.getStyleClass().addAll(CssConstants.CLASS_BOLD, CssConstants.CLASS_UNDERLINE);
        }

        return label;
    }
}
