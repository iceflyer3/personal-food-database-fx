/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.controller.dialog;

import com.iceflyer3.pfd.constants.TextConstants;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Dialog that shows the legal disclaimer and license notice upon application launch.
 *
 * This class extends AbstractController instead of AbstractDialogController because it is purely
 * informational and does not need support for callbacks or the ability to receive data like most
 * other dialogs need.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class DisclaimerDialogController extends AbstractDialogController<Void>
{
    @FXML
    public VBox rootContainer;

    @FXML
    public Button acknowledgeButton;

    public DisclaimerDialogController(WindowManager windowManager)
    {
        super(windowManager);
    }

    @Override
    public void initialize()
    {
        Image pfdGplV3Logo = new Image("/license/pfd_gplv3_logo.png");
        ImageView pfdGplV3LogoImageView = new ImageView(pfdGplV3Logo);
        rootContainer.getChildren().add(1, pfdGplV3LogoImageView);

        Label licenseLabel = new Label(TextConstants.COPYRIGHT_AND_LICENSE_NOTICES);
        licenseLabel.setWrapText(true);
        rootContainer.getChildren().add(2, licenseLabel);

        Label legalDisclaimerWarrantyLabel = new Label(TextConstants.LEGAL_DISCLAIMER_WARRANTY_NOTICES);
        legalDisclaimerWarrantyLabel.setWrapText(true);
        rootContainer.getChildren().add(3, legalDisclaimerWarrantyLabel);
    }

    @Override
    protected void registerDefaultListeners()
    {
        acknowledgeButton.setOnAction(event -> {
            windowManager.close(WindowManager.DIALOG_WINDOW);
        });
    }
}
