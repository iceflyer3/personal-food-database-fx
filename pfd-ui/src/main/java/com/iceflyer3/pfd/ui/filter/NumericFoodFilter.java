package com.iceflyer3.pfd.ui.filter;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.ui.enums.FoodFilterProperty;
import com.iceflyer3.pfd.util.NumberUtils;
import com.iceflyer3.pfd.validation.ComparisonOperator;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;

import java.math.BigDecimal;

/**
 * A food filter to be used with numeric properties of a food such as the number of calories.
 */

public class NumericFoodFilter implements Filter<BigDecimal> {

    private final BooleanProperty active;
    private final ObjectProperty<BigDecimal> searchValue;
    private final ObjectProperty<ComparisonOperator> operator;
    private final ObjectProperty<FoodFilterProperty> foodProperty;

    public NumericFoodFilter(FoodFilterProperty property, ComparisonOperator operator) {
        this.foodProperty = new SimpleObjectProperty<>(property);
        this.operator = new SimpleObjectProperty<>(operator);
        this.active = new SimpleBooleanProperty(true);
        this.searchValue = new SimpleObjectProperty<>(NumberUtils.withStandardPrecision(0));
    }

    public FoodFilterProperty getFoodProperty() {
        return foodProperty.get();
    }

    public ObjectProperty<FoodFilterProperty> foodPropertyProperty() {
        return foodProperty;
    }

    public ComparisonOperator getOperator() {
        return operator.get();
    }

    public ObjectProperty<ComparisonOperator> operatorProperty() {
        return operator;
    }

    public BigDecimal getSearchValue() {
        return searchValue.get();
    }

    public void setSearchValue(BigDecimal searchValue) {
        this.searchValue.set(searchValue);
    }

    public ObjectProperty<BigDecimal> searchValueProperty() {
        return searchValue;
    }

    public boolean isActive() {
        return active.get();
    }

    public BooleanProperty activeProperty() {
        return active;
    }

    public void toggleActiveStatus() {
        active.set(!active.get());
    }

    @Override
    public boolean apply(BigDecimal sourceValue) {

        if (active.get())
        {
            // BigDecimal.compareTo returns -1 for less than, 0 for equal to, and 1 for greater than
            switch(operator.get())
            {
                case EQUALS:
                    return sourceValue.compareTo(searchValue.get()) == 0;
                case GREATER_THAN:
                    return sourceValue.compareTo(searchValue.get()) > 0;
                case GREATER_THAN_OR_EQUAL:
                    return sourceValue.compareTo(searchValue.get()) >= 0;
                case LESS_THAN:
                    return sourceValue.compareTo(searchValue.get()) < 0;
                case LESS_THAN_OR_EQUAL:
                    return sourceValue.compareTo(searchValue.get()) <= 0;
                default:
                    throw new UnsupportedOperationException(String.format("Operation %s is not supported with numeric filters", operator.get()));
            }
        }
        else
        {
            // If the filter is not active then it shouldn't apply so it automatically passes
            return true;
        }
    }

    @Override
    public String toString() {
        return "NumericFoodFilter{" +
                "active=" + active +
                ", searchValue=" + searchValue +
                ", operator=" + operator +
                ", foodProperty=" + foodProperty +
                '}';
    }
}
