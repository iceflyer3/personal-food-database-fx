package com.iceflyer3.pfd.ui.filter;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.validation.ComparisonOperator;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class FoodFilterRequest {

    // This will be either OR or AND
    private final ObjectProperty<ComparisonOperator> logicalOperation;

    private final ObservableList<TextFoodFilter> textFilters;
    private final ObservableList<NumericFoodFilter> numericFilters;

    public FoodFilterRequest(ComparisonOperator logicalOperation) {
        this.logicalOperation = new SimpleObjectProperty<>(logicalOperation);
        this.textFilters = FXCollections.observableArrayList();
        this.numericFilters = FXCollections.observableArrayList();
    }

    public ComparisonOperator getLogicalOperation() {
        return logicalOperation.get();
    }

    public ObjectProperty<ComparisonOperator> logicalOperationProperty() {
        return logicalOperation;
    }

    public ObservableList<TextFoodFilter> getTextFilters() {
        return textFilters;
    }

    public ObservableList<NumericFoodFilter> getNumericFilters() {
        return numericFilters;
    }

    @Override
    public String toString() {
        return "FoodFilterRequest{" +
                "logicalOperation=" + logicalOperation +
                ", textFilters=" + textFilters +
                ", numericFilters=" + numericFilters +
                '}';
    }
}
