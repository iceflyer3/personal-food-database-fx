package com.iceflyer3.pfd.ui.controller.sections;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.constants.CssConstants;
import com.iceflyer3.pfd.constants.FontAwesomeIcons;
import com.iceflyer3.pfd.ui.cell.ActionButtonProvider;
import com.iceflyer3.pfd.ui.cell.ActionButtonTableCell;
import com.iceflyer3.pfd.ui.cell.EditableBigDecimalTableCell;
import com.iceflyer3.pfd.ui.enums.PfdActionType;
import com.iceflyer3.pfd.ui.listener.Destroyable;
import com.iceflyer3.pfd.ui.listener.ObservableValueListenerBinding;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.api.NutritionalSummaryReporter;
import com.iceflyer3.pfd.ui.model.api.owner.IngredientWriter;
import com.iceflyer3.pfd.ui.model.proxy.food.ReadOnlyFoodModelProxy;
import com.iceflyer3.pfd.ui.util.LayoutUtils;
import com.iceflyer3.pfd.ui.util.builder.ActionNodeBuilder;
import javafx.beans.property.ObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import org.controlsfx.control.textfield.AutoCompletionBinding;
import org.controlsfx.control.textfield.TextFields;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * This screen section is a Titled Pane that features an auto-complete for textual search of foods by name
 * or brand and a table of added ingredients where you can adjust ingredient details.
 * @param <T> The type of the owner of the ingredients in the list
 */
public class EditIngredientScreenSection<T extends IngredientWriter & NutritionalSummaryReporter> implements ScreenSection, Destroyable
{
    private final T entity;
    private final List<ReadOnlyFoodModelProxy> masterFoodList;
    private final Set<ObservableValueListenerBinding<BigDecimal>> servingsIncludedPropertyBindings;

    private List<ReadOnlyFoodModelProxy> filteredAutocompleteChoices;

    /**
     * Creates a new ingredients pane.
     *
     * @param autocompleteFoodsList The list of foods to be used in the auto-complete when adding new ingredients
     * @param entity The entity this screen section will display the information of
     */
    public EditIngredientScreenSection(List<ReadOnlyFoodModelProxy> autocompleteFoodsList, T entity)
    {
        this.entity = entity;
        this.masterFoodList = autocompleteFoodsList;
        this.servingsIncludedPropertyBindings = new HashSet<>();

        /*
         * Initialize the list of autocomplete ingredients
         *
         * When editing a previously saved entity that already has ingredients this prevents
         * the already existing ingredients from being duplicated in the candidate ingredinet
         * results list for the first search
         */
        this.filteredAutocompleteChoices = autocompleteFoodsList;
        refreshAutocompleteChoices();
    }

    @Override
    public Node create()
    {
        TitledPane ingredientsPane = new TitledPane();
        ingredientsPane.setText("Ingredients");
        VBox.setMargin(ingredientsPane, LayoutUtils.getSectionElementVerticalSpacing());

        Label ingredientSearchLabel = new Label("Ingredient Search");
        Label addedIngredientsLabel = new Label("Added Ingredients");
        addedIngredientsLabel.getStyleClass().addAll(CssConstants.CLASS_BOLD, CssConstants.CLASS_UNDERLINE);
        VBox.setMargin(addedIngredientsLabel, new Insets(20, 0, 0, 0));

        TextField autocompleteTextField = setupAutocompleteTextBox();
        TableView<IngredientModel> ingredientsTable = setupIngredientsTable();

        VBox container = new VBox(10);
        container.getChildren().addAll(ingredientSearchLabel, autocompleteTextField, addedIngredientsLabel, ingredientsTable);

        ingredientsPane.setContent(container);
        return ingredientsPane;
    }

    @Override
    public void destroy()
    {
        this.servingsIncludedPropertyBindings.forEach(binding -> binding.unbind());
        this.servingsIncludedPropertyBindings.clear();
    }

    private TextField setupAutocompleteTextBox()
    {
        // Setup the autocomplete and refresh the binding. Apparently using an ObservableList doesn't work.
        TextField autoCompleteTextField = new TextField();
        autoCompleteTextField.setPromptText("Search for a food by food or brand name to add a new ingredient");
        Callback<AutoCompletionBinding.ISuggestionRequest, Collection<ReadOnlyFoodModelProxy>> suggestionProvider = new Callback<AutoCompletionBinding.ISuggestionRequest, Collection<ReadOnlyFoodModelProxy>>() {
            @Override
            public Collection<ReadOnlyFoodModelProxy> call(AutoCompletionBinding.ISuggestionRequest param) {
                // TODO: It'd be nice if we had some kind of default "no results found" result here instead of just displaying nothing.
                //       Not sure how we'd go about that within the constraints of ControlsFx though.
                String userText = param.getUserText();
                return filteredAutocompleteChoices.stream().filter(candidateFood -> candidateFood.getName().toLowerCase().contains(userText.toLowerCase())).collect(Collectors.toList());
            }
        };

        AutoCompletionBinding<ReadOnlyFoodModelProxy> binding = TextFields.bindAutoCompletion(autoCompleteTextField, suggestionProvider);
        binding.setHideOnEscape(true);
        binding.setOnAutoCompleted(new EventHandler<AutoCompletionBinding.AutoCompletionEvent<ReadOnlyFoodModelProxy>>() {
            @Override
            public void handle(AutoCompletionBinding.AutoCompletionEvent<ReadOnlyFoodModelProxy> event) {
                // Add the new ingredient, refresh nutritional details, and then register a listener
                // for changes to the serving size so that nutritional details may be refreshed upon change
                entity.addIngredient(event.getCompletion());
                entity.refreshNutritionDetails();
                IngredientModel mostRecentIngredient = entity.getIngredients().get(entity.getIngredients().size() - 1);
                addServingsIncludedPropertyListener(mostRecentIngredient.servingsIncludedProperty());

                autoCompleteTextField.clear();
                refreshAutocompleteChoices();
            }
        });

        return autoCompleteTextField;
    }

    private TableView<IngredientModel> setupIngredientsTable()
    {
        // Setup handler for remove button
        EventHandler<ActionEvent> removeServingHandler = event -> {
            IngredientModel ingredientToRemove = (IngredientModel) ((Node)event.getSource()).getUserData();
            entity.removeIngredient(ingredientToRemove);
            entity.refreshNutritionDetails();
            refreshAutocompleteChoices();
        };

        ActionButtonProvider<IngredientModel> buttonProvider = rowData -> {
            List<Button> buttonList = new ArrayList<>();
            Button deleteButton = ActionNodeBuilder
                    .createIconButton(null,  FontAwesomeIcons.TIMES, 16)
                    .forActionType(PfdActionType.DANGER)
                    .withAction(removeServingHandler)
                    .build();
            buttonList.add(deleteButton);
            return buttonList;
        };

        // Listen for changes to the serving sizes for any ingredients that already exist.
        // Listeners for new ingredients are added at the time of addition to the list
        for (IngredientModel ingredient : entity.getIngredients())
        {
            addServingsIncludedPropertyListener(ingredient.servingsIncludedProperty());
        }

        // Setup table
        TableView<IngredientModel> ingredientsTableView = new TableView<>();
        ingredientsTableView.setEditable(true);
        ingredientsTableView.setPlaceholder(new Label("You must have at least two ingredients before you may save!"));

        TableColumn<IngredientModel, String> foodNameColumn = new TableColumn<>("Food Name");
        foodNameColumn.setCellValueFactory(cellDataFeatures -> cellDataFeatures.getValue().getIngredientFood().nameProperty());

        TableColumn<IngredientModel, String> brandNameColumn = new TableColumn<>("Food Brand");
        brandNameColumn.setCellValueFactory(cellDataFeatures -> cellDataFeatures.getValue().getIngredientFood().brandProperty());

        TableColumn<IngredientModel, Boolean> isAlternateColumn = new TableColumn<>("Alternate?");
        isAlternateColumn.setCellFactory(col -> new CheckBoxTableCell<>());
        isAlternateColumn.setCellValueFactory(new PropertyValueFactory<>("isAlternate"));

        TableColumn<IngredientModel, Boolean> isOptionalColumn = new TableColumn<>("Optional?");
        isOptionalColumn.setCellFactory(col -> new CheckBoxTableCell<>());
        isOptionalColumn.setCellValueFactory(new PropertyValueFactory<>("isOptional"));

        TableColumn<IngredientModel, BigDecimal> servingsIncludedColumn = new TableColumn<>("# servings included");
        servingsIncludedColumn.setCellFactory(col -> new EditableBigDecimalTableCell<>());
        servingsIncludedColumn.setCellValueFactory(new PropertyValueFactory<>("servingsIncluded"));

        TableColumn<IngredientModel, BigDecimal> actionButtonColumn  = new TableColumn<>("Actions");
        actionButtonColumn.setCellFactory(col -> new ActionButtonTableCell<>(buttonProvider));
        actionButtonColumn.setCellValueFactory(new PropertyValueFactory<>("servingsIncluded"));
        actionButtonColumn.setEditable(false);

        ingredientsTableView.getColumns().addAll(
                foodNameColumn,
                brandNameColumn,
                isAlternateColumn,
                isOptionalColumn,
                servingsIncludedColumn,
                actionButtonColumn
        );

        ingredientsTableView.setItems(entity.getIngredients());

        return ingredientsTableView;
    }

    private void refreshAutocompleteChoices()
    {
        if (masterFoodList.size() > 0)
        {
            // Find all foods not already in the list of ingredients
            this.filteredAutocompleteChoices = masterFoodList
                    .stream()
                    .filter(food ->
                            entity
                                    .getIngredients()
                                    .stream()
                                    .noneMatch(ingredient -> ingredient.getIngredientFood().getId().equals(food.getId()))
                    )
                    .collect(Collectors.toList());

        }
    }

    private void addServingsIncludedPropertyListener(ObjectProperty<BigDecimal> servingsIncludedProperty)
    {
        ObservableValueListenerBinding<BigDecimal> listenerBinding = new ObservableValueListenerBinding<>(servingsIncludedProperty);
        ChangeListener<BigDecimal> changeListener = (observable, oldValue, newValue) -> entity.refreshNutritionDetails();
        listenerBinding.bindChangeListener(changeListener);
        servingsIncludedPropertyBindings.add(listenerBinding);
    }
}
