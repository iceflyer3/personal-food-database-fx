/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.controller;

import com.google.common.eventbus.EventBus;
import com.iceflyer3.pfd.PfdApplication;
import com.iceflyer3.pfd.constants.ApplicationSections;
import com.iceflyer3.pfd.constants.FontAwesomeIcons;
import com.iceflyer3.pfd.constants.SceneSets;
import com.iceflyer3.pfd.ui.Breadcrumb;
import com.iceflyer3.pfd.ui.event.ReturnToProfileSelectEvent;
import com.iceflyer3.pfd.ui.cell.LeftHandNavigationItemListCell;
import com.iceflyer3.pfd.ui.listener.Destroyable;
import com.iceflyer3.pfd.ui.listener.ObservableListListenerBinding;
import com.iceflyer3.pfd.ui.listener.ObservableValueListenerBinding;
import com.iceflyer3.pfd.ui.manager.BreadcrumbManager;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.util.ControllerUtils;
import com.iceflyer3.pfd.ui.util.FxmlLoadResult;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
// This particular controller is only ever loaded in the "main" stage that is the primary application window
public class HomeScreenController extends AbstractHomeController implements Destroyable
{

    private static final Logger LOG = LoggerFactory.getLogger(HomeScreenController.class);

    private ObservableValueListenerBinding<String> leftNavBinding;
    private ObservableListListenerBinding<Breadcrumb> breadcrumbsBinding;

    @FXML
    public StackPane homeRootContainer;

    @FXML
    private Label userIconLabel;

    @FXML
    private Label usernameLabel;

    @FXML
    private Hyperlink changeUserLink;

    @FXML
    private HBox breadcrumbsHbox;

    @FXML
    private ListView<String> leftNavListView;

    public HomeScreenController(EventBus eventBus, BreadcrumbManager breadcrumbManager, WindowManager windowManager, ControllerUtils controllerUtils)
    {
        super(eventBus, breadcrumbManager, windowManager, controllerUtils);
    }

    public void initUser(UserViewModel user)
    {
        this.user = user;
        this.usernameLabel.setText(user.getUsername());
    }

    @Override
    public void initialize() {
        initRootContainer(homeRootContainer);
        initHostingStage(PfdApplication.getApplicationRootStage(), PfdApplication.MAIN_STAGE_NAME);

        ObservableList<String> leftNavItems = FXCollections.observableArrayList("Food Database", "Recipes", "Meal Planning", "Settings", "About");
        this.leftNavListView.setItems(leftNavItems);
        this.leftNavListView.setCellFactory(col -> new LeftHandNavigationItemListCell(windowManager, user));

        leftNavBinding = new ObservableValueListenerBinding<>(leftNavListView.getSelectionModel().selectedItemProperty());
        leftNavBinding.bindChangeListener((observableValue, previousAppSectionListLabel, selectedAppSectionListLabel) -> {

            // Map from the left nav label to the ApplicationSections constant
            String selectedApplicationSection;
            switch(selectedAppSectionListLabel)
            {
                case "Food Database":
                    selectedApplicationSection = ApplicationSections.FOOD_DATABASE;
                    break;
                case "Recipes":
                    selectedApplicationSection = ApplicationSections.RECIPES;
                    break;
                case "Meal Planning":
                    selectedApplicationSection = ApplicationSections.MEAL_PLANNING;
                    break;
                case "Settings":
                    selectedApplicationSection = ApplicationSections.SETTINGS;
                    break;
                case "About":
                    selectedApplicationSection = ApplicationSections.ABOUT;
                    break;
                default:
                    selectedApplicationSection = ApplicationSections.HOME;
                    break;
            }
            navigateToAppSection(selectedApplicationSection);
        });

        this.userIconLabel.setText(FontAwesomeIcons.USER);

        breadcrumbsBinding = new ObservableListListenerBinding<>(breadcrumbs);
        breadcrumbsBinding.bindChangeListener(change -> breadcrumbManager.updateBreadcrumbBar(change, breadcrumbsHbox, user));

        // Load the landing screen by default
        FxmlLoadResult<HostStageAware> loadResult = controllerUtils.loadFxml(SceneSets.LANDING_SCREEN);
        loadResult.getController().initHostingStageName(PfdApplication.MAIN_STAGE_NAME);
        homeRootContainer.getChildren().clear();
        homeRootContainer.getChildren().add(loadResult.getRootNode());
    }

    @Override
    protected void registerDefaultListeners() {

        changeUserLink.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                LOG.debug("Returning to user profile screen...");
                ReturnToProfileSelectEvent navigationEvent = new ReturnToProfileSelectEvent();
                eventBus.post(navigationEvent);
            }
        });
    }

    @Override
    public void destroy()
    {
        leftNavBinding.unbind();
        breadcrumbsBinding.unbind();
        timer.cancel();
    }
}

