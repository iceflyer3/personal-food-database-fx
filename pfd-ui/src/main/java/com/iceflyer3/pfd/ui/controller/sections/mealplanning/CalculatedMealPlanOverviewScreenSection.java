package com.iceflyer3.pfd.ui.controller.sections.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023, 2025 Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.ui.controller.sections.ScreenSection;
import com.iceflyer3.pfd.ui.model.api.mealplanning.MealPlanModel;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.util.converter.BigDecimalStringConverter;

/**
 * Screen section implementation that shows overview information for a meal plan than was
 * created via the "calculated" registration mode
 */
public class CalculatedMealPlanOverviewScreenSection implements ScreenSection
{
    private final MealPlanModel mealPlan;
    private final BigDecimalStringConverter bigDecimalStringConverter;

    public CalculatedMealPlanOverviewScreenSection(MealPlanModel mealPlan)
    {
        this.mealPlan = mealPlan;
        bigDecimalStringConverter = new BigDecimalStringConverter();
    }

    @Override
    public Node create()
    {
        Label subHeader = new Label("Active plan details");
        subHeader.getStyleClass().addAll("bold", "underline", "sub-header-text");

        VBox container = new VBox(10);
        container.getChildren().add(0, subHeader);
        container.getChildren().add(1, createConfigurationDetailsGridPane());

        return new TitledPane("Calculated Meal Plan Configuration", container);
    }

    private GridPane createConfigurationDetailsGridPane()
    {
        GridPane gridPane = new GridPane();
        gridPane.setVgap(10);
        gridPane.setHgap(20);

        // First column
        Label heightLabel = new Label("Height:");
        Label heightValueLabel = new Label(bigDecimalStringConverter.toString(mealPlan.getHeight()));

        Label weightLabel = new Label("Weight:");
        Label weightValueLabel = new Label(bigDecimalStringConverter.toString(mealPlan.getWeight()));

        Label ageLabel = new Label("Age:");
        Label ageValueLabel = new Label(Integer.toString(mealPlan.getAge()));

        Label sexLabel = new Label("Sex:");
        Label sexValueLabel = new Label(mealPlan.getSex().getName());

        gridPane.add(heightLabel, 0, 0);
        gridPane.add(heightValueLabel, 1, 0);
        gridPane.add(weightLabel, 0, 1);
        gridPane.add(weightValueLabel, 1, 1);
        gridPane.add(ageLabel, 0, 2);
        gridPane.add(ageValueLabel, 1, 2);
        gridPane.add(sexLabel, 0, 3);
        gridPane.add(sexValueLabel, 1, 3);

        // Second column
        Label goalLabel = new Label("Goal:");
        Label goalValueLabel = new Label(mealPlan.getFitnessGoal().getName());

        Label bmrLabel = new Label("BMR Formula:");
        Label bmrValueLabel = new Label(mealPlan.getBmrFormula().getName());

        Label activityLevelLabel = new Label("Activity Level:");
        Label activityLevelValueLabel = new Label(mealPlan.getActivityLevel().getName());

        Label bodyFatLabel = new Label("Body Fat Percentage:");
        Label bodyFatValueLabel = new Label();
        String bodyFatDisplayValue = mealPlan.getBodyFatPercentage() == null ? "Not Provided" : bigDecimalStringConverter.toString(mealPlan.getBodyFatPercentage());
        bodyFatValueLabel.setText(bodyFatDisplayValue);

        gridPane.add(goalLabel, 2, 0);
        gridPane.add(goalValueLabel, 3, 0);
        gridPane.add(bmrLabel, 2, 1);
        gridPane.add(bmrValueLabel, 3, 1);
        gridPane.add(activityLevelLabel, 2, 2);
        gridPane.add(activityLevelValueLabel, 3, 2);
        gridPane.add(bodyFatLabel, 2, 3);
        gridPane.add(bodyFatValueLabel, 3, 3);

        return gridPane;
    }
}
