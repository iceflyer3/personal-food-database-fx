/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.controller.sections.mealplanning;

import com.iceflyer3.pfd.constants.CssConstants;
import com.iceflyer3.pfd.ui.controller.sections.ScreenSection;
import com.iceflyer3.pfd.ui.util.TextUtils;
import javafx.geometry.Insets;
import javafx.scene.Node;
import com.iceflyer3.pfd.ui.model.api.mealplanning.carbcycling.ReadOnlyCarbCyclingMealModel;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

public class CarbCyclingMealBasicInfoScreenSection<T extends ReadOnlyCarbCyclingMealModel> implements ScreenSection
{
    private final T entity;

    public CarbCyclingMealBasicInfoScreenSection(T entity)
    {
        this.entity = entity;
    }

    @Override
    public Node create()
    {
        Label mealNameLabel = new Label(entity.getName());
        mealNameLabel.getStyleClass().addAll(
                CssConstants.CLASS_BOLD,
                CssConstants.CLASS_UNDERLINE,
                CssConstants.CLASS_TEXT_SUB_HEADER
        );

        // Header labels and content labels for the grid view
        Label lastUpdatedHeader = new Label("Last Updated:");
        lastUpdatedHeader.getStyleClass().add(CssConstants.CLASS_BOLD);
        Label lastUpdatedContent = new Label(TextUtils.getDateTimeString(entity.getLastModifiedDate()));

        Label mealTypeHeader = new Label("Meal Type:");
        mealTypeHeader.getStyleClass().add(CssConstants.CLASS_BOLD);
        Label mealTypeContent = new Label(entity.getMealType().getName());

        Label mealNumberHeader = new Label("Meal Number:");
        mealNumberHeader.getStyleClass().add(CssConstants.CLASS_BOLD);
        Label mealNumberContent = new Label(Integer.toString(entity.getMealNumber()));

        Label requiredCaloriesHeader = new Label("Required Calories:");
        requiredCaloriesHeader.getStyleClass().add(CssConstants.CLASS_BOLD);
        Label requiredCaloriesContent = new Label(entity.getRequiredCalories().toString());

        Label requiredProteinHeader = new Label("Required Protein:");
        requiredProteinHeader.getStyleClass().add(CssConstants.CLASS_BOLD);
        Label requiredProteinContent = new Label(entity.getRequiredProtein().toString());

        Label requiredCarbsHeader = new Label("Required Carbs:");
        requiredCarbsHeader.getStyleClass().add(CssConstants.CLASS_BOLD);
        Label requiredCarbsContent = new Label(entity.getRequiredCarbs().toString());

        Label requiredFatsHeader = new Label("Required Fats:");
        requiredFatsHeader.getStyleClass().add(CssConstants.CLASS_BOLD);
        Label requiredFatsContent = new Label(entity.getRequiredFats().toString());

        // Setup the gridview
        GridPane basicDetailsGridPane = new GridPane();
        basicDetailsGridPane.setVgap(10);
        basicDetailsGridPane.setHgap(20);

        // Column one
        basicDetailsGridPane.add(lastUpdatedHeader, 0, 0);
        basicDetailsGridPane.add(lastUpdatedContent, 0, 1);

        basicDetailsGridPane.add(mealTypeHeader, 1, 0);
        basicDetailsGridPane.add(mealTypeContent, 1, 1);

        basicDetailsGridPane.add(mealNumberHeader, 2, 0);
        basicDetailsGridPane.add(mealNumberContent, 2, 1);

        // Column two
        basicDetailsGridPane.add(requiredCaloriesHeader, 0, 2);
        basicDetailsGridPane.add(requiredCaloriesContent, 0, 3);

        basicDetailsGridPane.add(requiredProteinHeader, 1, 2);
        basicDetailsGridPane.add(requiredProteinContent, 1, 3);

        basicDetailsGridPane.add(requiredCarbsHeader, 2, 2);
        basicDetailsGridPane.add(requiredCarbsContent, 2, 3);

        basicDetailsGridPane.add(requiredFatsHeader, 3, 2);
        basicDetailsGridPane.add(requiredFatsContent, 3, 3);

        // Setup vbox container for all content
        VBox paneContentContainer = new VBox(10);
        paneContentContainer.getChildren().add(mealNameLabel);
        paneContentContainer.getChildren().add(basicDetailsGridPane);

        // Setup the pane and set the container as its content
        TitledPane overviewPane = new TitledPane("Meal Overview", paneContentContainer);
        overviewPane.setMaxWidth(Double.MAX_VALUE);

        VBox paneWrapperContainer = new VBox();
        paneWrapperContainer.getChildren().add(overviewPane);
        VBox.setMargin(overviewPane, new Insets(20, 0, 20, 0));
        return paneWrapperContainer;
    }
}
