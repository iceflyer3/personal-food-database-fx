package com.iceflyer3.pfd.ui.widget.builder.screen;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023, 2025 Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.ui.widget.stepper.Steppable;
import com.iceflyer3.pfd.ui.widget.stepper.series.ScreenSeriesDecision;
import com.iceflyer3.pfd.ui.widget.stepper.series.ScreenSeriesScreen;

import java.util.HashMap;
import java.util.Map;

/**
 * Builder for creation of a single screen within a screen series.
 *
 * At minimum, the builder must be used to specify the class of the component to appear on the screen
 * and the decision that will decide what screen gets navigated to next.
 */
public class ScreenSeriesScreenBuilder implements ScreenSeriesScreenConfigurer
{
    private final Map<String, Object> componentData;
    private Class<?> componentControllerClass;
    private ScreenSeriesDecision navigationDecision;
    private boolean isInitial;
    private boolean isFinalCandidate;

    public ScreenSeriesScreenBuilder()
    {
        componentData = new HashMap<>();
    }

    @Override
    public ScreenSeriesScreenConfigurer usingComponent(Class<? extends Steppable> componentControllerClass)
    {
        this.componentControllerClass = componentControllerClass;
        return this;
    }

    @Override
    public ScreenSeriesScreenConfigurer withConcludingDecision(ScreenSeriesDecision decision)
    {
        navigationDecision = decision;
        return this;
    }

    @Override
    public ScreenSeriesScreenConfigurer isInitial()
    {
        isInitial = true;
        return this;
    }

    @Override
    public ScreenSeriesScreenConfigurer isFinal()
    {
        isFinalCandidate = true;
        return this;
    }

    @Override
    public ScreenSeriesScreenConfigurer withComponentInitializationDataItem(String key, Object value)
    {
        componentData.put(key, value);
        return this;
    }

    /**
     * Build the screen
     * @return The ScreenSeriesScreen instance representing the screen
     * @throws IllegalStateException If the screen does not have either a component class or a concluding decision
     */
    public ScreenSeriesScreen build() throws IllegalStateException
    {
        if (componentControllerClass == null)
        {
            throw new IllegalStateException("You must specify the controller class of the component that will be displayed on this screen");
        }

        if (!isFinalCandidate && navigationDecision == null)
        {
            throw new IllegalStateException("You must specify a ScreenSeriesDecision callback to determine how this screen should pick the next screen to navigate to");
        }

        ScreenSeriesScreen screen = new ScreenSeriesScreen(componentControllerClass, navigationDecision);
        screen.setInitial(isInitial);
        screen.setFinal(isFinalCandidate);
        screen.setComponentInitData(componentData);

        return screen;
    }
}
