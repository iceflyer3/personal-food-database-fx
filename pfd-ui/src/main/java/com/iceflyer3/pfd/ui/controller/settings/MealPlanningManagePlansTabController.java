package com.iceflyer3.pfd.ui.controller.settings;

/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.google.common.eventbus.EventBus;
import com.iceflyer3.pfd.constants.FontAwesomeIcons;
import com.iceflyer3.pfd.enums.ActivityLevel;
import com.iceflyer3.pfd.enums.BasalMetabolicRateFormula;
import com.iceflyer3.pfd.enums.FitnessGoal;
import com.iceflyer3.pfd.enums.Sex;
import com.iceflyer3.pfd.ui.cell.ActionButtonTableCell;
import com.iceflyer3.pfd.ui.controller.AbstractTaskController;
import com.iceflyer3.pfd.ui.controller.HostStageAware;
import com.iceflyer3.pfd.ui.enums.PfdActionType;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.MealPlanModelProxy;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.MealPlanningProxyFactory;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.util.ScreenLoadEventUtils;
import com.iceflyer3.pfd.ui.util.builder.ActionNodeBuilder;
import com.iceflyer3.pfd.ui.util.builder.IconButtonBuilder;
import com.iceflyer3.pfd.ui.util.builder.NotificationBuilder;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MealPlanningManagePlansTabController extends AbstractTaskController implements HostStageAware
{
    private final WindowManager windowManager;
    private final MealPlanningProxyFactory mealPlanningProxyFactory;
    private final ObservableList<MealPlanModelProxy> mealPlans;

    private UserViewModel user;
    private String hostingStageName;
    private boolean hasActiveMealPlan;

    @FXML
    public VBox rootContainer;

    @FXML
    public VBox mealPlanLoadingStatusContainer;

    @FXML
    public VBox mealPlanningTabContainer;

    public MealPlanningManagePlansTabController(
            EventBus eventBus,
            NotificationBuilder notificationBuilder,
            ScreenLoadEventUtils loadEventBuilder,
            TaskExecutor taskExecutor,
            WindowManager windowManager,
            MealPlanningProxyFactory mealPlanningProxyFactory)
    {
        super(eventBus, notificationBuilder, loadEventBuilder, taskExecutor);
        this.windowManager = windowManager;
        this.mealPlanningProxyFactory = mealPlanningProxyFactory;
        this.mealPlans = FXCollections.observableArrayList();
    }

    @Override
    public void initialize()
    {
        mealPlanningTabContainer.managedProperty().bind(mealPlanningTabContainer.visibleProperty());
        mealPlanLoadingStatusContainer.managedProperty().bind(mealPlanLoadingStatusContainer.visibleProperty());
    }

    @Override
    protected void registerDefaultListeners()
    {

    }

    @Override
    public void initControllerData(Map<String, Object> controllerData)
    {
        super.initControllerData(controllerData);
        user = (UserViewModel) controllerData.get(UserViewModel.class.getName());
        initMealPlanningMealPlanManagementTable();
    }

    @Override
    public void initHostingStageName(String stageName)
    {
        hostingStageName = stageName;
        refreshMealPlans();
    }

    private void initMealPlanningMealPlanManagementTable()
    {
        TableView<MealPlanModelProxy> mealPlansTableView = new TableView<>();
        mealPlansTableView.setPlaceholder(new Label("You have not yet created any meal plans"));

        TableColumn<MealPlanModelProxy, Integer> ageColumn = new TableColumn<>("Age");
        ageColumn.setCellValueFactory(new PropertyValueFactory<>("age"));

        TableColumn<MealPlanModelProxy, BigDecimal> heightColumn = new TableColumn<>("Height");
        heightColumn.setCellValueFactory(new PropertyValueFactory<>("height"));

        TableColumn<MealPlanModelProxy, BigDecimal> weightColumn = new TableColumn<>("Weight");
        weightColumn.setCellValueFactory(new PropertyValueFactory<>("weight"));

        TableColumn<MealPlanModelProxy, Sex> sexColumn = new TableColumn<>("Sex");
        sexColumn.setCellValueFactory(new PropertyValueFactory<>("sex"));

        TableColumn<MealPlanModelProxy, ActivityLevel> activityLevelColumn = new TableColumn<>("Activity Level");
        activityLevelColumn.setCellValueFactory(new PropertyValueFactory<>("activityLevel"));

        TableColumn<MealPlanModelProxy, FitnessGoal> fitnessGoalColumn = new TableColumn<>("Fitness Goal");
        fitnessGoalColumn.setCellValueFactory(new PropertyValueFactory<>("fitnessGoal"));

        TableColumn<MealPlanModelProxy, BasalMetabolicRateFormula> bmrFormulaColumn = new TableColumn<>("BMR Formula");
        bmrFormulaColumn.setCellValueFactory(new PropertyValueFactory<>("bmrFormula"));

        TableColumn<MealPlanModelProxy, Boolean> actionButtonColumn = new TableColumn<>("Actions");
        actionButtonColumn.setCellFactory(col -> new ActionButtonTableCell<>(this::getActionButtonProviderForActionTableColumn));
        actionButtonColumn.setCellValueFactory(new PropertyValueFactory<>("isActive"));

        mealPlansTableView.getColumns().addAll(
                ageColumn,
                heightColumn,
                weightColumn,
                sexColumn,
                activityLevelColumn,
                fitnessGoalColumn,
                bmrFormulaColumn,
                actionButtonColumn
        );
        mealPlansTableView.setItems(mealPlans);
        mealPlanningTabContainer.getChildren().add(mealPlansTableView);
    }

    private List<Button> getActionButtonProviderForActionTableColumn(MealPlanModelProxy mealPlanForRow)
    {
        List<Button> buttons = new ArrayList<>();

        Button deleteButton = ActionNodeBuilder
                .createIconButton(null, FontAwesomeIcons.TIMES, FontAwesomeIcons.DEFAULT_SIZE_TABLE_ROW_ICON)
                .forActionType(PfdActionType.DANGER)
                .withAction(event -> {
                    windowManager.openConfirmationDialog(mealPlanningTabContainer.getScene().getWindow(), () -> {
                        mealPlanForRow.delete((wasSuccessful, resultMessage) -> {
                            notificationBuilder.create(resultMessage, hostingStageName).show();

                            if (wasSuccessful)
                            {
                                refreshMealPlans();
                            }
                        });
                    });
                })
                .build();
        deleteButton.setDisable(mealPlanForRow.isActive());

        IconButtonBuilder toggleButtonBuilder = ActionNodeBuilder
                .createIconButton(null, FontAwesomeIcons.POWER, FontAwesomeIcons.DEFAULT_SIZE_TABLE_ROW_ICON)
                .forActionType(PfdActionType.PRIMARY);

        if (mealPlanForRow.isActive())
        {
            Button deactivateButton = toggleButtonBuilder.withAction(event -> {
                        mealPlanForRow.deactivate((wasSuccessful, proxyResult) -> {
                            if (wasSuccessful)
                            {
                                refreshMealPlans();
                                notificationBuilder.create("The meal plan has been deactivated", hostingStageName).show();
                            }
                            else
                            {
                                notificationBuilder.create("An error has occurred while trying to deactivate the meal plan", hostingStageName).show();
                            }
                        });
                    })
                    .build();

            deactivateButton.setTooltip(new Tooltip("Deactivate"));
            buttons.add(deactivateButton);
        }
        else
        {
            Button activateButton = toggleButtonBuilder.withAction(event -> {
                        mealPlanForRow.activate((wasSuccessful, proxyResult) -> {
                            if (wasSuccessful)
                            {
                                refreshMealPlans();
                                notificationBuilder.create("The meal plan has been activated", hostingStageName).show();
                            }
                            else
                            {
                                notificationBuilder.create("An error has occurred while trying to activate the meal plan", hostingStageName).show();
                            }
                        });
                    })
                    .build();

            // Only one meal plan may be active at a time. If another already is it must be deactivated first.
            activateButton.setDisable(hasActiveMealPlan);
            activateButton.setTooltip(new Tooltip("Activate"));
            buttons.add(activateButton);
        }

        buttons.add(deleteButton);
        return buttons;
    }

    private void refreshMealPlans()
    {
        toggleLoadingStatusDisplay(true);
        mealPlanningProxyFactory.getMealPlansForUser(user, (wasSuccessful, proxyResult) -> {
            if (wasSuccessful)
            {
                mealPlans.clear();
                mealPlans.addAll(proxyResult);
                hasActiveMealPlan = proxyResult.stream().anyMatch(MealPlanModelProxy::isActive);
            }
            else
            {
                notificationBuilder.create("Failed to load list of all meal plans", hostingStageName).show();
            }
            toggleLoadingStatusDisplay(false);
        });
    }

    private void toggleLoadingStatusDisplay(boolean isVisible)
    {
        mealPlanLoadingStatusContainer.setVisible(isVisible);
        mealPlanningTabContainer.setVisible(!isVisible);
    }
}
