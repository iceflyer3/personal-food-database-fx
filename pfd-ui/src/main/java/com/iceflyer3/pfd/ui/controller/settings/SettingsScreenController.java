package com.iceflyer3.pfd.ui.controller.settings;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.constants.ApplicationSections;
import com.iceflyer3.pfd.constants.SceneSets;
import com.iceflyer3.pfd.ui.event.PublishBreadcrumbEvent;
import com.iceflyer3.pfd.ui.controller.AbstractTaskController;
import com.iceflyer3.pfd.ui.controller.HostStageAware;
import com.iceflyer3.pfd.ui.listener.Destroyable;
import com.iceflyer3.pfd.ui.listener.ObservableValueListenerBinding;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.util.ControllerUtils;
import com.iceflyer3.pfd.ui.util.FxmlLoadResult;
import com.iceflyer3.pfd.ui.util.LayoutUtils;
import com.iceflyer3.pfd.ui.util.builder.NotificationBuilder;
import com.iceflyer3.pfd.ui.util.ScreenLoadEventUtils;

import com.google.common.eventbus.EventBus;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SettingsScreenController extends AbstractTaskController implements HostStageAware, Destroyable
{
    private static final String MEAL_PLAN_GENERAL_TAB = "mealPlanningGeneralTab";
    private static final String MEAL_PLAN_MANAGE_TAB = "mealPlanningManageMealPlansTab";
    private static final String BACKUP_IMPORT_TAB = "importDataTab";
    private static final String BACKUP_EXPORT_TAB = "exportDataTab";

    private final ControllerUtils controllerUtils;

    private final List<ObservableValueListenerBinding<?>> tabPaneListenerBindings;
    private String hostingStageName;
    private AbstractTaskController selectedMealPlanningTabController;
    private AbstractTaskController selectedDataImportExportTabController;

    @FXML
    public TabPane mealPlanningSettingsTabPane;

    @FXML
    public Tab mealPlanningGeneralTab;

    @FXML
    public Tab mealPlanningManageMealPlansTab;

    @FXML
    public TabPane importExportDataTabPane;
    @FXML
    public Tab importDataTab;

    @FXML
    public Tab exportDataTab;

    @FXML
    public Label screenLabel;

    public SettingsScreenController(EventBus eventBus,
                                    NotificationBuilder notificationBuilder,
                                    ScreenLoadEventUtils loadEventBuilder,
                                    TaskExecutor taskExecutor,
                                    ControllerUtils controllerUtils)
    {
        super(eventBus, notificationBuilder, loadEventBuilder, taskExecutor);
        this.controllerUtils = controllerUtils;
        this.tabPaneListenerBindings = new ArrayList<>();
    }

    @Override
    public void initialize() {
        VBox.setMargin(screenLabel, LayoutUtils.getHeaderLabelMarginInsets());

        mealPlanningGeneralTab.setUserData(MEAL_PLAN_GENERAL_TAB);
        mealPlanningManageMealPlansTab.setUserData(MEAL_PLAN_MANAGE_TAB);
        importDataTab.setUserData(BACKUP_IMPORT_TAB);
        exportDataTab.setUserData(BACKUP_EXPORT_TAB);
    }

    @Override
    protected void registerDefaultListeners() {

    }

    @Override
    public void initControllerData(Map<String, Object> controllerData)
    {
        // Load tab content when the tab is selected
        ObservableValueListenerBinding<Tab> mealPlanningTabPaneBinding = new ObservableValueListenerBinding<>(mealPlanningSettingsTabPane.getSelectionModel().selectedItemProperty());
        mealPlanningTabPaneBinding.bindChangeListener(
                (observable, oldTab, newTab) ->
                {
                    // Clear out the content for the old tab (if any)
                    if (oldTab != null)
                    {
                        if (selectedMealPlanningTabController instanceof Destroyable destroyableController)
                        {
                            destroyableController.destroy();
                        }

                        oldTab.setContent(null);
                        selectedMealPlanningTabController = null;
                    }

                    // Populate the new tab (if any)
                    if (newTab != null)
                    {
                        String selectedTabName = (String)newTab.getUserData();
                        switch (selectedTabName)
                        {
                            case MEAL_PLAN_GENERAL_TAB ->
                            {
                                String generalTabSet = String.format("%s/%s", ApplicationSections.SETTINGS, SceneSets.SETTINGS_MEAL_PLANNING_GENERAL_TAB);
                                FxmlLoadResult<MealPlanningGeneralTabController> generalTabLoadResult = controllerUtils.loadFxml(generalTabSet);
                                generalTabLoadResult.getController().initControllerData(Map.of(UserViewModel.class.getName(), controllerData.get(UserViewModel.class.getName())));
                                generalTabLoadResult.getController().initHostingStageName(hostingStageName);
                                selectedMealPlanningTabController = generalTabLoadResult.getController();
                                mealPlanningGeneralTab.setContent(generalTabLoadResult.getRootNode());
                            }
                            case MEAL_PLAN_MANAGE_TAB ->
                            {
                                String managePlansTabSet = String.format("%s/%s", ApplicationSections.SETTINGS, SceneSets.SETTINGS_MEAL_PLANNING_MANAGE_PLANS_TAB);
                                FxmlLoadResult<MealPlanningManagePlansTabController> managePlansTabLoadResult = controllerUtils.loadFxml(managePlansTabSet);
                                managePlansTabLoadResult.getController().initControllerData(Map.of(UserViewModel.class.getName(), controllerData.get(UserViewModel.class.getName())));
                                managePlansTabLoadResult.getController().initHostingStageName(hostingStageName);
                                selectedMealPlanningTabController = managePlansTabLoadResult.getController();
                                mealPlanningManageMealPlansTab.setContent(managePlansTabLoadResult.getRootNode());
                            }
                        }
                    }
                });
        tabPaneListenerBindings.add(mealPlanningTabPaneBinding);

        ObservableValueListenerBinding<Tab> importExportTabPaneBinding = new ObservableValueListenerBinding<>(importExportDataTabPane.getSelectionModel().selectedItemProperty());
        importExportTabPaneBinding.bindChangeListener(
                (observable, oldTab, newTab) ->
                {
                    // Clear out the content for the old tab (if any)
                    if (oldTab != null)
                    {
                        if (selectedDataImportExportTabController instanceof Destroyable destroyableController)
                        {
                            destroyableController.destroy();
                        }

                        oldTab.setContent(null);
                        selectedDataImportExportTabController = null;
                    }

                    // Populate the new tab (if any)
                    if (newTab != null)
                    {
                        String selectedTabName = (String)newTab.getUserData();
                        switch(selectedTabName)
                        {
                            case BACKUP_IMPORT_TAB ->
                            {
                                String setToLoad = String.format("%s/%s", ApplicationSections.SETTINGS, SceneSets.SETTINGS_IMPORT_DATA_TAB);
                                FxmlLoadResult<ImportDataTabController> importTabLoadResult = controllerUtils.loadFxml(setToLoad);
                                importTabLoadResult.getController().initHostingStageName(hostingStageName);
                                selectedDataImportExportTabController = importTabLoadResult.getController();
                                importDataTab.setContent(importTabLoadResult.getRootNode());
                            }
                            case BACKUP_EXPORT_TAB ->
                            {
                                String setToLoad = String.format("%s/%s", ApplicationSections.SETTINGS, SceneSets.SETTINGS_EXPORT_DATA_TAB);
                                FxmlLoadResult<ExportDataTabController> exportTabLoadResult = controllerUtils.loadFxml(setToLoad);
                                exportTabLoadResult.getController().initHostingStageName(hostingStageName);
                                selectedDataImportExportTabController = exportTabLoadResult.getController();
                                exportDataTab.setContent(exportTabLoadResult.getRootNode());
                            }
                        }
                    }
                }
        );
        tabPaneListenerBindings.add(importExportTabPaneBinding);

        /*
         * A bit of a hack to get the change listener to trigger and populate the default tabs
         * of each second level TabPane.
         *
         * Not sure why this listener doesn't fire on load when the pane is constructed and the
         * first tab is selected, but it doesn't especially matter because this works just fine
         * and is easy enough.
         */
        mealPlanningSettingsTabPane.getSelectionModel().clearSelection();
        mealPlanningSettingsTabPane.getSelectionModel().select(mealPlanningGeneralTab);

        importExportDataTabPane.getSelectionModel().clearSelection();
        importExportDataTabPane.getSelectionModel().select(exportDataTab);
    }

    @Override
    public void initHostingStageName(String stageName)
    {
        hostingStageName = stageName;
        PublishBreadcrumbEvent landingScreenBreadcrumb = new PublishBreadcrumbEvent("Application Settings", ApplicationSections.SETTINGS, SceneSets.SETTINGS_SCREEN, stageName, 1);
        eventBus.post(landingScreenBreadcrumb);
    }

    @Override
    public void destroy()
    {
        tabPaneListenerBindings.forEach(ObservableValueListenerBinding::unbind);
    }
}
