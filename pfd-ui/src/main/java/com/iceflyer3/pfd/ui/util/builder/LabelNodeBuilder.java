/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.util.builder;

import com.iceflyer3.pfd.constants.CssConstants;
import com.iceflyer3.pfd.ui.enums.PfdActionType;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Font;

public class LabelNodeBuilder
{
    private final Label label;

    protected LabelNodeBuilder(String labelText)
    {
        label = new Label(labelText);
        label.getStyleClass().add(CssConstants.CLASS_CLICKABLE);
    }

    public LabelNodeBuilder italicize()
    {
        label.getStyleClass().add(CssConstants.CLASS_ITALICIZE);
        return this;
    }

    public LabelNodeBuilder embolden()
    {
        label.getStyleClass().add(CssConstants.CLASS_BOLD);
        return this;
    }

    public LabelNodeBuilder underline()
    {
        label.getStyleClass().add(CssConstants.CLASS_UNDERLINE);
        return this;
    }

    public LabelNodeBuilder withAction(EventHandler<MouseEvent> actionHandler)
    {
        label.setOnMouseClicked(actionHandler);
        return this;
    }

    public LabelNodeBuilder withFontSize(int fontSize)
    {
        label.setFont(Font.font(fontSize));
        return this;
    }

    public LabelNodeBuilder forActionType(PfdActionType actionType)
    {
        label.getStyleClass().removeAll(
                CssConstants.CLASS_BUTTON_PRIMARY,
                CssConstants.CLASS_BUTTON_SECONDARY,
                CssConstants.CLASS_BUTTON_TERTIARY,
                CssConstants.CLASS_BUTTON_DANGER);

        switch(actionType)
        {
            case PRIMARY:
                label.getStyleClass().add(CssConstants.CLASS_TEXT_PRIMARY);
                break;
            case SECONDARY:
                label.getStyleClass().add(CssConstants.CLASS_TEXT_SECONDARY);
                break;
            case TERTIARY:
                label.getStyleClass().add(CssConstants.CLASS_TEXT_TERTIARY);
                break;
            case DANGER:
                label.getStyleClass().add(CssConstants.CLASS_TEXT_DANGER);
                break;
        }

        return this;
    }

    public Label build()
    {
        return label;
    }
}
