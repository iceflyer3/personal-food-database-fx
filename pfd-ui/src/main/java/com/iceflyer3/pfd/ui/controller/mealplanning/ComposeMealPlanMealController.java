package com.iceflyer3.pfd.ui.controller.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.google.common.eventbus.EventBus;
import com.iceflyer3.pfd.constants.ApplicationSections;
import com.iceflyer3.pfd.constants.FontAwesomeIcons;
import com.iceflyer3.pfd.constants.SceneSets;
import com.iceflyer3.pfd.ui.controller.sections.mealplanning.MealPlanMealBasicInfoScreenSection;
import com.iceflyer3.pfd.ui.event.PublishBreadcrumbEvent;
import com.iceflyer3.pfd.ui.controller.AbstractTaskController;
import com.iceflyer3.pfd.ui.controller.HostStageAware;
import com.iceflyer3.pfd.ui.controller.Savable;
import com.iceflyer3.pfd.ui.controller.sections.EditIngredientScreenSection;
import com.iceflyer3.pfd.ui.controller.sections.MacronutrientBreakdownScreenSection;
import com.iceflyer3.pfd.ui.controller.sections.MicronutrientBreakdownScreenSection;
import com.iceflyer3.pfd.ui.controller.sections.ScreenSection;
import com.iceflyer3.pfd.ui.listener.Destroyable;
import com.iceflyer3.pfd.ui.listener.ObservableListListenerBinding;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.proxy.food.FoodProxyFactory;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.MealPlanMealModelProxy;
import com.iceflyer3.pfd.ui.util.LayoutUtils;
import com.iceflyer3.pfd.ui.util.ScreenLoadEventUtils;
import com.iceflyer3.pfd.ui.util.builder.ActionNodeBuilder;
import com.iceflyer3.pfd.ui.util.builder.NotificationBuilder;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ComposeMealPlanMealController extends AbstractTaskController implements HostStageAware, Savable, Destroyable
{
    private final FoodProxyFactory foodProxyFactory;

    private String hostingStageName;
    private MealPlanMealModelProxy meal;
    private Button saveButton;
    private ObservableListListenerBinding<IngredientModel> mealIngredientsListChangeListener;

    @FXML
    public VBox rootContainer;

    @FXML
    public Label screenLabel;

    @FXML
    public AnchorPane buttonContainerAnchorPane;

    public ComposeMealPlanMealController(EventBus eventBus,
                                         NotificationBuilder notificationBuilder,
                                         ScreenLoadEventUtils loadEventBuilder,
                                         TaskExecutor taskExecutor,
                                         FoodProxyFactory foodProxyFactory) {
        super(eventBus, notificationBuilder, loadEventBuilder, taskExecutor);
        this.foodProxyFactory = foodProxyFactory;
    }

    @Override
    public void initControllerData(Map<String, Object> controllerData) {
        loadEventBuilder.emitBegin("Searching for meal planning meal details", hostingStageName);
        meal = (MealPlanMealModelProxy) controllerData.get(MealPlanMealModelProxy.class.getName());

        PublishBreadcrumbEvent breadcrumb = new PublishBreadcrumbEvent("Compose Meal Plan Meal", ApplicationSections.MEAL_PLANNING, SceneSets.MEAL_PLANNING_COMPOSE_MEAL, hostingStageName, 3);
        breadcrumb.addData(MealPlanMealModelProxy.class.getName(), meal);
        eventBus.post(breadcrumb);

        meal.loadIngredients((wasSuccessful, resultMessage) ->
        {
            if (wasSuccessful)
            {
                // Load the list of all foods needed by the ingredients screen section
                foodProxyFactory.getAllFoods((foundFoodsSuccessfully, allFoodsList) ->
                {
                    if (foundFoodsSuccessfully)
                    {
                        // Add the ingredients, macronutrient, and micronutrient sections now that the ingredient data has been loaded.
                        ScreenSection ingredientsSection = new EditIngredientScreenSection<>(allFoodsList, meal);
                        ScreenSection macroBreakdownSection = new MacronutrientBreakdownScreenSection<>(meal);
                        ScreenSection microBreakdownSection = new MicronutrientBreakdownScreenSection<>(meal);

                        // Register change listener on ingredient change to update save validity.
                        mealIngredientsListChangeListener = new ObservableListListenerBinding<>(meal.getIngredients());
                        mealIngredientsListChangeListener.bindChangeListener(change -> updateSaveValidity());

                        rootContainer.getChildren().addAll(
                                ingredientsSection.create(),
                                macroBreakdownSection.create(),
                                microBreakdownSection.create()
                        );
                    }

                    loadEventBuilder.emitCompleted(hostingStageName);
                });
            }
            else
            {
                loadEventBuilder.emitCompleted(hostingStageName);
                notificationBuilder.create(resultMessage, hostingStageName).show();
            }
        });
        ScreenSection overviewSection = new MealPlanMealBasicInfoScreenSection<>(meal);
        rootContainer.getChildren().add(overviewSection.create());
    }

    @Override
    public void initialize() {
        VBox.setMargin(screenLabel, LayoutUtils.getHeaderLabelMarginInsets());

        saveButton = ActionNodeBuilder
                .createIconButton("SAVE", FontAwesomeIcons.SAVE)
                .withAction(this::save)
                .build();
        saveButton.setDisable(true);
        AnchorPane.setRightAnchor(saveButton, 0.0);
        buttonContainerAnchorPane.getChildren().add(saveButton);
    }

    @Override
    public void initHostingStageName(String stageName) {
        hostingStageName = stageName;
    }

    @Override
    protected void registerDefaultListeners()
    {

    }

    @Override
    public void updateSaveValidity() {
        saveButton.setDisable(!meal.validate().wasSuccessful());
    }

    @Override
    public void save(ActionEvent event) {
        meal.saveIngredients((wasSuccessful, resultMessage) -> notificationBuilder.create(resultMessage, hostingStageName).show());
    }

    @Override
    public void destroy()
    {
        mealIngredientsListChangeListener.unbind();
    }
}
