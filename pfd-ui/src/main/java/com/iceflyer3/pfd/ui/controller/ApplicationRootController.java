/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.controller;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.iceflyer3.pfd.PfdApplication;
import com.iceflyer3.pfd.constants.SceneSets;
import com.iceflyer3.pfd.ui.event.ReturnToProfileSelectEvent;
import com.iceflyer3.pfd.ui.event.ShowNotificationEvent;
import com.iceflyer3.pfd.ui.event.UserProfileSelectionEvent;
import com.iceflyer3.pfd.ui.listener.Destroyable;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.util.ControllerUtils;
import com.iceflyer3.pfd.ui.util.FxmlLoadResult;
import com.iceflyer3.pfd.ui.util.NotificationUtils;
import com.iceflyer3.pfd.user.AuthenticatedUser;
import com.iceflyer3.pfd.user.UserAuthentication;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.controlsfx.control.NotificationPane;
import org.springframework.stereotype.Component;

@Component
public class ApplicationRootController extends AbstractController implements Destroyable
{

    private static final Logger LOG = LoggerFactory.getLogger(ApplicationRootController.class);

    private final ControllerUtils controllerUtils;

    private HomeScreenController loadedHomeScreen;

    private NotificationUtils notificationUtils;

    @FXML
    private NotificationPane notificationPane;

    @FXML
    private VBox rootContainer;

    public ApplicationRootController(EventBus eventBus, ControllerUtils controllerUtils)
    {
        this.controllerUtils = controllerUtils;
        eventBus.register(this);
    }

    @Override
    public void initialize() {
        notificationUtils = new NotificationUtils(notificationPane);
        notificationUtils.initPaneDefaultSettings();
        this.updateSet(controllerUtils.loadFxml(SceneSets.PROFILE_SELECT).getRootNode());
    }

    @Override
    protected void registerDefaultListeners() { }

    @Subscribe
    protected void handleUserProfileSelectionEvent(UserProfileSelectionEvent userProfileSelectionEvent)
    {
        LOG.debug("A user profile has been selected! Event: {}", userProfileSelectionEvent);

        UserViewModel selectedUser = userProfileSelectionEvent.getSelectedUser();
        UserAuthentication.instance().setAuthenticatedUser(new AuthenticatedUser(selectedUser.getUserId(), selectedUser.getUsername()));

        FxmlLoadResult<HomeScreenController> loadResult = controllerUtils.loadFxml(SceneSets.HOME);
        loadedHomeScreen = loadResult.getController();
        // TODO: With the advent of the UserAuthentication singleton we'll no longer need functions
        //       like these to pass the user around. Any part of the application interested can
        //       simply get access to it at any time.
        loadedHomeScreen.initUser(userProfileSelectionEvent.getSelectedUser());
        this.updateSet(loadResult.getRootNode());
    }

    @Subscribe
    protected void handleReturnToUserProfileSelection(ReturnToProfileSelectEvent returnToProfileSelectEvent)
    {
        LOG.debug("Returning to user profile selection screen. Event: {}", returnToProfileSelectEvent);

        UserAuthentication.instance().clearAuthenticatedUser();

        // See the comment in the changeApplicationScreen function of AbstractHomeController for a note explaining this.
        loadedHomeScreen.destroy();
        loadedHomeScreen = null;

        FxmlLoadResult<ProfileSelectController> loadResult = controllerUtils.loadFxml(SceneSets.PROFILE_SELECT);
        this.updateSet(loadResult.getRootNode());
    }

    @Subscribe
    protected void handleNotificationRequest(ShowNotificationEvent event)
    {
        /*
         * This screen hosts both the user profile selection screen and the main screen
         * and is always in the main application windows. So requests for notifications
         * should only be shown here if they are for the main application window.
         */
        LOG.debug("Received request to show notification: {}", event);
        if (event.getForStage().equals(PfdApplication.MAIN_STAGE_NAME))
        {
            notificationUtils.showNotification(event);
        }
    }

    private void updateSet(Parent newRootNode)
    {
        // Clear out the last sub-scene
        rootContainer.getChildren().clear();

        // Make sure the root for the new sub-scene fills the entire window
        VBox.setVgrow(newRootNode, Priority.ALWAYS);

        // Set the root for the new sub-scene in the root scenes VBox
        rootContainer.getChildren().add(newRootNode);
    }

    @Override
    public void destroy()
    {
        if (loadedHomeScreen != null)
        {
            loadedHomeScreen.destroy();
        }
    }
}
