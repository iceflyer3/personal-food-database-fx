package com.iceflyer3.pfd.ui.enums;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * An enum that lists all of the food properties that may be used when filtering foods.
 */
public enum FoodFilterProperty {
    NAME("Name"),
    BRAND("Brand"),
    SOURCE("Source"),
    CALORIES("Calories"),
    GLYCEMIC_INDEX("Glycemic Index"),
    PROTEIN("Protein"),
    TOTAL_FATS("Total Fats"),
    TRANS_FAT("Trans Fat"),
    MONO_FAT("Monounsaturated Fat"),
    POLY_FAT("Polyunsaturated Fat"),
    SAT_FAT("Saturated Fat"),
    CARBS("Carbohydrates"),
    SUGARS("Sugars"),
    FIBER("Fiber"),
    CHOLESTEROL("Cholesterol"),
    IRON("Iron"),
    MANGANESE("Manganese"),
    COPPER("Copper"),
    ZINC("Zinc"),
    CALCIUM("Calcium"),
    MAGNESIUM("Magnesium"),
    PHOSPHORUS("Phosphorus"),
    POTASSIUM("Potassium"),
    SODIUM("Sodium"),
    SULFUR("Sulfur"),
    VITAMIN_A("Vitamin A"),
    VITAMIN_C("Vitamin C"),
    VITAMIN_K("Vitamin K"),
    VITAMIN_D("Vitamin D"),
    VITAMIN_E("Vitamin E"),
    VITAMIN_B1("Vitamin B1"),
    VITAMIN_B2("Vitamin B2"),
    VITAMIN_B3("Vitamin B3"),
    VITAMIN_B5("Vitamin B5"),
    VITAMIN_B6("Vitamin B6"),
    VITAMIN_B7("Vitamin B7"),
    VITAMIN_B8("Vitamin B8"),
    VITAMIN_B9("Vitamin B9"),
    VITAMIN_B12("Vitamin B12");

    private final String name;

    FoodFilterProperty(String name)
    {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
