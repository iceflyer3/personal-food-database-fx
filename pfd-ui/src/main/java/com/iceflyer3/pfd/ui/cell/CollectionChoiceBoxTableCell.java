/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.cell;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableCell;

import java.util.Collection;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * TableCell implementation that allows rendering of an arbitrary collection into a ChoiceBox
 * control for display purposes. The ChoiceBox is then used as the value of the cell.
 *
 * The collection must be a property of the object to which the row in the table that this
 * cell belongs to is bound and is provided via the collectionProvider.
 *
 * You must also specify how to render the string labels that will appear in the ChoiceBox for
 * each item in the collection via the cellTextProvider.
 *
 * @param <TableType> The generic type of the TableView to which this cell will belong
 * @param <CollectionType> The generic type of the Collection that will be rendered as a choicebox
 */
public class CollectionChoiceBoxTableCell<TableType, CollectionType> extends TableCell<TableType, Collection<CollectionType>>
{
    private final ChoiceBox<String> choiceBox;
    private final Function<TableType, Collection<CollectionType>> collectionProvider;
    private final BiFunction<TableType, CollectionType, String> optionTextProvider;

    /**
     * Create a new CollectionChoiceBoxTableCell
     *
     * @param collectionProvider A function describing which collection property of the object bound
     *                           to the row this cell belongs to should be the source of the items
     *                           to be placed in the ChoiceBox.
     * @param optionTextProvider A function describing how the text for each item is the ChoiceBox
     *                         should be created for each item in the collection. This function
     *                         has access to both the row data and the item for the current iteration
     *                         of the collection
     */
    public CollectionChoiceBoxTableCell(Function<TableType, Collection<CollectionType>> collectionProvider, BiFunction<TableType, CollectionType, String> optionTextProvider)
    {
        this.collectionProvider = collectionProvider;
        this.optionTextProvider = optionTextProvider;
        this.choiceBox = new ChoiceBox<>();
        this.setAlignment(Pos.CENTER);
    }

    @Override
    protected void updateItem(Collection<CollectionType> item, boolean empty)
    {
        super.updateItem(item, empty);

        if (empty || item == null)
        {
            // According to the docs this MUST happen when overriding this method or weirdness will ensue
            // https://openjfx.io/javadoc/13/javafx.controls/javafx/scene/control/Cell.html#updateItem(T,boolean)
            reset();
        }
        else
        {
            if (this.getTableRow() != null)
            {
                TableType rowItem = this.getTableRow().getItem();
                if (rowItem != null)
                {
                    ObservableList<String> options = FXCollections.observableArrayList();

                    Collection<CollectionType> collection = collectionProvider.apply(rowItem);
                    collection.forEach(collectionItem -> {
                        options.add(optionTextProvider.apply(rowItem, collectionItem));
                    });

                    choiceBox.setItems(options);
                    choiceBox.getSelectionModel().selectFirst();

                    this.setText(null);
                    this.setGraphic(choiceBox);
                }
                else
                {
                    reset();
                }
            }
            else
            {
                reset();
            }

        }
    }

    private void reset()
    {
        this.setText(null);
        this.setGraphic(null);
    }
}
