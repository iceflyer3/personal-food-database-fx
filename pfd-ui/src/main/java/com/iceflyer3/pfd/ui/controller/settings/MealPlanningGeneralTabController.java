package com.iceflyer3.pfd.ui.controller.settings;

/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.google.common.eventbus.EventBus;
import com.iceflyer3.pfd.constants.FontAwesomeIcons;
import com.iceflyer3.pfd.ui.controller.AbstractTaskController;
import com.iceflyer3.pfd.ui.controller.HostStageAware;
import com.iceflyer3.pfd.ui.controller.steppable.MealPlanningRegisterCalculatedUserSteppableController;
import com.iceflyer3.pfd.ui.controller.steppable.MealPlanningRegisterManualUserSteppableController;
import com.iceflyer3.pfd.ui.controller.steppable.MealPlanningRegistrationTypeSteppableController;
import com.iceflyer3.pfd.ui.enums.MealPlanningRegistrationType;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import com.iceflyer3.pfd.ui.model.api.mealplanning.registration.MealPlanningRegistration;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.MealPlanningProxyFactory;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.util.FxmlLoadResult;
import com.iceflyer3.pfd.ui.util.ScreenLoadEventUtils;
import com.iceflyer3.pfd.ui.util.builder.ActionNodeBuilder;
import com.iceflyer3.pfd.ui.util.builder.NotificationBuilder;
import com.iceflyer3.pfd.ui.widget.builder.WidgetNodeBuilder;
import com.iceflyer3.pfd.ui.widget.stepper.StepOrchestrator;
import com.iceflyer3.pfd.ui.widget.stepper.series.ScreenSeriesDecision;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.function.Consumer;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MealPlanningGeneralTabController extends AbstractTaskController implements HostStageAware
{
    private final WindowManager windowManager;
    private final MealPlanningProxyFactory mealPlanningProxyFactory;

    private String hostingStageName;

    private UserViewModel user;

    @FXML
    public VBox rootContainer;

    private final WidgetNodeBuilder widgetNodeBuilder;

    public MealPlanningGeneralTabController(
            EventBus eventBus,
            NotificationBuilder notificationBuilder,
            ScreenLoadEventUtils loadEventBuilder,
            TaskExecutor taskExecutor,
            WindowManager windowManager,
            MealPlanningProxyFactory mealPlanningProxyFactory,
            WidgetNodeBuilder widgetNodeBuilder)
    {
        super(eventBus, notificationBuilder, loadEventBuilder, taskExecutor);
        this.windowManager = windowManager;
        this.mealPlanningProxyFactory = mealPlanningProxyFactory;
        this.widgetNodeBuilder = widgetNodeBuilder;
    }

    @Override
    public void initialize()
    {

    }

    @Override
    protected void registerDefaultListeners()
    {
        Button updateMealPlanningInfoButton = ActionNodeBuilder
                .createIconButton("Register",  FontAwesomeIcons.PENCIL)
                .withAction(event -> openUpdateMealPlanningPersonalDetailsDialog())
                .build();
        rootContainer.getChildren().add(updateMealPlanningInfoButton);
    }

    @Override
    public void initControllerData(Map<String, Object> controllerData)
    {
        user = (UserViewModel) controllerData.get(UserViewModel.class.getName());
    }

    @Override
    public void initHostingStageName(String stageName)
    {
        hostingStageName = stageName;
    }

    private void openUpdateMealPlanningPersonalDetailsDialog()
    {
        ScreenSeriesDecision registrationScreenDecision = (availableScreens, screenResult) -> {
            switch((MealPlanningRegistrationType) screenResult)
            {
                case MANUAL:
                {
                    return availableScreens.stream()
                            .filter(screen -> screen.getScreenComponentClass().equals(MealPlanningRegisterManualUserSteppableController.class))
                            .findFirst()
                            .orElseThrow();
                }
                case CALCULATED:
                    return availableScreens.stream()
                            .filter(screen -> screen.getScreenComponentClass().equals(MealPlanningRegisterCalculatedUserSteppableController.class))
                            .findFirst()
                            .orElseThrow();
                default:
                    throw new IllegalArgumentException("The value returned from the step did not correspond to a new screen");
            }
        };

        Consumer<MealPlanningRegistration> resultCallback = (registrationResult) -> {
            if (registrationResult != null)
            {
                saveUpdatedMealPlanPersonalInformation(registrationResult);
            }

            windowManager.close(WindowManager.DIALOG_WINDOW);
        };

        FxmlLoadResult<StepOrchestrator> registrationStepper = widgetNodeBuilder.createScreenSeries(MealPlanningRegistration.class)
                .addScreen(config -> config
                        .usingComponent(MealPlanningRegistrationTypeSteppableController.class)
                        .withConcludingDecision(registrationScreenDecision)
                        .isInitial()
                )
                .addScreen(config -> config
                        .usingComponent(MealPlanningRegisterManualUserSteppableController.class)
                        .isFinal()
                )
                .addScreen(config -> config
                        .usingComponent(MealPlanningRegisterCalculatedUserSteppableController.class)
                        .isFinal()
                )
                .usingResultCallback(resultCallback)
                .build();

        windowManager.openStepperDialog(rootContainer.getScene().getWindow(), registrationStepper, "Meal Planning Registration");
    }

    private void saveUpdatedMealPlanPersonalInformation(MealPlanningRegistration request)
    {
        mealPlanningProxyFactory.getMealPlan(null, user, (wasRetrievalSuccessful, newMealPlanProxy) -> {
            if (wasRetrievalSuccessful)
            {
                newMealPlanProxy.save(user, request, (wasSaveSuccessful, saveResult) -> {
                    if (wasSaveSuccessful)
                    {
                        //refreshMealPlans(); Shouldn't need to do this anymore. Will be handled by that tabs controller
                        notificationBuilder.create("Personal information updated successfully. A new meal plan has been created", hostingStageName).show();
                    }
                    else
                    {
                        notificationBuilder.create("Failed to update the meal plan personal information. The new meal plan could not be saved", hostingStageName).show();
                    }
                });
            }
            else
            {
                notificationBuilder.create("Failed to update the meal plan personal information. A new meal plan could not be created", hostingStageName).show();
            }
        });
    }
}
