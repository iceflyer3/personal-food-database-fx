package com.iceflyer3.pfd.ui.cell;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.ui.model.viewmodel.ServingSizeViewModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableCell;

import java.util.List;

public class ServingSizeChoiceBoxTableCell<T> extends TableCell<T, List<ServingSizeViewModel>> {

    private final ChoiceBox<String> choiceBox;

    public ServingSizeChoiceBoxTableCell() {
        choiceBox = new ChoiceBox<>();
        this.setAlignment(Pos.CENTER);
    }

    @Override
    public void updateItem(List<ServingSizeViewModel> item, boolean empty)
    {
        super.updateItem(item, empty);

        if (empty || item == null)
        {
            // According to the docs this MUST happen when overriding this method or weirdness will ensue
            // https://openjfx.io/javadoc/13/javafx.controls/javafx/scene/control/Cell.html#updateItem(T,boolean)
            this.setText(null);
            this.setGraphic(null);
        }
        else
        {
            if (choiceBox != null)
            {
                choiceBox.setItems(getChoiceBoxOptions());
                choiceBox.getSelectionModel().selectFirst();
            }

            this.setText(null);
            this.setGraphic(choiceBox);
        }
    }

    private ObservableList<String> getChoiceBoxOptions()
    {
        ObservableList<String> options = FXCollections.observableArrayList();

        List<ServingSizeViewModel> servingsSizes = this.getItem();
        for (ServingSizeViewModel servingSize : servingsSizes)
        {
            options.add(String.format("%s %s", servingSize.getServingSize(), servingSize.getUnitOfMeasure().getAbbreviation()));
        }

        return options;
    }
}
