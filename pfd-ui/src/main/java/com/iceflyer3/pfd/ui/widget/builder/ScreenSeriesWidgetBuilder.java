package com.iceflyer3.pfd.ui.widget.builder;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023, 2025 Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.PfdApplication;
import com.iceflyer3.pfd.exception.InvalidApplicationStateException;
import com.iceflyer3.pfd.ui.controller.Action;
import com.iceflyer3.pfd.ui.util.ControllerUtils;
import com.iceflyer3.pfd.ui.util.FxmlLoadResult;
import com.iceflyer3.pfd.ui.widget.builder.screen.ScreenSeriesScreenBuilder;
import com.iceflyer3.pfd.ui.widget.builder.screen.ScreenSeriesScreenConfigurer;
import com.iceflyer3.pfd.ui.widget.stepper.StepOrchestrator;
import com.iceflyer3.pfd.ui.widget.stepper.Steppable;
import com.iceflyer3.pfd.ui.widget.stepper.series.ScreenSeriesScreen;
import com.iceflyer3.pfd.ui.widget.stepper.series.ScreenSeriesWidgetController;
import javafx.fxml.FXMLLoader;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

/**
 * Builder that offers a fluent API for creating a new screen series widget
 * @param <WidgetResultType> The return type of the screen series widget
 */
public class ScreenSeriesWidgetBuilder<WidgetResultType>
{
    private final FXMLLoader fxmlLoader;
    private final Set<ScreenSeriesScreen> screens;

    private Action stepChangeCallback;
    private Consumer<WidgetResultType> resultCallback;

    protected ScreenSeriesWidgetBuilder(ControllerUtils controllerUtils)
    {
        // Builder state initialization
        screens = new HashSet<>();

        // FXML loader initialization
        fxmlLoader = new FXMLLoader(PfdApplication.class.getResource("/fxml/widget/screen-series-widget.fxml"));
        fxmlLoader.setControllerFactory((controllerClassParam) -> new ScreenSeriesWidgetController<>(controllerUtils, screens, resultCallback));
    }

    public <T extends Steppable> ScreenSeriesWidgetBuilder<WidgetResultType> addScreen(Consumer<ScreenSeriesScreenConfigurer> screenConfigurer)
    {
        ScreenSeriesScreenBuilder screenBuilder = new ScreenSeriesScreenBuilder();
        screenConfigurer.accept(screenBuilder);
        ScreenSeriesScreen newScreen = screenBuilder.build();
        screens.add(newScreen);

        return this;
    }

    public ScreenSeriesWidgetBuilder<WidgetResultType> usingResultCallback(Consumer<WidgetResultType> callback)
    {
        this.resultCallback = callback;
        return this;
    }

    public ScreenSeriesWidgetBuilder<WidgetResultType> usingStepChangeCallback(Action callback)
    {
        this.stepChangeCallback = callback;
        return this;
    }

    /**
     * Build the screen series widget
     * @return An FxmlLoadResult that contains a reference to the root node and the controller for the widget
     * @throws InvalidApplicationStateException If an error occurs while attempting to load the FXML for the widget
     * @throws IllegalStateException If the series has no screens, the series has more than one initial screen, or the series has no final screens
     */
    public FxmlLoadResult<StepOrchestrator> build() throws InvalidApplicationStateException, IllegalStateException
    {
        // Make sure there are both screens and a callback defined to be able to receive the final result
        if (resultCallback == null)
        {
            throw new IllegalStateException("You must specify the callback used to receive a result from the stepper");
        }

        if (screens.size() == 0)
        {
            throw new IllegalStateException("You must have at least one screen in the series");
        }

        // Make sure there is one initial screen and at least one final screen
        long initialScreens = screens.stream().filter(ScreenSeriesScreen::isInitial).count();
        long finalScreens = screens.stream().filter(ScreenSeriesScreen::isFinal).count();

        if (initialScreens != 1)
        {
            throw new IllegalStateException("You must have exactly one initial screen. This is the first screen displayed at the start of the series");
        }

        if (finalScreens == 0)
        {
            throw new IllegalStateException("You must have at least one final screen. This is the last screen displayed at the end of the series.");
        }

        // Make sure that all non-final screens may navigate to a subsequent screen
        long nonFinalScreensWithoutDecision = screens.stream().filter(screen -> !screen.isFinal() && screen.getNavigationDecision() == null).count();

        if (nonFinalScreensWithoutDecision > 0)
        {
            throw new IllegalStateException("All screens except for final screens must have an associated ScreenSeriesDecision.");
        }

        try
        {
            FxmlLoadResult<StepOrchestrator> result = new FxmlLoadResult<>(fxmlLoader.load(), fxmlLoader.getController());
            result.getController().onStepChange(stepChangeCallback);
            return result;
        }
        catch(IOException ioe)
        {
            throw new InvalidApplicationStateException("The application has failed to load the FXML for the Screen Series Widget.", ioe);
        }

    }
}
