/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.util.builder;

import com.iceflyer3.pfd.constants.CssConstants;
import com.iceflyer3.pfd.constants.FontAwesomeIcons;
import com.iceflyer3.pfd.ui.enums.FontAwesomeIconType;
import com.iceflyer3.pfd.ui.enums.PfdActionType;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.OverrunStyle;
import javafx.scene.text.Font;

public class IconButtonBuilder {

    private final Button button;
    private final Label icon;
    private final int iconSize;

    protected IconButtonBuilder(String buttonText, String iconName)
    {
        this(buttonText, ActionNodeBuilder.DEFAULT_TEXT_SIZE, iconName, ActionNodeBuilder.DEFAULT_ICON_SIZE);
    }

    protected IconButtonBuilder(String buttonText, String iconName, int iconSize)
    {
        this(buttonText, ActionNodeBuilder.DEFAULT_TEXT_SIZE, iconName, iconSize);
    }

    protected IconButtonBuilder(String buttonText, int textSize, String iconName, int iconSize)
    {
        this.iconSize = iconSize;
        this.icon = new Label(iconName);
        this.icon.setTextOverrun(OverrunStyle.CLIP);
        this.icon.setFont(Font.font(FontAwesomeIcons.ICON_FAMILY_SOLID, iconSize));

        this.button = new Button(buttonText);
        this.button.setFont(Font.font(textSize));
        button.getStyleClass().addAll(CssConstants.CLASS_BUTTON_BASE, CssConstants.CLASS_BUTTON_PRIMARY, CssConstants.CLASS_CLICKABLE);
    }

    public IconButtonBuilder withIcon(String iconName)
    {
        this.icon.setText(iconName);
        return this;
    }

    public IconButtonBuilder withIconType(FontAwesomeIconType iconType)
    {
        switch(iconType)
        {
            case REGULAR:
                this.icon.setFont(Font.font(FontAwesomeIcons.ICON_FAMILY_REGULAR, iconSize));
                break;
            case SOLID:
                this.icon.setFont(Font.font(FontAwesomeIcons.ICON_FAMILY_SOLID, iconSize));
                break;
            case BRAND:
                this.icon.setFont(Font.font(FontAwesomeIcons.ICON_FAMILY_BRANDS, iconSize));
                break;
        }

        return this;
    }

    public IconButtonBuilder withIconLocation(ContentDisplay iconLocation)
    {
        this.button.setContentDisplay(iconLocation);
        return this;
    }

    public IconButtonBuilder withAction(EventHandler<ActionEvent> actionHandler)
    {
        this.button.setOnAction(actionHandler);
        return this;
    }

    public IconButtonBuilder forActionType(PfdActionType actionType)
    {
        this.button.getStyleClass().removeAll(
                CssConstants.CLASS_BUTTON_PRIMARY,
                CssConstants.CLASS_BUTTON_SECONDARY,
                CssConstants.CLASS_BUTTON_TERTIARY,
                CssConstants.CLASS_BUTTON_DANGER);

        switch(actionType)
        {
            case PRIMARY:
                this.button.getStyleClass().add(CssConstants.CLASS_BUTTON_PRIMARY);
                break;
            case SECONDARY:
                this.button.getStyleClass().add(CssConstants.CLASS_BUTTON_SECONDARY);
                break;
            case TERTIARY:
                this.button.getStyleClass().add(CssConstants.CLASS_BUTTON_TERTIARY);
                break;
            case DANGER:
                this.button.getStyleClass().add(CssConstants.CLASS_BUTTON_DANGER);
                break;
        }

        return this;
    }

    public Button build()
    {
        this.button.setGraphic(this.icon);
        return this.button;
    }
}
