/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.util.builder;

/**
 * Builder for nodes that the user may interact with to perform an action
 */
public final class ActionNodeBuilder
{

    public static final int DEFAULT_TEXT_SIZE = 12;
    public static final int DEFAULT_ICON_SIZE = 24;


    private ActionNodeBuilder() {};

    /**
     * Builder for a button that includes an icon.
     *
     * @param buttonText The text of the button
     * @param iconName The name of the icon
     * @return an IconButtonBuilder used to build a button
     */
    public static IconButtonBuilder createIconButton(String buttonText, String iconName)
    {
        return new IconButtonBuilder(buttonText, iconName);
    }

    /**
     * Builder for a button that includes an icon.
     *
     * @param buttonText The text of the button
     * @param iconName The name of the icon
     * @param iconSize The size of the icon
     * @return an IconButtonBuilder used to build a button
     */
    public static IconButtonBuilder createIconButton(String buttonText, String iconName, int iconSize)
    {
        return new IconButtonBuilder(buttonText, iconName, iconSize);
    }

    /**
     * Builder for a button that includes an icon.
     *
     * @param buttonText The text of the button
     * @param textSize The size of the button text
     * @param iconName The name of the icon
     * @param iconSize The size of the icon
     * @return an IconButtonBuilder used to build a button
     */
    public static IconButtonBuilder createIconButton(String buttonText, int textSize, String iconName, int iconSize)
    {
        return new IconButtonBuilder(buttonText, textSize, iconName, iconSize);
    }

    public static LabelNodeBuilder createLabel(String labelText)
    {
        return new LabelNodeBuilder(labelText);
    }
}
