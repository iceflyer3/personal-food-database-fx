package com.iceflyer3.pfd.ui.controller.dialog;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023, 2025 Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.ui.manager.WindowManager;
import com.iceflyer3.pfd.ui.widget.stepper.StepOrchestrator;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.layout.VBox;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Controller class that allows for stepper widgets to be displayed in a dialog box.
 */
@Component
@Scope("prototype")
public class StepperAdapterDialogController extends AbstractDialogController<Object>
{
    public static final String STEPPER_NODE_KEY = "stepperRootNodeDataKey";
    public static final String STEPPER_CONTROLLER_KEY = "stepperControllerDataKey";

    @FXML
    public VBox stepperAdapterContainer;

    public StepperAdapterDialogController(WindowManager windowManager)
    {
        super(windowManager);
    }

    @Override
    public void initialize() { }

    @Override
    protected void registerDefaultListeners() { }

    @Override
    public void initControllerData(Map<String, Object> controllerData)
    {
        // Initialize the wrapped stepper component
        StepOrchestrator stepper = (StepOrchestrator) controllerData.get(STEPPER_CONTROLLER_KEY);
        stepper.onStepChange(() -> {
            if (stepperAdapterContainer.getScene() != null)
            {
                stepperAdapterContainer.getScene().getWindow().sizeToScene();
                stepperAdapterContainer.getScene().getWindow().centerOnScreen();
            }
        });
        stepper.initialize();

        // Display the wrapped stepper component
        Parent stepperComponentRootNode = (Parent) controllerData.get(STEPPER_NODE_KEY);
        stepperAdapterContainer.getChildren().add(stepperComponentRootNode);
    }
}
