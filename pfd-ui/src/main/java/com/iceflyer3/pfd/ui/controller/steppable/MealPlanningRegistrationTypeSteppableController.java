package com.iceflyer3.pfd.ui.controller.steppable;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023, 2025 Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.ui.controller.AbstractController;
import com.iceflyer3.pfd.ui.enums.MealPlanningRegistrationType;
import com.iceflyer3.pfd.ui.widget.stepper.StepOrchestrator;
import com.iceflyer3.pfd.ui.widget.stepper.Steppable;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

// This can probably extend abstract controller instead of abstract dialog controller.
@Component
@Scope("prototype")
public class MealPlanningRegistrationTypeSteppableController
        extends AbstractController
        implements Steppable
{
    private StepOrchestrator stepOrchestrator;

    @FXML
    public Button calculatedRegistrationButton;

    @FXML
    public Button manualRegistrationButton;

    public MealPlanningRegistrationTypeSteppableController() { }

    @Override
    public void initialize() { }

    @Override
    protected void registerDefaultListeners()
    {
        calculatedRegistrationButton.setOnAction(event -> stepOrchestrator.completeStep(MealPlanningRegistrationType.CALCULATED));
        manualRegistrationButton.setOnAction(event -> stepOrchestrator.completeStep(MealPlanningRegistrationType.MANUAL));
    }

    @Override
    public void usingStepOrchestrator(StepOrchestrator stepOrchestrator)
    {
        this.stepOrchestrator = stepOrchestrator;
    }
}
