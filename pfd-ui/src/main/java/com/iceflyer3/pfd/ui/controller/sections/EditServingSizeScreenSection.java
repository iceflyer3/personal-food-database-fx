package com.iceflyer3.pfd.ui.controller.sections;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.constants.FontAwesomeIcons;
import com.iceflyer3.pfd.enums.UnitOfMeasure;
import com.iceflyer3.pfd.ui.cell.ActionButtonProvider;
import com.iceflyer3.pfd.ui.cell.ActionButtonTableCell;
import com.iceflyer3.pfd.ui.cell.EditableBigDecimalTableCell;
import com.iceflyer3.pfd.ui.enums.PfdActionType;
import com.iceflyer3.pfd.ui.format.UnitOfMeasureStringConverter;
import com.iceflyer3.pfd.ui.model.api.ServingSizeModel;
import com.iceflyer3.pfd.ui.model.api.owner.ServingSizeWriter;
import com.iceflyer3.pfd.ui.util.LayoutUtils;
import com.iceflyer3.pfd.ui.util.builder.ActionNodeBuilder;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.ChoiceBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This screen section is a Titled Pane that features a tableview for displaying serving sizes
 * and a means to add, edit, and remove serving sizes.
 * @param <T> The type of the owner of the serving sizes
 */
public class EditServingSizeScreenSection<T extends ServingSizeWriter> implements ScreenSection
{
    T entity;

    /**
     * Creates a new screen serving size pane.
     *
     * @param entity The entity this screen section will display the information of
     */
    public EditServingSizeScreenSection(T entity)
    {
        this.entity = entity;
    }

    @Override
    public Node create()
    {
        TitledPane servingSizesPane = new TitledPane();
        servingSizesPane.setText("Serving Sizes");
        VBox.setMargin(servingSizesPane, LayoutUtils.getSectionElementVerticalSpacing());

        VBox container = new VBox(10);
        AnchorPane addServingsButtonAnchorPane = setupAddServingsButton();
        TableView<ServingSizeModel> servingSizesTableView = setupServingSizesTableView();
        container.getChildren().addAll(addServingsButtonAnchorPane, servingSizesTableView);

        servingSizesPane.setContent(container);
        return servingSizesPane;
    }

    private AnchorPane setupAddServingsButton()
    {
        AnchorPane anchorPane = new AnchorPane();

        Button addServingButton = ActionNodeBuilder
                .createIconButton("ADD SERVING", FontAwesomeIcons.PLUS)
                .forActionType(PfdActionType.SECONDARY)
                .withAction(event -> entity.addServingSize())
                .build();
        AnchorPane.setTopAnchor(addServingButton, 10.0);
        AnchorPane.setRightAnchor(addServingButton, 0.0);
        anchorPane.getChildren().add(addServingButton);
        return anchorPane;
    }

    private TableView<ServingSizeModel> setupServingSizesTableView()
    {
        TableView<ServingSizeModel> servingSizeTable = new TableView<>();
        servingSizeTable.setEditable(true);
        servingSizeTable.setPlaceholder(new Label("You must have at least one serving size before you may save!"));

        // Serving Size Column
        TableColumn<ServingSizeModel, BigDecimal> servingSizeColumn = new TableColumn<>("Serving Size");
        servingSizeColumn.setCellFactory(col -> new EditableBigDecimalTableCell<>());
        servingSizeColumn.setCellValueFactory(new PropertyValueFactory<>("servingSize"));

        // Unit of Measure Column
        // Centimeters and Inches are not valid units of measurement for foods
        List<UnitOfMeasure> unitOfMeasureList = Arrays.stream(UnitOfMeasure.values()).filter(uom -> uom != UnitOfMeasure.CENTIMETERS && uom != UnitOfMeasure.INCHES).collect(Collectors.toList());
        ObservableList<UnitOfMeasure> uomChoices = FXCollections.observableArrayList(unitOfMeasureList);
        TableColumn<ServingSizeModel, UnitOfMeasure> uomColumn = new TableColumn<>("Unit of Measure");
        uomColumn.setCellFactory(col -> new ChoiceBoxTableCell<>(new UnitOfMeasureStringConverter(), uomChoices));
        uomColumn.setCellValueFactory(new PropertyValueFactory<>("unitOfMeasure"));

        // Action column
        EventHandler<ActionEvent> removeServingHandler = (ActionEvent event) -> {
            ServingSizeModel servingSizeToRemove = (ServingSizeModel) ((Node)event.getSource()).getUserData();
            entity.removeServingSize(servingSizeToRemove);
        };

        ActionButtonProvider<ServingSizeModel> tableButtonProvider = (ServingSizeModel rowData) -> {
            Button deleteButton = ActionNodeBuilder
                    .createIconButton(null,  FontAwesomeIcons.TIMES, 16)
                    .forActionType(PfdActionType.DANGER)
                    .withAction(removeServingHandler)
                    .build();

            ArrayList<Button> buttons = new ArrayList<>();
            buttons.add(deleteButton);
            return buttons;
        };

        /*
         * The column has to have a cell value factory bound to a property of the table type
         * in order to render but it doesn't really matter which property we pick here since
         * we're only showing a button anyways.
         *
         * This is hacky but I don't know a better way to go about just adding a button
         * to the row that can take an action on the item for an individual row.
         *
         * So hacky it is.
         */
        TableColumn<ServingSizeModel, BigDecimal> actionButtonColumn  = new TableColumn<>();
        actionButtonColumn.setCellFactory(col -> new ActionButtonTableCell<>(tableButtonProvider));
        actionButtonColumn.setCellValueFactory(new PropertyValueFactory<>("servingSize"));
        actionButtonColumn.setEditable(false);

        servingSizeTable.getColumns().add(servingSizeColumn);
        servingSizeTable.getColumns().add(uomColumn);
        servingSizeTable.getColumns().add(actionButtonColumn);
        servingSizeTable.setItems(entity.getServingSizes());

        return servingSizeTable;
    }
}
