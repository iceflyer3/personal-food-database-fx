package com.iceflyer3.pfd.ui.format;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import javafx.scene.control.TextFormatter;
import javafx.util.converter.NumberStringConverter;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.util.function.UnaryOperator;

public class RequiredNumberTextFormatter implements UnaryOperator<TextFormatter.Change> {

    private static final Logger LOG = LoggerFactory.getLogger(RequiredNumberTextFormatter.class);

    private final NumberStringConverter numberStringConverter;
    private final boolean allowDecimals;
    private final String regex;

    public RequiredNumberTextFormatter(boolean allowDecimals)
    {
        this.regex = allowDecimals ? "^\\d+\\.?\\d*$" : "^\\d+$";
        this.allowDecimals = allowDecimals;
        this.numberStringConverter = new NumberStringConverter();
    }

    @Override
    public TextFormatter.Change apply(TextFormatter.Change change) {

        if (change.isAdded() || change.isReplaced())
        {
            String fullText = change.getControlNewText().equals(".") ? "" : change.getControlNewText();
            boolean isTextValid = fullText.matches(regex);
            LOG.debug("Validating ControlNewText of {} with regex {} - Result is: {}", change.getControlNewText(), regex, isTextValid);

            if (isTextValid)
            {
                Number newValue = numberStringConverter.fromString(fullText);

                // Required values must be non-zero
                boolean isNumberValid = allowDecimals ? newValue.doubleValue() != 0.0 : newValue.intValue() != 0;

                LOG.debug("Validating number of {} from valid control - Result is {}", newValue, isNumberValid);
                return isNumberValid ? change : null;
            }
            else
            {
                return null;
            }
        }

        // If text was deleted then we have no need to apply the filter
        return change;
    }
}