package com.iceflyer3.pfd.ui.widget.stepper;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023, 2025 Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.ui.controller.Action;

/**
 * To be implemented by widgets that are capable of managing stepping through a series of steps comprised of
 * Steppable components.
 *
 * This interface defines the public API that Steppable components may interact with.
 */
public interface StepOrchestrator
{
    /**
     * Initializes the step orchestrator's state and loads the initial step
     */
    void initialize();

    /**
     * Hook to allow for taking an arbitrary action upon the change from one step to the next
     * @param action The action to take after the step has changed
     */
    void onStepChange(Action action);

    /**
     * Indicates that the current step has completed and the orchestrator may advance to the next step (if any).
     */
    void completeStep();

    /**
     * Indicates that the current step has completed and the orchestrator may advance to the next step (if any).
     *
     * Allows returning of a result from processing that occurred during the step for use by either the
     * orchestrator itself or the next subsequent step.
     * @param stepResult The result of the step as reported by the associated Steppable component
     */
    void completeStep(Object stepResult);

    /**
     * Indicates that the current step will not be completed. What exactly the cancellation of a given step
     * means will be up to the step orchestrator implementation.
     */
    void cancelStep();
}
