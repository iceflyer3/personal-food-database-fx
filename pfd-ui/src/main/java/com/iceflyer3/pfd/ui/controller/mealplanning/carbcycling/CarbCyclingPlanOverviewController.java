package com.iceflyer3.pfd.ui.controller.mealplanning.carbcycling;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.google.common.eventbus.EventBus;
import com.iceflyer3.pfd.constants.ApplicationSections;
import com.iceflyer3.pfd.constants.CssConstants;
import com.iceflyer3.pfd.constants.FontAwesomeIcons;
import com.iceflyer3.pfd.constants.SceneSets;
import com.iceflyer3.pfd.enums.CarbCyclingDayType;
import com.iceflyer3.pfd.enums.Nutrient;
import com.iceflyer3.pfd.ui.controller.AbstractTaskController;
import com.iceflyer3.pfd.ui.controller.HostStageAware;
import com.iceflyer3.pfd.ui.controller.dialog.CreateCarbCyclingDayDialogController;
import com.iceflyer3.pfd.ui.event.PublishBreadcrumbEvent;
import com.iceflyer3.pfd.ui.event.ScreenNavigationEvent;
import com.iceflyer3.pfd.ui.format.FitnessGoalStringConverter;
import com.iceflyer3.pfd.ui.manager.OpenDialogRequest;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.MealPlanModelProxy;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.MealPlanningProxyFactory;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.carbcycling.CarbCyclingDayModelProxy;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.carbcycling.CarbCyclingPlanModelProxy;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.util.LayoutUtils;
import com.iceflyer3.pfd.ui.util.ScreenLoadEventUtils;
import com.iceflyer3.pfd.ui.util.builder.ActionNodeBuilder;
import com.iceflyer3.pfd.ui.util.builder.NotificationBuilder;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.util.converter.BigDecimalStringConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CarbCyclingPlanOverviewController extends AbstractTaskController implements HostStageAware
{
    private static final Logger LOG = LoggerFactory.getLogger(CarbCyclingPlanOverviewController.class);

    public static final String DATA_CARB_CYCLING_PLAN = "carbCyclingPlan";
    public static final String DATA_MEAL_PLAN = "mealPlan";

    private final WindowManager windowManager;
    private final MealPlanningProxyFactory mealPlanningProxyFactory;
    private final ObservableList<CarbCyclingDayModelProxy> daysForPlan;


    private UserViewModel user;
    private String hostingStageName;
    private CarbCyclingPlanModelProxy carbCyclingPlanProxy;
    private MealPlanModelProxy activeMealPlanProxy;

    @FXML
    public VBox rootContainer;

    @FXML
    public VBox overviewContainer;

    @FXML
    public Label screenLabel;

    @FXML
    public Label tdeeCaloriesLabel;

    @FXML
    public Label goalLabel;

    @FXML
    public Label highLowDayRatioLabel;

    @FXML
    public Label mealsPerDayLabel;

    @FXML
    public Label shouldTimeFatsLabel;

    @FXML
    public VBox carbCyclingDaySectionContainer;

    public CarbCyclingPlanOverviewController(
            EventBus eventBus,
            NotificationBuilder notificationBuilder,
            ScreenLoadEventUtils loadEventBuilder,
            TaskExecutor taskExecutor,
            WindowManager windowManager,
            MealPlanningProxyFactory mealPlanningProxyFactory)
    {
        super(eventBus, notificationBuilder, loadEventBuilder, taskExecutor);
        this.windowManager = windowManager;
        this.mealPlanningProxyFactory = mealPlanningProxyFactory;
        this.daysForPlan = FXCollections.observableArrayList();
    }

    @Override
    public void initialize()
    {
        VBox.setMargin(screenLabel, LayoutUtils.getHeaderLabelMarginInsets());
    }

    @Override
    public void initControllerData(Map<String, Object> controllerData)
    {
        user = (UserViewModel) controllerData.get(UserViewModel.class.getName());
        activeMealPlanProxy = (MealPlanModelProxy) controllerData.get(DATA_MEAL_PLAN);
        carbCyclingPlanProxy = (CarbCyclingPlanModelProxy) controllerData.get(DATA_CARB_CYCLING_PLAN);

        PublishBreadcrumbEvent breadcrumbEvent = new PublishBreadcrumbEvent("Carb Cycling Plan Overview", ApplicationSections.MEAL_PLANNING, SceneSets.CARB_CYCLING_PLAN_OVERVIEW, hostingStageName, 2);
        breadcrumbEvent.addData(DATA_CARB_CYCLING_PLAN, carbCyclingPlanProxy);
        breadcrumbEvent.addData(DATA_MEAL_PLAN, activeMealPlanProxy);
        eventBus.post(breadcrumbEvent);

        loadEventBuilder.emitBegin("Searching for carb cycling plan details", hostingStageName);
        carbCyclingPlanProxy.loadMealConfigurations((wasSuccessful, proxyResult) -> {
           if (wasSuccessful)
           {
               initOverviewBindings();
               initCarbCyclingPlanDaysSection();
               refreshCarbCyclingDaysForPlan();
           }
           else
           {
               notificationBuilder.create("An error has occurred while loading the meal configurations for this carb cycling plan", hostingStageName).show();
           }
        });
        loadEventBuilder.emitCompleted(hostingStageName);
    }

    @Override
    protected void registerDefaultListeners()
    {

    }

    @Override
    public void initHostingStageName(String stageName)
    {
        hostingStageName = stageName;
    }

    private void initOverviewBindings()
    {
        FitnessGoalStringConverter fitnessGoalStringConverter = new FitnessGoalStringConverter();
        BigDecimalStringConverter bigDecimalStringConverter = new BigDecimalStringConverter();

        tdeeCaloriesLabel.setText(bigDecimalStringConverter.toString(activeMealPlanProxy.getTdeeCalories()));
        goalLabel.setText(fitnessGoalStringConverter.toString(activeMealPlanProxy.getFitnessGoal()));
        highLowDayRatioLabel.setText(String.format("%s high carb - %s low carb", carbCyclingPlanProxy.getHighCarbDays(), carbCyclingPlanProxy.getLowCarbDays()));
        mealsPerDayLabel.setText(String.valueOf(carbCyclingPlanProxy.getMealsPerDay()));

        String shouldTimeFatsText = carbCyclingPlanProxy.isShouldTimeFats() ? "Yes" : "No";
        shouldTimeFatsLabel.setText(shouldTimeFatsText);
    }

    private void initCarbCyclingPlanDaysSection()
    {
        // Add Day button
        Button newCarbCyclingPlanButton = ActionNodeBuilder
                .createIconButton("ADD DAY", FontAwesomeIcons.PLUS)
                .withAction(this::openCreateNewDayDialog)
                .build();
        carbCyclingDaySectionContainer.getChildren().add(0, newCarbCyclingPlanButton);
        carbCyclingDaySectionContainer.setPadding(new Insets(10, 10, 10, 10));

        // Tableview setup
        TableView<CarbCyclingDayModelProxy> carbCyclingDayTableView = new TableView<>();
        carbCyclingDayTableView.setPlaceholder(new Label("You have not yet created any days for this carb cycling plan"));

        TableColumn<CarbCyclingDayModelProxy, String> dayNameColumn = new TableColumn<>("Name");
        dayNameColumn.setCellValueFactory(new PropertyValueFactory<>("dayLabel"));

        TableColumn<CarbCyclingDayModelProxy, CarbCyclingDayType> dayTypeColumn = new TableColumn<>("Type");
        dayTypeColumn.setCellValueFactory(new PropertyValueFactory<>("dayType"));

        TableColumn<CarbCyclingDayModelProxy, String> caloriesColumn = new TableColumn<>("Calories");
        caloriesColumn.setCellValueFactory(cellDataFeatures -> getMacronutrientSummaryDisplayProperty(Nutrient.CALORIES, cellDataFeatures.getValue()));
        caloriesColumn.getStyleClass().add(CssConstants.CLASS_TEXT_TABLE_CENTER_RIGHT);

        TableColumn<CarbCyclingDayModelProxy, String> proteinColumn = new TableColumn<>("Protein");
        proteinColumn.setCellValueFactory(cellDataFeatures -> getMacronutrientSummaryDisplayProperty(Nutrient.PROTEIN, cellDataFeatures.getValue()));
        proteinColumn.getStyleClass().add(CssConstants.CLASS_TEXT_TABLE_CENTER_RIGHT);

        TableColumn<CarbCyclingDayModelProxy, String> carbsColumn = new TableColumn<>("Carbs");
        carbsColumn.setCellValueFactory(cellDataFeatures -> getMacronutrientSummaryDisplayProperty(Nutrient.CARBOHYDRATES, cellDataFeatures.getValue()));
        carbsColumn.getStyleClass().add(CssConstants.CLASS_TEXT_TABLE_CENTER_RIGHT);

        TableColumn<CarbCyclingDayModelProxy, String> fatsColumn = new TableColumn<>("Fats");
        fatsColumn.setCellValueFactory(cellDataFeatures -> getMacronutrientSummaryDisplayProperty(Nutrient.TOTAL_FATS, cellDataFeatures.getValue()));
        fatsColumn.getStyleClass().add(CssConstants.CLASS_TEXT_TABLE_CENTER_RIGHT);

        TableColumn<CarbCyclingDayModelProxy, Boolean> isTrainingDayColumn = new TableColumn<>("Is Training Day?");
        isTrainingDayColumn.setCellValueFactory(new PropertyValueFactory<>("isTrainingDay"));
        isTrainingDayColumn.setCellFactory(CheckBoxTableCell.forTableColumn(isTrainingDayColumn));

        carbCyclingDayTableView.getColumns().addAll(
                dayNameColumn,
                dayTypeColumn,
                caloriesColumn,
                proteinColumn,
                carbsColumn,
                fatsColumn,
                isTrainingDayColumn);

        carbCyclingDayTableView.setItems(daysForPlan);
        carbCyclingDayTableView.setContextMenu(createContextMenuForCarbCyclingDayTable(carbCyclingDayTableView));
        carbCyclingDaySectionContainer.getChildren().add(carbCyclingDayTableView);
    }

    private ContextMenu createContextMenuForCarbCyclingDayTable(TableView<CarbCyclingDayModelProxy> planDaysTable)
    {
        EventHandler<ActionEvent> editActionHandler = event -> {
            CarbCyclingDayModelProxy selectedDay = planDaysTable.getSelectionModel().getSelectedItem();
            ScreenNavigationEvent navigationEvent = new ScreenNavigationEvent(ApplicationSections.MEAL_PLANNING, SceneSets.CARB_CYCLING_COMPOSE_DAY, hostingStageName, user);
            navigationEvent.getControllerData().put(ComposeCarbCyclingDayController.DATA_FOR_DAY, selectedDay);
            navigationEvent.getControllerData().put(ComposeCarbCyclingDayController.DATA_MEAL_PLAN, activeMealPlanProxy);
            navigationEvent.getControllerData().put(ComposeCarbCyclingDayController.DATA_CARB_CYCLING_PLAN, carbCyclingPlanProxy);

            eventBus.post(navigationEvent);
        };

        EventHandler<ActionEvent> deleteActionHandler = event -> {
            windowManager.openConfirmationDialog(rootContainer.getScene().getWindow(), () -> {
                CarbCyclingDayModelProxy selectedDay = planDaysTable.getSelectionModel().getSelectedItem();
                selectedDay.delete((wasSuccessful, resultMessage) -> {
                    notificationBuilder.create(resultMessage, hostingStageName).show();

                    if (wasSuccessful)
                    {
                        refreshCarbCyclingDaysForPlan();
                    }
                });
            });
        };

        return LayoutUtils.getDefaultContextMenu(editActionHandler, editActionHandler, deleteActionHandler);
    }

    private void openCreateNewDayDialog(ActionEvent event)
    {
        OpenDialogRequest<CarbCyclingDayModelProxy> newMealPlanDayDialogRequest = new OpenDialogRequest<>(overviewContainer.getScene().getWindow(), SceneSets.DIALOG_CREATE_CARB_CYCLING_PLAN_DAY, this::saveNewCarbCyclingDay);
        newMealPlanDayDialogRequest.setDialogTitle("Create Carb Cycling Day");
        newMealPlanDayDialogRequest.getDialogData().put(CreateCarbCyclingDayDialogController.DATA_MEAL_PLAN, activeMealPlanProxy);
        newMealPlanDayDialogRequest.getDialogData().put(CreateCarbCyclingDayDialogController.DATA_CARB_CYCLING_PLAN, carbCyclingPlanProxy);

        windowManager.openDialog(newMealPlanDayDialogRequest);
    }

    private void saveNewCarbCyclingDay(CarbCyclingDayModelProxy newDay)
    {
        newDay.save((wasSuccessful, resultMessage) -> {
            notificationBuilder.create(resultMessage, hostingStageName).show();

            if (wasSuccessful)
            {
                refreshCarbCyclingDaysForPlan();
            }
        });
    }

    private void refreshCarbCyclingDaysForPlan()
    {
        loadEventBuilder.emitBegin("Searching for carb cycling plan details", hostingStageName);
        mealPlanningProxyFactory.getCarbCyclingDaysForMealPlan(carbCyclingPlanProxy, (wasSuccessful, proxyResult) -> {
            if (wasSuccessful)
            {
                daysForPlan.clear();

                if (proxyResult.size() > 0)
                {
                    daysForPlan.addAll(proxyResult);
                }
            }
            else
            {
                notificationBuilder.create("An error has occurred while looking for the days for this carb cycling plan", hostingStageName).show();
            }
            loadEventBuilder.emitCompleted(hostingStageName);
        });
    }

    private SimpleStringProperty getMacronutrientSummaryDisplayProperty(Nutrient macronutrient, CarbCyclingDayModelProxy day)
    {
        LOG.debug("Generating summary display string for day {}", day);
        String displayString = "";

        switch(macronutrient)
        {
            case CALORIES:
                displayString = String.format("%s/%s", day.getCalories(), day.getRequiredCalories());
                break;
            case PROTEIN:
                displayString = String.format("%s/%s", day.getProtein(), day.getRequiredProtein());
                break;
            case CARBOHYDRATES:
                displayString = String.format("%s/%s", day.getCarbohydrates(), day.getRequiredCarbs());
                break;
            case TOTAL_FATS:
                displayString = String.format("%s/%s", day.getTotalFats(), day.getRequiredFats());
                break;
        }

        return new SimpleStringProperty(displayString);
    }
}
