package com.iceflyer3.pfd.ui.controller.foods;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.google.common.eventbus.EventBus;
import com.iceflyer3.pfd.constants.ApplicationSections;
import com.iceflyer3.pfd.constants.FontAwesomeIcons;
import com.iceflyer3.pfd.constants.SceneSets;
import com.iceflyer3.pfd.ui.controller.sections.EditServingSizeScreenSection;
import com.iceflyer3.pfd.ui.event.PublishBreadcrumbEvent;
import com.iceflyer3.pfd.ui.controller.AbstractTaskController;
import com.iceflyer3.pfd.ui.controller.HostStageAware;
import com.iceflyer3.pfd.ui.controller.Savable;
import com.iceflyer3.pfd.ui.listener.Destroyable;
import com.iceflyer3.pfd.ui.listener.ObservableListListenerBinding;
import com.iceflyer3.pfd.ui.listener.ObservableValueListenerBinding;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import com.iceflyer3.pfd.ui.model.api.ServingSizeModel;
import com.iceflyer3.pfd.ui.model.proxy.food.FoodProxyFactory;
import com.iceflyer3.pfd.ui.model.proxy.food.ReadOnlyFoodModelProxy;
import com.iceflyer3.pfd.ui.model.proxy.food.SimpleFoodModelProxy;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.util.BindingUtils;
import com.iceflyer3.pfd.ui.util.LayoutUtils;
import com.iceflyer3.pfd.ui.util.TextUtils;
import com.iceflyer3.pfd.ui.util.builder.ActionNodeBuilder;
import com.iceflyer3.pfd.ui.util.builder.NotificationBuilder;
import com.iceflyer3.pfd.ui.util.ScreenLoadEventUtils;
import com.iceflyer3.pfd.ui.listener.ModelPropertyValidationEventConfig;
import javafx.beans.binding.Bindings;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.util.converter.BigDecimalStringConverter;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ComposeSimpleFoodController extends AbstractTaskController implements HostStageAware, Savable, Destroyable
{
    private static final Logger LOG = LoggerFactory.getLogger(ComposeSimpleFoodController.class);

    private final FoodProxyFactory foodProxyFactory;
    private final WindowManager windowManager;
    private final Set<ObservableValueListenerBinding<?>> listenerBindings;

    private String hostingStageName;
    private SimpleFoodModelProxy simpleFood;
    private Button saveButton;

    private ObservableListListenerBinding<ServingSizeModel> servingSizesListBinding;

    @FXML
    public VBox rootContainer;

    @FXML
    public AnchorPane buttonContainerAnchorPane;

    @FXML
    public GridPane macronutrientGridPane;

    @FXML
    public GridPane micronutrientGridPane;

    @FXML
    public Label screenLabel;

    @FXML
    public TextField nameInput;

    @FXML
    public TextField brandInput;

    @FXML
    public TextField caloriesInput;

    @FXML
    public TextField giInput;

    @FXML
    public TextField foodSourceInput;

    @FXML
    public TextField proteinInput;

    @FXML
    public TextField carbsInput;

    @FXML
    public TextField sugarsInput;

    @FXML
    public TextField fiberInput;

    @FXML
    public TextField totalFatsInput;

    @FXML
    public TextField transFatInput;

    @FXML
    public TextField monoFatInput;

    @FXML
    public TextField polyFatInput;

    @FXML
    public TextField satFatInput;

    @FXML
    public TextField cholesterolInput;

    @FXML
    public TextField ironInput;

    @FXML
    public TextField manganeseInput;

    @FXML
    public TextField copperInput;

    @FXML
    public TextField zincInput;

    @FXML
    public TextField calciumInput;

    @FXML
    public TextField magnesiumInput;

    @FXML
    public TextField phosphorusInput;

    @FXML
    public TextField potassiumInput;

    @FXML
    public TextField sodiumInput;

    @FXML
    public TextField sulfurInput;

    @FXML
    public TextField vitaminAInput;

    @FXML
    public TextField vitaminCInput;

    @FXML
    public TextField vitaminDInput;

    @FXML
    public TextField vitaminEInput;

    @FXML
    public TextField vitaminKInput;

    @FXML
    public TextField vitaminB1Input;

    @FXML
    public TextField vitaminB2Input;

    @FXML
    public TextField vitaminB3Input;

    @FXML
    public TextField vitaminB5Input;

    @FXML
    public TextField vitaminB6Input;

    @FXML
    public TextField vitaminB7Input;

    @FXML
    public TextField vitaminB8Input;

    @FXML
    public TextField vitaminB9Input;

    @FXML
    public TextField vitaminB12Input;

    public ComposeSimpleFoodController(
            EventBus eventBus,
            WindowManager windowManager,
            FoodProxyFactory foodProxyFactory,
            NotificationBuilder notificationBuilder,
            ScreenLoadEventUtils loadEventBuilder,
            TaskExecutor taskExecutor)
    {
        super(eventBus, notificationBuilder, loadEventBuilder, taskExecutor);
        this.foodProxyFactory = foodProxyFactory;
        this.windowManager = windowManager;
        listenerBindings = new HashSet<>();
    }

    @Override
    public void initialize() {
        LOG.debug("ComposeSimpleFoodController is initializing...");

        VBox.setMargin(screenLabel, LayoutUtils.getHeaderLabelMarginInsets());

        // Setup Save Button
        saveButton = ActionNodeBuilder
                .createIconButton("SAVE", FontAwesomeIcons.SAVE)
                .withAction(this::save)
                .build();
        saveButton.setDisable(true);
        AnchorPane.setRightAnchor(saveButton, 0.0);
        buttonContainerAnchorPane.getChildren().add(saveButton);
    }

    @Override
    public void initControllerData(Map<String, Object> controllerData) {
        UserViewModel user = (UserViewModel) controllerData.get(UserViewModel.class.getName());

        // Loading an existing food?
        if (controllerData.containsKey(ReadOnlyFoodModelProxy.class.getName()))
        {
            ReadOnlyFoodModelProxy foodToLoad = (ReadOnlyFoodModelProxy) controllerData.get(ReadOnlyFoodModelProxy.class.getName());
            loadEventBuilder.emitBegin("Searching for simple food details", hostingStageName);
            foodProxyFactory.getSimpleFood(foodToLoad.getId(), user, (wasSuccessful, proxyResult) -> {
                if (wasSuccessful)
                {
                    processDataLoad(proxyResult);
                }
                else
                {
                    notificationBuilder.create("An error has occurred while attempting to load the details for the food", hostingStageName).show();
                }
                loadEventBuilder.emitCompleted(hostingStageName);
            });
        }
        // Creating a brand-new food?
        else
        {
            foodProxyFactory.getSimpleFood(null, user, (wasSuccessful, proxyResult) -> {
                if (wasSuccessful)
                {
                    processDataLoad(proxyResult);
                }
                else
                {
                    // Theoretically this should never actually happen
                    windowManager.openFatalErrorDialog();
                }
            });
        }
    }

    @Override
    public void initHostingStageName(String stageName) {
        hostingStageName = stageName;
        PublishBreadcrumbEvent landingScreenBreadcrumb = new PublishBreadcrumbEvent("Compose Simple Food", ApplicationSections.FOOD_DATABASE, SceneSets.COMPOSE_SIMPLE_FOOD, stageName, 2);
        eventBus.post(landingScreenBreadcrumb);
    }

    @Override
    public void updateSaveValidity() {
        saveButton.setDisable(!simpleFood.validate().wasSuccessful());
    }


    @Override
    public void save(ActionEvent event)
    {
        simpleFood.isUnique((wasSuccessful, isUnique) -> {
            if (wasSuccessful)
            {
                if (isUnique)
                {
                    notificationBuilder.create("Saving simple food...", hostingStageName).show();
                    simpleFood.save((wasSaveSuccessful, resultMessage) -> notificationBuilder.create(resultMessage, hostingStageName).show());
                }
                else
                {
                    notificationBuilder.create("A simple food with the same name and brand already exists.", hostingStageName).show();
                }
            }
            else
            {
                notificationBuilder.create("Failed to save the simple food. An error has occurred while verifying the uniqueness of the food.", hostingStageName).show();
            }
        });

    }

    @Override
    public void destroy()
    {
        servingSizesListBinding.unbind();
        listenerBindings.forEach(binding -> binding.unbind());
        listenerBindings.clear();
    }

    @Override
    protected void registerDefaultListeners() {

    }

    private void processDataLoad(SimpleFoodModelProxy loadedFood)
    {
        simpleFood = loadedFood;

        initBindingsAndValidation();

        // Setup Serving Sizes Pane
        Node servingSizeSection = new EditServingSizeScreenSection<>(simpleFood).create();
        rootContainer.getChildren().add(3, servingSizeSection);
    }

    private void initBindingsAndValidation()
    {
        // Text field setup
        caloriesInput.setTextFormatter(TextUtils.getRequiredDecimalTextInputFormatter());
        giInput.setTextFormatter(TextUtils.getOptionalDecimalTextInputFormatter());

        addDecimalTextFieldFormattersForGridPane(macronutrientGridPane.getChildren());
        addDecimalTextFieldFormattersForGridPane(micronutrientGridPane.getChildren());

        // Validation change listeners
        listenerBindings.add(BindingUtils.bindModelObservableValueToGuiControl(new ModelPropertyValidationEventConfig<>(simpleFood, simpleFood.nameProperty(), nameInput, this::updateSaveValidity)));
        listenerBindings.add(BindingUtils.bindModelObservableValueToGuiControl(new ModelPropertyValidationEventConfig<>(simpleFood, simpleFood.brandProperty(), brandInput, this::updateSaveValidity)));
        listenerBindings.add(BindingUtils.bindModelObservableValueToGuiControl(new ModelPropertyValidationEventConfig<>(simpleFood, simpleFood.sourceProperty(), foodSourceInput, this::updateSaveValidity)));
        listenerBindings.add(BindingUtils.bindModelObservableValueToGuiControl(new ModelPropertyValidationEventConfig<>(simpleFood, simpleFood.caloriesProperty(), caloriesInput, this::updateSaveValidity)));

        servingSizesListBinding = new ObservableListListenerBinding<>(simpleFood.getServingSizes());
        servingSizesListBinding.bindChangeListener(change -> this.updateSaveValidity());

        // Basic Info
        Bindings.bindBidirectional(nameInput.textProperty(), simpleFood.nameProperty());
        Bindings.bindBidirectional(brandInput.textProperty(), simpleFood.brandProperty());
        Bindings.bindBidirectional(caloriesInput.textProperty(), simpleFood.caloriesProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(foodSourceInput.textProperty(), simpleFood.sourceProperty());

        // Macros
        Bindings.bindBidirectional(proteinInput.textProperty(), simpleFood.proteinProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(carbsInput.textProperty(), simpleFood.carbohydratesProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(sugarsInput.textProperty(), simpleFood.sugarsProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(fiberInput.textProperty(), simpleFood.fiberProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(totalFatsInput.textProperty(), simpleFood.totalFatsProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(transFatInput.textProperty(), simpleFood.transFatProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(monoFatInput.textProperty(), simpleFood.monounsaturatedFatProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(polyFatInput.textProperty(), simpleFood.polyunsaturatedFatProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(satFatInput.textProperty(), simpleFood.saturatedFatProperty(), new BigDecimalStringConverter());

        // Micros
        Bindings.bindBidirectional(cholesterolInput.textProperty(), simpleFood.cholesterolProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(ironInput.textProperty(), simpleFood.ironProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(manganeseInput.textProperty(), simpleFood.manganeseProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(copperInput.textProperty(), simpleFood.copperProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(zincInput.textProperty(), simpleFood.zincProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(calciumInput.textProperty(), simpleFood.calciumProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(magnesiumInput.textProperty(), simpleFood.magnesiumProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(phosphorusInput.textProperty(), simpleFood.phosphorusProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(potassiumInput.textProperty(), simpleFood.potassiumProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(sodiumInput.textProperty(), simpleFood.sodiumProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(sulfurInput.textProperty(), simpleFood.sulfurProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(vitaminAInput.textProperty(), simpleFood.vitaminAProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(vitaminCInput.textProperty(), simpleFood.vitaminCProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(vitaminDInput.textProperty(), simpleFood.vitaminDProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(vitaminEInput.textProperty(), simpleFood.vitaminEProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(vitaminKInput.textProperty(), simpleFood.vitaminKProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(vitaminB1Input.textProperty(), simpleFood.vitaminB1Property(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(vitaminB2Input.textProperty(), simpleFood.vitaminB2Property(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(vitaminB3Input.textProperty(), simpleFood.vitaminB3Property(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(vitaminB5Input.textProperty(), simpleFood.vitaminB5Property(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(vitaminB6Input.textProperty(), simpleFood.vitaminB6Property(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(vitaminB7Input.textProperty(), simpleFood.vitaminB7Property(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(vitaminB8Input.textProperty(), simpleFood.vitaminB8Property(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(vitaminB9Input.textProperty(), simpleFood.vitaminB9Property(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(vitaminB12Input.textProperty(), simpleFood.vitaminB12Property(), new BigDecimalStringConverter());
    }

    /**
     * Convenience function for the macro / micronutrient GridPanes which contain only TextFields which should be numeric
     * @param childNodes The list of child nodes from either the macro or micronutrient grid panes
     */
    private void addDecimalTextFieldFormattersForGridPane(ObservableList<Node> childNodes)
    {
        for(Node gridPaneNode : childNodes)
        {
            if (gridPaneNode instanceof TextField textField)
            {
                textField.setTextFormatter(TextUtils.getOptionalDecimalTextInputFormatter());
            }
        }
    }
}
