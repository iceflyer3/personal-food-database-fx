/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.util.builder;

import com.google.common.eventbus.EventBus;
import com.iceflyer3.pfd.ui.event.ShowNotificationEvent;
import javafx.scene.Node;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;

/**
 * Builder for creating and showing notifications.
 *
 * This is different from the other builders in that it builds an event for posting to the event bus
 * instead of a node to be added to the scene graph.
 */

@Component
public class NotificationBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(NotificationBuilder.class);

    private final EventBus eventBus;
    private static ShowNotificationEvent event;

    // TODO: Add ability to create a notification with a duration once the duration is added to the event.
    //       See similar comment in the ShowNotificationEvent class

    public NotificationBuilder(EventBus eventBus)
    {
        this.eventBus = eventBus;
    }

    /**
     * Create a new default notification (one with a message and a close button but no graphic)
     * to be displayed in the specified stage
     * @param message The message to be displayed in the notification
     * @param forStage The stage on which this notification should be shown
     * @return A reference to this object
     */
    public NotificationBuilder create(String message, String forStage)
    {
        LOG.debug("Creating a new notification with a message of {}", message);
        event = new ShowNotificationEvent(message, forStage);
        return this;
    }

    /**
     * Set a graphic on the notification
     * @param graphic The Node representing the graphic to be set
     * @return A reference to this object
     */
    public NotificationBuilder withGraphic(Node graphic)
    {
        event.setGraphic(graphic);
        return this;
    }

    /**
     * Hide the close button for the notification preventing users from manually closing the
     * notification.
     * @return A reference to this object
     */
    public NotificationBuilder withoutCloseButton()
    {
        event.setCloseButtonShown(false);
        return this;
    }

    /**
     * Post the notification to the event bus causing it to be displayed on the relevant stage.
     */
    public void show()
    {
        LOG.debug("Posting notification event to the event bus {}", eventBus);
        eventBus.post(event);
        event = null;
    }
}
