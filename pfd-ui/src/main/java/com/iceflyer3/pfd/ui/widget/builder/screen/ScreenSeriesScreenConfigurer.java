package com.iceflyer3.pfd.ui.widget.builder.screen;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023, 2025 Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.ui.widget.stepper.Steppable;
import com.iceflyer3.pfd.ui.widget.stepper.series.ScreenSeriesDecision;

/**
 * Used to provide the configuration for a screen when building a screen series widget.
 */
public interface ScreenSeriesScreenConfigurer
{
    ScreenSeriesScreenConfigurer usingComponent(Class<? extends Steppable> componentControllerClass);
    ScreenSeriesScreenConfigurer withConcludingDecision(ScreenSeriesDecision decision);
    ScreenSeriesScreenConfigurer withComponentInitializationDataItem(String key, Object value);
    ScreenSeriesScreenConfigurer isInitial();
    ScreenSeriesScreenConfigurer isFinal();
}
