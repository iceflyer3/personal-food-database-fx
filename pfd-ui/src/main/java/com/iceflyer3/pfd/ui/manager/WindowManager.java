package com.iceflyer3.pfd.ui.manager;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.PfdApplication;
import com.iceflyer3.pfd.constants.ApplicationSections;
import com.iceflyer3.pfd.constants.SceneSets;
import com.iceflyer3.pfd.ui.controller.Action;
import com.iceflyer3.pfd.ui.controller.PopoutRootController;
import com.iceflyer3.pfd.ui.controller.dialog.AbstractDialogController;
import com.iceflyer3.pfd.ui.controller.dialog.FatalErrorDialogController;
import com.iceflyer3.pfd.ui.controller.dialog.LoadingNotificationDialogController;
import com.iceflyer3.pfd.ui.controller.dialog.StepperAdapterDialogController;
import com.iceflyer3.pfd.ui.listener.Destroyable;
import com.iceflyer3.pfd.ui.util.ControllerUtils;
import com.iceflyer3.pfd.ui.util.FxmlLoadResult;
import com.iceflyer3.pfd.ui.util.builder.DialogRequestBuilder;
import com.iceflyer3.pfd.ui.widget.stepper.StepOrchestrator;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.*;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Class that handles the opening of additional stages for other windows that need to be shown
 * by the application including both dialog and popout windows.
 *
 * If opening a popout window it will also be closed automatically when the main application window is
 * closed.
 */
@Component
public class WindowManager {
    public static final String DIALOG_WINDOW = "dialog";
    public static final String DIALOG_FATAL_ERROR = "fatalErrorDialog";

    private final ControllerUtils controllerUtils;

    // Tracks all open top-level windows in the application (including the main window)
    private final Map<String, Stage> openWindows;
    private final Map<String, String> popoutWindowTitles;

    // Map of stage name to controller so that destroyable controllers may be destroyed
    private final Map<String, Destroyable> loadedDestroyableControllers;

    private WindowManager(ControllerUtils controllerUtils)
    {
        this.openWindows = new HashMap<>();
        this.controllerUtils = controllerUtils;

        this.popoutWindowTitles = new HashMap<>();
        this.loadedDestroyableControllers = new HashMap<>();
        initPopoutWindowTitles();
    }

    public void registerMainApplicationWindow(Stage mainStage)
    {
        openWindows.put("main", mainStage);
        mainStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                // Upon closing the main window also close any other open windows
                for(Map.Entry<String, Stage> entry : openWindows.entrySet())
                {
                    entry.getValue().close();
                }
            }
        });
    }

    /**
     * Convenience function for hiding a stage that has been registered with the window manager
     * @param stageName The name of the stage to hide
     */
    public void hideStage(String stageName)
    {
        Stage stage = openWindows.get(stageName);

        if (stage == null)
        {
            throw new IllegalArgumentException(String.format("No stage with name of %s has been registered with the window manager", stageName));
        }
        else
        {
            stage.hide();
        }
    }

    /**
     * Convenience function for showing a stage that has been registered with the window manager
     * @param stageName The name of the stage to show
     */
    public void showStage(String stageName)
    {
        Stage stage = openWindows.get(stageName);

        if (stage == null)
        {
            throw new IllegalArgumentException(String.format("No stage with name of %s has been registered with the window manager", stageName));
        }
        else
        {
            stage.show();
        }
    }

    // For the time being this is limited to application sections
    // May also be used for calculators in the future
    public void openPopout(OpenPopoutRequest openPopoutRequest)
    {
        String applicationSection = openPopoutRequest.getApplicationSection();

        // The name of popout stages is dynamic based on the section that was opened using format
        // {applicationSection}-popout.
        String stageName = String.format("%s-popout", applicationSection);

        if (openWindows.containsKey(stageName))
        {
            openWindows.get(stageName).toFront();
        }
        else
        {
            Stage newPopoutStage = new Stage(StageStyle.DECORATED);
            newPopoutStage.initModality(Modality.NONE);
            newPopoutStage.getIcons().add(new Image("/logo.png"));

            /*
             * Set initial width and height for reliable sizing upon display of popout
             * If these aren't set you can end up with some wonky default sizing
             *
             * By default the popout will be the same size as the main application window.
             */
            newPopoutStage.setHeight(PfdApplication.WINDOW_MIN_HEIGHT);
            newPopoutStage.setWidth(PfdApplication.WINDOW_MIN_WIDTH);

            /*
             * Popouts are their own fully fledged windows with the same height as the main
             * application window. They don't have the left nav like the main application window
             * does so the min width can be smaller
             */
            newPopoutStage.setMinHeight(PfdApplication.WINDOW_MIN_HEIGHT);
            newPopoutStage.setMinWidth(675);

            newPopoutStage.setTitle(popoutWindowTitles.get(applicationSection));

            openWindows.put(stageName, newPopoutStage);
            setStageOnCloseAction(newPopoutStage, stageName);

            // Load the controller via FXMLLoader
            FxmlLoadResult<PopoutRootController> loadResult = controllerUtils.loadFxml(SceneSets.APP_POPOUT);
            loadedDestroyableControllers.put(stageName, loadResult.getController());

            // Setup the new scene
            Scene popoutScene = new Scene(new VBox());
            popoutScene.getStylesheets().addAll("/styles/fonts.css", "/styles/theme-dark.css");
            popoutScene.setRoot(loadResult.getRootNode());
            newPopoutStage.setScene(popoutScene);

            // Initialize the controller
            PopoutRootController controller = loadResult.getController();
            Map<String, Object> popoutControllerData = new HashMap<>();
            popoutControllerData.put(PopoutRootController.DATA_SECTION_TO_OPEN, applicationSection);
            controller.initHostingStage(newPopoutStage, stageName);
            controller.initUser(openPopoutRequest.getUser());
            controller.initControllerData(popoutControllerData);

            // Show the stage
            newPopoutStage.show();
        }
    }

    /**
     * Convenience function for opening the confirmation dialog that prompts for user confirmation
     * before executing some action
     * @param confirmationAction The action that should be executed upon user confirmation
     */
    public void openConfirmationDialog(Window rootWindow, Action confirmationAction)
    {
        OpenDialogRequest<Boolean> request = new OpenDialogRequest<>(rootWindow, SceneSets.DIALOG_CONFIRM_ACTION, hasConfirmed -> {
            if (hasConfirmed)
            {
                confirmationAction.perform();
            }
        });
        request.setHeight(120);
        request.setWidth(350);
        request.setDialogTitle("Confirm Action");
        this.openDialog(request);
    }

    /**
     * Convenience function for opening the loading dialog that displays while a screen loads its
     * data and blocks user interaction with the screen.
     * @param rootWindow The root window which will own the new stage that comprises the dialog window
     */
    public void openLoadingDialog(Window rootWindow, String loadingMessage)
    {
        /*
         * In theory there shouldn't ever be any dialogs open by the time a new screen hits initialization.
         *
         * But if there are any open dialogs (this one potentially included if we change screens too fast)
         * then be sure to close the previous dialog before opening the new loading dialog.
         */
        if (openWindows.containsKey(DIALOG_WINDOW))
        {
            openWindows.get(DIALOG_WINDOW).close();
            openWindows.remove(DIALOG_WINDOW);
        }

        OpenDialogRequest<Void> request = new OpenDialogRequest<>(rootWindow, SceneSets.DIALOG_LOADING_NOTIFICATION, new VoidDialogCallback());
        request.setClosable(false);
        request.setAlwaysOnTop(true);
        request.getDialogData().put(LoadingNotificationDialogController.DATA_LOADING_MESSAGE, loadingMessage);
        this.openDialog(request);
    }

    /**
     * Convenience function that opens the License and Legal Disclaimer dialog window.
     * @param rootWindow The root window which will own the new stage that comprises the dialog window
     */
    public void openDisclaimerDialog(Window rootWindow)
    {
        OpenDialogRequest<Void> request = new OpenDialogRequest<>(rootWindow, SceneSets.DIALOG_DISCLAIMER, new VoidDialogCallback());
        request.setHeight(550);
        request.setWidth(600);
        request.setClosable(false);
        this.openDialog(request);
    }

    /**
     * Opens the fatal error dialog that is shown when the application encounters some manner of
     * unexpected application state that it cannot recover from. The only action the user may take
     * from this dialog is to close the application.
     *
     * This dialog uses its own stage and blocks interaction with all other application windows.
     *
     * Theoretically this should never actually be shown because most of the errors that would cause
     * it to be shown are developer / programming errors. If it has then the user almost certainly
     * can't do anything to fix the error outside of attempting to restart the application which I
     * don't think likely to work anyways.
     */
    public void openFatalErrorDialog()
    {
        Stage dialogStage = new Stage(StageStyle.UNDECORATED);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        dialogStage.setAlwaysOnTop(true);

        openWindows.put(DIALOG_FATAL_ERROR, dialogStage);
        setStageOnCloseAction(dialogStage, DIALOG_WINDOW);

        FxmlLoadResult<FatalErrorDialogController> loadResult = controllerUtils.loadDialogFxml(SceneSets.DIALOG_FATAL_ERROR);

        Scene dialogScene = new Scene(new VBox());
        dialogScene.getStylesheets().addAll("/styles/fonts.css", "/styles/theme-dark.css");
        dialogScene.setRoot(loadResult.getRootNode());
        dialogStage.setScene(dialogScene);
        dialogStage.centerOnScreen();
        dialogStage.show();
        dialogStage.toFront();
    }

    /**
     * Open a stepper widget that is wrapped by and will display in a separate dialog window.
     * @param parentWindow The parent window that will own the new dialog window
     * @param stepperWidget The FxmlLoadResult of the loaded stepper widget that should be displayed in the dialog
     * @param dialogTitle The title of the new dialog window
     */
    public void openStepperDialog(Window parentWindow, FxmlLoadResult<StepOrchestrator> stepperWidget, String dialogTitle)
    {
        // Ensure the request is set up appropriately to wrap the desired stepper in a dialog adapter controller
        OpenDialogRequest<Void> dialogRequest = DialogRequestBuilder
                .create(parentWindow, new VoidDialogCallback())
                .withDialogDataItem(StepperAdapterDialogController.STEPPER_NODE_KEY, stepperWidget.getRootNode())
                .withDialogDataItem(StepperAdapterDialogController.STEPPER_CONTROLLER_KEY, stepperWidget.getController())
                .displayApplicationSet(SceneSets.DIALOG_STEPPER_ADAPTER)
                .withTitle(dialogTitle)
                .build();

        openDialog(dialogRequest);
    }

    public <T> void openDialog(OpenDialogRequest<T> request)
    {
        // Close any already open dialog windows. Only one dialog may be opened at a time.
        if (openWindows.containsKey(DIALOG_WINDOW))
        {
            close(DIALOG_WINDOW);
        }

        // Initialize dialog window properties
        String dialogTitle = request.getDialogTitle() == null ? "Dialog" : request.getDialogTitle();
        StageStyle stageStyle = request.isClosable() ? StageStyle.UTILITY : StageStyle.UNDECORATED;
        Stage dialogStage = new Stage(stageStyle);
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(request.getParentWindow());
        dialogStage.setAlwaysOnTop(request.isAlwaysOnTop());
        dialogStage.setResizable(false); // TODO: This apparently is straight up broken on Linux and just doesn't prevent resizing. So we should make sure we're setting the height and width of all dialogs.
        dialogStage.setTitle(dialogTitle);

        if (request.getHeight() != -1)
        {
            dialogStage.setMaxHeight(request.getHeight());
            dialogStage.setMinHeight(request.getHeight());
            dialogStage.setHeight(request.getHeight());
        }

        if (request.getWidth() != -1)
        {
            dialogStage.setMaxWidth(request.getWidth());
            dialogStage.setMinWidth(request.getWidth());
            dialogStage.setWidth(request.getWidth());
        }

        openWindows.put(DIALOG_WINDOW, dialogStage);
        setStageOnCloseAction(dialogStage, DIALOG_WINDOW);

        // Initialize the node to be displayed in the dialog
        Parent dialogRootNode;
        if (request.getApplicationSetToDisplay() != null)
        {
            FxmlLoadResult<AbstractDialogController<T>> loadResult = controllerUtils.loadDialogFxml(request.getDialogSet());
            AbstractDialogController<T> controller = loadResult.getController();

            if (loadResult.getController() instanceof Destroyable destroyableController)
            {
                loadedDestroyableControllers.put(DIALOG_WINDOW, destroyableController);
            }

            controller.initControllerData(request.getDialogData());
            controller.setCallback(request.getCallback());
            dialogRootNode = loadResult.getRootNode();
        }
        else if (request.getNodeToDisplay() != null)
        {
            dialogRootNode = request.getNodeToDisplay();
        }
        else
        {
            throw new IllegalStateException("You must specify either an application set or a node that should be displayed in the dialog window");
        }

        // Initialize and set the scene for the dialog, then display it
        Scene dialogScene = new Scene(dialogRootNode);
        dialogScene.getStylesheets().addAll("/styles/fonts.css", "/styles/theme-dark.css");

        dialogStage.setScene(dialogScene);
        dialogStage.show();
    }

    public void close(String stageName)
    {
        Stage stage = openWindows.get(stageName);
        openWindows.remove(stageName);

        if (stage != null)
        {
            // If the window we're closing is destroyable then be sure to destroy it
            Destroyable destroyableController = loadedDestroyableControllers.get(stageName);
            if (destroyableController != null)
            {
                destroyableController.destroy();
                loadedDestroyableControllers.remove(stageName);
            }

            stage.close();
        }
    }

    private void setStageOnCloseAction(Stage stage, String stageName)
    {
        // If the "x" button is clicked then be sure to still clean up
        // by closing the stage and removing it from our list of open windows.
        stage.setOnCloseRequest(event -> close(stageName));
    }

    private void initPopoutWindowTitles()
    {
        popoutWindowTitles.put(ApplicationSections.FOOD_DATABASE, "Food Database Section Popout");
        popoutWindowTitles.put(ApplicationSections.MEAL_PLANNING, "Meal Planning Section Popout");
        popoutWindowTitles.put(ApplicationSections.RECIPES, "Recipe Section Popout");
        popoutWindowTitles.put(ApplicationSections.ABOUT, "About Section Popout");
        popoutWindowTitles.put(ApplicationSections.SETTINGS, "Settings Section Popout");
    }
}
