package com.iceflyer3.pfd.ui.controller.foods;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.google.common.eventbus.EventBus;
import com.iceflyer3.pfd.constants.ApplicationSections;
import com.iceflyer3.pfd.constants.SceneSets;
import com.iceflyer3.pfd.enums.UnitOfMeasure;
import com.iceflyer3.pfd.ui.event.PublishBreadcrumbEvent;
import com.iceflyer3.pfd.ui.controller.AbstractTaskController;
import com.iceflyer3.pfd.ui.controller.HostStageAware;
import com.iceflyer3.pfd.ui.model.api.ServingSizeModel;
import com.iceflyer3.pfd.ui.model.proxy.food.ReadOnlyFoodModelProxy;
import com.iceflyer3.pfd.ui.model.viewmodel.ServingSizeViewModel;
import com.iceflyer3.pfd.ui.util.LayoutUtils;
import com.iceflyer3.pfd.ui.util.builder.NotificationBuilder;
import com.iceflyer3.pfd.ui.util.ScreenLoadEventUtils;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TitledPane;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Map;


@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ViewFoodController extends AbstractTaskController implements HostStageAware {

    private ReadOnlyFoodModelProxy foodProxy;
    private String hostingStageName;

    @FXML
    public Label screenLabel;

    @FXML
    public TitledPane servingSizesPane;

    @FXML
    public Label nameLabel;

    @FXML
    public Label typeLabel;

    @FXML
    public Label brandLabel;

    @FXML
    public Label caloriesLabel;

    @FXML
    public Label giLabel;

    @FXML
    public Label foodSourceLabel;

    @FXML
    public Label proteinLabel;

    @FXML
    public Label carbsLabel;

    @FXML
    public Label sugarsLabel;

    @FXML
    public Label fiberLabel;

    @FXML
    public Label totalFatsLabel;

    @FXML
    public Label transFatLabel;

    @FXML
    public Label monoFatLabel;

    @FXML
    public Label polyFatLabel;

    @FXML
    public Label satFatLabel;

    @FXML
    public Label cholesterolLabel;

    @FXML
    public Label ironLabel;

    @FXML
    public Label manganeseLabel;

    @FXML
    public Label copperLabel;

    @FXML
    public Label zincLabel;

    @FXML
    public Label calciumLabel;

    @FXML
    public Label magnesiumLabel;

    @FXML
    public Label phosphorusLabel;

    @FXML
    public Label potassiumLabel;

    @FXML
    public Label sodiumLabel;

    @FXML
    public Label sulfurLabel;

    @FXML
    public Label vitaminALabel;

    @FXML
    public Label vitaminCLabel;

    @FXML
    public Label vitaminDLabel;

    @FXML
    public Label vitaminELabel;

    @FXML
    public Label vitaminKLabel;

    @FXML
    public Label vitaminB1Label;

    @FXML
    public Label vitaminB2Label;

    @FXML
    public Label vitaminB3Label;

    @FXML
    public Label vitaminB5Label;

    @FXML
    public Label vitaminB6Label;

    @FXML
    public Label vitaminB7Label;

    @FXML
    public Label vitaminB8Label;

    @FXML
    public Label vitaminB9Label;

    @FXML
    public Label vitaminB12Label;

    public ViewFoodController(EventBus eventBus,
                              NotificationBuilder notificationBuilder,
                              ScreenLoadEventUtils loadEventBuilder,
                              TaskExecutor taskExecutor) {
        super(eventBus, notificationBuilder, loadEventBuilder, taskExecutor);
    }

    @Override
    public void initialize() {
        VBox.setMargin(screenLabel, LayoutUtils.getHeaderLabelMarginInsets());
    }

    @Override
    public void initControllerData(Map<String, Object> controllerData) {
        loadEventBuilder.emitBegin("Searching for food details", hostingStageName);
        foodProxy = (ReadOnlyFoodModelProxy) controllerData.get(ReadOnlyFoodModelProxy.class.getName());

        initFoodData();
        initServingSizes();
        loadEventBuilder.emitCompleted(hostingStageName);
    }

    @Override
    protected void registerDefaultListeners() {

    }

    @Override
    public void initHostingStageName(String stageName) {
        hostingStageName = stageName;
        PublishBreadcrumbEvent landingScreenBreadcrumb = new PublishBreadcrumbEvent("View Food", ApplicationSections.FOOD_DATABASE, SceneSets.VIEW_FOOD, stageName, 2);
        eventBus.post(landingScreenBreadcrumb);
    }

    private void initFoodData()
    {
        nameLabel.setText(foodProxy.getName());
        typeLabel.setText(foodProxy.getFoodType().name());
        brandLabel.setText(foodProxy.getBrand());
        foodSourceLabel.setText(foodProxy.getSource());
        giLabel.setText(Integer.toString(foodProxy.getGlycemicIndex()));
        caloriesLabel.setText(foodProxy.getCalories().toPlainString());

        proteinLabel.setText(foodProxy.getProtein().toPlainString());
        carbsLabel.setText(foodProxy.getCarbohydrates().toPlainString());
        sugarsLabel.setText(foodProxy.getSugars().toPlainString());
        fiberLabel.setText(foodProxy.getFiber().toPlainString());
        totalFatsLabel.setText(foodProxy.getTotalFats().toPlainString());
        transFatLabel.setText(foodProxy.getTransFat().toPlainString());
        monoFatLabel.setText(foodProxy.getMonounsaturatedFat().toPlainString());
        polyFatLabel.setText(foodProxy.getPolyunsaturatedFat().toPlainString());
        satFatLabel.setText(foodProxy.getSaturatedFat().toPlainString());

        cholesterolLabel.setText(foodProxy.getCholesterol().toPlainString());
        ironLabel.setText(foodProxy.getIron().toPlainString());
        manganeseLabel.setText(foodProxy.getManganese().toPlainString());
        copperLabel.setText(foodProxy.getCopper().toPlainString());
        zincLabel.setText(foodProxy.getZinc().toPlainString());
        calciumLabel.setText(foodProxy.getCalcium().toPlainString());
        magnesiumLabel.setText(foodProxy.getMagnesium().toPlainString());
        phosphorusLabel.setText(foodProxy.getPhosphorus().toPlainString());
        potassiumLabel.setText(foodProxy.getPotassium().toPlainString());
        sodiumLabel.setText(foodProxy.getSodium().toPlainString());
        sulfurLabel.setText(foodProxy.getSulfur().toPlainString());

        vitaminALabel.setText(foodProxy.getVitaminA().toPlainString());
        vitaminCLabel.setText(foodProxy.getVitaminC().toPlainString());
        vitaminDLabel.setText(foodProxy.getVitaminD().toPlainString());
        vitaminELabel.setText(foodProxy.getVitaminE().toPlainString());
        vitaminKLabel.setText(foodProxy.getVitaminK().toPlainString());
        vitaminB1Label.setText(foodProxy.getVitaminB1().toPlainString());
        vitaminB2Label.setText(foodProxy.getVitaminB2().toPlainString());
        vitaminB3Label.setText(foodProxy.getVitaminB3().toPlainString());
        vitaminB5Label.setText(foodProxy.getVitaminB5().toPlainString());
        vitaminB6Label.setText(foodProxy.getVitaminB6().toPlainString());
        vitaminB7Label.setText(foodProxy.getVitaminB7().toPlainString());
        vitaminB8Label.setText(foodProxy.getVitaminB8().toPlainString());
        vitaminB9Label.setText(foodProxy.getVitaminB9().toPlainString());
        vitaminB12Label.setText(foodProxy.getVitaminB12().toPlainString());
    }

    private void initServingSizes()
    {
        // Setup the servings table
        TableView<ServingSizeModel> servingSizeTable = new TableView<>();
        servingSizeTable.setEditable(false);
        servingSizeTable.setPlaceholder(new Label("No data available"));

        TableColumn<ServingSizeModel, BigDecimal> servingSizeColumn = new TableColumn<>("Serving Size");
        servingSizeColumn.setCellValueFactory(new PropertyValueFactory<>("servingSize"));

        TableColumn<ServingSizeModel, UnitOfMeasure> uomColumn = new TableColumn<>("Unit of Measure");
        uomColumn.setCellValueFactory(new PropertyValueFactory<>("unitOfMeasure"));

        servingSizeTable.getColumns().add(servingSizeColumn);
        servingSizeTable.getColumns().add(uomColumn);
        servingSizeTable.setItems(foodProxy.getServingSizes());
        servingSizesPane.setContent(servingSizeTable);
    }
}
