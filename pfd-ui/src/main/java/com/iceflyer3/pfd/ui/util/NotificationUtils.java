/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.util;

import com.iceflyer3.pfd.ui.event.ShowNotificationEvent;
import org.controlsfx.control.NotificationPane;

public class NotificationUtils
{
    private final NotificationPane notificationPane;

    public NotificationUtils(NotificationPane notificationPane)
    {
        this.notificationPane = notificationPane;
    }

    public void initPaneDefaultSettings()
    {
        notificationPane.setShowFromTop(false);
        notificationPane.setOnHidden(event -> {
            // Show the close button and hide the graphic by default for all notifications
            notificationPane.setCloseButtonVisible(true);
            notificationPane.setGraphic(null);
        });
    }

    public void showNotification(ShowNotificationEvent event)
    {
        if (event.getGraphic() != null)
        {
            notificationPane.setGraphic(event.getGraphic());
        }

        notificationPane.setCloseButtonVisible(event.isCloseButtonShown());
        notificationPane.setText(event.getText());
        notificationPane.show();
    }
}
