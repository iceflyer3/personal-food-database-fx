/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.controller.dialog;

import com.iceflyer3.pfd.constants.CssConstants;
import com.iceflyer3.pfd.enums.WeeklyInterval;
import com.iceflyer3.pfd.exception.InvalidApplicationStateException;
import com.iceflyer3.pfd.ui.cell.NonSundayDisabledDateCell;
import com.iceflyer3.pfd.ui.controller.Savable;
import com.iceflyer3.pfd.ui.format.WeeklyIntervalStringConverter;
import com.iceflyer3.pfd.ui.listener.Destroyable;
import com.iceflyer3.pfd.ui.listener.ObservableListListenerBinding;
import com.iceflyer3.pfd.ui.listener.ObservableValueListenerBinding;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import com.iceflyer3.pfd.data.request.mealplanning.AssociateMealPlanDayToCalendarDayRequest;
import com.iceflyer3.pfd.ui.model.api.mealplanning.MealPlanDayModel;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.MealPlanDayModelProxy;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.MealPlanModelProxy;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.MealPlanningProxyFactory;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.carbcycling.CarbCyclingPlanModelProxy;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.util.BindingUtils;
import com.iceflyer3.pfd.ui.util.TextUtils;
import com.iceflyer3.pfd.ui.util.builder.NotificationBuilder;
import com.iceflyer3.pfd.ui.listener.ModelPropertyValidationEventConfig;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.util.converter.NumberStringConverter;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.*;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class AssociateMealPlanCalendarDaysDialogController
        extends AbstractDialogController<AssociateMealPlanDayToCalendarDayRequest>
        implements Savable, Destroyable
{
    public static final int WIDTH = 900;
    public static final int HEIGHT = 500;

    public static final String DATA_USER = "user";
    public static final String DATA_PARENT_WINDOW_STAGE_NAME = "parentWindowStageName";

    private final NotificationBuilder notificationBuilder;
    private final MealPlanningProxyFactory mealPlanningProxyFactory;

    private final ObservableList<MealPlanDayModelProxy> mealPlanDays;
    private final ObservableList<CarbCyclingPlanModelProxy> carbCyclingPlans;
    private final List<CheckBox> selectDayCheckboxes;

    private final List<ObservableValueListenerBinding<?>> valueListenerBindings;
    private final List<ObservableListListenerBinding<?>> listListenerBindings;

    private TextField weeklyIntervalMonthsInput;
    private DatePicker forWeekDatePicker;
    private ChoiceBox<WeeklyInterval> weeklyIntervalChoiceBox;
    private String parentStageName;
    private MealPlanModelProxy mealPlan;
    private AssociateMealPlanDayToCalendarDayRequest calendarDayRequest;

    @FXML
    public VBox loadStatusContainer;

    @FXML
    public VBox mainContentContainer;

    @FXML
    public Button saveButton;

    @FXML
    public Hyperlink cancelButton;

    public AssociateMealPlanCalendarDaysDialogController(
            WindowManager windowManager,
            NotificationBuilder notificationBuilder,
            MealPlanningProxyFactory mealPlanningProxyFactory)
    {
        super(windowManager);
        this.notificationBuilder = notificationBuilder;
        this.mealPlanningProxyFactory = mealPlanningProxyFactory;

        valueListenerBindings = new ArrayList<>();
        listListenerBindings = new ArrayList<>();
        selectDayCheckboxes = new ArrayList<>();

        mealPlanDays = FXCollections.observableArrayList();
        carbCyclingPlans = FXCollections.observableArrayList();
    }

    @Override
    public void initialize()
    {
        // Ignore the invisible container for the purposes of layout so it won't take up any space
        loadStatusContainer.managedProperty().bind(loadStatusContainer.visibleProperty());
        mainContentContainer.managedProperty().bind(mainContentContainer.visibleProperty());
    }

    @Override
    protected void registerDefaultListeners()
    {
        saveButton.setOnAction(this::save);
        cancelButton.setOnAction(event -> windowManager.close(WindowManager.DIALOG_WINDOW));
    }

    @Override
    public void initControllerData(Map<String, Object> controllerData)
    {
        UserViewModel currentUser = (UserViewModel) controllerData.get(DATA_USER);
        parentStageName = (String) controllerData.get(DATA_PARENT_WINDOW_STAGE_NAME);

        calendarDayRequest = new AssociateMealPlanDayToCalendarDayRequest(currentUser);

        // Initialize dialog validation state
        updateSaveValidity();

        // First look up the active meal plan for the user
        mealPlanningProxyFactory.getActiveMealPlanForUser(currentUser.getUserId(), (wasMealPlanRetrievalSuccessful, activeMealPlanResult) -> {
            if (wasMealPlanRetrievalSuccessful)
            {
                if (activeMealPlanResult.isPresent())
                {
                    mealPlan = activeMealPlanResult.get();

                    /*
                     * Initialize the non-loading notification section of the screen.
                     * The default first tab is the Meal Plan tab.
                     *
                     * The loading screen is shown by default on first load so doesn't
                     * need to be manually toggled here
                     */
                    Tab mealPlanTab = createMealPlanTab();
                    ObservableListListenerBinding<MealPlanDayModelProxy> plannedDaysListChangeBinding = new ObservableListListenerBinding<>(mealPlanDays);
                    plannedDaysListChangeBinding.bindChangeListener(change -> {
                        ((BorderPane) mealPlanTab.getContent()).setLeft(createScrollableDaysPane(change.getList()));
                    });
                    listListenerBindings.add(plannedDaysListChangeBinding);

                    Tab carbCyclingTab = createCarbCyclingTab();

                    TabPane tabPane = new TabPane();
                    tabPane.getTabs().addAll(mealPlanTab, carbCyclingTab);
                    VBox.setVgrow(tabPane, Priority.ALWAYS);
                    mainContentContainer.getChildren().add(0, tabPane);
                }
                else
                {
                    /*
                     * By this point we should actually be guaranteed to have a meal plan. There must
                     * be an active plan to get to the screen which opens this dialog
                     */
                    LOG.error("An error occurred during association", new InvalidApplicationStateException("Attempted to open the dialog to associate a planned day to a calendar day but no active meal plan was found."));
                    windowManager.openFatalErrorDialog();
                }
            }
            else
            {
                notificationBuilder.create("An error has occurred while searching for an active meal plan", parentStageName).show();
                windowManager.close(WindowManager.DIALOG_WINDOW);
            }
        });
    }

    private void toggleLoadingScreen(boolean isLoading)
    {
        loadStatusContainer.setVisible(isLoading);
        mainContentContainer.setVisible(!isLoading);
    }

    private Tab createMealPlanTab()
    {
        Tab mealPlanTab = new Tab("Meal Plan");
        mealPlanTab.setClosable(false);

        // Refresh the days for the meal plan on tab selection
        ObservableValueListenerBinding<Boolean> mealPlanTabSelectionBinding = new ObservableValueListenerBinding<>(mealPlanTab.selectedProperty());
        mealPlanTabSelectionBinding.bindChangeListener((observable, wasSelected, isSelected) -> {
            if (isSelected)
            {
                toggleLoadingScreen(true);
                mealPlanningProxyFactory.getDaysForMealPlan(mealPlan, (wasMealPlanDayRetrievalSuccessful, mealPlanDaysResult) -> {
                    if (wasMealPlanDayRetrievalSuccessful)
                    {
                        VBox calendarDetailsPane = createCalendarDayDetailsPane();
                        BorderPane borderPane = new BorderPane();
                        borderPane.setCenter(calendarDetailsPane);
                        mealPlanTab.setContent(borderPane);

                        // Filter out any empty days with no nutritional info yet
                        mealPlanDaysResult = mealPlanDaysResult.stream().filter(day -> day.getCalories().compareTo(BigDecimal.ZERO) > 0).toList();
                        mealPlanDays.setAll(mealPlanDaysResult);

                        reset();
                        toggleLoadingScreen(false);
                    }
                    else
                    {
                        notificationBuilder.create("An error has occurred while looking for the days for the meal plan", parentStageName).show();
                        windowManager.close(WindowManager.DIALOG_WINDOW);
                    }
                });
            }
        });
        valueListenerBindings.add(mealPlanTabSelectionBinding);

        return mealPlanTab;
    }

    private Tab createCarbCyclingTab()
    {
        BorderPane borderPane = new BorderPane();

        Tab carbCyclingTab = new Tab("Carb Cycling Plan");
        carbCyclingTab.setClosable(false);
        carbCyclingTab.setContent(borderPane);

        // Refresh the carb cycling plans on tab selection
        ObservableValueListenerBinding<Boolean> carbCyclingTabSelectionBinding = new ObservableValueListenerBinding<>(carbCyclingTab.selectedProperty());
        carbCyclingTabSelectionBinding.bindChangeListener((observable, wasSelected, isSelected) -> {
            if (isSelected)
            {
                toggleLoadingScreen(true);
                mealPlanningProxyFactory.getCarbCyclingPlansForMealPlan(mealPlan.getId(), (wasCarbCyclingPlanSearchSuccessful, carbCyclingPlansResult) -> {
                    if (wasCarbCyclingPlanSearchSuccessful)
                    {
                        carbCyclingPlans.setAll(carbCyclingPlansResult);

                        // Add the carb cycling plan choice box
                        HBox container = new HBox(10);
                        container.setAlignment(Pos.CENTER);
                        container.setPadding(new Insets(20, 0, 20, 20));
                        container.setMaxWidth(Double.MAX_VALUE);


                        if (carbCyclingPlans.size() == 0)
                        {
                            Label noPlansFoundLabel = new Label("No carb cycling plans were found");
                            container.getChildren().add(noPlansFoundLabel);
                        }
                        else
                        {
                            Label selectPlanLabel = new Label("Select a carb cycling plan");
                            ChoiceBox<CarbCyclingPlanModelProxy> planChoiceBox = new ChoiceBox<>(carbCyclingPlans);
                            HBox.setHgrow(planChoiceBox, Priority.ALWAYS);
                            container.getChildren().addAll(selectPlanLabel, planChoiceBox);

                            // Refresh the list of days when the carb cycling plan is selected
                            ObservableValueListenerBinding<CarbCyclingPlanModelProxy> planChoiceBoxSelectionBinding = new ObservableValueListenerBinding<>(planChoiceBox.valueProperty());
                            planChoiceBoxSelectionBinding.bindChangeListener((selectedValueObservable, oldValue, newValue) -> {
                                toggleLoadingScreen(true);
                                mealPlanningProxyFactory.getCarbCyclingDaysForMealPlan(newValue, (wasCarbCyclingDaySearchSuccessful, proxyResult) -> {
                                    if (wasCarbCyclingDaySearchSuccessful)
                                    {
                                        // Filter out any empty carb cycling days with no nutritional info
                                        proxyResult = proxyResult.stream().filter(day -> day.getCalories().compareTo(BigDecimal.ZERO) > 0).toList();

                                        // With plan selected and days for that plan loaded populate the rest of the tab
                                        VBox calendarDetailsPane = createCalendarDayDetailsPane();
                                        ((BorderPane) carbCyclingTab.getContent()).setCenter(calendarDetailsPane);
                                        ((BorderPane) carbCyclingTab.getContent()).setLeft(createScrollableDaysPane(proxyResult));

                                        reset();
                                        toggleLoadingScreen(false);
                                    }
                                    else
                                    {
                                        notificationBuilder.create("An error has occurred while looking for the days for the carb cycling plan", parentStageName).show();
                                        windowManager.close(WindowManager.DIALOG_WINDOW);
                                    }
                                });
                            });
                        }

                        ((BorderPane) carbCyclingTab.getContent()).setTop(container);
                        toggleLoadingScreen(false);
                    }
                    else
                    {
                        notificationBuilder.create("An error has occurred while looking for the carb cycling plans for the meal plan", parentStageName).show();
                        windowManager.close(WindowManager.DIALOG_WINDOW);
                    }
                });
            }
            else
            {
                // The main content should be removed when navigating away from this tab until a new plan is selected
                // Otherwise stale data will be shown upon a subsequent navigation to the tab after the first navigation
                ((BorderPane) carbCyclingTab.getContent()).setCenter(null);
                ((BorderPane) carbCyclingTab.getContent()).setLeft(null);
            }
        });


        return carbCyclingTab;
    }

    private <T extends MealPlanDayModel> ScrollPane createScrollableDaysPane(List<T> days)
    {
        // Width of both the scrollpane and the vboxes contained within the scrollpane
        // Always one third of the total dialog width
        double paneWidth = WIDTH / 3.0;

        VBox container = new VBox(20);

        Label plannedDaysLabel = new Label("Select a planned day");
        plannedDaysLabel.setAlignment(Pos.CENTER);
        plannedDaysLabel.setMaxWidth(Double.MAX_VALUE);
        plannedDaysLabel.getStyleClass().add(CssConstants.CLASS_UNDERLINE);

        TilePane tilePane = new TilePane(Orientation.VERTICAL, 0, 20);
        container.getChildren().addAll(plannedDaysLabel, tilePane);

        for(T day : days)
        {
            Label mealNameLabel = new Label(day.getDayLabel());
            mealNameLabel.getStyleClass().addAll(CssConstants.CLASS_TEXT_SUB_HEADER, CssConstants.CLASS_BOLD);

            Label caloriesLabel = new Label(String.format("Calories %s", day.getCalories()));
            Label proteinLabel = new Label(String.format("Protein %s", day.getProtein()));
            Label carbsLabel = new Label(String.format("Carbohydrates %s", day.getCarbohydrates()));
            Label totalFatsLabel = new Label(String.format("Total Fats %s", day.getTotalFats()));

            CheckBox selectPlannedDayCheckbox = new CheckBox("Select");
            selectDayCheckboxes.add(selectPlannedDayCheckbox);

            ObservableValueListenerBinding<Boolean> selectDayCheckboxBinding = new ObservableValueListenerBinding<>(selectPlannedDayCheckbox.selectedProperty());
            selectDayCheckboxBinding.bindChangeListener((observable, wasSelected, isSelected) -> {

                for(CheckBox checkbox : selectDayCheckboxes)
                {
                    if (checkbox.equals(selectPlannedDayCheckbox))
                    {
                        if (isSelected)
                        {
                            // Update the selected day
                            calendarDayRequest.setSelectedDay(day);
                        }
                        else
                        {
                            /*
                             * Only one checkbox for one of the listed days may ever be checked at a time.
                             * If a checkbox is unchecked then we've unselected the (now previously) selected
                             * day and should null it out.
                             */
                            calendarDayRequest.setSelectedDay(null);
                        }
                    }
                    else
                    {
                        // Uncheck all other checkboxes
                        checkbox.setSelected(false);
                    }
                }

                updateSaveValidity();
            });
            valueListenerBindings.add(selectDayCheckboxBinding);


            VBox dayVBox = new VBox(10);
            dayVBox.setMaxWidth(paneWidth);
            dayVBox.setMinWidth(paneWidth);
            dayVBox.setPrefWidth(paneWidth);
            dayVBox.setPadding(new Insets(10, 10, 10, 10));
            dayVBox.getStyleClass().add(CssConstants.CLASS_BACKGROUND_SHADOW_LIGHT);
            dayVBox.getChildren().addAll(mealNameLabel, caloriesLabel, proteinLabel, carbsLabel, totalFatsLabel, selectPlannedDayCheckbox);

            tilePane.getChildren().add(dayVBox);
        }

        ScrollPane daysScrollPane = new ScrollPane();
        daysScrollPane.setMaxWidth(paneWidth);
        daysScrollPane.setMinWidth(paneWidth);
        daysScrollPane.setPrefWidth(paneWidth);
        VBox.setVgrow(daysScrollPane, Priority.ALWAYS);
        daysScrollPane.setContent(container);
        return daysScrollPane;
    }

    private VBox createCalendarDayDetailsPane()
    {
        VBox container = new VBox(10);
        container.setPadding(new Insets(10, 0, 0, 20));
        Label headerLabel = new Label("Calendar Entry Details");
        headerLabel.getStyleClass().add(CssConstants.CLASS_TEXT_HEADLINE);
        container.getChildren().add(headerLabel);

        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setHgap(10);
        gridPane.setVgap(10);

        // Week starting row
        Label weekStartLabel = new Label("For week starting");
        forWeekDatePicker = new DatePicker();
        forWeekDatePicker.setMaxWidth(Double.MAX_VALUE);
        forWeekDatePicker.setEditable(false);
        forWeekDatePicker.setDayCellFactory(datePickerParam -> new NonSundayDisabledDateCell());

        ObservableValueListenerBinding<LocalDate> datePickerChangeListenerBinding = new ObservableValueListenerBinding<>(forWeekDatePicker.valueProperty());
        datePickerChangeListenerBinding.bindChangeListener((observable, oldDate, newDate) -> {
            calendarDayRequest.setWeekStartDate(newDate);
            updateSaveValidity();
        });
        valueListenerBindings.add(datePickerChangeListenerBinding);

        Label weeklyIntervalLabel = new Label("Weekly Interval");
        ObservableList<WeeklyInterval> weeklyIntervals = FXCollections.observableArrayList(Arrays.asList(WeeklyInterval.values()));
        weeklyIntervalChoiceBox = new ChoiceBox<>(weeklyIntervals);
        weeklyIntervalChoiceBox.setMaxWidth(Double.MAX_VALUE);
        weeklyIntervalChoiceBox.setConverter(new WeeklyIntervalStringConverter());
        weeklyIntervalChoiceBox.getSelectionModel().selectFirst();

        // Months interval row
        HBox forMonthsContainer = new HBox(10);
        forMonthsContainer.setVisible(false);
        forMonthsContainer.setAlignment(Pos.CENTER_LEFT);
        Label forLabel = new Label("For");

        weeklyIntervalMonthsInput = new TextField();
        weeklyIntervalMonthsInput.setTextFormatter(TextUtils.getRequiredDecimalTextInputFormatter());

        Bindings.bindBidirectional(weeklyIntervalMonthsInput.textProperty(), calendarDayRequest.weeklyIntervalMonthsProperty(), new NumberStringConverter());
        valueListenerBindings.add(BindingUtils.bindModelObservableValueToGuiControl(new ModelPropertyValidationEventConfig<>(calendarDayRequest, calendarDayRequest.weeklyIntervalMonthsProperty(), weeklyIntervalMonthsInput, this::updateSaveValidity)));

        Label monthsLabel = new Label("months");
        forMonthsContainer.getChildren().addAll(forLabel, weeklyIntervalMonthsInput, monthsLabel);

        // Binding for the weekly interval choice box. Has to be done here after the months container has been declared.
        ObservableValueListenerBinding<WeeklyInterval> weeklyIntervalChangeListenerBinding = new ObservableValueListenerBinding<>(weeklyIntervalChoiceBox.valueProperty());
        weeklyIntervalChangeListenerBinding.bindChangeListener((observable, oldInterval, newInterval) -> {
            forMonthsContainer.setVisible(newInterval != WeeklyInterval.NONE);
            calendarDayRequest.setWeeklyInterval(newInterval);
            updateSaveValidity();
        });
        valueListenerBindings.add(weeklyIntervalChangeListenerBinding);

        // Day selection description label row
        Label selectDaysOfWeekLabel = new Label("Select the day(s) of the week on which the meals for the selected planned day will be consumed");
        selectDaysOfWeekLabel.setWrapText(true);

        // Add controls (sans day check boxes) to grid pane and center them vertically
        gridPane.add(weekStartLabel, 0, 0, 4, 1);
        gridPane.add(forWeekDatePicker, 4, 0, 4, 1);
        gridPane.add(weeklyIntervalLabel, 0, 1, 4, 1);
        gridPane.add(weeklyIntervalChoiceBox, 4, 1, 4, 1);
        gridPane.add(forMonthsContainer, 0, 2, 8, 1);
        gridPane.add(selectDaysOfWeekLabel, 0, 3, 8, 1);

        GridPane.setValignment(weekStartLabel, VPos.CENTER);
        GridPane.setValignment(forWeekDatePicker, VPos.CENTER);
        GridPane.setValignment(weeklyIntervalLabel, VPos.CENTER);
        GridPane.setValignment(weeklyIntervalChoiceBox, VPos.CENTER);
        GridPane.setValignment(forMonthsContainer, VPos.CENTER);
        GridPane.setValignment(selectDaysOfWeekLabel, VPos.CENTER);

        // Add a checkbox for each day of the week to the grid
        int gridDayColumn = 0;
        int gridDayRow = 4;
        for(DayOfWeek dayOfWeek : DayOfWeek.values())
        {
            // Checkboxes have their own labels that may be used too but it was easier to get the grind to line up like this
            Label dayLabel = new Label(dayOfWeek.getDisplayName(TextStyle.SHORT, Locale.US));
            gridPane.add(dayLabel, gridDayColumn, gridDayRow);

            CheckBox dayCheckbox = new CheckBox();
            gridPane.add(dayCheckbox, gridDayColumn + 1, gridDayRow);

            ObservableValueListenerBinding<Boolean> dayCheckboxBinding = new ObservableValueListenerBinding<>(dayCheckbox.selectedProperty());
            dayCheckboxBinding.bindChangeListener((observable, wasSelected, isSelected) -> {
                if (isSelected)
                {
                    calendarDayRequest.getForDaysOfWeek().add(dayOfWeek);
                }
                else
                {
                    calendarDayRequest.getForDaysOfWeek().remove(dayOfWeek);
                }
                updateSaveValidity();
            });
            valueListenerBindings.add(dayCheckboxBinding);

            gridDayColumn += 2;

            // 4 days per row for the day checkboxes
            if (gridDayColumn % 8 == 0)
            {
                gridDayColumn = 0;
                gridDayRow += 1;
            }
        }

        container.getChildren().add(gridPane);
        return container;
    }

    @Override
    public void destroy()
    {
        valueListenerBindings.forEach(ObservableValueListenerBinding::unbind);
        listListenerBindings.forEach(ObservableListListenerBinding::unbind);
        valueListenerBindings.clear();
        listListenerBindings.clear();
    }

    @Override
    public void updateSaveValidity()
    {
        saveButton.setDisable(!calendarDayRequest.validate().wasSuccessful());
    }

    @Override
    public void save(ActionEvent event)
    {
        callback.complete(calendarDayRequest);
        windowManager.close(WindowManager.DIALOG_WINDOW);
    }

    /**
     * Reset the state of the UI, the backing view model, and the validation.
     */
    private void reset()
    {
        // Reset the state of the UI controls
        forWeekDatePicker.setValue(null);
        weeklyIntervalMonthsInput.clear();
        weeklyIntervalChoiceBox.getSelectionModel().selectFirst();
        selectDayCheckboxes.forEach(checkBox -> checkBox.setSelected(false));

        // Reset the model

        calendarDayRequest.reset();

        // Invalidate the form
        updateSaveValidity();
    }
}
