package com.iceflyer3.pfd.ui.controller.sections;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.enums.UnitOfMeasure;
import com.iceflyer3.pfd.ui.model.api.ServingSizeModel;
import com.iceflyer3.pfd.ui.model.api.owner.ServingSizeReader;
import com.iceflyer3.pfd.ui.model.viewmodel.ServingSizeViewModel;
import com.iceflyer3.pfd.ui.util.LayoutUtils;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TitledPane;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;

import java.math.BigDecimal;

/**
 * This screen section is a Titled Pane that features a tableview for displaying the list of
 * serving sizes for the entity
 * @param <T> The type of the owner of the serving sizes
 */
public class ViewServingSizeScreenSection<T extends ServingSizeReader> implements ScreenSection
{
    T entity;

    public ViewServingSizeScreenSection(T entity)
    {
        this.entity = entity;
    }

    @Override
    public Node create()
    {
        TitledPane servingSizesPane = new TitledPane();
        servingSizesPane.setText("Serving Sizes");
        VBox.setMargin(servingSizesPane, LayoutUtils.getSectionElementVerticalSpacing());

        TableView<ServingSizeModel> servingSizesTableView = setupServingSizesTableView();
        servingSizesPane.setContent(servingSizesTableView);

        return servingSizesPane;
    }

    private TableView<ServingSizeModel> setupServingSizesTableView()
    {
        // Setup the servings table
        TableView<ServingSizeModel> servingSizeTable = new TableView<>();
        servingSizeTable.setEditable(false);
        servingSizeTable.setPlaceholder(new Label("No data available"));

        TableColumn<ServingSizeModel, BigDecimal> servingSizeColumn = new TableColumn<>("Serving Size");
        servingSizeColumn.setCellValueFactory(new PropertyValueFactory<>("servingSize"));

        TableColumn<ServingSizeModel, UnitOfMeasure> uomColumn = new TableColumn<>("Unit of Measure");
        uomColumn.setCellValueFactory(new PropertyValueFactory<>("unitOfMeasure"));

        servingSizeTable.getColumns().add(servingSizeColumn);
        servingSizeTable.getColumns().add(uomColumn);
        servingSizeTable.setItems(entity.getServingSizes());

        return servingSizeTable;
    }
}
