package com.iceflyer3.pfd.ui.util;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.google.common.eventbus.EventBus;
import com.iceflyer3.pfd.ui.event.ScreenLoadBeginEvent;
import com.iceflyer3.pfd.ui.event.ScreenLoadCompletedEvent;
import org.springframework.stereotype.Component;

@Component
public class ScreenLoadEventUtils
{
    private final EventBus eventBus;

    public ScreenLoadEventUtils(EventBus eventBus)
    {
        this.eventBus = eventBus;
    }

    public void emitBegin(String loadMessage, String forStage)
    {
        ScreenLoadBeginEvent event = new ScreenLoadBeginEvent(loadMessage, forStage);
        eventBus.post(event);
    }

    public void emitCompleted(String forStage)
    {
        ScreenLoadCompletedEvent event = new ScreenLoadCompletedEvent(forStage);
        eventBus.post(event);
    }
}
