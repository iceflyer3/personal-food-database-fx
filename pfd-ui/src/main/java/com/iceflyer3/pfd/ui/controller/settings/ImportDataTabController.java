/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.controller.settings;

import com.google.common.eventbus.EventBus;
import com.iceflyer3.pfd.constants.ApplicationSections;
import com.iceflyer3.pfd.constants.CssConstants;
import com.iceflyer3.pfd.constants.FontAwesomeIcons;
import com.iceflyer3.pfd.data.factory.ImportExportDataTaskFactory;
import com.iceflyer3.pfd.data.task.data.ImportDataTask;
import com.iceflyer3.pfd.ui.controller.AbstractTaskController;
import com.iceflyer3.pfd.ui.controller.HostStageAware;
import com.iceflyer3.pfd.ui.enums.PfdActionType;
import com.iceflyer3.pfd.ui.listener.Destroyable;
import com.iceflyer3.pfd.ui.listener.ObservableValueListenerBinding;
import com.iceflyer3.pfd.ui.util.ScreenLoadEventUtils;
import com.iceflyer3.pfd.ui.util.builder.ActionNodeBuilder;
import com.iceflyer3.pfd.ui.util.builder.NotificationBuilder;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FilenameFilter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ImportDataTabController extends AbstractTaskController implements HostStageAware, Destroyable
{
    private final static Logger LOG = LoggerFactory.getLogger(ImportDataTabController.class);
    private final static String FOOD_TOGGLE_GROUP_NAME = "foodDataToggleGroup";

    @FXML
    public VBox dataImportTaskStatusContainer;

    @FXML
    public Label dataImportTaskStatusLabel;

    @FXML
    public VBox dataImportTabContainer;

    @FXML
    public ScrollPane importFileOptionsScrollPane;

    private final ImportExportDataTaskFactory importExportDataTaskFactory;
    private final ArrayList<String> filenamesToImport;

    private final File exportDirectory;

    private final Pattern filenameDateSearchRegex;

    private final Set<ObservableValueListenerBinding<Toggle>> toggleGroupBindings;

    private final Set<ToggleGroup> toggleGroups;

    private String hostingStageName;


    public ImportDataTabController(
            EventBus eventBus,
            NotificationBuilder notificationBuilder,
            ScreenLoadEventUtils loadEventBuilder,
            TaskExecutor taskExecutor,
            ImportExportDataTaskFactory importExportDataTaskFactory)
    {
        super(eventBus, notificationBuilder, loadEventBuilder, taskExecutor);
        this.importExportDataTaskFactory = importExportDataTaskFactory;
        this.filenamesToImport = new ArrayList<>();
        this.exportDirectory = new File("../data-export");
        this.filenameDateSearchRegex = Pattern.compile("\\d+");
        this.toggleGroupBindings = new HashSet<>();
        this.toggleGroups = new HashSet<>();
    }

    @Override
    public void initialize()
    {
        dataImportTaskStatusContainer.managedProperty().bind(dataImportTaskStatusContainer.visibleProperty());
        dataImportTabContainer.managedProperty().bind(dataImportTabContainer.visibleProperty());

        Label headerLabel = new Label("Select data to import");
        headerLabel.getStyleClass().addAll(CssConstants.CLASS_BOLD, CssConstants.CLASS_UNDERLINE);

        GridPane dataImportOptionsGridPane = new GridPane();
        dataImportOptionsGridPane.setHgap(20);
        dataImportOptionsGridPane.setVgap(10);
        dataImportOptionsGridPane.add(headerLabel, 0, 1);

        createFoodFileImportOptionsUI(dataImportOptionsGridPane);
        createNonFoodFileImportOptionsUI(ApplicationSections.MEAL_PLANNING, dataImportOptionsGridPane);
        createNonFoodFileImportOptionsUI(ApplicationSections.RECIPES, dataImportOptionsGridPane);

        Button importButton = ActionNodeBuilder
                .createIconButton("Import", FontAwesomeIcons.DOWNLOAD)
                .forActionType(PfdActionType.PRIMARY)
                .withAction(actionEvent -> {

                    if (filenamesToImport.size() == 0)
                    {
                        notificationBuilder.create(
                                "At least one file must be selected",
                                hostingStageName
                        ).show();
                    }
                    else if (filenamesToImport.stream().noneMatch(fileName -> fileName.contains(ApplicationSections.FOOD_DATABASE)))
                    {
                        notificationBuilder.create(
                                "Food data is required for import",
                                hostingStageName
                        ).show();
                    }
                    else
                    {
                        boolean areFileImportSelectionsValid = true;

                        // Max size of this list is three. One file for each application section
                        // of food, meal planning, and recipes.
                        if (filenamesToImport.size() == 2)
                        {
                            LocalDateTime selectedFileOneExportDate = parseDateFromFileName(filenamesToImport.get(0));
                            LocalDateTime selectedFileTwoExportDate = parseDateFromFileName(filenamesToImport.get(1));
                            areFileImportSelectionsValid = selectedFileOneExportDate.equals(selectedFileTwoExportDate);
                        }
                        else if (filenamesToImport.size() == 3)
                        {
                            LocalDateTime selectedFileOneExportDate = parseDateFromFileName(filenamesToImport.get(0));
                            LocalDateTime selectedFileTwoExportDate = parseDateFromFileName(filenamesToImport.get(1));
                            LocalDateTime selectedFileThreeExportDate = parseDateFromFileName(filenamesToImport.get(2));
                            areFileImportSelectionsValid = selectedFileOneExportDate.equals(selectedFileTwoExportDate);
                            areFileImportSelectionsValid = areFileImportSelectionsValid && selectedFileTwoExportDate.equals(selectedFileThreeExportDate);
                        }

                        if (areFileImportSelectionsValid)
                        {
                            ImportDataTask importDataTask = importExportDataTaskFactory.importData(new ArrayList<>(filenamesToImport));
                            dataImportTaskStatusLabel.textProperty().bind(importDataTask.messageProperty());

                            importDataTask.setOnRunning(workerStateEvent -> {
                                toggleDataImportStatusDisplay(true);
                            });
                            importDataTask.setOnSucceeded(workerStateEvent -> {
                                toggleDataImportStatusDisplay(false);
                                notificationBuilder.create("The data has been successfully imported!", hostingStageName).show();
                            });
                            importDataTask.setOnFailed(workerStateEvent -> {
                                toggleDataImportStatusDisplay(false);
                                LOG.error("An error has occurred while attempting to import the data", importDataTask.getException());
                                notificationBuilder.create("An error has occurred while attempting to import the data", hostingStageName).show();
                            });
                            taskExecutor.execute(importDataTask);

                            resetScreen();
                        }
                        else
                        {
                            notificationBuilder.create("All files to import must be from the same date", hostingStageName).show();
                        }

                    }
                })
                .build();
        importFileOptionsScrollPane.setContent(dataImportOptionsGridPane);
        dataImportTabContainer.getChildren().add(0, importButton);
    }

    @Override
    protected void registerDefaultListeners()
    {

    }

    @Override
    public void initHostingStageName(String stageName)
    {
        hostingStageName = stageName;
    }

    @Override
    public void destroy()
    {
        toggleGroupBindings.forEach(ObservableValueListenerBinding::unbind);
    }

    private void createFoodFileImportOptionsUI(GridPane parentContainer)
    {
        FilenameFilter filter = (dir, name) -> name.contains(ApplicationSections.FOOD_DATABASE);
        File[] foodDataExportFiles = exportDirectory.listFiles(filter);

        if (foodDataExportFiles == null)
        {
            parentContainer.add(new Label("No exported data was found to import"), 0, 1);
        }
        else
        {
            ToggleGroup foodOptionsToggleGroup = new ToggleGroup();
            foodOptionsToggleGroup.setUserData(FOOD_TOGGLE_GROUP_NAME);
            toggleGroups.add(foodOptionsToggleGroup);

            ObservableValueListenerBinding<Toggle> toggleGroupBinding = new ObservableValueListenerBinding<>(foodOptionsToggleGroup.selectedToggleProperty());
            toggleGroupBinding.bindChangeListener((observable, oldValue, newValue) -> {

                // Remove previously selected value (if any)
                String previouslySelectedFile = oldValue == null ? "none" : (String)oldValue.getUserData();
                filenamesToImport.remove(previouslySelectedFile);

                /*
                 * Not sure why but when programmatically selecting a toggle this listener appears
                 * to be firing twice. The first time with a null "newValue" and the second time
                 * with a null "oldValue".
                 *
                 * So if "newValue" is null we'll treat it as a selection of the None option.
                 */
                String selectedFile = newValue == null ? "none" : (String)newValue.getUserData();
                if (!selectedFile.equals("none"))
                {
                    filenamesToImport.add(0, selectedFile);
                }
            });
            toggleGroupBindings.add(toggleGroupBinding);

            VBox foodsDataImportOptions = createCheckBoxesForFileList(foodDataExportFiles, foodOptionsToggleGroup);

            Label foodDataLabel = new Label("Foods");
            foodDataLabel.getStyleClass().add(CssConstants.CLASS_UNDERLINE);
            parentContainer.add(foodDataLabel, 0, 2);
            parentContainer.add(foodsDataImportOptions, 0, 3);
        }
    }

    private void createNonFoodFileImportOptionsUI(String sectionName, GridPane parentContainer)
    {
        FilenameFilter filter = (dir, name) -> name.contains(sectionName);
        File[] dataExportFiles = exportDirectory.listFiles(filter);

        /* If we're not exporting food data and no data files were found then simply leave the UI
         * alone.
         *
         * In the best case scenario where food data files are present but no other data files are
         * present then no error message is shown and the food options are still presented.
         *
         * In the worst case scenario where we (for some reason) have non-food export files available
         * but no food files available then no options will be presented to the user and the "no data found"
         * message will be displayed instead. If options are shown then at best this situation leads to
         * incomplete import data that the user can't take meaningful action on anyway.
         */
        if (dataExportFiles != null)
        {
            ToggleGroup toggleGroup = new ToggleGroup();
            toggleGroups.add(toggleGroup);

            ObservableValueListenerBinding<Toggle> toggleGroupBinding = new ObservableValueListenerBinding<>(toggleGroup.selectedToggleProperty());
            toggleGroupBinding.bindChangeListener((observable, oldValue, newValue) -> {
                String selectedFile = (String)newValue.getUserData();
                String previouslySelectedFile = oldValue == null ? "" : (String)oldValue.getUserData();
                if (selectedFile.equals("none"))
                {
                    filenamesToImport.remove(previouslySelectedFile);
                }
                else
                {
                    filenamesToImport.add(selectedFile);
                    filenamesToImport.remove(previouslySelectedFile);
                }
            });
            toggleGroupBindings.add(toggleGroupBinding);

            VBox importOptionsVBox = createCheckBoxesForFileList(dataExportFiles, toggleGroup);
            if (sectionName.equals(ApplicationSections.MEAL_PLANNING))
            {
                Label mealPlanningDataLabel = new Label("Meal Planning");
                mealPlanningDataLabel.getStyleClass().add(CssConstants.CLASS_UNDERLINE);
                parentContainer.add(mealPlanningDataLabel, 1, 2);
                parentContainer.add(importOptionsVBox, 1, 3);
            }
            else
            {
                Label recipesDataLabel = new Label("Recipes");
                recipesDataLabel.getStyleClass().add(CssConstants.CLASS_UNDERLINE);
                parentContainer.add(recipesDataLabel, 2, 2);
                parentContainer.add(importOptionsVBox, 2, 3);
            }
        }
    }

    private VBox createCheckBoxesForFileList(File[] files, ToggleGroup toggleGroup)
    {
        VBox radioButtonsContainer = new VBox(10);

        RadioButton noneOption = new RadioButton("None");
        noneOption.setUserData("none");
        noneOption.setToggleGroup(toggleGroup);
        noneOption.setSelected(true);
        radioButtonsContainer.getChildren().add(noneOption);

        // Create a radio button for each file in the list of files
        for(File exportDataFile : files)
        {
            String fileName = exportDataFile.getName();
            LocalDateTime exportDate = parseDateFromFileName(fileName);
            String exportDateStr = exportDate.format(DateTimeFormatter.ofPattern("EEEE, d LLL yyyy"));

            RadioButton option = new RadioButton(exportDateStr);
            option.setUserData(fileName);
            option.setToggleGroup(toggleGroup);
            radioButtonsContainer.getChildren().add(option);
        }

        return radioButtonsContainer;
    }

    private LocalDateTime parseDateFromFileName(String fileName)
    {
        List<String> matches = filenameDateSearchRegex
                .matcher(fileName)
                .results()
                .map(MatchResult::group)
                .toList();

        return LocalDateTime.of(Integer.parseInt(matches.get(0)), Integer.parseInt(matches.get(1)), Integer.parseInt(matches.get(2)), 0, 0);
    }

    private void toggleDataImportStatusDisplay(boolean isVisible)
    {
        dataImportTaskStatusContainer.setVisible(isVisible);
        dataImportTabContainer.setVisible(!isVisible);
    }

    private void resetScreen()
    {
        // Select the "None" option for each ToggleGroup
        for(ToggleGroup toggleGroup : toggleGroups)
        {
            Toggle noneOption = toggleGroup.getToggles().stream().filter(toggle -> toggle.getUserData().equals("none")).findFirst().orElseThrow();
            noneOption.setSelected(true);
        }

        // Make sure no files are set to be imported
        filenamesToImport.clear();
    }
}
