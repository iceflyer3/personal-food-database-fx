package com.iceflyer3.pfd.ui.cell;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023, 2025 Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import javafx.scene.control.TableCell;
import javafx.util.StringConverter;

/**
 * Table cell implementation that allows the use of a string converter to determine the content of the cell
 * @param <TableType> The type of the TableView
 * @param <CellType> The type of the data in the cell
 */
public class StringConverterTableCell<TableType, CellType> extends TableCell<TableType, CellType>
{
    private final StringConverter<CellType> stringConverter;

    public StringConverterTableCell(StringConverter<CellType> stringConverter)
    {
        this.stringConverter = stringConverter;
    }

    @Override
    public void updateItem(CellType item, boolean empty)
    {
        super.updateItem(item, empty);

        if (empty || item == null)
        {
            this.setText(null);
            this.setGraphic(null);
        }
        else
        {
            this.setText(stringConverter.toString(this.getItem()));
        }
    }
}
