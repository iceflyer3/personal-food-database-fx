package com.iceflyer3.pfd.ui.controller.mealplanning.carbcycling;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.google.common.eventbus.EventBus;
import com.iceflyer3.pfd.constants.ApplicationSections;
import com.iceflyer3.pfd.constants.FontAwesomeIcons;
import com.iceflyer3.pfd.constants.SceneSets;
import com.iceflyer3.pfd.ui.controller.sections.mealplanning.CarbCyclingMealBasicInfoScreenSection;
import com.iceflyer3.pfd.ui.event.PublishBreadcrumbEvent;
import com.iceflyer3.pfd.ui.controller.AbstractTaskController;
import com.iceflyer3.pfd.ui.controller.HostStageAware;
import com.iceflyer3.pfd.ui.controller.Savable;
import com.iceflyer3.pfd.ui.controller.sections.EditIngredientScreenSection;
import com.iceflyer3.pfd.ui.controller.sections.MacronutrientBreakdownScreenSection;
import com.iceflyer3.pfd.ui.controller.sections.MicronutrientBreakdownScreenSection;
import com.iceflyer3.pfd.ui.controller.sections.ScreenSection;
import com.iceflyer3.pfd.ui.listener.Destroyable;
import com.iceflyer3.pfd.ui.listener.ObservableListListenerBinding;
import com.iceflyer3.pfd.ui.listener.ObservableValueListenerBinding;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.proxy.food.FoodProxyFactory;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.carbcycling.CarbCyclingMealModelProxy;
import com.iceflyer3.pfd.ui.util.LayoutUtils;
import com.iceflyer3.pfd.ui.util.builder.ActionNodeBuilder;
import com.iceflyer3.pfd.ui.util.builder.NotificationBuilder;
import com.iceflyer3.pfd.ui.util.ScreenLoadEventUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

// This is very similar to the Compose Meal Plan Meal. The main difference is they operate on
// different data types. But most of the setup is the same. So we'll suppress those warnings.
@SuppressWarnings("DuplicatedCode")
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ComposeCarbCyclingMealController extends AbstractTaskController implements HostStageAware, Savable, Destroyable
{
    public static final String DATA_MEAL_TO_COMPOSE = "mealToCompose";

    private final FoodProxyFactory foodProxyFactory;
    private final Set<ObservableValueListenerBinding<BigDecimal>> servingIncludedListenerBindings;

    private String hostingStageName;
    private Button saveButton;
    private CarbCyclingMealModelProxy carbCyclingMeal;

    private ObservableListListenerBinding<IngredientModel> ingredientListListenerBinding;

    @FXML
    public VBox rootContainer;

    @FXML
    public Label screenLabel;

    @FXML
    public AnchorPane buttonContainerAnchorPane;

    public ComposeCarbCyclingMealController(EventBus eventBus,
                                            NotificationBuilder notificationBuilder,
                                            ScreenLoadEventUtils loadEventBuilder,
                                            TaskExecutor taskExecutor,
                                            FoodProxyFactory foodProxyFactory)
    {
        super(eventBus, notificationBuilder, loadEventBuilder, taskExecutor);
        this.foodProxyFactory = foodProxyFactory;
        this.servingIncludedListenerBindings = new HashSet<>();
    }

    @Override
    public void initialize()
    {
        VBox.setMargin(screenLabel, LayoutUtils.getHeaderLabelMarginInsets());

        saveButton = ActionNodeBuilder
                .createIconButton("SAVE", FontAwesomeIcons.SAVE)
                .withAction(this::save)
                .build();
        saveButton.setDisable(true);
        AnchorPane.setRightAnchor(saveButton, 0.0);
        buttonContainerAnchorPane.getChildren().add(saveButton);
    }

    @Override
    protected void registerDefaultListeners()
    {

    }

    @Override
    public void initHostingStageName(String stageName)
    {
        this.hostingStageName = stageName;
    }

    @Override
    public void initControllerData(Map<String, Object> controllerData)
    {
        loadEventBuilder.emitBegin("Searching for carb cycling meal details", hostingStageName);

        carbCyclingMeal = (CarbCyclingMealModelProxy) controllerData.get(DATA_MEAL_TO_COMPOSE);

        PublishBreadcrumbEvent breadcrumb = new PublishBreadcrumbEvent("Compose Carb Cycling Meal", ApplicationSections.MEAL_PLANNING, SceneSets.CARB_CYCLING_COMPOSE_MEAL, hostingStageName, 4);
        breadcrumb.addData(DATA_MEAL_TO_COMPOSE, carbCyclingMeal);
        eventBus.post(breadcrumb);

        carbCyclingMeal.loadIngredients((wasSuccessful, resultMessage) -> {
            if (wasSuccessful)
            {
                // Load the list of all foods needed by the ingredients screen section
                foodProxyFactory.getAllFoods((foundFoodsSuccessfully, allFoodsList) ->
                {
                    if (foundFoodsSuccessfully)
                    {
                        // Add the ingredients, macronutrient, and micronutrient sections now that the ingredient data has been loaded.
                        ScreenSection basicDetailsSection = new CarbCyclingMealBasicInfoScreenSection<>(carbCyclingMeal);
                        ScreenSection ingredientsSection = new EditIngredientScreenSection<>(allFoodsList, carbCyclingMeal);
                        ScreenSection macroBreakdownSection = new MacronutrientBreakdownScreenSection<>(carbCyclingMeal);
                        ScreenSection microBreakdownSection = new MicronutrientBreakdownScreenSection<>(carbCyclingMeal);

                        ingredientListListenerBinding = new ObservableListListenerBinding<>(carbCyclingMeal.getIngredients());
                        ingredientListListenerBinding.bindChangeListener(change -> updateSaveValidity());
                        carbCyclingMeal.getIngredients().forEach(ingredient -> {
                            ObservableValueListenerBinding<BigDecimal> servingsIncludedBinding = new ObservableValueListenerBinding<>(ingredient.servingsIncludedProperty());
                            servingsIncludedBinding.bindChangeListener((observable, oldValue, newValue) -> updateSaveValidity());
                            servingIncludedListenerBindings.add(servingsIncludedBinding);
                        });

                        rootContainer.getChildren().addAll(
                                basicDetailsSection.create(),
                                ingredientsSection.create(),
                                macroBreakdownSection.create(),
                                microBreakdownSection.create()
                        );
                    }
                    else
                    {
                        notificationBuilder.create("The ingredients for this meal have failed to load!", hostingStageName).show();
                    }
                    loadEventBuilder.emitCompleted(hostingStageName);
                });
            }
            else
            {
                loadEventBuilder.emitCompleted(hostingStageName);
                notificationBuilder.create(resultMessage, hostingStageName).show();
            }
        });
    }

    @Override
    public void updateSaveValidity()
    {
        saveButton.setDisable(!carbCyclingMeal.validate().wasSuccessful());
    }

    @Override
    public void save(ActionEvent event)
    {
        carbCyclingMeal.saveIngredients(((wasSuccessful, resultSuccessful) -> notificationBuilder.create(resultSuccessful, hostingStageName).show()));
    }

    @Override
    public void destroy()
    {
        ingredientListListenerBinding.unbind();
        servingIncludedListenerBindings.forEach(binding -> binding.unbind());
        servingIncludedListenerBindings.clear();
    }
}
