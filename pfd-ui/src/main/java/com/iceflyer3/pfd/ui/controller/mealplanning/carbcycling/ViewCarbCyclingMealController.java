/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.controller.mealplanning.carbcycling;

import com.google.common.eventbus.EventBus;
import com.iceflyer3.pfd.constants.ApplicationSections;
import com.iceflyer3.pfd.constants.SceneSets;
import com.iceflyer3.pfd.ui.controller.AbstractTaskController;
import com.iceflyer3.pfd.ui.controller.HostStageAware;
import com.iceflyer3.pfd.ui.controller.sections.MacronutrientBreakdownScreenSection;
import com.iceflyer3.pfd.ui.controller.sections.MicronutrientBreakdownScreenSection;
import com.iceflyer3.pfd.ui.controller.sections.mealplanning.CarbCyclingMealBasicInfoScreenSection;
import com.iceflyer3.pfd.ui.event.PublishBreadcrumbEvent;
import com.iceflyer3.pfd.ui.listener.Destroyable;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.MealPlanningProxyFactory;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.carbcycling.ReadOnlyCarbCyclingMealModelProxy;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.util.LayoutUtils;
import com.iceflyer3.pfd.ui.util.ScreenLoadEventUtils;
import com.iceflyer3.pfd.ui.util.builder.NotificationBuilder;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.UUID;

@Component
public class ViewCarbCyclingMealController extends AbstractTaskController implements HostStageAware, Destroyable
{
    public static final String DATA_MEAL_ID = "carbCyclingMealId";

    private final MealPlanningProxyFactory mealPlanningProxyFactory;

    private String hostingStageName;

    private UserViewModel userViewModel;

    private MacronutrientBreakdownScreenSection<ReadOnlyCarbCyclingMealModelProxy> macrosSection;
    private MicronutrientBreakdownScreenSection<ReadOnlyCarbCyclingMealModelProxy> microsSection;

    private UUID mealId;

    @FXML
    public VBox rootContainer;

    @FXML
    public Label screenLabel;

    public ViewCarbCyclingMealController(EventBus eventBus,
                                         NotificationBuilder notificationBuilder,
                                         ScreenLoadEventUtils loadEventBuilder,
                                         TaskExecutor taskExecutor,
                                         MealPlanningProxyFactory mealPlanningProxyFactory)
    {
        super(eventBus, notificationBuilder, loadEventBuilder, taskExecutor);
        this.mealPlanningProxyFactory = mealPlanningProxyFactory;
    }

    @Override
    public void initialize()
    {
        VBox.setMargin(screenLabel, LayoutUtils.getHeaderLabelMarginInsets());
    }

    @Override
    protected void registerDefaultListeners()
    {

    }

    @Override
    public void initHostingStageName(String stageName)
    {
        hostingStageName = stageName;

        PublishBreadcrumbEvent breadcrumbEvent = new PublishBreadcrumbEvent("View Carb Cycling Meal", ApplicationSections.RECIPES, SceneSets.VIEW_RECIPE, hostingStageName, 4);
        breadcrumbEvent.addData(UserViewModel.class.getName(), userViewModel);
        breadcrumbEvent.addData(DATA_MEAL_ID, mealId);
        eventBus.post(breadcrumbEvent);
    }

    @Override
    public void initControllerData(Map<String, Object> controllerData)
    {
        userViewModel = (UserViewModel) controllerData.get(UserViewModel.class.getName());
        mealId = (UUID) controllerData.get(DATA_MEAL_ID);

        loadEventBuilder.emitBegin("Searching for meal details", hostingStageName);

        mealPlanningProxyFactory.getReadOnlyCarbCyclingMealById(mealId, (wasMealLoadSuccessful, carbCyclingMealProxy) -> {
            if (wasMealLoadSuccessful)
            {
                carbCyclingMealProxy.loadIngredients((wasIngredientLoadSuccessful, ingredientLoadResultMessage) -> {
                    if (wasIngredientLoadSuccessful)
                    {
                        CarbCyclingMealBasicInfoScreenSection<ReadOnlyCarbCyclingMealModelProxy> basicInfoSection = new CarbCyclingMealBasicInfoScreenSection<>(carbCyclingMealProxy);
                        macrosSection = new MacronutrientBreakdownScreenSection<>(carbCyclingMealProxy);
                        microsSection = new MicronutrientBreakdownScreenSection<>(carbCyclingMealProxy);

                        rootContainer.getChildren().add(basicInfoSection.create());
                        rootContainer.getChildren().add(macrosSection.create());
                        rootContainer.getChildren().add(microsSection.create());
                    }
                    else
                    {
                        notificationBuilder.create(ingredientLoadResultMessage, hostingStageName).show();
                    }
                    loadEventBuilder.emitCompleted(hostingStageName);
                });
            }
            else
            {
                loadEventBuilder.emitCompleted(hostingStageName);
                notificationBuilder.create("An error has occurred while loading the meal", hostingStageName).show();
            }
        });
    }

    @Override
    public void destroy()
    {
        macrosSection.destroy();
        microsSection.destroy();
    }
}
