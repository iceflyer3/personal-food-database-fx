package com.iceflyer3.pfd.ui.controller.recipes;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.google.common.collect.MoreCollectors;
import com.google.common.eventbus.EventBus;
import com.iceflyer3.pfd.constants.ApplicationSections;
import com.iceflyer3.pfd.constants.CssConstants;
import com.iceflyer3.pfd.constants.FontAwesomeIcons;
import com.iceflyer3.pfd.constants.SceneSets;
import com.iceflyer3.pfd.ui.controller.sections.*;
import com.iceflyer3.pfd.ui.event.PublishBreadcrumbEvent;
import com.iceflyer3.pfd.ui.cell.RecipeStepListCell;
import com.iceflyer3.pfd.ui.controller.AbstractTaskController;
import com.iceflyer3.pfd.ui.controller.HostStageAware;
import com.iceflyer3.pfd.ui.controller.Savable;
import com.iceflyer3.pfd.ui.enums.PfdActionType;
import com.iceflyer3.pfd.ui.listener.Destroyable;
import com.iceflyer3.pfd.ui.listener.ObservableListListenerBinding;
import com.iceflyer3.pfd.ui.listener.ObservableValueListenerBinding;
import com.iceflyer3.pfd.ui.manager.DialogCallback;
import com.iceflyer3.pfd.ui.manager.OpenDialogRequest;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.api.ServingSizeModel;
import com.iceflyer3.pfd.ui.model.api.recipe.RecipeStepModel;
import com.iceflyer3.pfd.ui.model.proxy.food.FoodProxyFactory;
import com.iceflyer3.pfd.ui.model.proxy.food.ReadOnlyFoodModelProxy;
import com.iceflyer3.pfd.ui.model.proxy.recipe.RecipeModelProxy;
import com.iceflyer3.pfd.ui.model.proxy.recipe.RecipeProxyFactory;
import com.iceflyer3.pfd.ui.model.viewmodel.IngredientViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.recipe.RecipeStepViewModel;
import com.iceflyer3.pfd.ui.util.BindingUtils;
import com.iceflyer3.pfd.ui.util.LayoutUtils;
import com.iceflyer3.pfd.ui.util.ScreenLoadEventUtils;
import com.iceflyer3.pfd.ui.util.TextUtils;
import com.iceflyer3.pfd.ui.util.builder.ActionNodeBuilder;
import com.iceflyer3.pfd.ui.util.builder.NotificationBuilder;
import com.iceflyer3.pfd.ui.listener.ModelPropertyValidationEventConfig;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.util.converter.BigDecimalStringConverter;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ComposeRecipeController extends AbstractTaskController implements HostStageAware, Savable, Destroyable
{
    public static final String DATA_RECIPE_ID = "recipeIdToEdit";

    private final RecipeProxyFactory recipeProxyFactory;
    private final FoodProxyFactory foodProxyFactory;
    private final WindowManager windowManager;
    private final Set<ObservableValueListenerBinding<?>> listenerBindings;
    private final Set<ObservableListListenerBinding<?>> listListenerBindings;

    private RecipeModelProxy recipeProxy;
    private Button saveButton;
    private ListView<RecipeStepModel> stepsListView;
    private String hostingStageName;

    @FXML
    public ScrollPane rootContainer;

    @FXML
    public Label screenLabel;

    @FXML
    public AnchorPane buttonContainerAnchorPane;

    @FXML
    public VBox stepsContainer;

    @FXML
    public VBox centerScreenContainer;

    @FXML
    public TextField recipeNameInput;

    @FXML
    public TextField recipeSourceInput;

    @FXML
    public TextField numberOfServingsMadeInput;


    public ComposeRecipeController(
            EventBus eventBus,
            NotificationBuilder notificationBuilder,
            ScreenLoadEventUtils loadEventBuilder,
            TaskExecutor taskExecutor,
            WindowManager windowManager,
            FoodProxyFactory foodProxyFactory,
            RecipeProxyFactory recipeProxyFactory) {
        super(eventBus, notificationBuilder, loadEventBuilder, taskExecutor);
        this.recipeProxyFactory = recipeProxyFactory;
        this.foodProxyFactory = foodProxyFactory;
        this.windowManager = windowManager;
        listenerBindings = new HashSet<>();
        listListenerBindings = new HashSet<>();
    }

    @Override
    public void initialize() {
        VBox.setMargin(screenLabel, LayoutUtils.getHeaderLabelMarginInsets());

        numberOfServingsMadeInput.setTextFormatter(TextUtils.getRequiredDecimalTextInputFormatter());

        saveButton = ActionNodeBuilder
                .createIconButton("SAVE", FontAwesomeIcons.SAVE)
                .withAction(this::save)
                .build();
        saveButton.setDisable(true);
        AnchorPane.setRightAnchor(saveButton, 0.0);
        buttonContainerAnchorPane.getChildren().add(saveButton);
    }

    @Override
    public void initControllerData(Map<String, Object> controllerData) {
        UserViewModel user = (UserViewModel) controllerData.get(UserViewModel.class.getName());

        // Are we editing an existing recipe?
        if (controllerData.containsKey(DATA_RECIPE_ID))
        {
            UUID recipeId = (UUID) controllerData.get(DATA_RECIPE_ID);
            // TODO: Constant chaining of callbacks like this is rather gross when you run into the need for several.
            //       Something like RX's forkJoin would be nice here. Not sure how RxJava would play with Tasks though.
            //       But some food for thought.

            /* TODO: Also, constant spamming of the hostingStageName like this is quickly growing ugly as more things
             *       begin to become stage specific / dependent. Not sure how feasible I'd like to find a better way
             *       to deal with this. For now though this will suffice. Another investigation for another day.
             *       This comment really applies to all uses of ScreenLoadEventUtils but this section probably
             *       demonstrates it the best.
             */
            loadEventBuilder.emitBegin("Searching for recipe details", hostingStageName);
            recipeProxyFactory.getMutableRecipe(recipeId, user, (wasSuccessful, proxyResult) -> {
                if (wasSuccessful)
                {
                    // Load ingredients
                    proxyResult.loadIngredients((ingredientLoadSuccessful, ingredientLoadResultMessage) -> {
                        if (ingredientLoadSuccessful)
                        {
                            // Load steps
                            proxyResult.loadSteps((stepLoadSuccessful, stepLoadResultMessage) -> {
                                if (stepLoadSuccessful)
                                {
                                    // Load the list of all foods so we have a list of available ingredients
                                    foodProxyFactory.getAllFoods((foodLoadSuccessful, allFoods) -> {
                                        if (foodLoadSuccessful)
                                        {
                                            processDataLoad(proxyResult, allFoods);
                                        }
                                        else
                                        {
                                            notificationBuilder.create("An error has occurred while loading the recipe data", hostingStageName).show();
                                        }
                                        loadEventBuilder.emitCompleted(hostingStageName);
                                    });
                                }
                                else
                                {
                                    notificationBuilder.create(stepLoadResultMessage, hostingStageName).show();
                                    loadEventBuilder.emitCompleted(hostingStageName);
                                }
                            });
                        }
                        else
                        {
                            notificationBuilder.create(ingredientLoadResultMessage, hostingStageName).show();
                            loadEventBuilder.emitCompleted(hostingStageName);
                        }
                    });
                }
                else
                {
                    notificationBuilder.create("An error has occurred while loading the recipe data", hostingStageName).show();
                    loadEventBuilder.emitCompleted(hostingStageName);
                }
            });
        }
        else
        {
            recipeProxyFactory.getMutableRecipe(null, user, (wasSuccessful, proxyResult) -> {
                if (wasSuccessful)
                {
                    foodProxyFactory.getAllFoods((foodLoadSuccessful, allFoods) -> {
                        if (foodLoadSuccessful)
                        {
                            processDataLoad(proxyResult, allFoods);
                        }
                        else
                        {
                            notificationBuilder.create("An error has occurred while loading ingredient food data", hostingStageName).show();
                        }
                    });
                }
                else
                {
                    // Theoretically this should never actually happen
                    windowManager.openFatalErrorDialog();
                }
            });
        }
    }

    @Override
    public void initHostingStageName(String stageName) {
        hostingStageName = stageName;

        PublishBreadcrumbEvent landingScreenBreadcrumb = new PublishBreadcrumbEvent("Compose Recipe", ApplicationSections.RECIPES, SceneSets.COMPOSE_RECIPE, hostingStageName, 2);
        eventBus.post(landingScreenBreadcrumb);
    }

    @Override
    public void updateSaveValidity() {
        saveButton.setDisable(!recipeProxy.validate().wasSuccessful());
    }

    @Override
    public void save(ActionEvent event) {
        recipeProxy.isUnique((wasSuccessful, isUnique) -> {
            if (wasSuccessful)
            {
                if (isUnique)
                {
                    notificationBuilder.create("Saving recipe...", hostingStageName).show();
                    recipeProxy.save((wasSaveSuccessful, resultMessage) -> notificationBuilder.create(resultMessage, hostingStageName).show());
                }
                else
                {
                    notificationBuilder.create("A recipe with the same name and nutritional profile already exists", hostingStageName).show();
                }
            }
            else
            {
                notificationBuilder.create("Failed to save the recipe. An error has occurred while verifying the uniqueness of the recipe.", hostingStageName).show();
            }
        });
    }

    @Override
    protected void registerDefaultListeners() {
         saveButton.setOnAction(this::save);
    }

    @Override
    public void destroy()
    {
        listenerBindings.forEach(ObservableValueListenerBinding::unbind);
        listListenerBindings.forEach(ObservableListListenerBinding::unbind);
        listenerBindings.clear();
        listListenerBindings.clear();
    }

    private void processDataLoad(RecipeModelProxy loadedRecipe, List<ReadOnlyFoodModelProxy> allFoodsList)
    {
        recipeProxy = loadedRecipe;
        updateSaveValidity();

        // Setup bindings
        Bindings.bindBidirectional(recipeNameInput.textProperty(), recipeProxy.nameProperty());
        Bindings.bindBidirectional(recipeSourceInput.textProperty(), recipeProxy.sourceProperty());
        Bindings.bindBidirectional(numberOfServingsMadeInput.textProperty(), recipeProxy.numberOfServingsMadeProperty(), new BigDecimalStringConverter());

        // Setup save validity refreshing
        listenerBindings.add(BindingUtils.bindModelObservableValueToGuiControl(new ModelPropertyValidationEventConfig<>(recipeProxy, recipeProxy.nameProperty(), recipeNameInput, this::updateSaveValidity)));
        listenerBindings.add(BindingUtils.bindModelObservableValueToGuiControl(new ModelPropertyValidationEventConfig<>(recipeProxy, recipeProxy.sourceProperty(), recipeSourceInput, this::updateSaveValidity)));
        listenerBindings.add(BindingUtils.bindModelObservableValueToGuiControl(new ModelPropertyValidationEventConfig<>(recipeProxy, recipeProxy.numberOfServingsMadeProperty(), numberOfServingsMadeInput, this::updateSaveValidity)));

        ObservableListListenerBinding<RecipeStepModel> recipeStepListListenerBinding = new ObservableListListenerBinding<>(recipeProxy.getSteps());
        recipeStepListListenerBinding.bindChangeListener(change -> updateSaveValidity());
        listListenerBindings.add(recipeStepListListenerBinding);

        ObservableListListenerBinding<ServingSizeModel> servingSizeListListenerBinding = new ObservableListListenerBinding<>(recipeProxy.getServingSizes());
        servingSizeListListenerBinding.bindChangeListener(change -> updateSaveValidity());
        listListenerBindings.add(servingSizeListListenerBinding);

        ObservableListListenerBinding<IngredientModel> ingredientListListenerBinding = new ObservableListListenerBinding<>(recipeProxy.getIngredients());
        ingredientListListenerBinding.bindChangeListener(change -> updateSaveValidity());
        listListenerBindings.add(ingredientListListenerBinding);

        // Setup serving sizes, ingredients, and macro / micro breakdown sections
        ScreenSection servingSizeSection = new EditServingSizeScreenSection<>(recipeProxy);
        ScreenSection ingredientsSection = new EditIngredientScreenSection<>(allFoodsList, recipeProxy);
        ScreenSection macroBreakdownSection = new MacronutrientBreakdownScreenSection<>(recipeProxy);
        ScreenSection microBreakdownSection = new MicronutrientBreakdownScreenSection<>(recipeProxy);

        // These two go above the "Steps" section
        centerScreenContainer.getChildren().add(1, servingSizeSection.create());
        centerScreenContainer.getChildren().add(2, ingredientsSection.create());

        // These two go below the "Steps" section
        centerScreenContainer.getChildren().addAll(macroBreakdownSection.create(), microBreakdownSection.create());

        // Setup the recipe steps section
        initRecipeStepsSection();
    }

    private void initRecipeStepsSection()
    {
        // Add Step button
        Button addStepButton = ActionNodeBuilder.createIconButton("ADD STEP", FontAwesomeIcons.PLUS).forActionType(PfdActionType.SECONDARY).withAction(event -> {
            openStepCreationDialog(null);
        }).build();
        AnchorPane stepsButtonAnchorPane = new AnchorPane();
        AnchorPane.setRightAnchor(addStepButton, 5.0);
        AnchorPane.setTopAnchor(addStepButton, 10.0);
        stepsButtonAnchorPane.getChildren().add(addStepButton);
        stepsContainer.getChildren().add(stepsButtonAnchorPane);

        // List of existing steps
        stepsListView = new ListView<>(recipeProxy.getSteps());
        stepsListView.getStyleClass().add(CssConstants.CLASS_BORDER_WHITE);
        stepsListView.setMinWidth(400);
        stepsListView.setMaxWidth(Double.MAX_VALUE);
        stepsListView.setPlaceholder(new Label("No steps have been added"));
        stepsListView.setCellFactory(col -> new RecipeStepListCell(this::getButtonsForRecipeStepRow));

        stepsContainer.getChildren().add(stepsListView);
    }

    /**
     * Generates the buttons for a single step in the recipe steps list view
     * @param stepForRow The RecipeStepViewModel instance for the current row of the ListView
     * @return The list of buttons to be show in the header section of the row
     */
    private List<Button> getButtonsForRecipeStepRow(RecipeStepModel stepForRow)
    {
        List<Button> buttonsForCell = new ArrayList<>();

        Button editButton = ActionNodeBuilder.createIconButton(null, FontAwesomeIcons.PENCIL, FontAwesomeIcons.DEFAULT_SIZE_TABLE_ROW_ICON)
                .forActionType(PfdActionType.TERTIARY)
                .withAction(event -> {
                    RecipeStepViewModel stepToEdit =  (RecipeStepViewModel) ((Node)event.getSource()).getUserData();
                    openStepCreationDialog(stepToEdit);
                })
                .build();

        Button deleteButton = ActionNodeBuilder.createIconButton(null, FontAwesomeIcons.TIMES, FontAwesomeIcons.DEFAULT_SIZE_TABLE_ROW_ICON)
                .forActionType(PfdActionType.DANGER)
                .withAction(event -> {
                    // Decrement the step number of each step after the removed step
                    List<RecipeStepModel> stepsToUpdate = recipeProxy.getSteps().stream().filter(step -> step.getStepNumber() > stepForRow.getStepNumber()).collect(Collectors.toList());
                    stepsToUpdate.forEach(step -> step.setStepNumber(step.getStepNumber() -1));

                    // Remove the step from the list of steps for this recipe
                    recipeProxy.getSteps().remove(stepForRow);
                })
                .build();

        buttonsForCell.add(editButton);
        buttonsForCell.add(deleteButton);
        return buttonsForCell;
    }

    /**
     * Find the list of all ingredients that belong to the recipe but have not
     * yet been assigned to any step.
     *
     * @return The list of candidate ingredients for this step.
     */
    private List<IngredientModel> getCandidateIngredientsForNewStep()
    {
        // First, gather the list of all ingredients that belong to any step
        List<IngredientModel> allStepIngredients = new ArrayList<>();
        for(RecipeStepModel step : recipeProxy.getSteps())
        {
            allStepIngredients.addAll(step.getIngredients());
        }

        // Next find ingredients that belong to the recipe but do not belong to a step (do not many any in the list above)
        List<IngredientModel> candidateIngredients = new ArrayList<>();
        for(IngredientModel recipeIngredient : recipeProxy.getIngredients())
        {
            if (allStepIngredients.stream().noneMatch(existingStepIngredient -> existingStepIngredient.equals(recipeIngredient)))
            {
                candidateIngredients.add(recipeIngredient);
            }
        }

        return candidateIngredients;
    }

    private void openStepCreationDialog(RecipeStepViewModel stepToEdit)
    {
        // Open the new step dialog
        DialogCallback<RecipeStepViewModel> callback = newStep -> {
            // If a step with the same step number as that returned by the dialog already exists in the list of all steps for the recipe
            Optional<RecipeStepModel> existingStep = recipeProxy.getSteps()
                                                                        .stream()
                                                                        .filter(ingredientStep -> ingredientStep.getStepNumber() == newStep.getStepNumber())
                                                                        .collect(MoreCollectors.toOptional());
            if (existingStep.isPresent())
            {
                stepsListView.refresh();
            }
            else
            {
                recipeProxy.getSteps().add(newStep);
            }
        };
        OpenDialogRequest<RecipeStepViewModel> newStepDialogRequest = new OpenDialogRequest<>(rootContainer.getScene().getWindow(), SceneSets.DIALOG_CREATE_RECIPE_STEP, callback);
        newStepDialogRequest.setDialogTitle("Add Recipe Step");

        // Find the list of ingredients that belong to the recipe but not any  other steps
        List<IngredientModel> candidateIngredients = getCandidateIngredientsForNewStep();
        newStepDialogRequest.getDialogData().put(IngredientViewModel.class.getName(), candidateIngredients);

        // If we aren't editing a step then we're creating a new one
        if (stepToEdit == null)
        {
            newStepDialogRequest.getDialogData().put(RecipeStepViewModel.class.getName(), new RecipeStepViewModel(recipeProxy.getSteps().size() + 1));
        }
        else
        {
            newStepDialogRequest.getDialogData().put(RecipeStepViewModel.class.getName(), stepToEdit);
        }

        windowManager.openDialog(newStepDialogRequest);
    }
}
