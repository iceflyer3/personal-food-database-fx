package com.iceflyer3.pfd.ui.cell;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import javafx.scene.control.Button;

import java.util.List;

/**
 * A functional interface that facilitates creation of action buttons for ActionButtonTableCells.
 *
 * @param <T> The type of the data that the tableview this cell belongs to operates on.
 *            This is the same as the first generic type of the TableView.
 */
@FunctionalInterface
public interface ActionButtonProvider<T> {

    /**
     * Function called by the ActionButtonTableCell to get the buttons that should be used by the cell.
     *
     * Receives a handle to the data for the row in which the button will appear in the event this is needed to make
     * decisions regarding the returned button.
     *
     * Additionally, each button registered in this way will itself receive a handle to the data backing the row of the
     * table in which the button appears as "user data" on the button. This is then available to click handlers and the
     * like via the button's getUserData function.
     *
     * @param rowData The data for the row in which the associated ActionButtonTableCell will render
     * @return A list of Buttons that will be included in the ActionButtonTableCell that this provider is for
     */
    List<Button> get(T rowData);
}
