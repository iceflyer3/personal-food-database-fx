package com.iceflyer3.pfd.ui.widget.builder;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023, 2025 Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.ui.util.ControllerUtils;
import org.springframework.stereotype.Component;

/**
 * Builder for widget nodes. These are typically more involved than the nodes created by the ActionNodeBuilder, usually
 * representing a composition of nodes that may include those produced by ActionNodeBuilder.
 *
 * For example, the Wizard widget represents a collection of entire screens as opposed to producing only a single button
 * or label.
 *
 * Due to this larger scale, this class also requires being a candidate for dependency injection and access to the
 * application context.
 */
@Component
public class WidgetNodeBuilder
{
    private final ControllerUtils controllerUtils;

    public WidgetNodeBuilder(ControllerUtils controllerUtils)
    {
        this.controllerUtils = controllerUtils;
    }

    public <T> ScreenSeriesWidgetBuilder<T> createScreenSeries(Class<T> resultType)
    {
        return new ScreenSeriesWidgetBuilder<>(controllerUtils);
    }
}
