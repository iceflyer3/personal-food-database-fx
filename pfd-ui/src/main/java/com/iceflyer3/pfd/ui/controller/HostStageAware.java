package com.iceflyer3.pfd.ui.controller;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * Interface to be implemented by controllers that need to know the name of the stage in which they
 * are loaded.
 *
 * This applies to most of the controllers in the application as each main application section may be
 * opened in a popout window.
 *
 * This is needed to ensure that events that apply across multiple stages (if there are several open
 * such as the main application window and a couple of popout windows) only apply to the stage that the
 * event was triggered in and not across all open stages.
 *
 * During controller initialization the hosting stage name will be set before any controller data is
 * initialized.
 */
public interface HostStageAware {
    void initHostingStageName(String stageName);
}
