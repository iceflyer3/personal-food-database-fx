package com.iceflyer3.pfd.ui.controller.settings;

/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.google.common.eventbus.EventBus;
import com.iceflyer3.pfd.constants.CssConstants;
import com.iceflyer3.pfd.constants.FontAwesomeIcons;
import com.iceflyer3.pfd.data.factory.ImportExportDataTaskFactory;
import com.iceflyer3.pfd.data.request.backup.ExportDataRequest;
import com.iceflyer3.pfd.data.task.data.ExportDataTask;
import com.iceflyer3.pfd.ui.controller.AbstractTaskController;
import com.iceflyer3.pfd.ui.controller.HostStageAware;
import com.iceflyer3.pfd.ui.enums.PfdActionType;
import com.iceflyer3.pfd.ui.listener.Destroyable;
import com.iceflyer3.pfd.ui.listener.ObservableValueListenerBinding;
import com.iceflyer3.pfd.ui.util.ScreenLoadEventUtils;
import com.iceflyer3.pfd.ui.util.builder.ActionNodeBuilder;
import com.iceflyer3.pfd.ui.util.builder.NotificationBuilder;
import javafx.beans.value.ChangeListener;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ExportDataTabController extends AbstractTaskController implements HostStageAware, Destroyable
{
    private final ImportExportDataTaskFactory importExportDataTaskFactory;
    private final List<ObservableValueListenerBinding<?>> listenerBindings;

    private String hostingStageName;

    @FXML
    public VBox dataExportTabContainer;

    @FXML
    public VBox dataExportTaskStatusContainer;

    @FXML
    public Label dataExportTaskStatusLabel;

    public ExportDataTabController(
            EventBus eventBus,
            NotificationBuilder notificationBuilder,
            ScreenLoadEventUtils loadEventBuilder,
            TaskExecutor taskExecutor,
            ImportExportDataTaskFactory importExportDataTaskFactory)
    {
        super(eventBus, notificationBuilder, loadEventBuilder, taskExecutor);
        this.importExportDataTaskFactory = importExportDataTaskFactory;
        this.listenerBindings = new ArrayList<>();
    }

    @Override
    public void initialize()
    {
        dataExportTabContainer.managedProperty().bind(dataExportTabContainer.visibleProperty());
        dataExportTaskStatusContainer.managedProperty().bind(dataExportTaskStatusContainer.visibleProperty());
    }

    @Override
    protected void registerDefaultListeners()
    {

    }

    @Override
    public void initHostingStageName(String stageName)
    {
        hostingStageName = stageName;
        createTabUI();
    }

    @Override
    public void destroy()
    {
        listenerBindings.forEach(ObservableValueListenerBinding::unbind);
    }

    private void createTabUI()
    {
        ExportDataRequest exportDataRequest = new ExportDataRequest();

        // Create the labels and checkboxes and establish bindings to the request object
        Label headerLabel = new Label("Select data to export");
        headerLabel.getStyleClass().addAll(CssConstants.CLASS_BOLD, CssConstants.CLASS_UNDERLINE);

        CheckBox foodCheckbox = new CheckBox("Food");
        exportDataRequest.includeFoodsProperty().bind(foodCheckbox.selectedProperty());

        CheckBox mealPlanCheckbox = new CheckBox("Meal Plans");
        exportDataRequest.includeMealPlansProperty().bind(mealPlanCheckbox.selectedProperty());


        CheckBox recipeCheckBox = new CheckBox("Recipes");
        exportDataRequest.includeRecipesProperty().bind(recipeCheckBox.selectedProperty());

        /*
         * Meals Plans and Recipes are built on top of food data. You can't meaningfully export
         * meal planning or recipe data without also exporting the food data that it relies upon.
         */
        ChangeListener<Boolean> mealPlanAndRecipeCheckboxChangeListener = (observable, oldValue, newValue) -> {
            if (mealPlanCheckbox.isSelected() || recipeCheckBox.isSelected())
            {
                foodCheckbox.setDisable(true);
                foodCheckbox.setSelected(true);
            }
            else
            {
                foodCheckbox.setDisable(false);
            }
        };

        ObservableValueListenerBinding<Boolean> mealPlansDataCheckboxListenerBinding = new ObservableValueListenerBinding<>(mealPlanCheckbox.selectedProperty());
        mealPlansDataCheckboxListenerBinding.bindChangeListener(mealPlanAndRecipeCheckboxChangeListener);
        listenerBindings.add(mealPlansDataCheckboxListenerBinding);

        ObservableValueListenerBinding<Boolean> recipeDataCheckboxListener = new ObservableValueListenerBinding<>(recipeCheckBox.selectedProperty());
        recipeDataCheckboxListener.bindChangeListener(mealPlanAndRecipeCheckboxChangeListener);
        listenerBindings.add(recipeDataCheckboxListener);

        // Creation and initial setup of the gridpane responsible for displaying all the import / export options
        GridPane dataExportOptionsGridPane = new GridPane();
        dataExportOptionsGridPane.setHgap(20);
        dataExportOptionsGridPane.setVgap(10);

        dataExportOptionsGridPane.add(headerLabel, 0, 1);
        dataExportOptionsGridPane.add(foodCheckbox, 0, 2);
        dataExportOptionsGridPane.add(mealPlanCheckbox, 1, 2);
        dataExportOptionsGridPane.add(recipeCheckBox, 2, 2);

        Button exportButton = ActionNodeBuilder
                .createIconButton("Export", FontAwesomeIcons.UPLOAD)
                .forActionType(PfdActionType.PRIMARY)
                .withAction(actionEvent -> {

                    if (exportDataRequest.hasExportSelections())
                    {
                        ExportDataTask exportDataTask = importExportDataTaskFactory.exportData(exportDataRequest);
                        dataExportTaskStatusLabel.textProperty().bind(exportDataTask.messageProperty());

                        exportDataTask.setOnRunning(workerStateEvent -> {
                            toggleDataExportStatusDisplay(true);
                        });
                        exportDataTask.setOnSucceeded(workerStateEvent -> {
                            toggleDataExportStatusDisplay(false);
                            notificationBuilder.create("The data has been successfully exported!", hostingStageName).show();
                        });
                        exportDataTask.setOnFailed(workerStateEvent -> {
                            toggleDataExportStatusDisplay(false);
                            notificationBuilder.create("An error has occurred while attempting to export the data", hostingStageName).show();
                        });
                        taskExecutor.execute(exportDataTask);
                    }
                    else
                    {
                        notificationBuilder.create("You must first select the data you wish to export", hostingStageName);
                    }
                })
                .build();
        dataExportOptionsGridPane.add(exportButton, 0, 0);

        dataExportTabContainer.getChildren().add(dataExportOptionsGridPane);
    }

    private void toggleDataExportStatusDisplay(boolean isVisible)
    {
        dataExportTaskStatusContainer.setVisible(isVisible);
        dataExportTabContainer.setVisible(!isVisible);
    }
}
