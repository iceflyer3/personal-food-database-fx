package com.iceflyer3.pfd.ui.controller;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import javafx.event.ActionEvent;

/**
 * Interface that is to be implemented by controllers that are capable
 * of saving data.
 *
 * Used to determine if the state of the data the controller manages is
 * valid for saving as well as for invoking the save operation on that
 * data (presumably via a save button)
 */
public interface Savable {

    /**
     * Used to check the state of the data that the controller manages and update
     * the disabled status of the save button accordingly.
     */
    void updateSaveValidity();

    /**
     * Invokes the save operation on the data the controller manages.
     */
    void save(ActionEvent event);
}
