package com.iceflyer3.pfd.ui.controller.dialog;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.ui.controller.AbstractController;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import org.springframework.stereotype.Component;

@Component
public class FatalErrorDialogController extends AbstractController
{
    private final WindowManager windowManager;

    @FXML
    public Button exitButton;

    public FatalErrorDialogController(WindowManager windowManager)
    {
        this.windowManager = windowManager;
    }

    @Override
    public void initialize()
    {

    }

    @Override
    protected void registerDefaultListeners()
    {
        exitButton.setOnAction(event -> {
            Platform.exit();
        });
    }
}
