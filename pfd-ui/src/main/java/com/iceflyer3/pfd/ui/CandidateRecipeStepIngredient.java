package com.iceflyer3.pfd.ui;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import javafx.beans.property.*;
import org.springframework.lang.NonNull;

/**
 * Represents a candidate ingredient choice option that is
 * shown on the Create Recipe Step Dialog.
 */
public class CandidateRecipeStepIngredient implements Comparable<CandidateRecipeStepIngredient> {

    private final IngredientModel candidateIngredient;
    private final BooleanProperty isSelected;
    private final StringProperty ingredientName;

    public CandidateRecipeStepIngredient(IngredientModel candidateIngredient) {
        this(candidateIngredient, false);
    }

    public CandidateRecipeStepIngredient(IngredientModel candidateIngredient, boolean isSelected) {
        this.candidateIngredient = candidateIngredient;
        this.isSelected = new SimpleBooleanProperty(isSelected);
        ingredientName = new SimpleStringProperty(candidateIngredient.getIngredientFood().getName());
    }

    public IngredientModel getCandidateIngredient() {
        return candidateIngredient;
    }

    public String getIngredientName() {
        return ingredientName.get();
    }

    public StringProperty ingredientNameProperty() {
        return ingredientName;
    }

    public boolean isSelected() {
        return isSelected.get();
    }

    public BooleanProperty isSelectedProperty() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected.set(isSelected);
    }

    @Override
    public int compareTo(@NonNull CandidateRecipeStepIngredient o) {
        return getIngredientName().compareTo(o.getIngredientName());
    }
}
