package com.iceflyer3.pfd.ui.filter;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.ui.enums.FoodFilterProperty;
import com.iceflyer3.pfd.validation.ComparisonOperator;
import javafx.beans.property.*;

/**
 * A food filter to be used with text properties of a food such as
 * food name or brand name.
 */
public class TextFoodFilter implements Filter<String> {

    private final BooleanProperty active;
    private final StringProperty searchValue;
    private final ObjectProperty<ComparisonOperator> operator;
    private final ObjectProperty<FoodFilterProperty> foodProperty;

    public TextFoodFilter(FoodFilterProperty property, ComparisonOperator operator) {
        this.foodProperty = new SimpleObjectProperty<>(property);
        this.operator = new SimpleObjectProperty<>(operator);
        this.active = new SimpleBooleanProperty(true);
        this.searchValue = new SimpleStringProperty();
    }

    public ComparisonOperator getOperator() {
        return operator.get();
    }

    public ObjectProperty<ComparisonOperator> operatorProperty() {
        return operator;
    }

    public FoodFilterProperty getFoodProperty() {
        return foodProperty.get();
    }

    public ObjectProperty<FoodFilterProperty> foodPropertyProperty() {
        return foodProperty;
    }

    public String getSearchValue() {
        return searchValue.get();
    }

    public StringProperty searchValueProperty() {
        return searchValue;
    }

    public void setSearchValue(String searchValue) {
        this.searchValue.set(searchValue);
    }

    public boolean isActive() {
        return active.get();
    }

    public BooleanProperty activeProperty() {
        return active;
    }

    public void toggleActiveStatus() {
        active.set(!active.get());
    }

    @Override
    public boolean apply(String sourceValue) {
        /*
         * This already has all of the information it needs to apply the filter.
         * We know the value we want to filter against (stored in comparisonValue), we know the
         * operation we wish to perform (stored in operator) and we know if this filter is active or not.
         *
         * What we need to do here is switch on "operator" and then perform the comparison against "comparisonValue" as
         * define by "operator"
         */
        if (active.get())
        {
            switch(operator.get())
            {
                case LIKE:
                    return sourceValue.toLowerCase().contains(searchValue.get().toLowerCase());
                case NOT_LIKE:
                    return !sourceValue.toLowerCase().contains(searchValue.get().toLowerCase());
                default:
                    throw new UnsupportedOperationException(String.format("Operation %s is not supported with text filters", operator.get()));
            }
        }
        else
        {
            // If the filter is not active then it shouldn't apply so it automatically passes
            return true;
        }
    }

    @Override
    public String toString() {
        return "TextFoodFilter{" +
                "active=" + active +
                ", searchValue=" + searchValue +
                ", operator=" + operator +
                ", foodProperty=" + foodProperty +
                '}';
    }
}
