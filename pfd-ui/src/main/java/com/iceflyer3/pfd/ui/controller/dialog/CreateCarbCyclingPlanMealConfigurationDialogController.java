package com.iceflyer3.pfd.ui.controller.dialog;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.PfdApplication;
import com.iceflyer3.pfd.constants.SceneSets;
import com.iceflyer3.pfd.enums.CarbCyclingMealRelation;
import com.iceflyer3.pfd.exception.InvalidApplicationStateException;
import com.iceflyer3.pfd.ui.controller.Savable;
import com.iceflyer3.pfd.ui.listener.Destroyable;
import com.iceflyer3.pfd.ui.listener.ObservableValueListenerBinding;
import com.iceflyer3.pfd.ui.manager.OpenDialogRequest;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import com.iceflyer3.pfd.ui.model.api.mealplanning.carbcycling.CarbCyclingMealConfigurationModel;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.carbcycling.CarbCyclingPlanModelProxy;
import com.iceflyer3.pfd.ui.util.BindingUtils;
import com.iceflyer3.pfd.ui.util.TextUtils;
import com.iceflyer3.pfd.ui.listener.ModelPropertyValidationEventConfig;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.util.converter.BigDecimalStringConverter;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CreateCarbCyclingPlanMealConfigurationDialogController extends AbstractDialogController<CarbCyclingPlanModelProxy> implements Savable, Destroyable
{
    public static final String DATA_CARB_CYCLING_PLAN = "carbCyclingPlan";

    private static final Logger LOG = LoggerFactory.getLogger(CreateCarbCyclingPlanMealConfigurationDialogController.class);

    private final Set<ObservableValueListenerBinding<?>> listenerBindings;


    private CarbCyclingPlanModelProxy carbCyclingPlan;

    @FXML
    public Label descriptionLabel;

    @FXML
    public TextField postWorkoutConsumptionMinPercentInput;

    @FXML
    public TextField postWorkoutConsumptionMaxPercentInput;

    @FXML
    public GridPane mealsBeforePostWorkoutGrid;

    @FXML
    public GridPane mealsAfterPostWorkoutGrid;

    @FXML
    public GridPane nonTrainingDayMealsGrid;

    @FXML
    public Button saveButton;

    @FXML
    public Hyperlink cancelButton;

    public CreateCarbCyclingPlanMealConfigurationDialogController(WindowManager windowManager)
    {
        super(windowManager);
        listenerBindings = new HashSet<>();
    }

    @Override
    public void initialize()
    {
        descriptionLabel.setText("""
                Using this screen you may configure the carbohydrate consumption percentage restrictions to be applied later when creating meals for this carb cycling plan. The valid range for all meals is 0% to 99% but you must have at least one meal with a non-zero minimum percentage. The minimum values must be less than the maximum values.
                
                After plan creation these values cannot be changed.
                
                Use the "Confirm" button to accept changes. Use the "Clear" button to clear out / remove all meal configurations.""");
    }

    @Override
    public void initControllerData(Map<String, Object> controllerData)
    {
        carbCyclingPlan = (CarbCyclingPlanModelProxy) controllerData.get(DATA_CARB_CYCLING_PLAN);

        /*
         * It is possible for the dialog to already have configured meals if the dialog was
         * previously opened, confirmed (and closed), then re-opened for further tweaks.
         */
        if (!carbCyclingPlan.hasConfiguredMeals())
        {
            carbCyclingPlan.initMealConfigurations();
        }

        // Setup bindings for the post-workout meal
        Optional<CarbCyclingMealConfigurationModel> optionalPostWorkoutMealConfig = carbCyclingPlan
                .getMealConfigurations()
                .stream()
                .filter(config ->
                        config.getPostWorkoutMealOffset() == 0
                                && config.getPostWorkoutMealRelation() == CarbCyclingMealRelation.NONE
                                && config.isForTrainingDay()
                )
                .findFirst();

        if (optionalPostWorkoutMealConfig.isPresent())
        {
            CarbCyclingMealConfigurationModel postWorkoutMealConfig = optionalPostWorkoutMealConfig.get();
            setupBindingsForMinMaxCarbPercentageFields(postWorkoutMealConfig, postWorkoutConsumptionMinPercentInput, postWorkoutConsumptionMaxPercentInput);
        }
        else
        {
            // We expect there should always be a post-workout meal as it is what other meals are based around.
            // If there isn't the application should yell loudly about it because something has gone quite wrong.
            LOG.error("An error has occurred during carb cycling plan creation", new InvalidApplicationStateException("Could not find carb cycling meal configuration for post-workout meal for the new carb cycling plan"));
            windowManager.openFatalErrorDialog();
        }

        // Setup the meal grids
        setupNonTrainingDayMealGrid();
        setupTrainingDayMealGrid(CarbCyclingMealRelation.BEFORE);
        setupTrainingDayMealGrid(CarbCyclingMealRelation.AFTER);

        // Set initial invalid state
        updateSaveValidity();
    }

    @Override
    protected void registerDefaultListeners()
    {
        saveButton.setOnAction(this::save);
        cancelButton.setOnAction(this::cancel);
    }

    /**
     * Populates the non-training day meal grid with the appropriate number of inputs as defined by
     * the number of meals per day
     */
    private void setupNonTrainingDayMealGrid()
    {
        Set<CarbCyclingMealConfigurationModel> nonTrainingDayMealConfigurations = carbCyclingPlan
                .getMealConfigurations()
                .stream()
                .filter(config -> !config.isForTrainingDay())
                .collect(Collectors.toSet());

        for (int i = 1; i <= carbCyclingPlan.getMealsPerDay(); i++)
        {
            // Form setup
            Label mealLabel = new Label(String.format("Meal %s carb consumption percentage:", i));
            Label toLabel = new Label("to");

            TextField minPercentageInputTextField = new TextField();
            TextField maxPercentageInputTextField = new TextField();
            minPercentageInputTextField.setTextFormatter(TextUtils.getOptionalDecimalTextInputFormatter());
            maxPercentageInputTextField.setTextFormatter(TextUtils.getOptionalDecimalTextInputFormatter());

            // Bind and register the text boxes with the model
            int currentMealOffset = i;
            Optional<CarbCyclingMealConfigurationModel> mealConfigViewModel = nonTrainingDayMealConfigurations
                    .stream()
                    .filter(config -> config.getPostWorkoutMealOffset() == currentMealOffset)
                    .findFirst();

            if (mealConfigViewModel.isPresent())
            {
                CarbCyclingMealConfigurationModel currentMealConfig = mealConfigViewModel.get();
                setupBindingsForMinMaxCarbPercentageFields(currentMealConfig, minPercentageInputTextField, maxPercentageInputTextField);
            }
            else
            {
                /*
                 * We expect there should always be one meal config for the number of meals per day
                 * on non-training days.
                 */
                LOG.error("An error has occurred during carb cycling plan creation", new InvalidApplicationStateException(String.format("Could not find carb cycling meal configuration for meal %s on non-training days for the new carb cycling plan", currentMealOffset)));

                // TODO: I'd like to find a better way to handle this error than just making it fatal. But theoretically this should never actually happen anyways so it'll suffice for now.
                windowManager.openFatalErrorDialog();
            }

            // Add the controls to the grid
            // Subtract one because we aligned the loop index to the current meal config which starts at one
            int gridRowIndex = i -1;
            nonTrainingDayMealsGrid.add(mealLabel, 0, gridRowIndex);
            nonTrainingDayMealsGrid.add(minPercentageInputTextField, 1, gridRowIndex);
            nonTrainingDayMealsGrid.add(toLabel, 2, gridRowIndex);
            nonTrainingDayMealsGrid.add(maxPercentageInputTextField, 3, gridRowIndex);
        }
    }

    /**
     * Populates the before and after post-workout meal grids with the appropriate number of inputs
     * as defined by the number of meals per day
     * @param mealRelation Determines if the before or after post-workout grid is being set up
     */
    private void setupTrainingDayMealGrid(CarbCyclingMealRelation mealRelation)
    {
        int numberOfMealsInGrid = carbCyclingPlan.getMealsPerDay() - 1;
        Set<CarbCyclingMealConfigurationModel> trainingDayMealConfigurations = carbCyclingPlan
                .getMealConfigurations()
                .stream()
                .filter(config -> config.isForTrainingDay())
                .collect(Collectors.toSet());

        for(int i = 1; i <= numberOfMealsInGrid; i++)
        {
            // Create the label
            Label mealLabel = new Label(String.format("%s meal(s) %s carb consumption percentage:", i, mealRelation.toString().toLowerCase()));
            Label toLabel = new Label("to");

            // Create the text box
            TextField minPercentageInputTextField = new TextField();
            TextField maxPercentageInputTextField = new TextField();
            minPercentageInputTextField.setTextFormatter(TextUtils.getOptionalDecimalTextInputFormatter());
            maxPercentageInputTextField.setTextFormatter(TextUtils.getOptionalDecimalTextInputFormatter());

            // Bind and register the text boxes with the model
            int currentMealOffset = i;
            Optional<CarbCyclingMealConfigurationModel> mealConfigViewModel = trainingDayMealConfigurations
                    .stream()
                    .filter(config -> config.getPostWorkoutMealOffset() == currentMealOffset && config.getPostWorkoutMealRelation() == mealRelation)
                    .findFirst();

            if (mealConfigViewModel.isPresent())
            {
                CarbCyclingMealConfigurationModel currentMealConfig = mealConfigViewModel.get();
                setupBindingsForMinMaxCarbPercentageFields(currentMealConfig, minPercentageInputTextField, maxPercentageInputTextField);
            }
            else
            {
                /*
                 * We expect there should always be one meal config for the number of meals per day
                 * before and after the post-workout meal. If there isn't something has gone decidedly
                 * pear shaped.
                 */
                LOG.error("An error has occurred during carb cycling plan creation", new InvalidApplicationStateException(String.format("Could not find carb cycling meal configuration for meal %s on training days for the new carb cycling plan", currentMealOffset)));
                windowManager.openFatalErrorDialog();
            }

            // Add everything to the grid
            // Subtract one because we aligned the loop index to the current meal config which starts at one
            int gridRowIndex = i -1;
            if (mealRelation == CarbCyclingMealRelation.BEFORE)
            {
                mealsBeforePostWorkoutGrid.add(mealLabel, 0, gridRowIndex);
                mealsBeforePostWorkoutGrid.add(minPercentageInputTextField, 1, gridRowIndex);
                mealsBeforePostWorkoutGrid.add(toLabel, 2, gridRowIndex);
                mealsBeforePostWorkoutGrid.add(maxPercentageInputTextField, 3, gridRowIndex);
            }
            else if (mealRelation == CarbCyclingMealRelation.AFTER)
            {
                mealsAfterPostWorkoutGrid.add(mealLabel, 0, gridRowIndex);
                mealsAfterPostWorkoutGrid.add(minPercentageInputTextField, 1, gridRowIndex);
                mealsAfterPostWorkoutGrid.add(toLabel, 2, gridRowIndex);
                mealsAfterPostWorkoutGrid.add(maxPercentageInputTextField, 3, gridRowIndex);
            }
        }
    }

    @Override
    public void updateSaveValidity()
    {
        saveButton.setDisable(!carbCyclingPlan.validateMealConfigurations().wasSuccessful());
    }

    @Override
    public void save(ActionEvent event)
    {
        // The actual save operation will happen upon confirmation in the main carb cycling plan
        // creation dialog. This just passes back the modified plan with the new meal configs.
        closeDialog();
    }

    public void cancel(ActionEvent event)
    {
        carbCyclingPlan.clearMealConfigurations();
        closeDialog();
    }

    @Override
    public void destroy()
    {
        listenerBindings.forEach(binding -> binding.unbind());
        listenerBindings.clear();
    }

    private void closeDialog()
    {
        windowManager.close(WindowManager.DIALOG_WINDOW);
        OpenDialogRequest<CarbCyclingPlanModelProxy> dialogRequest = new OpenDialogRequest<>(PfdApplication.getApplicationRootStage().getOwner(), SceneSets.DIALOG_CREATE_CARB_CYCLING_PLAN, callback);
        dialogRequest.setDialogTitle("Create Carb Cycling Plan");
        dialogRequest.getDialogData().put(CreateCarbCyclingPlanDialogController.DATA_CARB_CYCLING_PLAN, carbCyclingPlan);
        dialogRequest.setWidth(570);
        dialogRequest.setHeight(570);
        windowManager.openDialog(dialogRequest);
    }

    private void setupBindingsForMinMaxCarbPercentageFields(CarbCyclingMealConfigurationModel mealConfig, TextField minPercentageField, TextField maxPercentageField)
    {
        Bindings.bindBidirectional(minPercentageField.textProperty(), mealConfig.carbConsumptionMinimumPercentProperty(), new BigDecimalStringConverter());
        listenerBindings.add(BindingUtils.bindModelObservableValueToGuiControl(new ModelPropertyValidationEventConfig<>(mealConfig, mealConfig.carbConsumptionMinimumPercentProperty(), minPercentageField, this::updateSaveValidity)));

        Bindings.bindBidirectional(maxPercentageField.textProperty(), mealConfig.carbConsumptionMaximumPercentProperty(), new BigDecimalStringConverter());
        listenerBindings.add(BindingUtils.bindModelObservableValueToGuiControl(new ModelPropertyValidationEventConfig<>(mealConfig, mealConfig.carbConsumptionMaximumPercentProperty(), maxPercentageField, this::updateSaveValidity)));
    }
}
