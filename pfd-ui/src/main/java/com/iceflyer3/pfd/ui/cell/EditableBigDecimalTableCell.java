package com.iceflyer3.pfd.ui.cell;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.ui.util.TextUtils;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.util.converter.BigDecimalStringConverter;

import java.math.BigDecimal;

/**
 * An editable table cell that supports required big decimal numeric only values
 *
 * For the most part this class uses the official OpenJfx implementation of TextFieldTableCell as a blueprint for how
 * a custom table cell should be implemented.
 *
 * That source file can be found at:
 * https://github.com/openjdk/jfx/blob/master/modules/javafx.controls/src/main/java/javafx/scene/control/cell/TextFieldTableCell.java
 *
 * @param <T> The generic type of the tableview in which this cell is used
 */
public class EditableBigDecimalTableCell<T> extends TableCell<T, BigDecimal> {

    private TextField textField;
    private final BigDecimalStringConverter converter;
    private final ObjectProperty<BigDecimal> value;

    public EditableBigDecimalTableCell()
    {
        this.value = new SimpleObjectProperty<>(new BigDecimal(0));
        this.converter = new BigDecimalStringConverter();
    }

    public BigDecimal getValue() {
        return value.get();
    }

    public ObjectProperty<BigDecimal> valueProperty() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value.set(value);
    }

    @Override
    public void startEdit()
    {
        if (!isEditable() || !getTableView().isEditable() || !getTableColumn().isEditable())
        {
            return;
        }

        super.startEdit();

        if (isEditing())
        {
            if (textField == null)
            {
                textField = createTextField();
            }

            textField.setText(getItemText());
            this.setText(null);

            this.setGraphic(textField);

            textField.selectAll();
            textField.requestFocus();
        }
    }

    @Override
    public void cancelEdit()
    {
        super.cancelEdit();
        this.setText(getItemText());
        this.setGraphic(null);
    }

    @Override
    public void updateItem(BigDecimal item, boolean empty)
    {
        super.updateItem(item, empty);

        if (empty || item == null)
        {
            // According to the docs this MUST happen when overriding this method or weirdness will ensue
            // https://openjfx.io/javadoc/13/javafx.controls/javafx/scene/control/Cell.html#updateItem(T,boolean)
            this.setText(null);
            this.setGraphic(null);
        }
        else
        {
            if (this.isEditing())
            {
                if (textField != null)
                {
                    textField.setText(getItemText());
                }

                this.setText(null);
                this.setGraphic(textField);
            }
            else
            {
                this.setAlignment(Pos.CENTER_RIGHT);
                this.setText(getItemText());
                this.setGraphic(null);
            }
        }
    }

    private TextField createTextField()
    {
        TextField textField = new TextField();
        textField.setTextFormatter(TextUtils.getOptionalDecimalTextInputFormatter());

        textField.setOnAction(actionEvent -> {
            BigDecimal newValue = converter.fromString(textField.getText());
            this.commitEdit(newValue);
            this.value.setValue(newValue);
            actionEvent.consume();
        });

        textField.setOnKeyReleased(keyEvent -> {
            if (keyEvent.getCode() == KeyCode.ESCAPE) {
                this.cancelEdit();
                keyEvent.consume();
            }
        });

        textField.setAlignment(Pos.CENTER_RIGHT);
        this.setAlignment(Pos.CENTER_RIGHT);
        return textField;
    }

    private String getItemText()
    {
        BigDecimal item = this.getItem();
        return item == null ? "" : converter.toString(item);
    }
}
