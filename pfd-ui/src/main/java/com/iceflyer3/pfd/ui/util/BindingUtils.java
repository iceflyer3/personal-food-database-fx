/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.util;

import com.iceflyer3.pfd.constants.CssConstants;
import com.iceflyer3.pfd.ui.listener.ObservableListListenerBinding;
import com.iceflyer3.pfd.ui.listener.ObservableValueListenerBinding;
import com.iceflyer3.pfd.ui.model.api.viewmodel.ValidatingViewModel;
import com.iceflyer3.pfd.ui.listener.ModelPropertyValidationEventConfig;
import com.iceflyer3.pfd.validation.ValidationResult;
import javafx.beans.value.ChangeListener;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.TextInputControl;

public class BindingUtils
{
    /**
     * Binds a JavaFx property that implements the ObservableValue<T> interface from a model object
     * to an associated GUI object from which the JavaFx property gets its value.
     *
     * This binding will register a change listener on the specified JavaFx property that will update
     * the graphical state of the associated GUI control to indicate the valid / invalid state of the
     * value of the control as determined by the model.
     *
     * @param propValidationConfig Configuration object used to configure the binding
     * @return An ObservableValueListenerBinding instance with a generic type of the PropertyType
     * @param <ModelType> The type of the model object
     * @param <PropertyType> The type of the property
     */
    public static <ModelType extends ValidatingViewModel, PropertyType> ObservableValueListenerBinding<PropertyType> bindModelObservableValueToGuiControl(ModelPropertyValidationEventConfig<ModelType, PropertyType> propValidationConfig)
    {
        /*
         * Change listener that invokes validation upon the specified property of the model
         * when a change is detected to the value of said property.
         *
         * The associated GUI control is then graphically updated to reflect valid / invalid
         * state via the presence (or lack thereof) of a red outline based upon the result
         * of the validation performed by the model.
         */
        ChangeListener<PropertyType> changeListener = (observable, oldValue, newValue) -> {
            ValidationResult validationResult = propValidationConfig.getModel().validateProperty(propValidationConfig.getModelProperty().hashCode());

            if (validationResult.wasSuccessful())
            {
                propValidationConfig.getControl().getStyleClass().remove(CssConstants.CLASS_REQUIRED);
            }
            else
            {
                if (!propValidationConfig.getControl().getStyleClass().contains(CssConstants.CLASS_REQUIRED))
                {
                    propValidationConfig.getControl().getStyleClass().add(CssConstants.CLASS_REQUIRED);
                }
            }

            if (propValidationConfig.getAfterModelPropertyValidationAction() != null)
            {
                propValidationConfig.getAfterModelPropertyValidationAction().perform();
            }
        };

        ObservableValueListenerBinding<PropertyType> listenerBinding = new ObservableValueListenerBinding<>(propValidationConfig.getModelProperty());
        listenerBinding.bindChangeListener(changeListener);
        return listenerBinding;
    }

    /**
     * Create a new ObservableListListenerBinding for an ObservableList and binds the provided ChangeListener to it
     * @param list The ObservableList on which to register a new change listener
     * @param listener The change listener instance to register
     * @return ObservableListListenerBinding instance representing the binding between the change listener and the provided list.
     * @param <T> The type of the objects contained in the ObservableList
     */
    public static <T> ObservableListListenerBinding<T> bindObservableListToChangeListener(ObservableList<T> list, ListChangeListener<T> listener)
    {
        ObservableListListenerBinding<T> listBinding = new ObservableListListenerBinding<>(list);
        listBinding.bindChangeListener(listener);
        return listBinding;
    }

    /**
     * Create a new ObservableValueListenerBinding for a TextInputControl and binds the provided ChangeListener to it
     * @param textInputControl The control that a change listener will be registered on
     * @return ObservableValueListenerBinding instance representing the binding between the change listener and the provided control.
     */
    public static ObservableValueListenerBinding<String> bindTextInputToChangeListener(TextInputControl textInputControl, ChangeListener<String> listener)
    {
        ObservableValueListenerBinding<String> binding = new ObservableValueListenerBinding<>(textInputControl.textProperty());
        binding.bindChangeListener(listener);
        return binding;
    }
}
