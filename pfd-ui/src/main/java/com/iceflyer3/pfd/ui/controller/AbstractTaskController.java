package com.iceflyer3.pfd.ui.controller;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.google.common.eventbus.EventBus;
import com.iceflyer3.pfd.ui.util.builder.NotificationBuilder;
import com.iceflyer3.pfd.ui.util.ScreenLoadEventUtils;
import org.springframework.core.task.TaskExecutor;

/**
 * Abstract controller that should be extended by any screen that needs to execute Task(s).
 * This will be most, but not strictly all, of the controllers in the application.
 *
 * Facilitates access to commonly needed items across all of these types of controllers.
 */
public abstract class AbstractTaskController extends AbstractController{

    protected final EventBus eventBus;
    protected final NotificationBuilder notificationBuilder;
    protected final ScreenLoadEventUtils loadEventBuilder;


    /*
     * TODO: The lone place in the application that this task executor is actually
     *       used is on the Select Profile screen. Which means that it is a leftover
     *       from early prototyping before we actually settled on any manner of design.
     *
     *       Let's rework that controller to use a factory like the rest of the application
     *       does and then we can remove the need for the TaskExecutor in the constructor here.
     *
     *       Similarly this will give us the change to address the issue that, due to the same
     *       reason of being a vestige from early prototyping / proof of concept, the user object
     *       just directly uses the view model instead of a proxy.
     */
    protected final TaskExecutor taskExecutor;

    public AbstractTaskController(EventBus eventBus,
                                  NotificationBuilder notificationBuilder,
                                  ScreenLoadEventUtils loadEventBuilder,
                                  TaskExecutor taskExecutor)
    {
        this.eventBus = eventBus;
        this.notificationBuilder = notificationBuilder;
        this.taskExecutor = taskExecutor;
        this.loadEventBuilder = loadEventBuilder;
    }

    public abstract void initialize();
    protected abstract void registerDefaultListeners();
}
