package com.iceflyer3.pfd.ui.controller.dialog;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.PfdApplication;
import com.iceflyer3.pfd.constants.SceneSets;
import com.iceflyer3.pfd.ui.controller.Savable;
import com.iceflyer3.pfd.ui.listener.Destroyable;
import com.iceflyer3.pfd.ui.listener.ObservableValueListenerBinding;
import com.iceflyer3.pfd.ui.manager.OpenDialogRequest;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.MealPlanningProxyFactory;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.carbcycling.CarbCyclingPlanModelProxy;
import com.iceflyer3.pfd.ui.util.BindingUtils;
import com.iceflyer3.pfd.ui.util.TextUtils;
import com.iceflyer3.pfd.ui.listener.ModelPropertyValidationEventConfig;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.util.converter.BigDecimalStringConverter;
import javafx.util.converter.NumberStringConverter;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CreateCarbCyclingPlanDialogController extends AbstractDialogController<CarbCyclingPlanModelProxy> implements Savable, Destroyable
{
    public static final String DATA_CARB_CYCLING_PLAN = "carbCyclingPlan";

    private final MealPlanningProxyFactory mealPlanningProxyFactory;
    private final Set<ObservableValueListenerBinding<?>> listenerBindings;

    private CarbCyclingPlanModelProxy newCarbCyclingPlan;

    public Label descriptionLabel;

    @FXML
    public TextField highCarbRatioInput;

    @FXML
    public TextField lowCarbRatioInput;

    @FXML
    public TextField highCarbDaySurplusPercentInput;

    @FXML
    public TextField lowCarbDayDeficitPercentInput;

    @FXML
    public TextField mealsPerDayInput;

    @FXML
    public CheckBox shouldTimeFatsCheckbox;

    @FXML
    public Button saveButton;

    @FXML
    public Button mealConfigButton;

    @FXML
    public Hyperlink cancelButton;

    public CreateCarbCyclingPlanDialogController(WindowManager windowManager, MealPlanningProxyFactory mealPlanningProxyFactory) {
        super(windowManager);
        this.mealPlanningProxyFactory = mealPlanningProxyFactory;
        this.listenerBindings = new HashSet<>();
    }

    @Override
    public void initialize() {
        this.descriptionLabel.setWrapText(true);
        this.descriptionLabel.setText("""
                The below is the minimum required information for a carb cycling plan.
                
                You must also configure carbohydrate consumption restrictions for the meals that will take place in a given day by clicking on the "Configure Meals" button.
                
                The "Save" button will save the new carb cycling plan.
                """);

        mealsPerDayInput.setTextFormatter(TextUtils.getRequiredIntegerTextInputFormatter());
        highCarbRatioInput.setTextFormatter(TextUtils.getRequiredIntegerTextInputFormatter());
        lowCarbRatioInput.setTextFormatter(TextUtils.getRequiredIntegerTextInputFormatter());
        highCarbDaySurplusPercentInput.setTextFormatter(TextUtils.getRequiredDecimalTextInputFormatter());
        lowCarbDayDeficitPercentInput.setTextFormatter(TextUtils.getRequiredDecimalTextInputFormatter());
    }

    @SuppressWarnings("DuplicatedCode")
    @Override
    protected void registerDefaultListeners() {
        saveButton.setOnAction(this::save);
        mealConfigButton.setOnAction(this::openMealConfigurationDialog);
        cancelButton.setOnAction(this::cancel);
    }

    @Override
    public void initControllerData(Map<String, Object> controllerData)
    {
        // If using meal configuration then the meal configuration dialog will pass the in progress
        // carb cycling plan back to this dialog upon close.
        if (controllerData.containsKey(DATA_CARB_CYCLING_PLAN))
        {
            newCarbCyclingPlan = (CarbCyclingPlanModelProxy) controllerData.get(DATA_CARB_CYCLING_PLAN);
            initBindings();
        }
        else
        {
            mealPlanningProxyFactory.getCarbCyclingPlan(null, (wasSuccessful, proxyResult) -> {
                // We're creating a new proxy instead of loading one here so there isn't really any reason this should ever fail
                if (wasSuccessful)
                {
                    newCarbCyclingPlan = proxyResult;
                    initBindings();
                }
                else
                {
                    // Theoretically this should never actually happen
                    windowManager.openFatalErrorDialog();
                }
            });
        }
    }

    @Override
    public void updateSaveValidity()
    {
        mealConfigButton.setDisable(newCarbCyclingPlan.getMealsPerDay() < 1);
        saveButton.setDisable(!newCarbCyclingPlan.validate().wasSuccessful());
    }

    @Override
    public void save(ActionEvent event)
    {
        callback.complete(newCarbCyclingPlan);
        windowManager.close(WindowManager.DIALOG_WINDOW);
    }

    @Override
    public void destroy()
    {
        listenerBindings.forEach(binding -> binding.unbind());
        listenerBindings.clear();
    }

    @SuppressWarnings("DuplicatedCode")
    private void initBindings()
    {
        // JavaFx bidirectional bindings
        Bindings.bindBidirectional(highCarbRatioInput.textProperty(), newCarbCyclingPlan.highCarbDaysProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(lowCarbRatioInput.textProperty(), newCarbCyclingPlan.lowCarbDaysProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(highCarbDaySurplusPercentInput.textProperty(), newCarbCyclingPlan.highCarbDaySurplusPercentageProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(lowCarbDayDeficitPercentInput.textProperty(), newCarbCyclingPlan.lowCarbDayDeficitPercentageProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(shouldTimeFatsCheckbox.selectedProperty(), newCarbCyclingPlan.shouldTimeFatsProperty());
        Bindings.bindBidirectional(mealsPerDayInput.textProperty(), newCarbCyclingPlan.mealsPerDayProperty(), new NumberStringConverter());

        // Property and save validity bindings
        listenerBindings.add(BindingUtils.bindModelObservableValueToGuiControl(new ModelPropertyValidationEventConfig<>(newCarbCyclingPlan, newCarbCyclingPlan.highCarbDaysProperty(), highCarbRatioInput, this::updateSaveValidity)));
        listenerBindings.add(BindingUtils.bindModelObservableValueToGuiControl(new ModelPropertyValidationEventConfig<>(newCarbCyclingPlan, newCarbCyclingPlan.lowCarbDaysProperty(), lowCarbRatioInput, this::updateSaveValidity)));
        listenerBindings.add(BindingUtils.bindModelObservableValueToGuiControl(new ModelPropertyValidationEventConfig<>(newCarbCyclingPlan, newCarbCyclingPlan.highCarbDaySurplusPercentageProperty(), highCarbDaySurplusPercentInput, this::updateSaveValidity)));
        listenerBindings.add(BindingUtils.bindModelObservableValueToGuiControl(new ModelPropertyValidationEventConfig<>(newCarbCyclingPlan, newCarbCyclingPlan.lowCarbDayDeficitPercentageProperty(), lowCarbDayDeficitPercentInput, this::updateSaveValidity)));
        listenerBindings.add(BindingUtils.bindModelObservableValueToGuiControl(new ModelPropertyValidationEventConfig<>(newCarbCyclingPlan, newCarbCyclingPlan.mealsPerDayProperty(), mealsPerDayInput, this::updateSaveValidity)));

        // Trigger initial invalid form state.
        updateSaveValidity();
    }

    private void cancel(ActionEvent event)
    {
        windowManager.close(WindowManager.DIALOG_WINDOW);
    }

    private void openMealConfigurationDialog(ActionEvent event)
    {
        /*
         * TODO: Frankly, this is hacky and a bit convoluted. But for the time being it is the
         *       path of least resistance.
         *
         *       What we really _actually_ want here is a stepper control that allows the user to
         *       advance from the first step of defining the plan details to the second step of
         *       configuring the meal carb consumption restrictions. Then everything is in a single
         *       dialog.
         *
         *       But I haven't investigated creating custom controls in JavaFx yet and ControlsFx
         *       does not have a stepper control. So for now hacky and convoluted will have to suffice.
         */
        /*
         * Currently, we don't support having multiple dialogs open at the same time. Since this is
         * (at the time of writing) the lone place in the application in which one dialog opens another
         * secondary dialog these two dialogs interact by just passing the data on which they operate
         * back and forth. That way we still only have one actually open dialog at a time.
         *
         * Due to this interaction pattern we don't care about and have no use for the callback here.
         * Because we don't care about the callback we can pass the callback for the Create Carb Cycling
         * Plan dialog to the Meal Configuration dialog so that it may be restored when the Create Carb
         * Cycling dialog is re-opened.
        */
        windowManager.close(WindowManager.DIALOG_WINDOW);
        OpenDialogRequest<CarbCyclingPlanModelProxy> configurationDialogRequest = new OpenDialogRequest<>(PfdApplication.getApplicationRootStage().getOwner(), SceneSets.DIALOG_CARB_CYCLING_MEAL_CONFIGURATION, callback);
        configurationDialogRequest.getDialogData().put(CreateCarbCyclingPlanMealConfigurationDialogController.DATA_CARB_CYCLING_PLAN, newCarbCyclingPlan);
        configurationDialogRequest.setDialogTitle("New Carb Cycling Plan Meal Configuration");
        configurationDialogRequest.setHeight(780);
        configurationDialogRequest.setWidth(900);
        windowManager.openDialog(configurationDialogRequest);
    }
}
