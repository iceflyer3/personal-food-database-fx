package com.iceflyer3.pfd.ui.controller.sections.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.enums.Nutrient;
import com.iceflyer3.pfd.ui.controller.sections.ScreenSection;
import com.iceflyer3.pfd.ui.model.api.mealplanning.MealPlanModel;
import javafx.beans.binding.StringExpression;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.util.converter.BigDecimalStringConverter;

/**
 * Screen section implementation that shows overview information for a meal plan than was
 * created via the "manual" registration mode
 */
public class ManualMealPlanOverviewScreenSection implements ScreenSection
{
    private final MealPlanModel mealPlan;
    private final BigDecimalStringConverter bigDecimalStringConverter;

    public ManualMealPlanOverviewScreenSection(MealPlanModel mealPlan)
    {
        this.mealPlan = mealPlan;
        this.bigDecimalStringConverter = new BigDecimalStringConverter();
    }

    @Override
    public Node create()
    {
        Label subHeader = new Label("Active plan details");
        subHeader.getStyleClass().addAll("bold", "underline", "sub-header-text");

        VBox container = new VBox(10);
        container.getChildren().add(0, subHeader);
        container.getChildren().add(1, createConfigurationDetailsGridPane());

        return new TitledPane("Manual Meal Plan Configuration", container);
    }

    // IntelliJ detecting duplicated code when adding to the GridPane
    @SuppressWarnings("DuplicatedCode")
    private GridPane createConfigurationDetailsGridPane()
    {
        GridPane gridPane = new GridPane();
        gridPane.setVgap(10);
        gridPane.setHgap(20);

        // First column
        Label tdeeCaloriesLabel = new Label("TDEE Calories:");
        Label tdeeCaloriesValueLabel = new Label();
        tdeeCaloriesValueLabel.textProperty().bind(StringExpression.stringExpression(mealPlan.tdeeCaloriesProperty()));

        Label dailyCaloriesLabel = new Label("Daily Calories:");
        Label dailyCaloriesValueLabel = new Label(bigDecimalStringConverter.toString(mealPlan.getDailyCalories()));

        gridPane.add(tdeeCaloriesLabel, 0, 0);
        gridPane.add(tdeeCaloriesValueLabel, 1, 0);
        gridPane.add(dailyCaloriesLabel, 0, 1);
        gridPane.add(dailyCaloriesValueLabel, 1, 1);

        // Second column
        Label proteinLabel = new Label("Protein:");
        Label proteinValueLabel = new Label(bigDecimalStringConverter.toString(mealPlan.getNutrientSuggestedAmount(Nutrient.PROTEIN)));

        Label carbsLabel = new Label("Carbs:");
        Label carbsValueLabel = new Label(bigDecimalStringConverter.toString(mealPlan.getNutrientSuggestedAmount(Nutrient.CARBOHYDRATES)));

        Label fatsLabel = new Label("Total Fats:");
        Label fatsValueLabel = new Label(bigDecimalStringConverter.toString(mealPlan.getNutrientSuggestedAmount(Nutrient.TOTAL_FATS)));

        gridPane.add(proteinLabel, 2, 0);
        gridPane.add(proteinValueLabel, 3, 0);
        gridPane.add(carbsLabel, 2, 1);
        gridPane.add(carbsValueLabel, 3, 1);
        gridPane.add(fatsLabel, 2, 2);
        gridPane.add(fatsValueLabel, 3, 2);

        return gridPane;
    }
}
