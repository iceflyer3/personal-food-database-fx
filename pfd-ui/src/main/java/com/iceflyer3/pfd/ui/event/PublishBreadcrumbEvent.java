/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.event;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class PublishBreadcrumbEvent {

    private final String label;
    private final String applicationSection;
    private final String setToLoad;
    private final String forStage;
    private final int depth;
    private final HashMap<String, Object> controllerData;

    /**
     * Event that notifies the application to add a new breadcrumb to the list of breadcrumbs found
     * in the breadcrumb bar at the top of the window.
     * @param breadcrumbLabel The label to be used in the breadcrumb bar
     * @param applicationSection The name of the section of the application that the set belongs to
     * @param setToLoad The name of the set that should be loaded upon a click on the breadcrumb.
     *                  This should be the name of the screen that the breadcrumb represents.
     * @param forStage  The name of the stage to which this breadcrumb should apply
     * @param breadcrumbDepth The depth of the breadcrumb. The home screen is depth zero. Each subsequent
     *                        screen that drills down further from there adds one to the depth.
     *                        Ex: Home (depth 1) > Meal Planning Overview (depth 2) > View Day (depth 3)
     */
    public PublishBreadcrumbEvent(String breadcrumbLabel, String applicationSection, String setToLoad, String forStage, int breadcrumbDepth)
    {
        this.label = breadcrumbLabel;
        this.applicationSection = applicationSection;
        this.setToLoad = setToLoad;
        this.forStage = forStage;
        this.depth = breadcrumbDepth;
        this.controllerData = new HashMap<>();
    }

    public String getLabel() {
        return label;
    }

    public String getApplicationSection() {
        return applicationSection;
    }

    public String getSetToLoad() {
        return setToLoad;
    }

    public String getForStage() {
        return forStage;
    }

    public int getDepth() {
        return depth;
    }

    /**
     * Add an object that should be passed to the target screen of the breadcrumb
     * during screen initialization
     * @param dataClassName The class name of the object as acquired by calling Object.class.getClassName()
     * @param data The data that will passed to the controller for the screen targeted by this breadcrum
     */
    public void addData(String dataClassName, Object data)
    {
        controllerData.put(dataClassName, data);
    }

    /**
     * Gets the map of initialization data that should be passed to the screen targeted by this controller.
     * @return An immutable map
     */
    public Map<String, Object> getControllerData() {
        return Collections.unmodifiableMap(controllerData);
    }

    @Override
    public String toString()
    {
        return "PublishBreadcrumbEvent{" + "label='" + label + '\'' + ", applicationSection='" + applicationSection + '\'' + ", setToLoad='" + setToLoad + '\'' + ", depth=" + depth + '}';
    }
}
