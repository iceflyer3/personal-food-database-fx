package com.iceflyer3.pfd.ui.controller.sections.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023, 2025 Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.constants.FontAwesomeIcons;
import com.iceflyer3.pfd.enums.Nutrient;
import com.iceflyer3.pfd.enums.UnitOfMeasure;
import com.iceflyer3.pfd.ui.cell.ActionButtonProvider;
import com.iceflyer3.pfd.ui.cell.ActionButtonTableCell;
import com.iceflyer3.pfd.ui.cell.EditableBigDecimalTableCell;
import com.iceflyer3.pfd.ui.controller.sections.ScreenSection;
import com.iceflyer3.pfd.ui.enums.PfdActionType;
import com.iceflyer3.pfd.ui.format.NutrientStringConverter;
import com.iceflyer3.pfd.ui.format.UnitOfMeasureStringConverter;
import com.iceflyer3.pfd.ui.model.api.mealplanning.MealPlanNutrientSuggestionModel;
import com.iceflyer3.pfd.ui.model.api.mealplanning.registration.MealPlanningRegistration;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanNutrientSuggestionViewModel;
import com.iceflyer3.pfd.ui.util.builder.ActionNodeBuilder;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TitledPane;
import javafx.scene.control.cell.ChoiceBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class MealPlanRegistrationCustomNutrientsScreenSection implements ScreenSection
{
    private final ObservableList<MealPlanNutrientSuggestionModel> customNutrients;

    public MealPlanRegistrationCustomNutrientsScreenSection(MealPlanningRegistration mealPlanRegistration)
    {
        customNutrients = mealPlanRegistration.getCustomNutrients();
    }

    @Override
    public Node create()
    {
        Button addNutrientButton = ActionNodeBuilder.createIconButton("ADD NUTRIENT", FontAwesomeIcons.PLUS)
                .forActionType(PfdActionType.SECONDARY)
                .withAction(event -> {
                    customNutrients.add(new MealPlanNutrientSuggestionViewModel(Nutrient.FIBER, BigDecimal.ONE, UnitOfMeasure.GRAM));
                })
                .build();

        TableView<MealPlanNutrientSuggestionModel> customNutrientsTable = createTableView();

        VBox.setMargin(customNutrientsTable, new Insets(20, 0, 20, 0));
        VBox container = new VBox(10);
        container.getChildren().addAll(addNutrientButton, customNutrientsTable);

        return new TitledPane("Custom Nutrients", container);
    }

    private TableView<MealPlanNutrientSuggestionModel> createTableView()
    {
        TableView<MealPlanNutrientSuggestionModel> nutrientsTable = new TableView<>(customNutrients);
        nutrientsTable.setEditable(true);

        List<Nutrient> macronutrients = List.of(Nutrient.CALORIES, Nutrient.PROTEIN, Nutrient.CARBOHYDRATES, Nutrient.TOTAL_FATS);
        List<Nutrient> validNutrients = Arrays.stream(Nutrient.values()).filter(nutrient -> !macronutrients.contains(nutrient)).toList();
        TableColumn<MealPlanNutrientSuggestionModel, Nutrient> nutrientColumn = new TableColumn<>("Nutrient");
        nutrientColumn.setCellFactory(col -> new ChoiceBoxTableCell<>(new NutrientStringConverter(), FXCollections.observableList(validNutrients)));
        nutrientColumn.setCellValueFactory(new PropertyValueFactory<>("nutrient"));

        TableColumn<MealPlanNutrientSuggestionModel, BigDecimal> amountColumn = new TableColumn<>("Amount");
        amountColumn.setCellFactory(col -> new EditableBigDecimalTableCell<>());
        amountColumn.setCellValueFactory(new PropertyValueFactory<>("suggestedAmount"));

        List<UnitOfMeasure> nutrientUnitOfMeasures = List.of(UnitOfMeasure.GRAM, UnitOfMeasure.MILLIGRAM, UnitOfMeasure.MICROGRAM);
        List<UnitOfMeasure> validUomList = Arrays.stream(UnitOfMeasure.values()).filter(nutrientUnitOfMeasures::contains).toList();
        TableColumn<MealPlanNutrientSuggestionModel, UnitOfMeasure> uomColumn = new TableColumn<>("Unit of Measure");
        uomColumn.setCellFactory(col -> new ChoiceBoxTableCell<>(new UnitOfMeasureStringConverter(), FXCollections.observableList(validUomList)));
        uomColumn.setCellValueFactory(new PropertyValueFactory<>("unitOfMeasure"));

        ActionButtonProvider<MealPlanNutrientSuggestionModel> buttonProvider = rowData ->
        {
            Button deleteButton = ActionNodeBuilder
                    .createIconButton("", FontAwesomeIcons.TIMES, 16)
                    .forActionType(PfdActionType.DANGER)
                    .withAction((event) -> {
                        Node buttonNode = ((Node) event.getSource());
                        MealPlanNutrientSuggestionModel buttonBackingRowData = (MealPlanNutrientSuggestionModel) buttonNode.getUserData();
                        customNutrients.remove(buttonBackingRowData);
                    })
                    .build();

            return List.of(deleteButton);
        };
        TableColumn<MealPlanNutrientSuggestionModel, Nutrient> actionColumn = new TableColumn<>();
        actionColumn.setCellFactory(column -> new ActionButtonTableCell<>(buttonProvider));
        actionColumn.setCellValueFactory(new PropertyValueFactory<>("nutrient"));

        nutrientsTable.getColumns().addAll(nutrientColumn, amountColumn, uomColumn, actionColumn);
        return nutrientsTable;
    }
}
