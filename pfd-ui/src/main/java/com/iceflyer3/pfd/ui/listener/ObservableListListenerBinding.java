package com.iceflyer3.pfd.ui.listener;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import javafx.beans.InvalidationListener;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;

import java.util.HashSet;
import java.util.Set;

/**
 * Wrapper class that associates listeners to the ObservableList to which they were registered on.
 * This class maintains a reference to each registered listener so that they may later be removed
 * from the ObservableList when no longer needed.
 *
 * @param <T> The type of the ObservableList (which should also be the type of the ChangeListener)
 */
public class ObservableListListenerBinding<T>
{
    private final ObservableList<T> list;
    private final Set<ListChangeListener<T>> changeListeners;
    private final Set<InvalidationListener> invalidationListeners;

    public ObservableListListenerBinding(ObservableList<T> list)
    {
        this.list = list;
        changeListeners = new HashSet<>();
        invalidationListeners = new HashSet<>();
    }

    /**
     * Register a new change listener with the observable value
     * @param listener The listener that will be registered
     */
    public void bindChangeListener(ListChangeListener<T> listener)
    {
        changeListeners.add(listener);
        list.addListener(listener);
    }

    /**
     * Register a new invalidation listener with the observable value
     * @param listener The listener that will be registered
     */
    public void bindInvalidationListener(InvalidationListener listener)
    {
        invalidationListeners.add(listener);
        list.addListener(listener);
    }

    /**
     * Removes any registered listeners (both change and invalidation) from the ObservableValue
     */
    public void unbind()
    {
        changeListeners.forEach(list::removeListener);
        invalidationListeners.forEach(list::removeListener);
        changeListeners.clear();
        invalidationListeners.clear();
    }
}
