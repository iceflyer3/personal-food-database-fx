/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.controller.mealplanning;

import com.calendarfx.model.Calendar;
import com.calendarfx.model.CalendarSource;
import com.calendarfx.model.Entry;
import com.calendarfx.model.LoadEvent;
import com.calendarfx.view.DateControl;
import com.calendarfx.view.MonthView;
import com.google.common.eventbus.EventBus;
import com.iceflyer3.pfd.constants.ApplicationSections;
import com.iceflyer3.pfd.constants.CssConstants;
import com.iceflyer3.pfd.constants.FontAwesomeIcons;
import com.iceflyer3.pfd.constants.SceneSets;
import com.iceflyer3.pfd.ui.event.PublishBreadcrumbEvent;
import com.iceflyer3.pfd.ui.event.ScreenNavigationEvent;
import com.iceflyer3.pfd.exception.InvalidApplicationStateException;
import com.iceflyer3.pfd.ui.controller.AbstractTaskController;
import com.iceflyer3.pfd.ui.controller.HostStageAware;
import com.iceflyer3.pfd.ui.controller.dialog.AssociateMealPlanCalendarDaysDialogController;
import com.iceflyer3.pfd.ui.controller.dialog.MealPlanCalendarEntryDetailsDialogController;
import com.iceflyer3.pfd.ui.enums.PfdActionType;
import com.iceflyer3.pfd.ui.manager.OpenDialogRequest;
import com.iceflyer3.pfd.ui.manager.VoidDialogCallback;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import com.iceflyer3.pfd.data.request.mealplanning.AssociateMealPlanDayToCalendarDayRequest;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.MealPlanCalendarDayModelProxy;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.MealPlanningProxyFactory;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.util.CalendarUtils;
import com.iceflyer3.pfd.ui.util.ScreenLoadEventUtils;
import com.iceflyer3.pfd.ui.util.builder.ActionNodeBuilder;
import com.iceflyer3.pfd.ui.util.builder.NotificationBuilder;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MealPlanningCalendarController extends AbstractTaskController implements HostStageAware
{
    private final static Logger LOG = LoggerFactory.getLogger(MealPlanningCalendarController.class);

    private final CalendarUtils calendarUtils;
    private final WindowManager windowManager;
    private final MealPlanningProxyFactory mealPlanningProxyFactory;
    private final Label dateHeaderLabel;

    private Calendar calendar;
    private UserViewModel user;
    private String hostingStageName;
    private Set<MealPlanCalendarDayModelProxy> calendarEntries;

    @FXML
    public VBox rootContainer;

    public MealPlanningCalendarController(
            EventBus eventBus,
            NotificationBuilder notificationBuilder,
            ScreenLoadEventUtils loadEventBuilder,
            TaskExecutor taskExecutor,
            CalendarUtils calendarUtils,
            WindowManager windowManager,
            MealPlanningProxyFactory mealPlanningProxyFactory)
    {
        super(eventBus, notificationBuilder, loadEventBuilder, taskExecutor);

        this.calendarUtils = calendarUtils;
        this.windowManager = windowManager;
        this.mealPlanningProxyFactory = mealPlanningProxyFactory;

        dateHeaderLabel = new Label();
        dateHeaderLabel.getStyleClass().add(CssConstants.CLASS_TEXT_HEADLINE);
    }

    @Override
    public void initialize()
    {
        MonthView calendarView = setupCalendarView();
        AnchorPane buttonsBar = setupActionButtonBar(calendarView);
        updateHeader(LocalDate.now());

        String descriptionText = """
                Utilizing the calendar you may plan out which of the planned days from either the active meal plan or a carb cycling plan for the active meal plan you are going to eat on each calendar day. A list of raw ingredients (simple foods) needed for the meals that comprise the planned days in the calendar may be generated by using the "Generate Grocery List" button.""";
        Label descriptionLabel = new Label(descriptionText);
        descriptionLabel.setWrapText(true);

        VBox.setVgrow(calendarView, Priority.ALWAYS);
        rootContainer.getChildren().addAll(dateHeaderLabel, descriptionLabel, buttonsBar, calendarView);
    }

    @Override
    protected void registerDefaultListeners()
    {

    }

    @Override
    public void initControllerData(Map<String, Object> controllerData)
    {
        user = (UserViewModel) controllerData.get(UserViewModel.class.getName());
        refreshCalendarEntries();
    }

    @Override
    public void initHostingStageName(String stageName)
    {
        this.hostingStageName = stageName;

        // Publish the new breadcrumb
        PublishBreadcrumbEvent landingScreenBreadcrumb = new PublishBreadcrumbEvent("Meal Planning Calendar", ApplicationSections.MEAL_PLANNING, SceneSets.MEAL_PLANNING_CALENDAR, hostingStageName, 2);
        eventBus.post(landingScreenBreadcrumb);
    }


    private MonthView setupCalendarView()
    {
        // Setup the calendar
        calendar = new Calendar("Meal Plan Calendar");
        calendar.setReadOnly(true);

        CalendarSource calendarSource = new CalendarSource("Meal Plan");
        calendarSource.getCalendars().add(calendar);

        // Month view does not have navigation buttons or a date label which we encountered problems theming.
        MonthView monthView = new MonthView();
        monthView.getCalendarSources().add(calendarSource);
        monthView.addEventFilter(MouseEvent.MOUSE_CLICKED, event -> {
            if (event.getClickCount() > 1)
            {
                // Disable CalendarFx double click entry creation. All events are added to the
                // calendar programmatically. It should not support freeform entry.
                event.consume();
            }
        });
        monthView.addEventFilter(LoadEvent.LOAD, event -> {
            updateHeader(monthView.getDate());
        });
        monthView.setEntryContextMenuCallback(new Callback<DateControl.EntryContextMenuParameter, ContextMenu>()
        {
            @Override
            public ContextMenu call(DateControl.EntryContextMenuParameter param)
            {
                ContextMenu contextMenu = new ContextMenu();

                MenuItem detailsItem = new MenuItem("Details");
                detailsItem.setOnAction(event -> {
                    MealPlanCalendarDayModelProxy pfdCalendarEntry = getPfdEntryForCalendarFxEntry(param.getEntry());

                    OpenDialogRequest<Void> detailsDialogRequest = new OpenDialogRequest<>(rootContainer.getScene().getWindow(), SceneSets.DIALOG_MEAL_PLAN_CALENDAR_ENTRY_DETAILS, new VoidDialogCallback());
                    detailsDialogRequest.getDialogData().put(MealPlanCalendarEntryDetailsDialogController.DATA_CALENDAR_DAY, pfdCalendarEntry);
                    detailsDialogRequest.setHeight(700);
                    detailsDialogRequest.setWidth(750);
                    windowManager.openDialog(detailsDialogRequest);
                });

                MenuItem deleteItem = new MenuItem("Delete");
                deleteItem.setOnAction(event -> {
                    MealPlanCalendarDayModelProxy pfdCalendarEntry = getPfdEntryForCalendarFxEntry(param.getEntry());

                    // Recurrences will have no MealPlanCalendarDay to tie back to. So it is possible the optional will be empty.
                    pfdCalendarEntry.delete((wasSuccessful, resultMessage) -> {

                        notificationBuilder.create(resultMessage, hostingStageName).show();

                        if (wasSuccessful)
                        {
                            refreshCalendarEntries();
                        }
                    });
                });

                contextMenu.getItems().addAll(detailsItem, deleteItem);
                return contextMenu;
            }
        });

        return monthView;
    }

    private AnchorPane setupActionButtonBar(MonthView calendar)
    {
        AnchorPane container = new AnchorPane();

        // Left-hand buttons
        HBox leftButtonContainer = new HBox(10);
        Button addEventButton = ActionNodeBuilder
                .createIconButton("NEW ENTRY", FontAwesomeIcons.PLUS)
                .forActionType(PfdActionType.PRIMARY)
                .withAction(event -> {
                    OpenDialogRequest<AssociateMealPlanDayToCalendarDayRequest> dialogRequest = new OpenDialogRequest<>(rootContainer.getScene().getWindow(), SceneSets.DIALOG_MEAL_PLAN_CALENDAR_ADD_ENTRY, dialogOutput -> {
                        LOG.debug("Dialog has closed... saving new entries...");
                        calendarUtils.createCalendarEntries(dialogOutput, (wasSuccessful, proxyResult) -> {
                            if (wasSuccessful)
                            {
                                refreshCalendarEntries();
                            }
                            else
                            {
                                notificationBuilder.create("An error has occurred while saving the new calendar day", hostingStageName).show();
                            }
                        });
                    });
                    dialogRequest.setDialogTitle("Add Planned Day To Calendar");
                    dialogRequest.setWidth(AssociateMealPlanCalendarDaysDialogController.WIDTH);
                    dialogRequest.setHeight(AssociateMealPlanCalendarDaysDialogController.HEIGHT);
                    dialogRequest.getDialogData().put(AssociateMealPlanCalendarDaysDialogController.DATA_USER, user);
                    dialogRequest.getDialogData().put(AssociateMealPlanCalendarDaysDialogController.DATA_PARENT_WINDOW_STAGE_NAME, hostingStageName);
                    windowManager.openDialog(dialogRequest);
                })
                .build();

        Button generateGroceryListButton = ActionNodeBuilder
                .createIconButton("GENERATE GROCERY LIST", FontAwesomeIcons.DOWNLOAD)
                .forActionType(PfdActionType.SECONDARY)
                .withAction(event -> {
                    OpenDialogRequest<Void> dialogRequest = new OpenDialogRequest<>(rootContainer.getScene().getWindow(), SceneSets.DIALOG_MEAL_PLAN_CALENDAR_GROCERY_LIST, new VoidDialogCallback());
                    dialogRequest.setDialogTitle("Grocery List");
                    dialogRequest.setWidth(700);
                    dialogRequest.setHeight(750);
                    windowManager.openDialog(dialogRequest);
                })
                .build();

        Button backToOverviewButton = ActionNodeBuilder
                .createIconButton("BACK TO OVERVIEW", FontAwesomeIcons.CIRCLE_ARROW_LEFT)
                .forActionType(PfdActionType.SECONDARY)
                .withAction(event -> {
                    ScreenNavigationEvent navigationEvent = new ScreenNavigationEvent(ApplicationSections.MEAL_PLANNING, SceneSets.MEAL_PLANNING_OVERVIEW, hostingStageName, user);
                    eventBus.post(navigationEvent);
                })
                .build();
        leftButtonContainer.getChildren().addAll(addEventButton, generateGroceryListButton, backToOverviewButton);

        // Right-hand buttons
        HBox rightButtonContainer = new HBox(10);

        Button previousMonthButton = ActionNodeBuilder
                .createIconButton("PREV", FontAwesomeIcons.CHEVRON_LEFT)
                .forActionType(PfdActionType.TERTIARY)
                .withAction(event -> {
                    calendar.goBack();
                    updateHeader(calendar.getDate());
                })
                .build();

        Button todayButton = ActionNodeBuilder
                .createIconButton("TODAY", null)
                .withIconLocation(ContentDisplay.RIGHT)
                .forActionType(PfdActionType.TERTIARY)
                .withAction(event -> {
                    calendar.goToday();
                    updateHeader(calendar.getDate());
                })
                .build();

        Button nextMonthButton = ActionNodeBuilder
                .createIconButton("NEXT", FontAwesomeIcons.CHEVRON_RIGHT)
                .withIconLocation(ContentDisplay.RIGHT)
                .forActionType(PfdActionType.TERTIARY)
                .withAction(event -> {
                    calendar.goForward();
                    updateHeader(calendar.getDate());
                })
                .build();
        rightButtonContainer.getChildren().addAll(previousMonthButton, todayButton, nextMonthButton);

        AnchorPane.setLeftAnchor(leftButtonContainer, 0.0);
        AnchorPane.setRightAnchor(rightButtonContainer, 0.0);
        container.getChildren().addAll(leftButtonContainer, rightButtonContainer);
        return container;
    }

    private void updateHeader(LocalDate date)
    {
        String newHeader = String.format("Meal Planning Calendar for %s %s", date.getMonth().getDisplayName(TextStyle.SHORT, Locale.US), date.getYear());
        dateHeaderLabel.setText(newHeader);
    }

    private void refreshCalendarEntries()
    {
        loadEventBuilder.emitBegin("Loading calendar details", hostingStageName);
        mealPlanningProxyFactory.getCalendarDaysForUser(user.getUserId(), (wasSuccessful, proxyResult) -> {
            if (wasSuccessful)
            {
                // Clear out any previously rendered events before rendering the new ones
                calendar.clear();

                calendarEntries = proxyResult;
                calendarEntries.forEach(pfdEntry -> calendar.addEntry(calendarUtils.createCalendarFxEntry(pfdEntry, pfdEntry.getCalendarDate())));
            }
            else
            {
                notificationBuilder.create("An error has occurred while retrieving the entries for the calendar", hostingStageName).show();
            }
            loadEventBuilder.emitCompleted(hostingStageName);
        });
    }

    private MealPlanCalendarDayModelProxy getPfdEntryForCalendarFxEntry(Entry<?> calendarFxEntry)
    {
        Optional<MealPlanCalendarDayModelProxy> pfdCalendarEntry = calendarEntries
                .stream()
                .filter(pfdEntry -> pfdEntry.getCalendarDate().equals(calendarFxEntry.getStartDate()))
                .findFirst();

        if (pfdCalendarEntry.isPresent())
        {
            return pfdCalendarEntry.get();
        }
        else
        {
            LOG.error("An error has occurred while retrieving data for the entry", new InvalidApplicationStateException(String.format("No PFD Calendar Day was found for the CalendarFx entry for date %s", calendarFxEntry.getStartDate())));
            windowManager.openFatalErrorDialog();
        }

        return null;
    }
}
