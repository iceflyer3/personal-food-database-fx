package com.iceflyer3.pfd.ui.widget.stepper;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023, 2025 Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Implemented by FXML Controller classes which wish to be able to be part of a series of controllers that will be
 * "stepped" through one at a time.
 *
 * Controllers that wish to implement this interface should follow the naming convention of:
 *      The class name should be in Pascal Case and should be the same as the associated FXML file with the word Controller on the end
 *      The FXML file name should separate each word in the file name with a hyphen (-)
 */
public interface Steppable
{
    /**
     * Hook for receiving a handle to the StepOrchestrator to which this step belongs. The StepOrchestrator is used
     * by the steppable to indicate to the StepOrchestrator that this step has completed.
     * @param stepOrchestrator The orchestrator in which this steppable is being displayed
     */
    void usingStepOrchestrator(StepOrchestrator stepOrchestrator);
}