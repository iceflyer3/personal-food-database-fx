/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.util;

import com.calendarfx.model.Entry;
import com.iceflyer3.pfd.enums.WeeklyInterval;
import com.iceflyer3.pfd.data.factory.MealPlanningTaskFactory;
import com.iceflyer3.pfd.data.request.mealplanning.AssociateMealPlanDayToCalendarDayRequest;
import com.iceflyer3.pfd.data.request.mealplanning.GroceryListForMealPlanCalendarDateRangeSearchRequest;
import com.iceflyer3.pfd.data.task.mealplanning.calendar.FindGroceryListForCalendarDateRangeTask;
import com.iceflyer3.pfd.data.task.mealplanning.calendar.SaveMealPlanCalendarDayTask;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.api.proxy.ProxyDataResultCallback;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.MealPlanCalendarDayModelProxy;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanCalendarDayViewModel;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Convenience class that serves as a holder of calendar related business functionality so that we
 * don't have to inject the TaskExecutor and MealPlanningTaskFactory into the calendar related
 * controllers.
 *
 * This is a break from our usual proxy paradigm where the proxy would usually save itself. But given
 * that calendar days are a bit unique anyway in that there is a decent likelihood that we're
 * associating a planned day to multiple calendar days the one-to-one saving paradigm of the proxies
 * doesn't quite make as much sense for them.
 */
@Component
public class CalendarUtils
{
    private final static Logger LOG = LoggerFactory.getLogger(CalendarUtils.class);

    private final TaskExecutor taskExecutor;
    private final MealPlanningTaskFactory mealPlanningTaskFactory;

    public CalendarUtils(TaskExecutor taskExecutor, MealPlanningTaskFactory mealPlanningTaskFactory)
    {
        this.taskExecutor = taskExecutor;
        this.mealPlanningTaskFactory = mealPlanningTaskFactory;
    }

    public void createCalendarEntries(AssociateMealPlanDayToCalendarDayRequest request, ProxyDataResultCallback<Set<MealPlanCalendarDayModelProxy>> saveCallback)
    {
        SaveMealPlanCalendarDayTask createCalendarDayTask = mealPlanningTaskFactory.createCalendarDay(request);
        createCalendarDayTask.setOnSucceeded(event -> {
            List<MealPlanCalendarDayViewModel> viewModels = createCalendarDayTask.getValue();
            Set<MealPlanCalendarDayModelProxy> proxies = viewModels
                    .stream()
                    .map(viewModel -> new MealPlanCalendarDayModelProxy(taskExecutor, mealPlanningTaskFactory, viewModel))
                    .collect(Collectors.toSet());

            saveCallback.receiveResult(true, proxies);
        });
        createCalendarDayTask.setOnFailed(event -> {
            LOG.error("An error has occurred while creating the calendar entry", createCalendarDayTask.getException());
            saveCallback.receiveResult(false, null);
        });
        taskExecutor.execute(createCalendarDayTask);
    }

    public Entry<?> createCalendarFxEntry(MealPlanCalendarDayModelProxy forDay, LocalDate entryDate)
    {
        Entry<?> newEntry = new Entry<>();
        newEntry.getStyleClass().add("calendar-entry-primary");
        newEntry.setTitle(forDay.getPlannedDay().getDayLabel());
        newEntry.setFullDay(true);
        newEntry.changeStartDate(entryDate);
        newEntry.changeEndDate(entryDate);
        return newEntry;
    }

    public void getGroceryList(GroceryListForMealPlanCalendarDateRangeSearchRequest searchRequest, ProxyDataResultCallback<List<IngredientModel>> resultCallback)
    {
        LOG.debug("Searching for grocery list for dates {} to {}", searchRequest.getStartDate(), searchRequest.getEndDate());
        FindGroceryListForCalendarDateRangeTask findGroceryListTask = mealPlanningTaskFactory.getGroceryList(searchRequest);
        findGroceryListTask.setOnSucceeded(event -> resultCallback.receiveResult(true, findGroceryListTask.getValue()));
        findGroceryListTask.setOnFailed(event -> {
            LOG.error(
                    "An error has occurred searching for the grocery list for the date range of {} to {}",
                    searchRequest.getStartDate(),
                    searchRequest.getEndDate(),
                    findGroceryListTask.getException()
            );

            resultCallback.receiveResult(false, null);
        });
        taskExecutor.execute(findGroceryListTask);
    }

    /**
     * Creates a Recurrence Rule according to RFC 2445.
     * https://datatracker.ietf.org/doc/html/rfc2445
     *
     * @param calendarDay The day the rule will be created for
     * @return The recurrence rule
     */
    /* NOTE:
     * This didn't end up working with CalendarFx. Even some example rules from the RFC itself would
     * cause it to throw an exception. I'm leaving it here just in case there is ever need for it
     * again because it is already written anyway.
     *
     * However, for the time being we're just going to generate our own recurrences for events and
     * manually add them to the calendar.
     */
    public String getRecurrenceRuleForEntry(MealPlanCalendarDayModelProxy calendarDay)
    {
        StringBuilder ruleBuilder = new StringBuilder("RRULE:FREQ=WEEKLY");

        Instant recurrenceEndDate = calendarDay
                .getCalendarDate()
                .plusMonths(calendarDay.getIntervalMonthsDuration())
                .atStartOfDay()
                .toInstant(ZoneOffset.UTC);

        String untilString = recurrenceEndDate
                .toString()
                .replace("-", "")
                .replace(":", "");
        ruleBuilder.append(String.format(";UNTIL=%s", untilString));

        String dayName = "";
        switch (calendarDay.getCalendarDate().getDayOfWeek())
        {
            case MONDAY -> dayName = "MO";
            case TUESDAY -> dayName = "TU";
            case WEDNESDAY -> dayName = "WE";
            case THURSDAY -> dayName = "TH";
            case FRIDAY -> dayName = "FR";
            case SATURDAY -> dayName = "SA";
            case SUNDAY -> dayName = "SU";
        }
        ruleBuilder.append(String.format(";BYDAY=%s", dayName));

        if (calendarDay.getWeeklyInterval() == WeeklyInterval.BI_WEEKLY)
        {
            ruleBuilder.append(";INTERVAL=2");
        }

        return ruleBuilder.toString();
    }
}
