package com.iceflyer3.pfd.ui.controller.about;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.google.common.eventbus.EventBus;
import com.google.common.io.CharStreams;
import com.iceflyer3.pfd.PfdApplication;
import com.iceflyer3.pfd.constants.*;
import com.iceflyer3.pfd.ui.event.PublishBreadcrumbEvent;
import com.iceflyer3.pfd.exception.InvalidApplicationStateException;
import com.iceflyer3.pfd.ui.controller.AbstractController;
import com.iceflyer3.pfd.ui.controller.HostStageAware;
import com.iceflyer3.pfd.ui.enums.PfdActionType;
import com.iceflyer3.pfd.ui.util.LayoutUtils;
import com.iceflyer3.pfd.ui.util.builder.ActionNodeBuilder;
import javafx.application.HostServices;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class AboutScreenController extends AbstractController implements HostStageAware {

    private final HostServices hostServices;
    private final EventBus eventBus;
    private final Insets tabInsets;

    @FXML
    public Tab detailsTab;

    @FXML
    public Tab licenseTab;

    @FXML
    public Tab ossLibrariesTab;

    @FXML
    public Label screenLabel;

    /*
     * The HostServices bean is registered with the context when the application starts
     * So this inspection detecting it as not found is actually a false positive.
     */
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    public AboutScreenController(EventBus eventBus, HostServices hostServices) {
        this.eventBus = eventBus;
        this.hostServices = hostServices;
        tabInsets = new Insets(20, 20, 20, 20);
    }

    @Override
    public void initialize() {
        VBox.setMargin(screenLabel, LayoutUtils.getHeaderLabelMarginInsets());
        setupDetailsTab();
        setupLicenseTab();
        setupOssLibrariesTab();
    }

    @Override
    protected void registerDefaultListeners() {

    }

    @Override
    public void initHostingStageName(String stageName) {
        PublishBreadcrumbEvent landingScreenBreadcrumb = new PublishBreadcrumbEvent("About", ApplicationSections.ABOUT, SceneSets.ABOUT_SCREEN, stageName, 1);
        eventBus.post(landingScreenBreadcrumb);
    }

    private void setupDetailsTab()
    {
        VBox container = new VBox(10);
        container.setPadding(LayoutUtils.getSectionElementVerticalSpacing());

        // License and Legal Disclaimer section
        Label licenseAndDisclaimerHeader = new Label("License Information and Legal Disclaimer");
        licenseAndDisclaimerHeader.getStyleClass().addAll(CssConstants.CLASS_TEXT_SUB_HEADER, CssConstants.CLASS_BOLD, CssConstants.CLASS_UNDERLINE);

        HBox licenseRow = new HBox(10);
        Image pfdGplV3Logo = new Image("/license/pfd_gplv3_logo.png");
        ImageView pfdGplV3LogoImageView = new ImageView(pfdGplV3Logo);
        licenseRow.getChildren().add(pfdGplV3LogoImageView);

        Label licenseLabel = new Label(TextConstants.COPYRIGHT_AND_LICENSE_NOTICES);
        licenseLabel.setWrapText(true);
        licenseRow.getChildren().add(licenseLabel);

        Label legalDisclaimerWarrantyLabel = new Label(TextConstants.LEGAL_DISCLAIMER_WARRANTY_NOTICES);
        legalDisclaimerWarrantyLabel.setWrapText(true);

        container.getChildren().addAll(licenseAndDisclaimerHeader, licenseRow, legalDisclaimerWarrantyLabel);

        // Source code section
        Label sourceCodeHeader = new Label("Application Information and Source Code");
        sourceCodeHeader.getStyleClass().addAll(CssConstants.CLASS_TEXT_SUB_HEADER, CssConstants.CLASS_BOLD, CssConstants.CLASS_UNDERLINE);
        String appInfoSourceCodeText = """
                Further information about the application and executables for the Linux (Debian) and Windows operating systems may be found on the website for the application located at pfd.iceflyer3.com.
                
                The full source code for the application is hosted on Bitbucket.
                
                Both the application website and the source code on Bitbucket may be accessed via the below links""";
        Label appInfoSourceCodeLabel = new Label(appInfoSourceCodeText);
        appInfoSourceCodeLabel.setWrapText(true);

        Hyperlink sourceCodeHyperlink = new Hyperlink("SOURCE CODE");
        sourceCodeHyperlink.getStyleClass().add(CssConstants.CLASS_TEXT_PRIMARY);
        sourceCodeHyperlink.setOnAction(event -> hostServices.showDocument("https://bitbucket.org/iceflyer3/personal-food-database-fx"));

        Hyperlink appWebsiteHyperlink = new Hyperlink("APPLICATION WEBSITE");
        appWebsiteHyperlink.getStyleClass().add(CssConstants.CLASS_TEXT_PRIMARY);
        appWebsiteHyperlink.setOnAction(event -> hostServices.showDocument("https://pfd.iceflyer3.com"));

        container.getChildren().addAll(sourceCodeHeader, appInfoSourceCodeLabel, sourceCodeHyperlink, appWebsiteHyperlink);

        // Iconography section
        Label iconographyHeader = new Label("Iconography");
        iconographyHeader.getStyleClass().addAll(CssConstants.CLASS_TEXT_SUB_HEADER, CssConstants.CLASS_BOLD, CssConstants.CLASS_UNDERLINE);
        Label iconographyLabel = new Label("Icons used in the application are provided by Font Awesome. You may learn more about them at the link below");
        Hyperlink fontAwesomeHyperlink = new Hyperlink("FONT AWESOME");
        fontAwesomeHyperlink.getStyleClass().add(CssConstants.CLASS_TEXT_PRIMARY);
        fontAwesomeHyperlink.setOnAction(event -> hostServices.showDocument("https://fontawesome.com/"));

        container.getChildren().addAll(iconographyHeader, iconographyLabel, fontAwesomeHyperlink);

        // Support section
        Label supportHeader = new Label("Support");
        supportHeader.getStyleClass().addAll(CssConstants.CLASS_TEXT_SUB_HEADER, CssConstants.CLASS_BOLD, CssConstants.CLASS_UNDERLINE);

        String supportText = """
                As previously mentioned above this program is free software as defined by the GPL v3. It wasn't created with the intention of making any money from it or attempting to sell it in any way. That is one of the primary reasons it is licensed under the GPL v3 as well as why executables are offered on the application's website.
                
                However, if you desire some way to be able to show support, you can buy me a coffee via Ko-Fi at the link below.""";
        Label supportLabel = new Label(supportText);
        supportLabel.setWrapText(true);

        Hyperlink kofiLink = new Hyperlink("KO-FI");
        kofiLink.getStyleClass().add(CssConstants.CLASS_TEXT_PRIMARY);
        kofiLink.setOnAction(event -> hostServices.showDocument("https://ko-fi.com/iceflyer3"));

        container.getChildren().addAll(supportHeader, supportLabel, kofiLink);

        detailsTab.setContent(container);
    }

    private void setupLicenseTab()
    {
        VBox container = new VBox(10);
        Label licenseLabel = new Label();

        // Load the text of the license from the file in the resources directory and display it in the label
        String fullLicenseText = getFullLicenseTextForLicense(Licenses.GPL_V3);
        licenseLabel.setText(fullLicenseText);
        licenseLabel.setWrapText(true);
        container.getChildren().add(licenseLabel);
        VBox.setMargin(licenseLabel, tabInsets);

        licenseTab.setContent(container);
    }

    private void setupOssLibrariesTab()
    {
        VBox container = new VBox(20);
        container.setPadding(LayoutUtils.getSectionElementVerticalSpacing());

        String introParagraph = "The Personal Food Database application is built using other open source software libraries. Below you will find each library that is used grouped by the license under which the libraries are released. You may view the complete license for any of the listed licenses by clicking on the license name.";
        Label headerParagraphLabel = new Label(introParagraph);
        headerParagraphLabel.setWrapText(true);
        container.getChildren().add(headerParagraphLabel);

        HBox masterDetailPane = new HBox(10);

        // Master detail right-hand side
        VBox fullLicenseTextPane = new VBox();
        fullLicenseTextPane.getStyleClass().add(CssConstants.CLASS_BORDER_WHITE);
        HBox.setHgrow(fullLicenseTextPane, Priority.ALWAYS);

        // Display the first license in the list by default
        String apacheLicenseFullText = getFullLicenseTextForLicense(Licenses.APACHE_V2);
        Label fullLicenseTextLabel = new Label(apacheLicenseFullText);
        fullLicenseTextLabel.setWrapText(true);
        fullLicenseTextPane.setAlignment(Pos.TOP_CENTER);
        fullLicenseTextPane.getChildren().add(fullLicenseTextLabel);

        // Master detail left-hand side
        VBox librariesUsedPane = new VBox(10);

        // Apache libraries
        Label apacheLabel = getLabelForLicense(Licenses.APACHE_V2, fullLicenseTextLabel);
        Label springLabel = new Label("Spring Framework");
        Label guavaLabel = new Label("Google Guava");
        Label calendarFxLabel = new Label("CalendarFX");
        Label jacksonLabel = new Label("Jackson");
        Label assertJLabel = new Label("AssertJ");
        Label testNgLabel = new Label("TestNG");
        librariesUsedPane.getChildren().addAll(apacheLabel, springLabel, guavaLabel, calendarFxLabel, jacksonLabel, assertJLabel, testNgLabel);

        // BSD 3 Clause libraries
        Label bsd3ClauseLabel = getLabelForLicense(Licenses.BSD_THREE_CLAUSE, fullLicenseTextLabel);
        Label controlsFxLabel = new Label("ControlsFX");
        librariesUsedPane.getChildren().addAll(bsd3ClauseLabel, controlsFxLabel);

        // GPL V2 libraries
        Label gplV2Label = getLabelForLicense(Licenses.GPL_V2, fullLicenseTextLabel);
        Label javaFxLabel = new Label("JavaFX");
        librariesUsedPane.getChildren().addAll(gplV2Label, javaFxLabel);

        // LGPL V2.1 libraries
        Label lgplV21Label = getLabelForLicense(Licenses.LGPL_V21, fullLicenseTextLabel);
        Label hibernateLabel = new Label("Hibernate ORM");
        Label logbackLabel = new Label("Logback");
        librariesUsedPane.getChildren().addAll(lgplV21Label, hibernateLabel, logbackLabel);

        // MPL V2 libraries
        Label mplV2Label = getLabelForLicense(Licenses.MPL_V2, fullLicenseTextLabel);
        Label h2Label = new Label("H2 Database");
        librariesUsedPane.getChildren().addAll(mplV2Label, h2Label);

        masterDetailPane.getChildren().addAll(librariesUsedPane, fullLicenseTextPane);
        container.getChildren().add(masterDetailPane);
        ossLibrariesTab.setContent(container);
    }

    /**
     * Generates a clickable label for a license that will show the full license text in the
     * license details pane on the right-hand side of the tab.
     * @param licenseName The name of the license to dispaly in the label and load upon click.
     * @param licenseTextLabel The label into which the full license text should be populated upon
     *                         a click on the label.
     * @return A new label with an OnMouseClicked handler attached
     */
    private Label getLabelForLicense(String licenseName, Label licenseTextLabel)
    {
        EventHandler<MouseEvent> labelClickHandler = mouseEvent -> {
            String fullLicenseText = getFullLicenseTextForLicense(licenseName);
            licenseTextLabel.setText(fullLicenseText);
        };

        Label label = ActionNodeBuilder.createLabel(licenseName)
                .embolden()
                .underline()
                .forActionType(PfdActionType.PRIMARY)
                .withAction(labelClickHandler)
                .build();
        label.getStyleClass().add(CssConstants.CLASS_TEXT_SUB_HEADER);
        label.setWrapText(true);
        return label;
    }

    private String getFullLicenseTextForLicense(String licenseName)
    {
        try
        {
            InputStream licenseStream;
            switch (licenseName)
            {
                case Licenses.APACHE_V2:
                    licenseStream = PfdApplication.class.getResourceAsStream("/license/apache_v2.txt");
                    break;
                case Licenses.BSD_THREE_CLAUSE:
                    licenseStream = PfdApplication.class.getResourceAsStream("/license/bsd_3_clause.txt");
                    break;
                case Licenses.GPL_V2:
                    licenseStream = PfdApplication.class.getResourceAsStream("/license/gpl_v2_with_classpath.txt");
                    break;
                case Licenses.GPL_V3:
                    licenseStream = PfdApplication.class.getResourceAsStream("/license/gpl_v3.txt");
                    break;
                case Licenses.LGPL_V21:
                    licenseStream = PfdApplication.class.getResourceAsStream("/license/lgpl_v2_1.txt");
                    break;
                case Licenses.MPL_V2:
                    licenseStream = PfdApplication.class.getResourceAsStream("/license/mpl_v2.txt");
                    break;
                default:
                    licenseStream = null;
            }

            if (licenseStream == null)
            {
                throw new InvalidApplicationStateException(String.format("Failed to load the license text file for %s", licenseName));
            }
            else
            {
                try(InputStreamReader inputStreamReader = new InputStreamReader(licenseStream))
                {
                    return CharStreams.toString(inputStreamReader);
                }
            }
        }
        catch (IOException e)
        {
            throw new InvalidApplicationStateException(String.format("Failed to read license text file for %s", licenseName), e);
        }
    }
}
