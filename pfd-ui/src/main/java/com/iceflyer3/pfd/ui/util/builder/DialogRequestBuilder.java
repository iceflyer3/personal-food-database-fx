package com.iceflyer3.pfd.ui.util.builder;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023, 2025 Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.ui.manager.DialogCallback;
import com.iceflyer3.pfd.ui.manager.OpenDialogRequest;
import javafx.scene.Parent;
import javafx.stage.Window;

import java.util.HashMap;
import java.util.Map;

/**
 * Builder that offers a fluent API for creating requests to open a new dialog window
 * @param <T> The return type of the dialog that will be opened
 */
public class DialogRequestBuilder<T>
{
    private final OpenDialogRequest<T> dialogRequest;
    private final Map<String, Object> dialogData;

    private DialogRequestBuilder(Window parentWindow, DialogCallback<T> callback) {
        // TODO: Refactor the request to remove the set from the constructor and introduce usage of the builder across the application
        dialogRequest = new OpenDialogRequest<>(parentWindow, null, callback);
        dialogData = new HashMap<>();
    }

    public static <T> DialogRequestBuilder<T> create(Window parentWindow, DialogCallback<T> callback)
    {
        return new DialogRequestBuilder<>(parentWindow, callback);
    }

    public DialogRequestBuilder<T> displayApplicationSet(String applicationSetName)
    {
        dialogRequest.setApplicationSetToDisplay(applicationSetName);
        return this;
    }

    public DialogRequestBuilder<T> displayNode(Parent rootNode)
    {
        dialogRequest.setNodeToDisplay(rootNode);
        return this;
    }

    public DialogRequestBuilder<T> withTitle(String dialogTitle)
    {
        dialogRequest.setDialogTitle(dialogTitle);
        return this;
    }

    public DialogRequestBuilder<T> withDialogDataItem(String key, Object value)
    {
        dialogData.put(key, value);
        return this;
    }

    public DialogRequestBuilder<T> width(double dialogWidth)
    {
        dialogRequest.setWidth(dialogWidth);
        return this;
    }

    public DialogRequestBuilder<T> height(double dialogHeight)
    {
        dialogRequest.setHeight(dialogHeight);
        return this;
    }

    public DialogRequestBuilder<T> closable(boolean isClosable)
    {
        dialogRequest.setClosable(isClosable);
        return this;
    }

    public DialogRequestBuilder<T> alwaysOnTop()
    {
        dialogRequest.setAlwaysOnTop(true);
        return this;
    }

    /**
     * Build the request to open a dialog with the specified options
     * @return The OpenDialogRequest instance used with WindowManager to open the dialog
     * @throws IllegalStateException If neither an application set nor a node were specified for display
     */
    public OpenDialogRequest<T> build() throws IllegalStateException
    {
        if (dialogRequest.getApplicationSetToDisplay() == null && dialogRequest.getNodeToDisplay() == null)
        {
            throw new IllegalStateException("You must specify either an application set or a node that should be displayed in the dialog window");
        }

        if (dialogRequest.getApplicationSetToDisplay() != null && dialogRequest.getNodeToDisplay() != null)
        {
            throw new IllegalStateException("You may not specify both an application set and a node at the same time. A dialog can only display one or the other.");
        }

        dialogRequest.getDialogData().putAll(dialogData);
        return dialogRequest;
    }
}
