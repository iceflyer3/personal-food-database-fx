/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.event;

import javafx.scene.Node;

public class ShowNotificationEvent {

    private final String text;
    private final String forStage;

    private Node graphic;
    private boolean closeButtonShown;
    // TODO: Add a duration setting here

    public ShowNotificationEvent(String notificationText, String forStage)
    {
        this.text = notificationText;
        this.forStage = forStage;
        this.closeButtonShown = true;
    }

    public String getForStage()
    {
        return forStage;
    }

    public String getText() {
        return text;
    }

    public Node getGraphic() {
        return graphic;
    }

    public void setGraphic(Node graphic) {
        this.graphic = graphic;
    }

    public boolean isCloseButtonShown() {
        return closeButtonShown;
    }

    public void setCloseButtonShown(boolean closeButtonShown) {
        this.closeButtonShown = closeButtonShown;
    }

    @Override
    public String toString() {
        return "ShowNotificationEvent{" +
                "text='" + text + '\'' +
                ", graphic=" + graphic +
                ", closeButtonShown=" + closeButtonShown +
                '}';
    }
}
