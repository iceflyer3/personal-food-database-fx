/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.event;

import com.iceflyer3.pfd.constants.SceneSets;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;

/**
 * Event that is fired when the set hosted in the ApplicationRootController needs to be changed.
 */
public class UserProfileSelectionEvent {
    private final String setToLoad;
    private final UserViewModel selectedUser;

    public UserProfileSelectionEvent(UserViewModel selectedUser)
    {
        this.selectedUser = selectedUser;
        this.setToLoad = SceneSets.HOME;
    }

    public UserViewModel getSelectedUser() {
        return selectedUser;
    }

    public String getSetToLoad() {
        return setToLoad;
    }

    @Override
    public String toString() {
        return "UserProfileSelectionEvent{" +
                "setToLoad='" + setToLoad + '\'' +
                ", selectedUser=" + selectedUser +
                '}';
    }
}
