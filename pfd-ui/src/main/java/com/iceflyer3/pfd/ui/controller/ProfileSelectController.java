/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.controller;

import com.google.common.eventbus.EventBus;
import com.iceflyer3.pfd.PfdApplication;
import com.iceflyer3.pfd.constants.FontAwesomeIcons;
import com.iceflyer3.pfd.constants.SceneSets;
import com.iceflyer3.pfd.data.factory.UserTaskFactory;
import com.iceflyer3.pfd.data.task.user.AddUserTask;
import com.iceflyer3.pfd.data.task.user.GetAllUsersTask;
import com.iceflyer3.pfd.ui.event.UserProfileSelectionEvent;
import com.iceflyer3.pfd.ui.manager.DialogCallback;
import com.iceflyer3.pfd.ui.manager.OpenDialogRequest;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.util.ScreenLoadEventUtils;
import com.iceflyer3.pfd.ui.util.builder.ActionNodeBuilder;
import com.iceflyer3.pfd.ui.util.builder.NotificationBuilder;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Hyperlink;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

@Component
public class ProfileSelectController extends AbstractTaskController {

    private static final Logger LOG = LoggerFactory.getLogger(ProfileSelectController.class);

    private final WindowManager windowManager;
    private final UserTaskFactory userTaskFactory;

    // If this is the first time the screen is being displayed upon application launch
    private boolean isInitialDisplay = true;
    private ObservableList<UserViewModel> users;

    @FXML
    private BorderPane root;

    @FXML
    private Hyperlink createNewProfileButton;

    public ProfileSelectController(EventBus eventBus,
                                   NotificationBuilder notificationBuilder,
                                   ScreenLoadEventUtils loadEventBuilder,
                                   WindowManager windowManager,
                                   UserTaskFactory userTaskFactory,
                                   TaskExecutor taskExecutor)
    {
        super(eventBus, notificationBuilder, loadEventBuilder, taskExecutor);
        this.windowManager = windowManager;
        this.userTaskFactory = userTaskFactory;
        this.users = FXCollections.observableArrayList();
    }

    @Override
    public void initialize()
    {
        LOG.debug("ProfileSelectController is initializing");

        GetAllUsersTask getUsersTask = userTaskFactory.getAllAsync();
        getUsersTask.setOnSucceeded(succeededEvent -> {
            LOG.debug("Successfully retrieved user list. Found {} users", getUsersTask.getValue().size());
            users = getUsersTask.getValue();
            refreshUI();
        });
        getUsersTask.setOnFailed(failedEvent -> {
            LOG.error("Failed to retrieve user list", getUsersTask.getException());
            notificationBuilder.create("An error has occurred while retrieving the list of users", PfdApplication.MAIN_STAGE_NAME).show();
        });

        if (isInitialDisplay)
        {
            root.sceneProperty().addListener(new ChangeListener<>()
            {
                @Override
                public void changed(ObservableValue<? extends Scene> observableValue, Scene oldScene, Scene newScene)
                {
                    if (newScene != null)
                    {
                        isInitialDisplay = false;
                        windowManager.openDisclaimerDialog(newScene.getWindow());

                        // Ensure the listener is removed after the scene has been set and the dialog opened.
                        root.sceneProperty().removeListener(this);
                    }
                }
            });
        }

        taskExecutor.execute(getUsersTask);
    }

    @Override
    protected void registerDefaultListeners()
    {
        this.createNewProfileButton.setOnAction(this::openNewProfileDialog);
    }

    private void refreshUI()
    {
        if (users.size() > 0)
        {
            LOG.debug("Found users: {}", users);
            // Step 1.) Create a button for each user found
            root.setCenter(null);

            TilePane buttonContainer = new TilePane(Orientation.HORIZONTAL);
            buttonContainer.setAlignment(Pos.CENTER);
            buttonContainer.setHgap(10);
            buttonContainer.setVgap(10);
            buttonContainer.setPadding(new Insets(50, 50, 50, 50));

            for(UserViewModel user : users)
            {
                EventHandler<ActionEvent> buttonAction = new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        UserProfileSelectionEvent profileSelectionEvent = new UserProfileSelectionEvent(user);
                        eventBus.post(profileSelectionEvent);
                    }
                };
                Button userButton = ActionNodeBuilder.createIconButton(user.getUsername(),12, FontAwesomeIcons.USER, 28)
                                                      .withIconLocation(ContentDisplay.TOP)
                                                      .withAction(buttonAction)
                                                      .build();

                // Ensure all buttons are the same size by allowing them to resize without limitation
                // This will allow a button to be the same size as it's pane in the TilePane
                userButton.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

                // Min width for short profile names
                userButton.setMinWidth(70.0);
                userButton.setMinHeight(70.0);

                buttonContainer.getChildren().add(userButton);
            }

            root.setCenter(buttonContainer);

            // Step 2.) Move the Create New Profile link to near the bottom of the screen
            createNewProfileButton.setAlignment(Pos.CENTER);

            VBox vBox = new VBox();
            vBox.setAlignment(Pos.BOTTOM_CENTER);
            vBox.getChildren().add(createNewProfileButton);
            root.setBottom(vBox);

            root.requestFocus();
        }
        else
        {
            // Step 1.) Place "Create Profile" link in center of hbox
            LOG.debug("Did not find any users");
        }
    }

    private void openNewProfileDialog(ActionEvent actionEvent) {
        DialogCallback<UserViewModel> callback = this::saveNewProfile;
        windowManager.openDialog(new OpenDialogRequest<>(root.getScene().getWindow(), SceneSets.DIALOG_NAME_PROMPT, callback));
    }

    private void saveNewProfile(UserViewModel newUser)
    {
        if (users.stream().map(user -> user.getUsername()).anyMatch(username -> username.toLowerCase().equals(newUser.getUsername().toLowerCase())))
        {
            notificationBuilder.create("A user with that name already exists. Please pick a different name", PfdApplication.MAIN_STAGE_NAME).show();
        }
        else
        {
            notificationBuilder.create("Saving new user...", PfdApplication.MAIN_STAGE_NAME).show();

            // TODO: This was done before the concept of the proxy had been thought of or implemented. But perhaps we should introduce a UserProxy if for no
            // other reason than for the sake of consistency. Then we could see about removing the dependency of the ViewModel module from the UI module
            AddUserTask addUserTask = userTaskFactory.addAsync(newUser);
            addUserTask.setOnSucceeded(event -> {
                users.add(addUserTask.getValue());
                notificationBuilder.create("The new user has been saved successfully!", PfdApplication.MAIN_STAGE_NAME).show();
                refreshUI();
            });
            addUserTask.setOnFailed(event -> {
                LOG.error("Failed to save user", addUserTask.getException());
                notificationBuilder.create("An error has occurred while saving the user", PfdApplication.MAIN_STAGE_NAME).show();
            });
            taskExecutor.execute(addUserTask);
        }
    }
}
