package com.iceflyer3.pfd.ui.controller.dialog;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.ui.controller.Savable;
import com.iceflyer3.pfd.ui.listener.Destroyable;
import com.iceflyer3.pfd.ui.listener.ObservableValueListenerBinding;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.MealPlanDayModelProxy;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.MealPlanModelProxy;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.MealPlanningProxyFactory;
import com.iceflyer3.pfd.ui.util.BindingUtils;
import com.iceflyer3.pfd.ui.listener.ModelPropertyValidationEventConfig;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CreateMealPlanDayDialogController extends AbstractDialogController<MealPlanDayModelProxy> implements Savable, Destroyable
{
    private final MealPlanningProxyFactory mealPlanningProxyFactory;

    private MealPlanModelProxy forMealPlanProxy;
    private MealPlanDayModelProxy mealPlanDayProxy;

    private ObservableValueListenerBinding<String> dayNameBinding;

    @FXML
    public TextField dayNameInput;

    @FXML
    public Button saveButton;

    @FXML
    public Hyperlink cancelButton;

    public CreateMealPlanDayDialogController(WindowManager windowManager, MealPlanningProxyFactory mealPlanningProxyFactory)
    {
        super(windowManager);
        this.mealPlanningProxyFactory = mealPlanningProxyFactory;
    }

    @Override
    public void initialize() { }

    @Override
    protected void registerDefaultListeners()
    {
        saveButton.setOnAction(this::save);
        cancelButton.setOnAction(this::cancel);
    }

    @Override
    public void initControllerData(Map<String, Object> dialogData)
    {
        this.forMealPlanProxy = (MealPlanModelProxy) dialogData.get(MealPlanModelProxy.class.getName());
        mealPlanningProxyFactory.getMealPlanDayById(null, forMealPlanProxy, (wasSuccessful, proxyResult) ->
        {
            if (wasSuccessful)
            {
                mealPlanDayProxy = proxyResult;
                mealPlanDayProxy.setFitnessGoal(forMealPlanProxy.getFitnessGoal());

                Bindings.bindBidirectional(mealPlanDayProxy.dayLabelProperty(), dayNameInput.textProperty());
                dayNameBinding = BindingUtils.bindModelObservableValueToGuiControl(new ModelPropertyValidationEventConfig<>(mealPlanDayProxy, mealPlanDayProxy.dayLabelProperty(), dayNameInput, this::updateSaveValidity));

                updateSaveValidity();
            }
            else
            {
                // Theoretically this should never actually happen
                windowManager.openFatalErrorDialog();
            }
        });
    }

    @Override
    public void updateSaveValidity()
    {
        saveButton.setDisable(!mealPlanDayProxy.validate().wasSuccessful());
    }

    @Override
    public void save(ActionEvent event)
    {
        callback.complete(mealPlanDayProxy);
        windowManager.close(WindowManager.DIALOG_WINDOW);
    }

    private void cancel(ActionEvent event)
    {
        windowManager.close(WindowManager.DIALOG_WINDOW);
    }

    @Override
    public void destroy()
    {
        dayNameBinding.unbind();
    }
}
