package com.iceflyer3.pfd.ui.manager;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023, 2025  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import javafx.scene.Parent;
import javafx.stage.Window;

import java.util.HashMap;
import java.util.Map;

/**
 * Encapsulates the required information to be able to open a dialog
 * using the Window Manager.
 * @param <T> The type that is returned by the dialog
 */
public class OpenDialogRequest<T> {

    private final Window parentWindow;
    private final DialogCallback<T> callback;
    private final Map<String, Object> dialogData;

    private String applicationSetToDisplay;

    private Parent nodeToDisplay;
    private String dialogTitle;
    private double width;
    private double height;
    private boolean isClosable;
    private boolean isAlwaysOnTop;

    public OpenDialogRequest(Window parentWindow, String applicationSetToDisplay, DialogCallback<T> callback)
    {
        this.parentWindow = parentWindow;
        this.applicationSetToDisplay = applicationSetToDisplay;
        this.callback = callback;
        this.width = -1;
        this.height = -1;
        this.isClosable = true;
        this.dialogData = new HashMap<>();
    }

    public Window getParentWindow() {
        return parentWindow;
    }

    public DialogCallback<T> getCallback() {
        return callback;
    }

    public String getDialogSet() {
        return applicationSetToDisplay;
    }

    public void setApplicationSetToDisplay(String applicationSetToDisplay)
    {
        this.applicationSetToDisplay = applicationSetToDisplay;
    }

    public String getApplicationSetToDisplay()
    {
        return applicationSetToDisplay;
    }

    public Parent getNodeToDisplay()
    {
        return nodeToDisplay;
    }

    public void setNodeToDisplay(Parent nodeToDisplay)
    {
        this.nodeToDisplay = nodeToDisplay;
    }

    public String getDialogTitle() {
        return dialogTitle;
    }

    public void setDialogTitle(String dialogTitle) {
        this.dialogTitle = dialogTitle;
    }

    /**
     * TODO: Maybe re-evaluate the use of class name here. On a subsequent visit to this it works okay-ish but I'm not really sure that
     *       I still like this approach. At the moment I kinda like the idea of using more descriptive key names as opposed to just the
     *       class name. Though I'm not sure exactly how this would work.
     *
     * Retrieve the current map of data for the dialog. Also used to facilitate adding new data to the map via the returned map.
     *
     * @return A Map containing the data for the dialog. The String key here is the name of the class as acquired by calling Class.getName
     *         and the value is the instance of that object that is being passed into the controller.
     */
    public Map<String, Object> getDialogData()
    {
        return dialogData;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public boolean isClosable()
    {
        return isClosable;
    }

    public void setClosable(boolean closable)
    {
        isClosable = closable;
    }

    public boolean isAlwaysOnTop()
    {
        return isAlwaysOnTop;
    }

    public void setAlwaysOnTop(boolean alwaysOnTop)
    {
        isAlwaysOnTop = alwaysOnTop;
    }
}
