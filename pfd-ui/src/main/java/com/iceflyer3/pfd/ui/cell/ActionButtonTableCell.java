package com.iceflyer3.pfd.ui.cell;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.ui.util.CellUtils;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.layout.HBox;

public class ActionButtonTableCell<T, S> extends TableCell<T, S> {

    private final ActionButtonProvider<T> buttonProvider;

    public ActionButtonTableCell(ActionButtonProvider<T> buttonProvider)
    {
        this.setEditable(false);
        this.setAlignment(Pos.CENTER);

        this.buttonProvider = buttonProvider;
    }

    @Override
    public void updateItem(S item, boolean empty)
    {
        super.updateItem(item, empty);

        if (empty || item == null)
        {
            this.setText(null);
            this.setGraphic(null);
        }
        else
        {
            if (this.getTableRow() != null)
            {
                T rowItem = this.getTableRow().getItem();
                if (rowItem != null)
                {
                    HBox buttonContainer = CellUtils.getButtonsForButtonProvider(buttonProvider, rowItem);
                    this.setGraphic(buttonContainer);
                }
            }
        }
    }
}
