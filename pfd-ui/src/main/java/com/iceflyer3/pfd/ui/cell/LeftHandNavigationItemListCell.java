package com.iceflyer3.pfd.ui.cell;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.constants.ApplicationSections;
import com.iceflyer3.pfd.constants.FontAwesomeIcons;
import com.iceflyer3.pfd.ui.enums.PfdActionType;
import com.iceflyer3.pfd.ui.manager.OpenPopoutRequest;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.util.builder.ActionNodeBuilder;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;

/**
 * A cell that represents one of the options in the ListView that powers the main navigation bar
 * found on the left hand side of the screen
 */
public class LeftHandNavigationItemListCell extends ListCell<String> {

    private final UserViewModel user;
    private final WindowManager windowManager;

    private HBox cellContainer;

    public LeftHandNavigationItemListCell(WindowManager windowManager, UserViewModel user)
    {
        this.user = user;
        this.windowManager = windowManager;
    }

    protected void updateItem(String item, boolean empty)
    {
        super.updateItem(item, empty);

        if (empty || item == null)
        {
            // According to the docs this MUST happen when overriding this method or weirdness will ensue
            // https://openjfx.io/javadoc/13/javafx.controls/javafx/scene/control/Cell.html#updateItem(T,boolean)
            this.setText(null);
            this.setGraphic(null);

            /*
             * Be sure to null out the container for a cell that previously wasn't empty that
             * now is or it won't get re-drawn properly the next time that it is not empty again.
             */
            this.cellContainer = null;
        }
        else
        {
            if (cellContainer == null)
            {
                cellContainer = new HBox(10);

                // Section name label
                Label sectionLabel = new Label(item);

                // Pop-out section button
                Button popOutButton = ActionNodeBuilder.createIconButton(null, FontAwesomeIcons.EXTERNAL_LINK, 10)
                        .forActionType(PfdActionType.SECONDARY)
                        .withAction(event -> {
                            switch(item)
                            {
                                case "Food Database":
                                    windowManager.openPopout(new OpenPopoutRequest(ApplicationSections.FOOD_DATABASE, user));
                                    break;
                                case "Recipes":
                                    windowManager.openPopout(new OpenPopoutRequest(ApplicationSections.RECIPES, user));
                                    break;
                                case "Meal Planning":
                                    windowManager.openPopout(new OpenPopoutRequest(ApplicationSections.MEAL_PLANNING, user));
                                    break;
                                case "Settings":
                                    windowManager.openPopout(new OpenPopoutRequest(ApplicationSections.SETTINGS, user));
                                    break;
                                case "About":
                                    windowManager.openPopout(new OpenPopoutRequest(ApplicationSections.ABOUT, user));
                                    break;
                            }
                        })
                        .build();

                // Force button size. Pref wasn't being obeyed so we'll just clamp min/max size to the pref size.
                // This button should never change in size anyways even when the window is at its smallest possible size.
                popOutButton.setPrefSize(30, 30);
                popOutButton.setMaxSize(Control.USE_PREF_SIZE, Control.USE_PREF_SIZE);
                popOutButton.setMinSize(Control.USE_PREF_SIZE, Control.USE_PREF_SIZE);

                // Add the label and the button to our container
                cellContainer.getChildren().addAll(sectionLabel, popOutButton);
                this.setGraphic(cellContainer);
                this.setText(null);
            }
        }
    }
}
