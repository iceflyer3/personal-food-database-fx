package com.iceflyer3.pfd.ui.controller.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.google.common.eventbus.EventBus;
import com.iceflyer3.pfd.constants.ApplicationSections;
import com.iceflyer3.pfd.constants.FontAwesomeIcons;
import com.iceflyer3.pfd.constants.SceneSets;
import com.iceflyer3.pfd.enums.Nutrient;
import com.iceflyer3.pfd.ui.controller.AbstractTaskController;
import com.iceflyer3.pfd.ui.controller.HostStageAware;
import com.iceflyer3.pfd.ui.controller.dialog.CreateMealPlanMealDialogController;
import com.iceflyer3.pfd.ui.controller.sections.MacronutrientBreakdownScreenSection;
import com.iceflyer3.pfd.ui.controller.sections.MicronutrientBreakdownScreenSection;
import com.iceflyer3.pfd.ui.controller.sections.ScreenSection;
import com.iceflyer3.pfd.ui.controller.sections.mealplanning.MealPlanCustomNutrientRequirementsScreenSection;
import com.iceflyer3.pfd.ui.event.PublishBreadcrumbEvent;
import com.iceflyer3.pfd.ui.event.ScreenNavigationEvent;
import com.iceflyer3.pfd.ui.manager.OpenDialogRequest;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.MealPlanDayModelProxy;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.MealPlanMealModelProxy;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.MealPlanModelProxy;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.MealPlanningProxyFactory;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.util.LayoutUtils;
import com.iceflyer3.pfd.ui.util.ScreenLoadEventUtils;
import com.iceflyer3.pfd.ui.util.builder.ActionNodeBuilder;
import com.iceflyer3.pfd.ui.util.builder.NotificationBuilder;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Map;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ComposeMealPlanDayController extends AbstractTaskController implements HostStageAware {

    private final WindowManager windowManager;
    private final MealPlanningProxyFactory mealPlanningProxyFactory;
    private final ObservableList<MealPlanMealModelProxy> mealsForDay;

    // These should never change but can't be final because they aren't assigned
    // until the initControllerData method is called during controller initialization
    private UserViewModel user;
    private MealPlanDayModelProxy dayModelProxy;
    private MealPlanModelProxy mealPlan;


    private final StringProperty dayCaloriesGrandTotalProperty;
    private final StringProperty dayProteinGrandTotalProperty;
    private final StringProperty dayCarbsGrandTotalProperty;
    private final StringProperty dayTotalFatsGrandTotalProperty;

    private String hostingStageName;

    @FXML
    public VBox rootContainer;

    @FXML
    public Label screenLabel;

    @FXML
    public Label dayLabelHeader;

    @FXML
    public Label dailyCaloriesLabel;

    @FXML
    public Label dailyProteinLabel;

    @FXML
    public Label dailyCarbsLabel;

    @FXML
    public Label dailyFatsLabel;

    @FXML
    public Label fitnessGoalLabel;

    @FXML
    public VBox mealSectionContainer;

    public ComposeMealPlanDayController(EventBus eventBus,
                                        NotificationBuilder notificationBuilder,
                                        ScreenLoadEventUtils loadEventBuilder,
                                        TaskExecutor taskExecutor,
                                        WindowManager windowManager,
                                        MealPlanningProxyFactory mealPlanningProxyFactory) {
        super(eventBus, notificationBuilder, loadEventBuilder, taskExecutor);
        this.windowManager = windowManager;
        this.mealPlanningProxyFactory = mealPlanningProxyFactory;
        this.mealsForDay = FXCollections.observableArrayList();

        this.dayCaloriesGrandTotalProperty = new SimpleStringProperty();
        this.dayProteinGrandTotalProperty = new SimpleStringProperty();
        this.dayCarbsGrandTotalProperty = new SimpleStringProperty();
        this.dayTotalFatsGrandTotalProperty = new SimpleStringProperty();
    }

    @Override
    public void initialize() {
        VBox.setMargin(screenLabel, LayoutUtils.getHeaderLabelMarginInsets());
    }

    @Override
    protected void registerDefaultListeners() {

    }

    @Override
    public void initHostingStageName(String stageName) {
        hostingStageName = stageName;
    }

    @Override
    public void initControllerData(Map<String, Object> controllerData) {

        this.user = (UserViewModel) controllerData.get(UserViewModel.class.getName());
        this.dayModelProxy = (MealPlanDayModelProxy) controllerData.get(MealPlanDayModelProxy.class.getName());
        this.mealPlan = (MealPlanModelProxy) controllerData.get(MealPlanModelProxy.class.getName());

        PublishBreadcrumbEvent breadcrumb = new PublishBreadcrumbEvent("Compose Meal Plan Day", ApplicationSections.MEAL_PLANNING, SceneSets.MEAL_PLANNING_COMPOSE_DAY, hostingStageName, 2);
        breadcrumb.addData(MealPlanModelProxy.class.getName(), mealPlan);
        breadcrumb.addData(MealPlanDayModelProxy.class.getName(), dayModelProxy);
        eventBus.post(breadcrumb);

        initOverviewSection();
        initMealsSection();

        ScreenSection customNutrientsSection = new MealPlanCustomNutrientRequirementsScreenSection(mealPlan, dayModelProxy);
        ScreenSection macroBreakdownSection = new MacronutrientBreakdownScreenSection<>(dayModelProxy);
        ScreenSection microBreakdownSection = new MicronutrientBreakdownScreenSection<>(dayModelProxy);
        rootContainer.getChildren().add(customNutrientsSection.create());
        rootContainer.getChildren().add(macroBreakdownSection.create());
        rootContainer.getChildren().add(microBreakdownSection.create());
    }

    private void initOverviewSection()
    {
        dayLabelHeader.setText(dayModelProxy.getDayLabel());

        dailyCaloriesLabel.textProperty().bind(dayCaloriesGrandTotalProperty);
        dailyProteinLabel.textProperty().bind(dayProteinGrandTotalProperty);
        dailyCarbsLabel.textProperty().bind(dayCarbsGrandTotalProperty);
        dailyFatsLabel.textProperty().bind(dayTotalFatsGrandTotalProperty);
    }

    private void initMealsSection()
    {
        Button addMealButton = ActionNodeBuilder.createIconButton("ADD MEAL", FontAwesomeIcons.PLUS)
                                              .withAction(this::openNewMealDialog)
                                              .build();

        mealSectionContainer.getChildren().add(0, addMealButton);
        mealSectionContainer.setPadding(new Insets(10, 10, 10, 10));

        refreshMealsForDay();
        initMealsForDayTableview();
    }

    private void initMealsForDayTableview()
    {
        TableView<MealPlanMealModelProxy> mealsTable = new TableView<>();
        mealsTable.setEditable(true);
        mealsTable.setPlaceholder(new Label("You have not yet created any meals"));

        TableColumn<MealPlanMealModelProxy, String> mealNameColumn = new TableColumn<>("Name");
        mealNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));

        TableColumn<MealPlanMealModelProxy, BigDecimal> requiredCaloriesColumn = new TableColumn<>("Calories");
        requiredCaloriesColumn.setCellValueFactory(cellDataFeatures -> cellDataFeatures.getValue().caloriesProperty());

        TableColumn<MealPlanMealModelProxy, BigDecimal> requiredProteinColumn = new TableColumn<>("Protein");
        requiredProteinColumn.setCellValueFactory(cellDataFeatures -> cellDataFeatures.getValue().proteinProperty());

        TableColumn<MealPlanMealModelProxy, BigDecimal> requiredCarbsColumn = new TableColumn<>("Carbs");
        requiredCarbsColumn.setCellValueFactory(cellDataFeatures -> cellDataFeatures.getValue().carbohydratesProperty());

        TableColumn<MealPlanMealModelProxy, BigDecimal> requiredFatsColumn = new TableColumn<>("Total Fats");
        requiredFatsColumn.setCellValueFactory(cellDataFeatures -> cellDataFeatures.getValue().totalFatsProperty());

        mealsTable.getColumns().addAll(
                mealNameColumn,
                requiredCaloriesColumn,
                requiredProteinColumn,
                requiredCarbsColumn,
                requiredFatsColumn
        );

        mealsTable.setContextMenu(createContextMenuForMealsTable(mealsTable));
        mealsTable.setItems(mealsForDay);
        mealSectionContainer.getChildren().add(mealsTable);
    }

    private ContextMenu createContextMenuForMealsTable(TableView<MealPlanMealModelProxy> mealsTable)
    {
        EventHandler<ActionEvent> viewActionHandler = event -> {
            MealPlanMealModelProxy selectedMeal = mealsTable.getSelectionModel().getSelectedItem();
            ScreenNavigationEvent navigationEvent = new ScreenNavigationEvent(ApplicationSections.MEAL_PLANNING, SceneSets.MEAL_PLANNING_VIEW_MEAL, hostingStageName, user);
            navigationEvent.getControllerData().put(ViewMealPlanMealController.DATA_MEAL_ID, selectedMeal.getId());
            eventBus.post(navigationEvent);
        };

        EventHandler<ActionEvent> editActionHandler = event -> {
            MealPlanMealModelProxy selectedMeal = mealsTable.getSelectionModel().getSelectedItem();
            ScreenNavigationEvent navigationEvent = new ScreenNavigationEvent(ApplicationSections.MEAL_PLANNING, SceneSets.MEAL_PLANNING_COMPOSE_MEAL, hostingStageName, user);
            navigationEvent.getControllerData().put(MealPlanMealModelProxy.class.getName(), selectedMeal);
            eventBus.post(navigationEvent);
        };

        EventHandler<ActionEvent> deleteActionHandler = event -> {
            windowManager.openConfirmationDialog(rootContainer.getScene().getWindow(), () -> {
                MealPlanMealModelProxy selectedMeal = mealsTable.getSelectionModel().getSelectedItem();
                selectedMeal.delete((wasSuccessful, resultMessage) -> {

                    notificationBuilder.create(resultMessage, hostingStageName).show();

                    if (wasSuccessful)
                    {
                        refreshMealsForDay();
                    }
                });
            });
        };

        return LayoutUtils.getDefaultContextMenu(viewActionHandler, editActionHandler, deleteActionHandler);
    }

    private void openNewMealDialog(ActionEvent event)
    {
        OpenDialogRequest<MealPlanMealModelProxy> newMealDialogRequest = new OpenDialogRequest<>(rootContainer.getScene().getWindow(), SceneSets.DIALOG_CREATE_MEAL_PLAN_MEAL, (newMeal) ->
        {
            newMeal.save((wasSuccessful, resultMessage) -> {

                notificationBuilder.create(resultMessage, hostingStageName).show();

                if (wasSuccessful)
                {
                    refreshMealsForDay();
                }
            });
        });
        newMealDialogRequest.setDialogTitle("Create New Meal");
        newMealDialogRequest.getDialogData().put(CreateMealPlanMealDialogController.DATA_FOR_DAY, dayModelProxy);
        newMealDialogRequest.getDialogData().put(CreateMealPlanMealDialogController.DATA_MEAL_PLAN, mealPlan);

        windowManager.openDialog(newMealDialogRequest);
    }

    private void refreshMealsForDay()
    {
        loadEventBuilder.emitBegin("Searching for meal planning day details", hostingStageName);
        mealPlanningProxyFactory.getMealsForDay(mealPlan, dayModelProxy, (wasSuccessful, mealsForDay) -> {
            if (wasSuccessful)
            {
                this.mealsForDay.clear();

                if (mealsForDay.size() > 0)
                {
                    this.mealsForDay.addAll(mealsForDay);
                }

                // Ensure that the day and the UI bound properties have been refreshed with the latest data from the new meal list
                BigDecimal suggestedProtein = mealPlan.getNutrientSuggestedAmount(Nutrient.PROTEIN);
                BigDecimal suggestedCarbs = mealPlan.getNutrientSuggestedAmount(Nutrient.CARBOHYDRATES);
                BigDecimal suggestedFats = mealPlan.getNutrientSuggestedAmount(Nutrient.TOTAL_FATS);

                dayModelProxy.refreshNutritionDetails(mealsForDay);
                dayCaloriesGrandTotalProperty.set(String.format("%s / %s", dayModelProxy.getCalories(), mealPlan.getDailyCalories()));
                dayProteinGrandTotalProperty.set(String.format("%s / %s", dayModelProxy.getProtein(), suggestedProtein));
                dayCarbsGrandTotalProperty.set(String.format("%s / %s", dayModelProxy.getCarbohydrates(), suggestedCarbs));
                dayTotalFatsGrandTotalProperty.set(String.format("%s / %s", dayModelProxy.getTotalFats(), suggestedFats));
            }
            else
            {
                notificationBuilder.create("Could not load the meals for the day!", hostingStageName).show();
            }
            loadEventBuilder.emitCompleted(hostingStageName);
        });
    }
}
