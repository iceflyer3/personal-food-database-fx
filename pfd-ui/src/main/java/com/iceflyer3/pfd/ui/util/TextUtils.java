package com.iceflyer3.pfd.ui.util;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.ui.format.RequiredNumberTextFormatter;
import com.iceflyer3.pfd.ui.format.OptionalNumberTextFormatter;
import javafx.scene.control.TextFormatter;
import javafx.util.converter.BigDecimalStringConverter;
import javafx.util.converter.IntegerStringConverter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class TextUtils {
    public static TextFormatter<BigDecimal> getRequiredDecimalTextInputFormatter()
    {
        return new TextFormatter<>(new BigDecimalStringConverter(), new BigDecimal(0), new RequiredNumberTextFormatter(true));
    }

    public static TextFormatter<BigDecimal> getOptionalDecimalTextInputFormatter()
    {
        return new TextFormatter<>(new BigDecimalStringConverter(), new BigDecimal(0), new OptionalNumberTextFormatter(true));
    }

    public static TextFormatter<Integer> getRequiredIntegerTextInputFormatter()
    {
        return new TextFormatter<>(new IntegerStringConverter(), 0, new OptionalNumberTextFormatter(false));
    }

    public static TextFormatter<Integer> getOptionalIntegerTextInputFormatter()
    {
        return new TextFormatter<>(new IntegerStringConverter(), 0, new OptionalNumberTextFormatter(false));
    }

    /**
     * Formats the value of a LocalDateInstance as a string in the format [date] at [hour]:[minute]
     * @param localDateTime The LocalDateTime to derive the date string from
     * @return A string in the format [date] at [hour]:[minute]
     */
    public static String getDateTimeString(LocalDateTime localDateTime)
    {
        LocalDate date = localDateTime.toLocalDate();
        LocalTime time = localDateTime.toLocalTime();

        // Zero pad the front of single digit values for both hours and minutes
        // TODO: We could eventually add a setting to display 24-hour time or not and have this handle AM / PM (if hour > 12 -> hour - 12) conversion
        //       By default time.getHour() returns 24 hour time.
        String hourString = time.getHour() < 10 ? String.format("0%s", time.getHour()) : String.valueOf(time.getHour());
        String minuteString = time.getMinute() < 10 ? String.format("0%s", time.getMinute()) : String.valueOf(time.getMinute());

        return String.format("%s at %s:%s", date.toString(), hourString, minuteString);
    }
}
