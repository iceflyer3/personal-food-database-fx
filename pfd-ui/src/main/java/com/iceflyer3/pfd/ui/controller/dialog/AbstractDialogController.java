/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.controller.dialog;

import com.iceflyer3.pfd.ui.controller.AbstractController;
import com.iceflyer3.pfd.ui.manager.DialogCallback;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Base controller for dialog controllers.
 * @param <T> The type of the data returned by the dialog
 */
public abstract class AbstractDialogController<T> extends AbstractController {

    protected static final Logger LOG = LoggerFactory.getLogger(AbstractDialogController.class);

    protected WindowManager windowManager;
    protected DialogCallback<T> callback;

    public AbstractDialogController(WindowManager windowManager)
    {
        this.windowManager = windowManager;
    }

    public void setCallback(DialogCallback<T> callback)
    {
        this.callback = callback;
    }
}
