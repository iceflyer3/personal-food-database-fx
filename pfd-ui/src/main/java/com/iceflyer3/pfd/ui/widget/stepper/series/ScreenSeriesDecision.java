package com.iceflyer3.pfd.ui.widget.stepper.series;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023, 2025 Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import java.util.Set;

/**
 * Callback interface for use by a screen series widget. Allows a screen in the series to indicate what screen should
 * be advanced to next after the screen has indicated it is ready to advance.
 *
 * The client that created the screen series widget will provide an implementation of this interface and associate it
 * with a particular screen during registration of the screen with the screen series widget.
 */
@FunctionalInterface
public interface ScreenSeriesDecision
{
    ScreenSeriesScreen makeScreenChoice(Set<ScreenSeriesScreen> availableScreens, Object screenResult);
}
