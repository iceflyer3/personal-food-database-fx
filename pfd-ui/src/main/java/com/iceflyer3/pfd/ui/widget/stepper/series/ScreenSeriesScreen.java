package com.iceflyer3.pfd.ui.widget.stepper.series;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023, 2025 Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ScreenSeriesScreen
{
    private final Class<?> screenComponentClass;
    private final ScreenSeriesDecision navigationDecision;
    private final Map<String, Object> componentInitData;
    private boolean isInitial;
    private boolean isFinal;

    public ScreenSeriesScreen(Class<?> screenComponentClass, ScreenSeriesDecision navigationDecision)
    {
        this.screenComponentClass = screenComponentClass;
        this.navigationDecision = navigationDecision;
        this.componentInitData = new HashMap<>();
        this.isInitial = false;
        this.isFinal = false;
    }

    public Class<?> getScreenComponentClass()
    {
        return screenComponentClass;
    }

    public ScreenSeriesDecision getNavigationDecision()
    {
        return navigationDecision;
    }

    public void setComponentInitData(Map<String, Object> componentInitData)
    {
        this.componentInitData.putAll(componentInitData);
    }

    public Map<String, Object> getComponentInitData()
    {
        return Collections.unmodifiableMap(componentInitData);
    }

    public boolean isInitial()
    {
        return isInitial;
    }

    public void setInitial(boolean isInitial)
    {
        this.isInitial = isInitial;
    }

    public boolean isFinal()
    {
        return isFinal;
    }

    public void setFinal(boolean isFinal)
    {
        this.isFinal = isFinal;
    }

    public boolean hasInitData()
    {
        return this.getComponentInitData() != null;
    }
}
