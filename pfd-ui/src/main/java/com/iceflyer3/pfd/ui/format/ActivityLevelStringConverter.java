package com.iceflyer3.pfd.ui.format;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.enums.ActivityLevel;
import javafx.util.StringConverter;

public class ActivityLevelStringConverter extends StringConverter<ActivityLevel> {
    @Override
    public String toString(ActivityLevel activityLevel) {
        return activityLevel.getName();
    }

    @Override
    public ActivityLevel fromString(String string) {
        return ActivityLevel.valueOf(string);
    }
}
