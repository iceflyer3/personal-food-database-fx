/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.controller;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.iceflyer3.pfd.constants.ApplicationSections;
import com.iceflyer3.pfd.constants.SceneSets;
import com.iceflyer3.pfd.ui.Breadcrumb;
import com.iceflyer3.pfd.ui.event.PublishBreadcrumbEvent;
import com.iceflyer3.pfd.ui.event.ScreenLoadBeginEvent;
import com.iceflyer3.pfd.ui.event.ScreenLoadCompletedEvent;
import com.iceflyer3.pfd.ui.event.ScreenNavigationEvent;
import com.iceflyer3.pfd.ui.listener.Destroyable;
import com.iceflyer3.pfd.ui.manager.BreadcrumbManager;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.util.ControllerUtils;
import com.iceflyer3.pfd.ui.util.FxmlLoadResult;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.util.Timer;
import java.util.TimerTask;

/**
 * A "home" controller is a controller that effectively serves as a container to host the rest of
 * the application that is accessible once past the "User Profile" screen where a user profile is
 * selected.
 *
 * This base class contains event handlers for the events that any "home" controller will need to
 * be able to handle. Examples include events that request changes of the screen that is currently
 * displayed, or to show a notification or a dialog to the user.
 *
 * The HomeScreenController is the main "home" controller that always exists. But there will be an
 * additional one for each popout window that is open.
 */
public abstract class AbstractHomeController extends AbstractController
{
    private static final Logger LOG = LoggerFactory.getLogger(AbstractHomeController.class);


    protected final EventBus eventBus;
    protected final BreadcrumbManager breadcrumbManager;
    protected final WindowManager windowManager;
    protected final ControllerUtils controllerUtils;
    protected final ObservableList<Breadcrumb> breadcrumbs;

    // The name of the window (stage) in which this popout is shown
    // Used to determine if breadcrumb events belong to this stage or not
    protected String hostingStageName;
    protected Stage hostingStage;

    protected StackPane rootContainer;
    protected AbstractController loadedController;
    protected Timer timer;

    protected UserViewModel user;

    public AbstractHomeController(
            EventBus eventBus,
            BreadcrumbManager breadcrumbManager,
            WindowManager windowManager,
            ControllerUtils controllerUtils)
    {
        this.eventBus = eventBus;
        this.breadcrumbManager = breadcrumbManager;
        this.windowManager = windowManager;
        this.controllerUtils = controllerUtils;

        this.breadcrumbs = FXCollections.observableArrayList();

        eventBus.register(this);
        LOG.debug("AbstractHomeController constructor has completed!");
    }

    /**
     * Initializes the currently loaded user property of the controller
     *
     * This should be called before initControllerData.
     *
     * @param user The user that was selected at the "User Profile" screen
     */
    public abstract void initUser(UserViewModel user);

    /**
     * Initializes the stage that the concrete implementation is hosted in as well as defining the
     * name for that stage.
     *
     * This stage is the stage that will be the parent of any dialog windows that are opened from
     * this home controller.
     *
     * This should be called before initControllerData.
     *
     * @param stage The stage the controller is displayed in
     * @param stageName The name of the stage
     */
    public void initHostingStage(Stage stage, String stageName)
    {
        LOG.debug("Initializing hosting stage with stage {} and name {}", stage, stageName);
        hostingStage = stage;
        hostingStageName = stageName;
        timer = new Timer(String.format("%s-timer", hostingStageName));
    }

    /**
     * Sets the root container new screens will be loaded into upon receiving ScreenNavigationEvents.
     *
     * @param rootContainer The root container for this home controller into which the root node for
     *                      new screens will be populated.
     */
    protected void initRootContainer(StackPane rootContainer)
    {
        this.rootContainer = rootContainer;
    }


    @Subscribe
    protected void changeApplicationScreen(ScreenNavigationEvent screenNavigationEvent)
    {
        if (screenNavigationEvent.getForStage().equals(hostingStageName))
        {
            LOG.debug("Received new screen navigation event! Event: {}", screenNavigationEvent);

            // Load the screen
            String sectionSetToLoad;
            if (screenNavigationEvent.getApplicationSection().equals(ApplicationSections.HOME))
            {
                sectionSetToLoad = screenNavigationEvent.getSetToLoad();
            }
            else
            {
                sectionSetToLoad = String.format("%s/%s", screenNavigationEvent.getApplicationSection(), screenNavigationEvent.getSetToLoad());
            }

            /*
             * If there is a previously loaded controller and it is destroyable then be sure to
             * destroy it
             *
             * Theoretically we shouldn't really need to do this. At the time of writing destroy()
             * for application section controllers is mostly concerned with making sure listeners
             * registered on observable values are unregistered in an effort to avoid any potential
             * memory leaks.
             *
             * However, the controllers are at the root of the reference hierarchy. So, assuming I
             * understand gc correctly, once the controller is available for gc then all the
             * references that only it owns should also be available to gc. Meaning the observable
             * values and subsequently the listeners registered on them should be subject to gc even
             * without un-registration.
             *
             * But this can be tricky to try and reason about so we're gonna play it safe and do manual
             * un-registration of listeners before we release the reference to the controller anyway.
             */
            if (loadedController != null && loadedController instanceof Destroyable destroyableController)
            {
                destroyableController.destroy();
            }

            FxmlLoadResult<AbstractController> loadResult = controllerUtils.loadFxml(sectionSetToLoad);
            loadedController = loadResult.getController();

            // If the controller should be stage aware then inject the stage name.
            if (HostStageAware.class.isAssignableFrom(loadResult.getController().getClass()))
            {
                HostStageAware stageAwareController = (HostStageAware)loadResult.getController();
                stageAwareController.initHostingStageName(hostingStageName);
            }

            // If there was data to be passed to the controller in the event then forward it along
            LOG.debug("Found controller data of size {}", screenNavigationEvent.getControllerData().size());
            if (screenNavigationEvent.getControllerData().size() > 0)
            {
                loadResult.getController().initControllerData(screenNavigationEvent.getControllerData());
            }

            // Clear out the section which houses the set that represents the "screen"
            // and set it to the root node for the new screen
            rootContainer.getChildren().clear();
            rootContainer.getChildren().add(loadResult.getRootNode());
        }
    }

    /**
     * Hook to receive breadcrumb publish events. Delegates to the breadcrumb manager.
     * @param publishBreadcrumbEvent A breadcrumb event published by another screen.
     */
    @Subscribe
    protected void publishBreadcrumb(PublishBreadcrumbEvent publishBreadcrumbEvent)
    {
        if (publishBreadcrumbEvent.getForStage().equals(hostingStageName))
        {
            breadcrumbManager.publishBreadcrumb(publishBreadcrumbEvent, breadcrumbs);
        }
    }

    /**
     * Hook to display the screen loading dialog when a screen begins to load any data that is needs.
     * @param screenLoadBeginEvent Event published by the screen that has begun loading data
     */
    @Subscribe
    protected void showScreenLoadingDialog(ScreenLoadBeginEvent screenLoadBeginEvent)
    {
        windowManager.hideStage(screenLoadBeginEvent.getForStage());
        windowManager.openLoadingDialog(hostingStage.getScene().getWindow(), screenLoadBeginEvent.getLoadingMessage());
    }

    /**
     * Hook to close the screen loading dialog when a screen has completed loading any data that it
     * needs.
     *
     * @param screenLoadCompletedEvent Event published by the screen that has completed loading data
     */
    @Subscribe
    protected void hideScreenLoadingDialog(ScreenLoadCompletedEvent screenLoadCompletedEvent)
    {
        /*
         * Ensure that the loading screen shows for a minimum of half of a second.
         *
         * This prevents the screen from being just a flash which both looks bad and prevents the
         * purpose of notifying the user what is happening.
         *
         * Most of the time the load speeds should be pretty snappy and the load screen really only
         * exists for the times when that isn't the case. But I think if the message is going to
         * display it should at least display for long enough for the user to glimpse it.
         */
        TimerTask delayCloseTask = new TimerTask()
        {
            @Override
            public void run()
            {
                Platform.runLater(() -> {
                    windowManager.close(WindowManager.DIALOG_WINDOW);
                    windowManager.showStage(screenLoadCompletedEvent.getForStage());
                });
            }
        };
        timer.schedule(delayCloseTask, 500);
    }

    /**
     * Navigates to the main screen of the selected application section when it is clicked on in
     * the left hand navigation menu.
     *
     * @param sectionName The name of the application section that was selected from the list
     */
    protected void navigateToAppSection(String sectionName)
    {
        LOG.debug("Navigating to screen section {}", sectionName);
        ScreenNavigationEvent screenNavigationEvent;
        switch(sectionName)
        {
            case ApplicationSections.FOOD_DATABASE:
                LOG.debug("Navigating to the Food Database application section...");
                screenNavigationEvent = new ScreenNavigationEvent(ApplicationSections.FOOD_DATABASE, SceneSets.VIEW_FOODS, hostingStageName, user);
                break;
            case ApplicationSections.RECIPES:
                LOG.debug("Navigating to the Recipes application section...");
                screenNavigationEvent = new ScreenNavigationEvent(ApplicationSections.RECIPES, SceneSets.RECIPES_OVERVIEW, hostingStageName, user);
                break;
            case ApplicationSections.MEAL_PLANNING:
                LOG.debug("Navigating to the Meal Planning application section...");
                screenNavigationEvent = new ScreenNavigationEvent(ApplicationSections.MEAL_PLANNING, SceneSets.MEAL_PLANNING_OVERVIEW, hostingStageName, user);
                break;
            case ApplicationSections.SETTINGS:
                LOG.debug("Navigating to the Settings application section...");
                screenNavigationEvent = new ScreenNavigationEvent(ApplicationSections.SETTINGS, SceneSets.SETTINGS_SCREEN, hostingStageName, user);
                break;
            case ApplicationSections.ABOUT:
                LOG.debug("Navigating to the About application section...");
                screenNavigationEvent = new ScreenNavigationEvent(ApplicationSections.ABOUT, SceneSets.ABOUT_SCREEN, hostingStageName, user);
                break;
            default:
                LOG.debug("Invalid section to open was specified. Defaulting to the Landing Screen.");
                screenNavigationEvent = new ScreenNavigationEvent(ApplicationSections.HOME, SceneSets.LANDING_SCREEN, hostingStageName, user);
                break;
        }

        eventBus.post(screenNavigationEvent);
    }
}
