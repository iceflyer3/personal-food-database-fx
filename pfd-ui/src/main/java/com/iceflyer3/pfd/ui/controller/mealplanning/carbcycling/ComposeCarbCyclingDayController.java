package com.iceflyer3.pfd.ui.controller.mealplanning.carbcycling;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.google.common.eventbus.EventBus;
import com.iceflyer3.pfd.constants.ApplicationSections;
import com.iceflyer3.pfd.constants.FontAwesomeIcons;
import com.iceflyer3.pfd.constants.SceneSets;
import com.iceflyer3.pfd.enums.MealType;
import com.iceflyer3.pfd.enums.Nutrient;
import com.iceflyer3.pfd.ui.controller.AbstractTaskController;
import com.iceflyer3.pfd.ui.controller.HostStageAware;
import com.iceflyer3.pfd.ui.controller.dialog.CreateCarbCyclingMealDialogController;
import com.iceflyer3.pfd.ui.controller.sections.MacronutrientBreakdownScreenSection;
import com.iceflyer3.pfd.ui.controller.sections.MicronutrientBreakdownScreenSection;
import com.iceflyer3.pfd.ui.controller.sections.ScreenSection;
import com.iceflyer3.pfd.ui.event.PublishBreadcrumbEvent;
import com.iceflyer3.pfd.ui.event.ScreenNavigationEvent;
import com.iceflyer3.pfd.ui.manager.OpenDialogRequest;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.MealPlanModelProxy;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.MealPlanningProxyFactory;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.carbcycling.CarbCyclingDayModelProxy;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.carbcycling.CarbCyclingMealModelProxy;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.carbcycling.CarbCyclingPlanModelProxy;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.util.LayoutUtils;
import com.iceflyer3.pfd.ui.util.ScreenLoadEventUtils;
import com.iceflyer3.pfd.ui.util.builder.ActionNodeBuilder;
import com.iceflyer3.pfd.ui.util.builder.NotificationBuilder;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ComposeCarbCyclingDayController extends AbstractTaskController implements HostStageAware
{
    public static final String DATA_FOR_DAY = "forDay";
    public static final String DATA_MEAL_PLAN = "mealPlan";
    public static final String DATA_CARB_CYCLING_PLAN = "carbCyclingPlan";

    private final WindowManager windowManager;
    private final MealPlanningProxyFactory mealPlanningProxyFactory;
    private final ObservableList<CarbCyclingMealModelProxy> mealsForDay;

    private final StringProperty dayCaloriesGrandTotalProperty;
    private final StringProperty dayProteinGrandTotalProperty;
    private final StringProperty dayCarbsGrandTotalProperty;
    private final StringProperty dayTotalFatsGrandTotalProperty;

    private UserViewModel user;
    private String hostingStageName;
    private CarbCyclingDayModelProxy dayModelProxy;
    private MealPlanModelProxy mealPlan;
    private CarbCyclingPlanModelProxy carbCyclingPlan;

    @FXML
    public VBox rootContainer;

    @FXML
    public Label screenLabel;

    @FXML
    public Label dayLabelHeader;

    @FXML
    public Label dailyCaloriesLabel;

    @FXML
    public Label dailyProteinLabel;

    @FXML
    public Label dailyCarbsLabel;

    @FXML
    public Label dailyFatsLabel;

    @FXML
    public Label fitnessGoalLabel;

    @FXML
    public Label dayTypeLabel;

    @FXML
    public Label isTrainingDayLabel;

    @FXML
    public VBox mealSectionContainer;

    public ComposeCarbCyclingDayController(EventBus eventBus,
                                           NotificationBuilder notificationBuilder,
                                           ScreenLoadEventUtils loadEventBuilder,
                                           TaskExecutor taskExecutor,
                                           WindowManager windowManager,
                                           MealPlanningProxyFactory mealPlanningProxyFactory)
    {
        super(eventBus, notificationBuilder, loadEventBuilder, taskExecutor);
        this.windowManager = windowManager;
        this.mealPlanningProxyFactory = mealPlanningProxyFactory;
        this.mealsForDay = FXCollections.observableArrayList();

        this.dayCaloriesGrandTotalProperty = new SimpleStringProperty();
        this.dayProteinGrandTotalProperty = new SimpleStringProperty();
        this.dayCarbsGrandTotalProperty = new SimpleStringProperty();
        this.dayTotalFatsGrandTotalProperty = new SimpleStringProperty();
    }

    @Override
    public void initialize()
    {

        VBox.setMargin(screenLabel, LayoutUtils.getHeaderLabelMarginInsets());
    }

    @Override
    protected void registerDefaultListeners()
    {

    }

    @Override
    public void initHostingStageName(String stageName)
    {
        hostingStageName = stageName;
    }

    @Override
    public void initControllerData(Map<String, Object> controllerData) {

        this.user = (UserViewModel) controllerData.get(UserViewModel.class.getName());
        this.dayModelProxy = (CarbCyclingDayModelProxy) controllerData.get(DATA_FOR_DAY);
        this.mealPlan = (MealPlanModelProxy) controllerData.get(DATA_MEAL_PLAN);
        this.carbCyclingPlan = (CarbCyclingPlanModelProxy) controllerData.get(DATA_CARB_CYCLING_PLAN);

        PublishBreadcrumbEvent breadcrumb = new PublishBreadcrumbEvent("Compose Carb Cycling Day", ApplicationSections.MEAL_PLANNING, SceneSets.CARB_CYCLING_COMPOSE_DAY, hostingStageName, 3);
        breadcrumb.addData(DATA_FOR_DAY, dayModelProxy);
        breadcrumb.addData(DATA_MEAL_PLAN, mealPlan);
        breadcrumb.addData(DATA_CARB_CYCLING_PLAN, carbCyclingPlan);
        eventBus.post(breadcrumb);

        initOverviewSection();
        initMealsSection();

        ScreenSection macroBreakdownSection = new MacronutrientBreakdownScreenSection<>(dayModelProxy);
        ScreenSection microBreakdownSection = new MicronutrientBreakdownScreenSection<>(dayModelProxy);
        rootContainer.getChildren().add(macroBreakdownSection.create());
        rootContainer.getChildren().add(microBreakdownSection.create());
    }

    private void initOverviewSection()
    {
        dayLabelHeader.setText(dayModelProxy.getDayLabel());
        dayTypeLabel.setText(dayModelProxy.getDayType().getName());
        fitnessGoalLabel.setText(dayModelProxy.getFitnessGoal().getName());

        String isTrainingDayText = dayModelProxy.isTrainingDay() ? "Yes" : "No";
        isTrainingDayLabel.setText(isTrainingDayText);

        dailyCaloriesLabel.textProperty().bind(dayCaloriesGrandTotalProperty);
        dailyProteinLabel.textProperty().bind(dayProteinGrandTotalProperty);
        dailyCarbsLabel.textProperty().bind(dayCarbsGrandTotalProperty);
        dailyFatsLabel.textProperty().bind(dayTotalFatsGrandTotalProperty);
    }

    private void initMealsSection()
    {
        Button addMealButton = ActionNodeBuilder.createIconButton("ADD MEAL", FontAwesomeIcons.PLUS)
                .withAction(this::openNewMealDialog)
                .build();

        mealSectionContainer.getChildren().add(0, addMealButton);
        mealSectionContainer.setPadding(new Insets(10, 10, 10, 10));

        refreshMealsForDay();
        initMealsForDayTableview();
    }

    private void initMealsForDayTableview()
    {
        // Setup table view
        TableView<CarbCyclingMealModelProxy> mealsTable = new TableView<>();
        mealsTable.setPlaceholder(new Label("You have not yet created any meals"));

        TableColumn<CarbCyclingMealModelProxy, String> mealNameColumn = new TableColumn<>("Name");
        mealNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));

        TableColumn<CarbCyclingMealModelProxy, String> mealTypeColumn = new TableColumn<>("Type");
        mealTypeColumn.setCellValueFactory(new PropertyValueFactory<>("mealType"));

        TableColumn<CarbCyclingMealModelProxy, String> mealNumberColumn = new TableColumn<>("Number");
        mealNumberColumn.setCellValueFactory(new PropertyValueFactory<>("mealNumber"));

        TableColumn<CarbCyclingMealModelProxy, BigDecimal> carbConsumptionPercentageColumn = new TableColumn<>("Carb Consumption %");
        carbConsumptionPercentageColumn.setCellValueFactory(new PropertyValueFactory<>("carbConsumptionPercentage"));

        TableColumn<CarbCyclingMealModelProxy, String> caloriesColumn = new TableColumn<>("Calories");
        caloriesColumn.setCellValueFactory(cellDataFeatures -> getSummaryDisplayStringPropertyForCarbCyclingMealNutrient(cellDataFeatures.getValue(), Nutrient.CALORIES));

        TableColumn<CarbCyclingMealModelProxy, String> proteinColumn = new TableColumn<>("Protein");
        proteinColumn.setCellValueFactory(cellDataFeatures -> getSummaryDisplayStringPropertyForCarbCyclingMealNutrient(cellDataFeatures.getValue(), Nutrient.PROTEIN));

        TableColumn<CarbCyclingMealModelProxy, String> carbsColumn = new TableColumn<>("Carbs");
        carbsColumn.setCellValueFactory(cellDataFeatures -> getSummaryDisplayStringPropertyForCarbCyclingMealNutrient(cellDataFeatures.getValue(), Nutrient.CARBOHYDRATES));

        TableColumn<CarbCyclingMealModelProxy, String> fatsColumn = new TableColumn<>("Fats");
        fatsColumn.setCellValueFactory(cellDataFeatures -> getSummaryDisplayStringPropertyForCarbCyclingMealNutrient(cellDataFeatures.getValue(), Nutrient.TOTAL_FATS));

        mealsTable.getColumns().addAll(
                mealNameColumn,
                mealTypeColumn,
                mealNumberColumn,
                carbConsumptionPercentageColumn,
                caloriesColumn,
                proteinColumn,
                carbsColumn,
                fatsColumn);
        mealsTable.setItems(mealsForDay);
        mealsTable.setContextMenu(createContextMenuForMealsTable(mealsTable));

        mealSectionContainer.getChildren().add(mealsTable);
    }

    private ContextMenu createContextMenuForMealsTable(TableView<CarbCyclingMealModelProxy> mealsTable)
    {
        EventHandler<ActionEvent> viewActionHandler = event -> {
            CarbCyclingMealModelProxy selectedMeal = mealsTable.getSelectionModel().getSelectedItem();
            ScreenNavigationEvent navigationEvent = new ScreenNavigationEvent(ApplicationSections.MEAL_PLANNING, SceneSets.CARB_CYCLING_VIEW_MEAL, hostingStageName, user);
            navigationEvent.getControllerData().put(ViewCarbCyclingMealController.DATA_MEAL_ID, selectedMeal.getId());
            eventBus.post(navigationEvent);
        };

        EventHandler<ActionEvent> editActionHandler = event -> {
            CarbCyclingMealModelProxy selectedMeal = mealsTable.getSelectionModel().getSelectedItem();
            ScreenNavigationEvent navigationEvent = new ScreenNavigationEvent(ApplicationSections.MEAL_PLANNING, SceneSets.CARB_CYCLING_COMPOSE_MEAL, hostingStageName, user);
            navigationEvent.getControllerData().put(ComposeCarbCyclingMealController.DATA_MEAL_TO_COMPOSE, selectedMeal);
            eventBus.post(navigationEvent);
        };

        EventHandler<ActionEvent> deleteActionHandler = event -> {
            windowManager.openConfirmationDialog(rootContainer.getScene().getWindow(), () -> {
                CarbCyclingMealModelProxy selectedMeal = mealsTable.getSelectionModel().getSelectedItem();
                selectedMeal.delete((wasSuccessful, resultMessage) -> {
                    notificationBuilder.create(resultMessage, hostingStageName).show();

                    if (wasSuccessful)
                    {
                        refreshMealsForDay();
                    }
                });
            });
        };

        return LayoutUtils.getDefaultContextMenu(viewActionHandler, editActionHandler, deleteActionHandler);
    }

    private SimpleStringProperty getSummaryDisplayStringPropertyForCarbCyclingMealNutrient(CarbCyclingMealModelProxy meal, Nutrient macronutrient)
    {
        String nutrientSummaryString = "";

        switch(macronutrient)
        {
            case CALORIES -> nutrientSummaryString = String.format("%s / %s", meal.getCalories(), meal.getRequiredCalories());
            case PROTEIN -> nutrientSummaryString = String.format("%s / %s", meal.getProtein(), meal.getRequiredProtein());
            case CARBOHYDRATES -> nutrientSummaryString = String.format("%s / %s", meal.getCarbohydrates(), meal.getRequiredCarbs());
            case TOTAL_FATS -> nutrientSummaryString = String.format("%s / %s", meal.getTotalFats(), meal.getRequiredFats());
        }

        return new SimpleStringProperty(nutrientSummaryString);
    }

    private void refreshMealsForDay()
    {
        loadEventBuilder.emitBegin("Searching for carb cycling day details", hostingStageName);
        mealPlanningProxyFactory.getCarbCyclingMealsForDay(dayModelProxy, (wasSuccessful, proxyResult) -> {
            if (wasSuccessful)
            {
                this.mealsForDay.clear();

                if (proxyResult.size() > 0)
                {
                    this.mealsForDay.addAll(proxyResult);
                }

                // Ensure that the day and the UI bound properties have been refreshed with the latest data from the new meal list
                dayModelProxy.refreshNutritionDetails(mealsForDay);
                dayCaloriesGrandTotalProperty.set(String.format("%s / %s", dayModelProxy.getCalories(), dayModelProxy.getRequiredCalories()));
                dayProteinGrandTotalProperty.set(String.format("%s / %s", dayModelProxy.getProtein(), dayModelProxy.getRequiredProtein()));
                dayCarbsGrandTotalProperty.set(String.format("%s / %s", dayModelProxy.getCarbohydrates(), dayModelProxy.getRequiredCarbs()));
                dayTotalFatsGrandTotalProperty.set(String.format("%s / %s", dayModelProxy.getTotalFats(), dayModelProxy.getRequiredFats()));
            }
            else
            {
                notificationBuilder.create("Could not load the meals for the day!", hostingStageName).show();
            }
            loadEventBuilder.emitCompleted(hostingStageName);
        });
    }

    private void openNewMealDialog(ActionEvent event)
    {
        OpenDialogRequest<CarbCyclingMealModelProxy> newMealDialogRequest = new OpenDialogRequest<>(rootContainer.getScene().getWindow(), SceneSets.DIALOG_CREATE_CARB_CYCLING_PLAN_MEAL, this::saveMeal);
        newMealDialogRequest.setDialogTitle("Create New Carb Cycling Meal");

        // Find the post-workout meal number (if there is one)
        int postWorkoutMealNumber = -1;
        Optional<CarbCyclingMealModelProxy> postWorkoutMeal = mealsForDay.stream().filter(meal -> meal.getMealType() == MealType.POST_WORKOUT).findFirst();
        if (postWorkoutMeal.isPresent())
        {
            postWorkoutMealNumber = postWorkoutMeal.get().getMealNumber();
        }

        List<Integer> existingMealNumbers = mealsForDay.stream().map(CarbCyclingMealModelProxy::getMealNumber).collect(Collectors.toList());
        newMealDialogRequest.getDialogData().put(CreateCarbCyclingMealDialogController.DATA_FOR_CARB_CYCLING_PLAN, carbCyclingPlan);
        newMealDialogRequest.getDialogData().put(CreateCarbCyclingMealDialogController.DATA_FOR_MEAL_PLAN, mealPlan);
        newMealDialogRequest.getDialogData().put(CreateCarbCyclingMealDialogController.DATA_TAKEN_MEAL_NUMBERS, existingMealNumbers);
        newMealDialogRequest.getDialogData().put(CreateCarbCyclingMealDialogController.DATA_FOR_DAY, dayModelProxy);
        newMealDialogRequest.getDialogData().put(CreateCarbCyclingMealDialogController.DATA_POST_WORKOUT_MEAL_NUMBER, postWorkoutMealNumber);

        windowManager.openDialog(newMealDialogRequest);
    }

    private void saveMeal(CarbCyclingMealModelProxy meal)
    {
        meal.save((wasSuccessful, resultMessage) ->
        {
            notificationBuilder.create(resultMessage, hostingStageName).show();

            if (wasSuccessful)
            {
                refreshMealsForDay();
            }
        });
    }
}
