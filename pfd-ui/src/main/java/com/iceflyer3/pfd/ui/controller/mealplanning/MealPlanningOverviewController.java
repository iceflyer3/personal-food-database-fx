package com.iceflyer3.pfd.ui.controller.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.google.common.eventbus.EventBus;
import com.iceflyer3.pfd.constants.ApplicationSections;
import com.iceflyer3.pfd.constants.CssConstants;
import com.iceflyer3.pfd.constants.FontAwesomeIcons;
import com.iceflyer3.pfd.constants.SceneSets;
import com.iceflyer3.pfd.enums.ActivityLevel;
import com.iceflyer3.pfd.ui.controller.AbstractTaskController;
import com.iceflyer3.pfd.ui.controller.HostStageAware;
import com.iceflyer3.pfd.ui.controller.mealplanning.carbcycling.CarbCyclingPlanOverviewController;
import com.iceflyer3.pfd.ui.controller.sections.ScreenSection;
import com.iceflyer3.pfd.ui.controller.sections.mealplanning.CalculatedMealPlanOverviewScreenSection;
import com.iceflyer3.pfd.ui.controller.sections.mealplanning.ManualMealPlanOverviewScreenSection;
import com.iceflyer3.pfd.ui.enums.PfdActionType;
import com.iceflyer3.pfd.ui.event.PublishBreadcrumbEvent;
import com.iceflyer3.pfd.ui.event.ScreenNavigationEvent;
import com.iceflyer3.pfd.ui.manager.OpenDialogRequest;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import com.iceflyer3.pfd.ui.model.api.proxy.ProxyOperationResultCallback;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.MealPlanDayModelProxy;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.MealPlanModelProxy;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.MealPlanningProxyFactory;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.carbcycling.CarbCyclingPlanModelProxy;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.util.LayoutUtils;
import com.iceflyer3.pfd.ui.util.ScreenLoadEventUtils;
import com.iceflyer3.pfd.ui.util.TextUtils;
import com.iceflyer3.pfd.ui.util.builder.ActionNodeBuilder;
import com.iceflyer3.pfd.ui.util.builder.NotificationBuilder;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import org.springframework.context.annotation.Scope;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@Scope("prototype")
public class MealPlanningOverviewController extends AbstractTaskController implements HostStageAware
{
    private final WindowManager windowManager;
    private final MealPlanningProxyFactory mealPlanningProxyFactory;
    private final ObservableList<CarbCyclingPlanModelProxy> carbCyclingPlans;
    private final ObservableList<MealPlanDayModelProxy> daysForMealPlan;


    private UserViewModel user;
    private String hostingStageName;
    private MealPlanModelProxy activeMealPlan;

    @FXML
    public VBox rootContainer;

    @FXML
    public VBox registrationContainer;

    @FXML
    public Hyperlink appSettingsHyperlink;

    @FXML
    public VBox overviewContainer;

    @FXML
    public VBox carbCyclingSectionContainer;

    @FXML
    public VBox mealPlanningDaySectionContainer;

    @FXML
    public Label screenLabel;

    public MealPlanningOverviewController(EventBus eventBus,
                                          NotificationBuilder notificationBuilder,
                                          ScreenLoadEventUtils loadEventBuilder,
                                          TaskExecutor taskExecutor,
                                          WindowManager windowManager,
                                          MealPlanningProxyFactory mealPlanningProxyFactory)
    {
        super(eventBus, notificationBuilder, loadEventBuilder, taskExecutor);
        this.windowManager = windowManager;
        this.mealPlanningProxyFactory = mealPlanningProxyFactory;
        this.carbCyclingPlans = FXCollections.observableArrayList();
        this.daysForMealPlan = FXCollections.observableArrayList();
    }

    @Override
    public void initialize() {
        registrationContainer.managedProperty().bind(registrationContainer.visibleProperty());
        overviewContainer.managedProperty().bind(overviewContainer.visibleProperty());
    }

    @Override
    protected void registerDefaultListeners() {
        appSettingsHyperlink.setOnAction(event -> {
            ScreenNavigationEvent screenNavigationEvent = new ScreenNavigationEvent(ApplicationSections.SETTINGS, SceneSets.SETTINGS_SCREEN, hostingStageName, user);
            eventBus.post(screenNavigationEvent);
        });
    }

    @Override
    public void initControllerData(Map<String, Object> controllerData)
    {
        user = (UserViewModel) controllerData.get(UserViewModel.class.getName());

        loadEventBuilder.emitBegin("Searching for meal plan details", hostingStageName);

        // Look to see if meal planning has been set up or not
        mealPlanningProxyFactory.getActiveMealPlanForUser(user.getUserId(), (wasSuccessful, proxyResult) -> {
            if (wasSuccessful)
            {
                if (proxyResult.isPresent())
                {
                    activeMealPlan = proxyResult.get();
                    initOverviewScreen();
                }
                else
                {
                    initRegistrationScreen();
                }
            }
            else
            {
                notificationBuilder.create("An error has occurred while searching for an active meal plan", hostingStageName).show();
            }
            loadEventBuilder.emitCompleted(hostingStageName);
        });
    }

    @Override
    public void initHostingStageName(String stageName) {
        hostingStageName = stageName;

        PublishBreadcrumbEvent landingScreenBreadcrumb = new PublishBreadcrumbEvent("Meal Planning", ApplicationSections.MEAL_PLANNING, SceneSets.MEAL_PLANNING_OVERVIEW, hostingStageName, 1);
        eventBus.post(landingScreenBreadcrumb);
    }

    // Screen for when no meal plan exists and user has not registered. Allows registration for meal planning
    private void initRegistrationScreen()
    {
        overviewContainer.setVisible(false);
        registrationContainer.setVisible(true);
    }

    // Screen for meal planning user does exist.
    private void initOverviewScreen()
    {
        registrationContainer.setVisible(false);
        overviewContainer.setVisible(true);

        VBox.setMargin(screenLabel, LayoutUtils.getHeaderLabelMarginInsets());

        Button viewCalendarButton = ActionNodeBuilder
                .createIconButton("VIEW CALENDAR", FontAwesomeIcons.CALENDAR_DAYS)
                .forActionType(PfdActionType.PRIMARY)
                .withAction(event -> {
                    ScreenNavigationEvent navigationEvent = new ScreenNavigationEvent(ApplicationSections.MEAL_PLANNING, SceneSets.MEAL_PLANNING_CALENDAR, hostingStageName, user);
                    eventBus.post(navigationEvent);
                })
                .build();
        overviewContainer.getChildren().add(1, viewCalendarButton);

        initMealPlanningPanel();
        initCarbCyclingPanel();
        initMealPlanDaysPanel();
    }

    private void openNewCarbCyclingPlanDialog(ActionEvent event)
    {
        OpenDialogRequest<CarbCyclingPlanModelProxy> registrationDialogRequest = new OpenDialogRequest<>(rootContainer.getScene().getWindow(), SceneSets.DIALOG_CREATE_CARB_CYCLING_PLAN, this::saveCarbCyclingPlan);
        registrationDialogRequest.setDialogTitle("Create Carb Cycling Plan");
        registrationDialogRequest.setWidth(580);
        registrationDialogRequest.setHeight(570);
        windowManager.openDialog(registrationDialogRequest);
    }

    private void openNewMealPlanDayDialog(ActionEvent event)
    {
        OpenDialogRequest<MealPlanDayModelProxy> newMealPlanDayDialogRequest = new OpenDialogRequest<>(rootContainer.getScene().getWindow(), SceneSets.DIALOG_CREATE_MEAL_PLAN_DAY, this::saveMealPlanDay);
        newMealPlanDayDialogRequest.setDialogTitle("Create Meal Plan Day");
        newMealPlanDayDialogRequest.getDialogData().put(MealPlanModelProxy.class.getName(), activeMealPlan);

        windowManager.openDialog(newMealPlanDayDialogRequest);
    }

    private void saveCarbCyclingPlan(CarbCyclingPlanModelProxy dialogOutput)
    {
        notificationBuilder.create("Creating carb cycling plan...", hostingStageName).show();
        dialogOutput.save(user, activeMealPlan, (wasSuccessful, proxyResult) -> {
            if (wasSuccessful)
            {
                this.carbCyclingPlans.add(proxyResult);
                notificationBuilder.create("Carb cycling plan was saved successfully!", hostingStageName).show();
            }
            else
            {
                notificationBuilder.create("An error has occurred while saving the carb cycling plan", hostingStageName).show();
            }
        });
    }

    private void saveMealPlanDay(MealPlanDayModelProxy dialogOutput)
    {
        ProxyOperationResultCallback callback = (wasSuccessful, resultMessage) -> {
            notificationBuilder.create(resultMessage, hostingStageName).show();

            if (wasSuccessful)
            {
                refreshDaysForMealPlan();
            }
        };

        notificationBuilder.create("Creating day for meal plan...", hostingStageName).show();
        dialogOutput.save(callback);
    }

    private void initMealPlanningPanel()
    {
        ScreenSection mealPlanConfigurationSection;
        if (activeMealPlan.getActivityLevel() == ActivityLevel.UNUSED)
        {
            mealPlanConfigurationSection = new ManualMealPlanOverviewScreenSection(activeMealPlan);
        }
        else
        {
            mealPlanConfigurationSection = new CalculatedMealPlanOverviewScreenSection(activeMealPlan);
        }

        Node configurationSectionRootNode = mealPlanConfigurationSection.create();
        VBox.setMargin(configurationSectionRootNode, new Insets(20, 0, 20, 0));
        overviewContainer.getChildren().add(2, configurationSectionRootNode);
    }

    private void initCarbCyclingPanel()
    {
        Button newCarbCyclingPlanButton = ActionNodeBuilder
                .createIconButton("ADD PLAN", FontAwesomeIcons.PLUS)
                .withAction(this::openNewCarbCyclingPlanDialog)
                .build();
        carbCyclingSectionContainer.getChildren().add(0, newCarbCyclingPlanButton);
        carbCyclingSectionContainer.setPadding(new Insets(10, 10, 10, 10));

        initCarbCyclingTableview();
        refreshCarbCyclingPlans();
    }

    private void initCarbCyclingTableview()
    {
        TableView<CarbCyclingPlanModelProxy> carbCyclingPlansTable = new TableView<>();
        carbCyclingPlansTable.setEditable(false);
        carbCyclingPlansTable.setPlaceholder(new Label("You have not yet created any carb cycling plans"));

        TableColumn<CarbCyclingPlanModelProxy, Integer> lowCarbDaysColumn = new TableColumn<>("Low Carb Days");
        lowCarbDaysColumn.setCellValueFactory(new PropertyValueFactory<>("lowCarbDays"));
        lowCarbDaysColumn.getStyleClass().add(CssConstants.CLASS_TEXT_TABLE_CENTER_RIGHT);

        TableColumn<CarbCyclingPlanModelProxy, Integer> highCarbDaysColumn = new TableColumn<>("High Carb Days");
        highCarbDaysColumn.setCellValueFactory(new PropertyValueFactory<>("highCarbDays"));
        highCarbDaysColumn.getStyleClass().add(CssConstants.CLASS_TEXT_TABLE_CENTER_RIGHT);

        TableColumn<CarbCyclingPlanModelProxy, Integer> mealsPerDayColumn = new TableColumn<>("Meals Per Day");
        mealsPerDayColumn.setCellValueFactory(new PropertyValueFactory<>("mealsPerDay"));
        mealsPerDayColumn.getStyleClass().add(CssConstants.CLASS_TEXT_TABLE_CENTER_RIGHT);

        TableColumn<CarbCyclingPlanModelProxy, Boolean> shouldTimeFatsColumn = new TableColumn<>("Should Time Fats?");
        shouldTimeFatsColumn.setCellValueFactory(new PropertyValueFactory<>("shouldTimeFats"));
        shouldTimeFatsColumn.setCellFactory(CheckBoxTableCell.forTableColumn(shouldTimeFatsColumn));
        shouldTimeFatsColumn.setEditable(false);

        carbCyclingPlansTable.getColumns().addAll(
                lowCarbDaysColumn,
                highCarbDaysColumn,
                mealsPerDayColumn,
                shouldTimeFatsColumn);

        carbCyclingPlansTable.setContextMenu(createCarbCyclingTableContextMenu(carbCyclingPlansTable));
        carbCyclingPlansTable.setItems(carbCyclingPlans);
        carbCyclingSectionContainer.getChildren().add(carbCyclingPlansTable);
    }

    private ContextMenu createCarbCyclingTableContextMenu(TableView<CarbCyclingPlanModelProxy> carbCyclingPlansTable)
    {
        EventHandler<ActionEvent> editActionHandler = event -> {
            CarbCyclingPlanModelProxy selectedPlan = carbCyclingPlansTable.getSelectionModel().getSelectedItem();


            ScreenNavigationEvent carbCyclingPlanOverviewNavigationEvent = new ScreenNavigationEvent(ApplicationSections.MEAL_PLANNING, SceneSets.CARB_CYCLING_PLAN_OVERVIEW, hostingStageName, user);
            carbCyclingPlanOverviewNavigationEvent.getControllerData().put(CarbCyclingPlanOverviewController.DATA_CARB_CYCLING_PLAN, selectedPlan);
            carbCyclingPlanOverviewNavigationEvent.getControllerData().put(CarbCyclingPlanOverviewController.DATA_MEAL_PLAN, activeMealPlan);
            eventBus.post(carbCyclingPlanOverviewNavigationEvent);
        };

        EventHandler<ActionEvent> deleteActionHandler = event -> {
            windowManager.openConfirmationDialog(rootContainer.getScene().getWindow(), () ->
            {
                CarbCyclingPlanModelProxy selectedPlan = carbCyclingPlansTable.getSelectionModel().getSelectedItem();
                selectedPlan.delete((wasSuccessful, resultMessage) ->
                {
                    notificationBuilder.create(resultMessage, hostingStageName).show();

                    if (wasSuccessful)
                    {
                        refreshCarbCyclingPlans();
                    }
                });
            });
        };

        return LayoutUtils.getDefaultContextMenu(editActionHandler, editActionHandler, deleteActionHandler);
    }

    private void initMealPlanDaysPanel()
    {
        Button addButton = ActionNodeBuilder
                                .createIconButton("ADD DAY", FontAwesomeIcons.PLUS)
                                .withAction(this::openNewMealPlanDayDialog)
                                .build();

        mealPlanningDaySectionContainer.getChildren().add(addButton);
        mealPlanningDaySectionContainer.setPadding(new Insets(10, 10, 10, 10));

        initMealPlanDaysTableView();
        refreshDaysForMealPlan();
    }

    private void initMealPlanDaysTableView()
    {
        TableView<MealPlanDayModelProxy> mealPlanDaysTable = new TableView<>();
        mealPlanDaysTable.setEditable(false);
        mealPlanDaysTable.setPlaceholder(new Label("You have not yet created any days for this meal plan"));

        TableColumn<MealPlanDayModelProxy, String> dayLabelColumn = new TableColumn<>("Name");
        dayLabelColumn.setCellValueFactory(new PropertyValueFactory<>("dayLabel"));

        TableColumn<MealPlanDayModelProxy, String> lastModifiedColumn = new TableColumn<>("Last Modified");
        lastModifiedColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<MealPlanDayModelProxy, String>, ObservableValue<String>>()
        {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<MealPlanDayModelProxy, String> param)
            {
                return new ReadOnlyStringWrapper(TextUtils.getDateTimeString(param.getValue().getLastModifiedDate()));
            }
        });

        mealPlanDaysTable.getColumns().addAll(
                dayLabelColumn,
                lastModifiedColumn
        );

        mealPlanDaysTable.setContextMenu(createContextMenuForMealPlanDayTable(mealPlanDaysTable));
        mealPlanDaysTable.setItems(daysForMealPlan);
        mealPlanningDaySectionContainer.getChildren().add(mealPlanDaysTable);
    }

    private ContextMenu createContextMenuForMealPlanDayTable(TableView<MealPlanDayModelProxy> mealPlanDaysTable)
    {
        EventHandler<ActionEvent> editActionHandler = event -> {
            // Navigate to edit day screen
            MealPlanDayModelProxy selectedDay = mealPlanDaysTable.getSelectionModel().getSelectedItem();
            ScreenNavigationEvent navigationEvent = new ScreenNavigationEvent(ApplicationSections.MEAL_PLANNING, SceneSets.MEAL_PLANNING_COMPOSE_DAY, hostingStageName, user);
            navigationEvent.getControllerData().put(MealPlanDayModelProxy.class.getName(), selectedDay);
            navigationEvent.getControllerData().put(MealPlanModelProxy.class.getName(), activeMealPlan);

            eventBus.post(navigationEvent);
        };

        EventHandler<ActionEvent> deleteActionHandler = event -> {
            windowManager.openConfirmationDialog(rootContainer.getScene().getWindow(), () -> {
                MealPlanDayModelProxy selectedDay = mealPlanDaysTable.getSelectionModel().getSelectedItem();
                selectedDay.delete((wasSuccessful, resultMessage) -> {
                    notificationBuilder.create(resultMessage, hostingStageName).show();

                    if (wasSuccessful)
                    {
                        refreshDaysForMealPlan();
                    }
                });
            });
        };
        return LayoutUtils.getDefaultContextMenu(editActionHandler, editActionHandler, deleteActionHandler);
    }

    private void refreshCarbCyclingPlans()
    {
        mealPlanningProxyFactory.getCarbCyclingPlansForMealPlan(activeMealPlan.getId(), (wasSuccessful, proxyResult) -> {
            if (wasSuccessful)
            {
                carbCyclingPlans.clear();

                if (proxyResult.size() > 0)
                {
                    carbCyclingPlans.addAll(proxyResult);
                }
            }
            else
            {
                notificationBuilder.create("Could not load carb cycling plans", hostingStageName).show();
            }
        });
    }

    private void refreshDaysForMealPlan()
    {
        mealPlanningProxyFactory.getDaysForMealPlan(activeMealPlan, (wasSuccessful, proxyResult) -> {
            if (wasSuccessful)
            {
                daysForMealPlan.clear();

                if (proxyResult.size() > 0)
                {
                    daysForMealPlan.addAll(proxyResult);
                }
            }
        });
    }
}
