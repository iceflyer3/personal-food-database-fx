package com.iceflyer3.pfd.ui.controller.dialog;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.ui.CandidateRecipeStepIngredient;
import com.iceflyer3.pfd.ui.controller.Savable;
import com.iceflyer3.pfd.ui.listener.Destroyable;
import com.iceflyer3.pfd.ui.listener.ObservableValueListenerBinding;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.viewmodel.IngredientViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.recipe.RecipeStepViewModel;
import com.iceflyer3.pfd.ui.util.BindingUtils;
import com.iceflyer3.pfd.ui.listener.ModelPropertyValidationEventConfig;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CreateRecipeStepDialogController extends AbstractDialogController<RecipeStepViewModel> implements Savable, Destroyable
{

    private final Set<ObservableValueListenerBinding<?>> listenerBindings;

    private RecipeStepViewModel recipeStep;

    private ObservableList<CandidateRecipeStepIngredient> stepIngredients;

    @FXML
    public TextField stepNameInput;

    @FXML
    public TextArea directionsInput;

    @FXML
    public ScrollPane candidateStepIngredientsContainer;

    @FXML
    public Button saveButton;

    @FXML
    public Hyperlink cancelButton;

    public CreateRecipeStepDialogController(WindowManager windowManager) {
        super(windowManager);
        listenerBindings = new HashSet<>();
    }

    @Override
    public void initialize() {

    }

    @Override
    public void initControllerData(Map<String, Object> dialogData) {
        recipeStep = (RecipeStepViewModel) dialogData.get(RecipeStepViewModel.class.getName());
        List<IngredientViewModel> ingredientViewModels = FXCollections.observableArrayList((List<IngredientViewModel>) dialogData.get(IngredientViewModel.class.getName()));
        stepIngredients = FXCollections.observableArrayList(ingredientViewModels.stream().map(CandidateRecipeStepIngredient::new).collect(Collectors.toList()));

        // If the step already has any ingredients then add them to the list of ingredients and make sure they're already checked
        if (recipeStep.getIngredients().size() > 0)
        {
            List<CandidateRecipeStepIngredient> ingredientsAlreadyOnStep = FXCollections.observableArrayList(recipeStep
                                                                                                                .getIngredients()
                                                                                                                .stream()
                                                                                                                .map(ingredient -> new CandidateRecipeStepIngredient(ingredient, true))
                                                                                                                .collect(Collectors.toList()));
            stepIngredients.addAll(ingredientsAlreadyOnStep);
        }

        FXCollections.sort(stepIngredients);

        // Setup bindings for user inputs and validations
        Bindings.bindBidirectional(stepNameInput.textProperty(), recipeStep.stepNameProperty());
        listenerBindings.add(BindingUtils.bindModelObservableValueToGuiControl(new ModelPropertyValidationEventConfig<>(recipeStep, recipeStep.stepNameProperty(), stepNameInput, this::updateSaveValidity)));

        Bindings.bindBidirectional(directionsInput.textProperty(), recipeStep.directionsProperty());
        listenerBindings.add(BindingUtils.bindModelObservableValueToGuiControl(new ModelPropertyValidationEventConfig<>(recipeStep, recipeStep.directionsProperty(), directionsInput, this::updateSaveValidity)));

        // After validators have been initialized check initial save validity. This will be initially invalid for new steps and valid for editing existing steps
        updateSaveValidity();

        // Create the tableview that allows the user to select which ingredients to use for this step
        initCandidateStepIngredientsTableView();
    }

    @Override
    protected void registerDefaultListeners() {
        saveButton.setOnAction(this::save);
        cancelButton.setOnAction(event -> windowManager.close(WindowManager.DIALOG_WINDOW));
    }

    @Override
    public void updateSaveValidity() {
        this.saveButton.setDisable(!recipeStep.validate().wasSuccessful());
    }

    @Override
    public void save(ActionEvent event) {
        // Clear out any previous list of ingredients.
        recipeStep.getIngredients().clear();

        // Refresh with the newest list of selected ingredients and set them on the step
        List<IngredientModel> selectedIngredients = stepIngredients
                .stream()
                .filter(CandidateRecipeStepIngredient::isSelected)
                .map(CandidateRecipeStepIngredient::getCandidateIngredient)
                .collect(Collectors.toList());
        recipeStep.getIngredients().addAll(selectedIngredients);
        callback.complete(recipeStep);
        windowManager.close(WindowManager.DIALOG_WINDOW);
    }

    @Override
    public void destroy()
    {
        listenerBindings.forEach(binding -> binding.unbind());
        listenerBindings.clear();
    }

    private void initCandidateStepIngredientsTableView()
    {
        TableView<CandidateRecipeStepIngredient> candidateIngredientsTableView = new TableView<>();
        candidateIngredientsTableView.setPlaceholder(new Label("No ingredients remaining"));
        candidateIngredientsTableView.setEditable(true);

        TableColumn<CandidateRecipeStepIngredient, Boolean> selectIngredientColumn = new TableColumn<>();
        selectIngredientColumn.setCellValueFactory(new PropertyValueFactory<>("isSelected"));
        selectIngredientColumn.setCellFactory(CheckBoxTableCell.forTableColumn(selectIngredientColumn));
        selectIngredientColumn.setEditable(true);

        TableColumn<CandidateRecipeStepIngredient, String> ingredientNameColumn = new TableColumn<>("Ingredient Name");
        ingredientNameColumn.setCellValueFactory(new PropertyValueFactory<>("ingredientName"));
        ingredientNameColumn.setEditable(false);

        candidateIngredientsTableView.getColumns().addAll(selectIngredientColumn, ingredientNameColumn);
        candidateIngredientsTableView.setItems(stepIngredients);

        candidateStepIngredientsContainer.setContent(candidateIngredientsTableView);
    }
}
