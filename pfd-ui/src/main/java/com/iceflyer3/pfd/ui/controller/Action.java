package com.iceflyer3.pfd.ui.controller;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Functional interface akin to Runnable that is designed to execute an arbitrary action.
 *
 * It takes no arguments, returns no value, and may take whatever action is needed.
 *
 * We probably could've used Runnable for this. But it seemed more semantically correct
 * to declare our own interface since Runnable is generally (afaik) associated with use
 * with a thread.
 */
@FunctionalInterface
public interface Action
{
    void perform();
}
