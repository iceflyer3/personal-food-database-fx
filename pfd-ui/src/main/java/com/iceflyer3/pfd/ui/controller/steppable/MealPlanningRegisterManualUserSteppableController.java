package com.iceflyer3.pfd.ui.controller.steppable;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023, 2025 Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.ui.controller.AbstractController;
import com.iceflyer3.pfd.ui.controller.Savable;
import com.iceflyer3.pfd.ui.controller.sections.mealplanning.MealPlanRegistrationCustomNutrientsScreenSection;
import com.iceflyer3.pfd.ui.listener.Destroyable;
import com.iceflyer3.pfd.ui.listener.ModelPropertyValidationEventConfig;
import com.iceflyer3.pfd.ui.listener.ObservableValueListenerBinding;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.registration.MealPlanningManualRegistration;
import com.iceflyer3.pfd.ui.util.BindingUtils;
import com.iceflyer3.pfd.ui.util.TextUtils;
import com.iceflyer3.pfd.ui.widget.stepper.StepOrchestrator;
import com.iceflyer3.pfd.ui.widget.stepper.Steppable;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.util.converter.BigDecimalStringConverter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Component
@Scope("prototype")
public class MealPlanningRegisterManualUserSteppableController extends AbstractController implements Savable, Steppable, Destroyable
{
    private final MealPlanningManualRegistration userRegistration;
    private final BigDecimalStringConverter bigDecimalStringConverter;

    private final Set<ObservableValueListenerBinding<BigDecimal>> guiListenerBindings;

    private StepOrchestrator stepOrchestrator;

    @FXML
    public VBox rootContainer;

    @FXML
    public TextField tdeeCaloriesInput;

    @FXML
    public TextField dailyCaloriesInput;

    @FXML
    public TextField proteinAmountInput;

    @FXML
    public TextField carbohydratesAmountInput;

    @FXML
    public TextField totalFatsAmountInput;

    @FXML
    public Button saveButton;

    @FXML
    public Hyperlink cancelButton;

    public MealPlanningRegisterManualUserSteppableController(WindowManager windowManager)
    {
        userRegistration = new MealPlanningManualRegistration();
        bigDecimalStringConverter = new BigDecimalStringConverter();
        guiListenerBindings = new HashSet<>();
    }

    @Override
    public void initialize()
    {
        Node customNutrientsSection = new MealPlanRegistrationCustomNutrientsScreenSection(userRegistration).create();
        VBox.setMargin(customNutrientsSection, new Insets(0, 20, 0, 20));
        rootContainer.getChildren().add(rootContainer.getChildren().size() - 1, customNutrientsSection);

        // Set up controls
        tdeeCaloriesInput.setTextFormatter(TextUtils.getRequiredDecimalTextInputFormatter());
        dailyCaloriesInput.setTextFormatter(TextUtils.getRequiredDecimalTextInputFormatter());
        proteinAmountInput.setTextFormatter(TextUtils.getRequiredDecimalTextInputFormatter());
        carbohydratesAmountInput.setTextFormatter(TextUtils.getRequiredDecimalTextInputFormatter());
        totalFatsAmountInput.setTextFormatter(TextUtils.getRequiredDecimalTextInputFormatter());

        // Bind controls to data
        Bindings.bindBidirectional(tdeeCaloriesInput.textProperty(), userRegistration.tdeeCaloriesProperty(), bigDecimalStringConverter);
        Bindings.bindBidirectional(dailyCaloriesInput.textProperty(), userRegistration.dailyCaloriesProperty(), bigDecimalStringConverter);
        Bindings.bindBidirectional(proteinAmountInput.textProperty(), userRegistration.proteinAmountProperty(), bigDecimalStringConverter);
        Bindings.bindBidirectional(carbohydratesAmountInput.textProperty(), userRegistration.carbsAmountProperty(), bigDecimalStringConverter);
        Bindings.bindBidirectional(totalFatsAmountInput.textProperty(), userRegistration.totalFatsAmountProperty(), bigDecimalStringConverter);

        // Bind controls for validation
        guiListenerBindings.add(BindingUtils.bindModelObservableValueToGuiControl(new ModelPropertyValidationEventConfig<>(userRegistration, userRegistration.tdeeCaloriesProperty(), tdeeCaloriesInput, this::updateSaveValidity)));
        guiListenerBindings.add(BindingUtils.bindModelObservableValueToGuiControl(new ModelPropertyValidationEventConfig<>(userRegistration, userRegistration.dailyCaloriesProperty(), dailyCaloriesInput, this::updateSaveValidity)));
        guiListenerBindings.add(BindingUtils.bindModelObservableValueToGuiControl(new ModelPropertyValidationEventConfig<>(userRegistration, userRegistration.proteinAmountProperty(), proteinAmountInput, this::updateSaveValidity)));
        guiListenerBindings.add(BindingUtils.bindModelObservableValueToGuiControl(new ModelPropertyValidationEventConfig<>(userRegistration, userRegistration.carbsAmountProperty(), carbohydratesAmountInput, this::updateSaveValidity)));
        guiListenerBindings.add(BindingUtils.bindModelObservableValueToGuiControl(new ModelPropertyValidationEventConfig<>(userRegistration, userRegistration.totalFatsAmountProperty(), totalFatsAmountInput, this::updateSaveValidity)));
    }

    @Override
    public void initControllerData(Map<String, Object> controllerData)
    {

    }

    @Override
    protected void registerDefaultListeners()
    {
        saveButton.setOnAction(this::save);
        cancelButton.setOnAction(event -> stepOrchestrator.cancelStep());
    }

    @Override
    public void usingStepOrchestrator(StepOrchestrator stepOrchestrator)
    {
        this.stepOrchestrator = stepOrchestrator;
    }

    @Override
    public void updateSaveValidity()
    {
        saveButton.setDisable(!userRegistration.validate().wasSuccessful());
    }

    @Override
    public void save(ActionEvent event)
    {
        stepOrchestrator.completeStep(userRegistration);
    }

    @Override
    public void destroy()
    {
        guiListenerBindings.forEach(ObservableValueListenerBinding::unbind);
    }
}
