package com.iceflyer3.pfd.ui.controller.sections.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.enums.Nutrient;
import com.iceflyer3.pfd.enums.UnitOfMeasure;
import com.iceflyer3.pfd.ui.CustomNutrientRequirementConsumptionProgress;
import com.iceflyer3.pfd.ui.cell.StringConverterTableCell;
import com.iceflyer3.pfd.ui.controller.sections.ScreenSection;
import com.iceflyer3.pfd.ui.format.NutrientStringConverter;
import com.iceflyer3.pfd.ui.format.UnitOfMeasureStringConverter;
import com.iceflyer3.pfd.ui.model.api.mealplanning.MealPlanDayModel;
import com.iceflyer3.pfd.ui.model.api.mealplanning.MealPlanModel;
import com.iceflyer3.pfd.ui.model.api.mealplanning.MealPlanNutrientSuggestionModel;
import javafx.collections.FXCollections;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TitledPane;
import javafx.scene.control.cell.PropertyValueFactory;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Screen section that displays a summary of consumed vs required information for any
 * custom nutrient requirements that the user specified during meal plan creation.
 */
public class MealPlanCustomNutrientRequirementsScreenSection implements ScreenSection
{
    private final MealPlanModel mealPlan;
    private final MealPlanDayModel mealPlanDay;

    public MealPlanCustomNutrientRequirementsScreenSection(MealPlanModel mealPlan, MealPlanDayModel mealPlanDay)
    {
        this.mealPlan = mealPlan;
        this.mealPlanDay = mealPlanDay;
    }

    @Override
    public Node create()
    {
        Collection<CustomNutrientRequirementConsumptionProgress> nutrientConsumptionProgress = calculateNutrientConsumptionProgress();
        TableView<CustomNutrientRequirementConsumptionProgress> tableView = createTableView();
        tableView.setItems(FXCollections.observableArrayList(nutrientConsumptionProgress));

        return new TitledPane("Custom Nutrient Requirements", tableView);
    }

    private TableView<CustomNutrientRequirementConsumptionProgress> createTableView()
    {
        TableView<CustomNutrientRequirementConsumptionProgress> customNutrientsTable = new TableView<>();

        TableColumn<CustomNutrientRequirementConsumptionProgress, Nutrient> nutrientColumn = new TableColumn<>("Nutrient");
        nutrientColumn.setCellFactory(col -> new StringConverterTableCell<>(new NutrientStringConverter()));
        nutrientColumn.setCellValueFactory(new PropertyValueFactory<>("nutrient"));

        TableColumn<CustomNutrientRequirementConsumptionProgress, BigDecimal> consumedColumn = new TableColumn<>("Required");
        consumedColumn.setCellValueFactory(new PropertyValueFactory<>("consumedAmount"));

        TableColumn<CustomNutrientRequirementConsumptionProgress, BigDecimal> requiredColumn = new TableColumn<>("Required");
        requiredColumn.setCellValueFactory(new PropertyValueFactory<>("requiredAmount"));

        TableColumn<CustomNutrientRequirementConsumptionProgress, UnitOfMeasure> unitOfMeasureColumn = new TableColumn<>("Unit of Measure");
        unitOfMeasureColumn.setCellFactory(col -> new StringConverterTableCell<>(new UnitOfMeasureStringConverter()));
        unitOfMeasureColumn.setCellValueFactory(new PropertyValueFactory<>("unitOfMeasure"));

        customNutrientsTable.getColumns().addAll(nutrientColumn, consumedColumn, requiredColumn, unitOfMeasureColumn);

        return customNutrientsTable;
    }

    private Collection<CustomNutrientRequirementConsumptionProgress> calculateNutrientConsumptionProgress()
    {
        Set<CustomNutrientRequirementConsumptionProgress> customNutrientConsumptionProgress = new HashSet<>();
        for(MealPlanNutrientSuggestionModel customNutrient : mealPlan.getCustomNutrients())
        {
            CustomNutrientRequirementConsumptionProgress nutrientConsumptionProgress = new CustomNutrientRequirementConsumptionProgress(
                    customNutrient.getNutrient(),
                    mealPlanDay.getNutrient(customNutrient.getNutrient()),
                    customNutrient.getSuggestedAmount(),
                    customNutrient.getUnitOfMeasure()
            );
            customNutrientConsumptionProgress.add(nutrientConsumptionProgress);
        }

        return customNutrientConsumptionProgress;
    }
}
