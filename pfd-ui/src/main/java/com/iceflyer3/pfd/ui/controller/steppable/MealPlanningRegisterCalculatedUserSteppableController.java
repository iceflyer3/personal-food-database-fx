package com.iceflyer3.pfd.ui.controller.steppable;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023, 2025 Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.enums.*;
import com.iceflyer3.pfd.ui.controller.AbstractController;
import com.iceflyer3.pfd.ui.controller.Savable;
import com.iceflyer3.pfd.ui.controller.sections.mealplanning.MealPlanRegistrationCustomNutrientsScreenSection;
import com.iceflyer3.pfd.ui.format.*;
import com.iceflyer3.pfd.ui.listener.Destroyable;
import com.iceflyer3.pfd.ui.listener.ModelPropertyValidationEventConfig;
import com.iceflyer3.pfd.ui.listener.ObservableValueListenerBinding;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.registration.MealPlanningCalculatedRegistration;
import com.iceflyer3.pfd.ui.util.BindingUtils;
import com.iceflyer3.pfd.ui.util.TextUtils;
import com.iceflyer3.pfd.ui.widget.stepper.StepOrchestrator;
import com.iceflyer3.pfd.ui.widget.stepper.Steppable;
import com.iceflyer3.pfd.util.NumberUtils;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.util.converter.BigDecimalStringConverter;
import javafx.util.converter.NumberStringConverter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Component
@Scope("prototype")
public class MealPlanningRegisterCalculatedUserSteppableController
        extends AbstractController
        implements Savable, Destroyable, Steppable
{
    private UnitOfMeasure heightUom;

    private UnitOfMeasure weightUom;

    private MealPlanningCalculatedRegistration mealPlanningRegistration;

    private StepOrchestrator stepOrchestrator;
    private final BigDecimalStringConverter converter;

    private final Set<ObservableValueListenerBinding<?>> listenerBindings;

    private final ObservableList<Sex> sexOptions;
    private final ObservableList<FitnessGoal> goalOptions;
    private final ObservableList<ActivityLevel> activityLevelOptions;
    private final ObservableList<UnitOfMeasure> allowedHeightUnitOptions;
    private final ObservableList<UnitOfMeasure> allowedWeightUnitOptions;
    private final ObservableList<BasalMetabolicRateFormula> bmrFormulaOptions;

    @FXML
    public VBox rootContainer;

    @FXML
    public TextField ageInput;

    @FXML
    public ChoiceBox<Sex> sexChoiceBox;

    @FXML
    public TextField heightInput;

    @FXML
    public ChoiceBox<UnitOfMeasure> heightUomChoiceBox;

    @FXML
    public TextField weightInput;

    @FXML
    public ChoiceBox<UnitOfMeasure> weightUomChoiceBox;

    @FXML
    public TextField bodyFatPercentageInput;

    @FXML
    public ChoiceBox<FitnessGoal> goalChoiceBox;

    @FXML
    public ChoiceBox<ActivityLevel> activityLevelChoiceBox;

    @FXML
    public ChoiceBox<BasalMetabolicRateFormula> bmrFormulaChoiceBox;

    @FXML
    public TextField proteinPercentageInput;

    @FXML
    public TextField carbPercentageInput;

    @FXML
    public TextField fatsPercentageInput;

    @FXML
    public Button saveButton;

    @FXML
    public Hyperlink cancelButton;

    @FXML
    public Label proteinRangeLabel;

    @FXML
    public Label carbsRangeLabel;

    @FXML
    public Label fatsRangeLabel;

    @FXML
    public Label invalidMacroGrandTotalLabel;

    public MealPlanningRegisterCalculatedUserSteppableController() {
        converter = new BigDecimalStringConverter();

        listenerBindings = new HashSet<>();

        sexOptions = FXCollections.observableArrayList(Sex.validSexOptions());
        goalOptions = FXCollections.observableArrayList(FitnessGoal.validFitnessGoals());
        activityLevelOptions = FXCollections.observableArrayList(ActivityLevel.validActivityLevels());
        bmrFormulaOptions = FXCollections.observableArrayList(BasalMetabolicRateFormula.validBmrFormulas());

        allowedWeightUnitOptions = FXCollections.observableArrayList();
        allowedWeightUnitOptions.add(UnitOfMeasure.POUND);
        allowedWeightUnitOptions.add(UnitOfMeasure.KILOGRAM);

        allowedHeightUnitOptions = FXCollections.observableArrayList();
        allowedHeightUnitOptions.add(UnitOfMeasure.INCHES);
        allowedHeightUnitOptions.add(UnitOfMeasure.CENTIMETERS);

        heightUom = UnitOfMeasure.INCHES;
        weightUom = UnitOfMeasure.POUND;
    }

    @Override
    public void initialize()
    {
        mealPlanningRegistration = new MealPlanningCalculatedRegistration();

        Node customNutrientsSection = new MealPlanRegistrationCustomNutrientsScreenSection(mealPlanningRegistration).create();
        VBox.setMargin(customNutrientsSection, new Insets(0, 20, 0, 20));
        int indexToAdd = rootContainer.getChildren().size() - 1;
        rootContainer.getChildren().add(indexToAdd, customNutrientsSection);

        // Text fields
        ageInput.setTextFormatter(TextUtils.getRequiredDecimalTextInputFormatter());
        heightInput.setTextFormatter(TextUtils.getRequiredDecimalTextInputFormatter());
        weightInput.setTextFormatter(TextUtils.getRequiredDecimalTextInputFormatter());
        proteinPercentageInput.setTextFormatter(TextUtils.getRequiredDecimalTextInputFormatter());
        carbPercentageInput.setTextFormatter(TextUtils.getRequiredDecimalTextInputFormatter());
        fatsPercentageInput.setTextFormatter(TextUtils.getRequiredDecimalTextInputFormatter());

        bodyFatPercentageInput.setTextFormatter(TextUtils.getOptionalDecimalTextInputFormatter());

        // Dropdowns
        sexChoiceBox.setItems(sexOptions);
        sexChoiceBox.setConverter(new SexStringConverter());

        goalChoiceBox.setItems(goalOptions);
        goalChoiceBox.setConverter(new FitnessGoalStringConverter());
        ObservableValueListenerBinding<FitnessGoal> goalListenerBinding = new ObservableValueListenerBinding<>(goalChoiceBox.valueProperty());
        goalListenerBinding.bindChangeListener(this::updateMacronutrientRestrictionsForGoal);
        listenerBindings.add(goalListenerBinding);

        activityLevelChoiceBox.setItems(activityLevelOptions);
        activityLevelChoiceBox.setConverter(new ActivityLevelStringConverter());

        bmrFormulaChoiceBox.setItems(bmrFormulaOptions);
        bmrFormulaChoiceBox.setConverter(new BasalMetabolicRateFormulaStringConverter());

        weightUomChoiceBox.setItems(allowedWeightUnitOptions);
        weightUomChoiceBox.setConverter(new UnitOfMeasureStringConverter());
        weightUomChoiceBox.getSelectionModel().selectFirst();

        heightUomChoiceBox.setItems(allowedHeightUnitOptions);
        heightUomChoiceBox.setConverter(new UnitOfMeasureStringConverter());
        heightUomChoiceBox.getSelectionModel().selectFirst();

        initBindings();
        updateSaveValidity();
    }

    @Override
    public void initControllerData(Map<String, Object> dialogData) { }

    @Override
    protected void registerDefaultListeners() {
        saveButton.setOnAction(this::save);
        cancelButton.setOnAction(this::cancel);
    }

    private void initBindings()
    {
        // Choice Boxes
        Bindings.bindBidirectional(sexChoiceBox.valueProperty(), mealPlanningRegistration.sexProperty());
        Bindings.bindBidirectional(goalChoiceBox.valueProperty(), mealPlanningRegistration.fitnessGoalProperty());
        Bindings.bindBidirectional(activityLevelChoiceBox.valueProperty(), mealPlanningRegistration.activityLevelProperty());
        Bindings.bindBidirectional(bmrFormulaChoiceBox.valueProperty(), mealPlanningRegistration.bmrFormulaProperty());

        // Text fields
        Bindings.bindBidirectional(heightInput.textProperty(), mealPlanningRegistration.heightProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(weightInput.textProperty(), mealPlanningRegistration.weightProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(ageInput.textProperty(), mealPlanningRegistration.ageProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(bodyFatPercentageInput.textProperty(), mealPlanningRegistration.bodyFactPercentageProperty(), converter);
        Bindings.bindBidirectional(proteinPercentageInput.textProperty(), mealPlanningRegistration.proteinPercentProperty(), converter);
        Bindings.bindBidirectional(carbPercentageInput.textProperty(), mealPlanningRegistration.carbsPercentProperty(), converter);
        Bindings.bindBidirectional(fatsPercentageInput.textProperty(), mealPlanningRegistration.fatsPercentProperty(), converter);

        // Validation bindings
        listenerBindings.add(BindingUtils.bindModelObservableValueToGuiControl(new ModelPropertyValidationEventConfig<>(mealPlanningRegistration, mealPlanningRegistration.ageProperty(), ageInput, this::updateSaveValidity)));
        listenerBindings.add(BindingUtils.bindModelObservableValueToGuiControl(new ModelPropertyValidationEventConfig<>(mealPlanningRegistration, mealPlanningRegistration.heightProperty(), heightInput, this::updateSaveValidity)));
        listenerBindings.add(BindingUtils.bindModelObservableValueToGuiControl(new ModelPropertyValidationEventConfig<>(mealPlanningRegistration, mealPlanningRegistration.weightProperty(), weightInput, this::updateSaveValidity)));
        listenerBindings.add(BindingUtils.bindModelObservableValueToGuiControl(new ModelPropertyValidationEventConfig<>(mealPlanningRegistration, mealPlanningRegistration.proteinPercentProperty(), proteinPercentageInput, this::updateSaveValidity)));
        listenerBindings.add(BindingUtils.bindModelObservableValueToGuiControl(new ModelPropertyValidationEventConfig<>(mealPlanningRegistration, mealPlanningRegistration.carbsPercentProperty(), carbPercentageInput, this::updateSaveValidity)));
        listenerBindings.add(BindingUtils.bindModelObservableValueToGuiControl(new ModelPropertyValidationEventConfig<>(mealPlanningRegistration, mealPlanningRegistration.fatsPercentProperty(), fatsPercentageInput, this::updateSaveValidity)));

        // Height is stored as kgs in the db. So we have to listen to this to know if we need to convert upon save.
        ObservableValueListenerBinding<UnitOfMeasure> heightListenerBinding = new ObservableValueListenerBinding<>(heightUomChoiceBox.valueProperty());
        heightListenerBinding.bindChangeListener((observable, oldValue, newValue) -> heightUom = newValue);
        listenerBindings.add(heightListenerBinding);

        ObservableValueListenerBinding<UnitOfMeasure> weightListenerBinding = new ObservableValueListenerBinding<>(weightUomChoiceBox.valueProperty());
        weightListenerBinding.bindChangeListener((observable, oldValue, newValue) -> weightUom = newValue);
        listenerBindings.add(weightListenerBinding);
    }

    @Override
    public void updateSaveValidity()
    {
        // The properties can actually be null if no value is entered in the bidirectionally bound text field. So we must account for that.
        BigDecimal proteinPercent = mealPlanningRegistration.getProteinPercent() == null ? BigDecimal.ZERO : mealPlanningRegistration.getProteinPercent();
        BigDecimal carbsPercent = mealPlanningRegistration.getCarbsPercent() == null ? BigDecimal.ZERO : mealPlanningRegistration.getCarbsPercent();
        BigDecimal fatsPercent = mealPlanningRegistration.getFatsPercent() == null ? BigDecimal.ZERO : mealPlanningRegistration.getFatsPercent();

        BigDecimal macroGrandTotal = proteinPercent.add(carbsPercent.add(fatsPercent));
        invalidMacroGrandTotalLabel.setVisible(macroGrandTotal.compareTo(BigDecimal.valueOf(100)) != 0);
        saveButton.setDisable(!mealPlanningRegistration.validate().wasSuccessful());
    }

    @Override
    public void save(ActionEvent event)
    {
        if (heightUom == UnitOfMeasure.INCHES)
        {
            // Convert to CM
            mealPlanningRegistration.setHeight(mealPlanningRegistration.getHeight().multiply(NumberUtils.INCHES_TO_CM_CONVERSION_FACTOR));
        }

        if (weightUom == UnitOfMeasure.POUND)
        {
            // Convert to KG
            mealPlanningRegistration.setWeight(mealPlanningRegistration.getWeight().multiply(NumberUtils.POUND_TO_KG_CONVERSION_FACTOR));
        }

        stepOrchestrator.completeStep(mealPlanningRegistration);
    }

    @Override
    public void destroy()
    {
        listenerBindings.forEach(ObservableValueListenerBinding::unbind);
        listenerBindings.clear();
    }

    @Override
    public void usingStepOrchestrator(StepOrchestrator stepOrchestrator)
    {
        this.stepOrchestrator = stepOrchestrator;
    }

    private void cancel(ActionEvent event)
    {
        stepOrchestrator.cancelStep();
    }

    private void updateMacronutrientRestrictionsForGoal(ObservableValue<? extends FitnessGoal> observable, FitnessGoal oldValue, FitnessGoal newValue)
    {
        // These restrictions are based upon https://www.bodybuilding.com/content/macro-math-3-keys-to-dialing-in-your-macro-ratios.html
        if (newValue != oldValue)
        {
            switch (newValue)
            {
                case GAIN ->
                {
                    proteinRangeLabel.setText(String.format("Range: %s%% - %s%%", 25, 35));
                    carbsRangeLabel.setText(String.format("Range: %s%% - %s%%", 40, 60));
                    fatsRangeLabel.setText(String.format("Range: %s%% - %s%%", 15, 25));
                }
                case LOSE ->
                {
                    proteinRangeLabel.setText(String.format("Range: %s%% - %s%%", 40, 50));
                    carbsRangeLabel.setText(String.format("Range: %s%% - %s%%", 10, 30));
                    fatsRangeLabel.setText(String.format("Range: %s%% - %s%%", 30, 40));
                }
                case MAINTAIN ->
                {
                    proteinRangeLabel.setText(String.format("Range: %s%% - %s%%", 25, 35));
                    carbsRangeLabel.setText(String.format("Range: %s%% - %s%%", 30, 50));
                    fatsRangeLabel.setText(String.format("Range: %s%% - %s%%", 25, 35));
                }
            }
        }
    }
}
