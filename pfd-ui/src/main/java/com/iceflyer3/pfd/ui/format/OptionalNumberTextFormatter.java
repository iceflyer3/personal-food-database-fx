package com.iceflyer3.pfd.ui.format;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import javafx.scene.control.TextFormatter;
import javafx.util.converter.NumberStringConverter;

import java.math.BigDecimal;
import java.util.function.UnaryOperator;

public class OptionalNumberTextFormatter implements UnaryOperator<TextFormatter.Change> {

    private final NumberStringConverter numberStringConverter;
    private final String regex;

    public OptionalNumberTextFormatter(boolean allowDecimals)
    {
        this.regex = allowDecimals ? "^\\d+\\.?\\d*$" : "^\\d+$";
        this.numberStringConverter = new NumberStringConverter();
    }

    @Override
    public TextFormatter.Change apply(TextFormatter.Change change) {
        if (change.isAdded() || change.isReplaced())
        {
            String fullText = change.getControlNewText();
            boolean isValid = fullText.matches(regex);

            if (isValid)
            {
                return change;
            }
            else
            {
                return null;
            }
        }

        // If text was deleted then we have no need to apply the filter
        return change;
    }
}
