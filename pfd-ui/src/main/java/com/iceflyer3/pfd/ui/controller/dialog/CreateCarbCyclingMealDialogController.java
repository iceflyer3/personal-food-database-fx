package com.iceflyer3.pfd.ui.controller.dialog;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.constants.CssConstants;
import com.iceflyer3.pfd.enums.CarbCyclingMealRelation;
import com.iceflyer3.pfd.enums.MealType;
import com.iceflyer3.pfd.exception.InvalidApplicationStateException;
import com.iceflyer3.pfd.ui.controller.Savable;
import com.iceflyer3.pfd.ui.format.MealTypeStringConverter;
import com.iceflyer3.pfd.ui.listener.Destroyable;
import com.iceflyer3.pfd.ui.listener.ObservableValueListenerBinding;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import com.iceflyer3.pfd.ui.model.api.mealplanning.carbcycling.CarbCyclingMealConfigurationModel;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.MealPlanModelProxy;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.MealPlanningProxyFactory;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.carbcycling.CarbCyclingDayModelProxy;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.carbcycling.CarbCyclingMealModelProxy;
import com.iceflyer3.pfd.ui.model.proxy.mealplanning.carbcycling.CarbCyclingPlanModelProxy;
import com.iceflyer3.pfd.ui.util.BindingUtils;
import com.iceflyer3.pfd.ui.util.TextUtils;
import com.iceflyer3.pfd.ui.listener.ModelPropertyValidationEventConfig;
import com.iceflyer3.pfd.validation.NumericRangeDatumRestriction;
import com.iceflyer3.pfd.validation.ObservableValuePropertyRestrictionBinding;
import com.iceflyer3.pfd.validation.ValidationResult;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.util.converter.BigDecimalStringConverter;
import javafx.util.converter.IntegerStringConverter;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CreateCarbCyclingMealDialogController extends AbstractDialogController<CarbCyclingMealModelProxy> implements Savable, Destroyable
{
    private final static Logger LOG = LoggerFactory.getLogger(CreateCarbCyclingMealDialogController.class);

    public static final String DATA_FOR_DAY = "forDay";
    public static final String DATA_FOR_MEAL_PLAN = "forMealPlan";
    public static final String DATA_FOR_CARB_CYCLING_PLAN = "forCarbCyclingPlan";
    public static final String DATA_TAKEN_MEAL_NUMBERS = "takenMealNumbers";
    public static final String DATA_POST_WORKOUT_MEAL_NUMBER = "postWorkoutMealNumber";

    private final NumericRangeDatumRestriction carbConsumptionRangeValidator;
    private final MealPlanningProxyFactory mealPlanningProxyFactory;
    private final Set<ObservableValueListenerBinding<?>> listenerBindings;

    // Dynamically set on dialog data load
    private CarbCyclingPlanModelProxy forPlan;
    private CarbCyclingMealModelProxy newMeal;
    private int postWorkoutMealNumber;
    private List<Integer> takenMealNumbers;
    private CarbCyclingDayModelProxy forDay;

    // Dynamically added if the user can enter a carb consumption percentage
    private TextField carbConsumptionPercentageInput;
    private Label carbConsumptionRangeLabel;
    private ObservableValuePropertyRestrictionBinding<BigDecimal> carbConsumptionPercentageRestriction;

    @FXML
    public VBox rootContainer;

    @FXML
    public VBox contentContainer;

    @FXML
    public TextField mealNameInput;

    @FXML
    public ChoiceBox<MealType> mealTypeChoiceBox;

    @FXML
    public ChoiceBox<Integer> mealNumberChoiceBox;

    @FXML
    public Button saveButton;

    @FXML
    public Hyperlink cancelButton;

    public CreateCarbCyclingMealDialogController(WindowManager windowManager, MealPlanningProxyFactory mealPlanningProxyFactory)
    {
        super(windowManager);
        this.carbConsumptionRangeValidator = new NumericRangeDatumRestriction(0, 0);
        this.mealPlanningProxyFactory = mealPlanningProxyFactory;
        this.listenerBindings = new HashSet<>();
    }


    @Override
    public void initialize()
    {

    }

    @Override
    protected void registerDefaultListeners()
    {
        saveButton.setOnAction(this::save);
        cancelButton.setOnAction(this::cancel);
    }

    @Override
    public void initControllerData(Map<String, Object> dialogData)
    {
        forPlan = (CarbCyclingPlanModelProxy) dialogData.get(DATA_FOR_CARB_CYCLING_PLAN);
        forDay = (CarbCyclingDayModelProxy) dialogData.get(DATA_FOR_DAY);
        takenMealNumbers = (List<Integer>) dialogData.get(DATA_TAKEN_MEAL_NUMBERS);
        postWorkoutMealNumber = (int) dialogData.get(DATA_POST_WORKOUT_MEAL_NUMBER);
        MealPlanModelProxy forMealPlan = (MealPlanModelProxy) dialogData.get(DATA_FOR_MEAL_PLAN);

        mealPlanningProxyFactory.getCarbCyclingMealForDayById(null, forMealPlan, forDay, (wasSuccessful, proxyResult) -> {
            if (wasSuccessful)
            {
                newMeal = proxyResult;
                initScreenControls();
            }
            else
            {
                // Theoretically this should never actually happen
                windowManager.openFatalErrorDialog();
            }
        });
    }

    @Override
    public void updateSaveValidity()
    {
        ValidationResult validationResult = ValidationResult.flatten(carbConsumptionPercentageRestriction.applyRestrictions(), newMeal.validate());
        this.saveButton.setDisable(!validationResult.wasSuccessful());
    }

    @Override
    public void save(ActionEvent event)
    {
        callback.complete(newMeal);
        windowManager.close(WindowManager.DIALOG_WINDOW);
    }

    @Override
    public void destroy()
    {
        listenerBindings.forEach(binding -> binding.unbind());
        listenerBindings.clear();
    }

    private void cancel(ActionEvent event)
    {
        windowManager.close(WindowManager.DIALOG_WINDOW);
    }

    private void initScreenControls()
    {
        // Meal name input setup
        Bindings.bindBidirectional(mealNameInput.textProperty(), newMeal.nameProperty());
        listenerBindings.add(BindingUtils.bindModelObservableValueToGuiControl(new ModelPropertyValidationEventConfig<>(newMeal, newMeal.nameProperty(), mealNameInput, this::updateSaveValidity)));

        // Meal Number choicebox setup
        ObservableList<Integer> remainingMealNumbers = getRemainingMealNumbersList();
        int defaultNextMealNumber = remainingMealNumbers.stream().mapToInt(mealNumber -> mealNumber).min().orElseThrow(() -> new InvalidApplicationStateException("Could not find a default meal number when attempting to create a new carb cycling meal"));
        newMeal.setMealNumber(defaultNextMealNumber);
        LOG.debug("Populating meal number choice box with {} items", remainingMealNumbers.size());
        mealNumberChoiceBox.setItems(remainingMealNumbers);
        mealNumberChoiceBox.setConverter(new IntegerStringConverter());
        mealNumberChoiceBox.getSelectionModel().select(defaultNextMealNumber);
        Bindings.bindBidirectional(mealNumberChoiceBox.valueProperty(), newMeal.mealNumberProperty().asObject());

        // Meal Type choicebox setup
        // The pre and post-work meal type options aren't applicable for non-training days
        if (forDay.isTrainingDay())
        {
            mealTypeChoiceBox.setItems(FXCollections.observableArrayList(MealType.values()));
            newMeal.setMealType(MealType.POST_WORKOUT);
        }
        else
        {
            List<MealType> mealTypes = Arrays.stream(MealType.values()).filter(mealType -> mealType == MealType.REGULAR).collect(Collectors.toList());
            mealTypeChoiceBox.setItems(FXCollections.observableArrayList(mealTypes));
            newMeal.setMealType(MealType.REGULAR);
        }
        mealTypeChoiceBox.setConverter(new MealTypeStringConverter());
        Bindings.bindBidirectional(mealTypeChoiceBox.valueProperty(), newMeal.mealTypeProperty());

        // Carb consumption percentage input setup
        if (forPlan.hasConfiguredMeals())
        {
            Label carbConsumptionLabel = new Label("Carb Consumption Percentage");
            carbConsumptionPercentageInput = new TextField();
            carbConsumptionPercentageInput.setTextFormatter(TextUtils.getOptionalDecimalTextInputFormatter());

            /*
             * The carb consumption percentage is a bit odd as it is an invariant that needs to hold true for the dialog but also lives outside of the meal model itself.
             * So we have to enforce the invariant here instead of in the model as it isn't actually an invariant of the model itself but is still important in the larger
             * picture.
             */
            Bindings.bindBidirectional(carbConsumptionPercentageInput.textProperty(), newMeal.carbConsumptionPercentageProperty(), new BigDecimalStringConverter());
            carbConsumptionPercentageRestriction = new ObservableValuePropertyRestrictionBinding<>(newMeal.carbConsumptionPercentageProperty());
            carbConsumptionPercentageRestriction.bindRestriction(carbConsumptionRangeValidator);
            listenerBindings.add(BindingUtils.bindTextInputToChangeListener(carbConsumptionPercentageInput, (observable, oldValue, newValue) -> {

                ValidationResult result = carbConsumptionPercentageRestriction.applyRestrictions();

                if (result.wasSuccessful())
                {
                    carbConsumptionPercentageInput.getStyleClass().remove(CssConstants.CLASS_REQUIRED);
                }
                else
                {
                    if (!carbConsumptionPercentageInput.getStyleClass().contains(CssConstants.CLASS_REQUIRED))
                    {
                        carbConsumptionPercentageInput.getStyleClass().add(CssConstants.CLASS_REQUIRED);
                    }
                }

                updateSaveValidity();
            }));

            carbConsumptionRangeLabel = new Label();

            contentContainer.getChildren().add(carbConsumptionLabel);
            contentContainer.getChildren().add(carbConsumptionPercentageInput);
            contentContainer.getChildren().add(carbConsumptionRangeLabel);
        }

        /*
         * Changes to the meal number can affect the valid carb consumption percentage range
         *
         * So we must listen for changes to this field and re-calculate the carb cycling control
         * restrictions
         */
        ChangeListener<Number> mealNumberChangeListener = (observableValue, oldValue, newValue) -> {

            refreshCarbCyclingInputControlRestrictions();
            updateSaveValidity();
        };
        ObservableValueListenerBinding<Number> mealNumBinding = new ObservableValueListenerBinding<>(newMeal.mealNumberProperty());
        mealNumBinding.bindChangeListener(mealNumberChangeListener);
        listenerBindings.add(mealNumBinding);

        // Set initial carb cycling restrictions
        refreshCarbCyclingInputControlRestrictions();

        // Ensure valid screen state upon first load
        updateSaveValidity();
    }

    private ObservableList<Integer> getRemainingMealNumbersList()
    {
        ArrayList<Integer> remainingMealNumbers = new ArrayList<>();
        for(int i = 1; i <= forPlan.getMealsPerDay(); i++)
        {
            if (!takenMealNumbers.contains(i))
            {
                remainingMealNumbers.add(i);
            }
        }
        return FXCollections.observableList(remainingMealNumbers);
    }

    private void refreshCarbCyclingInputControlRestrictions()
    {
        /*
         * Where applicable, the carb consumption percentage required for a carb cycling meal must
         * fall within a certain range.
         *
         * This isn't an invariant of the individual carb cycling meal itself, but is instead collectively
         * derived from all the meals in the day.
         *
         * When carb cycling all the meals must be considered together as they are all pieces of the
         * puzzle of the day.
         */
        if (forPlan.hasConfiguredMeals())
        {
            if (forDay.isTrainingDay())
            {
                refreshRestrictionsForTrainingDay();
            }
            else
            {
                refreshRestrictionsForNonTrainingDay();
            }

            String carbRangeLabelString = String.format("Valid range is %s to %s", carbConsumptionRangeValidator.getMinValue(), carbConsumptionRangeValidator.getMaxValue());
            carbConsumptionRangeLabel.setText(carbRangeLabelString);
        }
    }

    private void refreshRestrictionsForNonTrainingDay()
    {
        /*
         * If it is not a training day then entering carb consumption percentages is
         * only valid for the first meal of the day. After which carbs are evenly split
         * between all remaining meals by default.
         */
        int selectedMealNumber = mealNumberChoiceBox.getSelectionModel().getSelectedItem();
        CarbCyclingMealConfigurationModel mealConfig = forPlan
                .getMealConfigurations()
                .stream()
                .filter(config -> !config.isForTrainingDay() && config.getPostWorkoutMealOffset() == selectedMealNumber)
                .findFirst()
                .get();

        carbConsumptionRangeValidator.setRange(mealConfig.getCarbConsumptionMinimumPercent().doubleValue(), mealConfig.getCarbConsumptionMaximumPercent().doubleValue());
    }

    private void refreshRestrictionsForTrainingDay()
    {
        int selectedMealNumber = mealNumberChoiceBox.getSelectionModel().getSelectedItem();

        /*
         * When creating a meal for a training day the first meal entered by the user _must_
         * be the post-workout meal as this informs how to calculate other parts of the other
         * meals for the day.
         */
        if (postWorkoutMealNumber == -1)
        {
            mealTypeChoiceBox.getSelectionModel().select(MealType.POST_WORKOUT);
            mealTypeChoiceBox.setDisable(true);

            CarbCyclingMealConfigurationModel mealConfig = forPlan
                    .getMealConfigurations()
                    .stream()
                    .filter(config -> config.getPostWorkoutMealOffset() == 0 && config.isForTrainingDay())
                    .findFirst()
                    .get();

            carbConsumptionRangeValidator.setRange(mealConfig.getCarbConsumptionMinimumPercent().doubleValue(), mealConfig.getCarbConsumptionMaximumPercent().doubleValue());
        }
        else
        {
            CarbCyclingMealRelation mealRelation = selectedMealNumber > postWorkoutMealNumber ? CarbCyclingMealRelation.AFTER : CarbCyclingMealRelation.BEFORE;
            int mealOffset = Math.abs(postWorkoutMealNumber - selectedMealNumber);
            CarbCyclingMealConfigurationModel mealConfig = forPlan
                    .getMealConfigurations()
                    .stream()
                    .filter(config ->
                            config.isForTrainingDay()
                                    && config.getPostWorkoutMealOffset() == mealOffset
                                    && config.getPostWorkoutMealRelation() == mealRelation)
                    .findFirst()
                    .get();
            carbConsumptionRangeValidator.setRange(mealConfig.getCarbConsumptionMinimumPercent().doubleValue(), mealConfig.getCarbConsumptionMaximumPercent().doubleValue());
        }
    }
}
