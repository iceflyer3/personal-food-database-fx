package com.iceflyer3.pfd.ui.controller.foods;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.google.common.eventbus.EventBus;
import com.iceflyer3.pfd.constants.ApplicationSections;
import com.iceflyer3.pfd.constants.FontAwesomeIcons;
import com.iceflyer3.pfd.constants.SceneSets;
import com.iceflyer3.pfd.ui.controller.sections.*;
import com.iceflyer3.pfd.ui.event.PublishBreadcrumbEvent;
import com.iceflyer3.pfd.ui.controller.AbstractTaskController;
import com.iceflyer3.pfd.ui.controller.HostStageAware;
import com.iceflyer3.pfd.ui.controller.Savable;
import com.iceflyer3.pfd.ui.listener.Destroyable;
import com.iceflyer3.pfd.ui.listener.ObservableListListenerBinding;
import com.iceflyer3.pfd.ui.listener.ObservableValueListenerBinding;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import com.iceflyer3.pfd.ui.model.proxy.food.ComplexFoodModelProxy;
import com.iceflyer3.pfd.ui.model.proxy.food.FoodProxyFactory;
import com.iceflyer3.pfd.ui.model.proxy.food.ReadOnlyFoodModelProxy;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.ComplexFoodViewModel;
import com.iceflyer3.pfd.ui.util.BindingUtils;
import com.iceflyer3.pfd.ui.util.LayoutUtils;
import com.iceflyer3.pfd.ui.util.builder.ActionNodeBuilder;
import com.iceflyer3.pfd.ui.util.builder.NotificationBuilder;
import com.iceflyer3.pfd.ui.util.ScreenLoadEventUtils;
import com.iceflyer3.pfd.ui.listener.ModelPropertyValidationEventConfig;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ComposeComplexFoodController extends AbstractTaskController implements HostStageAware, Savable, Destroyable
{
    private static final Logger LOG = LoggerFactory.getLogger(ComposeComplexFoodController.class);

    private final WindowManager windowManager;
    private final FoodProxyFactory foodProxyFactory;
    private final Set<ObservableListListenerBinding<?>> listListenerBindings;

    private ObservableValueListenerBinding<String> nameInputListenerBinding;
    private ComplexFoodModelProxy complexFood;
    private Button saveButton;
    private String hostingStageName;

    @FXML
    public VBox rootContainer;

    @FXML
    public Label screenLabel;

    @FXML
    public AnchorPane buttonContainerAnchorPane;

    @FXML
    public VBox basicInformationPaneContainer;

    @FXML
    public TextField nameInput;

    public ComposeComplexFoodController(EventBus eventBus,
                                        WindowManager windowManager,
                                        NotificationBuilder notificationBuilder,
                                        ScreenLoadEventUtils loadEventBuilder,
                                        TaskExecutor taskExecutor,
                                        FoodProxyFactory foodProxyFactory)
    {
        super(eventBus, notificationBuilder, loadEventBuilder, taskExecutor);

        this.windowManager = windowManager;
        this.foodProxyFactory = foodProxyFactory;
        this.listListenerBindings = new HashSet<>();
    }

    @Override
    public void initialize() {
        LOG.debug("ComposeComplexFoodController is initializing...");

        VBox.setMargin(screenLabel, LayoutUtils.getHeaderLabelMarginInsets());

        // Setup Save Button
        saveButton = ActionNodeBuilder
                .createIconButton("SAVE", FontAwesomeIcons.SAVE)
                .withAction(this::save)
                .build();
        saveButton.setDisable(true);
        AnchorPane.setRightAnchor(saveButton, 0.0);
        buttonContainerAnchorPane.getChildren().add(saveButton);
    }

    @Override
    public void initControllerData(Map<String, Object> controllerData) {
        UserViewModel user = (UserViewModel) controllerData.get(UserViewModel.class.getName());

        // Loading an existing complex food
        if (controllerData.containsKey(ReadOnlyFoodModelProxy.class.getName()))
        {
            ReadOnlyFoodModelProxy foodToLoad = (ReadOnlyFoodModelProxy) controllerData.get(ReadOnlyFoodModelProxy.class.getName());
            loadEventBuilder.emitBegin("Searching for complex food details", hostingStageName);
            // First load the existing food
            foodProxyFactory.getComplexFood(foodToLoad.getId(), user, (wasSuccessful, proxyResult) -> {
                if (wasSuccessful)
                {
                    proxyResult.loadIngredients((ingredientLoadSuccessful, resultMessage) -> {
                        if (ingredientLoadSuccessful)
                        {
                            processDataLoad(proxyResult);
                        }
                        else
                        {
                            notificationBuilder.create(resultMessage, hostingStageName).show();
                        }
                    });
                }
                else
                {
                    notificationBuilder.create("An error has occurred while attempting to load the details for the complex food", hostingStageName).show();
                }
            });
        }
        // Creating a new food from the view recipe screen?
        else if (controllerData.containsKey(ComplexFoodViewModel.class.getName()))
        {
            // If we're creating a new complex food from a recipe
            ComplexFoodViewModel complexFoodFromRecipe = (ComplexFoodViewModel) controllerData.get(ComplexFoodViewModel.class.getName());
            foodProxyFactory.getComplexFoodFromModel(complexFoodFromRecipe, user, (wasSuccessful, proxyResult) -> {
                if (wasSuccessful)
                {
                    processDataLoad(proxyResult);
                }
            });
        }
        // Creating a new complex food from scratch
        else
        {
            foodProxyFactory.getComplexFood(null, user, (wasSuccessful, proxyResult) -> {
                if (wasSuccessful)
                {
                    processDataLoad(proxyResult);
                }
                else
                {
                    // Theoretically this should never actually happen
                    windowManager.openFatalErrorDialog();
                }
            });
        }
    }

    @Override
    public void initHostingStageName(String stageName) {
        hostingStageName = stageName;
        PublishBreadcrumbEvent landingScreenBreadcrumb = new PublishBreadcrumbEvent("Compose Complex Food", ApplicationSections.FOOD_DATABASE, SceneSets.COMPOSE_COMPLEX_FOOD, hostingStageName, 2);
        eventBus.post(landingScreenBreadcrumb);
    }

    @Override
    public void updateSaveValidity() {
        saveButton.setDisable(!complexFood.validate().wasSuccessful());
    }

    @Override
    public void save(ActionEvent event) {
        complexFood.isUnique((wasSuccessful, isUnique) -> {
            if (wasSuccessful)
            {
                if (isUnique)
                {
                    notificationBuilder.create("Saving the complex food...", hostingStageName).show();
                    complexFood.save((wasSaveSuccessful, resultMessage) -> notificationBuilder.create(resultMessage, hostingStageName).show());
                }
                else
                {
                    notificationBuilder.create("A complex food with the same name and nutritional profile already exists.", hostingStageName).show();
                }
            }
            else
            {
                notificationBuilder.create("Failed to save the food. An error has occurred while verifying the uniqueness of the food.", hostingStageName).show();
            }
        });
    }

    @Override
    protected void registerDefaultListeners() { }

    @Override
    public void destroy()
    {
        nameInputListenerBinding.unbind();
        listListenerBindings.forEach(binding -> binding.unbind());
        listListenerBindings.clear();
    }

    private void processDataLoad(ComplexFoodModelProxy loadedFood)
    {
        LOG.debug("Loaded a complex food of {}", loadedFood);
        complexFood = loadedFood;

        // Register listeners
        Bindings.bindBidirectional(nameInput.textProperty(), complexFood.nameProperty());
        nameInputListenerBinding = BindingUtils.bindModelObservableValueToGuiControl(new ModelPropertyValidationEventConfig<>(complexFood, complexFood.nameProperty(), nameInput, this::updateSaveValidity));

        listListenerBindings.add(BindingUtils.bindObservableListToChangeListener(complexFood.getServingSizes(), (change) -> updateSaveValidity()));
        listListenerBindings.add(BindingUtils.bindObservableListToChangeListener(complexFood.getIngredients(), (change) -> updateSaveValidity()));

        // Load the data for the autocomplete. This must be loaded before we can construct the ingredients list
        foodProxyFactory.getAllFoods((wasSuccessful, foodsListResult) -> {
            if (wasSuccessful)
            {
                if (complexFood.getId() != null)
                {
                    foodsListResult.removeIf(food -> food.getId().equals(complexFood.getId()));
                }

                // After autocomplete data has loaded then we can create the needed ScreenSections
                // and populate the screen
                ScreenSection servingSizesSection = new EditServingSizeScreenSection<>(complexFood);
                ScreenSection ingredientsListSection = new EditIngredientScreenSection<>(foodsListResult, complexFood);
                ScreenSection macroBreakdownSection = new MacronutrientBreakdownScreenSection<>(complexFood);
                ScreenSection microBreakdownSection = new MicronutrientBreakdownScreenSection<>(complexFood);
                rootContainer.getChildren().addAll(
                        servingSizesSection.create(),
                        ingredientsListSection.create(),
                        macroBreakdownSection.create(),
                        microBreakdownSection.create()
                );

                loadEventBuilder.emitCompleted(hostingStageName);
            }
            else
            {
                notificationBuilder.create("An error has occurred while loading the food data", hostingStageName).show();
            }
        });
    }
}
