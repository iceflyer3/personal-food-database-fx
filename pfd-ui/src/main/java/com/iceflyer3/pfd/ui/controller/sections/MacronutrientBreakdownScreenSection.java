package com.iceflyer3.pfd.ui.controller.sections;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.constants.CssConstants;
import com.iceflyer3.pfd.ui.listener.Destroyable;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.api.NutritionalSummaryReporter;
import com.iceflyer3.pfd.ui.model.api.owner.IngredientReader;
import com.iceflyer3.pfd.ui.util.LayoutUtils;
import javafx.beans.binding.Bindings;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TitledPane;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import java.math.BigDecimal;
import java.util.List;

/**
 * This screen section is a Titled Pane that contains two tables that detail the breakdown of
 * macronutrient information. There is one table for Grand Total macronutrients and one table
 * for per ingredient macronutrient information.
 *
 * In the event the entity on which this section reports doesn't support ingredients then the
 * per ingredient section will not be shown
 *
 * @param <T> The concrete type of the entity this pane reports on
 */
public class MacronutrientBreakdownScreenSection<T extends NutritionalSummaryReporter> implements ScreenSection, Destroyable
{
    private final T entity;

    private final Label caloriesContent;
    private final Label proteinContent;
    private final Label carbsContent;
    private final Label sugarsContent;
    private final Label fiberContent;
    private final Label totalFatsContent;
    private final Label transFatContent;
    private final Label monoFatContent;
    private final Label polyFatContent;
    private final Label satFatContent;

    /**
     * Creates a new macronutrient breakdown pane
     *
     * @param entity The entity this screen section will display the information of
     */
    public MacronutrientBreakdownScreenSection(T entity)
    {
        this.entity = entity;

        caloriesContent = new Label();
        proteinContent = new Label();
        carbsContent = new Label();
        sugarsContent = new Label();
        fiberContent = new Label();
        totalFatsContent = new Label();
        transFatContent = new Label();
        monoFatContent = new Label();
        polyFatContent = new Label();
        satFatContent = new Label();
    }

    /*
     * Though there are actually differences in content and what precisely they display both macro
     * and micro sections share the same layout so this gets detected as duplicate code
     *
     * Technically the warning isn't wrong. We could base class to avoid the duplication. But
     * I don't want or see any real need to do that here.
     */
    @SuppressWarnings("DuplicatedCode")
    @Override
    public Node create()
    {
        TitledPane macroBreakdownPane = new TitledPane();
        macroBreakdownPane.setText("Macronutrient Breakdown");
        VBox.setMargin(macroBreakdownPane, LayoutUtils.getSectionElementVerticalSpacing());

        VBox paneContentContainer = new VBox(10);

        HBox grandTotalsHeader = getSubSectionHeader("GRAND TOTALS");
        GridPane grandTotalsGridPane = setupGrandTotalsGrid();
        paneContentContainer.getChildren().addAll(grandTotalsHeader, grandTotalsGridPane);

        // Ingredients section isn't shown if there isn't support for them
        if (entity instanceof IngredientReader ingredientOwner)
        {
            HBox individualIngredientsHeader = getSubSectionHeader("INDIVIDUAL INGREDIENTS");
            TableView<IngredientModel> ingredientMacrosTable = setupMacronutrientsPerIngredientTableView(ingredientOwner.getIngredients());
            paneContentContainer.getChildren().addAll(individualIngredientsHeader, ingredientMacrosTable);
        }

        macroBreakdownPane.setContent(paneContentContainer);
        return macroBreakdownPane;
    }

    @Override
    public void destroy()
    {
        List<Label> boundLabels = List.of(
                caloriesContent,
                proteinContent,
                carbsContent,
                sugarsContent,
                fiberContent,
                totalFatsContent,
                transFatContent,
                monoFatContent,
                polyFatContent,
                satFatContent
        );

        for(Label label : boundLabels)
        {
            label.textProperty().unbind();
        }
    }

    private GridPane setupGrandTotalsGrid()
    {
        GridPane grandTotalsContainer = new GridPane();
        grandTotalsContainer.setHgap(20);
        grandTotalsContainer.setVgap(10);

        Label caloriesHeader = getContentLabel("Calories");
        caloriesContent.textProperty().bind(Bindings.convert(entity.caloriesProperty()));
        grandTotalsContainer.add(caloriesHeader, 0, 0);
        grandTotalsContainer.add(caloriesContent, 0, 1);

        Label proteinHeader = getContentLabel("Protein");
        proteinContent.textProperty().bind(Bindings.convert(entity.proteinProperty()));
        grandTotalsContainer.add(proteinHeader, 1, 0);
        grandTotalsContainer.add(proteinContent, 1, 1);

        Label carbsHeader = getContentLabel("Carbohydrates");
        carbsContent.textProperty().bind(Bindings.convert(entity.carbohydratesProperty()));
        grandTotalsContainer.add(carbsHeader, 2, 0);
        grandTotalsContainer.add(carbsContent, 2, 1);

        Label sugarsHeader = getContentLabel("Sugars");
        sugarsContent.textProperty().bind(Bindings.convert(entity.sugarsProperty()));
        grandTotalsContainer.add(sugarsHeader, 3, 0);
        grandTotalsContainer.add(sugarsContent, 3, 1);

        Label fiberHeader = getContentLabel("Fiber");
        fiberContent.textProperty().bind(Bindings.convert(entity.fiberProperty()));
        grandTotalsContainer.add(fiberHeader, 4, 0);
        grandTotalsContainer.add(fiberContent, 4, 1);

        Label totalFatsHeader = getContentLabel("Total Fats");
        totalFatsContent.textProperty().bind(Bindings.convert(entity.totalFatsProperty()));
        grandTotalsContainer.add(totalFatsHeader, 0, 2);
        grandTotalsContainer.add(totalFatsContent, 0, 3);

        Label transFatHeader = getContentLabel("Trans Fat");
        transFatContent.textProperty().bind(Bindings.convert(entity.transFatProperty()));
        grandTotalsContainer.add(transFatHeader, 1, 2);
        grandTotalsContainer.add(transFatContent, 1, 3);

        Label monoFatHeader = getContentLabel("Monounsaturated Fat");
        monoFatContent.textProperty().bind(Bindings.convert(entity.monounsaturatedFatProperty()));
        grandTotalsContainer.add(monoFatHeader, 2, 2);
        grandTotalsContainer.add(monoFatContent, 2, 3);

        Label polyFatHeader = getContentLabel("Polyunsaturated Fat");
        polyFatContent.textProperty().bind(Bindings.convert(entity.polyunsaturatedFatProperty()));
        grandTotalsContainer.add(polyFatHeader, 3, 2);
        grandTotalsContainer.add(polyFatContent, 3, 3);

        Label satFatHeader = getContentLabel("Saturated Fat");
        satFatContent.textProperty().bind(Bindings.convert(entity.saturatedFatProperty()));
        grandTotalsContainer.add(satFatHeader, 4, 2);
        grandTotalsContainer.add(satFatContent, 4, 3);

        return grandTotalsContainer;
    }

    private TableView<IngredientModel> setupMacronutrientsPerIngredientTableView(ObservableList<IngredientModel> ingredients)
    {
        TableView<IngredientModel> macroTotalsTable = new TableView<>();
        macroTotalsTable.setPlaceholder(new Label("No ingredient data to display"));

        TableColumn<IngredientModel, String> nameCol = new TableColumn<>("Name");
        nameCol.setCellValueFactory(cellDataFeatures -> cellDataFeatures.getValue().getIngredientFood().nameProperty());
        macroTotalsTable.getColumns().add(nameCol);

        TableColumn<IngredientModel, BigDecimal> caloriesTotalCol = new TableColumn<>("Calories");
        TableColumn<IngredientModel, BigDecimal> proteinTotalCol = new TableColumn<>("Protein");
        TableColumn<IngredientModel, BigDecimal> carbsTotalCol = new TableColumn<>("Carbs");
        TableColumn<IngredientModel, BigDecimal> fiberTotalCol = new TableColumn<>("Fiber");
        TableColumn<IngredientModel, BigDecimal> sugarTotalCol = new TableColumn<>("Sugar");
        TableColumn<IngredientModel, BigDecimal> totalFatsTotalCol = new TableColumn<>("Total Fats");
        TableColumn<IngredientModel, BigDecimal> transFatTotalCol = new TableColumn<>("Trans Fat");
        TableColumn<IngredientModel, BigDecimal> monoFatTotalCol = new TableColumn<>("Monounsaturated Fat");
        TableColumn<IngredientModel, BigDecimal> polyunsaturatedFatTotalCol = new TableColumn<>("Polyunsaturated Fat");
        TableColumn<IngredientModel, BigDecimal> saturatedFatTotalCol = new TableColumn<>("Saturated Fat");

        caloriesTotalCol.setCellValueFactory(new PropertyValueFactory<>("calories"));
        proteinTotalCol.setCellValueFactory(new PropertyValueFactory<>("protein"));
        carbsTotalCol.setCellValueFactory(new PropertyValueFactory<>("carbohydrates"));
        fiberTotalCol.setCellValueFactory(new PropertyValueFactory<>("fiber"));
        sugarTotalCol.setCellValueFactory(new PropertyValueFactory<>("sugars"));
        totalFatsTotalCol.setCellValueFactory(new PropertyValueFactory<>("totalFats"));
        transFatTotalCol.setCellValueFactory(new PropertyValueFactory<>("transFat"));
        monoFatTotalCol.setCellValueFactory(new PropertyValueFactory<>("monounsaturatedFat"));
        polyunsaturatedFatTotalCol.setCellValueFactory(new PropertyValueFactory<>("polyunsaturatedFat"));
        saturatedFatTotalCol.setCellValueFactory(new PropertyValueFactory<>("saturatedFat"));

        macroTotalsTable.getColumns().addAll(
                caloriesTotalCol,
                proteinTotalCol,
                carbsTotalCol,
                fiberTotalCol,
                sugarTotalCol,
                totalFatsTotalCol,
                transFatTotalCol,
                monoFatTotalCol,
                polyunsaturatedFatTotalCol,
                saturatedFatTotalCol
        );

        macroTotalsTable.setItems(ingredients);
        return macroTotalsTable;
    }

    /**
     * Convenience function for wrapping some header text in an HBox with a white border
     * to demarcate it as a header
     * @param subSectionTitle The text to wrap
     * @return HBox with a white border
     */
    // Both macro and micro breakdown sections share this convenience function
    @SuppressWarnings("DuplicatedCode")
    private HBox getSubSectionHeader(String subSectionTitle)
    {
        Label label = new Label(subSectionTitle);
        label.getStyleClass().add(CssConstants.CLASS_TEXT_SUB_HEADER);
        HBox container = new HBox();
        container.setMaxWidth(Double.MAX_VALUE);
        container.setPadding(LayoutUtils.getSectionElementVerticalSpacing());
        container.getStyleClass().add(CssConstants.CLASS_BORDER_WHITE);
        container.setAlignment(Pos.CENTER);
        HBox.setHgrow(container, Priority.ALWAYS);
        container.getChildren().add(label);
        return container;
    }

    private Label getContentLabel(String labelText)
    {
        Label label = new Label(labelText);
        label.getStyleClass().addAll(CssConstants.CLASS_BOLD, CssConstants.CLASS_UNDERLINE);
        return label;
    }
}
