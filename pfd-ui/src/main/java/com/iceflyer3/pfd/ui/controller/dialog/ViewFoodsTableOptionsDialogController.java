/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


package com.iceflyer3.pfd.ui.controller.dialog;

import com.iceflyer3.pfd.constants.FontAwesomeIcons;
import com.iceflyer3.pfd.ui.cell.ActionButtonTableCell;
import com.iceflyer3.pfd.ui.cell.EditableBigDecimalTableCell;
import com.iceflyer3.pfd.ui.enums.FoodFilterProperty;
import com.iceflyer3.pfd.ui.enums.PfdActionType;
import com.iceflyer3.pfd.ui.filter.FoodFilterRequest;
import com.iceflyer3.pfd.ui.filter.NumericFoodFilter;
import com.iceflyer3.pfd.ui.filter.TextFoodFilter;
import com.iceflyer3.pfd.ui.format.ComparisonOperatorStringConverter;
import com.iceflyer3.pfd.ui.format.FoodFilterPropertyStringConverter;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import com.iceflyer3.pfd.ui.util.builder.ActionNodeBuilder;
import com.iceflyer3.pfd.validation.ComparisonOperator;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.converter.DefaultStringConverter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Component
@Scope("prototype")
public class ViewFoodsTableOptionsDialogController extends AbstractDialogController<FoodFilterRequest>
{

    @FXML
    public ListView<String> columnVisibilityListView;

    @FXML
    public Button visibilityConfirmButton;

    @FXML
    public Button filtersConfirmButton;

    @FXML
    public Button resetDefaultsButton;

    @FXML
    public HBox logicalOperationContainer;

    @FXML
    public VBox textFiltersContainer;

    @FXML
    public VBox numericFiltersContainer;

    private final Set<String> defaultVisibleColumns;


    private HashMap<String, BooleanProperty> columnOptions;

    private FoodFilterRequest filterRequest;

    // A HashMap where the key is the class of the filter and the value is the list of filters to apply
    private final HashMap<String, ObservableList<?>> filtersMap;


    public ViewFoodsTableOptionsDialogController(WindowManager windowManager) {
        super(windowManager);
        defaultVisibleColumns = new HashSet<>();
        filtersMap = new HashMap<>();
        filterRequest = new FoodFilterRequest(ComparisonOperator.AND);
    }

    @Override
    public void initialize() {
        // TODO: We can make this configurable via the config file and / or settings later. But for now this will work just fine.
        defaultVisibleColumns.add("Name");
        defaultVisibleColumns.add("Brand");
        defaultVisibleColumns.add("Source");
        defaultVisibleColumns.add("Serving Size");
        defaultVisibleColumns.add("Calories");
        defaultVisibleColumns.add("Protein");
        defaultVisibleColumns.add("Carbohydrates");
        defaultVisibleColumns.add("Total Fats");
        defaultVisibleColumns.add("Sugars");
        defaultVisibleColumns.add("Fiber");
    }

    @Override
    public void initControllerData(Map<String, Object> dialogData) {
        if (dialogData.containsKey(FoodFilterRequest.class.getName()))
        {
            filterRequest = (FoodFilterRequest) dialogData.get(FoodFilterRequest.class.getName());
        }

        initFiltersTab();

        columnOptions = (HashMap<String, BooleanProperty>) dialogData.get(HashMap.class.getName());
        columnVisibilityListView.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
            @Override
            public ListCell<String> call(ListView<String> param) {
                return new CheckBoxListCell<>(new Callback<String, ObservableValue<Boolean>>() {
                    @Override
                    public ObservableValue<Boolean> call(String param) {
                        return columnOptions.get(param);
                    }
                });
            }
        });
        columnVisibilityListView.setItems(FXCollections.observableArrayList(columnOptions.keySet()));
    }

    @Override
    protected void registerDefaultListeners() {
        EventHandler<ActionEvent> confirmButtonPress = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                LOG.debug("Returning filter request of {}", filterRequest);
                callback.complete(filterRequest);
                windowManager.close(WindowManager.DIALOG_WINDOW);
            }
        };
        visibilityConfirmButton.setOnAction(confirmButtonPress);
        filtersConfirmButton.setOnAction(confirmButtonPress);

        resetDefaultsButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                for(String columnName : columnOptions.keySet())
                {
                    columnOptions.get(columnName).set(defaultVisibleColumns.contains(columnName));
                }
            }
        });
    }

    private void initFiltersTab()
    {
        List<ComparisonOperator> comparisonOperators = Arrays
                .stream(ComparisonOperator.values())
                .filter(comparisonOperator ->
                        comparisonOperator == ComparisonOperator.AND
                                || comparisonOperator == ComparisonOperator.OR)
                .collect(Collectors.toList());
        ComboBox<ComparisonOperator> comparisonOperatorComboBox = new ComboBox<>(FXCollections.observableArrayList(comparisonOperators));
        logicalOperationContainer.getChildren().add(comparisonOperatorComboBox);
        Bindings.bindBidirectional(comparisonOperatorComboBox.valueProperty(), filterRequest.logicalOperationProperty());

        initTextFiltersSection();
        initNumericFiltersSection();
    }

    private void initNumericFiltersSection()
    {
        // Create the buttons for adding a clearing filters
        HBox buttonsHbox = new HBox(10);

        Button newFilterButton = ActionNodeBuilder
                .createIconButton("ADD", FontAwesomeIcons.PLUS, 12)
                .forActionType(PfdActionType.SECONDARY)
                .withAction(event -> {
                    filterRequest.getNumericFilters().add(new NumericFoodFilter(FoodFilterProperty.CALORIES, ComparisonOperator.EQUALS));
                })
                .build();

        Button clearFiltersButton = ActionNodeBuilder
                .createIconButton("CLEAR ALL", FontAwesomeIcons.TIMES, 12)
                .forActionType(PfdActionType.DANGER)
                .withAction(event -> {
                    filterRequest.getNumericFilters().clear();
                })
                .build();

        buttonsHbox.getChildren().addAll(newFilterButton, clearFiltersButton);
        numericFiltersContainer.getChildren().add(buttonsHbox);

        // Setup the tableview
        TableView<NumericFoodFilter> numericFiltersTable = createTableView(NumericFoodFilter.class, filterRequest.getNumericFilters());
        numericFiltersContainer.getChildren().add(numericFiltersTable);
    }

    private void initTextFiltersSection()
    {
        // Create the buttons for adding a clearing filters
        HBox buttonsHbox = new HBox(10);

        Button newFilterButton = ActionNodeBuilder
                .createIconButton("ADD", FontAwesomeIcons.PLUS, 12)
                .forActionType(PfdActionType.SECONDARY)
                .withAction(event -> {
                    filterRequest.getTextFilters().add(new TextFoodFilter(FoodFilterProperty.NAME, ComparisonOperator.LIKE));
                })
                .build();

        Button clearFiltersButton = ActionNodeBuilder
                .createIconButton("CLEAR ALL", FontAwesomeIcons.TIMES, 12)
                .forActionType(PfdActionType.DANGER)
                .withAction(event -> {
                    filterRequest.getTextFilters().clear();
                })
                .build();

        buttonsHbox.getChildren().addAll(newFilterButton, clearFiltersButton);
        textFiltersContainer.getChildren().add(buttonsHbox);

        TableView<TextFoodFilter> textFiltersTable = createTableView(TextFoodFilter.class, filterRequest.getTextFilters());
        textFiltersContainer.getChildren().add(textFiltersTable);
    }


    private <T> TableView<T> createTableView(Class<T> filterClass, ObservableList<T> tableItems)
    {
        // Find the appropriate list of food properties and operations based on filter type
        List<FoodFilterProperty> foodPropertyList;
        List<ComparisonOperator> comparisonOperators;

        if (filterClass == NumericFoodFilter.class)
        {
            foodPropertyList = Arrays
                    .stream(FoodFilterProperty.values())
                    .filter(foodProperty ->
                            foodProperty != FoodFilterProperty.BRAND
                                    && foodProperty != FoodFilterProperty.NAME
                                    && foodProperty != FoodFilterProperty.SOURCE)
                    .collect(Collectors.toList());

            comparisonOperators = Arrays
                    .stream(ComparisonOperator.values())
                    .filter(comparisonOperator ->
                            comparisonOperator != ComparisonOperator.LIKE
                                    && comparisonOperator != ComparisonOperator.NOT_LIKE
                                    && comparisonOperator != ComparisonOperator.OR
                                    && comparisonOperator != ComparisonOperator.AND)
                    .collect(Collectors.toList());
        }
        else
        {
            foodPropertyList = Arrays
                    .stream(FoodFilterProperty.values())
                    .filter(foodProperty ->
                            foodProperty == FoodFilterProperty.BRAND
                                    || foodProperty == FoodFilterProperty.NAME
                                    || foodProperty == FoodFilterProperty.SOURCE)
                    .collect(Collectors.toList());

            comparisonOperators = Arrays
                    .stream(ComparisonOperator.values())
                    .filter(comparisonOperator ->
                            comparisonOperator == ComparisonOperator.LIKE
                                    || comparisonOperator == ComparisonOperator.NOT_LIKE)
                    .collect(Collectors.toList());
        }

        // Setup the tableview
        TableView<T> filtersTable = new TableView<>();
        filtersTable.setEditable(true);
        filtersTable.setPlaceholder(new Label("No filters have been added"));

        // Remove filter column
        TableColumn<T, Boolean> actionButtonColumn = new TableColumn<>();
        actionButtonColumn.setCellFactory(col -> new ActionButtonTableCell<T, Boolean>(rowData -> {
            ArrayList<Button> buttonsForCell = new ArrayList<>();
            Button removeFilterButton = ActionNodeBuilder
                    .createIconButton(null, FontAwesomeIcons.TIMES, 10)
                    .forActionType(PfdActionType.DANGER)
                    .withAction(event -> {
                        tableItems.remove(rowData);
                    })
                    .build();
            buttonsForCell.add(removeFilterButton);
            return buttonsForCell;
        }));
        actionButtonColumn.setCellValueFactory(new PropertyValueFactory<>("active"));
        filtersTable.getColumns().add(actionButtonColumn);

        // Activate / deactivate column
        TableColumn<T, Boolean> isActiveColumn = new TableColumn<>("Active?");
        isActiveColumn.setCellFactory(col -> new CheckBoxTableCell<>());
        isActiveColumn.setCellValueFactory(new PropertyValueFactory<>("active"));
        filtersTable.getColumns().add(isActiveColumn);

        // Food Property Column
        ObservableList<FoodFilterProperty> foodPropertyChoices = FXCollections.observableArrayList(foodPropertyList);
        TableColumn<T, FoodFilterProperty> foodPropertyColumn = new TableColumn<>("On Property");
        foodPropertyColumn.setCellFactory(col -> new ChoiceBoxTableCell<>(new FoodFilterPropertyStringConverter(), foodPropertyChoices));
        foodPropertyColumn.setCellValueFactory(new PropertyValueFactory<>("foodProperty"));
        filtersTable.getColumns().add(foodPropertyColumn);

        // Comparison Operator select column
        ObservableList<ComparisonOperator> comparisonOperatorChoices = FXCollections.observableArrayList(comparisonOperators);
        TableColumn<T, ComparisonOperator> comparisonOperatorColumn = new TableColumn<>("Operation");
        comparisonOperatorColumn.setCellFactory(col -> new ChoiceBoxTableCell<>(new ComparisonOperatorStringConverter(), comparisonOperatorChoices));
        comparisonOperatorColumn.setCellValueFactory(new PropertyValueFactory<>("operator"));
        filtersTable.getColumns().add(comparisonOperatorColumn);

        // Comparison Value column
        if (filterClass == NumericFoodFilter.class)
        {
            TableColumn<T, BigDecimal> comparisonValueColumn = new TableColumn<>("Value");
            comparisonValueColumn.setCellFactory(col -> new EditableBigDecimalTableCell<>());
            comparisonValueColumn.setCellValueFactory(new PropertyValueFactory<>("searchValue"));
            filtersTable.getColumns().add(comparisonValueColumn);
        }
        else
        {
            TableColumn<T, String> comparisonValueColumn = new TableColumn<>("Value");
            comparisonValueColumn.setCellFactory(col -> new TextFieldTableCell<>(new DefaultStringConverter()));
            comparisonValueColumn.setCellValueFactory(new PropertyValueFactory<>("searchValue"));
            filtersTable.getColumns().add(comparisonValueColumn);
        }

        filtersTable.setItems(tableItems);
        return filtersTable;
    }
}
