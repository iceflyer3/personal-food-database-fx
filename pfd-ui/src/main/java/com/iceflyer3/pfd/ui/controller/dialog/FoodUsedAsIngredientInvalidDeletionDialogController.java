package com.iceflyer3.pfd.ui.controller.dialog;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.ui.manager.WindowManager;
import com.iceflyer3.pfd.ui.model.viewmodel.FoodAsIngredientViewModel;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * Displays the details of which other entities a given food belongs to as an ingredient.
 *
 * This dialog does not return any data. It is for informational purposes only
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class FoodUsedAsIngredientInvalidDeletionDialogController extends AbstractDialogController<Void> {

    private List<FoodAsIngredientViewModel> ingredientOwnersForFood;

    @FXML
    public ScrollPane rootContainer;

    @FXML
    public Button dismissButton;

    public FoodUsedAsIngredientInvalidDeletionDialogController(WindowManager windowManager) {
        super(windowManager);
    }

    @Override
    public void initialize() {

    }

    @Override
    protected void registerDefaultListeners() {
        dismissButton.setOnAction(event -> windowManager.close(WindowManager.DIALOG_WINDOW));
    }

    @SuppressWarnings("unchecked")
    @Override
    public void initControllerData(Map<String, Object> dialogData) {
        ingredientOwnersForFood = (List<FoodAsIngredientViewModel>) dialogData.get(FoodAsIngredientViewModel.class.getName());
        setupDetailsTableView();
    }

    private void setupDetailsTableView()
    {
        TableView<FoodAsIngredientViewModel> detailsTableView = new TableView<>();
        detailsTableView.setEditable(false);

        TableColumn<FoodAsIngredientViewModel, String> ingredientOwnerColumn = new TableColumn<>("Ingredient Owner");
        ingredientOwnerColumn.setCellValueFactory(new PropertyValueFactory<>("ingredientOwnerName"));

        TableColumn<FoodAsIngredientViewModel, String> ownerTypeColumn = new TableColumn<>("Type");
        ownerTypeColumn.setCellValueFactory(new PropertyValueFactory<>("ownerType"));

        detailsTableView.getColumns().addAll(ingredientOwnerColumn, ownerTypeColumn);
        detailsTableView.setItems(FXCollections.observableArrayList(ingredientOwnersForFood));
        rootContainer.setContent(detailsTableView);
    }
}
