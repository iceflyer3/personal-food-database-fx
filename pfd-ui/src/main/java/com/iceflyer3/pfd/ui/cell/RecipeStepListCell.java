package com.iceflyer3.pfd.ui.cell;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.constants.CssConstants;
import com.iceflyer3.pfd.ui.model.api.recipe.RecipeStepModel;
import com.iceflyer3.pfd.ui.model.viewmodel.recipe.RecipeStepViewModel;
import com.iceflyer3.pfd.ui.util.CellUtils;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.util.List;
import java.util.stream.Collectors;

/**
 * A ListCell capable of displaying the details of a step for a recipe
 */
public class RecipeStepListCell extends ListCell<RecipeStepModel>
{
    private final static double MAX_LABEL_WIDTH = 470; // This should allow approx 75 characters per line

    private VBox cellContainer;
    private final ActionButtonProvider<RecipeStepModel> buttonProvider;

    /**
     * Create a new Recipe Step List cell.
     *
     * Use this constructor if you do not with to offer action buttons with the cell.
     */
    public RecipeStepListCell()
    {
        this(null);
    }

    /**
     * Create a new Recipe Step List cell.
     *
     * Use this constructor if you wish to offer action buttons in the header row of the cell
     * @param buttonProvider The ActionButtonProvider that provides the action buttons for the cell
     */
    public RecipeStepListCell(ActionButtonProvider<RecipeStepModel> buttonProvider) {
        this.setAlignment(Pos.TOP_LEFT);
        this.buttonProvider = buttonProvider;
    }

    @Override
    protected void updateItem(RecipeStepModel item, boolean empty) {
        super.updateItem(item, empty);

        if (empty || item == null)
        {
            // According to the docs this MUST happen when overriding this method or weirdness will ensue
            // https://openjfx.io/javadoc/13/javafx.controls/javafx/scene/control/Cell.html#updateItem(T,boolean)
            this.setText(null);
            this.setGraphic(null);

            /*
             * Be sure to null out the container for a cell that previously wasn't empty that
             * now is or it won't get re-drawn properly the next time that is not empty again.
             */
            this.cellContainer = null;
        }
        else
        {
            /*
             * If we don't null check the container here and instead initialize it in the constructor
             * and just do this every time then we'll get a secondary "ghost" item upon selecting of
             * an item in the list view.
             *
             * The null check ensures that this stuff is only created a single time so we avoid the
             * secondary "ghost" items.
             */
            if (cellContainer == null)
            {
                cellContainer = new VBox(20);
                // Header section
                AnchorPane headerNode = getHeaderNode(getItem());

                // Directions section
                Label directionsLabel = new Label(item.getDirections());
                directionsLabel.setMaxWidth(MAX_LABEL_WIDTH);
                directionsLabel.setWrapText(true);

                // Required ingredients section
                List<String> ingredientNames = item.getIngredients().stream().map(ingredient -> ingredient.getIngredientFood().getName()).collect(Collectors.toList());
                String requiredIngredientNamesString = ingredientNames.size() > 0 ? String.join(",", ingredientNames) : "None";
                String ingredientsString = String.format("Required Ingredients: %s", requiredIngredientNamesString);
                Label ingredientsLabel = new Label(ingredientsString);
                ingredientsLabel.getStyleClass().add(CssConstants.CLASS_TEXT_MUTED);
                ingredientsLabel.setWrapText(true);
                ingredientsLabel.setMaxWidth(MAX_LABEL_WIDTH);

                // Compile them all in our container vbox and set the container as the "graphic" for the cell
                cellContainer.getChildren().addAll(headerNode, directionsLabel, ingredientsLabel);
                this.setGraphic(cellContainer);
                this.setText(null);
            }
        }
    }

    private AnchorPane getHeaderNode(RecipeStepModel currentItem)
    {
        AnchorPane headerAnchorPane = new AnchorPane();

        // Setup the label that will appear on the left of the header row
        String stepNameString = String.format("Step %s: %s", currentItem.getStepNumber(), currentItem.getStepName());
        Label stepNameLabel = new Label(stepNameString);
        stepNameLabel.getStyleClass().add(CssConstants.CLASS_TEXT_SUB_HEADER);
        AnchorPane.setTopAnchor(stepNameLabel, 10.0);
        AnchorPane.setLeftAnchor(stepNameLabel, 0.0);
        headerAnchorPane.getChildren().add(stepNameLabel);

        // Setup the buttons (if any) that will appear on the right of the header row
        if (buttonProvider != null)
        {
            HBox rowButtonsHBox = CellUtils.getButtonsForButtonProvider(buttonProvider, currentItem);
            AnchorPane.setTopAnchor(rowButtonsHBox, 10.0);
            AnchorPane.setRightAnchor(rowButtonsHBox, 0.0);
            headerAnchorPane.getChildren().add(rowButtonsHBox);
        }

        return headerAnchorPane;
    }
}
