package com.iceflyer3.pfd.ui.manager;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.google.common.eventbus.EventBus;
import com.iceflyer3.pfd.constants.CssConstants;
import com.iceflyer3.pfd.constants.FontAwesomeIcons;
import com.iceflyer3.pfd.ui.Breadcrumb;
import com.iceflyer3.pfd.ui.event.PublishBreadcrumbEvent;
import com.iceflyer3.pfd.ui.event.ScreenNavigationEvent;
import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Manages adding and removing breadcrumbs to and from a breadcrumb bar.
 *
 * There are currently two such bars. One in the main window and one in popout
 * windows.
 *
 * Until such a time that we figure out how to make custom JavaFx controls (which
 * is probably what the breadcrumb bar should actually be) this will have suffice
 * as a place to make that functionality accessible on the different stages which
 * display the breadcrumb bar.
 */
@Component
public class BreadcrumbManager {

    private final static Logger LOG = LoggerFactory.getLogger(BreadcrumbManager.class);
    private final EventBus eventBus;

    public BreadcrumbManager(EventBus eventBus) {
        this.eventBus = eventBus;
    }

    /**
     * Handler for the breadcrumb event that other screens publish. This updates the list of
     * breadcrumbs that is stored by this controller adding or removing from the list as needed.
     *
     * The list has its own handler that then updates the UI.
     * @param publishBreadcrumbEvent A breadcrumb event published by another screen.
     */
    public void publishBreadcrumb(PublishBreadcrumbEvent publishBreadcrumbEvent, ObservableList<Breadcrumb> breadcrumbs)
    {
        int maxDepth = breadcrumbs.stream().mapToInt(Breadcrumb::getDepth).max().orElseGet(() -> -1);

        LOG.debug("Publishing new breadcrumb with a current max depth of {}. Event: {}", maxDepth, publishBreadcrumbEvent);

        // Check if we have navigated backwards (or to an earlier level). Remove any breadcrumbs that have a depth greater than or equal to the breadcrumb event
        List<Breadcrumb> breadcrumbsToRemove = breadcrumbs.stream().filter(breadcrumb -> breadcrumb.getDepth() >= publishBreadcrumbEvent.getDepth()).collect(Collectors.toList());
        breadcrumbs.removeAll(breadcrumbsToRemove);

        Breadcrumb breadcrumb = new Breadcrumb(publishBreadcrumbEvent.getLabel(), publishBreadcrumbEvent.getApplicationSection(),publishBreadcrumbEvent.getSetToLoad(), publishBreadcrumbEvent.getForStage(), publishBreadcrumbEvent.getDepth());
        if (publishBreadcrumbEvent.getControllerData().size() > 0)
        {
            breadcrumb.setControllerData(publishBreadcrumbEvent.getControllerData());
        }
        breadcrumbs.add(breadcrumb);
    }

    /**
     * Fires upon changes to the list of breadcrumbs so that the breadcrumbs bar in the UI may be kept
     * up-to-date and in sync with the actual list of breadcrumbs as it changes
     *
     * It is very likely that, in practice, this will be called several times in succession when removing
     * breadcrumbs thus re-drawing the UI several times which is, technically speaking, rather inefficient.
     *
     * However, this list should should never have more than five items in it anyways so I don't think it is
     * likely to actually matter.
     * @param change The Change instance that describes the change to the list that has occurred
     * @param breadcrumbContainer The HBox that contains the breadcrumbs
     */
    public void updateBreadcrumbBar(ListChangeListener.Change<? extends Breadcrumb> change, HBox breadcrumbContainer, UserViewModel user)
    {
        while(change.next())
        {
            if (change.wasRemoved())
            {
                /*
                 * If breadcrumbs have been removed (backwards navigation) then remove labels and chevrons
                 *
                 * Due to a very small maximum number of breadcrumbs that will ever be shown at one time it is
                 * easiest just to clear the whole container and then re-add all the of breadcrumbs still needed
                 */
                breadcrumbContainer.getChildren().clear();
                for(Breadcrumb breadcrumb : change.getList())
                {
                    addBreadcrumb(breadcrumb, breadcrumbContainer, user);
                }
            }
            else
            {
                // If added create new labels for both chevron and label of breadcrumb
                for(Breadcrumb newBreadcrumb : change.getAddedSubList())
                {
                    addBreadcrumb(newBreadcrumb, breadcrumbContainer, user);
                }
            }
        }
    }

    /**
     * Adds a new breadcrumb to the UI
     * @param newBreadcrumb The breadcrumb to add
     * @param breadcrumbContainer The HBox that contains the breadcrumbs
     */
    private void addBreadcrumb(Breadcrumb newBreadcrumb, HBox breadcrumbContainer, UserViewModel user)
    {
        /*
         * Display a chevron before all breadcrumbs except for the first.
         *
         * It isn't necessarily true that the first breadcrumb will always be of depth 0.
         * In fact, this is only true for the main application window, not for popout windows.
         *
         * This is because only the main application window has access to the "home" screen and
         * popout windows only have access to the section that was popped out.
         */
        if (breadcrumbContainer.getChildren().size() > 0)
        {
            Label chevronLabel = new Label(FontAwesomeIcons.CHEVRON_RIGHT);
            chevronLabel.getStyleClass().add(CssConstants.FONT_AWESOME_SOLID);
            HBox.setMargin(chevronLabel, new Insets(5, 5, 0, 5));
            breadcrumbContainer.getChildren().add(chevronLabel);
        }

        Hyperlink nameLabel = new Hyperlink(newBreadcrumb.getLabel());
        nameLabel.getStyleClass().add(CssConstants.CLASS_TEXT_SECONDARY);
        HBox.setMargin(nameLabel, new Insets(0, 0, 0, 0));

        nameLabel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ScreenNavigationEvent breadCrumbNavigationEvent = new ScreenNavigationEvent(newBreadcrumb.getApplicationSection(), newBreadcrumb.getSetToLoad(), newBreadcrumb.getForStage(), user);

                if (newBreadcrumb.getControllerData().size() > 0)
                {
                    breadCrumbNavigationEvent.getControllerData().putAll(newBreadcrumb.getControllerData());
                }

                eventBus.post(breadCrumbNavigationEvent);
            }
        });

        LOG.debug("Adding a new breadcrumb with label '{}' at depth {}", newBreadcrumb.getLabel(), newBreadcrumb.getDepth());
        breadcrumbContainer.getChildren().add(nameLabel);
    }
}
