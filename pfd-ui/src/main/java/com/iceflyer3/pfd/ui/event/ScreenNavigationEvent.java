/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.event;

import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;

import java.util.HashMap;

public class ScreenNavigationEvent {

    private final String setToLoad;
    private final String applicationSection;
    private final String forStage;
    private final HashMap<String, Object> controllerData;

    /**
     * Represents a request to load one of the application screens after a user profile has been selected.
     *
     * @param applicationSection The name of the section of the application that the set belongs to
     * @param setToLoad The name of the set name of the screen that should be loaded
     * @param forStage  The name of the stage in which the screen navigation will occur
     * @param user The user who invoked the navigation event. Always the currently selected user profile by definition.
     */
    public ScreenNavigationEvent(String applicationSection, String setToLoad, String forStage, UserViewModel user) {
        this.setToLoad = setToLoad;
        this.applicationSection = applicationSection;
        this.forStage = forStage;

        this.controllerData = new HashMap<>();
        this.controllerData.put(UserViewModel.class.getName(), user);
    }

    public String getApplicationSection() {
        return applicationSection;
    }

    public String getSetToLoad() {
        return setToLoad;
    }

    public String getForStage() {
        return forStage;
    }

    public HashMap<String, Object> getControllerData()
    {
        return controllerData;
    }

    @Override
    public String toString() {
        return "ScreenNavigationEvent{" +
                "setToLoad='" + setToLoad + '\'' +
                ", applicationSection='" + applicationSection + '\'' +
                ", forStage='" + forStage + '\'' +
                ", controllerData=" + controllerData +
                '}';
    }
}
