package com.iceflyer3.pfd.ui.controller.dialog;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.ui.manager.WindowManager;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class LoadingNotificationDialogController extends AbstractDialogController<Void>
{
    public static final String DATA_LOADING_MESSAGE = "loadingMessage";

    @FXML
    public Label loadingMessageLabel;

    public LoadingNotificationDialogController(WindowManager windowManager)
    {
        super(windowManager);
    }

    @Override
    public void initialize()
    {

    }

    @Override
    protected void registerDefaultListeners()
    {

    }

    @Override
    public void initControllerData(Map<String, Object> dialogData)
    {
        String loadingMessage = (String) dialogData.get(DATA_LOADING_MESSAGE);
        loadingMessageLabel.setText(loadingMessage);
    }
}
