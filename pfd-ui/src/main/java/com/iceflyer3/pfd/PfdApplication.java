/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd;

import com.iceflyer3.pfd.config.SpringConfiguration;
import com.iceflyer3.pfd.constants.SceneSets;
import com.iceflyer3.pfd.exception.InvalidApplicationStateException;
import com.iceflyer3.pfd.ui.controller.ApplicationRootController;
import com.iceflyer3.pfd.ui.manager.WindowManager;
import com.iceflyer3.pfd.ui.util.ControllerUtils;
import com.iceflyer3.pfd.ui.util.FxmlLoadResult;
import javafx.application.Application;
import javafx.application.HostServices;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.flywaydb.core.Flyway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.sql.DataSource;

public class PfdApplication extends Application {

    public final static int WINDOW_MIN_HEIGHT = 720;
    public final static int WINDOW_MIN_WIDTH = 1350;
    /*
     * The name of the stage in which the primary application window is hosted.
     *
     * It is worth noting that stages don't actually have names per se. But that the name is an
     * application construct for differentiating between different open stages. See the Javadoc
     * for the HostStageAware interface for further details.
     *
     * Though it would be possible to stash away the name via Stage.setUserData if it was ever
     * desired
     */
    public static final String MAIN_STAGE_NAME = "main";

    private static final Logger LOG = LoggerFactory.getLogger(PfdApplication.class);

    private static Stage appRootStage;

    private static AnnotationConfigApplicationContext springApplicationContext;
    private ApplicationRootController loadedAppRootController;

    @Override
    public void init()
    {
        LOG.debug("Application is initializing...");
        try
        {
            DataSource dataSource = springApplicationContext.getBean(DataSource.class);
            Flyway flyway = Flyway.configure()
                    .dataSource(dataSource)
                    .load();
            flyway.baseline();
            flyway.migrate();

            // Register the HostServices bean so that it is available to the rest of the application
            GenericBeanDefinition hostServicesBean = new GenericBeanDefinition();
            hostServicesBean.setBeanClass(HostServices.class);
            hostServicesBean.setInstanceSupplier(this::getHostServices);
            hostServicesBean.setScope(ConfigurableBeanFactory.SCOPE_SINGLETON);
            springApplicationContext.registerBeanDefinition("HostServices", hostServicesBean);
        }
        catch(Exception e)
        {
            LOG.error("An unexpected exception has occurred during spring application context initialization", e);
        }
    }


    @Override
    public void start(Stage rootStage) {
        LOG.debug("Application is starting...");

        try
        {
            WindowManager windowManager = springApplicationContext.getBean(WindowManager.class);
            windowManager.registerMainApplicationWindow(rootStage);

            // Load the first scene
            ControllerUtils controllerUtils = springApplicationContext.getBean(ControllerUtils.class);
            FxmlLoadResult<ApplicationRootController> loadResult = controllerUtils.loadFxml(SceneSets.APP_ROOT);
            loadedAppRootController = loadResult.getController();

            Scene scene = new Scene(loadResult.getRootNode(), 640, 480);

            // Load stylesheets from the classpath
            scene.getStylesheets().add("/styles/theme-dark.css");

            // Setup the stage
            appRootStage = rootStage;
            appRootStage.setTitle("Personal Food Database");
            appRootStage.getIcons().add(new Image("/logo.png"));
            appRootStage.setUserData("main");
            appRootStage.setScene(scene);
            appRootStage.setMinHeight(WINDOW_MIN_HEIGHT);
            appRootStage.setMinWidth(WINDOW_MIN_WIDTH);
            appRootStage.centerOnScreen();
            appRootStage.setOnCloseRequest(event -> {
                // If the stage is closed via the top right "X" button then ensure
                // that the controller hierarchy can properly clean up resources.
                loadedAppRootController.destroy();
                loadedAppRootController = null;
            });
            appRootStage.show();
        }
        catch(Exception ex)
        {
            LOG.error("An error has occurred while the application was attempting to start", ex);
            Platform.exit();
        }
    }

    @Override
    public void stop()
    {
        LOG.debug("Application is stopping...");
        springApplicationContext.close();

        Platform.exit();
        LOG.debug("Platform has exited");
    }

    public static void main(String[] args) {
        SpringApplicationBuilder springApplicationBuilder = new SpringApplicationBuilder();
        SpringApplication springApplication = springApplicationBuilder
                .sources(SpringConfiguration.class)
                .web(WebApplicationType.NONE)
                .build();

        /*
         * As stated in the documentation, because this is not a web project (and thus neither
         * Spring MVC nor Spring WebFlux will be present), the type of application context that
         * SpringApplication creates will be AnnotationConfigApplicationContext.
         *
         * https://docs.spring.io/spring-boot/docs/current/reference/html/features.html#features.spring-application.web-environment
         */
        springApplicationContext = (AnnotationConfigApplicationContext) springApplication.run(args);
        launch();
    }

    public static Stage getApplicationRootStage()
    {
        if (appRootStage == null)
        {
            // Theoretically this should never happen because it will have already been created before
            // any controller may attempt to access it. But we'll check just in case some weirdness ensues
            throw new InvalidApplicationStateException("The application root stage was null!");
        }
        else
        {
            return appRootStage;
        }
    }
}