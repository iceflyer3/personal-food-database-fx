/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


package com.iceflyer3.pfd.test.viewmodel.recipe;

import com.iceflyer3.pfd.test.TestDataProvider;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.api.recipe.RecipeStepModel;
import com.iceflyer3.pfd.ui.model.viewmodel.recipe.RecipeViewModel;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

public class RecipeViewModelTests
{
    /*
     * Invariants:
     *  - Recipe Name
     *  - Recipe Source
     *  - Number of servings the recipe will make
     *  - At least one ingredient
     *  - At least one serving size
     *  - At least one step
     */

    private final TestDataProvider testDataProvider = new TestDataProvider();

    @Test(dataProvider = "unmetInvariantDataProvider")
    public void Recipe_With_Unmet_Invariants_Is_Invalid(String name, String source, BigDecimal numberOfServingsMade, Collection<IngredientModel> ingredients, Collection<RecipeStepModel> steps, boolean hasServingSize)
    {
        RecipeViewModel sut = new RecipeViewModel("UnitTestUser");
        sut.setName(name);
        sut.setSource(source);
        sut.setNumberOfServingsMade(numberOfServingsMade);
        sut.setIngredients(ingredients);
        sut.setSteps(steps);

        if (hasServingSize)
        {
            sut.addServingSize();
        }

        assertThat(sut.validate().wasSuccessful()).isFalse();
    }

    @Test
    public void Recipe_With_Met_Invariants_Is_Valid()
    {
        RecipeViewModel sut = new RecipeViewModel("UnitTestUser");
        sut.setName("UnitTestName");
        sut.setSource("UnitTestSource");
        sut.setNumberOfServingsMade(BigDecimal.ONE);
        sut.setIngredients(testDataProvider.getTestIngredientViewModelSet(1));
        sut.setSteps(testDataProvider.getTestRecipeSteps());
        sut.addServingSize();

        assertThat(sut.validate().wasSuccessful()).isTrue();
    }

    @DataProvider(name = "unmetInvariantDataProvider")
    public Object[][] unmetInvariantDataProvider()
    {
        return new Object[][]{
                {null, "UnitTestSource", BigDecimal.ONE, testDataProvider.getTestIngredientViewModelSet(1), testDataProvider.getTestRecipeSteps(), true},             // Invalid recipe name
                {"", "UnitTestSource", BigDecimal.ONE, testDataProvider.getTestIngredientViewModelSet(1), testDataProvider.getTestRecipeSteps(), true},
                {"UnitTestName", null, BigDecimal.ONE, testDataProvider.getTestIngredientViewModelSet(1), testDataProvider.getTestRecipeSteps(), true},               // Invalid recipe source
                {"UnitTestName", "", BigDecimal.ONE, testDataProvider.getTestIngredientViewModelSet(1), testDataProvider.getTestRecipeSteps(), true},
                {"UnitTestName", "UnitTestSource", BigDecimal.ZERO, testDataProvider.getTestIngredientViewModelSet(1), testDataProvider.getTestRecipeSteps(), true},   // Invalid servings made
                {"UnitTestName", "UnitTestSource", BigDecimal.ONE, Collections.emptySet(), testDataProvider.getTestRecipeSteps(), true},                                     // Invalid ingredient list
                {"UnitTestName", "UnitTestSource", BigDecimal.ONE, testDataProvider.getTestIngredientViewModelSet(1), Collections.emptySet(), true},                  // Invalid step list
                {"UnitTestName", "UnitTestSource", BigDecimal.ONE, testDataProvider.getTestIngredientViewModelSet(1), testDataProvider.getTestRecipeSteps(), false},  // Invalid serving size list
        };
    }
}
