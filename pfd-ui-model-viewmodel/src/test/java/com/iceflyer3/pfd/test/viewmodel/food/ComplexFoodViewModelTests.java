/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.test.viewmodel.food;

import com.iceflyer3.pfd.test.TestDataProvider;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.ComplexFoodViewModel;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class ComplexFoodViewModelTests
{
    /*
     * Invariants
     *  - Name of at least one character
     *  - At least one serving size
     *  - At least two ingredients
     */

    private final TestDataProvider testDataProvider = new TestDataProvider();

    @Test(dataProvider = "unmetVariantsDataProvider")
    public void Complex_Food_With_Unmet_Invariants_Is_Invalid(String name, int numIngredients, boolean hasServingSize)
    {
        ComplexFoodViewModel sut = new ComplexFoodViewModel("UnitTest");
        sut.setName(name);
        Set<IngredientModel> testIngredients = numIngredients > 1 ? testDataProvider.getTestIngredientViewModelSet(2) : testDataProvider.getTestIngredientViewModelSet(1);
        sut.setIngredients(testIngredients);

        if (hasServingSize)
        {
            sut.addServingSize();
        }

        assertThat(sut.validate().wasSuccessful()).isFalse();
    }

    @Test
    public void Complex_Food_With_Met_Invariants_Is_Valid()
    {
        ComplexFoodViewModel sut = new ComplexFoodViewModel("UnitTest");
        sut.setName("Foo");
        sut.setIngredients(testDataProvider.getTestIngredientViewModelSet(2));
        sut.addServingSize();

        assertThat(sut.validate().wasSuccessful()).isTrue();
    }

    @DataProvider(name = "unmetVariantsDataProvider")
    public Object[][] unmetVariantsDataProvider()
    {
        return new Object[][]{
                {null, 2, true},
                {"", 2, true},
                {"UnitTestName", 1, true},
                {"UnitTestName", 2, false}
        };
    }
}
