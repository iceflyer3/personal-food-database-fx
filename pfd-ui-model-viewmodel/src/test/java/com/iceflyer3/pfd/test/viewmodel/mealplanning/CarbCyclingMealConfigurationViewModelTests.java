/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.test.viewmodel.mealplanning;

import com.iceflyer3.pfd.enums.CarbCyclingMealRelation;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingMealConfigurationViewModel;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class CarbCyclingMealConfigurationViewModelTests
{
    /*
     * Invariants
     *  - Minimum / maximum carb consumption percentages must be between 0% and 99%
     *  - The minimum percentage must be less than the maximum percentage
     */

    @Test(dataProvider = "unmetInvariantsDataProvider")
    public void Configuration_With_Unmet_Invariants_Is_Invalid(BigDecimal minCarbConsumptionPercent, BigDecimal maxCarbConsumptionPercent)
    {
        CarbCyclingMealConfigurationViewModel sut = new CarbCyclingMealConfigurationViewModel(0, CarbCyclingMealRelation.NONE, true);
        sut.setCarbConsumptionMinimumPercent(minCarbConsumptionPercent);
        sut.setCarbConsumptionMaximumPercent(maxCarbConsumptionPercent);

        assertThat(sut.validate().wasSuccessful()).isFalse();
    }

    @Test(dataProvider = "metInvariantsDataProvider")
    public void Configuration_With_Met_Invariants_Is_Valid(BigDecimal minCarbConsumptionPercent, BigDecimal maxCarbConsumptionPercent)
    {
        CarbCyclingMealConfigurationViewModel sut = new CarbCyclingMealConfigurationViewModel(0, CarbCyclingMealRelation.NONE, true);
        sut.setCarbConsumptionMinimumPercent(minCarbConsumptionPercent);
        sut.setCarbConsumptionMaximumPercent(maxCarbConsumptionPercent);

        assertThat(sut.validate().wasSuccessful()).isTrue();
    }

    @DataProvider(name = "unmetInvariantsDataProvider")
    public Object[][] unmetInvariantsDataProvider()
    {
        return new Object[][]{
                {BigDecimal.valueOf(100), BigDecimal.TEN}, // Min exceeds upper bound
                {BigDecimal.valueOf(-1), BigDecimal.TEN},  // Min less than lower bound
                {BigDecimal.TEN, BigDecimal.valueOf(100)}, // Max exceeds upper bound
                {BigDecimal.TEN, BigDecimal.valueOf(-1)},  // Max less than lower bound
                {BigDecimal.valueOf(50), BigDecimal.valueOf(30)}   // Min greater than max
        };
    }

    @DataProvider(name = "metInvariantsDataProvider")
    public Object[][] metInvariantsDataProvider()
    {
        return new Object[][]{
                {BigDecimal.ZERO, BigDecimal.ZERO}, // Both min and max may be zero for any given meal
                {BigDecimal.ONE, BigDecimal.TEN} // Min is less than max
        };
    }
}
