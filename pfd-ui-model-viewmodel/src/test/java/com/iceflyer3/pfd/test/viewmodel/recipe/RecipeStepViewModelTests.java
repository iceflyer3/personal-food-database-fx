/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.test.viewmodel.recipe;

import com.iceflyer3.pfd.ui.model.viewmodel.recipe.RecipeStepViewModel;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class RecipeStepViewModelTests
{
    /*
     * Establish property invariants:
     *  - Step must have a name
     *  - Step must have directions
     *  - Step number must be greater than 0
     */

    @Test(dataProvider = "unmetInvariantDataProvider")
    public void Recipe_Step_With_Unmet_Invariants_Is_Invalid(int stepNumber, String name, String directions)
    {
        RecipeStepViewModel sut = new RecipeStepViewModel(stepNumber);
        sut.setStepName(name);
        sut.setDirections(directions);

        assertThat(sut.validate().wasSuccessful()).isFalse();
    }

    @Test
    public void Recipe_Step_With_Met_Invariants_Is_Valid()
    {
        RecipeStepViewModel sut = new RecipeStepViewModel(1);
        sut.setStepName("UnitTestStep");
        sut.setDirections("UnitTestDirections");

        assertThat(sut.validate().wasSuccessful()).isTrue();
    }

    @DataProvider(name = "unmetInvariantDataProvider")
    public Object[][] unmetInvariantDataProvider()
    {
        return new Object[][]{
                {0, "UnitTestStep", "UnitTestDirections"},  // Invalid step number
                {1, null, "UnitTestDirections"},            // Invalid step name
                {1, "", "UnitTestDirections"},
                {1, "UnitTestStep", null},                  // Invalid directions
                {1, "UnitTestStep", ""},
        };
    }
}
