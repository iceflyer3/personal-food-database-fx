package com.iceflyer3.pfd.test.viewmodel.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023, 2025 Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.enums.Nutrient;
import com.iceflyer3.pfd.test.TestDataProvider;
import com.iceflyer3.pfd.ui.model.api.mealplanning.MealPlanNutrientSuggestionModel;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanViewModel;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class MealPlanViewModelTests
{
    private final TestDataProvider testDataProvider = new TestDataProvider();

    @Test
    public void Can_Get_Nutrient_Suggestion()
    {
        // Arrange
        List<MealPlanNutrientSuggestionModel> testNutrientSuggestions = testDataProvider.getTestMacronutrientSuggestions();
        testNutrientSuggestions.addAll(testDataProvider.getTestCustomNutrientEntries());

        BigDecimal testProteinAmount = testNutrientSuggestions.stream().filter(suggestion -> suggestion.getNutrient() == Nutrient.PROTEIN).findFirst().orElseThrow().getSuggestedAmount();
        BigDecimal testFiberAmount = testNutrientSuggestions.stream().filter(suggestion -> suggestion.getNutrient() == Nutrient.FIBER).findFirst().orElseThrow().getSuggestedAmount();

        MealPlanViewModel sut = new MealPlanViewModel(null);
        sut.getNutrientSuggestions().addAll(testNutrientSuggestions);

        // Act
        BigDecimal proteinAmount = sut.getNutrientSuggestedAmount(Nutrient.PROTEIN);
        BigDecimal fiberAmount = sut.getNutrientSuggestedAmount(Nutrient.FIBER);

        // Assert
        assertThat(proteinAmount).isEqualByComparingTo(testProteinAmount); // Test a macronutrient
        assertThat(fiberAmount).isEqualByComparingTo(testFiberAmount); // Test a custom nutrient
    }

    @Test
    public void Can_Get_Only_Custom_Nutrients()
    {
        // Arrange
        List<MealPlanNutrientSuggestionModel> testNutrientSuggestions = testDataProvider.getTestMacronutrientSuggestions();
        List<MealPlanNutrientSuggestionModel> testCustomNutrients = testDataProvider.getTestCustomNutrientEntries();
        testNutrientSuggestions.addAll(testCustomNutrients);

        MealPlanViewModel sut = new MealPlanViewModel(null);
        sut.getNutrientSuggestions().addAll(testNutrientSuggestions);

        // Act
        List<MealPlanNutrientSuggestionModel> result = sut.getCustomNutrients();

        // Assert
        assertThat(result.size()).isEqualTo(testCustomNutrients.size());
        assertThat(result.stream()
                .filter(suggestion ->
                        suggestion.getNutrient() == Nutrient.PROTEIN ||
                        suggestion.getNutrient() == Nutrient.CARBOHYDRATES ||
                        suggestion.getNutrient() == Nutrient.TOTAL_FATS)
                .toList()
                .size()
        )
        .isEqualTo(0);
    }
}
