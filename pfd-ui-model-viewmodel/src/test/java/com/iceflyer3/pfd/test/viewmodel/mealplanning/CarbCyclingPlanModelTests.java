/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.test.viewmodel.mealplanning;

import com.iceflyer3.pfd.enums.CarbCyclingMealRelation;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingMealConfigurationViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingPlanViewModel;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class CarbCyclingPlanModelTests
{
    /* Invariants:
            - Number of meals per day must be non-zero
            - Optional surplus or deficit for carbs if provided
                * Valid range 0% - 99%
            - Number of high-carb and low-carb days for the plan
                * Valid range 1 - 6. There are only 7 days in a week
                  so this range enforces you must have at least one
                  high-carb and one-low carb day per week.
            - Meal configurations must be provided
            - Meal configurations must include some carb consumption
              for both training and non-training days
         */
    private final Set<CarbCyclingMealConfigurationViewModel> validMealConfigs;
    private final Set<CarbCyclingMealConfigurationViewModel> invalidTrainingDayMealConfigs;
    private final Set<CarbCyclingMealConfigurationViewModel> invalidNonTrainingDayMealConfigs;

    public CarbCyclingPlanModelTests()
    {
        // Meal configs that include at least some carb consumption
        CarbCyclingMealConfigurationViewModel nonTrainingDayMealConfig = new CarbCyclingMealConfigurationViewModel(0, CarbCyclingMealRelation.NONE, false);
        nonTrainingDayMealConfig.setCarbConsumptionMinimumPercent(BigDecimal.ONE);
        nonTrainingDayMealConfig.setCarbConsumptionMaximumPercent(BigDecimal.TEN);

        CarbCyclingMealConfigurationViewModel trainingDayMealConfig = new CarbCyclingMealConfigurationViewModel(0, CarbCyclingMealRelation.NONE, true);
        trainingDayMealConfig.setCarbConsumptionMinimumPercent(BigDecimal.ONE);
        trainingDayMealConfig.setCarbConsumptionMaximumPercent(BigDecimal.TEN);

        validMealConfigs = new HashSet<>();
        validMealConfigs.add(nonTrainingDayMealConfig);
        validMealConfigs.add(trainingDayMealConfig);

        // Meal configs that have an invalid carb consumption total for non-training days
        CarbCyclingMealConfigurationViewModel invalidNonTrainingDayMealConfig = new CarbCyclingMealConfigurationViewModel(0, CarbCyclingMealRelation.NONE, false);
        invalidNonTrainingDayMealConfig.setCarbConsumptionMinimumPercent(BigDecimal.ZERO);

        invalidNonTrainingDayMealConfigs = new HashSet<>();
        invalidNonTrainingDayMealConfigs.add(invalidNonTrainingDayMealConfig);
        invalidNonTrainingDayMealConfigs.add(trainingDayMealConfig);

        // Meal configs that have an invalid carb consumption total for training days
        CarbCyclingMealConfigurationViewModel invalidTrainingDayMealConfig = new CarbCyclingMealConfigurationViewModel(0, CarbCyclingMealRelation.NONE, true);
        invalidTrainingDayMealConfig.setCarbConsumptionMinimumPercent(BigDecimal.ZERO);

        invalidTrainingDayMealConfigs = new HashSet<>();
        invalidTrainingDayMealConfigs.add(invalidTrainingDayMealConfig);
        invalidTrainingDayMealConfigs.add(nonTrainingDayMealConfig);

    }

    @Test(dataProvider = "unmetInvariantsDataProvider")
    public void Plan_With_UnmetInvariants_Is_Invalid(int mealsPerDay, BigDecimal carbSurplusPercent, BigDecimal carbDeficitPercent, int highCarbDays, int lowCarbDays, Set<CarbCyclingMealConfigurationViewModel> mealConfigs)
    {
        CarbCyclingPlanViewModel sut = new CarbCyclingPlanViewModel();
        sut.setMealsPerDay(mealsPerDay);
        sut.initMealConfigurations(mealConfigs);
        sut.setLowCarbDays(lowCarbDays);
        sut.setHighCarbDays(highCarbDays);
        sut.setLowCarbDayDeficitPercentage(carbDeficitPercent);
        sut.setHighCarbDaySurplusPercentage(carbSurplusPercent);

        assertThat(sut.validate().wasSuccessful()).isFalse();
    }

    @Test
    public void Plan_With_Met_Invariants_Is_Valid()
    {
        CarbCyclingPlanViewModel sut = new CarbCyclingPlanViewModel();
        sut.setMealsPerDay(3);
        sut.initMealConfigurations(validMealConfigs);
        sut.setLowCarbDays(1);
        sut.setHighCarbDays(2);
        sut.setLowCarbDayDeficitPercentage(BigDecimal.ZERO);
        sut.setHighCarbDaySurplusPercentage(BigDecimal.ZERO);

        assertThat(sut.validate().wasSuccessful()).isTrue();
    }

    @DataProvider(name = "unmetInvariantsDataProvider")
    public Object[][] unmetInvariantsDataProvider()
    {
        return new Object[][]{
                {0, BigDecimal.ZERO, BigDecimal.ZERO, 1, 2, validMealConfigs},                // Invalid meal per day count
                {3, BigDecimal.valueOf(-1), BigDecimal.ZERO, 1, 2, validMealConfigs},         // Invalid carb deficit percentage
                {3, BigDecimal.ZERO, BigDecimal.valueOf(-1), 1, 2, validMealConfigs},         // Invalid carb surplus percentage
                {3, BigDecimal.ZERO, BigDecimal.ZERO, 0, 2, validMealConfigs},                // Low-carb day count below minimum
                {3, BigDecimal.ZERO, BigDecimal.ZERO, 7, 2, validMealConfigs},                // Low-carb day count exceeds maximum
                {3, BigDecimal.ZERO, BigDecimal.ZERO, 1, 0, validMealConfigs},                // High-carb day count below minimum
                {3, BigDecimal.ZERO, BigDecimal.ZERO, 1, 7, validMealConfigs},                // High-carb day count exceeds maximum
                {3, BigDecimal.ZERO, BigDecimal.ZERO, 1, 7, Collections.emptySet()},          // No meal configs provided
                {3, BigDecimal.ZERO, BigDecimal.ZERO, 1, 7, invalidTrainingDayMealConfigs},   // Invalid training day meal configs provided
                {3, BigDecimal.ZERO, BigDecimal.ZERO, 1, 7, invalidNonTrainingDayMealConfigs} // Invalid non-training day meal configs provided
        };
    }
}
