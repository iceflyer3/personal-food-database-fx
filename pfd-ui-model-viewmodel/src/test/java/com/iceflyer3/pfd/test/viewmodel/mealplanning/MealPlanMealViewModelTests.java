/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.test.viewmodel.mealplanning;

import com.iceflyer3.pfd.test.TestDataProvider;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanMealViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Collection;
import java.util.HashSet;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class MealPlanMealViewModelTests
{
    /*
     * Establish property invariants
     *  - The meal plan day must have a name / label
     *  - The meal must have at least one ingredient
     *    (but only when previously saved [aka has an ID])
     */

    private final TestDataProvider testDataProvider = new TestDataProvider();

    @Test(dataProvider = "unmetInvariantsDataProvider")
    public void Meal_With_Unmet_Invariants_Is_Invalid(UUID mealId, String mealName, Collection<IngredientModel> ingredients)
    {
        MealPlanMealViewModel sut = new MealPlanMealViewModel(mealId, NutritionalSummary.empty());
        sut.setName(mealName);
        sut.setIngredients(ingredients);

        assertThat(sut.validate().wasSuccessful()).isFalse();
    }

    @Test(dataProvider = "metInvariantsDataProvider")
    public void Meal_With_Met_Invariants_Is_Valid(UUID mealId, String mealName, Collection<IngredientModel> ingredients)
    {
        MealPlanMealViewModel sut = new MealPlanMealViewModel(mealId, NutritionalSummary.empty());
        sut.setName(mealName);
        sut.setIngredients(ingredients);

        assertThat(sut.validate().wasSuccessful()).isTrue();
    }

    @DataProvider(name = "unmetInvariantsDataProvider")
    public Object[][] unmetInvariantsDataProvider()
    {
        return new Object[][]{
                {null, null, null},
                {null, "", null},
                {UUID.randomUUID(), "UnitTestMeal", new HashSet<>()},
                {UUID.randomUUID(), "", testDataProvider.getTestIngredientViewModelSet(1)}
        };
    }

    @DataProvider(name = "metInvariantsDataProvider")
    public Object[][] metInvariantsDataProvider()
    {
        return new Object[][]{
                {null, "UnitTestMeal", null},
                {UUID.randomUUID(), "UnitTestMeal", testDataProvider.getTestIngredientViewModelSet(1)}
        };
    }
}
