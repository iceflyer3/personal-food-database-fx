/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.test.viewmodel;

import com.iceflyer3.pfd.ui.model.viewmodel.UserViewModel;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class UserViewModelTests
{
    /*
     * Invariants:
     *  - The name of the user
     */

    @Test(dataProvider = "unmetInvariantsDataProvider")
    public void User_With_Unmet_Invariants_Is_Invalid(String username)
    {
        UserViewModel sut = new UserViewModel(UUID.randomUUID(), username);

        assertThat(sut.validate().wasSuccessful()).isFalse();
    }

    @Test
    public void User_With_Met_Invariants_Is_Valid()
    {
        UserViewModel sut = new UserViewModel(UUID.randomUUID(), "UnitTestUser");

        assertThat(sut.validate().wasSuccessful()).isTrue();
    }

    @DataProvider(name = "unmetInvariantsDataProvider")
    public Object[][] unmetInvariantsDataProvider()
    {
        return new Object[][]{
                {null},
                {""}
        };
    }
}
