/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


package com.iceflyer3.pfd.test.viewmodel.mealplanning;

import com.iceflyer3.pfd.test.TestDataProvider;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling.CarbCyclingMealViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;
import org.testng.annotations.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class CarbCyclingMealViewModelTests
{
    /*
     * Invariants:
     *  - The meal must have a non-zero meal number
     *
     * As an extension of the MealPlanMealViewModel this view model also shares the invariants of
     * that class. However, they aren't tested here because they have their own individual tests.
     */

    private final TestDataProvider testDataProvider = new TestDataProvider();

    @Test
    public void CarbCyclingMeal_With_Unmet_Invariants_Is_Invalid()
    {
        CarbCyclingMealViewModel sut = new CarbCyclingMealViewModel(UUID.randomUUID(), NutritionalSummary.empty());
        sut.setName("UnitTestName");
        sut.setIngredients(testDataProvider.getTestIngredientViewModelSet(1));
        sut.setMealNumber(0);

        assertThat(sut.validate().wasSuccessful()).isFalse();
    }

    @Test
    public void CarbCyclingMeal_With_Met_Invariants_Is_Valid()
    {
        CarbCyclingMealViewModel sut = new CarbCyclingMealViewModel(UUID.randomUUID(), NutritionalSummary.empty());
        sut.setName("UnitTestName");
        sut.setIngredients(testDataProvider.getTestIngredientViewModelSet(1));
        sut.setMealNumber(1);

        assertThat(sut.validate().wasSuccessful()).isTrue();
    }
}
