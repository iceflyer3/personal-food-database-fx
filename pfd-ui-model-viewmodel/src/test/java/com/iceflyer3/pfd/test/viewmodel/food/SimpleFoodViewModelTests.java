/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.test.viewmodel.food;

import com.iceflyer3.pfd.ui.model.viewmodel.food.SimpleFoodViewModel;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class SimpleFoodViewModelTests
{
    /*
     * Invariants
     *  - A name, source, and a brand
     *  - Non-zero caloric value
     *  - At least one serving size
     */

    @Test(dataProvider = "unmetVariantsForSimpleFoodDataProvider")
    public void Simple_Food_With_Unmet_Invariants_Is_Invalid(String name, String source, String brand, BigDecimal calories, boolean hasServingSize)
    {
        SimpleFoodViewModel sut = new SimpleFoodViewModel("UnitTest");
        sut.setName(name);
        sut.setSource(source);
        sut.setBrand(brand);
        sut.setCalories(calories);

        if (hasServingSize)
        {
            sut.addServingSize();
        }

        assertThat(sut.validate().wasSuccessful()).isFalse();
    }

    @Test
    public void Simple_Food_With_Met_Invariants_Is_Valid()
    {
        SimpleFoodViewModel sut = new SimpleFoodViewModel("UnitTest");
        sut.setName("Foo");
        sut.setSource("Bar");
        sut.setBrand("Baz");
        sut.setCalories(BigDecimal.ONE);
        sut.addServingSize();

        assertThat(sut.validate().wasSuccessful()).isTrue();
    }

    /*
     * Data provider for the test that tests that the validate() function returns a result indicating
     * failure if one of the invariants of a Simple Food isn't met
     */
    @DataProvider(name = "unmetVariantsForSimpleFoodDataProvider")
    public Object[][] unmetVariantsForSimpleFoodDataProvider()
    {
        return new Object[][]{
                {null, "UnitTestSource", "UnitTestBrand", BigDecimal.ONE, true},
                {"", "UnitTestSource", "UnitTestBrand", BigDecimal.ONE, true},
                {"UnitTestName", null, "UnitTestBrand", BigDecimal.ONE, true},
                {"UnitTestName", "", "UnitTestBrand", BigDecimal.ONE, true},
                {"UnitTestName", "UnitTestSource", null, BigDecimal.ONE, true},
                {"UnitTestName", "UnitTestSource", "", BigDecimal.ONE, true},
                {"UnitTestName", "UnitTestSource", "UnitTestBrand", null, true},
                {"UnitTestName", "UnitTestSource", "UnitTestBrand", BigDecimal.ZERO, true},
                {"UnitTestName", "UnitTestSource", "UnitTestBrand", BigDecimal.ONE, false},
        };
    }
}
