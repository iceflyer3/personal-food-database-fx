/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.test.viewmodel.mealplanning;

import com.iceflyer3.pfd.test.TestDataProvider;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanDayViewModel;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class MealPlanDayViewModelTests
{
    /*
     * Invariants
     *  - The meal plan day must have a name / label
     */
    private final TestDataProvider testDataProvider = new TestDataProvider();

    @Test
    public void MealPlanDay_With_Unmet_Invariants_Is_Invalid()
    {
        MealPlanDayViewModel sut = testDataProvider.getTestMealPlanDayViewModel();

        sut.setDayLabel(null);
        assertThat(sut.validate().wasSuccessful()).isFalse();

        sut.setDayLabel("");
        assertThat(sut.validate().wasSuccessful()).isFalse();
    }

    @Test
    public void MealPlanDay_With_Met_Invariants_Is_Valid()
    {
        MealPlanDayViewModel sut = testDataProvider.getTestMealPlanDayViewModel();
        sut.setDayLabel("UnitTestDay");

        assertThat(sut.validate().wasSuccessful()).isTrue();
    }
}
