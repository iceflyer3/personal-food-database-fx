/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.test.viewmodel;

import com.iceflyer3.pfd.test.TestDataProvider;
import com.iceflyer3.pfd.ui.model.viewmodel.IngredientViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.ReadOnlyFoodViewModel;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class IngredientViewModelTests
{
    /*
     * Invariants
     *  - Must have at least a single serving of each associated food that is
     *    needed for this ingredient
     */

    private final ReadOnlyFoodViewModel ingredientFood;

    public IngredientViewModelTests()
    {
        TestDataProvider testDataProvider = new TestDataProvider();
        ingredientFood = new ReadOnlyFoodViewModel(testDataProvider.getTestSimpleFoodViewModel(1));
    }

    @Test
    public void Ingredient_With_Unmet_Invariants_Is_Invalid()
    {
        IngredientViewModel sut = new IngredientViewModel(ingredientFood);

        sut.setServingsIncluded(null);
        assertThat(sut.validate().wasSuccessful()).isFalse();

        sut.setServingsIncluded(BigDecimal.ZERO);
        assertThat(sut.validate().wasSuccessful()).isFalse();
    }

    @Test
    public void Ingredient_With_Met_Invariants_Is_Valid()
    {
        IngredientViewModel sut = new IngredientViewModel(ingredientFood);
        sut.setServingsIncluded(BigDecimal.ONE);

        assertThat(sut.validate().wasSuccessful()).isTrue();
    }
}
