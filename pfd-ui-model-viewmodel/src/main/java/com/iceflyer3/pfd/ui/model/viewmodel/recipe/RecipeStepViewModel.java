package com.iceflyer3.pfd.ui.model.viewmodel.recipe;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.api.food.ReadOnlyFoodModel;
import com.iceflyer3.pfd.ui.model.api.owner.IngredientWriter;
import com.iceflyer3.pfd.ui.model.api.recipe.RecipeStepModel;
import com.iceflyer3.pfd.ui.model.api.viewmodel.ValidatingViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.IngredientViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.ReadOnlyFoodViewModel;
import com.iceflyer3.pfd.validation.*;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.*;

/**
 * View model that represents a single step in the process of baking a recipe.
 *
 * Note that this class needs no read-only variant because it does not have its own proxy.
 *
 * It will only ever be saved from the RecipeModelProxy of which the read-only variant already
 * disallows saving. So even if you make changes from a screen from which they should not
 * be made (which would be a silly thing to do in the first place, but accidents happen)
 * you won't be able to save them.
 */
public class RecipeStepViewModel implements RecipeStepModel, ValidatingViewModel, IngredientWriter, Comparable<RecipeStepViewModel> {
    private final UUID stepId;

    private final StringProperty stepName;
    private final StringProperty directions;
    private final IntegerProperty stepNumber;

    // This list will always be a subset of the list of ingredients found on the Recipe that are
    // needed for this step in creating the recipe
    private final ListProperty<IngredientModel> ingredients;

    /*
     * Maps the HashCode of a property to any restrictions that should be enforced
     * to ensure correct and valid values for that property.
     *
     * Collectively this map contains all the invariants that have to do with an individual
     * property. But not necessarily all invariants of the class.
     */
    private final Map<Integer, PropertyRestrictionBinding> propertyInvariants;

    public RecipeStepViewModel(int stepNumber) {
        this(null, "", "", stepNumber, new ArrayList<>());
    }

    public RecipeStepViewModel(UUID stepId,
                               String stepName,
                               String directions,
                               int stepNumber,
                               Collection<IngredientModel> stepIngredients)
    {
        this.stepId = stepId;
        this.stepName = new SimpleStringProperty(stepName);
        this.directions = new SimpleStringProperty(directions);
        this.stepNumber = new SimpleIntegerProperty(stepNumber);
        this.ingredients = new SimpleListProperty<>(FXCollections.observableArrayList(stepIngredients));

        /*
         * Establish property invariants:
         *  - Step must have a name
         *  - Step must have directions
         *  - Step number must be greater than 0
         */
        ObservableValuePropertyRestrictionBinding<String> namePropertyRestriction = new ObservableValuePropertyRestrictionBinding<>(this.stepName);
        namePropertyRestriction.bindRestriction(new MinimumLengthDatumRestriction(1));

        ObservableValuePropertyRestrictionBinding<Number> stepNumberPropertyRestriction = new ObservableValuePropertyRestrictionBinding<>(this.stepNumber);
        stepNumberPropertyRestriction.bindRestriction(new NonZeroDatumRestriction());

        ObservableValuePropertyRestrictionBinding<String> directionsPropertyRestriction = new ObservableValuePropertyRestrictionBinding<>(this.directions);
        directionsPropertyRestriction.bindRestriction(new MinimumLengthDatumRestriction(1));

        this.propertyInvariants = new HashMap<>();
        this.propertyInvariants.put(this.stepName.hashCode(), namePropertyRestriction);
        this.propertyInvariants.put(this.stepNumber.hashCode(), stepNumberPropertyRestriction);
        this.propertyInvariants.put(this.directions.hashCode(), directionsPropertyRestriction);
    }

    @Override
    public ValidationResult validate() {
        /*
         * It is possible that some steps may have no ingredients.
         * So we don't enforce requiring one or more for validity.
         */
        Set<ValidationResult> propertyInvariantResults = new HashSet<>();
        for(Map.Entry<Integer, PropertyRestrictionBinding> entry : propertyInvariants.entrySet())
        {
            propertyInvariantResults.add(entry.getValue().applyRestrictions());
        }

        return ValidationResult.flatten(propertyInvariantResults);
    }

    @Override
    public ValidationResult validateProperty(int propertyHashcode)
    {
        return propertyInvariants.get(propertyHashcode).applyRestrictions();
    }

    public UUID getId() {
        return stepId;
    }

    public String getStepName() {
        return stepName.get();
    }

    public StringProperty stepNameProperty() {
        return stepName;
    }

    public void setStepName(String stepName) {
        this.stepName.set(stepName);
    }

    public int getStepNumber() {
        return stepNumber.get();
    }

    public IntegerProperty stepNumberProperty() {
        return stepNumber;
    }

    public void setStepNumber(int stepNumber) {
        this.stepNumber.set(stepNumber);
    }

    public String getDirections() {
        return directions.get();
    }

    public StringProperty directionsProperty() {
        return directions;
    }

    public void setDirections(String directions) {
        this.directions.set(directions);
    }

    public ObservableList<IngredientModel> getIngredients() {
        return ingredients.get();
    }

    public ListProperty<IngredientModel> ingredientsProperty() {
        return ingredients;
    }

    @Override
    public String toString() {
        return "RecipeStepViewModel{" +
                "stepId=" + stepId +
                ", stepName=" + stepName +
                ", directions=" + directions +
                ", stepNumber=" + stepNumber +
                ", ingredients=" + ingredients +
                '}';
    }

    @Override
    public int compareTo(RecipeStepViewModel o) {
        return Integer.compare(getStepNumber(), o.getStepNumber());
    }

    /**
     * Add a new ingredient to the list of ingredients. The provided ingredient should be of the view model
     * implementation type which is ReadOnlyFoodViewModel.
     *
     * @param ingredient The ReadOnlyFoodViewModel instance that represents the ingredient food
     */
    @Override
    public void addIngredient(ReadOnlyFoodModel ingredient)
    {
        if (ingredient instanceof ReadOnlyFoodViewModel)
        {
            this.ingredients.add(new IngredientViewModel(ingredient));
        }
        else
        {
            throw new IllegalArgumentException("ingredientFood must be an instance of ReadOnlyFoodViewModel");
        }
    }

    @Override
    public void removeIngredient(IngredientModel ingredient)
    {
        this.ingredients.remove(ingredient);
    }
}
