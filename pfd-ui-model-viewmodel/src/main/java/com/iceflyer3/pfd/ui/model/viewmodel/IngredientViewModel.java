package com.iceflyer3.pfd.ui.model.viewmodel;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.api.food.ReadOnlyFoodModel;

import com.iceflyer3.pfd.ui.model.viewmodel.summary.base.AbstractPropertyBasedNutritionFactsReader;
import com.iceflyer3.pfd.util.NumberUtils;
import com.iceflyer3.pfd.validation.ObservableValuePropertyRestrictionBinding;
import com.iceflyer3.pfd.validation.ValidationResult;
import com.iceflyer3.pfd.validation.NonZeroDatumRestriction;
import javafx.beans.property.*;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.UUID;

public class IngredientViewModel extends AbstractPropertyBasedNutritionFactsReader implements IngredientModel
{
    private final UUID ingredientId;

    private final ObjectProperty<ReadOnlyFoodModel> ingredientFood;
    private final ObjectProperty<BigDecimal> servingsIncluded;
    private final BooleanProperty isAlternate;
    private final BooleanProperty isOptional;

    private final ObservableValuePropertyRestrictionBinding<BigDecimal> servingsPropertyRestriction;

    /**
     * Create a new Ingredient that hasn't yet been persisted to the database but will
     * be on the next meal save operation.
     * @param ingredientFood The food that is the ingredient
     */
    public IngredientViewModel(ReadOnlyFoodModel ingredientFood)
    {
        this(null, ingredientFood, BigDecimal.ONE);
    }

    /**
     * Create a new Ingredient that has previously been persisted to the database.
     * @param ingredientId The ID of this ingredient in the database
     * @param ingredientFood The food that is the ingredient
     */
    public IngredientViewModel(UUID ingredientId, ReadOnlyFoodModel ingredientFood, BigDecimal servingsIncluded) {
        this.ingredientId = ingredientId;

        this.ingredientFood = new SimpleObjectProperty<>(ingredientFood);
        this.servingsIncluded = new SimpleObjectProperty<>(servingsIncluded);
        this.isAlternate = new SimpleBooleanProperty(false);
        this.isOptional = new SimpleBooleanProperty(false);

        // Nutritional data properties - defaults to a single serving
        this.calories = new ReadOnlyObjectWrapper<>(ingredientFood.getCalories());

        // Macronutrients
        this.protein = new ReadOnlyObjectWrapper<>(ingredientFood.getProtein());
        this.totalFats = new ReadOnlyObjectWrapper<>(ingredientFood.getTotalFats());
        this.transFat = new ReadOnlyObjectWrapper<>(ingredientFood.getTransFat());
        this.monounsaturatedFat = new ReadOnlyObjectWrapper<>(ingredientFood.getMonounsaturatedFat());
        this.polyunsaturatedFat = new ReadOnlyObjectWrapper<>(ingredientFood.getPolyunsaturatedFat());
        this.saturatedFat = new ReadOnlyObjectWrapper<>(ingredientFood.getSaturatedFat());
        this.carbohydrates = new ReadOnlyObjectWrapper<>(ingredientFood.getCarbohydrates());
        this.sugars = new ReadOnlyObjectWrapper<>(ingredientFood.getSugars());
        this.fiber = new ReadOnlyObjectWrapper<>(ingredientFood.getFiber());

        // Micronutrients - Lipids
        this.cholesterol = new ReadOnlyObjectWrapper<>(ingredientFood.getCholesterol());

        // Micronutrients - Minerals
        this.iron = new ReadOnlyObjectWrapper<>(ingredientFood.getIron());
        this.manganese = new ReadOnlyObjectWrapper<>(ingredientFood.getManganese());
        this.copper = new ReadOnlyObjectWrapper<>(ingredientFood.getCopper());
        this.zinc = new ReadOnlyObjectWrapper<>(ingredientFood.getZinc());
        this.calcium = new ReadOnlyObjectWrapper<>(ingredientFood.getCalcium());
        this.magnesium = new ReadOnlyObjectWrapper<>(ingredientFood.getMagnesium());
        this.phosphorus = new ReadOnlyObjectWrapper<>(ingredientFood.getPhosphorus());
        this.potassium = new ReadOnlyObjectWrapper<>(ingredientFood.getPotassium());
        this.sodium  = new ReadOnlyObjectWrapper<>(ingredientFood.getSodium());
        this.sulfur = new ReadOnlyObjectWrapper<>(ingredientFood.getSulfur());

        // Micronutrients - Vitamins
        this.vitaminA = new ReadOnlyObjectWrapper<>(ingredientFood.getVitaminA());
        this.vitaminC = new ReadOnlyObjectWrapper<>(ingredientFood.getVitaminC());
        this.vitaminK = new ReadOnlyObjectWrapper<>(ingredientFood.getVitaminK());
        this.vitaminD = new ReadOnlyObjectWrapper<>(ingredientFood.getVitaminD());
        this.vitaminE = new ReadOnlyObjectWrapper<>(ingredientFood.getVitaminE());
        this.vitaminB1 = new ReadOnlyObjectWrapper<>(ingredientFood.getVitaminB1());
        this.vitaminB2 = new ReadOnlyObjectWrapper<>(ingredientFood.getVitaminB2());
        this.vitaminB3 = new ReadOnlyObjectWrapper<>(ingredientFood.getVitaminB3());
        this.vitaminB5 = new ReadOnlyObjectWrapper<>(ingredientFood.getVitaminB5());
        this.vitaminB6 = new ReadOnlyObjectWrapper<>(ingredientFood.getVitaminB6());
        this.vitaminB7 = new ReadOnlyObjectWrapper<>(ingredientFood.getVitaminB7());
        this.vitaminB8 = new ReadOnlyObjectWrapper<>(ingredientFood.getVitaminB8());
        this.vitaminB9 = new ReadOnlyObjectWrapper<>(ingredientFood.getVitaminB9());
        this.vitaminB12 = new ReadOnlyObjectWrapper<>(ingredientFood.getVitaminB12());

        /*
         * Establish property invariants
         *  - The number of servings of the associated food that are needed for this ingredient
         */
        servingsPropertyRestriction = new ObservableValuePropertyRestrictionBinding<>(this.servingsIncluded);
        servingsPropertyRestriction.bindRestriction(new NonZeroDatumRestriction());

        // Now calculate initial nutritional data
        refreshNutritionDetails();
    }

    @Override
    public UUID getId() {
        return ingredientId;
    }

    @Override
    public ReadOnlyFoodModel getIngredientFood() {
        return ingredientFood.get();
    }

    @Override
    public ObjectProperty<ReadOnlyFoodModel> ingredientFoodProperty() {
        return ingredientFood;
    }

    @Override
    public BigDecimal getServingsIncluded() {
        return servingsIncluded.get();
    }

    @Override
    public ObjectProperty<BigDecimal> servingsIncludedProperty() {
        return servingsIncluded;
    }

    @Override
    public void setServingsIncluded(BigDecimal servingsIncluded) {
        this.servingsIncluded.set(servingsIncluded);
    }

    @Override
    public boolean isAlternate() {
        return isAlternate.get();
    }

    @Override
    public BooleanProperty isAlternateProperty() {
        return isAlternate;
    }

    @Override
    public void setIsAlternate(boolean isAlternate) {
        this.isAlternate.set(isAlternate);
    }

    @Override
    public boolean isOptional() {
        return isOptional.get();
    }

    @Override
    public BooleanProperty isOptionalProperty() {
        return isOptional;
    }

    @Override
    public void setIsOptional(boolean isOptional) {
        this.isOptional.set(isOptional);
    }

    @Override
    public ValidationResult validate() {

        return servingsPropertyRestriction.applyRestrictions();
    }

    @Override
    public ValidationResult validateProperty(int propertyHashcode)
    {
        return servingsPropertyRestriction.applyRestrictions();
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }

        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        /*
         * Careful readers will note that Alternate and Optional status aren't included in the comparison
         * here. This is because they don't really need to be.
         *
         * You can't have the same ingredient twice (you would just have two servings of a single ingredient).
         * As such the optional / alternate status of an ingredient doesn't help to uniquely identify it.
         *
         * The name and brand of the food and the number of servings do uniquely identify an individual
         * ingredient. Thus, they are included in the comparison here.
         */
        IngredientViewModel that = (IngredientViewModel) o;
        return Objects.equals(ingredientId, that.ingredientId)
                && ingredientFood.get().getId().equals(that.ingredientFood.get().getId())
                && servingsIncluded.get().compareTo(that.servingsIncluded.get()) == 0;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(ingredientId, ingredientFood.get(), servingsIncluded.get());
    }

    @Override
    public String toString() {
        return "IngredientViewModel{" +
                "ingredientId=" + ingredientId +
                ", ingredientFood=" + ingredientFood +
                ", servingsIncluded=" + servingsIncluded +
                ", isAlternate=" + isAlternate +
                ", isOptional=" + isOptional +
                '}';
    }

    @Override
    public void refreshNutritionDetails()
    {
        ReadOnlyFoodModel ingredientFood = this.ingredientFood.get();
        BigDecimal servings = servingsIncluded.get();

        // Nutritional data
        this.calories.set(NumberUtils.withStandardPrecision(ingredientFood.getCalories().multiply(servings)));

        // Macronutrients
        this.protein.set(NumberUtils.withStandardPrecision(ingredientFood.getProtein().multiply(servings)));
        this.totalFats.set(NumberUtils.withStandardPrecision(ingredientFood.getTotalFats().multiply(servings)));
        this.transFat.set(NumberUtils.withStandardPrecision(ingredientFood.getTransFat().multiply(servings)));
        this.monounsaturatedFat.set(NumberUtils.withStandardPrecision(ingredientFood.getMonounsaturatedFat().multiply(servings)));
        this.polyunsaturatedFat.set(NumberUtils.withStandardPrecision(ingredientFood.getPolyunsaturatedFat().multiply(servings)));
        this.saturatedFat.set(NumberUtils.withStandardPrecision(ingredientFood.getSaturatedFat().multiply(servings)));
        this.carbohydrates.set(NumberUtils.withStandardPrecision(ingredientFood.getCarbohydrates().multiply(servings)));
        this.sugars.set(NumberUtils.withStandardPrecision(ingredientFood.getSugars().multiply(servings)));
        this.fiber.set(NumberUtils.withStandardPrecision(ingredientFood.getFiber().multiply(servings)));

        // Micronutrients - Lipids
        this.cholesterol.set(NumberUtils.withStandardPrecision(ingredientFood.getCholesterol().multiply(servings)));

        // Micronutrients - Minerals
        this.iron.set(NumberUtils.withStandardPrecision(ingredientFood.getIron().multiply(servings)));
        this.manganese.set(NumberUtils.withStandardPrecision(ingredientFood.getManganese().multiply(servings)));
        this.copper.set(NumberUtils.withStandardPrecision(ingredientFood.getCopper().multiply(servings)));
        this.zinc.set(NumberUtils.withStandardPrecision(ingredientFood.getZinc().multiply(servings)));
        this.calcium.set(NumberUtils.withStandardPrecision(ingredientFood.getCalcium().multiply(servings)));
        this.magnesium.set(NumberUtils.withStandardPrecision(ingredientFood.getMagnesium().multiply(servings)));
        this.phosphorus.set(NumberUtils.withStandardPrecision(ingredientFood.getPhosphorus().multiply(servings)));
        this.potassium.set(NumberUtils.withStandardPrecision(ingredientFood.getPotassium().multiply(servings)));
        this.sodium.set(NumberUtils.withStandardPrecision(ingredientFood.getSodium().multiply(servings)));
        this.sulfur.set(NumberUtils.withStandardPrecision(ingredientFood.getSulfur().multiply(servings)));

        // Micronutrients - Vitamins
        this.vitaminA.set(NumberUtils.withStandardPrecision(ingredientFood.getVitaminA().multiply(servings)));
        this.vitaminC.set(NumberUtils.withStandardPrecision(ingredientFood.getVitaminC().multiply(servings)));
        this.vitaminK.set(NumberUtils.withStandardPrecision(ingredientFood.getVitaminK().multiply(servings)));
        this.vitaminD.set(NumberUtils.withStandardPrecision(ingredientFood.getVitaminD().multiply(servings)));
        this.vitaminE.set(NumberUtils.withStandardPrecision(ingredientFood.getVitaminE().multiply(servings)));
        this.vitaminB1.set(NumberUtils.withStandardPrecision(ingredientFood.getVitaminB1().multiply(servings)));
        this.vitaminB2.set(NumberUtils.withStandardPrecision(ingredientFood.getVitaminB2().multiply(servings)));
        this.vitaminB3.set(NumberUtils.withStandardPrecision(ingredientFood.getVitaminB3().multiply(servings)));
        this.vitaminB5.set(NumberUtils.withStandardPrecision(ingredientFood.getVitaminB5().multiply(servings)));
        this.vitaminB6.set(NumberUtils.withStandardPrecision(ingredientFood.getVitaminB6().multiply(servings)));
        this.vitaminB7.set(NumberUtils.withStandardPrecision(ingredientFood.getVitaminB7().multiply(servings)));
        this.vitaminB8.set(NumberUtils.withStandardPrecision(ingredientFood.getVitaminB8().multiply(servings)));
        this.vitaminB9.set(NumberUtils.withStandardPrecision(ingredientFood.getVitaminB9().multiply(servings)));
        this.vitaminB12.set(NumberUtils.withStandardPrecision(ingredientFood.getVitaminB12().multiply(servings)));
    }
}
