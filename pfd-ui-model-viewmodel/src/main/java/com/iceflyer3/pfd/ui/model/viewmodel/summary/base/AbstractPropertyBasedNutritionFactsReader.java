package com.iceflyer3.pfd.ui.model.viewmodel.summary.base;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.ui.model.api.NutritionFactsReader;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;

import java.math.BigDecimal;

/**
 * NutritionFactsReader implementation backed by individual JavaFx properties for each datum that
 * makes up the nutrition facts data.
 *
 * Concrete classes are responsible for instantiating each of the properties and providing values
 * for them. This is just a convenience base class for sharing of getters and properties to prevent
 * needless code duplication.
 */
public abstract class AbstractPropertyBasedNutritionFactsReader implements NutritionFactsReader
{
    protected ReadOnlyObjectWrapper<BigDecimal> calories;

    // Macronutrients
    protected ReadOnlyObjectWrapper<BigDecimal> protein;
    protected ReadOnlyObjectWrapper<BigDecimal> totalFats;
    protected ReadOnlyObjectWrapper<BigDecimal> transFat;
    protected ReadOnlyObjectWrapper<BigDecimal> monounsaturatedFat;
    protected ReadOnlyObjectWrapper<BigDecimal> polyunsaturatedFat;
    protected ReadOnlyObjectWrapper<BigDecimal> saturatedFat;
    protected ReadOnlyObjectWrapper<BigDecimal> carbohydrates;
    protected ReadOnlyObjectWrapper<BigDecimal> sugars;
    protected ReadOnlyObjectWrapper<BigDecimal> fiber;

    // Micronutrients - Lipids
    protected ReadOnlyObjectWrapper<BigDecimal> cholesterol;

    // Micronutrients - Minerals
    protected ReadOnlyObjectWrapper<BigDecimal> iron;
    protected ReadOnlyObjectWrapper<BigDecimal> manganese;
    protected ReadOnlyObjectWrapper<BigDecimal> copper;
    protected ReadOnlyObjectWrapper<BigDecimal> zinc;
    protected ReadOnlyObjectWrapper<BigDecimal> calcium;
    protected ReadOnlyObjectWrapper<BigDecimal> magnesium;
    protected ReadOnlyObjectWrapper<BigDecimal> phosphorus;
    protected ReadOnlyObjectWrapper<BigDecimal> potassium;
    protected ReadOnlyObjectWrapper<BigDecimal> sodium;
    protected ReadOnlyObjectWrapper<BigDecimal> sulfur;

    // Micronutrients - Vitamins
    protected ReadOnlyObjectWrapper<BigDecimal> vitaminA;
    protected ReadOnlyObjectWrapper<BigDecimal> vitaminC;
    protected ReadOnlyObjectWrapper<BigDecimal> vitaminK;
    protected ReadOnlyObjectWrapper<BigDecimal> vitaminD;
    protected ReadOnlyObjectWrapper<BigDecimal> vitaminE;
    protected ReadOnlyObjectWrapper<BigDecimal> vitaminB1;
    protected ReadOnlyObjectWrapper<BigDecimal> vitaminB2;
    protected ReadOnlyObjectWrapper<BigDecimal> vitaminB3;
    protected ReadOnlyObjectWrapper<BigDecimal> vitaminB5;
    protected ReadOnlyObjectWrapper<BigDecimal> vitaminB6;
    protected ReadOnlyObjectWrapper<BigDecimal> vitaminB7;
    protected ReadOnlyObjectWrapper<BigDecimal> vitaminB8;
    protected ReadOnlyObjectWrapper<BigDecimal> vitaminB9;
    protected ReadOnlyObjectWrapper<BigDecimal> vitaminB12;

    @Override
    public BigDecimal getCalories() {
        return calories.get();
    }
    @Override
    public ReadOnlyObjectProperty<BigDecimal> caloriesProperty() {
        return calories.getReadOnlyProperty();
    }

    @Override
    public BigDecimal getProtein() {
        return protein.get();
    }
    @Override
    public ReadOnlyObjectProperty<BigDecimal> proteinProperty() {
        return protein.getReadOnlyProperty();
    }

    @Override
    public BigDecimal getTotalFats() {
        return totalFats.get();
    }
    @Override
    public ReadOnlyObjectProperty<BigDecimal> totalFatsProperty() {
        return totalFats.getReadOnlyProperty();
    }

    @Override
    public BigDecimal getTransFat() {
        return transFat.get();
    }
    @Override
    public ReadOnlyObjectProperty<BigDecimal> transFatProperty() {
        return transFat.getReadOnlyProperty();
    }

    @Override
    public BigDecimal getMonounsaturatedFat() {
        return monounsaturatedFat.get();
    }
    @Override
    public ReadOnlyObjectProperty<BigDecimal> monounsaturatedFatProperty() {
        return monounsaturatedFat.getReadOnlyProperty();
    }

    @Override
    public BigDecimal getPolyunsaturatedFat() {
        return polyunsaturatedFat.get();
    }
    @Override
    public ReadOnlyObjectProperty<BigDecimal> polyunsaturatedFatProperty() {
        return polyunsaturatedFat.getReadOnlyProperty();
    }

    @Override
    public BigDecimal getSaturatedFat() {
        return saturatedFat.get();
    }
    @Override
    public ReadOnlyObjectProperty<BigDecimal> saturatedFatProperty() {
        return saturatedFat.getReadOnlyProperty();
    }

    @Override
    public BigDecimal getCarbohydrates() {
        return carbohydrates.get();
    }
    @Override
    public ReadOnlyObjectProperty<BigDecimal> carbohydratesProperty() {
        return carbohydrates.getReadOnlyProperty();
    }

    @Override
    public BigDecimal getSugars() {
        return sugars.get();
    }
    @Override
    public ReadOnlyObjectProperty<BigDecimal> sugarsProperty() {
        return sugars.getReadOnlyProperty();
    }

    @Override
    public BigDecimal getFiber() {
        return fiber.get();
    }
    @Override
    public ReadOnlyObjectProperty<BigDecimal> fiberProperty() {
        return fiber.getReadOnlyProperty();
    }

    @Override
    public BigDecimal getCholesterol() {
        return cholesterol.get();
    }
    @Override
    public ReadOnlyObjectProperty<BigDecimal> cholesterolProperty() {
        return cholesterol.getReadOnlyProperty();
    }

    @Override
    public BigDecimal getIron() {
        return iron.get();
    }
    @Override
    public ReadOnlyObjectProperty<BigDecimal> ironProperty() {
        return iron.getReadOnlyProperty();
    }

    @Override
    public BigDecimal getManganese() {
        return manganese.get();
    }
    @Override
    public ReadOnlyObjectProperty<BigDecimal> manganeseProperty() {
        return manganese.getReadOnlyProperty();
    }

    @Override
    public BigDecimal getCopper() {
        return copper.get();
    }
    @Override
    public ReadOnlyObjectProperty<BigDecimal> copperProperty() {
        return copper.getReadOnlyProperty();
    }

    @Override
    public BigDecimal getZinc() {
        return zinc.get();
    }
    @Override
    public ReadOnlyObjectProperty<BigDecimal> zincProperty() {
        return zinc.getReadOnlyProperty();
    }

    @Override
    public BigDecimal getCalcium() {
        return calcium.get();
    }
    @Override
    public ReadOnlyObjectProperty<BigDecimal> calciumProperty() {
        return calcium.getReadOnlyProperty();
    }

    @Override
    public BigDecimal getMagnesium() {
        return magnesium.get();
    }
    @Override
    public ReadOnlyObjectProperty<BigDecimal> magnesiumProperty() {
        return magnesium.getReadOnlyProperty();
    }

    @Override
    public BigDecimal getPhosphorus() {
        return phosphorus.get();
    }
    @Override
    public ReadOnlyObjectProperty<BigDecimal> phosphorusProperty() {
        return phosphorus.getReadOnlyProperty();
    }

    @Override
    public BigDecimal getPotassium() {
        return potassium.get();
    }
    @Override
    public ReadOnlyObjectProperty<BigDecimal> potassiumProperty() {
        return potassium.getReadOnlyProperty();
    }

    @Override
    public BigDecimal getSodium() {
        return sodium.get();
    }
    @Override
    public ReadOnlyObjectProperty<BigDecimal> sodiumProperty() {
        return sodium.getReadOnlyProperty();
    }

    @Override
    public BigDecimal getSulfur() {
        return sulfur.get();
    }
    @Override
    public ReadOnlyObjectProperty<BigDecimal> sulfurProperty() {
        return sulfur.getReadOnlyProperty();
    }

    @Override
    public BigDecimal getVitaminA() {
        return vitaminA.get();
    }
    @Override
    public ReadOnlyObjectProperty<BigDecimal> vitaminAProperty() {
        return vitaminA.getReadOnlyProperty();
    }

    @Override
    public BigDecimal getVitaminC() {
        return vitaminC.get();
    }
    @Override
    public ReadOnlyObjectProperty<BigDecimal> vitaminCProperty() {
        return vitaminC.getReadOnlyProperty();
    }

    @Override
    public BigDecimal getVitaminK() {
        return vitaminK.get();
    }
    @Override
    public ReadOnlyObjectProperty<BigDecimal> vitaminKProperty() {
        return vitaminK.getReadOnlyProperty();
    }

    @Override
    public BigDecimal getVitaminD() {
        return vitaminD.get();
    }
    @Override
    public ReadOnlyObjectProperty<BigDecimal> vitaminDProperty() {
        return vitaminD.getReadOnlyProperty();
    }

    @Override
    public BigDecimal getVitaminE() {
        return vitaminE.get();
    }
    @Override
    public ReadOnlyObjectProperty<BigDecimal> vitaminEProperty() {
        return vitaminE.getReadOnlyProperty();
    }

    @Override
    public BigDecimal getVitaminB1() {
        return vitaminB1.get();
    }
    @Override
    public ReadOnlyObjectProperty<BigDecimal> vitaminB1Property() {
        return vitaminB1.getReadOnlyProperty();
    }

    @Override
    public BigDecimal getVitaminB2() {
        return vitaminB2.get();
    }
    @Override
    public ReadOnlyObjectProperty<BigDecimal> vitaminB2Property() {
        return vitaminB2.getReadOnlyProperty();
    }

    @Override
    public BigDecimal getVitaminB3() {
        return vitaminB3.get();
    }
    @Override
    public ReadOnlyObjectProperty<BigDecimal> vitaminB3Property() {
        return vitaminB3.getReadOnlyProperty();
    }

    @Override
    public BigDecimal getVitaminB5() {
        return vitaminB5.get();
    }
    @Override
    public ReadOnlyObjectProperty<BigDecimal> vitaminB5Property() {
        return vitaminB5.getReadOnlyProperty();
    }

    @Override
    public BigDecimal getVitaminB6() {
        return vitaminB6.get();
    }
    @Override
    public ReadOnlyObjectProperty<BigDecimal> vitaminB6Property() {
        return vitaminB6.getReadOnlyProperty();
    }

    @Override
    public BigDecimal getVitaminB7() {
        return vitaminB7.get();
    }
    @Override
    public ReadOnlyObjectProperty<BigDecimal> vitaminB7Property() {
        return vitaminB7.getReadOnlyProperty();
    }

    @Override
    public BigDecimal getVitaminB8() {
        return vitaminB8.get();
    }
    @Override
    public ReadOnlyObjectProperty<BigDecimal> vitaminB8Property() {
        return vitaminB8.getReadOnlyProperty();
    }

    @Override
    public BigDecimal getVitaminB9() {
        return vitaminB9.get();
    }
    @Override
    public ReadOnlyObjectProperty<BigDecimal> vitaminB9Property() {
        return vitaminB9.getReadOnlyProperty();
    }

    @Override
    public BigDecimal getVitaminB12() {
        return vitaminB12.get();
    }
    @Override
    public ReadOnlyObjectProperty<BigDecimal> vitaminB12Property() {
        return vitaminB12.getReadOnlyProperty();
    }
}
