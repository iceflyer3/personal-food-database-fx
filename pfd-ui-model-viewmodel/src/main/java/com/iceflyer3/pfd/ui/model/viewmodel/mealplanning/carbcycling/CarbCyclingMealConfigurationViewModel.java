/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling;

import com.iceflyer3.pfd.enums.CarbCyclingMealRelation;
import com.iceflyer3.pfd.ui.model.api.mealplanning.carbcycling.CarbCyclingMealConfigurationModel;
import com.iceflyer3.pfd.validation.NumericRangeDatumRestriction;
import com.iceflyer3.pfd.validation.ObservableValuePropertyRestrictionBinding;
import com.iceflyer3.pfd.validation.PropertyRestrictionBinding;
import com.iceflyer3.pfd.validation.ValidationResult;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;

import java.math.BigDecimal;
import java.util.*;

public class CarbCyclingMealConfigurationViewModel implements CarbCyclingMealConfigurationModel
{
    private final UUID mealConfigId;
    private final boolean isForTrainingDay;

    private final IntegerProperty postWorkoutMealOffset;
    private final ObjectProperty<BigDecimal> carbConsumptionMaximumPercent;
    private final ObjectProperty<BigDecimal> carbConsumptionMinimumPercent;
    private final ObjectProperty<CarbCyclingMealRelation> postWorkoutMealRelation;

    /*
     * Maps the HashCode of a property to any restrictions that should be enforced
     * to ensure correct and valid values for that property.
     *
     * Collectively this map contains all the invariants that have to do with an individual
     * property. But not necessarily all invariants of the class.
     */
    private final Map<Integer, PropertyRestrictionBinding> propertyInvariants;

    public CarbCyclingMealConfigurationViewModel(
            int postWorkoutMealOffset,
            CarbCyclingMealRelation postWorkoutMealRelation,
            boolean isForTrainingDay)
    {
        this(
                null,
                postWorkoutMealOffset,
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                postWorkoutMealRelation,
                isForTrainingDay
        );
    }

    public CarbCyclingMealConfigurationViewModel(
            UUID mealConfigId,
            int postWorkoutMealOffset,
            BigDecimal carbConsumptionMaximumPercent,
            BigDecimal carbConsumptionMinimumPercent,
            CarbCyclingMealRelation postWorkoutMealRelation,
            boolean isForTrainingDay)
    {
        this.mealConfigId = mealConfigId;
        this.isForTrainingDay = isForTrainingDay;

        this.postWorkoutMealOffset = new SimpleIntegerProperty(postWorkoutMealOffset);
        this.carbConsumptionMaximumPercent = new SimpleObjectProperty<>(carbConsumptionMaximumPercent);
        this.carbConsumptionMinimumPercent = new SimpleObjectProperty<>(carbConsumptionMinimumPercent);
        this.postWorkoutMealRelation = new SimpleObjectProperty<>(postWorkoutMealRelation);

        /*
         * Setup property invariant bindings
         *  - Minimum / maximum carb consumption percentages must be between 0% and 99%
         */
        ObservableValuePropertyRestrictionBinding<BigDecimal> minCarbPercentPropertyBinding = new ObservableValuePropertyRestrictionBinding<>(this.carbConsumptionMinimumPercent);
        minCarbPercentPropertyBinding.bindRestriction(new NumericRangeDatumRestriction(0, 99));

        ObservableValuePropertyRestrictionBinding<BigDecimal> maxCarbPercentPropertyBinding = new ObservableValuePropertyRestrictionBinding<>(this.carbConsumptionMaximumPercent);
        maxCarbPercentPropertyBinding.bindRestriction(new NumericRangeDatumRestriction(0, 99));

        this.propertyInvariants = new HashMap<>();
        this.propertyInvariants.put(this.carbConsumptionMinimumPercent.hashCode(), minCarbPercentPropertyBinding);
        this.propertyInvariants.put(this.carbConsumptionMaximumPercent.hashCode(), maxCarbPercentPropertyBinding);
    }

    @Override
    public UUID getId()
    {
        return mealConfigId;
    }

    public boolean isForTrainingDay()
    {
        return isForTrainingDay;
    }

    public int getPostWorkoutMealOffset()
    {
        return postWorkoutMealOffset.get();
    }

    public IntegerProperty postWorkoutMealOffsetProperty()
    {
        return postWorkoutMealOffset;
    }

    public void setPostWorkoutMealOffset(int postWorkoutMealOffset)
    {
        this.postWorkoutMealOffset.set(postWorkoutMealOffset);
    }

    public BigDecimal getCarbConsumptionMaximumPercent()
    {
        return carbConsumptionMaximumPercent.get();
    }

    public ObjectProperty<BigDecimal> carbConsumptionMaximumPercentProperty()
    {
        return carbConsumptionMaximumPercent;
    }

    public void setCarbConsumptionMaximumPercent(BigDecimal carbConsumptionMaximumPercent)
    {
        this.carbConsumptionMaximumPercent.set(carbConsumptionMaximumPercent);
    }

    public BigDecimal getCarbConsumptionMinimumPercent()
    {
        return carbConsumptionMinimumPercent.get();
    }

    public ObjectProperty<BigDecimal> carbConsumptionMinimumPercentProperty()
    {
        return carbConsumptionMinimumPercent;
    }

    public void setCarbConsumptionMinimumPercent(BigDecimal carbConsumptionMinimumPercent)
    {
        this.carbConsumptionMinimumPercent.set(carbConsumptionMinimumPercent);
    }

    public CarbCyclingMealRelation getPostWorkoutMealRelation()
    {
        return postWorkoutMealRelation.get();
    }

    public ObjectProperty<CarbCyclingMealRelation> postWorkoutMealRelationProperty()
    {
        return postWorkoutMealRelation;
    }

    public void setPostWorkoutMealRelation(CarbCyclingMealRelation postWorkoutMealRelation)
    {
        this.postWorkoutMealRelation.set(postWorkoutMealRelation);
    }

    @Override
    public ValidationResult validate()
    {
        Set<ValidationResult> propertyInvariantResults = new HashSet<>();
        for(Map.Entry<Integer, PropertyRestrictionBinding> entry : propertyInvariants.entrySet())
        {
            propertyInvariantResults.add(entry.getValue().applyRestrictions());
        }
        ValidationResult result = ValidationResult.flatten(propertyInvariantResults);


        // Properties can be null when bidirectionally bound to GUI controls
        if ((carbConsumptionMinimumPercent.get() != null && carbConsumptionMaximumPercent.get() != null))
        {
            // If either minimum or maximum carb consumption percentage are non-zero then enforce that the minimum percentage must be less than the maximum percentage
            result = result.and(() -> carbConsumptionMinimumPercent.get().compareTo(carbConsumptionMaximumPercent.get()) <= 0, "Carb consumption minimum percentage must be less than or equal to the maximum percentage");
        }

        return result;
    }

    @Override
    public ValidationResult validateProperty(int propertyHashcode)
    {
        return propertyInvariants.get(propertyHashcode).applyRestrictions();
    }

    @Override
    public String toString()
    {
        return "CarbCyclingMealConfigurationViewModel{" + "mealConfigId=" + mealConfigId + ", postWorkoutMealOffset=" + postWorkoutMealOffset + ", carbConsumptionMaximumPercent=" + carbConsumptionMaximumPercent + ", carbConsumptionMinimumPercent=" + carbConsumptionMinimumPercent + ", postWorkoutMealRelation=" + postWorkoutMealRelation + '}';
    }
}
