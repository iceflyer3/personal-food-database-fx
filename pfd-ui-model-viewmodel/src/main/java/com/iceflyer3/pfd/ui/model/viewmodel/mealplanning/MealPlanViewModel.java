package com.iceflyer3.pfd.ui.model.viewmodel.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.enums.*;
import com.iceflyer3.pfd.ui.model.api.mealplanning.MealPlanModel;
import com.iceflyer3.pfd.ui.model.api.mealplanning.MealPlanNutrientSuggestionModel;
import javafx.beans.property.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MealPlanViewModel implements MealPlanModel
{
    private final UUID planId;

    private final IntegerProperty age;
    private final ObjectProperty<BigDecimal> height;
    private final ObjectProperty<BigDecimal> weight;
    private final ObjectProperty<Sex> sex;
    private final ObjectProperty<BigDecimal> bodyFatPercentage;
    private final ObjectProperty<ActivityLevel> activityLevel;
    private final ObjectProperty<FitnessGoal> fitnessGoal;
    private final ObjectProperty<BasalMetabolicRateFormula> bmrFormula;
    private final ObjectProperty<BigDecimal> dailyCalories;
    private final ObjectProperty<BigDecimal> tdeeCalories;
    private final BooleanProperty isActive;
    private final List<MealPlanNutrientSuggestionModel> nutrientSuggestions;

    public MealPlanViewModel(UUID planId) {
        this.planId = planId;
        this.age = new SimpleIntegerProperty();
        this.height = new SimpleObjectProperty<>();
        this.weight = new SimpleObjectProperty<>();
        this.sex = new SimpleObjectProperty<>();
        this.bodyFatPercentage = new SimpleObjectProperty<>();
        this.activityLevel = new SimpleObjectProperty<>();
        this.fitnessGoal = new SimpleObjectProperty<>();
        this.bmrFormula = new SimpleObjectProperty<>();
        this.dailyCalories = new SimpleObjectProperty<>();
        this.tdeeCalories = new SimpleObjectProperty<>();
        this.isActive = new SimpleBooleanProperty(false);

        this.nutrientSuggestions = new ArrayList<>();
    }

    @Override
    public UUID getId() {
        return planId;
    }

    @Override
    public int getAge() {
        return age.get();
    }

    @Override
    public IntegerProperty ageProperty() {
        return age;
    }

    @Override
    public void setAge(int age) {
        this.age.set(age);
    }

    @Override
    public BigDecimal getHeight() {
        return height.get();
    }

    @Override
    public ObjectProperty<BigDecimal> heightProperty() {
        return height;
    }

    @Override
    public void setHeight(BigDecimal height) {
        this.height.set(height);
    }

    @Override
    public BigDecimal getWeight() {
        return weight.get();
    }

    @Override
    public ObjectProperty<BigDecimal> weightProperty() {
        return weight;
    }

    @Override
    public void setWeight(BigDecimal weight) {
        this.weight.set(weight);
    }

    @Override
    public Sex getSex() {
        return sex.get();
    }

    @Override
    public ObjectProperty<Sex> sexProperty() {
        return sex;
    }

    @Override
    public void setSex(Sex sex) {
        this.sex.set(sex);
    }

    @Override
    public BigDecimal getBodyFatPercentage() {
        return bodyFatPercentage.get();
    }

    @Override
    public ObjectProperty<BigDecimal> bodyFatPercentageProperty() {
        return bodyFatPercentage;
    }

    @Override
    public void setBodyFatPercentage(BigDecimal bodyFatPercentage) {
        this.bodyFatPercentage.set(bodyFatPercentage);
    }

    @Override
    public ActivityLevel getActivityLevel() {
        return activityLevel.get();
    }

    @Override
    public ObjectProperty<ActivityLevel> activityLevelProperty() {
        return activityLevel;
    }

    @Override
    public void setActivityLevel(ActivityLevel activityLevel) {
        this.activityLevel.set(activityLevel);
    }

    @Override
    public FitnessGoal getFitnessGoal() {
        return fitnessGoal.get();
    }

    @Override
    public ObjectProperty<FitnessGoal> fitnessGoalProperty() {
        return fitnessGoal;
    }

    @Override
    public void setFitnessGoal(FitnessGoal fitnessGoal) {
        this.fitnessGoal.set(fitnessGoal);
    }

    @Override
    public BasalMetabolicRateFormula getBmrFormula() {
        return bmrFormula.get();
    }

    @Override
    public ObjectProperty<BasalMetabolicRateFormula> bmrFormulaProperty() {
        return bmrFormula;
    }

    @Override
    public void setBmrFormula(BasalMetabolicRateFormula bmrFormula) {
        this.bmrFormula.set(bmrFormula);
    }

    @Override
    public BigDecimal getDailyCalories() {
        return dailyCalories.get();
    }

    @Override
    public ObjectProperty<BigDecimal> dailyCaloriesProperty() {
        return dailyCalories;
    }

    public void setDailyCalories(BigDecimal dailyCalories) {
        this.dailyCalories.set(dailyCalories);
    }

    @Override
    public BigDecimal getTdeeCalories()
    {
        return tdeeCalories.get();
    }

    @Override
    public ObjectProperty<BigDecimal> tdeeCaloriesProperty()
    {
        return tdeeCalories;
    }

    @Override
    public BigDecimal getNutrientSuggestedAmount(Nutrient nutrient)
    {
        return nutrientSuggestions.stream().filter(nutrientSuggestion -> nutrientSuggestion.getNutrient() == nutrient).map(MealPlanNutrientSuggestionModel::getSuggestedAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    @Override
    public List<MealPlanNutrientSuggestionModel> getCustomNutrients()
    {
        List<Nutrient> macronutrients = List.of(Nutrient.macronutrients());
        return nutrientSuggestions.stream().filter(nutrientSuggestion -> !macronutrients.contains(nutrientSuggestion.getNutrient())).toList();
    }

    public void setTdeeCalories(BigDecimal tdeeCalories)
    {
        this.tdeeCalories.set(tdeeCalories);
    }

    @Override
    public boolean isActive() {
        return isActive.get();
    }

    @Override
    public BooleanProperty isActiveProperty() {
        return isActive;
    }

    @Override
    public void toggleActiveStatus() {
        this.isActive.set(!this.isActive.get());
    }

    public List<MealPlanNutrientSuggestionModel> getNutrientSuggestions() {
        return nutrientSuggestions;
    }

    @Override
    public String toString()
    {
        return "MealPlanViewModel{"
                + "planId=" + planId +
                ", age=" + age +
                ", height=" + height +
                ", weight=" + weight +
                ", sex=" + sex +
                ", bodyFatPercentage=" + bodyFatPercentage +
                ", activityLevel=" + activityLevel +
                ", fitnessGoal=" + fitnessGoal +
                ", bmrFormula=" + bmrFormula +
                ", dailyCalories=" + dailyCalories +
                ", tdeeCalories=" + tdeeCalories +
                ", isActive=" + isActive +
                ", nutrientSuggestions=" + nutrientSuggestions + '}';
    }
}
