/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.model.viewmodel;

import com.iceflyer3.pfd.ui.model.api.viewmodel.ValidatingViewModel;
import com.iceflyer3.pfd.validation.MinimumLengthDatumRestriction;
import com.iceflyer3.pfd.validation.ObservableValuePropertyRestrictionBinding;
import com.iceflyer3.pfd.validation.ValidationResult;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.UUID;

public class UserViewModel implements ValidatingViewModel
{
    private final StringProperty username;
    private final UUID userId;

    private final ObservableValuePropertyRestrictionBinding<String> usernamePropertyRestriction;

    public UserViewModel(UUID userId, String username)
    {
        this.userId = userId;
        this.username = new SimpleStringProperty(username);

        /* Establish property invariants
         *  - The name of the user
         */
        usernamePropertyRestriction = new ObservableValuePropertyRestrictionBinding<>(this.username);
        usernamePropertyRestriction.bindRestriction(new MinimumLengthDatumRestriction(1));
    }

    public UUID getUserId() {
        return userId;
    }

    public String getUsername()
    {
        return username.get();
    }

    public StringProperty usernameProperty()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username.set(username);
    }

    @Override
    public ValidationResult validate()
    {
        return usernamePropertyRestriction.applyRestrictions();
    }

    @Override
    public ValidationResult validateProperty(int propertyHashcode)
    {
        return usernamePropertyRestriction.applyRestrictions();
    }

    @Override
    public String toString() {
        return "UserViewModel{" +
                "username='" + username + '\'' +
                ", userId=" + userId +
                '}';
    }
}
