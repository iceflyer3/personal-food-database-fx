package com.iceflyer3.pfd.ui.model.viewmodel.recipe;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.enums.UnitOfMeasure;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.api.ServingSizeModel;
import com.iceflyer3.pfd.ui.model.api.food.ReadOnlyFoodModel;
import com.iceflyer3.pfd.ui.model.api.recipe.RecipeStepModel;
import com.iceflyer3.pfd.ui.model.api.viewmodel.IngredientLazyLoadingViewModel;
import com.iceflyer3.pfd.ui.model.api.viewmodel.StepLazyLoadingViewModel;
import com.iceflyer3.pfd.ui.model.api.recipe.RecipeModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.base.AbstractSummaryBasedNutritionFactsReader;
import com.iceflyer3.pfd.ui.model.viewmodel.IngredientViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.ServingSizeViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.ReadOnlyFoodViewModel;
import com.iceflyer3.pfd.util.NumberUtils;
import com.iceflyer3.pfd.validation.*;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;

public class RecipeViewModel extends AbstractSummaryBasedNutritionFactsReader implements RecipeModel, IngredientLazyLoadingViewModel, StepLazyLoadingViewModel
{
    private final UUID recipeId;

    private final StringProperty name;
    private final StringProperty source;
    private final ObjectProperty<BigDecimal> numberOfServingsMade;
    private final StringProperty createdUsername;
    private final ObjectProperty<LocalDateTime> createdDate;
    private final StringProperty lastModifiedUsername;
    private final ObjectProperty<LocalDateTime> lastModifiedDate;

    private final ObservableList<IngredientModel> ingredients;
    private final ObservableList<ServingSizeModel> servingSizes;
    private final ListProperty<RecipeStepModel> steps;

    /*
     * Maps the HashCode of a property to any restrictions that should be enforced
     * to ensure correct and valid values for that property.
     *
     * Collectively this map contains all the invariants that have to do with an individual
     * property. But not necessarily all invariants of the class.
     */
    private final Map<Integer, PropertyRestrictionBinding> propertyInvariants;

    /**
     * Instantiates a new RecipeViewModel with the provided username used for both
     * the created and last modified user, created / last modified date set to LocalDateTime.now(),
     * and an empty nutritional summary.
     *
     * Convenience function for creating a new recipe for which the details do not yet exist.
     * @param creatorUsername The username of the user creating the new complex food
     */
    public RecipeViewModel(String creatorUsername)
    {
        this(
                null,
                "",
                "",
                NumberUtils.withStandardPrecision(1),
                creatorUsername,
                LocalDateTime.now(),
                creatorUsername,
                LocalDateTime.now(),
                new ArrayList<>(),
                NutritionalSummary.empty()
        );
    }

    /**
     * Instantiate a new recipe that has been loaded from the db and the details are known.
     * @param recipeId Id of the food in the database
     * @param name Name of the food
     * @param createdUsername Username of the user who created the food
     * @param createdDate Date the food was created
     * @param lastModifiedUsername Username of the last user to modify the food
     * @param lastModifiedDate Date the food was last updated
     * @param servingSizes Serving sizes list
     * @param nutritionalSummary Nutritional grand totals of the food
     */
    public RecipeViewModel(UUID recipeId,
                           String name,
                           String source,
                           BigDecimal numberOfServingsMade,
                           String createdUsername,
                           LocalDateTime createdDate,
                           String lastModifiedUsername,
                           LocalDateTime lastModifiedDate,
                           Collection<ServingSizeViewModel> servingSizes,
                           NutritionalSummary nutritionalSummary)
    {
        super();
        this.recipeId = recipeId;
        this.name = new SimpleStringProperty(name);
        this.source = new SimpleStringProperty(source);
        this.numberOfServingsMade = new SimpleObjectProperty<>(numberOfServingsMade);
        this.createdUsername = new SimpleStringProperty(createdUsername);
        this.createdDate = new SimpleObjectProperty<>(createdDate);
        this.lastModifiedUsername = new SimpleStringProperty(lastModifiedUsername);
        this.lastModifiedDate = new SimpleObjectProperty<>(lastModifiedDate);
        this.servingSizes = FXCollections.observableArrayList(servingSizes);
        this.nutritionalSummary = nutritionalSummary;

        // Ingredients and steps aren't required because we may not always want them to be loaded
        this.ingredients = FXCollections.observableArrayList();
        this.steps = new SimpleListProperty<>(FXCollections.observableArrayList());

        // Populate initial values for the JavaFx properties
        this.refreshNutritionalProperties();

        /*
         * Establish property invariants:
         *  - Recipe Name
         *  - Recipe Source
         *  - Number of servings the recipe will make
         *  - At least one ingredient
         *  - At least one serving size
         *  - At least one step
         */
        ObservableValuePropertyRestrictionBinding<String> namePropertyRestriction = new ObservableValuePropertyRestrictionBinding<>(this.name);
        namePropertyRestriction.bindRestriction(new MinimumLengthDatumRestriction(1));

        ObservableValuePropertyRestrictionBinding<String> sourcePropertyRestriction = new ObservableValuePropertyRestrictionBinding<>(this.source);
        sourcePropertyRestriction.bindRestriction(new MinimumLengthDatumRestriction(1));

        ObservableValuePropertyRestrictionBinding<BigDecimal> numberOfServingsPropertyRestriction = new ObservableValuePropertyRestrictionBinding<>(this.numberOfServingsMade);
        numberOfServingsPropertyRestriction.bindRestriction(new NonZeroDatumRestriction());

        this.propertyInvariants = new HashMap<>();
        this.propertyInvariants.put(this.name.hashCode(), namePropertyRestriction);
        this.propertyInvariants.put(this.source.hashCode(), sourcePropertyRestriction);
        this.propertyInvariants.put(this.numberOfServingsMade.hashCode(), numberOfServingsPropertyRestriction);
    }

    @Override
    public ValidationResult validate() {
        Set<ValidationResult> propertyInvariantResults = new HashSet<>();
        for(Map.Entry<Integer, PropertyRestrictionBinding> entry : propertyInvariants.entrySet())
        {
            propertyInvariantResults.add(entry.getValue().applyRestrictions());
        }
        ValidationResult result = ValidationResult.flatten(propertyInvariantResults);

        // See the note in the validation function of ComplexFoodViewModel for an explanation of why this approach
        // is taken for collections.
        return result
                .and(() -> this.ingredients.size() > 0, "Recipe must have at least one ingredients")
                .and(() -> this.servingSizes.size() > 0, "Recipe must have at least one serving size")
                .and(() -> this.steps.size() > 0, "Recipe must have at least one step");
    }

    @Override
    public ValidationResult validateProperty(int propertyHashcode)
    {
        return propertyInvariants.get(propertyHashcode).applyRestrictions();
    }

    @Override
    public UUID getId() {
        return recipeId;
    }

    public NutritionalSummary getNutritionalSummary()
    {
        return nutritionalSummary;
    }

    @Override
    public String getName() {
        return name.get();
    }

    @Override
    public StringProperty nameProperty() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name.set(name);
    }

    @Override
    public String getSource() {
        return source.get();
    }

    @Override
    public StringProperty sourceProperty() {
        return source;
    }

    @Override
    public void setSource(String source) {
        this.source.set(source);
    }

    @Override
    public BigDecimal getNumberOfServingsMade() {
        return numberOfServingsMade.get();
    }

    @Override
    public ObjectProperty<BigDecimal> numberOfServingsMadeProperty() {
        return numberOfServingsMade;
    }

    @Override
    public void setNumberOfServingsMade(BigDecimal numberOfServingsMade) {
        this.numberOfServingsMade.set(numberOfServingsMade);
    }

    @Override
    public String getCreatedUser() {
        return createdUsername.get();
    }

    @Override
    public StringProperty createdUserProperty() {
        return createdUsername;
    }

    @Override
    public LocalDateTime getCreatedDate() {
        return createdDate.get();
    }

    @Override
    public ObjectProperty<LocalDateTime> createdDateProperty() {
        return createdDate;
    }

    @Override
    public String getLastModifiedUser() {
        return lastModifiedUsername.get();
    }

    @Override
    public StringProperty lastModifiedUserProperty() {
        return lastModifiedUsername;
    }

    @Override
    public void setLastModifiedUser(String lastModifiedUserName) {
        this.lastModifiedUsername.set(lastModifiedUserName);
    }

    @Override
    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate.get();
    }

    @Override
    public ObjectProperty<LocalDateTime> lastModifiedDateProperty() {
        return lastModifiedDate;
    }

    @Override
    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate.set(lastModifiedDate);
    }

    @Override
    public ObservableList<IngredientModel> getIngredients() {
        return FXCollections.unmodifiableObservableList(ingredients);
    }

    public void setIngredients(Collection<IngredientModel> ingredients)
    {
        this.ingredients.clear();
        this.ingredients.addAll(ingredients);
    }

    /**
     * Add a new ingredient to the list of ingredients. The provided ingredient should be of the view model
     * implementation type which is ReadOnlyFoodViewModel.
     *
     * @param ingredient The ReadOnlyFoodViewModel instance that represents the ingredient food
     */
    @Override
    public void addIngredient(ReadOnlyFoodModel ingredient)
    {
        if (ingredient instanceof ReadOnlyFoodViewModel)
        {
            this.ingredients.add(new IngredientViewModel(ingredient));
        }
        else
        {
            throw new IllegalArgumentException("ingredientFood must be an instance of ReadOnlyFoodViewModel");
        }
    }

    @Override
    public void removeIngredient(IngredientModel ingredient)
    {
        // Remove the ingredient from the master list
        ingredients.remove(ingredient);

        // Also remove the ingredient from any steps that used it
        steps.forEach(step -> step.getIngredients().remove(ingredient));

    }

    @Override
    public ObservableList<ServingSizeModel> getServingSizes() {
        return FXCollections.unmodifiableObservableList(servingSizes);
    }


    @Override
    public void addServingSize(UnitOfMeasure uom, BigDecimal quantity)
    {
        servingSizes.add(new ServingSizeViewModel(uom, quantity));
    }

    @Override
    public void removeServingSize(ServingSizeModel servingSize)
    {
        servingSizes.remove(servingSize);
    }

    @Override
    public ObservableList<RecipeStepModel> getSteps() {
        return steps.get();
    }

    public void setSteps(Collection<RecipeStepModel> steps)
    {
        this.steps.clear();
        this.steps.addAll(steps);
    }

    @Override
    public ListProperty<RecipeStepModel> stepsProperty() {
        return steps;
    }

    @Override
    public void refreshNutritionDetails()
    {
        /*
         * Ingredients are only loaded when we actually need to interact with them. Edit, add / remove,
         * display, etc..
         *
         * If they're loaded then they should be refreshed to guarantee accurate information for the
         * recipe. But if they aren't then just assume that the summary data loaded from the appropriate
         * DB view is still correct and refresh the UI JavaFx properties with that data.
         */

        if (ingredients.size() > 0)
        {
            ingredients.forEach(ingredient -> ingredient.refreshNutritionDetails());
            nutritionalSummary = NutritionalSummary.deriveUpdateFrom(ingredients);
        }

        this.refreshNutritionalProperties();
    }

    @Override
    public String toString() {
        return "RecipeViewModel{" +
                "recipeId=" + recipeId +
                ", name=" + name +
                ", source=" + source +
                ", numberOfServingsMade=" + numberOfServingsMade +
                ", createdUsername=" + createdUsername +
                ", createdDate=" + createdDate +
                ", lastModifiedUsername=" + lastModifiedUsername +
                ", lastModifiedDate=" + lastModifiedDate +
                ", ingredients=" + ingredients +
                ", servingSizes=" + servingSizes +
                '}';
    }
}
