package com.iceflyer3.pfd.ui.model.viewmodel.food;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.enums.UnitOfMeasure;
import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.api.ServingSizeModel;
import com.iceflyer3.pfd.ui.model.api.food.ReadOnlyFoodModel;
import com.iceflyer3.pfd.ui.model.api.viewmodel.IngredientLazyLoadingViewModel;
import com.iceflyer3.pfd.ui.model.api.food.ComplexFoodModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.base.AbstractSummaryBasedNutritionFactsReader;
import com.iceflyer3.pfd.ui.model.viewmodel.IngredientViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.ServingSizeViewModel;

import com.iceflyer3.pfd.util.NumberUtils;
import com.iceflyer3.pfd.validation.*;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.math.BigDecimal;
import java.util.*;

import java.time.LocalDateTime;

public class ComplexFoodViewModel extends AbstractSummaryBasedNutritionFactsReader implements ComplexFoodModel, IngredientLazyLoadingViewModel
{
    private final UUID foodId;
    private final StringProperty name;
    private final StringProperty createdUser;
    private final ObjectProperty<LocalDateTime> createdDate;
    private final StringProperty lastModifiedUser;
    private final ObjectProperty<LocalDateTime> lastModifiedDate;
    private final ObservableList<IngredientModel> ingredients;
    private final ObservableList<ServingSizeModel> servingSizes;

    /*
     * Maps the HashCode of a property to any restrictions that should be enforced
     * to ensure correct and valid values for that property.
     *
     * Collectively this map contains all the invariants that have to do with an individual
     * property. But not necessarily all invariants of the class.
     */
    private final Map<Integer, PropertyRestrictionBinding> propertyInvariants;

    /**
     * Instantiates a new ComplexFoodViewModel with the provided username used for both
     * the created and last modified user, created / last modified date set to LocalDateTime.now(),
     * and an empty nutritional summary.
     *
     * Convenience function for creating a new complex food for which the details do not yet exist.
     * @param creatorUserName The username of the user creating the new complex food
     */
    public ComplexFoodViewModel(String creatorUserName) {
        this(
                null,
                "",
                creatorUserName,
                LocalDateTime.now(),
                creatorUserName,
                LocalDateTime.now(),
                new ArrayList<>(),
                NutritionalSummary.empty());
    }

    /**
     * Instantiate a new complex food
     * @param foodId Id of the food in the database
     * @param foodName Name of the food
     * @param creatorUserName Username of the user who created the food
     * @param createdDate Date the food was created
     * @param lastModifiedUserName Username of the last user to modify the food
     * @param lastModifiedDate Date the food was last updated
     * @param servingSizes Serving sizes list
     * @param nutritionalSummary Nutritional grand totals of the food
     */
    public ComplexFoodViewModel(UUID foodId,
                                String foodName,
                                String creatorUserName,
                                LocalDateTime createdDate,
                                String lastModifiedUserName,
                                LocalDateTime lastModifiedDate,
                                Collection<ServingSizeModel> servingSizes,
                                NutritionalSummary nutritionalSummary)
    {
        super();
        this.foodId = foodId;
        this.nutritionalSummary = nutritionalSummary;
        this.name = new SimpleStringProperty(foodName);
        this.createdUser = new SimpleStringProperty(creatorUserName);
        this.createdDate = new SimpleObjectProperty<>(createdDate);
        this.lastModifiedUser = new SimpleStringProperty(lastModifiedUserName);
        this.lastModifiedDate = new SimpleObjectProperty<>(lastModifiedDate);
        this.ingredients = new ReadOnlyListWrapper<>(FXCollections.observableArrayList());
        this.servingSizes = new ReadOnlyListWrapper<>(FXCollections.observableArrayList(servingSizes));


        /*
         * Establish property invariants
         *  - Name of at least one character
         *  - At least one serving size
         *  - At least two ingredients
         */
        ObservableValuePropertyRestrictionBinding<String> namePropertyBinding = new ObservableValuePropertyRestrictionBinding<>(this.name);
        namePropertyBinding.bindRestriction(new MinimumLengthDatumRestriction(1));

        this.propertyInvariants = new HashMap<>();
        propertyInvariants.put(this.name.hashCode(), namePropertyBinding);

        // Populate initial values for the JavaFx properties
        this.refreshNutritionalProperties();
    }

    @Override
    public ValidationResult validate() {
        Set<ValidationResult> propertyInvariantResults = new HashSet<>();
        for(Map.Entry<Integer, PropertyRestrictionBinding> entry : propertyInvariants.entrySet())
        {
            propertyInvariantResults.add(entry.getValue().applyRestrictions());
        }

        /*
         * There isn't any built in way that I'm aware of to be able to uniquely identify a collection.
         * In most situations the hashcode of an object would suffice for this but, as stated by the
         * JavaFx docs, the hashcode of a list is a function of all its elements. Which means the hashcode
         * changes every time an element is added to or removed from the list. Thus, it will not work for our
         * purposes of storing restrictions at construction time and looking them up later.
         */
        ValidationResult result = ValidationResult.flatten(propertyInvariantResults);
        return result
                .and(() -> this.ingredients.size() >= 2, "Complex food must have at least two ingredients")
                .and(() -> this.servingSizes.size() > 0, "Complex food must have at least one serving size");
    }

    @Override
    public ValidationResult validateProperty(int propertyHashcode)
    {
        return propertyInvariants.get(propertyHashcode).applyRestrictions();
    }

    @Override
    public void refreshNutritionDetails()
    {
        /*
         * Ingredients are only loaded when we actually need to interact with them. Edit, add / remove,
         * display, etc..
         *
         * If they're loaded then they should be refreshed to guarantee accurate information for the
         * food. But if they aren't then just assume that the summary data loaded from the appropriate
         * DB view is still correct and refresh the UI JavaFx properties with that data.
         */
        if (ingredients.size() > 0)
        {
            ingredients.forEach(ingredient -> ingredient.refreshNutritionDetails());
            this.nutritionalSummary = NutritionalSummary.deriveUpdateFrom(ingredients);
        }

        this.refreshNutritionalProperties();
    }

    @Override
    public UUID getId() {
        return foodId;
    }

    @Override
    public String getName() {
        return name.get();
    }

    @Override
    public StringProperty nameProperty() {
        return name;
    }

    @Override
    public void setName(String foodName) {
        this.name.set(foodName);
    }

    @Override
    public String getCreatedUser() {
        return createdUser.get();
    }

    @Override
    public StringProperty createdUserProperty() {
        return createdUser;
    }

    @Override
    public LocalDateTime getCreatedDate() {
        return createdDate.get();
    }

    @Override
    public ObjectProperty<LocalDateTime> createdDateProperty() {
        return createdDate;
    }

    @Override
    public String getLastModifiedUser() {
        return lastModifiedUser.get();
    }

    @Override
    public StringProperty lastModifiedUserProperty() {
        return lastModifiedUser;
    }

    @Override
    public void setLastModifiedUser(String lastModifiedUserName) {
        this.lastModifiedUser.set(lastModifiedUserName);
    }

    @Override
    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate.get();
    }

    @Override
    public ObjectProperty<LocalDateTime> lastModifiedDateProperty() {
        return lastModifiedDate;
    }

    @Override
    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate.set(lastModifiedDate);
    }

    @Override
    public ObservableList<IngredientModel> getIngredients() {
        return FXCollections.unmodifiableObservableList(ingredients);
    }

    public void setIngredients(Collection<IngredientModel> ingredients)
    {
        this.ingredients.clear();
        this.ingredients.addAll(ingredients);
    }

    /**
     * Add a new ingredient to the list of ingredients. The provided ingredient should be of the view model
     * implementation type which is ReadOnlyFoodViewModel.
     *
     * @param ingredient The ReadOnlyFoodViewModel instance that represents the ingredient food
     */
    @Override
    public void addIngredient(ReadOnlyFoodModel ingredient)
    {
        if (ingredient instanceof ReadOnlyFoodViewModel)
        {
            this.ingredients.add(new IngredientViewModel(ingredient));
        }
        else
        {
            throw new IllegalArgumentException("ingredientFood must be an instance of ReadOnlyFoodViewModel");
        }
    }

    @Override
    public void removeIngredient(IngredientModel ingredient)
    {
        this.ingredients.remove(ingredient);
    }

    @Override
    public ObservableList<ServingSizeModel> getServingSizes() {
        return FXCollections.unmodifiableObservableList(servingSizes);
    }

    @Override
    public void addServingSize(UnitOfMeasure uom, BigDecimal quantity)
    {
        this.servingSizes.add(new ServingSizeViewModel(uom, NumberUtils.withStandardPrecision(quantity)));
    }

    @Override
    public void removeServingSize(ServingSizeModel servingSize)
    {
        this.servingSizes.remove(servingSize);
    }

    @Override
    public String toString() {
        return "ComplexFoodViewModel{" +
                "foodId=" + foodId +
                ", name=" + name +
                ", createdUser=" + createdUser +
                ", createdDate=" + createdDate +
                ", lastModifiedUser=" + lastModifiedUser +
                ", lastModifiedDate=" + lastModifiedDate +
                ", ingredients=" + ingredients +
                ", servingSizes=" + servingSizes +
                '}';
    }
}
