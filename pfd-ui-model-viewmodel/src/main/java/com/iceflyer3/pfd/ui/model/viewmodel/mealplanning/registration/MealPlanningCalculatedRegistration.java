package com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.registration;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023, 2025 Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.enums.ActivityLevel;
import com.iceflyer3.pfd.enums.BasalMetabolicRateFormula;
import com.iceflyer3.pfd.enums.FitnessGoal;
import com.iceflyer3.pfd.enums.Sex;
import com.iceflyer3.pfd.ui.model.api.mealplanning.MealPlanNutrientSuggestionModel;
import com.iceflyer3.pfd.ui.model.api.mealplanning.registration.MealPlanningRegistration;
import com.iceflyer3.pfd.ui.model.api.viewmodel.ValidatingViewModel;
import com.iceflyer3.pfd.validation.*;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Encapsulates all the information needed to calculate all the information associated with a meal planning
 * profile.
 *
 * Items calculated based upon this information include data like BMR and caloric and macronutrient needs.
 *
 * This class is also a bit of a special case in that it is a request object for the meal planning calculator
 * but also functionally serves as a view model to collect said information from the user via the GUI.
 *
 * These are two closely related roles and the shape of the two objects would look nearly identical
 * (with the difference being one uses properties and one doesn't) so I saw no real advantage to separating them.
 */
/*
 * With the addition of the model based validation we may wish to revisit this point. It does feel a bit sloppy
 * to put validation logic into what is actually supposed to be a request object. But the fact remains that the
 * shapes are still exactly the same between a proper viewmodel / request split, so I'm undecided.
 *
 * For the time being this works well enough. We'll cross that bridge if and when we come to it.
 */

public class MealPlanningCalculatedRegistration implements MealPlanningRegistration, ValidatingViewModel
{
    private final IntegerProperty age;
    private final ObjectProperty<BigDecimal> height;
    private final ObjectProperty<BigDecimal> weight;
    private final ObjectProperty<Sex> sex;
    private final ObjectProperty<BigDecimal> bodyFactPercentage;
    private final ObjectProperty<ActivityLevel> activityLevel;
    private final ObjectProperty<FitnessGoal> fitnessGoal;
    private final ObjectProperty<BasalMetabolicRateFormula> bmrFormula;
    private final ObjectProperty<BigDecimal> dailyCalories;
    private final ObjectProperty<BigDecimal> proteinPercent;
    private final ObjectProperty<BigDecimal> carbsPercent;
    private final ObjectProperty<BigDecimal> fatsPercent;

    private final ObservableList<MealPlanNutrientSuggestionModel> customNutrients;

    // Validation
    private final Map<Integer, PropertyRestrictionBinding> propertyInvariants;
    private final NumericRangeDatumRestriction carbPercentageRestriction;
    private final NumericRangeDatumRestriction proteinPercentageRestriction;
    private final NumericRangeDatumRestriction fatPercentageRestriction;

    public MealPlanningCalculatedRegistration()
    {
        this.age = new SimpleIntegerProperty(0);
        this.height = new SimpleObjectProperty<>(new BigDecimal(0).setScale(2, RoundingMode.HALF_UP));
        this.weight = new SimpleObjectProperty<>(new BigDecimal(0).setScale(2, RoundingMode.HALF_UP));
        this.sex = new SimpleObjectProperty<>(Sex.MALE);
        this.bodyFactPercentage = new SimpleObjectProperty<>();
        this.activityLevel = new SimpleObjectProperty<>(ActivityLevel.MODERATE);
        this.fitnessGoal = new SimpleObjectProperty<>(FitnessGoal.MAINTAIN);
        this.bmrFormula = new SimpleObjectProperty<>(BasalMetabolicRateFormula.MIFFLIN_ST_JEOR);
        this.dailyCalories = new SimpleObjectProperty<>(new BigDecimal(0).setScale(2, RoundingMode.HALF_UP));
        this.proteinPercent = new SimpleObjectProperty<>(new BigDecimal(0).setScale(2, RoundingMode.HALF_UP));
        this.carbsPercent = new SimpleObjectProperty<>(new BigDecimal(0).setScale(2, RoundingMode.HALF_UP));
        this.fatsPercent = new SimpleObjectProperty<>(new BigDecimal(0).setScale(2, RoundingMode.HALF_UP));
        this.customNutrients = FXCollections.observableArrayList();

        /*
         * Establish property invariants
         *  - Age is required and must be non-zero
         *  - Height is required and must be non-zero
         *  - Weight is required and must be non-zero
         *  - Individual macronutrient percentages must fall in an allowable range
         *    that is determined by the user selected goal
         */

        // Defaults to the restrictions for the maintenance goal
        this.carbPercentageRestriction = new NumericRangeDatumRestriction(30.0, 50.0);
        this.proteinPercentageRestriction = new NumericRangeDatumRestriction(25.0, 35.0);
        this.fatPercentageRestriction = new NumericRangeDatumRestriction(25.0, 35.0);

        ObservableValuePropertyRestrictionBinding<Number> ageBinding = new ObservableValuePropertyRestrictionBinding<>(this.age);
        ageBinding.bindRestriction(new NonZeroDatumRestriction());

        ObservableValuePropertyRestrictionBinding<BigDecimal> heightBinding = new ObservableValuePropertyRestrictionBinding<>(this.height);
        heightBinding.bindRestriction(new NonZeroDatumRestriction());

        ObservableValuePropertyRestrictionBinding<BigDecimal> weightBinding = new ObservableValuePropertyRestrictionBinding<>(this.weight);
        weightBinding.bindRestriction(new NonZeroDatumRestriction());

        ObservableValuePropertyRestrictionBinding<BigDecimal> proteinPercentBinding = new ObservableValuePropertyRestrictionBinding<>(this.proteinPercent);
        proteinPercentBinding.bindRestriction(this.proteinPercentageRestriction);

        ObservableValuePropertyRestrictionBinding<BigDecimal> carbPercentBinding = new ObservableValuePropertyRestrictionBinding<>(this.carbsPercent);
        carbPercentBinding.bindRestriction(this.carbPercentageRestriction);

        ObservableValuePropertyRestrictionBinding<BigDecimal> fatPercentBinding = new ObservableValuePropertyRestrictionBinding<>(this.fatsPercent);
        fatPercentBinding.bindRestriction(this.fatPercentageRestriction);

        this.propertyInvariants = new HashMap<>();
        this.propertyInvariants.put(this.age.hashCode(), ageBinding);
        this.propertyInvariants.put(this.height.hashCode(), heightBinding);
        this.propertyInvariants.put(this.weight.hashCode(), weightBinding);
        this.propertyInvariants.put(this.proteinPercent.hashCode(), proteinPercentBinding);
        this.propertyInvariants.put(this.carbsPercent.hashCode(), carbPercentBinding);
        this.propertyInvariants.put(this.fatsPercent.hashCode(), fatPercentBinding);
    }

    public int getAge() {
        return age.get();
    }

    public IntegerProperty ageProperty() {
        return age;
    }

    public void setAge(int age) {
        this.age.set(age);
    }

    public BigDecimal getHeight() {
        return height.get();
    }

    public ObjectProperty<BigDecimal> heightProperty() {
        return height;
    }

    public void setHeight(BigDecimal height) {
        this.height.set(height);
    }

    public BigDecimal getWeight() {
        return weight.get();
    }

    public ObjectProperty<BigDecimal> weightProperty() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight.set(weight);
    }

    public Sex getSex() {
        return sex.get();
    }

    public ObjectProperty<Sex> sexProperty() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex.set(sex);
    }

    public BigDecimal getBodyFactPercentage() {
        return bodyFactPercentage.get();
    }

    public ObjectProperty<BigDecimal> bodyFactPercentageProperty() {
        return bodyFactPercentage;
    }

    public void setBodyFactPercentage(BigDecimal bodyFactPercentage) {
        this.bodyFactPercentage.set(bodyFactPercentage);
    }

    public ActivityLevel getActivityLevel() {
        return activityLevel.get();
    }

    public ObjectProperty<ActivityLevel> activityLevelProperty() {
        return activityLevel;
    }

    public void setActivityLevel(ActivityLevel activityLevel) {
        this.activityLevel.set(activityLevel);
    }

    public FitnessGoal getFitnessGoal() {
        return fitnessGoal.get();
    }

    public ObjectProperty<FitnessGoal> fitnessGoalProperty() {
        return fitnessGoal;
    }

    public void setFitnessGoal(FitnessGoal fitnessGoal) {
        this.fitnessGoal.set(fitnessGoal);
    }

    public BasalMetabolicRateFormula getBmrFormula() {
        return bmrFormula.get();
    }

    public ObjectProperty<BasalMetabolicRateFormula> bmrFormulaProperty() {
        return bmrFormula;
    }

    public void setBmrFormula(BasalMetabolicRateFormula bmrFormula) {
        this.bmrFormula.set(bmrFormula);
    }

    public BigDecimal getDailyCalories() {
        return dailyCalories.get();
    }

    public ObjectProperty<BigDecimal> dailyCaloriesProperty() {
        return dailyCalories;
    }

    public void setDailyCalories(BigDecimal dailyCalories) {
        this.dailyCalories.set(dailyCalories);
    }

    public BigDecimal getProteinPercent() {
        return proteinPercent.get();
    }

    public ObjectProperty<BigDecimal> proteinPercentProperty() {
        return proteinPercent;
    }

    public void setProteinPercent(BigDecimal proteinPercent) {
        this.proteinPercent.set(proteinPercent);
    }

    public BigDecimal getCarbsPercent() {
        return carbsPercent.get();
    }

    public ObjectProperty<BigDecimal> carbsPercentProperty() {
        return carbsPercent;
    }

    public void setCarbsPercent(BigDecimal carbsPercent) {
        this.carbsPercent.set(carbsPercent);
    }

    public BigDecimal getFatsPercent() {
        return fatsPercent.get();
    }

    public ObjectProperty<BigDecimal> fatsPercentProperty() {
        return fatsPercent;
    }

    public void setFatsPercent(BigDecimal fatsPercent) {
        this.fatsPercent.set(fatsPercent);
    }

    public ObservableList<MealPlanNutrientSuggestionModel> getCustomNutrients()
    {
        return customNutrients;
    }

    @Override
    public String toString() {
        return "MealPlanCalculationDetails{" +
                ", age=" + age +
                ", height=" + height +
                ", weight=" + weight +
                ", sex=" + sex +
                ", bodyFactPercentage=" + bodyFactPercentage +
                ", activityLevel=" + activityLevel +
                ", fitnessGoal=" + fitnessGoal +
                ", bmrFormula=" + bmrFormula +
                ", dailyCalories=" + dailyCalories +
                ", proteinPercent=" + proteinPercent +
                ", carbsPercent=" + carbsPercent +
                ", fatsPercent=" + fatsPercent +
                ", customNutrients=" + customNutrients +
                '}';
    }

    @Override
    public ValidationResult validate()
    {
        updateValidMacronutrientRanges();
        Set<ValidationResult> propertyInvariantResults = new HashSet<>();
        for(Map.Entry<Integer, PropertyRestrictionBinding> entry : propertyInvariants.entrySet())
        {
            propertyInvariantResults.add(entry.getValue().applyRestrictions());
        }
        ValidationResult result = ValidationResult.flatten(propertyInvariantResults);

        // Ensure that the macronutrient percentages collectively add up to 100%
        // Properties can be null when bidirectionally bound to GUI controls
        BigDecimal proteinPercent = this.proteinPercent.get() == null ? BigDecimal.ZERO : this.proteinPercent.get();
        BigDecimal carbsPercent = this.carbsPercent.get() == null ? BigDecimal.ZERO : this.carbsPercent.get();
        BigDecimal fatsPercent = this.fatsPercent.get() == null ? BigDecimal.ZERO : this.fatsPercent.get();
        BigDecimal macroGrandTotal = proteinPercent.add(carbsPercent.add(fatsPercent));
        return result.and(() -> macroGrandTotal.compareTo(BigDecimal.valueOf(100)) == 0, "Macronutrient percentages must collectively total 100%");
    }

    @Override
    public ValidationResult validateProperty(int propertyHashcode)
    {
        updateValidMacronutrientRanges();
        return propertyInvariants.get(propertyHashcode).applyRestrictions();
    }

    private void updateValidMacronutrientRanges()
    {
        // These restrictions are based upon https://www.bodybuilding.com/content/macro-math-3-keys-to-dialing-in-your-macro-ratios.html
        switch (fitnessGoal.get())
        {
            case GAIN ->
            {
                proteinPercentageRestriction.setRange(25.0, 35.0);
                carbPercentageRestriction.setRange(40.0, 60.0);
                fatPercentageRestriction.setRange(15.0, 25.0);
            }
            case LOSE ->
            {
                proteinPercentageRestriction.setRange(40.0, 50.0);
                carbPercentageRestriction.setRange(10.0, 30.0);
                fatPercentageRestriction.setRange(30.0, 40.0);
            }
            case MAINTAIN ->
            {
                proteinPercentageRestriction.setRange(25.0, 35.0);
                carbPercentageRestriction.setRange(30.0, 50.0);
                fatPercentageRestriction.setRange(25.0, 35.0);
            }
        }
    }
}
