package com.iceflyer3.pfd.ui.model.viewmodel;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.enums.UnitOfMeasure;
import com.iceflyer3.pfd.ui.model.api.ServingSizeModel;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

import java.math.BigDecimal;
import java.util.UUID;

public class ServingSizeViewModel implements ServingSizeModel
{

    private final UUID servingSizeId;

    private final ObjectProperty<UnitOfMeasure> unitOfMeasure;
    private final ObjectProperty<BigDecimal> servingSize;

    /**
     * Constructor to be used when we are instantiating a new instance that is being mapped from a loaded database entity.
     * @param servingSizeId ServingId of the database record
     * @param unitOfMeasure Unit of measure
     * @param servingSize Number of servings of the unit of measure
     */
    public ServingSizeViewModel(UUID servingSizeId, UnitOfMeasure unitOfMeasure, BigDecimal servingSize) {
        this(servingSizeId, new SimpleObjectProperty<>(unitOfMeasure), new SimpleObjectProperty<>(servingSize));
    }

    /**
     * Constructor to be used when we are instantiating a new instance that is not saved in the database.
     * @param unitOfMeasure Unit of measure
     * @param servingSize Number of servings of the unit of measure
     */
    public ServingSizeViewModel(UnitOfMeasure unitOfMeasure, BigDecimal servingSize) {
        this(null, new SimpleObjectProperty<>(unitOfMeasure), new SimpleObjectProperty<>(servingSize));
    }

    private ServingSizeViewModel(UUID servingSizeId, ObjectProperty<UnitOfMeasure> unitOfMeasureProperty, ObjectProperty<BigDecimal> servingSizeProperty) {
        this.servingSizeId = servingSizeId;
        this.unitOfMeasure = unitOfMeasureProperty;
        this.servingSize = servingSizeProperty;
    }

    public UUID getId() {
        return servingSizeId;
    }

    public UnitOfMeasure getUnitOfMeasure() {
        return unitOfMeasure.get();
    }

    public ObjectProperty<UnitOfMeasure> unitOfMeasureProperty() {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(UnitOfMeasure unitOfMeasure) {
        this.unitOfMeasure.set(unitOfMeasure);
    }

    public BigDecimal getServingSize() {
        return servingSize.get();
    }

    public ObjectProperty<BigDecimal> servingSizeProperty() {
        return servingSize;
    }

    public void setServingSize(BigDecimal servingSize) {
        this.servingSize.set(servingSize);
    }

    @Override
    public String toString() {
        return "FoodServingSizeViewModel{" +
                "servingId=" + servingSizeId +
                ", unitOfMeasure=" + unitOfMeasure +
                ", servingSize=" + servingSize +
                '}';
    }
}
