package com.iceflyer3.pfd.ui.model.viewmodel.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.enums.FitnessGoal;
import com.iceflyer3.pfd.ui.model.api.mealplanning.MealPlanDayModel;
import com.iceflyer3.pfd.ui.model.api.mealplanning.MealPlanMealModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.base.AbstractSummaryBasedNutritionFactsReader;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;
import com.iceflyer3.pfd.validation.MinimumLengthDatumRestriction;
import com.iceflyer3.pfd.validation.ObservableValuePropertyRestrictionBinding;
import com.iceflyer3.pfd.validation.ValidationResult;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.time.LocalDateTime;
import java.util.UUID;
import java.util.Collection;

public class MealPlanDayViewModel extends AbstractSummaryBasedNutritionFactsReader implements MealPlanDayModel
{
    protected final UUID dayId;
    protected final UUID owningMealPlanId;
    private final StringProperty dayLabel;
    private final ObjectProperty<FitnessGoal> fitnessGoal;
    private final ObjectProperty<LocalDateTime> lastModifiedDate;

    private final ObservableValuePropertyRestrictionBinding<String> dayLabelRestrictionBinding;

    public MealPlanDayViewModel(UUID dayId, UUID owningMealPlanId, NutritionalSummary nutrientConsumptionSummary)
    {
        super();
        this.dayId = dayId;
        this.owningMealPlanId = owningMealPlanId;
        this.nutritionalSummary = nutrientConsumptionSummary;

        this.dayLabel = new SimpleStringProperty();
        this.fitnessGoal = new SimpleObjectProperty<>();
        this.lastModifiedDate = new SimpleObjectProperty<>(LocalDateTime.now());

        /*
         * Establish property invariants
         *  - The meal plan day must have a name / label
         */
        this.dayLabelRestrictionBinding = new ObservableValuePropertyRestrictionBinding<>(this.dayLabel);
        this.dayLabelRestrictionBinding.bindRestriction(new MinimumLengthDatumRestriction(1));

        // Populate initial values for the JavaFx properties
        this.refreshNutritionalProperties();
    }

    @Override
    public ValidationResult validate()
    {
        return dayLabelRestrictionBinding.applyRestrictions();
    }

    @Override
    public ValidationResult validateProperty(int propertyHashcode)
    {
        return dayLabelRestrictionBinding.applyRestrictions();
    }

    @Override
    public UUID getId()
    {
        return dayId;
    }

    public UUID getOwningMealPlanId()
    {
        return owningMealPlanId;
    }

    @Override
    public String getDayLabel()
    {
        return dayLabel.get();
    }

    @Override
    public StringProperty dayLabelProperty()
    {
        return dayLabel;
    }

    @Override
    public void setDayLabel(String dayLabel)
    {
        this.dayLabel.set(dayLabel);
    }

    @Override
    public FitnessGoal getFitnessGoal()
    {
        return fitnessGoal.get();
    }

    @Override
    public ObjectProperty<FitnessGoal> fitnessGoalProperty()
    {
        return fitnessGoal;
    }

    @Override
    public void setFitnessGoal(FitnessGoal fitnessGoal)
    {
        this.fitnessGoal.set(fitnessGoal);
    }

    @Override
    public LocalDateTime getLastModifiedDate()
    {
        return lastModifiedDate.get();
    }

    @Override
    public ObjectProperty<LocalDateTime> lastModifiedDateProperty()
    {
        return lastModifiedDate;
    }

    @Override
    public void setLastModifiedDate(LocalDateTime lastModifiedDate)
    {
        this.lastModifiedDate.set(lastModifiedDate);
    }

    /**
     * The nutritional information for a day is defined by the meals which belong to it.
     * As such, this method is not supported. Use the variant that accepts meals as a parameter.
     *
     * If invoked this will throw a UnsupportedOperationException.
     */
    @Override
    @Deprecated
    public void refreshNutritionDetails()
    {
        throw new UnsupportedOperationException("Use the variant of this function that accepts the list of the meals for the day as a parameter instead.");
    }


    public <T extends MealPlanMealModel> void refreshNutritionDetails(Collection<T> meals)
    {
        meals.forEach(meal -> meal.refreshNutritionDetails());
        nutritionalSummary = NutritionalSummary.deriveUpdateFrom(meals);
        this.refreshNutritionalProperties();
    }

    @Override
    public String toString()
    {
        return "MealPlanDayViewModel{"
                + "dayId=" + dayId
                + ", dayLabel=" + dayLabel
                + ", fitnessGoal=" + fitnessGoal
                + ", lastModifiedDate=" + lastModifiedDate
                + '}';
    }
}
