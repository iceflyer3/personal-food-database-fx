package com.iceflyer3.pfd.ui.model.viewmodel.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.api.food.ReadOnlyFoodModel;
import com.iceflyer3.pfd.ui.model.api.viewmodel.IngredientLazyLoadingViewModel;
import com.iceflyer3.pfd.ui.model.api.mealplanning.MealPlanMealModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.base.AbstractSummaryBasedNutritionFactsReader;
import com.iceflyer3.pfd.ui.model.viewmodel.IngredientViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.food.ReadOnlyFoodViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;
import com.iceflyer3.pfd.validation.*;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class MealPlanMealViewModel extends AbstractSummaryBasedNutritionFactsReader implements MealPlanMealModel, IngredientLazyLoadingViewModel
{

    protected final UUID mealId;

    protected final StringProperty mealName;
    protected final ObjectProperty<LocalDateTime> lastModifiedDate;
    protected final ObservableList<IngredientModel> ingredients;

    /*
     * Maps the HashCode of a property to any restrictions that should be enforced
     * to ensure correct and valid values for that property.
     *
     * Collectively this map contains all the invariants that have to do with an individual
     * property. But not necessarily all invariants of the class.
     */
    private final Map<Integer, PropertyRestrictionBinding> propertyInvariants;

    public MealPlanMealViewModel(UUID mealId, NutritionalSummary nutrientConsumptionSummary) {
        super();
        this.mealId = mealId;
        this.nutritionalSummary = nutrientConsumptionSummary;

        this.mealName = new SimpleStringProperty();
        this.ingredients = FXCollections.observableArrayList();
        this.lastModifiedDate = new SimpleObjectProperty<>(LocalDateTime.now());

        /*
         * Establish property invariants
         *  - The meal plan day must have a name / label
         *  - The meal must have at least one ingredient
         *    (but only when previously saved. see comment in validate function)
         */
        ObservableValuePropertyRestrictionBinding<String> mealNameRestrictionBinding = new ObservableValuePropertyRestrictionBinding<>(this.mealName);
        mealNameRestrictionBinding.bindRestriction(new MinimumLengthDatumRestriction(1));

        this.propertyInvariants = new HashMap<>();
        this.propertyInvariants.put(this.mealName.hashCode(), mealNameRestrictionBinding);

        // Populate initial values for the JavaFx properties
        this.refreshNutritionalProperties();
    }

    @Override
    public ValidationResult validate() {
        /*
         * The meal will only ever have ingredients when it has already been saved.
         *
         * So we don't validate the invariant for requiring one or more ingredients until after
         * the meal has already been saved. When it is initially created only the name may be
         * specified.
         */
        ValidationResult validationResult = propertyInvariants.get(mealName.hashCode()).applyRestrictions();

        if (mealId != null)
        {
            validationResult = validationResult.and(() -> ingredients.size() > 0, "Meal must have at least one ingredient");
        }

        return validationResult;
    }

    @Override
    public ValidationResult validateProperty(int propertyHashcode)
    {
        return propertyInvariants.get(propertyHashcode).applyRestrictions();
    }

    @Override
    public UUID getId() {
        return mealId;
    }

    @Override
    public String getName() {
        return mealName.get();
    }

    @Override
    public void setName(String mealName) {
        this.mealName.set(mealName);
    }

    @Override
    public StringProperty nameProperty() {
        return mealName;
    }

    @Override
    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate.get();
    }

    @Override
    public void setLastModifiedDate(LocalDateTime lastModDate) {
        this.lastModifiedDate.set(lastModDate);
    }

    @Override
    public ObjectProperty<LocalDateTime> lastModifiedDateProperty() {
        return lastModifiedDate;
    }

    @Override
    public ObservableList<IngredientModel> getIngredients() {
        return FXCollections.unmodifiableObservableList(ingredients);
    }

    /**
     * Convenience function for the setting the collection of ingredients used in the meal.
     *
     * Passing in null as the parameter will clear the list.
     * @param ingredients The collection of ingredients used in the meal
     */
    public void setIngredients(Collection<IngredientModel> ingredients)
    {
        this.ingredients.clear();

        if (ingredients != null)
        {
            this.ingredients.addAll(ingredients);
        }
    }

    /**
     * Add a new ingredient to the list of ingredients. The provided ingredient should be of the view model
     * implementation type which is ReadOnlyFoodViewModel.
     *
     * @param ingredient The ReadOnlyFoodViewModel instance that represents the ingredient food
     */
    @Override
    public void addIngredient(ReadOnlyFoodModel ingredient)
    {
        if (ingredient instanceof ReadOnlyFoodViewModel)
        {
            this.ingredients.add(new IngredientViewModel(ingredient));
        }
        else
        {
            throw new IllegalArgumentException("ingredientFood must be an instance of ReadOnlyFoodViewModel");
        }
    }

    @Override
    public void removeIngredient(IngredientModel ingredient)
    {
        this.ingredients.remove(ingredient);
    }

    @Override
    public void refreshNutritionDetails()
    {
        /*
         * Ingredients are only loaded when we actually need to interact with them. Edit, add / remove,
         * display, etc..
         *
         * If they're loaded then they should be refreshed to guarantee accurate information for the
         * meal. But if they aren't then just assume that the summary data loaded from the appropriate
         * DB view is still correct and refresh the UI JavaFx properties with that data.
         */
        if (ingredients.size() > 0)
        {
            ingredients.forEach(IngredientModel::refreshNutritionDetails);
            this.nutritionalSummary = NutritionalSummary.deriveUpdateFrom(ingredients);
        }

        this.refreshNutritionalProperties();
    }
}
