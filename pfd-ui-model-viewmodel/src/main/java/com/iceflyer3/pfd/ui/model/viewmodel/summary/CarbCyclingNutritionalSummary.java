package com.iceflyer3.pfd.ui.model.viewmodel.summary;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;

import java.math.BigDecimal;

/**
 * Macronutrient and micronutrient summary information for either day or
 * a meal which is following a carb cycling plan
 *
 * This class is immutable.
 */
public class CarbCyclingNutritionalSummary extends NutritionalSummary
{
    protected ReadOnlyObjectWrapper<BigDecimal> requiredCalories;
    protected ReadOnlyObjectWrapper<BigDecimal> requiredProtein;
    protected ReadOnlyObjectWrapper<BigDecimal> requiredCarbohydrates;
    protected ReadOnlyObjectWrapper<BigDecimal> requiredTotalFats;

    public CarbCyclingNutritionalSummary(BigDecimal requiredCalories, BigDecimal requiredProtein, BigDecimal requiredCarbohydrates, BigDecimal requiredTotalFats, BigDecimal calories, BigDecimal protein, BigDecimal totalFats, BigDecimal transFat, BigDecimal monounsaturatedFat, BigDecimal polyunsaturatedFat, BigDecimal saturatedFat, BigDecimal carbohydrates, BigDecimal sugars, BigDecimal fiber, BigDecimal cholesterol, BigDecimal iron, BigDecimal manganese, BigDecimal copper, BigDecimal zinc, BigDecimal calcium, BigDecimal magnesium, BigDecimal phosphorus, BigDecimal potassium, BigDecimal sodium, BigDecimal sulfur, BigDecimal vitaminA, BigDecimal vitaminC, BigDecimal vitaminK, BigDecimal vitaminD, BigDecimal vitaminE, BigDecimal vitaminB1, BigDecimal vitaminB2, BigDecimal vitaminB3, BigDecimal vitaminB5, BigDecimal vitaminB6, BigDecimal vitaminB7, BigDecimal vitaminB8, BigDecimal vitaminB9, BigDecimal vitaminB12)
    {
        super(calories, protein, totalFats, transFat, monounsaturatedFat, polyunsaturatedFat, saturatedFat, carbohydrates, sugars, fiber, cholesterol, iron, manganese, copper, zinc, calcium, magnesium, phosphorus, potassium, sodium, sulfur, vitaminA, vitaminC, vitaminK, vitaminD, vitaminE, vitaminB1, vitaminB2, vitaminB3, vitaminB5, vitaminB6, vitaminB7, vitaminB8, vitaminB9, vitaminB12);
        this.requiredCalories = new ReadOnlyObjectWrapper<>(requiredCalories);
        this.requiredProtein = new ReadOnlyObjectWrapper<>(requiredProtein);
        this.requiredCarbohydrates = new ReadOnlyObjectWrapper<>(requiredCarbohydrates);
        this.requiredTotalFats = new ReadOnlyObjectWrapper<>(requiredTotalFats);
    }

    public BigDecimal getRequiredCalories()
    {
        return requiredCalories.get();
    }

    public ReadOnlyObjectProperty<BigDecimal> requiredCaloriesProperty()
    {
        return requiredCalories.getReadOnlyProperty();
    }

    public BigDecimal getRequiredProtein()
    {
        return requiredProtein.get();
    }

    public ReadOnlyObjectProperty<BigDecimal> requiredProteinProperty()
    {
        return requiredProtein.getReadOnlyProperty();
    }

    public BigDecimal getRequiredCarbohydrates()
    {
        return requiredCarbohydrates.get();
    }

    public ReadOnlyObjectProperty<BigDecimal> requiredCarbohydratesProperty()
    {
        return requiredCarbohydrates.getReadOnlyProperty();
    }

    public BigDecimal getRequiredTotalFats()
    {
        return requiredTotalFats.get();
    }

    public ReadOnlyObjectProperty<BigDecimal> requiredTotalFatsProperty()
    {
        return requiredTotalFats.getReadOnlyProperty();
    }
}
