/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


package com.iceflyer3.pfd.ui.model.viewmodel.food;

import com.iceflyer3.pfd.enums.FoodType;
import com.iceflyer3.pfd.enums.UnitOfMeasure;
import com.iceflyer3.pfd.ui.model.api.ServingSizeModel;
import com.iceflyer3.pfd.ui.model.api.food.SimpleFoodModel;
import com.iceflyer3.pfd.ui.model.viewmodel.ServingSizeViewModel;

import com.iceflyer3.pfd.util.NumberUtils;
import com.iceflyer3.pfd.validation.*;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;

/**
 * Mutable ReadOnlyFoodModel implementation for collecting information from the user
 * for the creation and editing of Simple Foods.
 */
public class SimpleFoodViewModel implements SimpleFoodModel {

    protected UUID foodId;

    // Basic information
    protected ObjectProperty<FoodType> foodType;
    protected StringProperty name;
    protected StringProperty brand;
    protected StringProperty source;
    protected ObservableList<ServingSizeModel> servingSizes; // If we want to be able to bind to tables this has to be a list instead of a set. There is no TableView.setItems(Set) function.
    protected IntegerProperty glycemicIndex;
    protected ObjectProperty<BigDecimal> calories;

    protected StringProperty createdUser;
    protected ObjectProperty<LocalDateTime> createdDate;
    protected StringProperty lastModifiedUser;
    protected ObjectProperty<LocalDateTime> lastModifiedDate;

    // Macronutrients
    protected ObjectProperty<BigDecimal> protein;
    protected ObjectProperty<BigDecimal> totalFats;
    protected ObjectProperty<BigDecimal> transFat;
    protected ObjectProperty<BigDecimal> monounsaturatedFat;
    protected ObjectProperty<BigDecimal> polyunsaturatedFat;
    protected ObjectProperty<BigDecimal> saturatedFat;
    protected ObjectProperty<BigDecimal> carbohydrates;
    protected ObjectProperty<BigDecimal> sugars;
    protected ObjectProperty<BigDecimal> fiber;

    // Micronutrients - Lipids
    protected ObjectProperty<BigDecimal> cholesterol;

    // Micronutrients - Minerals
    protected ObjectProperty<BigDecimal> iron;
    protected ObjectProperty<BigDecimal> manganese;
    protected ObjectProperty<BigDecimal> copper;
    protected ObjectProperty<BigDecimal> zinc;
    protected ObjectProperty<BigDecimal> calcium;
    protected ObjectProperty<BigDecimal> magnesium;
    protected ObjectProperty<BigDecimal> phosphorus;
    protected ObjectProperty<BigDecimal> potassium;
    protected ObjectProperty<BigDecimal> sodium;
    protected ObjectProperty<BigDecimal> sulfur;

    // Micronutrients - Vitamins
    protected ObjectProperty<BigDecimal> vitaminA;
    protected ObjectProperty<BigDecimal> vitaminC;
    protected ObjectProperty<BigDecimal> vitaminK;
    protected ObjectProperty<BigDecimal> vitaminD;
    protected ObjectProperty<BigDecimal> vitaminE;
    protected ObjectProperty<BigDecimal> vitaminB1;
    protected ObjectProperty<BigDecimal> vitaminB2;
    protected ObjectProperty<BigDecimal> vitaminB3;
    protected ObjectProperty<BigDecimal> vitaminB5;
    protected ObjectProperty<BigDecimal> vitaminB6;
    protected ObjectProperty<BigDecimal> vitaminB7;
    protected ObjectProperty<BigDecimal> vitaminB8;
    protected ObjectProperty<BigDecimal> vitaminB9;
    protected ObjectProperty<BigDecimal> vitaminB12;

    /*
     * Maps the HashCode of a property to any restrictions that should be enforced
     * to ensure correct and valid values for that property.
     *
     * Collectively this map contains all the invariants that have to do with an individual
     * property. But not necessarily all invariants of the class.
     */
    private final Map<Integer, PropertyRestrictionBinding> propertyInvariants;

    /**
     * Constructor to be used when details of the food are not yet known.
     *
     * @param creatorUsername The user who created the food
     */
    public SimpleFoodViewModel(String creatorUsername)
    {
        this(
                null,
                new SimpleStringProperty(creatorUsername),
                new SimpleObjectProperty<>(LocalDateTime.now()),
                new SimpleStringProperty(),
                new SimpleStringProperty(),
                new SimpleStringProperty(),
                new SimpleListProperty<>(FXCollections.observableArrayList()),
                new SimpleObjectProperty<>(new BigDecimal(1))
        );
    }

    /**
     * Constructor to be used when details of the food are known.
     * @param foodId FoodId of the database record
     * @param creatorUserName The user who created the food
     * @param createdDate The date the food was created
     * @param foodName Name of the food
     * @param brandName Brand of the food
     * @param source Where the nutritional stats for this food are sourced from
     * @param servingSizes List of serving sizes for the food
     * @param calories Amount of calories in a single serving
     */
    public SimpleFoodViewModel(UUID foodId,
                               String creatorUserName,
                               LocalDateTime createdDate,
                               String foodName,
                               String brandName,
                               String source,
                               Set<ServingSizeViewModel> servingSizes,
                               BigDecimal calories)
    {
        this(
                foodId,
                new SimpleStringProperty(creatorUserName),
                new SimpleObjectProperty<>(createdDate),
                new SimpleStringProperty(foodName),
                new SimpleStringProperty(brandName),
                new SimpleStringProperty(source),
                FXCollections.observableArrayList(servingSizes),
                new SimpleObjectProperty<>(calories)
        );
    }

    private SimpleFoodViewModel(UUID foodId,
                          SimpleStringProperty createdUserNameProperty,
                          SimpleObjectProperty<LocalDateTime> createdDateProperty,
                          SimpleStringProperty foodNameProperty,
                          SimpleStringProperty brandNameProperty,
                          SimpleStringProperty sourceProperty,
                          ObservableList<ServingSizeModel> servingSizes,
                          SimpleObjectProperty<BigDecimal> caloriesProperty)
    {
        this.foodId = foodId;

        // Init required fields first
        this.name = foodNameProperty;
        this.brand = brandNameProperty;
        this.source = sourceProperty;
        this.servingSizes = servingSizes;
        this.calories = caloriesProperty;
        this.createdDate = createdDateProperty;
        this.createdUser = createdUserNameProperty;

        // Remaining basic info
        this.foodType = new SimpleObjectProperty<>(FoodType.SIMPLE);
        this.glycemicIndex = new SimpleIntegerProperty();
        this.lastModifiedUser = new SimpleStringProperty(createdUserNameProperty.get());
        this.lastModifiedDate = new SimpleObjectProperty<>(LocalDateTime.now());

        // Macronutrients
        this.protein = new SimpleObjectProperty<>(NumberUtils.withStandardPrecision(0));
        this.totalFats = new SimpleObjectProperty<>(NumberUtils.withStandardPrecision(0));
        this.transFat = new SimpleObjectProperty<>(NumberUtils.withStandardPrecision(0));
        this.monounsaturatedFat = new SimpleObjectProperty<>(NumberUtils.withStandardPrecision(0));
        this.polyunsaturatedFat = new SimpleObjectProperty<>(NumberUtils.withStandardPrecision(0));
        this.saturatedFat = new SimpleObjectProperty<>(NumberUtils.withStandardPrecision(0));
        this.carbohydrates = new SimpleObjectProperty<>(NumberUtils.withStandardPrecision(0));
        this.sugars = new SimpleObjectProperty<>(NumberUtils.withStandardPrecision(0));
        this.fiber = new SimpleObjectProperty<>(NumberUtils.withStandardPrecision(0));

        // Micronutrients - Lipids
        this.cholesterol = new SimpleObjectProperty<>(NumberUtils.withStandardPrecision(0));

        // Micronutrients - Minerals
        this.iron = new SimpleObjectProperty<>(NumberUtils.withStandardPrecision(0));
        this.manganese = new SimpleObjectProperty<>(NumberUtils.withStandardPrecision(0));
        this.copper = new SimpleObjectProperty<>(NumberUtils.withStandardPrecision(0));
        this.zinc = new SimpleObjectProperty<>(NumberUtils.withStandardPrecision(0));
        this.calcium = new SimpleObjectProperty<>(NumberUtils.withStandardPrecision(0));
        this.magnesium = new SimpleObjectProperty<>(NumberUtils.withStandardPrecision(0));
        this.phosphorus = new SimpleObjectProperty<>(NumberUtils.withStandardPrecision(0));
        this.potassium = new SimpleObjectProperty<>(NumberUtils.withStandardPrecision(0));
        this.sodium  = new SimpleObjectProperty<>(NumberUtils.withStandardPrecision(0));
        this.sulfur = new SimpleObjectProperty<>(NumberUtils.withStandardPrecision(0));

        // Micronutrients - Vitamins
        this.vitaminA = new SimpleObjectProperty<>(NumberUtils.withStandardPrecision(0));
        this.vitaminC = new SimpleObjectProperty<>(NumberUtils.withStandardPrecision(0));
        this.vitaminK = new SimpleObjectProperty<>(NumberUtils.withStandardPrecision(0));
        this.vitaminD = new SimpleObjectProperty<>(NumberUtils.withStandardPrecision(0));
        this.vitaminE = new SimpleObjectProperty<>(NumberUtils.withStandardPrecision(0));
        this.vitaminB1 = new SimpleObjectProperty<>(NumberUtils.withStandardPrecision(0));
        this.vitaminB2 = new SimpleObjectProperty<>(NumberUtils.withStandardPrecision(0));
        this.vitaminB3 = new SimpleObjectProperty<>(NumberUtils.withStandardPrecision(0));
        this.vitaminB5 = new SimpleObjectProperty<>(NumberUtils.withStandardPrecision(0));
        this.vitaminB6 = new SimpleObjectProperty<>(NumberUtils.withStandardPrecision(0));
        this.vitaminB7 = new SimpleObjectProperty<>(NumberUtils.withStandardPrecision(0));
        this.vitaminB8 = new SimpleObjectProperty<>(NumberUtils.withStandardPrecision(0));
        this.vitaminB9 = new SimpleObjectProperty<>(NumberUtils.withStandardPrecision(0));
        this.vitaminB12 = new SimpleObjectProperty<>(NumberUtils.withStandardPrecision(0));

        /*
         * Setup property invariants
         *  - A name, source, and a brand
         *  - Non-zero caloric value
         *  - At least one serving size
         */
        ObservableValuePropertyRestrictionBinding<String> namePropertyBinding = new ObservableValuePropertyRestrictionBinding<>(this.name);
        namePropertyBinding.bindRestriction(new MinimumLengthDatumRestriction(1));

        ObservableValuePropertyRestrictionBinding<String> sourcePropertyBinding = new ObservableValuePropertyRestrictionBinding<>(this.source);
        sourcePropertyBinding.bindRestriction(new MinimumLengthDatumRestriction(1));

        ObservableValuePropertyRestrictionBinding<String> brandPropertyBinding = new ObservableValuePropertyRestrictionBinding<>(this.brand);
        brandPropertyBinding.bindRestriction(new MinimumLengthDatumRestriction(1));

        ObservableValuePropertyRestrictionBinding<BigDecimal> caloriesPropertyBinding = new ObservableValuePropertyRestrictionBinding<>(this.calories);
        caloriesPropertyBinding.bindRestriction(new NonZeroDatumRestriction());

        propertyInvariants = new HashMap<>();
        propertyInvariants.put(this.name.hashCode(), namePropertyBinding);
        propertyInvariants.put(this.brand.hashCode(), brandPropertyBinding);
        propertyInvariants.put(this.source.hashCode(), sourcePropertyBinding);
        propertyInvariants.put(this.calories.hashCode(), caloriesPropertyBinding);
    }

    @Override
    public ValidationResult validate()
    {
        Set<ValidationResult> propertyInvariantResults = new HashSet<>();
        for(Map.Entry<Integer, PropertyRestrictionBinding> entry : propertyInvariants.entrySet())
        {
            propertyInvariantResults.add(entry.getValue().applyRestrictions());
        }

        ValidationResult result = ValidationResult.flatten(propertyInvariantResults);
        return result.and(() -> servingSizes.size() > 0, "The simple food must have at least one serving size");
    }

    @Override
    public ValidationResult validateProperty(int propertyHashcode)
    {
        return propertyInvariants.get(propertyHashcode).applyRestrictions();
    }

    @Override
    public UUID getId() {
        return foodId;
    }

    // Basic info
    public FoodType getFoodType() {
        return foodType.get();
    }
    public ObjectProperty<FoodType> foodTypeProperty() {
        return foodType;
    }
    public void setFoodType(FoodType foodType) {
        this.foodType.set(foodType);
    }

    public String getName() {
        return name.get();
    }
    public StringProperty nameProperty() {
        return name;
    }
    public void setName(String foodName) {
        this.name.set(foodName);
    }

    public String getBrand() {
        return brand.get();
    }
    public StringProperty brandProperty() {
        return brand;
    }
    public void setBrand(String brandName) {
        this.brand.set(brandName);
    }

    public String getSource() {
        return source.get();
    }
    public StringProperty sourceProperty() {
        return source;
    }
    public void setSource(String source) {
        this.source.set(source);
    }

    public int getGlycemicIndex() {
        return glycemicIndex.get();
    }
    public IntegerProperty glycemicIndexProperty() {
        return glycemicIndex;
    }
    public void setGlycemicIndex(int glycemicIndex) {
        this.glycemicIndex.set(glycemicIndex);
    }

    public ObservableList<ServingSizeModel> getServingSizes() {
        return FXCollections.unmodifiableObservableList(servingSizes);
    }

    @Override
    public void addServingSize(UnitOfMeasure uom, BigDecimal quantity)
    {
        this.servingSizes.add(new ServingSizeViewModel(uom, quantity));
    }

    @Override
    public void removeServingSize(ServingSizeModel servingSize)
    {
        this.servingSizes.remove(servingSize);
    }

    public BigDecimal getCalories() {
        return calories.get();
    }
    public ObjectProperty<BigDecimal> caloriesProperty() {
        return calories;
    }
    public void setCalories(BigDecimal calories) {
        this.calories.set(calories);
    }

    public String getCreatedUser() {
        return createdUser.get();
    }
    public StringProperty createdUserProperty() {
        return createdUser;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate.get();
    }
    public ObjectProperty<LocalDateTime> createdDateProperty() {
        return createdDate;
    }

    public String getLastModifiedUser() {
        return lastModifiedUser.get();
    }
    public StringProperty lastModifiedUserProperty() {
        return lastModifiedUser;
    }
    public void setLastModifiedUser(String lastModifiedUser) {
        this.lastModifiedUser.set(lastModifiedUser);
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate.get();
    }
    public ObjectProperty<LocalDateTime> lastModifiedDateProperty() {
        return lastModifiedDate;
    }
    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate.set(lastModifiedDate);
    }



    // Macronutrients
    public BigDecimal getProtein() {
        return protein.get();
    }
    public ObjectProperty<BigDecimal> proteinProperty() {
        return protein;
    }
    public void setProtein(BigDecimal protein) {
        this.protein.set(protein);
    }

    public BigDecimal getTotalFats() {
        return totalFats.get();
    }
    public ObjectProperty<BigDecimal> totalFatsProperty() {
        return totalFats;
    }
    public void setTotalFats(BigDecimal totalFats) {
        this.totalFats.set(totalFats);
    }

    public BigDecimal getTransFat() {
        return transFat.get();
    }
    public ObjectProperty<BigDecimal> transFatProperty() {
        return transFat;
    }
    public void setTransFat(BigDecimal transFat) {
        this.transFat.set(transFat);
    }

    public BigDecimal getMonounsaturatedFat() {
        return monounsaturatedFat.get();
    }
    public ObjectProperty<BigDecimal> monounsaturatedFatProperty() {
        return monounsaturatedFat;
    }
    public void setMonounsaturatedFat(BigDecimal monounsaturatedFat) {
        this.monounsaturatedFat.set(monounsaturatedFat);
    }

    public BigDecimal getPolyunsaturatedFat() {
        return polyunsaturatedFat.get();
    }
    public ObjectProperty<BigDecimal> polyunsaturatedFatProperty() {
        return polyunsaturatedFat;
    }
    public void setPolyunsaturatedFat(BigDecimal polyunsaturatedFat) {
        this.polyunsaturatedFat.set(polyunsaturatedFat);
    }

    public BigDecimal getSaturatedFat() {
        return saturatedFat.get();
    }
    public ObjectProperty<BigDecimal> saturatedFatProperty() {
        return saturatedFat;
    }
    public void setSaturatedFat(BigDecimal saturatedFat) {
        this.saturatedFat.set(saturatedFat);
    }

    public BigDecimal getCarbohydrates() {
        return carbohydrates.get();
    }
    public ObjectProperty<BigDecimal> carbohydratesProperty() {
        return carbohydrates;
    }
    public void setCarbohydrates(BigDecimal carbohydrates) {
        this.carbohydrates.set(carbohydrates);
    }

    public BigDecimal getSugars() {
        return sugars.get();
    }
    public ObjectProperty<BigDecimal> sugarsProperty() {
        return sugars;
    }
    public void setSugars(BigDecimal sugars) {
        this.sugars.set(sugars);
    }

    public BigDecimal getFiber() {
        return fiber.get();
    }
    public ObjectProperty<BigDecimal> fiberProperty() {
        return fiber;
    }
    public void setFiber(BigDecimal fiber) {
        this.fiber.set(fiber);
    }



    // Micronutrients - Lipids
    public BigDecimal getCholesterol() {
        return cholesterol.get();
    }
    public ObjectProperty<BigDecimal> cholesterolProperty() {
        return cholesterol;
    }
    public void setCholesterol(BigDecimal cholesterol) {
        this.cholesterol.set(cholesterol);
    }



    // Micronutrients - Minerals
    public BigDecimal getIron() {
        return iron.get();
    }
    public ObjectProperty<BigDecimal> ironProperty() {
        return iron;
    }
    public void setIron(BigDecimal iron) {
        this.iron.set(iron);
    }

    public BigDecimal getManganese() {
        return manganese.get();
    }
    public ObjectProperty<BigDecimal> manganeseProperty() {
        return manganese;
    }
    public void setManganese(BigDecimal manganese) {
        this.manganese.set(manganese);
    }

    public BigDecimal getCopper() {
        return copper.get();
    }
    public ObjectProperty<BigDecimal> copperProperty() {
        return copper;
    }
    public void setCopper(BigDecimal copper) {
        this.copper.set(copper);
    }

    public BigDecimal getZinc() {
        return zinc.get();
    }
    public ObjectProperty<BigDecimal> zincProperty() {
        return zinc;
    }
    public void setZinc(BigDecimal zinc) {
        this.zinc.set(zinc);
    }

    public BigDecimal getCalcium() {
        return calcium.get();
    }
    public ObjectProperty<BigDecimal> calciumProperty() {
        return calcium;
    }
    public void setCalcium(BigDecimal calcium) {
        this.calcium.set(calcium);
    }

    public BigDecimal getMagnesium() {
        return magnesium.get();
    }
    public ObjectProperty<BigDecimal> magnesiumProperty() {
        return magnesium;
    }
    public void setMagnesium(BigDecimal magnesium) {
        this.magnesium.set(magnesium);
    }

    public BigDecimal getPhosphorus() {
        return phosphorus.get();
    }
    public ObjectProperty<BigDecimal> phosphorusProperty() {
        return phosphorus;
    }
    public void setPhosphorus(BigDecimal phosphorus) {
        this.phosphorus.set(phosphorus);
    }

    public BigDecimal getPotassium() {
        return potassium.get();
    }
    public ObjectProperty<BigDecimal> potassiumProperty() {
        return potassium;
    }
    public void setPotassium(BigDecimal potassium) {
        this.potassium.set(potassium);
    }

    public BigDecimal getSodium() {
        return sodium.get();
    }
    public ObjectProperty<BigDecimal> sodiumProperty() {
        return sodium;
    }
    public void setSodium(BigDecimal sodium) {
        this.sodium.set(sodium);
    }

    public BigDecimal getSulfur() {
        return sulfur.get();
    }
    public ObjectProperty<BigDecimal> sulfurProperty() {
        return sulfur;
    }
    public void setSulfur(BigDecimal sulfur) {
        this.sulfur.set(sulfur);
    }



    // Micronutrients - Vitamins
    public BigDecimal getVitaminA() {
        return vitaminA.get();
    }
    public ObjectProperty<BigDecimal> vitaminAProperty() {
        return vitaminA;
    }
    public void setVitaminA(BigDecimal vitaminA) {
        this.vitaminA.set(vitaminA);
    }

    public BigDecimal getVitaminC() {
        return vitaminC.get();
    }
    public ObjectProperty<BigDecimal> vitaminCProperty() {
        return vitaminC;
    }
    public void setVitaminC(BigDecimal vitaminC) {
        this.vitaminC.set(vitaminC);
    }

    public BigDecimal getVitaminK() {
        return vitaminK.get();
    }
    public ObjectProperty<BigDecimal> vitaminKProperty() {
        return vitaminK;
    }
    public void setVitaminK(BigDecimal vitaminK) {
        this.vitaminK.set(vitaminK);
    }

    public BigDecimal getVitaminD() {
        return vitaminD.get();
    }
    public ObjectProperty<BigDecimal> vitaminDProperty() {
        return vitaminD;
    }
    public void setVitaminD(BigDecimal vitaminD) {
        this.vitaminD.set(vitaminD);
    }

    public BigDecimal getVitaminE() {
        return vitaminE.get();
    }
    public ObjectProperty<BigDecimal> vitaminEProperty() {
        return vitaminE;
    }
    public void setVitaminE(BigDecimal vitaminE) {
        this.vitaminE.set(vitaminE);
    }

    public BigDecimal getVitaminB1() {
        return vitaminB1.get();
    }
    public ObjectProperty<BigDecimal> vitaminB1Property() {
        return vitaminB1;
    }
    public void setVitaminB1(BigDecimal vitaminB1) {
        this.vitaminB1.set(vitaminB1);
    }

    public BigDecimal getVitaminB2() {
        return vitaminB2.get();
    }
    public ObjectProperty<BigDecimal> vitaminB2Property() {
        return vitaminB2;
    }
    public void setVitaminB2(BigDecimal vitaminB2) {
        this.vitaminB2.set(vitaminB2);
    }

    public BigDecimal getVitaminB3() {
        return vitaminB3.get();
    }
    public ObjectProperty<BigDecimal> vitaminB3Property() {
        return vitaminB3;
    }
    public void setVitaminB3(BigDecimal vitaminB3) {
        this.vitaminB3.set(vitaminB3);
    }

    public BigDecimal getVitaminB5() {
        return vitaminB5.get();
    }
    public ObjectProperty<BigDecimal> vitaminB5Property() {
        return vitaminB5;
    }
    public void setVitaminB5(BigDecimal vitaminB5) {
        this.vitaminB5.set(vitaminB5);
    }

    public BigDecimal getVitaminB6() {
        return vitaminB6.get();
    }
    public ObjectProperty<BigDecimal> vitaminB6Property() {
        return vitaminB6;
    }
    public void setVitaminB6(BigDecimal vitaminB6) {
        this.vitaminB6.set(vitaminB6);
    }

    public BigDecimal getVitaminB7() {
        return vitaminB7.get();
    }
    public ObjectProperty<BigDecimal> vitaminB7Property() {
        return vitaminB7;
    }
    public void setVitaminB7(BigDecimal vitaminB7) {
        this.vitaminB7.set(vitaminB7);
    }

    public BigDecimal getVitaminB8() {
        return vitaminB8.get();
    }
    public ObjectProperty<BigDecimal> vitaminB8Property() {
        return vitaminB8;
    }
    public void setVitaminB8(BigDecimal vitaminB8) {
        this.vitaminB8.set(vitaminB8);
    }

    public BigDecimal getVitaminB9() {
        return vitaminB9.get();
    }
    public ObjectProperty<BigDecimal> vitaminB9Property() {
        return vitaminB9;
    }
    public void setVitaminB9(BigDecimal vitaminB9) {
        this.vitaminB9.set(vitaminB9);
    }

    public BigDecimal getVitaminB12() {
        return vitaminB12.get();
    }
    public ObjectProperty<BigDecimal> vitaminB12Property() {
        return vitaminB12;
    }
    public void setVitaminB12(BigDecimal vitaminB12) {
        this.vitaminB12.set(vitaminB12);
    }

    @Override
    public void refreshNutritionDetails()
    {
        /*
         * Simple foods are unique in that they have no ingredients.
         *
         * Any updates to any of the information of a simple food are
         * also updates directly to its nutritional details.
         *
         * So this is a no-op.
         */
    }
}
