package com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.enums.CarbCyclingMealRelation;
import com.iceflyer3.pfd.ui.model.api.mealplanning.carbcycling.CarbCyclingMealConfigurationModel;
import com.iceflyer3.pfd.ui.model.api.mealplanning.carbcycling.CarbCyclingPlanModel;
import com.iceflyer3.pfd.validation.*;
import javafx.beans.property.*;

import java.math.BigDecimal;
import java.util.*;

public class CarbCyclingPlanViewModel implements CarbCyclingPlanModel {

    private final UUID planId;

    private final IntegerProperty lowCarbDays;
    private final IntegerProperty highCarbDays;
    private final ObjectProperty<BigDecimal> highCarbDaySurplusPercentage;
    private final ObjectProperty<BigDecimal> lowCarbDayDeficitPercentage;
    private final IntegerProperty mealsPerDay;
    private final BooleanProperty isActive;
    private final BooleanProperty shouldTimeFats;

    private final Set<CarbCyclingMealConfigurationModel> mealConfigurations;

    /*
     * Maps the HashCode of a property to any restrictions that should be enforced
     * to ensure correct and valid values for that property.
     *
     * Collectively this map contains all the invariants that have to do with an individual
     * property. But not necessarily all invariants of the class.
     */
    private final Map<Integer, PropertyRestrictionBinding> propertyInvariants;

    public CarbCyclingPlanViewModel() {
        this(
                null,
                0,
                0,
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                0,
                false,
                false,
                new HashSet<>()
        );
    }
    
    public CarbCyclingPlanViewModel(
            UUID planId,
            int lowCarbDays,
            int highCarbDays,
            BigDecimal highCarbDaySurplusPercentage,
            BigDecimal lowCarbDayDeficitPercentage,
            int mealsPerDay,
            boolean isActive,
            boolean shouldTimeFats,
            Set<CarbCyclingMealConfigurationModel> mealConfigurations)
    {
        this.planId = planId;

        this.lowCarbDays = new SimpleIntegerProperty(lowCarbDays);
        this.lowCarbDayDeficitPercentage = new SimpleObjectProperty<>(lowCarbDayDeficitPercentage);

        this.highCarbDays = new SimpleIntegerProperty(highCarbDays);
        this.highCarbDaySurplusPercentage = new SimpleObjectProperty<>(highCarbDaySurplusPercentage);

        this.mealsPerDay = new SimpleIntegerProperty(mealsPerDay);
        this.shouldTimeFats = new SimpleBooleanProperty(shouldTimeFats);
        this.isActive = new SimpleBooleanProperty(isActive);

        this.mealConfigurations = mealConfigurations;

        /* Establish property invariants
            - Number of meals per day
            - Any surplus or deficit for carbs
                * Valid range 0% - 99%
            - Number of high-carb and low-carb days for the plan
                * Valid range 1 - 6. There are only 7 days in a week
                  so this range enforces you must have at least one
                  high-carb and one-low carb day per week.
         */
        ObservableValuePropertyRestrictionBinding<Number> mealsPerDayRestrictionBinding = new ObservableValuePropertyRestrictionBinding<>(this.mealsPerDay);
        mealsPerDayRestrictionBinding.bindRestriction(new NonZeroDatumRestriction());

        ObservableValuePropertyRestrictionBinding<BigDecimal> highCarbSurplusDeficitRestrictionBinding = new ObservableValuePropertyRestrictionBinding<>(this.highCarbDaySurplusPercentage);
        highCarbSurplusDeficitRestrictionBinding.bindRestriction(new NumericRangeDatumRestriction(0, 99));

        ObservableValuePropertyRestrictionBinding<BigDecimal> lowCarbSurplusDeficitRestrictionBinding = new ObservableValuePropertyRestrictionBinding<>(this.lowCarbDayDeficitPercentage);
        lowCarbSurplusDeficitRestrictionBinding.bindRestriction(new NumericRangeDatumRestriction(0, 99));

        ObservableValuePropertyRestrictionBinding<Number> highCarbDayCountRestrictionBinding = new ObservableValuePropertyRestrictionBinding<>(this.highCarbDays);
        highCarbDayCountRestrictionBinding.bindRestriction(new NumericRangeDatumRestriction(1, 6));

        ObservableValuePropertyRestrictionBinding<Number> lowCarbDayCountRestrictionBinding = new ObservableValuePropertyRestrictionBinding<>(this.lowCarbDays);
        lowCarbDayCountRestrictionBinding.bindRestriction(new NumericRangeDatumRestriction(1, 6));

        this.propertyInvariants = new HashMap<>();
        this.propertyInvariants.put(this.mealsPerDay.hashCode(), mealsPerDayRestrictionBinding);
        this.propertyInvariants.put(this.highCarbDaySurplusPercentage.hashCode(), highCarbSurplusDeficitRestrictionBinding);
        this.propertyInvariants.put(this.lowCarbDayDeficitPercentage.hashCode(), lowCarbSurplusDeficitRestrictionBinding);
        this.propertyInvariants.put(this.highCarbDays.hashCode(), highCarbDayCountRestrictionBinding);
        this.propertyInvariants.put(this.lowCarbDays.hashCode(), lowCarbDayCountRestrictionBinding);
    }

    @Override
    public UUID getId() {
        return planId;
    }

    public int getLowCarbDays() {
        return lowCarbDays.get();
    }

    public IntegerProperty lowCarbDaysProperty() {
        return lowCarbDays;
    }

    public void setLowCarbDays(int lowCarbDays) {
        this.lowCarbDays.set(lowCarbDays);
    }

    public int getHighCarbDays() {
        return highCarbDays.get();
    }

    public IntegerProperty highCarbDaysProperty() {
        return highCarbDays;
    }

    public void setHighCarbDays(int highCarbDays) {
        this.highCarbDays.set(highCarbDays);
    }

    @Override
    public BigDecimal getHighCarbDaySurplusPercentage()
    {
        return highCarbDaySurplusPercentage.get();
    }

    @Override
    public ObjectProperty<BigDecimal> highCarbDaySurplusPercentageProperty()
    {
        return highCarbDaySurplusPercentage;
    }

    @Override
    public void setHighCarbDaySurplusPercentage(BigDecimal newPercentage)
    {
        this.highCarbDaySurplusPercentage.set(newPercentage);
    }

    @Override
    public BigDecimal getLowCarbDayDeficitPercentage()
    {
        return lowCarbDayDeficitPercentage.get();
    }

    @Override
    public ObjectProperty<BigDecimal> lowCarbDayDeficitPercentageProperty()
    {
        return lowCarbDayDeficitPercentage;
    }

    @Override
    public void setLowCarbDayDeficitPercentage(BigDecimal newPercentage)
    {
        this.lowCarbDayDeficitPercentage.set(newPercentage);
    }

    public int getMealsPerDay() {
        return mealsPerDay.get();
    }

    public IntegerProperty mealsPerDayProperty() {
        return mealsPerDay;
    }

    public void setMealsPerDay(int mealsPerDay) {
        this.mealsPerDay.set(mealsPerDay);
    }

    public boolean isActive() {
        return isActive.get();
    }

    public BooleanProperty isActiveProperty() {
        return isActive;
    }

    @Override
    public void toggleShouldTimeFats() {
        this.shouldTimeFats.set(!this.shouldTimeFats.get());
    }

    public boolean isShouldTimeFats() {
        return shouldTimeFats.get();
    }

    public BooleanProperty shouldTimeFatsProperty() {
        return shouldTimeFats;
    }

    /**
     * Facilitates a way to initialize lazily-loaded meal configurations for this plan. This ensures
     * the configurations must only be loaded when they are actually needed.
     *
     * @param mealConfigurations The set of meal configurations that belong to this carb cycling plan
     */
    public void initMealConfigurations(Set<CarbCyclingMealConfigurationViewModel> mealConfigurations)
    {
        this.mealConfigurations.addAll(mealConfigurations);
    }

    /**
     * Initializes the list of meal configurations for this plan based upon the
     * specified number of meals per day
     *
     * @throws IllegalStateException If invoked before the number of meals per day has been specified.
     */
    @Override
    public void initMealConfigurations()
    {
        if (mealsPerDay.get() > 0)
        {
            // Meal configs for non-training days
            for (int i = 1; i <= mealsPerDay.get(); i++)
            {
                mealConfigurations.add(new CarbCyclingMealConfigurationViewModel(i, CarbCyclingMealRelation.NONE, false));
            }

            // Meal configs for training days. Starting with the post-workout meal.
            mealConfigurations.add(new CarbCyclingMealConfigurationViewModel(0, CarbCyclingMealRelation.NONE, true));

            for (int i = 1; i < mealsPerDay.get(); i++)
            {
                // Now add configs for before and after the post-workout meal for each remaining meal
                mealConfigurations.add(new CarbCyclingMealConfigurationViewModel(i, CarbCyclingMealRelation.BEFORE, true));
                mealConfigurations.add(new CarbCyclingMealConfigurationViewModel(i, CarbCyclingMealRelation.AFTER, true));
            }
        }
        else
        {
            throw new IllegalStateException("Cannot initialize meal configurations for a carb cycling plan that has not specified the numbers of meals per day.");
        }
    }

    @Override
    public void clearMealConfigurations()
    {
        mealConfigurations.clear();
    }

    @Override
    public boolean hasConfiguredMeals()
    {
        return mealConfigurations.size() > 0;
    }

    @Override
    public Set<CarbCyclingMealConfigurationModel> getMealConfigurations()
    {
        return Collections.unmodifiableSet(mealConfigurations);
    }

    @Override
    public String toString()
    {
        return "CarbCyclingPlanViewModel{" + "planId=" + planId + ", lowCarbDays=" + lowCarbDays + ", highCarbDays=" + highCarbDays + ", highCarbDaySurplusPercentage=" + highCarbDaySurplusPercentage + ", lowCarbDayDeficitPercentage=" + lowCarbDayDeficitPercentage + ", mealsPerDay=" + mealsPerDay + ", isActive=" + isActive + ", shouldTimeFats=" + shouldTimeFats + ", mealConfigurations=" + mealConfigurations + '}';
    }

    @Override
    public ValidationResult validate()
    {
        Set<ValidationResult> propertyInvariantResults = new HashSet<>();
        for(Map.Entry<Integer, PropertyRestrictionBinding> entry : propertyInvariants.entrySet())
        {
            propertyInvariantResults.add(entry.getValue().applyRestrictions());
        }

        ValidationResult validationResult = ValidationResult.flatten(propertyInvariantResults);

        // Ensure that the state is valid before validation meal configs
        if (validationResult.wasSuccessful())
        {
            validationResult = validateMealConfigurations();
        }

        return validationResult;
    }

    @Override
    public ValidationResult validateProperty(int propertyHashcode)
    {
        return propertyInvariants.get(propertyHashcode).applyRestrictions();
    }

    @Override
    public ValidationResult validateMealConfigurations()
    {
        ValidationResult mealConfigsValidationResult = ValidationResult.create(() -> mealConfigurations.size() > 0, "Carb Cycling Plan must have meal configurations");

        if (mealConfigsValidationResult.wasSuccessful())
        {
            // Validate that each individual meal config is valid
            mealConfigsValidationResult = ValidationResult.flatten(mealConfigurations.stream().map(config -> config.validate()).toList());

            if (mealConfigsValidationResult.wasSuccessful())
            {
                /*
                 * If all meal configs are valid then also validate that we have a grand total of a
                 * minimum required amount greater than 0 for both the training and non-training day
                 * configurations.
                 *
                 * While a goal is to allow some meals to include zero carbs if the user so chooses
                 * at least _some_ meals should include carbs.
                 */
                BigDecimal trainingDayMinimumGrandTotal = mealConfigurations
                        .stream()
                        .filter(config -> config.isForTrainingDay())
                        .map(config -> config.getCarbConsumptionMinimumPercent())
                        .reduce(BigDecimal.ZERO, BigDecimal::add);
                BigDecimal nonTrainingDayMinimumGrandTotal = mealConfigurations
                        .stream()
                        .filter(config -> !config.isForTrainingDay())
                        .map(config -> config.getCarbConsumptionMinimumPercent())
                        .reduce(BigDecimal.ZERO, BigDecimal::add);
                mealConfigsValidationResult = mealConfigsValidationResult.and(() -> trainingDayMinimumGrandTotal.compareTo(BigDecimal.ZERO) > 0, "Carb cycling plan must have at least one meal on training days with a non-zero carb consumption percentage")
                        .and(() -> nonTrainingDayMinimumGrandTotal.compareTo(BigDecimal.ZERO) > 0, "Carb cycling plan must have at least one meal on non-training days with a non-zero carb consumption percentage");
            }
        }

        return mealConfigsValidationResult;
    }
}
