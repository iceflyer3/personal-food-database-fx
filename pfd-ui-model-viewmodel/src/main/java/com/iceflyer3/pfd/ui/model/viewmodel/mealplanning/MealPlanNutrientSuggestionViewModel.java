package com.iceflyer3.pfd.ui.model.viewmodel.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.enums.Nutrient;
import com.iceflyer3.pfd.enums.UnitOfMeasure;
import com.iceflyer3.pfd.ui.model.api.mealplanning.MealPlanNutrientSuggestionModel;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

import java.math.BigDecimal;
import java.util.UUID;

public class MealPlanNutrientSuggestionViewModel implements MealPlanNutrientSuggestionModel
{
    private final UUID macroId;
    private final ObjectProperty<Nutrient> nutrient;
    private final ObjectProperty<BigDecimal> suggestedAmount;
    private final ObjectProperty<UnitOfMeasure> unitOfMeasure;

    public MealPlanNutrientSuggestionViewModel(Nutrient nutrient, BigDecimal suggestedAmount, UnitOfMeasure unitOfMeasure)
    {
        this(null, nutrient, suggestedAmount, unitOfMeasure);
    }

    public MealPlanNutrientSuggestionViewModel(UUID macroId, Nutrient nutrient, BigDecimal suggestedAmount, UnitOfMeasure unitOfMeasure) {
        this.macroId = macroId;
        this.nutrient = new SimpleObjectProperty<>(nutrient);
        this.suggestedAmount = new SimpleObjectProperty<>(suggestedAmount);
        this.unitOfMeasure = new SimpleObjectProperty<>(unitOfMeasure);
    }

    public UUID getId() {
        return macroId;
    }

    public Nutrient getNutrient() {
        return nutrient.get();
    }

    public ObjectProperty<Nutrient> nutrientProperty() {
        return nutrient;
    }

    public void setNutrient(Nutrient nutrient) {
        this.nutrient.set(nutrient);
    }

    public BigDecimal getSuggestedAmount() {
        return suggestedAmount.get();
    }

    public ObjectProperty<BigDecimal> suggestedAmountProperty() {
        return suggestedAmount;
    }

    public void setSuggestedAmount(BigDecimal suggestedAmount) {
        this.suggestedAmount.set(suggestedAmount);
    }

    public UnitOfMeasure getUnitOfMeasure()
    {
        return unitOfMeasure.get();
    }

    public ObjectProperty<UnitOfMeasure> unitOfMeasureProperty()
    {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(UnitOfMeasure unitOfMeasure)
    {
        this.unitOfMeasure.set(unitOfMeasure);
    }
}
