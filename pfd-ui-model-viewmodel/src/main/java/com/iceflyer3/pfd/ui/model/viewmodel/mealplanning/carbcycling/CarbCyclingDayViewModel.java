package com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.enums.CarbCyclingDayType;
import com.iceflyer3.pfd.ui.model.api.mealplanning.carbcycling.CarbCyclingDayModel;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanDayViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;

import java.math.BigDecimal;
import java.util.UUID;

public class CarbCyclingDayViewModel extends MealPlanDayViewModel implements CarbCyclingDayModel
{
    private final UUID owningCarbCyclingPlanId;
    private final BooleanProperty isTrainingDay;
    private final ObjectProperty<CarbCyclingDayType> dayType;
    private final ObjectProperty<BigDecimal> requiredCalories;
    private final ObjectProperty<BigDecimal> requiredProtein;
    private final ObjectProperty<BigDecimal> requiredCarbs;
    private final ObjectProperty<BigDecimal> requiredFats;

    public CarbCyclingDayViewModel(UUID dayId, UUID forMealPlanId, UUID forCarbCyclingPlanId, NutritionalSummary nutrientConsumptionSummary)
    {
        super(dayId, forMealPlanId, nutrientConsumptionSummary);
        this.owningCarbCyclingPlanId = forCarbCyclingPlanId;
        this.isTrainingDay = new SimpleBooleanProperty(false);
        this.dayType = new SimpleObjectProperty<>(CarbCyclingDayType.LOW);

        // These values have to be calculated so no defaults are provided
        this.requiredCalories = new SimpleObjectProperty<>();
        this.requiredProtein = new SimpleObjectProperty<>();
        this.requiredCarbs = new SimpleObjectProperty<>();
        this.requiredFats = new SimpleObjectProperty<>();
    }

    public UUID getOwningCarbCyclingPlanId()
    {
        return owningCarbCyclingPlanId;
    }

    @Override
    public CarbCyclingDayType getDayType()
    {
        return dayType.get();
    }

    @Override
    public ObjectProperty<CarbCyclingDayType> dayTypeProperty()
    {
        return dayType;
    }

    @Override
    public void setDayType(CarbCyclingDayType dayType)
    {
        this.dayType.set(dayType);
    }

    @Override
    public boolean isTrainingDay()
    {
        return isTrainingDay.get();
    }

    @Override
    public BooleanProperty isTrainingDayProperty()
    {
        return isTrainingDay;
    }

    @Override
    public void toggleTrainingDay()
    {
        isTrainingDay.set(!isTrainingDay.get());
    }

    @Override
    public BigDecimal getRequiredCalories()
    {
        return requiredCalories.get();
    }

    @Override
    public ObjectProperty<BigDecimal> requiredCaloriesProperty()
    {
        return requiredCalories;
    }

    public void setRequiredCalories(BigDecimal requiredCalories)
    {
        this.requiredCalories.set(requiredCalories);
    }

    @Override
    public BigDecimal getRequiredProtein()
    {
        return requiredProtein.get();
    }

    @Override
    public ObjectProperty<BigDecimal> requiredProteinProperty()
    {
        return requiredProtein;
    }

    public void setRequiredProtein(BigDecimal requiredProtein)
    {
        this.requiredProtein.set(requiredProtein);
    }

    @Override
    public BigDecimal getRequiredCarbs()
    {
        return requiredCarbs.get();
    }

    @Override
    public ObjectProperty<BigDecimal> requiredCarbsProperty()
    {
        return requiredCarbs;
    }

    public void setRequiredCarbs(BigDecimal requiredCarbs)
    {
        this.requiredCarbs.set(requiredCarbs);
    }

    @Override
    public BigDecimal getRequiredFats()
    {
        return requiredFats.get();
    }

    @Override
    public ObjectProperty<BigDecimal> requiredFatsProperty()
    {
        return requiredFats;
    }

    public void setRequiredFats(BigDecimal requiredFats)
    {
        this.requiredFats.set(requiredFats);
    }

    @Override
    public String toString()
    {
        return "CarbCyclingDayViewModel{" + "nutritionalSummary=" + nutritionalSummary + ", owningCarbCyclingPlanId=" + owningCarbCyclingPlanId + ", isTrainingDay=" + isTrainingDay + ", dayType=" + dayType + ", requiredCalories=" + requiredCalories + ", requiredProtein=" + requiredProtein + ", requiredCarbs=" + requiredCarbs + ", requiredFats=" + requiredFats + ", dayId=" + dayId + ", owningMealPlanId=" + owningMealPlanId + '}';
    }
}
