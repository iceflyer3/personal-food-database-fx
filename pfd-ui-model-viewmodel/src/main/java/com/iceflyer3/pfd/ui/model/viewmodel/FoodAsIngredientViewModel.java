package com.iceflyer3.pfd.ui.model.viewmodel;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class FoodAsIngredientViewModel {
    private final StringProperty ingredientOwnerName;
    private final StringProperty ownerType;

    public FoodAsIngredientViewModel(String ingredientOwnerName, String ownerType) {
        this.ingredientOwnerName = new SimpleStringProperty(ingredientOwnerName);
        this.ownerType = new SimpleStringProperty(ownerType);
    }

    public String getIngredientOwnerName() {
        return ingredientOwnerName.get();
    }

    public StringProperty ingredientOwnerNameProperty() {
        return ingredientOwnerName;
    }

    public String getOwnerType() {
        return ownerType.get();
    }

    public StringProperty ownerTypeProperty() {
        return ownerType;
    }
}
