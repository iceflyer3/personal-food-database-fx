package com.iceflyer3.pfd.ui.model.viewmodel.food;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.enums.FoodType;
import com.iceflyer3.pfd.ui.model.api.NutritionFactsReader;
import com.iceflyer3.pfd.ui.model.api.ServingSizeModel;
import com.iceflyer3.pfd.ui.model.api.food.ReadOnlyFoodModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.base.AbstractPropertyBasedNutritionFactsReader;
import com.iceflyer3.pfd.ui.model.viewmodel.ServingSizeViewModel;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * A ReadOnlyFoodModel implementation that transforms a mutable view model instance
 * into a read-only view model instance.
 *
 * This is the view model used by most of the application when it needs access
 * to the data of some food.
 *
 * Although it is very similar to the view model for Simple Foods this model encapsulates
 * the details of a single food for both types of foods (Simple and Complex) and ensures
 * that the data of the food can't be changed outside of the food's respective composition
 * screen.
 */
public class ReadOnlyFoodViewModel extends AbstractPropertyBasedNutritionFactsReader implements ReadOnlyFoodModel
{

    protected UUID foodId;

    // Basic information
    protected ReadOnlyObjectWrapper<FoodType> foodType;
    protected ReadOnlyStringWrapper name;
    protected ReadOnlyStringWrapper brand;
    protected ReadOnlyStringWrapper source;
    protected ReadOnlyListWrapper<ServingSizeModel> servingSizes; // If we want to be able to bind to tables this has to be a list instead of a set. There is no TableView.setItems(Set) function.
    protected ReadOnlyIntegerWrapper glycemicIndex;

    protected ReadOnlyStringWrapper createdUser;
    protected ReadOnlyObjectWrapper<LocalDateTime> createdDate;
    protected ReadOnlyStringWrapper lastModifiedUser;
    protected ReadOnlyObjectWrapper<LocalDateTime> lastModifiedDate;
    
    public ReadOnlyFoodViewModel(SimpleFoodViewModel simpleFood)
    {
        initNutritionalFacts(simpleFood);
        this.foodId = simpleFood.getId();

        // Basic info
        this.name = new ReadOnlyStringWrapper(simpleFood.getName());
        this.brand = new ReadOnlyStringWrapper(simpleFood.getBrand());
        this.source = new ReadOnlyStringWrapper(simpleFood.getSource());
        this.servingSizes = new ReadOnlyListWrapper<>(FXCollections.observableArrayList(simpleFood.getServingSizes()));
        this.createdDate = new ReadOnlyObjectWrapper<>(simpleFood.getCreatedDate());
        this.createdUser = new ReadOnlyStringWrapper(simpleFood.getCreatedUser());
        this.foodType = new ReadOnlyObjectWrapper<>(simpleFood.getFoodType());
        this.glycemicIndex = new ReadOnlyIntegerWrapper(simpleFood.getGlycemicIndex());
        this.lastModifiedUser = new ReadOnlyStringWrapper(simpleFood.getLastModifiedUser());
        this.lastModifiedDate = new ReadOnlyObjectWrapper<>(simpleFood.getLastModifiedDate());
    }

    public ReadOnlyFoodViewModel(ComplexFoodViewModel complexFood)
    {
        initNutritionalFacts(complexFood);
        this.foodId = complexFood.getId();

        // Complex foods don't support all the same fields that simple foods do
        this.glycemicIndex = new ReadOnlyIntegerWrapper();
        this.brand = new ReadOnlyStringWrapper("N/A");
        this.source = new ReadOnlyStringWrapper("N/A");

        // Basic info
        this.name = new ReadOnlyStringWrapper(complexFood.getName());
        this.servingSizes = new ReadOnlyListWrapper<>(FXCollections.observableArrayList(complexFood.getServingSizes()));
        this.createdDate = new ReadOnlyObjectWrapper<>(complexFood.getCreatedDate());
        this.createdUser = new ReadOnlyStringWrapper(complexFood.getCreatedUser());
        this.foodType = new ReadOnlyObjectWrapper<>(FoodType.COMPLEX);

        this.lastModifiedUser = new ReadOnlyStringWrapper(complexFood.getLastModifiedUser());
        this.lastModifiedDate = new ReadOnlyObjectWrapper<>(complexFood.getLastModifiedDate());
    }

    private void initNutritionalFacts(NutritionFactsReader source)
    {
        // Nutritional data
        this.calories = new ReadOnlyObjectWrapper<>(source.getCalories());

        // Macronutrients
        this.protein = new ReadOnlyObjectWrapper<>(source.getProtein());
        this.totalFats = new ReadOnlyObjectWrapper<>(source.getTotalFats());
        this.transFat = new ReadOnlyObjectWrapper<>(source.getTransFat());
        this.monounsaturatedFat = new ReadOnlyObjectWrapper<>(source.getMonounsaturatedFat());
        this.polyunsaturatedFat = new ReadOnlyObjectWrapper<>(source.getPolyunsaturatedFat());
        this.saturatedFat = new ReadOnlyObjectWrapper<>(source.getSaturatedFat());
        this.carbohydrates = new ReadOnlyObjectWrapper<>(source.getCarbohydrates());
        this.sugars = new ReadOnlyObjectWrapper<>(source.getSugars());
        this.fiber = new ReadOnlyObjectWrapper<>(source.getFiber());

        // Micronutrients - Lipids
        this.cholesterol = new ReadOnlyObjectWrapper<>(source.getCholesterol());

        // Micronutrients - Minerals
        this.iron = new ReadOnlyObjectWrapper<>(source.getIron());
        this.manganese = new ReadOnlyObjectWrapper<>(source.getManganese());
        this.copper = new ReadOnlyObjectWrapper<>(source.getCopper());
        this.zinc = new ReadOnlyObjectWrapper<>(source.getZinc());
        this.calcium = new ReadOnlyObjectWrapper<>(source.getCalcium());
        this.magnesium = new ReadOnlyObjectWrapper<>(source.getMagnesium());
        this.phosphorus = new ReadOnlyObjectWrapper<>(source.getPhosphorus());
        this.potassium = new ReadOnlyObjectWrapper<>(source.getPotassium());
        this.sodium  = new ReadOnlyObjectWrapper<>(source.getSodium());
        this.sulfur = new ReadOnlyObjectWrapper<>(source.getSulfur());

        // Micronutrients - Vitamins
        this.vitaminA = new ReadOnlyObjectWrapper<>(source.getVitaminA());
        this.vitaminC = new ReadOnlyObjectWrapper<>(source.getVitaminC());
        this.vitaminK = new ReadOnlyObjectWrapper<>(source.getVitaminK());
        this.vitaminD = new ReadOnlyObjectWrapper<>(source.getVitaminD());
        this.vitaminE = new ReadOnlyObjectWrapper<>(source.getVitaminE());
        this.vitaminB1 = new ReadOnlyObjectWrapper<>(source.getVitaminB1());
        this.vitaminB2 = new ReadOnlyObjectWrapper<>(source.getVitaminB2());
        this.vitaminB3 = new ReadOnlyObjectWrapper<>(source.getVitaminB3());
        this.vitaminB5 = new ReadOnlyObjectWrapper<>(source.getVitaminB5());
        this.vitaminB6 = new ReadOnlyObjectWrapper<>(source.getVitaminB6());
        this.vitaminB7 = new ReadOnlyObjectWrapper<>(source.getVitaminB7());
        this.vitaminB8 = new ReadOnlyObjectWrapper<>(source.getVitaminB8());
        this.vitaminB9 = new ReadOnlyObjectWrapper<>(source.getVitaminB9());
        this.vitaminB12 = new ReadOnlyObjectWrapper<>(source.getVitaminB12());
    }

    @Override
    public UUID getId()
    {
        return foodId;
    }

    @Override
    public FoodType getFoodType() {
        return foodType.get();
    }
    @Override
    public ReadOnlyObjectProperty<FoodType> foodTypeProperty() {
        return foodType.getReadOnlyProperty();
    }

    @Override
    public String getName() {
        return name.get();
    }
    @Override
    public ReadOnlyStringProperty nameProperty() {
        return name.getReadOnlyProperty();
    }

    @Override
    public String getBrand() {
        return brand.get();
    }
    @Override
    public ReadOnlyStringProperty brandProperty() {
        return brand.getReadOnlyProperty();
    }

    @Override
    public String getSource() {
        return source.get();
    }
    @Override
    public ReadOnlyStringProperty sourceProperty() {
        return source.getReadOnlyProperty();
    }

    @Override
    public ObservableList<ServingSizeModel> getServingSizes() {
        // The property is read-only here but we also need to make sure we don't
        // provide access to a mutable list via the property.
        return FXCollections.unmodifiableObservableList(servingSizes.get());
    }

    @Override
    public int getGlycemicIndex() {
        return glycemicIndex.get();
    }
    @Override
    public ReadOnlyIntegerProperty glycemicIndexProperty() {
        return glycemicIndex.getReadOnlyProperty();
    }

    @Override
    public String getCreatedUser() {
        return createdUser.get();
    }
    @Override
    public ReadOnlyStringProperty createdUserProperty() {
        return createdUser.getReadOnlyProperty();
    }

    @Override
    public LocalDateTime getCreatedDate() {
        return createdDate.get();
    }
    @Override
    public ReadOnlyObjectProperty<LocalDateTime> createdDateProperty() {
        return createdDate.getReadOnlyProperty();
    }

    @Override
    public String getLastModifiedUser() {
        return lastModifiedUser.get();
    }
    @Override
    public ReadOnlyStringProperty lastModifiedUserProperty() {
        return lastModifiedUser.getReadOnlyProperty();
    }

    @Override
    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate.get();
    }
    @Override
    public ReadOnlyObjectProperty<LocalDateTime> lastModifiedDateProperty() {
        return lastModifiedDate.getReadOnlyProperty();
    }

    @Override
    public String toString() {
        // This is needed by ControlsFx autocomplete functionality.
        return String.format("%s - %s", name.get(), brand.get());
    }
}
