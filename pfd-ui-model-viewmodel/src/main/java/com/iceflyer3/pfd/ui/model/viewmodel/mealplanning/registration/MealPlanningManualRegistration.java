package com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.registration;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023, 2025 Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.ui.model.api.mealplanning.MealPlanNutrientSuggestionModel;
import com.iceflyer3.pfd.ui.model.api.mealplanning.registration.MealPlanningRegistration;
import com.iceflyer3.pfd.ui.model.api.viewmodel.ValidatingViewModel;
import com.iceflyer3.pfd.validation.NonZeroDatumRestriction;
import com.iceflyer3.pfd.validation.ObservableValuePropertyRestrictionBinding;
import com.iceflyer3.pfd.validation.PropertyRestrictionBinding;
import com.iceflyer3.pfd.validation.ValidationResult;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Encapsulates the required information for manual registration of a meal planning profile
 */
public class MealPlanningManualRegistration implements MealPlanningRegistration, ValidatingViewModel
{
    private final ObjectProperty<BigDecimal> tdeeCalories;
    private final ObjectProperty<BigDecimal> dailyCalories;
    private final ObjectProperty<BigDecimal> proteinAmount;
    private final ObjectProperty<BigDecimal> carbsAmount;
    private final ObjectProperty<BigDecimal> totalFatsAmount;
    private final ObservableList<MealPlanNutrientSuggestionModel> customNutrients;
    private final Map<Integer, PropertyRestrictionBinding> propertyInvariants;

    public MealPlanningManualRegistration()
    {
        tdeeCalories = new SimpleObjectProperty<>();
        dailyCalories = new SimpleObjectProperty<>();
        proteinAmount = new SimpleObjectProperty<>();
        carbsAmount = new SimpleObjectProperty<>();
        totalFatsAmount = new SimpleObjectProperty<>();
        customNutrients = FXCollections.observableArrayList();

        // Property invariants here are simple. All properties must be greater than zero.
        NonZeroDatumRestriction nonZeroDatumRestriction = new NonZeroDatumRestriction();

        ObservableValuePropertyRestrictionBinding<BigDecimal> tdeeCaloriesBindingRestrictions = new ObservableValuePropertyRestrictionBinding<>(tdeeCalories);
        tdeeCaloriesBindingRestrictions.bindRestriction(nonZeroDatumRestriction);

        ObservableValuePropertyRestrictionBinding<BigDecimal> dailyCaloriesBindingRestrictions = new ObservableValuePropertyRestrictionBinding<>(dailyCalories);
        dailyCaloriesBindingRestrictions.bindRestriction(nonZeroDatumRestriction);

        ObservableValuePropertyRestrictionBinding<BigDecimal> proteinBindingRestrictions = new ObservableValuePropertyRestrictionBinding<>(proteinAmount);
        proteinBindingRestrictions.bindRestriction(nonZeroDatumRestriction);

        ObservableValuePropertyRestrictionBinding<BigDecimal> carbohydrateBindingRestrictions = new ObservableValuePropertyRestrictionBinding<>(carbsAmount);
        carbohydrateBindingRestrictions.bindRestriction(nonZeroDatumRestriction);

        ObservableValuePropertyRestrictionBinding<BigDecimal> totalFatsBindingRestrictions = new ObservableValuePropertyRestrictionBinding<>(totalFatsAmount);
        totalFatsBindingRestrictions.bindRestriction(nonZeroDatumRestriction);

        propertyInvariants = Map.ofEntries(
                Map.entry(tdeeCalories.hashCode(), tdeeCaloriesBindingRestrictions),
                Map.entry(dailyCalories.hashCode(), dailyCaloriesBindingRestrictions),
                Map.entry(proteinAmount.hashCode(), proteinBindingRestrictions),
                Map.entry(carbsAmount.hashCode(), carbohydrateBindingRestrictions),
                Map.entry(totalFatsAmount.hashCode(), totalFatsBindingRestrictions)
        );
    }

    public BigDecimal getTdeeCalories()
    {
        return tdeeCalories.get();
    }

    public ObjectProperty<BigDecimal> tdeeCaloriesProperty()
    {
        return tdeeCalories;
    }

    public void setTdeeCalories(BigDecimal tdeeCalories)
    {
        this.tdeeCalories.set(tdeeCalories);
    }

    public BigDecimal getDailyCalories()
    {
        return dailyCalories.get();
    }

    public ObjectProperty<BigDecimal> dailyCaloriesProperty()
    {
        return dailyCalories;
    }

    public void setDailyCalories(BigDecimal dailyCalories)
    {
        this.dailyCalories.set(dailyCalories);
    }

    public BigDecimal getProteinAmount()
    {
        return proteinAmount.get();
    }

    public ObjectProperty<BigDecimal> proteinAmountProperty()
    {
        return proteinAmount;
    }

    public void setProteinAmount(BigDecimal proteinAmount)
    {
        this.proteinAmount.set(proteinAmount);
    }

    public BigDecimal getCarbsAmount()
    {
        return carbsAmount.get();
    }

    public ObjectProperty<BigDecimal> carbsAmountProperty()
    {
        return carbsAmount;
    }

    public void setCarbsAmount(BigDecimal carbsAmount)
    {
        this.carbsAmount.set(carbsAmount);
    }

    public BigDecimal getTotalFatsAmount()
    {
        return totalFatsAmount.get();
    }

    public ObjectProperty<BigDecimal> totalFatsAmountProperty()
    {
        return totalFatsAmount;
    }

    public void setTotalFatsAmount(BigDecimal totalFatsAmount)
    {
        this.totalFatsAmount.set(totalFatsAmount);
    }

    public ObservableList<MealPlanNutrientSuggestionModel> getCustomNutrients()
    {
        return customNutrients;
    }

    @Override
    public ValidationResult validate()
    {
        Set<ValidationResult> propertyInvariantResults = new HashSet<>();
        propertyInvariants.forEach((propertyHashcode, propertyRestrictionBinding) -> propertyInvariantResults.add(propertyRestrictionBinding.applyRestrictions()));
        return ValidationResult.flatten(propertyInvariantResults);
    }

    @Override
    public ValidationResult validateProperty(int propertyHashcode)
    {
        return propertyInvariants.get(propertyHashcode).applyRestrictions();
    }
}
