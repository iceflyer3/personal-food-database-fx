package com.iceflyer3.pfd.ui.model.viewmodel.summary;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.enums.Nutrient;
import com.iceflyer3.pfd.exception.InvalidApplicationStateException;
import com.iceflyer3.pfd.ui.model.api.NutritionalSummaryReporter;
import com.iceflyer3.pfd.util.NumberUtils;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Objects;

/**
 * Caloric, macronutrient, and micronutrient summary information for a given entity
 *
 * Generally used to represent grand total summary information for entities which support
 * ingredients (ex: Complex Foods, Meal Planning Meals, Carb Cycling Meals, Recipes) as
 * derived from those ingredients.
 *
 * Strictly speaking there isn't any reason this class couldn't be used with entities that
 * don't support ingredients. There just wouldn't be much point since those entities are
 * their own summaries and this class would merely serve as an extra wrapper around already
 * accessible data.
 *
 * This class is immutable.
 */
public class NutritionalSummary
{
    private final BigDecimal calories;

    // Macronutrients
    private final BigDecimal protein;
    private final BigDecimal totalFats;
    private final BigDecimal transFat;
    private final BigDecimal monounsaturatedFat;
    private final BigDecimal polyunsaturatedFat;
    private final BigDecimal saturatedFat;
    private final BigDecimal carbohydrates;
    private final BigDecimal sugars;
    private final BigDecimal fiber;

    // Micronutrients - Lipids
    private final BigDecimal cholesterol;

    // Micronutrients - Minerals
    private final BigDecimal iron;
    private final BigDecimal manganese;
    private final BigDecimal copper;
    private final BigDecimal zinc;
    private final BigDecimal calcium;
    private final BigDecimal magnesium;
    private final BigDecimal phosphorus;
    private final BigDecimal potassium;
    private final BigDecimal sodium;
    private final BigDecimal sulfur;

    // Micronutrients - Vitamins
    private final BigDecimal vitaminA;
    private final BigDecimal vitaminC;
    private final BigDecimal vitaminK;
    private final BigDecimal vitaminD;
    private final BigDecimal vitaminE;
    private final BigDecimal vitaminB1;
    private final BigDecimal vitaminB2;
    private final BigDecimal vitaminB3;
    private final BigDecimal vitaminB5;
    private final BigDecimal vitaminB6;
    private final BigDecimal vitaminB7;
    private final BigDecimal vitaminB8;
    private final BigDecimal vitaminB9;
    private final BigDecimal vitaminB12;

    /*
     * Normally, this would accept some manner of configuration object once
     * we pass two or three constructor parameters.
     *
     * Unfortunately said configuration object would share the exact same shape
     * as this object. Unless we split it into groups of macro / micro nutrients.
     * That seems like more work than it is worth though. So an ugly super long
     * parameter list it is.
     */
    public NutritionalSummary(BigDecimal calories,
                              BigDecimal protein,
                              BigDecimal totalFats,
                              BigDecimal transFat,
                              BigDecimal monounsaturatedFat,
                              BigDecimal polyunsaturatedFat,
                              BigDecimal saturatedFat,
                              BigDecimal carbohydrates,
                              BigDecimal sugars,
                              BigDecimal fiber,
                              BigDecimal cholesterol,
                              BigDecimal iron,
                              BigDecimal manganese,
                              BigDecimal copper,
                              BigDecimal zinc,
                              BigDecimal calcium,
                              BigDecimal magnesium,
                              BigDecimal phosphorus,
                              BigDecimal potassium,
                              BigDecimal sodium,
                              BigDecimal sulfur,
                              BigDecimal vitaminA,
                              BigDecimal vitaminC,
                              BigDecimal vitaminK,
                              BigDecimal vitaminD,
                              BigDecimal vitaminE,
                              BigDecimal vitaminB1,
                              BigDecimal vitaminB2,
                              BigDecimal vitaminB3,
                              BigDecimal vitaminB5,
                              BigDecimal vitaminB6,
                              BigDecimal vitaminB7,
                              BigDecimal vitaminB8,
                              BigDecimal vitaminB9,
                              BigDecimal vitaminB12)
    {
        this.calories = NumberUtils.withStandardPrecision(calories);
        this.protein = NumberUtils.withStandardPrecision(protein);
        this.totalFats = NumberUtils.withStandardPrecision(totalFats);
        this.transFat = NumberUtils.withStandardPrecision(transFat);
        this.monounsaturatedFat = NumberUtils.withStandardPrecision(monounsaturatedFat);
        this.polyunsaturatedFat = NumberUtils.withStandardPrecision(polyunsaturatedFat);
        this.saturatedFat = NumberUtils.withStandardPrecision(saturatedFat);
        this.carbohydrates = NumberUtils.withStandardPrecision(carbohydrates);
        this.sugars = NumberUtils.withStandardPrecision(sugars);
        this.fiber = NumberUtils.withStandardPrecision(fiber);
        this.cholesterol = NumberUtils.withStandardPrecision(cholesterol);
        this.iron = NumberUtils.withStandardPrecision(iron);
        this.manganese = NumberUtils.withStandardPrecision(manganese);
        this.copper = NumberUtils.withStandardPrecision(copper);
        this.zinc = NumberUtils.withStandardPrecision(zinc);
        this.calcium = NumberUtils.withStandardPrecision(calcium);
        this.magnesium = NumberUtils.withStandardPrecision(magnesium);
        this.phosphorus = NumberUtils.withStandardPrecision(phosphorus);
        this.potassium = NumberUtils.withStandardPrecision(potassium);
        this.sodium = NumberUtils.withStandardPrecision(sodium);
        this.sulfur = NumberUtils.withStandardPrecision(sulfur);
        this.vitaminA = NumberUtils.withStandardPrecision(vitaminA);
        this.vitaminC = NumberUtils.withStandardPrecision(vitaminC);
        this.vitaminK = NumberUtils.withStandardPrecision(vitaminK);
        this.vitaminD = NumberUtils.withStandardPrecision(vitaminD);
        this.vitaminE = NumberUtils.withStandardPrecision(vitaminE);
        this.vitaminB1 = NumberUtils.withStandardPrecision(vitaminB1);
        this.vitaminB2 = NumberUtils.withStandardPrecision(vitaminB2);
        this.vitaminB3 = NumberUtils.withStandardPrecision(vitaminB3);
        this.vitaminB5 = NumberUtils.withStandardPrecision(vitaminB5);
        this.vitaminB6 = NumberUtils.withStandardPrecision(vitaminB6);
        this.vitaminB7 = NumberUtils.withStandardPrecision(vitaminB7);
        this.vitaminB8 = NumberUtils.withStandardPrecision(vitaminB8);
        this.vitaminB9 = NumberUtils.withStandardPrecision(vitaminB9);
        this.vitaminB12 = NumberUtils.withStandardPrecision(vitaminB12);
    }

    /**
     * Creates a new instance of NutritionalSummaryReporter with all values initialized to zero.
     *
     * @return A NutritionalSummaryReporter whose values are all zero
     */
    public static NutritionalSummary empty()
    {
        return new NutritionalSummary(
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                BigDecimal.ZERO,
                BigDecimal.ZERO
        );
    }

    public static <T extends NutritionalSummaryReporter> NutritionalSummary deriveUpdateFrom(Collection<T> items) throws InvalidApplicationStateException
    {
        return new NutritionalSummary(
                calculateNutrientTotal(Nutrient.CALORIES, items),
                calculateNutrientTotal(Nutrient.PROTEIN, items),
                calculateNutrientTotal(Nutrient.TOTAL_FATS, items),
                calculateNutrientTotal(Nutrient.TRANS_FAT, items),
                calculateNutrientTotal(Nutrient.MONOUNSATURATED_FAT, items),
                calculateNutrientTotal(Nutrient.POLYUNSATURATED_FAT, items),
                calculateNutrientTotal(Nutrient.SATURATED_FAT, items),
                calculateNutrientTotal(Nutrient.CARBOHYDRATES, items),
                calculateNutrientTotal(Nutrient.SUGARS, items),
                calculateNutrientTotal(Nutrient.FIBER, items),
                calculateNutrientTotal(Nutrient.CHOLESTEROL, items),
                calculateNutrientTotal(Nutrient.IRON, items),
                calculateNutrientTotal(Nutrient.MANGANESE, items),
                calculateNutrientTotal(Nutrient.COPPER, items),
                calculateNutrientTotal(Nutrient.ZINC, items),
                calculateNutrientTotal(Nutrient.CALCIUM, items),
                calculateNutrientTotal(Nutrient.MAGNESIUM, items),
                calculateNutrientTotal(Nutrient.PHOSPHORUS, items),
                calculateNutrientTotal(Nutrient.POTASSIUM, items),
                calculateNutrientTotal(Nutrient.SODIUM, items),
                calculateNutrientTotal(Nutrient.SULFUR, items),
                calculateNutrientTotal(Nutrient.VITAMIN_A, items),
                calculateNutrientTotal(Nutrient.VITAMIN_C, items),
                calculateNutrientTotal(Nutrient.VITAMIN_K, items),
                calculateNutrientTotal(Nutrient.VITAMIN_D, items),
                calculateNutrientTotal(Nutrient.VITAMIN_E, items),
                calculateNutrientTotal(Nutrient.VITAMIN_B1, items),
                calculateNutrientTotal(Nutrient.VITAMIN_B2, items),
                calculateNutrientTotal(Nutrient.VITAMIN_B3, items),
                calculateNutrientTotal(Nutrient.VITAMIN_B5, items),
                calculateNutrientTotal(Nutrient.VITAMIN_B6, items),
                calculateNutrientTotal(Nutrient.VITAMIN_B7, items),
                calculateNutrientTotal(Nutrient.VITAMIN_B8, items),
                calculateNutrientTotal(Nutrient.VITAMIN_B9, items),
                calculateNutrientTotal(Nutrient.VITAMIN_B12, items)
        );
    }

    @Override
    public String toString()
    {
        return "NutritionalSummary{" + "calories=" + calories + ", protein=" + protein + ", totalFats=" + totalFats + ", transFat=" + transFat + ", monounsaturatedFat=" + monounsaturatedFat + ", polyunsaturatedFat=" + polyunsaturatedFat + ", saturatedFat=" + saturatedFat + ", carbohydrates=" + carbohydrates + ", sugars=" + sugars + ", fiber=" + fiber + ", cholesterol=" + cholesterol + ", iron=" + iron + ", manganese=" + manganese + ", copper=" + copper + ", zinc=" + zinc + ", calcium=" + calcium + ", magnesium=" + magnesium + ", phosphorus=" + phosphorus + ", potassium=" + potassium + ", sodium=" + sodium + ", sulfur=" + sulfur + ", vitaminA=" + vitaminA + ", vitaminC=" + vitaminC + ", vitaminK=" + vitaminK + ", vitaminD=" + vitaminD + ", vitaminE=" + vitaminE + ", vitaminB1=" + vitaminB1 + ", vitaminB2=" + vitaminB2 + ", vitaminB3=" + vitaminB3 + ", vitaminB5=" + vitaminB5 + ", vitaminB6=" + vitaminB6 + ", vitaminB7=" + vitaminB7 + ", vitaminB8=" + vitaminB8 + ", vitaminB9=" + vitaminB9 + ", vitaminB12=" + vitaminB12 + '}';
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NutritionalSummary that = (NutritionalSummary) o;
        return calories.compareTo(that.calories) == 0 &&
                protein.compareTo(that.protein) == 0 &&
                carbohydrates.compareTo(that.carbohydrates) == 0 &&
                totalFats.compareTo(that.totalFats) == 0 &&
                transFat.compareTo(that.transFat) == 0 &&
                monounsaturatedFat.compareTo(that.monounsaturatedFat) == 0 &&
                polyunsaturatedFat.compareTo(that.polyunsaturatedFat) == 0 &&
                saturatedFat.compareTo(that.saturatedFat) == 0 &&
                sugars.compareTo(that.sugars) == 0 &&
                fiber.compareTo(that.fiber) == 0 &&
                cholesterol.compareTo(that.cholesterol) == 0 &&
                iron.compareTo(that.iron) == 0 &&
                manganese.compareTo(that.manganese) == 0 &&
                copper.compareTo(that.copper) == 0 &&
                zinc.compareTo(that.zinc) == 0 &&
                calcium.compareTo(that.calcium) == 0 &&
                magnesium.compareTo(that.magnesium) == 0 &&
                phosphorus.compareTo(that.phosphorus) == 0 &&
                potassium.compareTo(that.potassium) == 0 &&
                sodium.compareTo(that.sodium) == 0 &&
                sulfur.compareTo(that.sulfur) == 0 &&
                vitaminA.compareTo(that.vitaminA) == 0 &&
                vitaminC.compareTo(that.vitaminA) == 0 &&
                vitaminK.compareTo(that.vitaminK) == 0 &&
                vitaminD.compareTo(that.vitaminD) == 0 &&
                vitaminE.compareTo(that.vitaminE) == 0 &&
                vitaminB1.compareTo(that.vitaminB1) == 0 &&
                vitaminB2.compareTo(that.vitaminB2) == 0 &&
                vitaminB3.compareTo(that.vitaminB3) == 0 &&
                vitaminB5.compareTo(that.vitaminB5) == 0 &&
                vitaminB6.compareTo(that.vitaminB6) == 0 &&
                vitaminB7.compareTo(that.vitaminB7) == 0 &&
                vitaminB8.compareTo(that.vitaminB8) == 0 &&
                vitaminB9.compareTo(that.vitaminB9) == 0 &&
                vitaminB12.compareTo(that.vitaminB12) == 0;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(calories, protein, totalFats, transFat, monounsaturatedFat, polyunsaturatedFat, saturatedFat, carbohydrates, sugars, fiber, cholesterol, iron, manganese, copper, zinc, calcium, magnesium, phosphorus, potassium, sodium, sulfur, vitaminA, vitaminC, vitaminK, vitaminD, vitaminE, vitaminB1, vitaminB2, vitaminB3, vitaminB5, vitaminB6, vitaminB7, vitaminB8, vitaminB9, vitaminB12);
    }

    public BigDecimal getCalories()
    {
        return calories;
    }

    public BigDecimal getProtein()
    {
        return protein;
    }

    public BigDecimal getTotalFats()
    {
        return totalFats;
    }

    public BigDecimal getTransFat()
    {
        return transFat;
    }

    public BigDecimal getMonounsaturatedFat()
    {
        return monounsaturatedFat;
    }

    public BigDecimal getPolyunsaturatedFat()
    {
        return polyunsaturatedFat;
    }

    public BigDecimal getSaturatedFat()
    {
        return saturatedFat;
    }

    public BigDecimal getCarbohydrates()
    {
        return carbohydrates;
    }

    public BigDecimal getSugars()
    {
        return sugars;
    }

    public BigDecimal getFiber()
    {
        return fiber;
    }

    public BigDecimal getCholesterol()
    {
        return cholesterol;
    }

    public BigDecimal getIron()
    {
        return iron;
    }

    public BigDecimal getManganese()
    {
        return manganese;
    }

    public BigDecimal getCopper()
    {
        return copper;
    }

    public BigDecimal getZinc()
    {
        return zinc;
    }

    public BigDecimal getCalcium()
    {
        return calcium;
    }

    public BigDecimal getMagnesium()
    {
        return magnesium;
    }

    public BigDecimal getPhosphorus()
    {
        return phosphorus;
    }

    public BigDecimal getPotassium()
    {
        return potassium;
    }

    public BigDecimal getSodium()
    {
        return sodium;
    }

    public BigDecimal getSulfur()
    {
        return sulfur;
    }

    public BigDecimal getVitaminA()
    {
        return vitaminA;
    }

    public BigDecimal getVitaminC()
    {
        return vitaminC;
    }

    public BigDecimal getVitaminK()
    {
        return vitaminK;
    }

    public BigDecimal getVitaminD()
    {
        return vitaminD;
    }

    public BigDecimal getVitaminE()
    {
        return vitaminE;
    }

    public BigDecimal getVitaminB1()
    {
        return vitaminB1;
    }

    public BigDecimal getVitaminB2()
    {
        return vitaminB2;
    }

    public BigDecimal getVitaminB3()
    {
        return vitaminB3;
    }

    public BigDecimal getVitaminB5()
    {
        return vitaminB5;
    }

    public BigDecimal getVitaminB6()
    {
        return vitaminB6;
    }

    public BigDecimal getVitaminB7()
    {
        return vitaminB7;
    }

    public BigDecimal getVitaminB8()
    {
        return vitaminB8;
    }

    public BigDecimal getVitaminB9()
    {
        return vitaminB9;
    }

    public BigDecimal getVitaminB12()
    {
        return vitaminB12;
    }

    /*
     * A subtle fact to note for these calculations is that we don't have to multiply the
     * ingredients by the number of servings included with that ingredient to get an accurate
     * figure here (as one might expect we should).
     *
     * This is because the IngredientModel is a NutritionalSummaryReporter. Due to this fact the
     * values retrieved from calls to getter functions like getCalories and its ilk already factor
     * in the number of servings included during the calculation of those values.
     */
    private static <T extends NutritionalSummaryReporter> BigDecimal calculateNutrientTotal(Nutrient nutrient, Collection<T> items)
    {
        BigDecimal result = items.stream().map(item -> item.getNutrient(nutrient)).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
        return NumberUtils.withStandardPrecision(result);
    }
}