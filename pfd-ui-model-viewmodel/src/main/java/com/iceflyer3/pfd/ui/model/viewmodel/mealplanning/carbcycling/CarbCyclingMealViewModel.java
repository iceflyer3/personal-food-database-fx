package com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.enums.MealType;
import com.iceflyer3.pfd.ui.model.api.mealplanning.carbcycling.CarbCyclingMealModel;
import com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.MealPlanMealViewModel;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;
import com.iceflyer3.pfd.validation.NonZeroDatumRestriction;
import com.iceflyer3.pfd.validation.ObservableValuePropertyRestrictionBinding;
import com.iceflyer3.pfd.validation.ValidationResult;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;

import java.math.BigDecimal;
import java.util.UUID;

public class CarbCyclingMealViewModel extends MealPlanMealViewModel implements CarbCyclingMealModel {
    private final ObjectProperty<MealType> mealType;
    private final IntegerProperty mealNumber;
    private final ObjectProperty<BigDecimal> carbConsumptionPercentage;
    private final ObjectProperty<BigDecimal> requiredCalories;
    private final ObjectProperty<BigDecimal> requiredProtein;
    private final ObjectProperty<BigDecimal> requiredCarbs;
    private final ObjectProperty<BigDecimal> requiredFats;

    private final ObservableValuePropertyRestrictionBinding<Number> mealNumberInvariant;

    public CarbCyclingMealViewModel(UUID mealId, NutritionalSummary nutritionalSummary)
    {
        super(mealId, nutritionalSummary);
        mealType = new SimpleObjectProperty<>(MealType.POST_WORKOUT);
        mealNumber = new SimpleIntegerProperty(1);
        carbConsumptionPercentage = new SimpleObjectProperty<>();
        requiredCalories = new SimpleObjectProperty<>();
        requiredProtein = new SimpleObjectProperty<>();
        requiredCarbs = new SimpleObjectProperty<>();
        requiredFats = new SimpleObjectProperty<>();

        mealNumberInvariant = new ObservableValuePropertyRestrictionBinding<>(mealNumber);
        mealNumberInvariant.bindRestriction(new NonZeroDatumRestriction());
    }

    @Override
    public ValidationResult validate() {
        /*
         * Required fields and invariants:
         *  - The meal must have a non-zero meal number
         */
        return ValidationResult.flatten(super.validate(), mealNumberInvariant.applyRestrictions());
    }

    @Override
    public ValidationResult validateProperty(int propertyHashcode)
    {
        return mealNumberInvariant.applyRestrictions();
    }

    @Override
    public MealType getMealType() {
        return mealType.get();
    }

    @Override
    public ObjectProperty<MealType> mealTypeProperty() {
        return mealType;
    }

    @Override
    public void setMealType(MealType mealType) {
        this.mealType.set(mealType);
    }

    @Override
    public int getMealNumber() {
        return mealNumber.get();
    }

    @Override
    public IntegerProperty mealNumberProperty() {
        return mealNumber;
    }

    @Override
    public void setMealNumber(int mealNumber) {
        this.mealNumber.set(mealNumber);
    }

    @Override
    public BigDecimal getCarbConsumptionPercentage()
    {
        return carbConsumptionPercentage.get();
    }

    @Override
    public void setCarbConsumptionPercentage(BigDecimal newPercentage)
    {
        carbConsumptionPercentage.set(newPercentage);
    }

    @Override
    public ObjectProperty<BigDecimal> carbConsumptionPercentageProperty()
    {
        return carbConsumptionPercentage;
    }


    @Override
    public BigDecimal getRequiredCalories() {
        return requiredCalories.get();
    }

    public void setRequiredCalories(BigDecimal requiredCalories) {
        this.requiredCalories.set(requiredCalories);
    }

    @Override
    public ObjectProperty<BigDecimal> requiredCaloriesProperty() {
        return requiredCalories;
    }

    @Override
    public BigDecimal getRequiredProtein() {
        return requiredProtein.get();
    }

    public void setRequiredProtein(BigDecimal requiredProtein) {
        this.requiredProtein.set(requiredProtein);
    }

    @Override
    public ObjectProperty<BigDecimal> requiredProteinProperty() {
        return requiredProtein;
    }

    @Override
    public BigDecimal getRequiredCarbs() {
        return requiredCarbs.get();
    }

    public void setRequiredCarbs(BigDecimal requiredCarbs) {
        this.requiredCarbs.set(requiredCarbs);
    }

    @Override
    public ObjectProperty<BigDecimal> requiredCarbsProperty() {
        return requiredCarbs;
    }

    @Override
    public BigDecimal getRequiredFats() {
        return requiredFats.get();
    }

    public void setRequiredFats(BigDecimal requiredFats) {
        this.requiredFats.set(requiredFats);
    }

    @Override
    public ObjectProperty<BigDecimal> requiredFatsProperty() {
        return requiredFats;
    }

    @Override
    public String toString()
    {
        return "CarbCyclingMealViewModel{" + "nutritionalSummary=" + nutritionalSummary + ", mealType=" + mealType + ", mealNumber=" + mealNumber + ", carbConsumptionPercentage=" + carbConsumptionPercentage + ", requiredCalories=" + requiredCalories + ", requiredProtein=" + requiredProtein + ", requiredCarbs=" + requiredCarbs + ", requiredFats=" + requiredFats + ", mealId=" + mealId + ", mealName=" + mealName + ", lastModifiedDate=" + lastModifiedDate + ", ingredients=" + ingredients + '}';
    }
}
