/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.model.viewmodel.mealplanning;

import com.iceflyer3.pfd.enums.WeeklyInterval;
import com.iceflyer3.pfd.ui.model.api.mealplanning.MealPlanCalendarDayModel;

import java.time.LocalDate;
import java.util.UUID;

/**
 * Associates a planned day for either a meal plan or a carb cycling plan with a calendar day on
 * which the meals for the planned day are to be consumed.
 *
 * It contains the details for the planned day as well as the details for how the day should be
 * displayed in the calendar (which include the calendar date and if the day recurs within the
 * week).
 *
 * This class is immutable. 
 */
public class MealPlanCalendarDayViewModel implements MealPlanCalendarDayModel
{
    private final UUID calendarDayId;

    private final MealPlanDayViewModel plannedDay;

    // Calendar display fields
    private final LocalDate calendarDate;
    private final WeeklyInterval weeklyInterval;
    private final int intervalMonthsDuration;
    private final boolean isRecurrence;

    public MealPlanCalendarDayViewModel(UUID calendarDayId,
                                        MealPlanDayViewModel plannedDay,
                                        LocalDate calendarDate,
                                        WeeklyInterval weeklyInterval,
                                        int intervalMonthsDuration,
                                        boolean isRecurrence)
    {
        this.calendarDayId = calendarDayId;
        this.plannedDay = plannedDay;
        this.calendarDate = calendarDate;
        this.weeklyInterval = weeklyInterval;
        this.intervalMonthsDuration = intervalMonthsDuration;
        this.isRecurrence = isRecurrence;
    }

    public UUID getCalendarDayId()
    {
        return calendarDayId;
    }

    public MealPlanDayViewModel getPlannedDay()
    {
        return plannedDay;
    }

    public LocalDate getCalendarDate()
    {
        return calendarDate;
    }

    public WeeklyInterval getWeeklyInterval()
    {
        return weeklyInterval;
    }

    public int getIntervalMonthsDuration()
    {
        return intervalMonthsDuration;
    }

    public boolean isRecurrence()
    {
        return isRecurrence;
    }

    @Override
    public String toString()
    {
        return "MealPlanCalendarDayViewModel{" + "calendarDayId=" + calendarDayId + ", plannedDay=" + plannedDay + ", calendarDate=" + calendarDate + ", weeklyInterval=" + weeklyInterval + ", intervalMonthsDuration=" + intervalMonthsDuration + '}';
    }
}
