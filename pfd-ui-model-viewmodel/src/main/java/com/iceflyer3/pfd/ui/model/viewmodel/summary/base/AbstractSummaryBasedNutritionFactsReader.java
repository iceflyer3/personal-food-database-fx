package com.iceflyer3.pfd.ui.model.viewmodel.summary.base;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.ui.model.api.NutritionFactsReader;
import com.iceflyer3.pfd.ui.model.viewmodel.summary.NutritionalSummary;
import javafx.beans.property.ReadOnlyObjectWrapper;

/**
 * NutritionFactsReader implementation backed by a NutritionalSummary object from which
 * the nutrition facts are pulled.
 *
 * Concrete classes are responsible for setting the NutritionalSummary object and
 * triggering refreshes of the properties from the NutritionalSummary when appropriate.
 */
public abstract class AbstractSummaryBasedNutritionFactsReader extends AbstractPropertyBasedNutritionFactsReader
{
    protected NutritionalSummary nutritionalSummary;

    /**
     * The constructor initializes the properties for use by clients.
     * So it is important that concrete classes call super().
     */
    public AbstractSummaryBasedNutritionFactsReader()
    {
        this.calories = new ReadOnlyObjectWrapper<>();
        this.protein = new ReadOnlyObjectWrapper<>();
        this.carbohydrates = new ReadOnlyObjectWrapper<>();
        this.totalFats = new ReadOnlyObjectWrapper<>();
        this.transFat = new ReadOnlyObjectWrapper<>();
        this.monounsaturatedFat = new ReadOnlyObjectWrapper<>();
        this.polyunsaturatedFat = new ReadOnlyObjectWrapper<>();
        this.saturatedFat = new ReadOnlyObjectWrapper<>();
        this.sugars = new ReadOnlyObjectWrapper<>();
        this.fiber = new ReadOnlyObjectWrapper<>();
        this.cholesterol = new ReadOnlyObjectWrapper<>();
        this.iron = new ReadOnlyObjectWrapper<>();
        this.manganese = new ReadOnlyObjectWrapper<>();
        this.copper = new ReadOnlyObjectWrapper<>();
        this.zinc = new ReadOnlyObjectWrapper<>();
        this.calcium = new ReadOnlyObjectWrapper<>();
        this.magnesium = new ReadOnlyObjectWrapper<>();
        this.phosphorus = new ReadOnlyObjectWrapper<>();
        this.potassium = new ReadOnlyObjectWrapper<>();
        this.sodium = new ReadOnlyObjectWrapper<>();
        this.sulfur = new ReadOnlyObjectWrapper<>();
        this.vitaminA = new ReadOnlyObjectWrapper<>();
        this.vitaminC = new ReadOnlyObjectWrapper<>();
        this.vitaminK = new ReadOnlyObjectWrapper<>();
        this.vitaminD = new ReadOnlyObjectWrapper<>();
        this.vitaminE = new ReadOnlyObjectWrapper<>();
        this.vitaminB1 = new ReadOnlyObjectWrapper<>();
        this.vitaminB2 = new ReadOnlyObjectWrapper<>();
        this.vitaminB3 = new ReadOnlyObjectWrapper<>();
        this.vitaminB5 = new ReadOnlyObjectWrapper<>();
        this.vitaminB6 = new ReadOnlyObjectWrapper<>();
        this.vitaminB7 = new ReadOnlyObjectWrapper<>();
        this.vitaminB8 = new ReadOnlyObjectWrapper<>();
        this.vitaminB9 = new ReadOnlyObjectWrapper<>();
        this.vitaminB12 = new ReadOnlyObjectWrapper<>();
    }

    /**
     * Updates the nutrition JavaFx properties with values from the backing NutritionalSummary
     * object
     */
    protected void refreshNutritionalProperties()
    {
        this.calories.set(nutritionalSummary.getCalories());
        this.protein.set(nutritionalSummary.getProtein());
        this.carbohydrates.set(nutritionalSummary.getCarbohydrates());
        this.totalFats.set(nutritionalSummary.getTotalFats());
        this.transFat.set(nutritionalSummary.getTransFat());
        this.monounsaturatedFat.set(nutritionalSummary.getMonounsaturatedFat());
        this.polyunsaturatedFat.set(nutritionalSummary.getPolyunsaturatedFat());
        this.saturatedFat.set(nutritionalSummary.getSaturatedFat());
        this.sugars.set(nutritionalSummary.getSugars());
        this.fiber.set(nutritionalSummary.getFiber());
        this.cholesterol.set(nutritionalSummary.getCholesterol());
        this.iron.set(nutritionalSummary.getIron());
        this.manganese.set(nutritionalSummary.getManganese());
        this.copper.set(nutritionalSummary.getCopper());
        this.zinc.set(nutritionalSummary.getZinc());
        this.calcium.set(nutritionalSummary.getCalcium());
        this.magnesium.set(nutritionalSummary.getMagnesium());
        this.phosphorus.set(nutritionalSummary.getPhosphorus());
        this.potassium.set(nutritionalSummary.getPotassium());
        this.sodium.set(nutritionalSummary.getSodium());
        this.sulfur.set(nutritionalSummary.getSulfur());
        this.vitaminA.set(nutritionalSummary.getVitaminA());
        this.vitaminC.set(nutritionalSummary.getVitaminC());
        this.vitaminK.set(nutritionalSummary.getVitaminK());
        this.vitaminD.set(nutritionalSummary.getVitaminD());
        this.vitaminE.set(nutritionalSummary.getVitaminE());
        this.vitaminB1.set(nutritionalSummary.getVitaminB1());
        this.vitaminB2.set(nutritionalSummary.getVitaminB2());
        this.vitaminB3.set(nutritionalSummary.getVitaminB3());
        this.vitaminB5.set(nutritionalSummary.getVitaminB5());
        this.vitaminB6.set(nutritionalSummary.getVitaminB6());
        this.vitaminB7.set(nutritionalSummary.getVitaminB7());
        this.vitaminB8.set(nutritionalSummary.getVitaminB8());
        this.vitaminB9.set(nutritionalSummary.getVitaminB9());
        this.vitaminB12.set(nutritionalSummary.getVitaminB12());
    }
}
