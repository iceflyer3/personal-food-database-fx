/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023, 2025 Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


module pfd.ui.model.viewmodel {
    requires javafx.base;

    requires pfd.base;
    requires pfd.ui.model.api;
    requires org.slf4j;

    opens com.iceflyer3.pfd.ui.model.viewmodel;
    opens com.iceflyer3.pfd.ui.model.viewmodel.summary.base;

    // Required for use with property bindings in JavaFX controls
    opens com.iceflyer3.pfd.ui.model.viewmodel.mealplanning to javafx.base;

    exports com.iceflyer3.pfd.ui.model.viewmodel.food to
            pfd.data,
            pfd.ui,
            pfd.ui.model.proxy;

    exports com.iceflyer3.pfd.ui.model.viewmodel.recipe to
            pfd.data,
            pfd.ui,
            pfd.ui.model.proxy;

    exports com.iceflyer3.pfd.ui.model.viewmodel.mealplanning to
            pfd.data,
            pfd.ui,
            pfd.ui.model.proxy;

    exports com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.registration to
            pfd.data,
            pfd.ui,
            pfd.ui.model.proxy;

    exports com.iceflyer3.pfd.ui.model.viewmodel.mealplanning.carbcycling to
            pfd.data,
            pfd.ui,
            pfd.ui.model.proxy;

    exports com.iceflyer3.pfd.ui.model.viewmodel.summary to
            pfd.data,
            pfd.ui,
            pfd.ui.model.proxy;

    exports com.iceflyer3.pfd.ui.model.viewmodel to
            pfd.data,
            pfd.ui,
            pfd.ui.model.api,
            pfd.ui.model.proxy;
}