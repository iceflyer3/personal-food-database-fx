/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.user;

/**
 * Convenience singleton that provides application-wide access to the currently
 * selected / "logged-in" user profile.
 */
public class UserAuthentication
{
    private static UserAuthentication instance;

    private AuthenticatedUser authenticatedUser;

    private UserAuthentication() { }

    public static UserAuthentication instance()
    {
        if (instance == null)
        {
            instance = new UserAuthentication();
        }

        return instance;
    }

    public AuthenticatedUser getAuthenticatedUser()
    {
        return authenticatedUser;
    }

    public void setAuthenticatedUser(AuthenticatedUser authenticatedUser)
    {
        this.authenticatedUser = authenticatedUser;
    }

    public void clearAuthenticatedUser()
    {
        this.authenticatedUser = null;
    }
}
