/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.user;

import java.util.UUID;

/**
 * Represents the currently selected / active / "logged-in" user profile.
 *
 * At the time of writing there isn't any real "authentication" to perform as user profiles aren't
 * protected by a password. This class is named such in the interest of potential future proofing
 * were it to be decided that this should change in the future.
 */
public record AuthenticatedUser(UUID userId, String username) { }
