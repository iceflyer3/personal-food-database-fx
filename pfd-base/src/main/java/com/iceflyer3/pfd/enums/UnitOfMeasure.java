/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023, 2025 Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.enums;

public enum UnitOfMeasure {
    CALORIE("Calorie", "kcal"),
    TEASPOON("Teaspoon", "tsp"),
    TABLESPOON("Tablespoon", "Tbsp"),
    FLUID_OUNCE("Fluid Ounce", "fl oz"),
    GILL("Gill", "gi"),
    CUP("Cup", "c"),
    PINT("Pint", "pt"),
    QUART("Quart", "qp"),
    GALLON("Gallon", "gal"),
    PECK("Peck", "pk"),
    BUSHEL("Bushel", "bu"),
    OUNCE("Ounce", "oz"),
    POUND("Pound", "lb"),
    GRAM("Gram", "g"),
    KILOGRAM("Kilogram", "kg"),
    MILLIGRAM("Milligram", "mg"),
    MICROGRAM("Microgram", "mcg/ug"),
    LITER("Liter", "L"),
    MILLILITER("Milliliter", "ml"),
    INCHES("Inches", "in"),
    CENTIMETERS("Centimeters", "cm");


    private final String name;
    private final String abbreviation;

    UnitOfMeasure(String name, String abbreviation)
    {
        this.name = name;
        this.abbreviation = abbreviation;
    }

    public String getName()
    {
        return name;
    }

    public String getAbbreviation()
    {
        return abbreviation;
    }
}
