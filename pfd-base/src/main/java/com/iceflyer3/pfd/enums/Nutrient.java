package com.iceflyer3.pfd.enums;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

public enum Nutrient
{
    CALORIES("Calories"), // Yeah, calories aren't technically a nutrient, but they're going here anyway.
    /*
     *********************************************************
     * MACRONUTRIENTS
     *********************************************************
     */
    PROTEIN("Protein"),
    CARBOHYDRATES("Carbohydrates"),
    SUGARS("Sugars"),
    FIBER("Fiber"),
    TOTAL_FATS("Total Fats"),
    SATURATED_FAT("Saturated Fat"),
    TRANS_FAT("Trans Fat"),
    MONOUNSATURATED_FAT("Monounsaturated Fat"),
    POLYUNSATURATED_FAT("Polyunsaturated Fat"),
    /*
     *********************************************************
     * MICRONUTRIENTS
     *********************************************************
     */
    CHOLESTEROL("Cholesterol"),
    // Minerals
    CALCIUM("Calcium"),
    COPPER("Copper"),
    IRON("Iron"),
    MAGNESIUM("Magnesium"),
    MANGANESE("Manganese"),
    PHOSPHORUS("Phosphorus"),
    POTASSIUM("Potassium"),
    SODIUM("Sodium"),
    SULFUR("Sulfur"),
    ZINC("Zinc"),
    // Vitamins
    VITAMIN_A("Vitamin A"),
    VITAMIN_C("Vitamin C"),
    VITAMIN_K("Vitamin K"),
    VITAMIN_D("Vitamin D"),
    VITAMIN_E("Vitamin E"),
    VITAMIN_B1("Vitamin B1"),
    VITAMIN_B2("Vitamin B2"),
    VITAMIN_B3("Vitamin B3"),
    VITAMIN_B5("Vitamin B5"),
    VITAMIN_B6("Vitamin B6"),
    VITAMIN_B7("Vitamin B7"),
    VITAMIN_B8("Vitamin B8"),
    VITAMIN_B9("Vitamin B9"),
    VITAMIN_B12("Vitamin B12");

    private final String name;

    Nutrient(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public static Nutrient[] macronutrients()
    {
        return new Nutrient[] { CALORIES, PROTEIN, CARBOHYDRATES, TOTAL_FATS };
    }
}
