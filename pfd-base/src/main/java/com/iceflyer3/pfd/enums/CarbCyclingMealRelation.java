/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.enums;

/**
 * The relation that a meal has to the post-workout meal when defining the meal restrictions while setting
 * up a carb-cycling plan.
 *
 * Other meals are based around the post-workout meal because it is typically the largest meal of the day.
 * Thus, it has the biggest impact on planning of the other meals.
 */
public enum CarbCyclingMealRelation
{
    NONE,
    BEFORE,
    AFTER
}
