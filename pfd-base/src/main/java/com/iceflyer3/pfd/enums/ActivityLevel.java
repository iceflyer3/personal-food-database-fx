/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.enums;

public enum ActivityLevel {
    UNUSED("Unused"),
    SEDENTARY("Sedentary"),
    LIGHT("Light"),
    MODERATE("Moderate"),
    HIGH("High"),
    EXTREME("Extreme");

    private final String name;

    ActivityLevel(String name)
    {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    /**
     * Returns an array of the valid values for user selection via the GUI
     * @return An array of the valid values for user selection
     */
    public static ActivityLevel[] validActivityLevels()
    {
        return new ActivityLevel[] { SEDENTARY, LIGHT, MODERATE, HIGH, EXTREME };
    }
}
