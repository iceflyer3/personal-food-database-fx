/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.constants;

public final class CssConstants {

    private CssConstants() {}

    // FONTS
    public static final String FONT_AWESOME_SOLID = "fas";
    public static final String FONT_AWESOME_REGULAR = "far";
    public static final String FONT_AWESOME_BRANDS_REGULAR = "fab";

    // GENERAL STYLES
    public static final String CLASS_CLICKABLE = "clickable";
    public static final String CLASS_REQUIRED = "required";
    public static final String CLASS_UNDERLINE = "underline";
    public static final String CLASS_BOLD = "bold";
    public static final String CLASS_ITALICIZE = "italicize";

    public static final String CLASS_BACKGROUND_SHADOW_LIGHT = "background-shadow-light";

    // BORDERS
    public static final String CLASS_BORDER_WHITE = "white-border";
    public static final String CLASS_BORDER_MUTED = "muted-border";
    public static final String CLASS_BORDER_SECONDARY_ACCENT = "secondary-accent-border";

    // BUTTON STYLES
    public static final String CLASS_BUTTON_BASE = "btn";
    public static final String CLASS_BUTTON_PRIMARY = "btn-primary-action";
    public static final String CLASS_BUTTON_SECONDARY = "btn-secondary-action";
    public static final String CLASS_BUTTON_TERTIARY = "btn-tertiary-action";
    public static final String CLASS_BUTTON_DANGER = "btn-danger-action";

    // TEXT STYLES
    public static final String CLASS_TEXT_PRIMARY = "text-primary-action";
    public static final String CLASS_TEXT_SECONDARY = "text-secondary-action";
    public static final String CLASS_TEXT_TERTIARY = "text-tertiary-action";
    public static final String CLASS_TEXT_DANGER = "text-danger-action";

    public static final String CLASS_TEXT_HEADLINE = "headline-text";
    public static final String CLASS_TEXT_SUB_HEADER = "sub-header-text";

    public static final String CLASS_TEXT_MUTED = "muted-text";

    public static final String CLASS_TEXT_TABLE_CENTER_RIGHT = "table-cell-text-center-right";

}
