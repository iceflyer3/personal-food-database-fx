/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


package com.iceflyer3.pfd.constants;

public final class Licenses
{
    private Licenses() { }

    public static final String APACHE_V2 = "Apache License - Version 2.0";
    public static final String BSD_THREE_CLAUSE = "BSD 3-Clause License";
    public static final String GPL_V2 = "The GNU General Public License - Version 2";
    public static final String GPL_V3 = "The GNU General Public License - Version 3";
    public static final String LGPL_V21 = "The GNU Lesser General Public License - Version 2.1";
    public static final String MPL_V2 = "Mozilla Public License - Version 2.0";
}
