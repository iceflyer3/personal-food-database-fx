/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.constants;

public final class SceneSets {

    private SceneSets() {}

    public static final String APP_ROOT = "application-root";
    public static final String APP_POPOUT = "popout-root";

    public static final String PROFILE_SELECT = "profile-select";
    public static final String HOME = "home-screen";
    public static final String LANDING_SCREEN = "landing-screen";
    public static final String ABOUT_SCREEN = "about-screen";

    // Foods Section
    public static final String VIEW_FOODS = "view-foods";
    public static final String VIEW_FOOD = "view-food";
    public static final String COMPOSE_SIMPLE_FOOD = "compose-simple-food";
    public static final String COMPOSE_COMPLEX_FOOD = "compose-complex-food";

    // Meal Planning Section
    public static final String MEAL_PLANNING_CALENDAR = "meal-planning-calendar";
    public static final String MEAL_PLANNING_OVERVIEW = "meal-planning-overview";
    public static final String CARB_CYCLING_PLAN_OVERVIEW = "carb-cycling-plan-overview";

    public static final String MEAL_PLANNING_COMPOSE_DAY = "compose-meal-plan-day";
    public static final String CARB_CYCLING_COMPOSE_DAY = "compose-carb-cycling-day";

    public static final String MEAL_PLANNING_COMPOSE_MEAL = "compose-meal-plan-meal";
    public static final String CARB_CYCLING_COMPOSE_MEAL = "compose-carb-cycling-meal";

    public static final String MEAL_PLANNING_VIEW_MEAL = "view-meal-plan-meal";
    public static final String CARB_CYCLING_VIEW_MEAL = "view-carb-cycling-meal";

    // Recipes
    public static final String RECIPES_OVERVIEW = "view-all-recipes";
    public static final String VIEW_RECIPE = "view-recipe";
    public static final String COMPOSE_RECIPE = "compose-recipe";

    // Settings
    public static final String SETTINGS_SCREEN = "settings-screen";
    public static final String SETTINGS_IMPORT_DATA_TAB = "import-data-tab";
    public static final String SETTINGS_EXPORT_DATA_TAB = "export-data-tab";
    public static final String SETTINGS_MEAL_PLANNING_GENERAL_TAB = "meal-planning-general-tab";
    public static final String SETTINGS_MEAL_PLANNING_MANAGE_PLANS_TAB = "meal-planning-manage-plans-tab";

    // Dialogs
    public static final String DIALOG_NAME_PROMPT = "name-prompt-dialog";
    public static final String DIALOG_CONFIRM_ACTION = "confirm-action-dialog";
    public static final String DIALOG_LOADING_NOTIFICATION = "loading-notification-dialog";
    public static final String DIALOG_FATAL_ERROR = "fatal-error-dialog";
    public static final String DIALOG_DISCLAIMER = "disclaimer-dialog";
    public static final String DIALOG_STEPPER_ADAPTER = "stepper-adapter-dialog";

    public static final String DIALOG_NEW_FOOD_TYPE = "compose-food-food-type-dialog";
    public static final String DIALOG_VIEW_FOODS_OPTIONS = "view-foods-table-options-dialog";
    public static final String DIALOG_VIEW_FOODS_INVALID_FOOD_DELETION = "food-used-as-ingredient-invalid-deletion-dialog";

    public static final String DIALOG_MEAL_PLANNING_REGISTRATION = "meal-planning-register-user-dialog";

    public static final String DIALOG_MEAL_PLAN_CALENDAR_ADD_ENTRY = "associate-meal-plan-calendar-days-dialog";
    public static final String DIALOG_MEAL_PLAN_CALENDAR_ENTRY_DETAILS = "meal-plan-calendar-entry-details-dialog";
    public static final String DIALOG_MEAL_PLAN_CALENDAR_GROCERY_LIST = "meal-plan-calendar-grocery-list-dialog";

    public static final String DIALOG_CREATE_MEAL_PLAN_DAY = "create-meal-plan-day-dialog";
    public static final String DIALOG_CREATE_CARB_CYCLING_PLAN_DAY = "create-carb-cycling-day-dialog";

    public static final String DIALOG_CREATE_MEAL_PLAN_MEAL = "create-meal-plan-meal-dialog";
    public static final String DIALOG_CREATE_CARB_CYCLING_PLAN_MEAL = "create-carb-cycling-meal-dialog";
    
    public static final String DIALOG_CREATE_CARB_CYCLING_PLAN = "create-carb-cycling-plan-dialog";
    public static final String DIALOG_CARB_CYCLING_MEAL_CONFIGURATION = "create-carb-cycling-plan-meal-configuration-dialog";

    public static final String DIALOG_CREATE_RECIPE_STEP = "create-recipe-step-dialog";

}
