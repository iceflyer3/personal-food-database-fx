package com.iceflyer3.pfd.constants;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

public final class ApplicationSections
{

    private ApplicationSections() {}

    // Home does not have its own sub-folder.
    // It is root-level and contains only the landing screen.
    public static final String HOME = "";

    public static final String FOOD_DATABASE = "foods";
    public static final String MEAL_PLANNING = "meal-planning";
    public static final String RECIPES = "recipes";
    public static final String SETTINGS = "settings";
    public static final String ABOUT = "about";
}
