/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.constants;

public final class QueryConstants {

    private QueryConstants() {}

    ///////////////////////////////////////////////////////////////////
    //
    //                           USER QUERIES
    //
    ///////////////////////////////////////////////////////////////////
    public static final String QUERY_USER_FIND_ALL = "User.findAll";
    public static final String QUERY_USER_BY_USERNAME = "User.findByUsername";

    ///////////////////////////////////////////////////////////////////
    //
    //                           FOOD SECTION QUERIES
    //
    ///////////////////////////////////////////////////////////////////
    public static final String QUERY_FOOD_FIND_ALL_WITH_IDS = "Food.Metadata.findAll";
    public static final String QUERY_FOOD_FIND_ALL_SIMPLE = "Food.Simple.findAllSimpleFood";
    public static final String QUERY_FOOD_FIND_ALL_SIMPLE_WITH_IDS = "Food.Simple.findAllSimpleFoodWithIds";
    public static final String QUERY_FOOD_FIND_SIMPLE_FOR_USER = "Food.Simple.findSimpleFoodsForUser";
    public static final String QUERY_FOOD_FIND_SIMPLE_BY_NAME_AND_BRAND = "Food.Simple.findSimpleFoodByNameAndBrand";
    public static final String QUERY_FOOD_FIND_ALL_COMPLEX = "Food.Complex.findAllComplexFood";
    public static final String QUERY_FOOD_FIND_COMPLEX_FOR_USER = "Food.Complex.findComplexFoodsForUser";
    public static final String QUERY_FOOD_FIND_COMPLEX_BY_NAME_FOR_USER = "Food.Complex.findComplexFoodByName";
    public static final String QUERY_FOOD_FIND_COMPLEX_FOOD_INGREDIENTS = "Food.Complex.findComplexFoodIngredients";
    public static final String QUERY_FOOD_FIND_FOOD_AS_INGREDIENT = "Food.findFoodUsedAsIngredient";
    public static final String QUERY_FOOD_CALCULATE_COMPLEX_FOOD_NUTRITIONAL_SUMMARY = "Food.Complex.calculateComplexFoodNutritionalSummary";

    ///////////////////////////////////////////////////////////////////
    //
    //                           MEAL PLANNING QUERIES
    //
    ///////////////////////////////////////////////////////////////////

    // ****************************************************************
//                              Meal Plans
    // ****************************************************************
    public static final String QUERY_MEAL_PLANNING_FIND_ALL_PLANS = "MealPlanning.Plan.findAllPlans";
    public static final String QUERY_MEAL_PLANNING_FIND_PLANS_FOR_USER = "MealPlanning.Plan.findPlansForUser";
    public static final String QUERY_MEAL_PLANNING_FIND_ACTIVE_MEAL_PLAN = "MealPlanning.Plan.findActivePlan";

    // ****************************************************************
    //                     Meal Plan Macro Suggestions
    // ****************************************************************
    public static final String QUERY_MEAL_PLANNING_MACRO_SUGGESTIONS_FIND_FOR_USER = "MealPlanMacroSuggestions.findForUser";
    public static final String QUERY_MEAL_PLANNING_MACRO_SUGGESTIONS_REMOVE_ALL_FOR_PLAN = "MealPlanMacroSuggestions.removeAllForMealPlan";

    // ****************************************************************
    //                      Meal Plan Calendar Days
    // ****************************************************************
    public static final String QUERY_MEAL_PLANNING_FIND_PLANNED_CALENDAR_DAYS_FOR_USER = "MealPlanning.Calendar.findDaysForUser";
    public static final String QUERY_MEAL_PLANNING_FIND_PLANNED_CARB_CYCLING_CALENDAR_DAYS_FOR_USER = "MealPlanning.Calendar.findCarbCyclingDaysForUser";
    public static final String QUERY_MEAL_PLANNING_FIND_ALL_CALENDAR_DAYS_FOR_USER = "MealPlanning.Calendar.findAllDaysForUser";
    public static final String QUERY_MEAL_PLANNING_FIND_CALENDAR_DAYS_IN_RANGE = "MealPlanning.Calendar.findDaysInRange";

    // ****************************************************************
    //                              Days
    // ****************************************************************
    public static final String QUERY_MEAL_PLANNING_FIND_ALL_DAYS_FOR_USER = "MealPlanning.Day.findAllDaysForUser";
    public static final String QUERY_MEAL_PLANNING_FIND_DAYS_FOR_MEAL_PLAN = "MealPlanning.Day.findAllDaysForPlan";
    public static final String QUERY_MEAL_PLANNING_REMOVE_DAYS_FOR_MEAL_PLAN = "MealPlanning.Day.removeAllDaysForMealPlan";

    public static final String QUERY_MEAL_PLANNING_CARB_CYCLING_FIND_DAYS_FOR_USER = "MealPlanning.Day.CarbCycling.findDaysForUser";
    public static final String QUERY_MEAL_PLANNING_CARB_CYCLING_FIND_DAY_FOR_ID = "MealPlanning.Day.CarbCycling.findDayById";
    public static final String QUERY_MEAL_PLANNING_REMOVE_CARB_CYCLING_DAYS_FOR_MEAL_PLAN = "MealPlanning.Day.CarbCycling.removeAllDaysForMealPlan";
    public static final String QUERY_MEAL_PLANNING_FIND_CARB_CYCLING_DAYS_FOR_PLAN = "MealPlanning.Day.CarbCycling.findAllDaysForPlan";

    public static final String QUERY_MEAL_PLANNING_NUTRITIONAL_SUMMARY_FOR_DAY = "MealPlanning.Day.calculateNutritionalSummariesForDay";
    public static final String QUERY_MEAL_PLANNING_NUTRITIONAL_SUMMARIES_FOR_DAYS_FOR_MEAL_PLAN = "MealPlanning.Day.calculateNutritionalSummariesForDaysForPlan";
    public static final String QUERY_MEAL_PLANNING_NUTRITIONAL_SUMMARY_FOR_CARB_CYCLING_DAY = "MealPlanning.Day.CarbCycling.calculateNutritionalSummaryForDay";
    public static final String QUERY_MEAL_PLANNING_NUTRITIONAL_SUMMARIES_FOR_CARB_CYCLING_DAYS_FOR_PLAN = "MealPlanning.Day.CarbCycling.calculateNutritionalSummariesForDaysForPlan";

    // ****************************************************************
    //                              Meals
    // ****************************************************************
    public static final String QUERY_MEAL_PLANNING_FIND_ALL_MEALS_FOR_USER = "MealPlanning.Meal.findAllMealsForUser";
    public static final String QUERY_MEAL_PLANNING_FIND_MEALS_FOR_DAY = "MealPlanning.Meal.findAllMealsForDay";
    public static final String QUERY_MEAL_PLANNING_FIND_CARB_CYCLING_MEAL_FOR_DAY_BY_MEAL_NUMBER = "MealPlanning.Meal.findCarbCyclingMealByNumberForDay";
    public static final String QUERY_MEAL_PLANNING_REMOVE_MEALS_FOR_DAY = "MealPlanning.Meal.removeAllMealsForDay";

    public static final String QUERY_MEAL_PLANNING_CARB_CYCLING_FIND_MEALS_FOR_USER = "MealPlanning.Meal.CarbCycling.findMealsForUser";
    public static final String QUERY_MEAL_PLANNING_CARB_CYCLING_FIND_MEAL_FOR_ID = "MealPlanning.Meal.CarbCycling.findMealById";
    public static final String QUERY_MEAL_PLANNING_REMOVE_CARB_CYCLING_MEALS_FOR_DAY = "MealPlanning.Meal.CarbCycling.removeAllMealsForDay";
    public static final String QUERY_MEAL_PLANNING_CARB_CYCLING_FIND_MEALS_FOR_DAY = "MealPlanning.Meal.CarbCycling.findAllMealsForDay";

    public static final String QUERY_MEAL_PLANNING_NUTRITIONAL_SUMMARY_FOR_MEAL = "MealPlanning.Meal.calculateNutritionalSummariesForMeal";
    public static final String QUERY_MEAL_PLANNING_NUTRITIONAL_SUMMARIES_FOR_MEALS_FOR_DAY = "MealPlanning.Meal.calculateNutritionalSummariesForMealsForDay";
    public static final String QUERY_MEAL_PLANNING_NUTRITIONAL_SUMMARY_FOR_CARB_CYCLING_MEAL = "MealPlanning.CarbCycling.Meal.calculateNutritionalSummariesForMeal";
    public static final String QUERY_MEAL_PLANNING_NUTRITIONAL_SUMMARIES_FOR_CARB_CYCLING_MEALS_FOR_DAY = "MealPlanning.CarbCycling.Meal.calculateNutritionalSummariesForMealsForDay";

    // ****************************************************************
    //                              Ingredients
    // ****************************************************************
    public static final String QUERY_MEAL_PLANNING_FIND_MEAL_INGREDIENTS_FOR_USER = "MealPlanning.Meal.Ingredient.findIngredientsForUser";
    public static final String QUERY_MEAL_PLANNING_FIND_CARB_CYCLING_MEAL_INGREDIENTS_FOR_USER = "MealPlanning.Meal.CarbCycling.Ingredient.findIngredientsForUser";
    public static final String QUERY_MEAL_PLANNING_FIND_INGREDIENTS_FOR_MEAL = "MealPlanning.Meal.Ingredient.findIngredientsForMeal";
    public static final String QUERY_MEAL_PLANNING_REMOVE_INGREDIENTS_FOR_MEAL = "MealPlanning.Meal.Ingredient.removeIngredientsForMeal";

    // ****************************************************************
    //                         Carb Cycling plans
    // ****************************************************************
    public static final String QUERY_MEAL_PLANNING_FIND_CARB_CYCLING_PLANS_FOR_USER = "CarbCycling.Plan.findCarbCyclingPlansForUser";
    public static final String QUERY_MEAL_PLANNING_FIND_CARB_CYCLING_PLANS_FOR_MEAL_PLAN = "CarbCycling.Plan.findAllPlansForMealPlan";
    public static final String QUERY_MEAL_PLANNING_FIND_ACTIVE_CARB_CYCLING_PLAN = "CarbCycling.Plan.findActivePlan";

    // ****************************************************************
    //                  Carb Cycling meal configurations
    // ****************************************************************
    public static final String QUERY_MEAL_PLANNING_FIND_MEAL_CONFIGURATIONS_FOR_USER = "CarbCycling.Plan.findMealConfigurationsForUser";
    public static final String QUERY_MEAL_PLANNING_FIND_MEAL_CONFIGURATIONS_FOR_CARB_CYCLING_PLAN = "CarbCycling.Plan.findMealConfigurationsForCarbCyclingPlan";

    ///////////////////////////////////////////////////////////////////
    //
    //                           RECIPES
    //
    ///////////////////////////////////////////////////////////////////
    public static final String QUERY_RECIPE_FIND_ALL = "Recipe.findAll";
    public static final String QUERY_RECIPE_FIND_BY_NAME = "Recipe.findByName";
    public static final String QUERY_RECIPE_FIND_ALL_FOR_USER = "Recipe.findAllForUser";

    public static final String QUERY_RECIPE_FIND_SERVING_SIZES_FOR_USER = "Recipe.getAllServingSizesForUser";
    public static final String QUERY_RECIPE_FIND_SERVING_SIZES_FOR_RECIPE = "Recipe.getAllServingSizesForRecipe";

    public static final String QUERY_RECIPE_FIND_INGREDIENTS_FOR_USER = "Recipe.getAllIngredientsForUser";
    public static final String QUERY_RECIPE_FIND_INGREDIENTS_FOR_RECIPE = "Recipe.getAllIngredientsForRecipe";

    public static final String QUERY_RECIPE_FIND_STEPS_FOR_USER = "Recipe.getAllStepsForUser";
    public static final String QUERY_RECIPE_FIND_STEPS_FOR_RECIPE = "Recipe.getAllStepsForRecipe";

    public static final String QUERY_RECIPE_FIND_STEP_INGREDIENTS_FOR_USER = "Recipe.getAllStepIngredientsForUser";
    public static final String QUERY_RECIPE_FIND_STEP_INGREDIENTS_FOR_STEP_FOR_RECIPE = "Recipe.getAllStepIngredientsForStepForRecipe";

    public static final String QUERY_RECIPE_CALCULATE_ALL_NUTRITIONAL_SUMMARIES = "Recipe.calculateAllRecipeNutritionalSummaries";
    public static final String QUERY_RECIPE_CALCULATE_RECIPE_NUTRITIONAL_SUMMARY = "Recipe.calculateRecipeNutritionalSummary";

    ///////////////////////////////////////////////////////////////////
    //
    //                           PARAMETERS
    //
    ///////////////////////////////////////////////////////////////////
    public static final String PARAMETER_USERNAME = "username";
    public static final String PARAMETER_USER_ID = "userId";

    public static final String PARAMETER_FOOD_ID = "foodId";
    public static final String PARAMETER_FOOD_ID_LIST = "foodIds";
    public static final String PARAMETER_FOOD_NAME = "foodName";
    public static final String PARAMETER_FOOD_BRAND = "foodBrand";

    public static final String PARAMETER_CALENDAR_START_DATE = "startDate";
    public static final String PARAMETER_CALENDAR_END_DATE = "endDate";

    public static final String PARAMETER_MEAL_PLAN_ID = "mealPlanId";
    public static final String PARAMETER_CARB_CYCLING_PLAN_ID = "carbCyclingPlanId";
    public static final String PARAMETER_MEAL_PLAN_DAY_ID = "dayId";
    public static final String PARAMETER_MEAL_PLAN_MEAL_ID = "mealId";
    public static final String PARAMETER_MEAL_PLAN_CARB_CYCLING_MEAL_NUMBER = "mealNumber";
    public static final String PARAMETER_MEAL_PLAN_DAY_ID_LIST = "mealPlanningDays";
    public static final String PARAMETER_MEAL_PLAN_MEAL_ID_LIST = "mealPlanningMeals";

    public static final String PARAMETER_RECIPE_ID = "recipeId";
    public static final String PARAMETER_RECIPE_NAME = "recipeName";
    public static final String PARAMETER_RECIPE_STEP_ID = "recipeStepId";

    ///////////////////////////////////////////////////////////////////
    //
    //                           NATIVE NAMED QUERIES
    //
    ///////////////////////////////////////////////////////////////////
    public static final String NATIVE_DELETE_DAYS_FOR_MEAL_PLAN = """
            DELETE FROM
                PFD_V1.MEAL_PLANNING_DAY
            WHERE (DAY_ID) IN
                  (
                      SELECT
                          mealPlanDay.DAY_ID
                      FROM
                          PFD_V1.MEAL_PLANNING_DAY mealPlanDay
                      LEFT JOIN
                          PFD_V1.CARB_CYCLING_DAY carbCyclingDay ON mealPlanDay.DAY_ID = carbCyclingDay.DAY_ID
                      WHERE
                          mealPlanDay.MEAL_PLAN_ID = ?
                          AND carbCyclingDay.DAY_ID IS NULL
                  )""";
}
