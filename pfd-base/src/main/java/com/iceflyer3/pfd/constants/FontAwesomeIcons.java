/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.constants;

public final class FontAwesomeIcons {

    private FontAwesomeIcons() {}

    // This should really be consistent across the application.
    // Here like a reasonable place for the default icon size for table rows
    public final static int DEFAULT_SIZE_TABLE_ROW_ICON = 16;

    public final static String ICON_FAMILY_SOLID = "Font Awesome 5 Free Solid";
    public final static String ICON_FAMILY_REGULAR = "Font Awesome 5 Free Regular";
    public final static String ICON_FAMILY_BRANDS = "Font Awesome 5 Free Brands Regular";

    public final static String CALENDAR_DAYS = "\uf073";
    public final static String CHEVRON_RIGHT = "\uf054";
    public final static String CHEVRON_LEFT = "\uf053";
    public final static String CIRCLE_ARROW_LEFT = "\uf0a8";
    public final static String EXTERNAL_LINK = "\uf35d";
    public final static String EYE = "\uf06e";
    public final static String UPLOAD = "\uf093";
    public final static String DOWNLOAD = "\uf019";
    public final static String MONEY_CHECK = "\uf53c";
    public final static String PENCIL = "\uf044";
    public final static String PLUS = "\uf067";
    public final static String POWER = "\uf011";
    public final static String SAVE = "\uf0c7";
    public final static String SLIDERS_H = "\uf1de";
    public final static String TIMES = "\uf00d";
    public final static String TOGGLE_OFF = "\uf204";
    public final static String TOGGLE_ON = "\uf205";
    public final static String USER = "\uf007";

}

