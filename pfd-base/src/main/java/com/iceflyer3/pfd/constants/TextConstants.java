/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.constants;

public final class TextConstants
{
    private TextConstants() { }

    public static final String COPYRIGHT_AND_LICENSE_NOTICES = """
                Personal Food Database
                Copyright (C) 2022, 2023 Josh Mossman

                This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

                To view a copy of the license see the "Application License" tab in the "About" section of the application or visit https://www.gnu.org/licenses/gpl-3.0.html.""";

    public static final String LEGAL_DISCLAIMER_WARRANTY_NOTICES = """
            This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
            
            All information contained in this application is strictly for INFORMATIONAL PURPOSES ONLY. I am not a physician; no information contained in this application is medical advice and information contained in this application should not replace medical advice.""";
}
