package com.iceflyer3.pfd.exception;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * An exception that is thrown when some part of the application state is not as it is expected to be.
 *
 * Ideally, this exception should never really be thrown. If it is then it is almost certainly an indication
 * of a programming error the application can't recover from.
 */
public class InvalidApplicationStateException extends RuntimeException
{
    public InvalidApplicationStateException(String message) {
        super(message);
    }

    public InvalidApplicationStateException(Throwable cause) {
        super(cause);
    }

    public InvalidApplicationStateException(String message, Throwable cause) {
        super(message, cause);
    }
}
