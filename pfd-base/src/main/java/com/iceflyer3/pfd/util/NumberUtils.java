package com.iceflyer3.pfd.util;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import java.math.BigDecimal;
import java.math.RoundingMode;

public class NumberUtils {

    // Multiplication conversion factors for US (imperial) to SI (metric)
    // Conversion factors taken from https://extension.psu.edu/conversion-factors-for-english-and-si-metric-units
    public static final BigDecimal KG_TO_POUND_CONVERSION_FACTOR = new BigDecimal("2.2046");
    public static final BigDecimal POUND_TO_KG_CONVERSION_FACTOR = new BigDecimal("0.4536");
    public static final BigDecimal INCHES_TO_CM_CONVERSION_FACTOR = new BigDecimal("2.54");
    public static final BigDecimal CM_TO_INCHES_CONVERSION_FACTOR = new BigDecimal("0.3937");

    /**
     * Convenience function for getting a new BigDecimal instance that is already set up
     * to use the correct precision and rounding mode.
     *
     * This should be standardized across the entire UI layer of the application and this
     * provides an easy way to enforce that standardization and (if we ever choose to do so)
     * change all values at the same time.
     * @param forValue The value of the big decimal
     * @return A new BigDecimal instance that uses the standard "half-up" rounding mode and
     *         has a precision of two decimal places.
     */
    public static BigDecimal withStandardPrecision(double forValue)
    {
        return withStandardPrecision(BigDecimal.valueOf(forValue));
    }

    /*
     * We could use a math context here. But I don't see any particular advantage to doing so in this instance.
     *
     * This function would still operate in a similar way by returning the math context and we would still have to
     * invoke the function in order to pass in the math context for each operation on a BigDecimal that we want to
     * perform.
     *
     * Additionally, that would limit us to only being able to use the function when a math context is an argument
     * of a function on the BigDecimal rather than just being able to pass in a BigDecimal for the purposes of
     * ensuring that the value is to the precision that we want it to be at.
     *
     * Still not quite entirely satisfied with this approach but it works well enough.
     */
    public static BigDecimal withStandardPrecision(BigDecimal forValue)
    {
        return forValue.setScale(0, RoundingMode.HALF_UP);
    }

    /**
     * Numerically compares two BigDecimal values using compareTo.
     *
     * For the purposes of this comparison null is equal to null.
     * So null == null returns true.
     *
     * @param number1 The first number to compare
     * @param number2 The second number to compare
     * @return True if both values are either null or numerically equivalent according to
     *         BigDecimal.compareTo. Otherwise false.
     */
    public static boolean areNumericallyEqual(BigDecimal number1, BigDecimal number2)
    {
        if (number1 != null && number2 != null)
        {
            // If both values are not null then check for numeric equality
            return number1.compareTo(number2) == 0;
        }
        else
        {
            // For our purposes here null is equal to null.
            return number1 == null && number2 == null;
        }
    }
}
