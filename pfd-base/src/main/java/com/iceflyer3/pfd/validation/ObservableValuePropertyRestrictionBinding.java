/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.validation;

import javafx.beans.value.ObservableValue;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * Encapsulates a binding of a JavaFx property that implements the ObservableValue interface
 * with one or more DatumRestrictions that should be applied to the value of the property.
 * @param <T> The type of the generic parameter of the ObservableValue
 */
public class ObservableValuePropertyRestrictionBinding<T> implements PropertyRestrictionBinding
{
    private final ObservableValue<T> property;
    private final Set<DatumRestriction> restrictions;

    public ObservableValuePropertyRestrictionBinding(ObservableValue<T> property)
    {
        this.property = property;
        this.restrictions = new HashSet<>();
    }

    @Override
    public void bindRestriction(DatumRestriction restriction)
    {
        this.restrictions.add(restriction);
    }

    @Override
    public ValidationResult applyRestrictions()
    {
        ValidationResult result = ValidationResult.success();

        for(DatumRestriction restriction : restrictions)
        {
            T value = property.getValue();

            if (value instanceof String strValue)
            {
               result = result.and(restriction.apply(strValue));
            }
            else if (value instanceof Integer intValue)
            {
                result = result.and(restriction.apply(intValue));
            }
            else if (value instanceof BigDecimal decimalValue)
            {
                result = result.and(restriction.apply(decimalValue));
            }
            else if (value == null)
            {
                result = result.and(() -> false, "Value was null");
            }
        }

        return result;
    }

    @Override
    public String toString()
    {
        return "ObservableValuePropertyRestrictionBinding{" + "property=" + property + ", restrictions=" + restrictions + '}';
    }
}
