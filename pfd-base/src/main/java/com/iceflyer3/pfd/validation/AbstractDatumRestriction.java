/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


package com.iceflyer3.pfd.validation;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 * Base class for Datum Restriction implementations.
 *
 * Contains common functionality needed by all implementations.
 */
public class AbstractDatumRestriction
{
    private final Logger LOG = LoggerFactory.getLogger(AbstractDatumRestriction.class);

    protected <T> boolean isInputNull(T value)
    {
        if(value == null)
        {
            /*
             * Null values are going to be inevitable when bidirectionally bound UI controls are
             * in the picture. For example any time the user clears a text box to enter a new value.
             *
             * So let's not blow up the list of failure messages of the result with messages that the
             * application can't avoid and doesn't really care about.
             *
             * We will, however, log out the message because it could potentially help with debugging
             * efforts.
             */
            LOG.trace("Input value was null");
            return true;
        }
        else
        {
            return false;
        }
    }
}
