package com.iceflyer3.pfd.validation;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import java.util.*;
import java.util.function.BooleanSupplier;

/**
 * Represents the result of some manner of validation logic.
 *
 * This class combines the result of the validation logic with the error message that
 * should occur were that validation logic to fail.
 *
 * Additionally, facilities are also provided for combining results with each other
 * via logical AND and logical OR operations.
 *
 * Validation results are immutable.
 */
public class ValidationResult
{
    private boolean wasSuccessful;
    private final List<String> failureMessages;

    protected ValidationResult() {
        this.wasSuccessful = false;
        this.failureMessages = new ArrayList<>();
    }

    /**
     * Create a new ValidationResult based upon an arbitrary condition that should
     * be validated
     *
     * @param proposition The boolean condition that will determine success or failure
     * @return A ValidationResult that represents the result of the provided proposition
     */
    public static ValidationResult create(BooleanSupplier proposition, String failureMessage)
    {
        ValidationResult result = new ValidationResult();
        result.setWasSuccessful(proposition.getAsBoolean());

        if (!result.wasSuccessful())
        {
            result.addFailureMessage(failureMessage);
        }

        return result;
    }

    /**
     * Create a new ValidationResult that represents a successful validation
     *
     * @return A new ValidationResult that will report success
     */
    public static ValidationResult success()
    {
        ValidationResult successResult = new ValidationResult();
        successResult.setWasSuccessful(true);
        return successResult;
    }

    /**
     * Flattens a list of ValidationResult instances into a single instance.
     *
     * If any of the results in the list failed then the returned result will also have
     * a failed status (meaning a call to wasSuccessful will return false)
     *
     * All failure messages for any of the results will be put into the failure
     * messages map for the returned result.
     *
     * This can be used if you have multiple validation results from different
     * sources that you wish to combine and transform into a single result.
     *
     * @param resultList The list of ValidationResult instances that you wish
     *                   to flatten
     * @return An ValidationResult
     */
    public static ValidationResult flatten(Collection<ValidationResult> resultList)
    {
        ValidationResult flattenedResult = new ValidationResult();
        flattenedResult.setWasSuccessful(resultList.stream().allMatch(result -> result.wasSuccessful));
        resultList.stream().flatMap(modelValidationResult -> modelValidationResult.getFailureMessages().stream()).forEach(flattenedResult::addFailureMessage);
        return flattenedResult;
    }

    public static ValidationResult flatten(ValidationResult... results)
    {
        return flatten(Arrays.asList(results));
    }

    /**
     * Combines this validation result with an arbitrary proposition via
     * a logical AND operation.
     *
     * @param proposition The BooleanSupplier that acts as the proposition to combine.
     * @return A ValidationResult that is the result of the logical AND operation.
     */
    public ValidationResult and(BooleanSupplier proposition, String failureMessage)
    {
        ValidationResult combinedResult = new ValidationResult();
        combinedResult.setWasSuccessful(this.wasSuccessful && proposition.getAsBoolean());
        this.failureMessages.forEach(combinedResult::addFailureMessage);

        if (!proposition.getAsBoolean())
        {
            combinedResult.addFailureMessage(failureMessage);
        }

        return combinedResult;
    }

    /**
     * Combines this validation result with another validation result via a logical
     * AND operation
     *
     * @param otherResult The other ValidationResult object to combine with
     * @return A ValidationResult that is the result of the logical AND operation.
     */
    public ValidationResult and(ValidationResult otherResult)
    {
        ValidationResult combinedResult = new ValidationResult();
        combinedResult.setWasSuccessful(this.wasSuccessful && otherResult.wasSuccessful());
        this.failureMessages.forEach(combinedResult::addFailureMessage);
        return combinedResult;
    }

    /**
     * Combines this validation result with an arbitrary proposition via a logical
     * OR operation.
     *
     * @param proposition The BooleanSupplier that acts as the proposition to combine.
     * @return An ValidationResult that is the result of the logical OR operation.
     */
    public ValidationResult or(BooleanSupplier proposition, String failureMessage)
    {
        ValidationResult combinedResult = new ValidationResult();
        combinedResult.setWasSuccessful(this.wasSuccessful || proposition.getAsBoolean());
        this.failureMessages.forEach(combinedResult::addFailureMessage);

        if (!proposition.getAsBoolean())
        {
            combinedResult.addFailureMessage(failureMessage);
        }

        return combinedResult;
    }

    /**
     * Combines this validation result with another validation result via a logical
     * OR operation
     *
     * @param otherResult The other ValidationResult object to combine with
     * @return A ValidationResult that is the result of the logical OR operation.
     */
    public ValidationResult or(ValidationResult otherResult)
    {
        ValidationResult combinedResult = new ValidationResult();
        combinedResult.setWasSuccessful(this.wasSuccessful || otherResult.wasSuccessful());
        this.failureMessages.forEach(combinedResult::addFailureMessage);
        return combinedResult;
    }

    public boolean wasSuccessful() {
        return wasSuccessful;
    }

    /**
     * Get a list of the failure messages where the key is the control name and the value is the message.
     * The list returned by this function is immutable. Any attempts to change it will cause an exception.
     *
     * @return An UnmodifiableMap that contains the control which failed as the key and the failure
     * message as the value.
     */
    public List<String> getFailureMessages() {
        return Collections.unmodifiableList(failureMessages);
    }

    /*
     * The state of the result should be immutable to clients that receive a result
     * from a validation operation.
     *
     * Therefore these are restricted to package level access.
     */
    protected void setWasSuccessful(boolean wasSuccessful) {
        this.wasSuccessful = wasSuccessful;
    }

    protected void addFailureMessage(String failureMessage)
    {
        failureMessages.add(failureMessage);
    }

    @Override
    public String toString() {
        return "ValidationResult{" +
                "wasSuccessful=" + wasSuccessful +
                ", failureMessages=" + failureMessages +
                '}';
    }
}
