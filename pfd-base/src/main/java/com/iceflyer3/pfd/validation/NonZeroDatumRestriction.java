package com.iceflyer3.pfd.validation;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import java.math.BigDecimal;
import java.util.Collection;

/**
 * Restriction that verifies that a provided value must be greater than zero.
 *
 *  Numeric values are self-explanatory.
 *  For collections the size of the collection is used for the comparison.
 *  For Strings the length of the string is used for the comparison.
 */
public class NonZeroDatumRestriction extends AbstractDatumRestriction implements DatumRestriction
{
    public NonZeroDatumRestriction() { }

    @Override
    public <T> ValidationResult apply(Collection<T> collection)
    {
        ValidationResult result = new ValidationResult();

        if (!isInputNull(collection))
        {
            if (collection.size() > 0)
            {
                result.setWasSuccessful(true);
            }
            else
            {
                result.addFailureMessage("Collection did not have a size greater than zero");
            }
        }

        return result;
    }

    @Override
    public ValidationResult apply(String value)
    {
        ValidationResult result = new ValidationResult();

        if(!isInputNull(value))
        {
            if (value.length() > 0)
            {
                result.setWasSuccessful(true);
            }
            else
            {
                result.addFailureMessage("String did not have a length greater than zero");
            }
        }

        return result;
    }

    @Override
    public ValidationResult apply(int value)
    {
        ValidationResult result = new ValidationResult();

        if (!isInputNull(value))
        {
            if (value > 0)
            {
                result.setWasSuccessful(true);
            }
            else
            {
                result.addFailureMessage(String.format("Integer value %s was not greater than zero", value));
            }
        }

        return result;
    }

    @Override
    public ValidationResult apply(BigDecimal value)
    {
        ValidationResult result = new ValidationResult();

        if(!isInputNull(value))
        {
            if (value.compareTo(BigDecimal.ZERO) > 0)
            {
                result.setWasSuccessful(true);
            }
            else
            {
                result.addFailureMessage(String.format("BigDecimal value %s was not greater than zero", value));
            }
        }

        return result;
    }
}
