/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.validation;

import javafx.collections.ObservableList;

import java.util.HashSet;
import java.util.Set;

/**
 * Encapsulates a binding of a JavaFx property that implements the ObservableList interface
 * with one or more DatumRestrictions that should be applied to the list.
 *
 * The data restrictions apply not to the individual elements of the list but to the list
 * on the whole.
 */
public class ObservableListPropertyRestrictionBinding implements PropertyRestrictionBinding
{
    private final ObservableList<?> list;
    private final Set<DatumRestriction> restrictions;

    // This class isn't actually used at current due to the way hashcode works for collections.
    // However, it's already written, so I'm leaving it here in the event it is useful in the future.
    public ObservableListPropertyRestrictionBinding(ObservableList<?> list)
    {
        this.list = list;
        this.restrictions = new HashSet<>();
    }

    @Override
    public void bindRestriction(DatumRestriction restriction)
    {
        this.restrictions.add(restriction);
    }

    @Override
    public ValidationResult applyRestrictions()
    {
        ValidationResult result = ValidationResult.success();

        for(DatumRestriction restriction : restrictions)
        {
            result = result.and(restriction.apply(list));
        }

        return result;
    }
}
