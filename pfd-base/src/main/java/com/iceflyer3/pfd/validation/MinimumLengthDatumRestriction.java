package com.iceflyer3.pfd.validation;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * Restriction that specifies a minimum acceptable length for the passed in value.
 *
 * For collections the size of the collection is used for the comparison.
 * For Strings the length of the string is used for the comparison.
 *
 * Numeric values are not supported and will throw an UnsupportedOperationException.
 */
public class MinimumLengthDatumRestriction extends AbstractDatumRestriction implements DatumRestriction
{
    private final static Logger LOG = LoggerFactory.getLogger(MinimumLengthDatumRestriction.class);

    private final int minLength;

    /**
     * Create a new MinimumLengthDatumRestriction
     * @param minLength The minimum required length of the text for text controls registered with this validator
     */
    public MinimumLengthDatumRestriction(int minLength)
    {
        if (minLength < 0)
        {
            throw new IllegalArgumentException("minLength may not be a negative value.");
        }

        this.minLength = minLength;
    }

    @Override
    public <T> ValidationResult apply(Collection<T> collection)
    {
        ValidationResult result = new ValidationResult();

        if (!isInputNull(collection))
        {
            if (collection.size() >= minLength)
            {
                result.setWasSuccessful(true);
            }
            else
            {
                result.addFailureMessage(String.format("Collection of size %s had fewer elements than the required minimum of %s", collection.size(), minLength));
            }
        }

        return result;
    }

    @Override
    public ValidationResult apply(String value)
    {
        ValidationResult result = new ValidationResult();

        if(!isInputNull(value))
        {
            if (value.length() >= minLength)
            {
                result.setWasSuccessful(true);
            }
            else
            {
                result.addFailureMessage(String.format("Value of %s was not longer than required minimum length of %s", value, minLength));
            }
        }

        return result;
    }

    @Override
    public ValidationResult apply(int value)
    {
        LOG.error("Attempted to apply MinimumLengthDatumRestriction to a numeric value. The operation is not supported.");
        throw new UnsupportedOperationException("Minimum length datum restrictions do not support numeric values");
    }

    @Override
    public ValidationResult apply(BigDecimal value)
    {
        LOG.error("Attempted to apply MinimumLengthDatumRestriction to a numeric value. The operation is not supported.");
        throw new UnsupportedOperationException("Minimum length datum restrictions do not support numeric values");
    }
}
