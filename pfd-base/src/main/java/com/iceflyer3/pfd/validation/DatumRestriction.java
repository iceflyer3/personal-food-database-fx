/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


package com.iceflyer3.pfd.validation;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * Interface to be implemented by classes that wish to provide the ability to define and apply
 * restrictions that should apply to a particular piece of data.
 *
 * Not all implementations must support all overloads. Implementations that do not support one
 * or more overloads will throw an UnsupportedOperationException exception when the unsupported
 * overload is called.
 */
public interface DatumRestriction
{
    <T> ValidationResult apply(Collection<T> collection);
    ValidationResult apply(String value);
    // TODO: This can be changed to accept a parameter of type Number. Thus covering integers, BigDecimal, and other numeric values.
    ValidationResult apply(int value);
    ValidationResult apply(BigDecimal value);
}
