package com.iceflyer3.pfd.validation;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import java.math.BigDecimal;
import java.util.Collection;

/**
 * Validator for validating that a numeric value is within a specified range
 */
public class NumericRangeDatumRestriction extends AbstractDatumRestriction implements DatumRestriction {

    /*
     * We use BigDecimal elsewhere because that is what H2 maps their decimal type to.
     * But we don't have any values that are big enough to care about that kind of precision.
     * IMO double is just easier to deal with and also more readable in terms of expressing the
     * comparison logic we're doing here. So that's what we use here.
     */
    private double minValue;
    private double maxValue;

    public NumericRangeDatumRestriction(double minValue, double maxValue)
    {
        super();

        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    public double getMinValue() {
        return minValue;
    }

    public double getMaxValue() {
        return maxValue;
    }

    public void setRange(double minValue, double maxValue)
    {
        if (minValue > maxValue)
        {
            throw new IllegalArgumentException("Invalid min / max values detected. Ensure that your min value is less than or equal to your max value");
        }
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    @Override
    public <T> ValidationResult apply(Collection<T> collection)
    {
        ValidationResult result = new ValidationResult();

        if (!isInputNull(collection))
        {
            if (collection.size() > maxValue)
            {
                result.addFailureMessage(String.format("Collection had a size greater than the allowed maximum of %s", maxValue));
            }
            else if (collection.size() < minValue)
            {
                result.addFailureMessage(String.format("Collection had a size smaller than the allowed minimum of %s", minValue));
            }
            else
            {
                result.setWasSuccessful(true);
            }
        }

        return result;
    }

    @Override
    public ValidationResult apply(String value)
    {
        ValidationResult result = new ValidationResult();

        if (!isInputNull(value))
        {
            if (value.length() > maxValue)
            {
                result.addFailureMessage(String.format("String had a length greater than the allowed maximum of %s", maxValue));
            }
            else if (value.length() < minValue)
            {
                result.addFailureMessage(String.format("String had a length smaller than the allowed minimum of %s", minValue));
            }
            else
            {
                result.setWasSuccessful(true);
            }
        }

        return result;
    }

    @Override
    public ValidationResult apply(int value)
    {
        ValidationResult result = new ValidationResult();

        if (!isInputNull(value))
        {
            if (value > maxValue)
            {
                result.addFailureMessage(String.format("Integer was greater than the allowed maximum of %s", maxValue));
            }
            else if (value < minValue)
            {
                result.addFailureMessage(String.format("Integer was smaller than the allowed minimum of %s", minValue));
            }
            else
            {
                result.setWasSuccessful(true);
            }
        }

        return result;
    }

    @Override
    public ValidationResult apply(BigDecimal value)
    {
        ValidationResult result = new ValidationResult();

        if (!isInputNull(value))
        {
            if (value.compareTo(BigDecimal.valueOf(maxValue)) > 0)
            {
                result.addFailureMessage(String.format("BigDecimal was greater than the allowed maximum of %s", maxValue));
            }
            else if (value.compareTo(BigDecimal.valueOf(minValue)) < 0)
            {
                result.addFailureMessage(String.format("BigDecimal was smaller than the allowed minimum of %s", minValue));
            }
            else
            {
                result.setWasSuccessful(true);
            }
        }

        return result;
    }
}
