/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

module pfd.base {
    requires java.sql;
    requires org.slf4j;
    requires com.google.common;
    requires javafx.base;

    exports com.iceflyer3.pfd.constants;
    exports com.iceflyer3.pfd.enums;
    exports com.iceflyer3.pfd.exception;
    exports com.iceflyer3.pfd.util;
    exports com.iceflyer3.pfd.validation;
    exports com.iceflyer3.pfd.user;
}