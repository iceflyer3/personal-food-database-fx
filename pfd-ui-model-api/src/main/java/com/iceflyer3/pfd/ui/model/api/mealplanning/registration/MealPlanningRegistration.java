package com.iceflyer3.pfd.ui.model.api.mealplanning.registration;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023, 2025 Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.ui.model.api.mealplanning.MealPlanNutrientSuggestionModel;
import javafx.collections.ObservableList;

/**
 * Interface to be implemented by ViewModels that collect information required for registration of a meal plan.
 *
 * The information collected will generally differ by the type of the registration, but all meal planning registrations
 * support user defined custom requirements for nutrients that are not one of protein, fats, or carbs.
 */
public interface MealPlanningRegistration
{
    ObservableList<MealPlanNutrientSuggestionModel> getCustomNutrients();
}
