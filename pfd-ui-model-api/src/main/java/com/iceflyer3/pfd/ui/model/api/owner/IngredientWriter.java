package com.iceflyer3.pfd.ui.model.api.owner;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.ui.model.api.IngredientModel;
import com.iceflyer3.pfd.ui.model.api.food.ReadOnlyFoodModel;

/**
 * Interface for classes that wish to be able to expose the ability to manage the ingredients list
 *
 * Managing the list of ingredients is unique because the ingredient deals with an individual food.
 * Because foods also have their own proxy this means that implementors will (where applicable) be
 * dealing with a food proxy class at the proxy level and a food view model class at the view model
 * level.
 */

public interface IngredientWriter extends IngredientReader
{
    /**
     * Add a new ingredient to owner's list of ingredients.
     *
     * @param ingredientFood The ReadOnlyFoodModel implementation representing the food that will be
     *                       added as a new ingredient
     */
    void addIngredient(ReadOnlyFoodModel ingredientFood);

    /**
     * Remove a previously added ingredient
     * @param ingredient The IngredientViewModel representing the ingredient to be removed
     */
    void removeIngredient(IngredientModel ingredient);
}
