package com.iceflyer3.pfd.ui.model.api.recipe;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.ui.model.api.NutritionalSummaryReporter;
import com.iceflyer3.pfd.ui.model.api.owner.IngredientReader;
import com.iceflyer3.pfd.ui.model.api.owner.ServingSizeReader;
import javafx.beans.property.ReadOnlyListProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.collections.ObservableList;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

public interface ReadOnlyRecipeModel extends NutritionalSummaryReporter, ServingSizeReader, IngredientReader
{
    UUID getId();
    String getName();
    ReadOnlyStringProperty nameProperty();

    String getSource();
    ReadOnlyStringProperty sourceProperty();

    BigDecimal getNumberOfServingsMade();
    ReadOnlyObjectProperty<BigDecimal> numberOfServingsMadeProperty();

    String getCreatedUser();
    ReadOnlyStringProperty createdUserProperty();

    LocalDateTime getCreatedDate();
    ReadOnlyObjectProperty<LocalDateTime> createdDateProperty();

    String getLastModifiedUser();
    ReadOnlyStringProperty lastModifiedUserProperty();

    LocalDateTime getLastModifiedDate();
    ReadOnlyObjectProperty<LocalDateTime> lastModifiedDateProperty();

    ObservableList<RecipeStepModel> getSteps();
    ReadOnlyListProperty<RecipeStepModel> stepsProperty();
}
