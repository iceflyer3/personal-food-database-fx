package com.iceflyer3.pfd.ui.model.api.food;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.enums.FoodType;
import com.iceflyer3.pfd.ui.model.api.NutritionFactsReader;
import com.iceflyer3.pfd.ui.model.api.ServingSizeModel;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.collections.ObservableList;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

public interface ReadOnlyFoodModel extends NutritionFactsReader
{
  UUID getId();

  // Basic info
  FoodType getFoodType();
  ReadOnlyObjectProperty<FoodType> foodTypeProperty();

  String getName();
  ReadOnlyStringProperty nameProperty();

  String getBrand();
  ReadOnlyStringProperty brandProperty();

  String getSource();
  ReadOnlyStringProperty sourceProperty();

  int getGlycemicIndex();
  ReadOnlyIntegerProperty glycemicIndexProperty();

  ObservableList<ServingSizeModel> getServingSizes();

  BigDecimal getCalories();
  ReadOnlyObjectProperty<BigDecimal> caloriesProperty();

  String getCreatedUser();
  ReadOnlyStringProperty createdUserProperty();

  LocalDateTime getCreatedDate();
  ReadOnlyObjectProperty<LocalDateTime> createdDateProperty();

  String getLastModifiedUser();
  ReadOnlyStringProperty lastModifiedUserProperty();

  LocalDateTime getLastModifiedDate();
  ReadOnlyObjectProperty<LocalDateTime> lastModifiedDateProperty();

}
