package com.iceflyer3.pfd.ui.model.api.proxy;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Interface to be implemented to get back the results from a proxy operation that returns data
 * @param <T> The type of the proxy
 */
@FunctionalInterface
public interface ProxyDataResultCallback<T> {

    /**
     * Callback used to retrieve the loaded proxy / proxies from a proxy factory after they have been loaded.
     * @param wasSuccessful If the factories were successfully created. Used by the UI to display failure messages where needed.
     *                      If this is false then the result object will be null.
     * @param proxyResult The resulting proxy or proxies returned by the factory
     */
    void receiveResult(boolean wasSuccessful, T proxyResult);
}
