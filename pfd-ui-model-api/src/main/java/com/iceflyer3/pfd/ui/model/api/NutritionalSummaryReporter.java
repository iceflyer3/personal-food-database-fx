/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.model.api;

import com.iceflyer3.pfd.util.NumberUtils;

/**
 * Interface that allows access to nutritional summary (grand totals) information and facilitates requesting
 * recalculation of the nutritional summary data.
 *
 * Note that this is distinct from the NutritionalSummary class which ultimately represents the actual nutritional
 * summary data for the various types of entities as calculated by the various views within the database.
 *
 * This interface instead represents a standard / uniform way to access nutritional summary information along with
 * a means to perform any ancillary actions on that data.
 */
public interface NutritionalSummaryReporter extends NutritionFactsReader
{
    /**
     * Refreshes / recalculates the nutritional details for this reporter
     */
    void refreshNutritionDetails();

    /**
     * Compares the macronutrients and micronutrients of this NutritionFactsReader implementation to
     * those of another NutritionFactsReader implementation
     * @param other The other NutritionFactsReader implementation to which to compare
     * @return True if all the macro and micronutrients are numerically equal for both NutritionFactsReader. Otherwise false.
     */
    default boolean isNutritionallyEqualTo(NutritionFactsReader other)
    {
        // Check macros
        boolean result = 
            NumberUtils.areNumericallyEqual(this.getCalories(), other.getCalories())
                    && NumberUtils.areNumericallyEqual(this.getProtein(), other.getProtein())
                    && NumberUtils.areNumericallyEqual(this.getCarbohydrates(), other.getCarbohydrates())
                    && NumberUtils.areNumericallyEqual(this.getSugars(), other.getSugars())
                    && NumberUtils.areNumericallyEqual(this.getFiber(), other.getFiber())
                    && NumberUtils.areNumericallyEqual(this.getTotalFats(), other.getTotalFats())
                    && NumberUtils.areNumericallyEqual(this.getSaturatedFat(), other.getSaturatedFat())
                    && NumberUtils.areNumericallyEqual(this.getTransFat(), other.getTransFat())
                    && NumberUtils.areNumericallyEqual(this.getMonounsaturatedFat(), other.getMonounsaturatedFat())
                    && NumberUtils.areNumericallyEqual(this.getPolyunsaturatedFat(), other.getPolyunsaturatedFat());

        if (result)
        {
            // If macros were valid then check micros
            result = NumberUtils.areNumericallyEqual(this.getCholesterol(), other.getCholesterol())
                    && NumberUtils.areNumericallyEqual(this.getCalcium(), other.getCalcium())
                    && NumberUtils.areNumericallyEqual(this.getCopper(), other.getCopper())
                    && NumberUtils.areNumericallyEqual(this.getIron(), other.getIron())
                    && NumberUtils.areNumericallyEqual(this.getMagnesium(), other.getMagnesium())
                    && NumberUtils.areNumericallyEqual(this.getManganese(), other.getManganese())
                    && NumberUtils.areNumericallyEqual(this.getPhosphorus(), other.getPhosphorus())
                    && NumberUtils.areNumericallyEqual(this.getPotassium(), other.getPotassium())
                    && NumberUtils.areNumericallyEqual(this.getSodium(), other.getSodium())
                    && NumberUtils.areNumericallyEqual(this.getSulfur(), other.getSulfur())
                    && NumberUtils.areNumericallyEqual(this.getZinc(), other.getZinc())
                    && NumberUtils.areNumericallyEqual(this.getVitaminA(), other.getVitaminA())
                    && NumberUtils.areNumericallyEqual(this.getVitaminC(), other.getVitaminC())
                    && NumberUtils.areNumericallyEqual(this.getVitaminK(), other.getVitaminK())
                    && NumberUtils.areNumericallyEqual(this.getVitaminD(), other.getVitaminD())
                    && NumberUtils.areNumericallyEqual(this.getVitaminE(), other.getVitaminE())
                    && NumberUtils.areNumericallyEqual(this.getVitaminB1(), other.getVitaminB1())
                    && NumberUtils.areNumericallyEqual(this.getVitaminB2(), other.getVitaminB2())
                    && NumberUtils.areNumericallyEqual(this.getVitaminB3(), other.getVitaminB3())
                    && NumberUtils.areNumericallyEqual(this.getVitaminB5(), other.getVitaminB5())
                    && NumberUtils.areNumericallyEqual(this.getVitaminB6(), other.getVitaminB6())
                    && NumberUtils.areNumericallyEqual(this.getVitaminB7(), other.getVitaminB7())
                    && NumberUtils.areNumericallyEqual(this.getVitaminB8(), other.getVitaminB8())
                    && NumberUtils.areNumericallyEqual(this.getVitaminB9(), other.getVitaminB9())
                    && NumberUtils.areNumericallyEqual(this.getVitaminB12(), other.getVitaminB12());
        }

        return result;
    }
}
