package com.iceflyer3.pfd.ui.model.api.viewmodel;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.validation.ValidationResult;

/**
 * ViewModels are responsible for enforcing their own invariants.
 *
 * This interface facilitates interaction with the classes that are
 * found in the Validation package that allow the ViewModels to enforce
 * these by providing validation facilities and functionality.
 */
public interface ValidatingViewModel {
    /**
     * Validates the whole state of the view model
     * @return A ValidationResult object representing the result
     */
    ValidationResult validate();

    /**
     * Validates the state of an individual property of the view model
     *
     * @param propertyHashcode The hashcode of the property to validate
     * @return A ValidationResult object representing the result
     */
    ValidationResult validateProperty(int propertyHashcode);
}
