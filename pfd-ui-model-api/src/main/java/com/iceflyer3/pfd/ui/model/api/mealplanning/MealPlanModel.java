package com.iceflyer3.pfd.ui.model.api.mealplanning;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.enums.*;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public interface MealPlanModel
{
   UUID getId();
   int getAge();
   IntegerProperty ageProperty();
   void setAge(int age);

   BigDecimal getHeight();
   ObjectProperty<BigDecimal> heightProperty();
   void setHeight(BigDecimal height);

   BigDecimal getWeight();
   ObjectProperty<BigDecimal> weightProperty();
   void setWeight(BigDecimal weight);

   Sex getSex();
   ObjectProperty<Sex> sexProperty();
   void setSex(Sex sex);

   BigDecimal getBodyFatPercentage();
   ObjectProperty<BigDecimal> bodyFatPercentageProperty();
   void setBodyFatPercentage(BigDecimal bodyFatPercentage);

   ActivityLevel getActivityLevel();
   ObjectProperty<ActivityLevel> activityLevelProperty();
   void setActivityLevel(ActivityLevel activityLevel);

   FitnessGoal getFitnessGoal();
   ObjectProperty<FitnessGoal> fitnessGoalProperty();
   void setFitnessGoal(FitnessGoal fitnessGoal);

   BasalMetabolicRateFormula getBmrFormula();
   ObjectProperty<BasalMetabolicRateFormula> bmrFormulaProperty();
   void setBmrFormula(BasalMetabolicRateFormula bmrFormula);

   BigDecimal getDailyCalories();
   ObjectProperty<BigDecimal> dailyCaloriesProperty();

   BigDecimal getTdeeCalories();
   ObjectProperty<BigDecimal> tdeeCaloriesProperty();

   BigDecimal getNutrientSuggestedAmount(Nutrient nutrient);

   List<MealPlanNutrientSuggestionModel> getCustomNutrients();

   boolean isActive();
   BooleanProperty isActiveProperty();
   void toggleActiveStatus();
}
