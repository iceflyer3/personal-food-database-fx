package com.iceflyer3.pfd.ui.model.api.food;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.ui.model.api.owner.ServingSizeWriter;
import com.iceflyer3.pfd.enums.FoodType;
import com.iceflyer3.pfd.ui.model.api.NutritionalSummaryReporter;
import com.iceflyer3.pfd.ui.model.api.viewmodel.ValidatingViewModel;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public interface SimpleFoodModel extends ReadOnlyFoodModel, ServingSizeWriter, ValidatingViewModel, NutritionalSummaryReporter
{
    // Basic info
    void setFoodType(FoodType foodType);
    void setName(String foodName);
    void setBrand(String brandName);
    void setSource(String source);
    void setGlycemicIndex(int glycemicIndex);
    void setCalories(BigDecimal calories);
    void setLastModifiedUser(String lastModifiedUser);
    void setLastModifiedDate(LocalDateTime lastModifiedDate);


    // Macronutrients
    void setProtein(BigDecimal protein);
    void setTotalFats(BigDecimal totalFats);
    void setTransFat(BigDecimal transFat);
    void setMonounsaturatedFat(BigDecimal monounsaturatedFat);
    void setPolyunsaturatedFat(BigDecimal polyunsaturatedFat);
    void setSaturatedFat(BigDecimal saturatedFat);
    void setCarbohydrates(BigDecimal carbohydrates);
    void setSugars(BigDecimal sugars);
    void setFiber(BigDecimal fiber);


    // Micronutrients - Lipids
    void setCholesterol(BigDecimal cholesterol);


    // Micronutrients - Minerals
    void setIron(BigDecimal iron);
    void setManganese(BigDecimal manganese);
    void setCopper(BigDecimal copper);
    void setZinc(BigDecimal zinc);
    void setCalcium(BigDecimal calcium);
    void setMagnesium(BigDecimal magnesium);
    void setPhosphorus(BigDecimal phosphorus);
    void setPotassium(BigDecimal potassium);
    void setSodium(BigDecimal sodium);
    void setSulfur(BigDecimal sulfur);


    // Micronutrients - Vitamins
    void setVitaminA(BigDecimal vitaminA);
    void setVitaminC(BigDecimal vitaminC);
    void setVitaminK(BigDecimal vitaminK);
    void setVitaminD(BigDecimal vitaminD);
    void setVitaminE(BigDecimal vitaminE);
    void setVitaminB1(BigDecimal vitaminB1);
    void setVitaminB2(BigDecimal vitaminB2);
    void setVitaminB3(BigDecimal vitaminB3);
    void setVitaminB5(BigDecimal vitaminB5);
    void setVitaminB6(BigDecimal vitaminB6);
    void setVitaminB7(BigDecimal vitaminB7);
    void setVitaminB8(BigDecimal vitaminB8);
    void setVitaminB9(BigDecimal vitaminB9);
    void setVitaminB12(BigDecimal vitaminB12);

    // Properties (non-read-only here to allow binding)
    ObjectProperty<FoodType> foodTypeProperty();
    StringProperty nameProperty();
    StringProperty brandProperty();
    StringProperty sourceProperty();
    IntegerProperty glycemicIndexProperty();
    StringProperty createdUserProperty();
    ObjectProperty<LocalDateTime> createdDateProperty();
    StringProperty lastModifiedUserProperty();
    ObjectProperty<LocalDateTime> lastModifiedDateProperty();

    ObjectProperty<BigDecimal> caloriesProperty();

    // Macronutrients
    ObjectProperty<BigDecimal> proteinProperty();
    ObjectProperty<BigDecimal> totalFatsProperty();
    ObjectProperty<BigDecimal> transFatProperty();
    ObjectProperty<BigDecimal> monounsaturatedFatProperty();
    ObjectProperty<BigDecimal> polyunsaturatedFatProperty();
    ObjectProperty<BigDecimal> saturatedFatProperty();
    ObjectProperty<BigDecimal> carbohydratesProperty();
    ObjectProperty<BigDecimal> sugarsProperty();
    ObjectProperty<BigDecimal> fiberProperty();


    // Micronutrients - Lipids
    ObjectProperty<BigDecimal> cholesterolProperty();


    // Micronutrients - Minerals
    ObjectProperty<BigDecimal> ironProperty();
    ObjectProperty<BigDecimal> manganeseProperty();
    ObjectProperty<BigDecimal> copperProperty();
    ObjectProperty<BigDecimal> zincProperty();
    ObjectProperty<BigDecimal> calciumProperty();
    ObjectProperty<BigDecimal> magnesiumProperty();
    ObjectProperty<BigDecimal> phosphorusProperty();
    ObjectProperty<BigDecimal> potassiumProperty();
    ObjectProperty<BigDecimal> sodiumProperty();
    ObjectProperty<BigDecimal> sulfurProperty();


    // Micronutrients - Vitamins
    ObjectProperty<BigDecimal> vitaminAProperty();
    ObjectProperty<BigDecimal> vitaminCProperty();
    ObjectProperty<BigDecimal> vitaminKProperty();
    ObjectProperty<BigDecimal> vitaminDProperty();
    ObjectProperty<BigDecimal> vitaminEProperty();
    ObjectProperty<BigDecimal> vitaminB1Property();
    ObjectProperty<BigDecimal> vitaminB2Property();
    ObjectProperty<BigDecimal> vitaminB3Property();
    ObjectProperty<BigDecimal> vitaminB5Property();
    ObjectProperty<BigDecimal> vitaminB6Property();
    ObjectProperty<BigDecimal> vitaminB7Property();
    ObjectProperty<BigDecimal> vitaminB8Property();
    ObjectProperty<BigDecimal> vitaminB9Property();
    ObjectProperty<BigDecimal> vitaminB12Property();
}
