/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.model.api.mealplanning.carbcycling;

import com.iceflyer3.pfd.ui.model.api.viewmodel.ValidatingViewModel;
import com.iceflyer3.pfd.enums.CarbCyclingMealRelation;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;

import java.math.BigDecimal;
import java.util.UUID;


public interface CarbCyclingMealConfigurationModel extends ValidatingViewModel
{
    UUID getId();

    boolean isForTrainingDay();

    int getPostWorkoutMealOffset();
    void setPostWorkoutMealOffset(int offset);
    IntegerProperty postWorkoutMealOffsetProperty();

    BigDecimal getCarbConsumptionMaximumPercent();
    void setCarbConsumptionMaximumPercent(BigDecimal maximumPercent);
    ObjectProperty<BigDecimal> carbConsumptionMaximumPercentProperty();

    BigDecimal getCarbConsumptionMinimumPercent();
    void setCarbConsumptionMinimumPercent(BigDecimal minimumPercent);
    ObjectProperty<BigDecimal> carbConsumptionMinimumPercentProperty();

    CarbCyclingMealRelation getPostWorkoutMealRelation();
    void setPostWorkoutMealRelation(CarbCyclingMealRelation mealRelation);
    ObjectProperty<CarbCyclingMealRelation> postWorkoutMealRelationProperty();
}
