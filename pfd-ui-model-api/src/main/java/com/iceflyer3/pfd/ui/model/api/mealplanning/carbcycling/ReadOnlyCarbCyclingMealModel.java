/*
 *  Personal Food Database
 *  Copyright (C) 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


package com.iceflyer3.pfd.ui.model.api.mealplanning.carbcycling;

import com.iceflyer3.pfd.enums.MealType;
import com.iceflyer3.pfd.ui.model.api.mealplanning.ReadOnlyMealPlanMealModel;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyObjectProperty;

import java.math.BigDecimal;

public interface ReadOnlyCarbCyclingMealModel extends ReadOnlyMealPlanMealModel
{
    MealType getMealType();
    ReadOnlyObjectProperty<MealType> mealTypeProperty();

    int getMealNumber();
    ReadOnlyIntegerProperty mealNumberProperty();

    BigDecimal getCarbConsumptionPercentage();
    ReadOnlyObjectProperty<BigDecimal> carbConsumptionPercentageProperty();

    BigDecimal getRequiredCalories();
    ReadOnlyObjectProperty<BigDecimal> requiredCaloriesProperty();

    BigDecimal getRequiredProtein();
    ReadOnlyObjectProperty<BigDecimal> requiredProteinProperty();

    BigDecimal getRequiredCarbs();
    ReadOnlyObjectProperty<BigDecimal> requiredCarbsProperty();

    BigDecimal getRequiredFats();
    ReadOnlyObjectProperty<BigDecimal> requiredFatsProperty();
}
