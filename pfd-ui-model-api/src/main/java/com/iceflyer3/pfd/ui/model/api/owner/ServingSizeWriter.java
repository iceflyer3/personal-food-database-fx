package com.iceflyer3.pfd.ui.model.api.owner;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.enums.UnitOfMeasure;
import com.iceflyer3.pfd.ui.model.api.ServingSizeModel;

import java.math.BigDecimal;

/**
 * Interface for classes that wish to be able to expose the ability to manage the serving sizes list
 */
public interface ServingSizeWriter extends ServingSizeReader
{
    /**
     * Adds a new default serving size to owner's list of serving sizes.
     *
     * The default serving size is 1 gram.
     */
    default void addServingSize()
    {
        addServingSize(UnitOfMeasure.GRAM, BigDecimal.ONE);
    }

    /**
     * Add a new serving size to the owner's list of ingredients
     * @param uom The unit of measure of the serving size
     * @param quantity The number of the UOM included
     */
    void addServingSize(UnitOfMeasure uom, BigDecimal quantity);

    /**
     * Remove a previously added serving size
     * @param servingSize The serving size to remove
     */
    void removeServingSize(ServingSizeModel servingSize);
}
