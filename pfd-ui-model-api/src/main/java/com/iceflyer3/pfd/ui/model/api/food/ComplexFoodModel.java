/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package com.iceflyer3.pfd.ui.model.api.food;

import com.iceflyer3.pfd.ui.model.api.owner.IngredientWriter;
import com.iceflyer3.pfd.ui.model.api.owner.ServingSizeWriter;
import com.iceflyer3.pfd.ui.model.api.NutritionalSummaryReporter;
import com.iceflyer3.pfd.ui.model.api.viewmodel.ValidatingViewModel;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;

import java.time.LocalDateTime;
import java.util.UUID;

public interface ComplexFoodModel
        extends ServingSizeWriter, IngredientWriter, NutritionalSummaryReporter, ValidatingViewModel
{
    UUID getId();
    String getName() ;
    StringProperty nameProperty();
    void setName(String foodName);

    String getCreatedUser();
    StringProperty createdUserProperty();

    LocalDateTime getCreatedDate();
    ObjectProperty<LocalDateTime> createdDateProperty();

    String getLastModifiedUser();
    StringProperty lastModifiedUserProperty();
    void setLastModifiedUser(String lastModifiedUserName);

    LocalDateTime getLastModifiedDate();
    ObjectProperty<LocalDateTime> lastModifiedDateProperty();
    void setLastModifiedDate(LocalDateTime lastModifiedDate);
}
