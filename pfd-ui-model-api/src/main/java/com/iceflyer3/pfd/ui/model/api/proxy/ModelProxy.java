package com.iceflyer3.pfd.ui.model.api.proxy;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * Proxies are objects used by controllers for interacting with the data of the application.
 *
 * They abstract the loading, saving, and deletion of data for the purposes of both re-usability
 * and not cluttering up controllers which need access to application data.
 *
 * If using the GoF definitions this is really more of a proxy-decorator hybrid implementation
 * as it enhances the ViewModel's interface with the ability to save and delete itself but also
 * controls access to ViewModels by restricting access after a successful deletion.
 *
 * TODO: Potentially down the line, when we get around to implementing this for foods, it could
 *       also be used to restrict access for who can save what foods. So that if we wish (probably
 *       via a settings option) we could show foods for all users but restrict editing of foods
 *       created by users so that only those users can edit them.
 */
public interface ModelProxy {
    void save(ProxyOperationResultCallback saveResultCallback);
    void delete(ProxyOperationResultCallback deleteResultCallback);
}
