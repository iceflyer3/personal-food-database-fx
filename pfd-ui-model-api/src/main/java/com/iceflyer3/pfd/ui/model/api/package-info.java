/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Package that contains the interfaces that are implemented by models used in and by the GUI.
 *
 * This includes the ModelProxy interface which is implemented by the Proxy classes used by the
 * controllers of application screens to facilitate data access.
 *
 * It also includes the interfaces for each model type. Each model interface is implemented by both
 * the view model and the proxy that sits on top of and facilitates access to the view model.
 *
 * Most of the interfaces for individual model types will look like standard JavaBean definitions. This
 * is because they define the public APIs of the view models that should be exposed to the presentation
 * layer via the proxy.
 *
 * The view models may well contain other functionality that isn't exposed via these interfaces that
 * is needed by other parts of the application and which the presentation layer doesn't care about or
 * shouldn't have access to.
 */

package com.iceflyer3.pfd.ui.model.api;