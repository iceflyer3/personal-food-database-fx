package com.iceflyer3.pfd.ui.model.api.mealplanning.carbcycling;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.ui.model.api.viewmodel.ValidatingViewModel;
import com.iceflyer3.pfd.validation.ValidationResult;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;

import java.math.BigDecimal;
import java.util.Set;
import java.util.UUID;

public interface CarbCyclingPlanModel extends ValidatingViewModel
{
    UUID getId();
    int getLowCarbDays();
    IntegerProperty lowCarbDaysProperty();
    void setLowCarbDays(int lowCarbDays);

    int getHighCarbDays();
    IntegerProperty highCarbDaysProperty();
    void setHighCarbDays(int highCarbDays);

    BigDecimal getHighCarbDaySurplusPercentage();
    ObjectProperty<BigDecimal> highCarbDaySurplusPercentageProperty();
    void setHighCarbDaySurplusPercentage(BigDecimal newPercentage);

    BigDecimal getLowCarbDayDeficitPercentage();
    ObjectProperty<BigDecimal> lowCarbDayDeficitPercentageProperty();
    void setLowCarbDayDeficitPercentage(BigDecimal newPercentage);

    int getMealsPerDay();
    IntegerProperty mealsPerDayProperty();
    void setMealsPerDay(int mealsPerDay);

    boolean isActive();
    BooleanProperty isActiveProperty();

    void toggleShouldTimeFats();
    boolean isShouldTimeFats();
    BooleanProperty shouldTimeFatsProperty();

    /**
     * Meal configuration for a carb cycling plan is opt-in.
     *
     * This function initializes the list of meal configurations for this plan based upon the
     * specified number of meals per day
     *
     * @throws IllegalStateException If invoked before the number of meals per day has been specified.
     */
    void initMealConfigurations();

    void clearMealConfigurations();
    boolean hasConfiguredMeals();

    /**
     * Gets an unmodifiable set of the meal configurations defined for this carb cycling plan
     * @return An UnmodifiableSet that contains all meal configurations for this plan.
     */
    Set<CarbCyclingMealConfigurationModel> getMealConfigurations();

    ValidationResult validateMealConfigurations();
}
