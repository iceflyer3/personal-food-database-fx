package com.iceflyer3.pfd.ui.model.api;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


import com.iceflyer3.pfd.enums.Nutrient;
import javafx.beans.property.ReadOnlyObjectProperty;

import java.math.BigDecimal;

/**
 * Interface that provides read-only access to common nutritional information
 */
public interface NutritionFactsReader
{
    BigDecimal getCalories();
    ReadOnlyObjectProperty<BigDecimal> caloriesProperty();

    // Macronutrients
    BigDecimal getProtein();
    ReadOnlyObjectProperty<BigDecimal> proteinProperty();

    BigDecimal getTotalFats();
    ReadOnlyObjectProperty<BigDecimal> totalFatsProperty();

    BigDecimal getTransFat();
    ReadOnlyObjectProperty<BigDecimal> transFatProperty();

    BigDecimal getMonounsaturatedFat();
    ReadOnlyObjectProperty<BigDecimal> monounsaturatedFatProperty();

    BigDecimal getPolyunsaturatedFat();
    ReadOnlyObjectProperty<BigDecimal> polyunsaturatedFatProperty();

    BigDecimal getSaturatedFat();
    ReadOnlyObjectProperty<BigDecimal> saturatedFatProperty();

    BigDecimal getCarbohydrates();
    ReadOnlyObjectProperty<BigDecimal> carbohydratesProperty();

    BigDecimal getSugars();
    ReadOnlyObjectProperty<BigDecimal> sugarsProperty();

    BigDecimal getFiber();
    ReadOnlyObjectProperty<BigDecimal> fiberProperty();


    // Micronutrients - Lipids
    BigDecimal getCholesterol();
    ReadOnlyObjectProperty<BigDecimal> cholesterolProperty();


    // Micronutrients - Minerals
    BigDecimal getIron();
    ReadOnlyObjectProperty<BigDecimal> ironProperty();

    BigDecimal getManganese();
    ReadOnlyObjectProperty<BigDecimal> manganeseProperty();

    BigDecimal getCopper();
    ReadOnlyObjectProperty<BigDecimal> copperProperty();

    BigDecimal getZinc();
    ReadOnlyObjectProperty<BigDecimal> zincProperty();

    BigDecimal getCalcium();
    ReadOnlyObjectProperty<BigDecimal> calciumProperty();

    BigDecimal getMagnesium();
    ReadOnlyObjectProperty<BigDecimal> magnesiumProperty();

    BigDecimal getPhosphorus();
    ReadOnlyObjectProperty<BigDecimal> phosphorusProperty();

    BigDecimal getPotassium();
    ReadOnlyObjectProperty<BigDecimal> potassiumProperty();

    BigDecimal getSodium();
    ReadOnlyObjectProperty<BigDecimal> sodiumProperty();

    BigDecimal getSulfur();
    ReadOnlyObjectProperty<BigDecimal> sulfurProperty();


    // Micronutrients - Vitamins
    BigDecimal getVitaminA();
    ReadOnlyObjectProperty<BigDecimal> vitaminAProperty();

    BigDecimal getVitaminC();
    ReadOnlyObjectProperty<BigDecimal> vitaminCProperty();

    BigDecimal getVitaminK();
    ReadOnlyObjectProperty<BigDecimal> vitaminKProperty();

    BigDecimal getVitaminD();
    ReadOnlyObjectProperty<BigDecimal> vitaminDProperty();

    BigDecimal getVitaminE();
    ReadOnlyObjectProperty<BigDecimal> vitaminEProperty();

    BigDecimal getVitaminB1();
    ReadOnlyObjectProperty<BigDecimal> vitaminB1Property();

    BigDecimal getVitaminB2();
    ReadOnlyObjectProperty<BigDecimal> vitaminB2Property();

    BigDecimal getVitaminB3();
    ReadOnlyObjectProperty<BigDecimal> vitaminB3Property();

    BigDecimal getVitaminB5();
    ReadOnlyObjectProperty<BigDecimal> vitaminB5Property();

    BigDecimal getVitaminB6();
    ReadOnlyObjectProperty<BigDecimal> vitaminB6Property();

    BigDecimal getVitaminB7();
    ReadOnlyObjectProperty<BigDecimal> vitaminB7Property();

    BigDecimal getVitaminB8();
    ReadOnlyObjectProperty<BigDecimal> vitaminB8Property();

    BigDecimal getVitaminB9();
    ReadOnlyObjectProperty<BigDecimal> vitaminB9Property();

    BigDecimal getVitaminB12();
    ReadOnlyObjectProperty<BigDecimal> vitaminB12Property();

    /**
     * Convenience function to call the getter for an arbitrary nutrient
     * @param nutrient The nutrient to call the getter for
     * @return The amount of the nutrient. What precisely the amount means is dependent on the context of the implementation.
     *         For example, for foods it means the amount of the nutrient contained in the food. But for meal plan days
     *         it means the amount of the nutrient consumed in meals on that day.
     */
    default BigDecimal getNutrient(Nutrient nutrient)
    {
        return switch (nutrient)
                {
                    case CALORIES ->
                            getCalories();
                    case PROTEIN ->
                            getProtein();
                    case CARBOHYDRATES ->
                            getCarbohydrates();
                    case SUGARS ->
                            getSugars();
                    case FIBER ->
                            getFiber();
                    case TOTAL_FATS ->
                            getTotalFats();
                    case SATURATED_FAT ->
                            getSaturatedFat();
                    case TRANS_FAT ->
                            getTransFat();
                    case MONOUNSATURATED_FAT ->
                            getMonounsaturatedFat();
                    case POLYUNSATURATED_FAT ->
                            getPolyunsaturatedFat();
                    case CHOLESTEROL ->
                            getCholesterol();
                    case CALCIUM ->
                            getCalcium();
                    case COPPER ->
                            getCopper();
                    case IRON ->
                            getIron();
                    case MAGNESIUM ->
                            getMagnesium();
                    case MANGANESE ->
                            getManganese();
                    case PHOSPHORUS ->
                            getPhosphorus();
                    case POTASSIUM ->
                            getPotassium();
                    case SODIUM ->
                            getSodium();
                    case SULFUR ->
                            getSulfur();
                    case ZINC ->
                            getZinc();
                    case VITAMIN_A ->
                            getVitaminA();
                    case VITAMIN_C ->
                            getVitaminC();
                    case VITAMIN_K ->
                            getVitaminK();
                    case VITAMIN_D ->
                            getVitaminD();
                    case VITAMIN_E ->
                            getVitaminE();
                    case VITAMIN_B1 ->
                            getVitaminB1();
                    case VITAMIN_B2 ->
                            getVitaminB2();
                    case VITAMIN_B3 ->
                            getVitaminB3();
                    case VITAMIN_B5 ->
                            getVitaminB5();
                    case VITAMIN_B6 ->
                            getVitaminB6();
                    case VITAMIN_B7 ->
                            getVitaminB7();
                    case VITAMIN_B8 ->
                            getVitaminB8();
                    case VITAMIN_B9 ->
                            getVitaminB9();
                    case VITAMIN_B12 ->
                            getVitaminB12();
                };
    }
}
