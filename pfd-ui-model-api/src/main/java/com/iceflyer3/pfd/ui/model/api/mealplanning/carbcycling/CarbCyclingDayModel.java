package com.iceflyer3.pfd.ui.model.api.mealplanning.carbcycling;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.enums.CarbCyclingDayType;
import com.iceflyer3.pfd.ui.model.api.mealplanning.MealPlanDayModel;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;

import java.math.BigDecimal;

/**
 * Interface for the CarbCyclingDayModel.
 *
 * This is effectively an extension of the MealPlanningDay so is implemented by the
 * MealPlanDayModelProxy instead of receiving its own proxy.
 *
 * You may notice that there is no getId function or setters / property access for the "required" fields.
 * The id is the same as that of the meal planning day that this extends.
 *
 * The required properties are calculated internally and then don't change so the UI never
 * needs to set them.
 */
public interface CarbCyclingDayModel extends MealPlanDayModel
{
    CarbCyclingDayType getDayType();
    ObjectProperty<CarbCyclingDayType> dayTypeProperty();
    void setDayType(CarbCyclingDayType dayType);

    boolean isTrainingDay();
    BooleanProperty isTrainingDayProperty();
    void toggleTrainingDay();

    BigDecimal getRequiredCalories();
    ObjectProperty<BigDecimal> requiredCaloriesProperty();

    BigDecimal getRequiredProtein();
    ObjectProperty<BigDecimal> requiredProteinProperty();

    BigDecimal getRequiredCarbs();
    ObjectProperty<BigDecimal> requiredCarbsProperty();

    BigDecimal getRequiredFats();
    ObjectProperty<BigDecimal> requiredFatsProperty();
}