package com.iceflyer3.pfd.ui.model.api;

/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

import com.iceflyer3.pfd.ui.model.api.food.ReadOnlyFoodModel;
import com.iceflyer3.pfd.ui.model.api.viewmodel.ValidatingViewModel;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;

import java.math.BigDecimal;
import java.util.UUID;

public interface IngredientModel extends NutritionalSummaryReporter, ValidatingViewModel
{
    UUID getId();
    ReadOnlyFoodModel getIngredientFood();
    ObjectProperty<ReadOnlyFoodModel> ingredientFoodProperty();

    BigDecimal getServingsIncluded();
    void setServingsIncluded(BigDecimal servingsIncluded);
    ObjectProperty<BigDecimal> servingsIncludedProperty();

    boolean isAlternate();
    void setIsAlternate(boolean isAlternate);
    BooleanProperty isAlternateProperty();

    boolean isOptional();
    void setIsOptional(boolean isOptional);
    BooleanProperty isOptionalProperty();
}
