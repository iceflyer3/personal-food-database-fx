/*
 *  Personal Food Database
 *  Copyright (C) 2022, 2023  Josh Mossman
 *
 *  This file is part of Personal Food Database.
 *
 *  Personal Food Database is free software: you can redistribute it and/or modify it under the terms
 *  of the GNU General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  Personal Food Database is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Personal Food Database.
 *  If not, see <https://www.gnu.org/licenses/>.
 */


module pfd.ui.model.api {
    requires javafx.controls;

    requires pfd.base;

    exports com.iceflyer3.pfd.ui.model.api;
    exports com.iceflyer3.pfd.ui.model.api.owner;
    exports com.iceflyer3.pfd.ui.model.api.proxy;

    exports com.iceflyer3.pfd.ui.model.api.viewmodel;

    exports com.iceflyer3.pfd.ui.model.api.food;
    exports com.iceflyer3.pfd.ui.model.api.recipe;
    exports com.iceflyer3.pfd.ui.model.api.mealplanning;
    exports com.iceflyer3.pfd.ui.model.api.mealplanning.registration;
    exports com.iceflyer3.pfd.ui.model.api.mealplanning.carbcycling;
}